<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>FullMainWindow</name>
    <message>
        <location filename="../src/UserInterface/full_mainwindow.cpp" line="87"/>
        <source>Search</source>
        <translation>ᠬᠠᠢᠯᠳᠠ ᠵᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/full_mainwindow.cpp" line="145"/>
        <source>All</source>
        <translation>ᠪᠦᠬᠦ / ᠪᠦᠬᠦᠳᠡ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/full_mainwindow.cpp" line="147"/>
        <source>Letter</source>
        <translation>ᠴᠠᠭᠠᠨ ᠳᠤᠯᠤᠭᠠᠢ ᠵᠢᠨ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/full_mainwindow.cpp" line="149"/>
        <source>Function</source>
        <translation>ᠴᠢᠳᠠᠮᠵᠢ ᠵᠢᠨ ᠳᠦᠷᠦᠯᠵᠢᠬᠦᠯᠦᠯᠳᠡ</translation>
    </message>
</context>
<context>
    <name>FunctionWidget</name>
    <message>
        <location filename="../src/UserInterface/Widget/function_Widget.cpp" line="199"/>
        <source>Search</source>
        <translation>ᠪᠦᠬᠦ ᠲᠠᠯ᠎ᠠ ᠵᠢᠨ ᠬᠠᠢᠯᠳᠠ</translation>
    </message>
</context>
<context>
    <name>ItemDelegate</name>
    <message>
        <source>Open feature sort list</source>
        <translation type="vanished">打开功能排序菜单</translation>
    </message>
    <message>
        <source>Open alphabetical list</source>
        <translation type="vanished">打开字母排序菜单</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/ViewItem/itemdelegate.cpp" line="176"/>
        <source>Open the function sort menu</source>
        <translation>ᠴᠢᠳᠠᠮᠵᠢ ᠵᠢᠨ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤ᠋ᠨ ᠲᠤᠪᠶᠤᠭ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/ViewItem/itemdelegate.cpp" line="178"/>
        <source>Open the alphabetical menu</source>
        <translation>ᠴᠠᠭᠠᠨ ᠳᠤᠯᠤᠭᠠᠢ ᠵᠢᠨ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤ᠋ᠨ ᠲᠤᠪᠶᠤᠭ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="309"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="331"/>
        <source>Search</source>
        <translation>ᠬᠠᠢᠯᠳᠠ ᠵᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="449"/>
        <source>No recent files</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠳ᠋ᠤ᠌ ᠰᠡᠬᠦᠯ ᠤ᠋ᠨ ᠦᠶ᠎ᠡ ᠵᠢᠨ ᠹᠠᠢᠯ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="313"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="490"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="551"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="1119"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="1120"/>
        <source>All</source>
        <translation>ᠪᠦᠬᠦ / ᠪᠦᠬᠦᠳᠡ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="497"/>
        <source>collection</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="498"/>
        <source>recent</source>
        <translation>ᠰᠡᠬᠦᠯ ᠤ᠋ᠨ / ᠤᠢᠷ᠎ᠠ ᠵᠢᠨ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="499"/>
        <source>Max</source>
        <translation>ᠶᠡᠬᠡᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="501"/>
        <source>PowerOff</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠡᠬᠦᠰᠬᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <source>Search application</source>
        <translation type="vanished">搜索应用</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="553"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="1132"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="1133"/>
        <source>Letter</source>
        <translation>ᠴᠠᠭᠠᠨ ᠳᠤᠯᠤᠭᠠᠢ ᠵᠢᠨ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="555"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="1145"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="1146"/>
        <source>Function</source>
        <translation>ᠴᠢᠳᠠᠮᠵᠢ ᠵᠢᠨ ᠳᠦᠷᠦᠯᠵᠢᠬᠦᠯᠦᠯᠳᠡ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="73"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="29"/>
        <source>Office</source>
        <translation>ᠠᠵᠢᠯ ᠠᠯᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="74"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="30"/>
        <source>Development</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ/ ᠨᠡᠬᠡᠬᠡᠨ ᠠᠰᠢᠭᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="75"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="31"/>
        <source>Image</source>
        <translation>ᠢᠮᠡᠭᠸ ᠵᠢᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="76"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="32"/>
        <source>Video</source>
        <translation>ᠳᠦᠷᠰᠦ ᠳᠠᠭᠤ</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="77"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="33"/>
        <source>Internet</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="78"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="34"/>
        <source>Game</source>
        <translation>ᠲᠤᠭᠯᠠᠭᠠᠮ</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="79"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="35"/>
        <source>Education</source>
        <translation>ᠰᠤᠷᠭᠠᠨ ᠬᠦᠮᠦᠵᠢᠯ</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="80"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="36"/>
        <source>Social</source>
        <translation>ᠨᠡᠢᠭᠡᠮ ᠤ᠋ᠨ ᠬᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="81"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="37"/>
        <source>System</source>
        <translation>ᠰᠢᠰᠲᠸᠮ</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="82"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="38"/>
        <source>Safe</source>
        <translation>ᠠᠮᠤᠷ ᠳᠦᠪᠰᠢᠨ</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="83"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="39"/>
        <source>Others</source>
        <translation>ᠪᠤᠰᠤᠳ</translation>
    </message>
</context>
<context>
    <name>RightClickMenu</name>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="213"/>
        <source>Pin to all</source>
        <translation>《 ᠪᠦᠬᠦ ᠰᠤᠹᠲ》 ᠲᠤ᠌ ᠳᠤᠭᠳᠠᠪᠤᠷᠢᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="216"/>
        <source>Unpin from all</source>
        <translation>《 ᠪᠦᠬᠦ ᠰᠤᠹᠲ》 ᠡᠴᠡ ᠳᠤᠭᠳᠠᠪᠤᠷᠢᠵᠢᠭᠤᠯᠤᠯᠳᠠ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="226"/>
        <source>Pin to taskbar</source>
        <translation>ᠡᠬᠦᠷᠭᠡ ᠵᠢᠨ ᠪᠠᠭᠠᠷ ᠲᠤ᠌ ᠳᠤᠭᠳᠠᠪᠤᠷᠢᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="229"/>
        <source>Unpin from taskbar</source>
        <translation>ᠡᠬᠦᠷᠭᠡ ᠵᠢᠨ ᠪᠠᠭᠠᠷ ᠡᠴᠡ ᠳᠤᠭᠳᠠᠪᠤᠷᠢᠵᠢᠭᠤᠯᠤᠯᠳᠠ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="232"/>
        <source>Add to desktop shortcuts</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠳᠦᠳᠡ ᠴᠦᠷᠬᠡ ᠳ᠋ᠤ᠌ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="236"/>
        <source>Pin to collection</source>
        <translation>ᠬᠤᠷᠢᠶᠠᠬᠤ ᠳ᠋ᠤ᠌ ᠳᠤᠭᠳᠠᠪᠤᠷᠢᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="239"/>
        <source>Remove from collection</source>
        <translation>ᠬᠤᠷᠢᠶᠠᠬᠤ ᠡᠴᠡ ᠰᠢᠯᠵᠢᠬᠦᠯᠵᠤ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="254"/>
        <source>Uninstall</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="274"/>
        <source>Switch user</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="281"/>
        <source>Hibernate</source>
        <translation>ᠢᠴᠡᠬᠡᠯᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="288"/>
        <source>Sleep</source>
        <translation>ᠤᠨᠳᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="292"/>
        <source>Lock Screen</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ᠎ᠶᠢ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="297"/>
        <source>Log Out</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="304"/>
        <source>Restart</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="311"/>
        <source>Power Off</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="342"/>
        <source>Personalize this list</source>
        <translation>ᠲᠤᠰ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠵᠢ ᠪᠤᠳᠠᠭᠠᠯᠢ ᠰᠢᠨᠵᠢ ᠲᠠᠢ ᠪᠡᠷ ᠪᠠᠢᠷᠢᠯᠠᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>TabletRightClickMenu</name>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/tabletrightclickmenu.cpp" line="220"/>
        <source>Pin to taskbar</source>
        <translation>ᠡᠬᠦᠷᠭᠡ ᠵᠢᠨ ᠪᠠᠭᠠᠷ ᠲᠤ᠌ ᠳᠤᠭᠳᠠᠪᠤᠷᠢᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/tabletrightclickmenu.cpp" line="223"/>
        <source>Unpin from taskbar</source>
        <translation>ᠡᠬᠦᠷᠭᠡ ᠵᠢᠨ ᠪᠠᠭᠠᠷ ᠡᠴᠡ ᠳᠤᠭᠳᠠᠪᠤᠷᠢᠵᠢᠭᠤᠯᠤᠯᠳᠠ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/tabletrightclickmenu.cpp" line="229"/>
        <source>Add to desktop shortcuts</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠳᠦᠳᠡ ᠴᠦᠷᠬᠡ ᠳ᠋ᠤ᠌ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/tabletrightclickmenu.cpp" line="248"/>
        <source>Uninstall</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
</context>
</TS>
