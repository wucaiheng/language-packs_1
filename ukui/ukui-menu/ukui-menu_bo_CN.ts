<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>FullMainWindow</name>
    <message>
        <location filename="../src/UserInterface/full_mainwindow.cpp" line="87"/>
        <source>Search</source>
        <translation>ཉེར་སྤྱོད་འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/full_mainwindow.cpp" line="145"/>
        <source>All</source>
        <translation>ཚང་མ།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/full_mainwindow.cpp" line="147"/>
        <source>Letter</source>
        <translation>གསལ་བཤད་ཀྱི་གོ་རིམ།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/full_mainwindow.cpp" line="149"/>
        <source>Function</source>
        <translation>བྱེད་ནུས་ཀྱི་རིགས་དབྱེ།</translation>
    </message>
</context>
<context>
    <name>FunctionWidget</name>
    <message>
        <location filename="../src/UserInterface/Widget/function_Widget.cpp" line="199"/>
        <source>Search</source>
        <translation>ཧྲིལ་བོ་འཚོལ་ཞིབ།</translation>
    </message>
</context>
<context>
    <name>ItemDelegate</name>
    <message>
        <source>Open feature sort list</source>
        <translation type="vanished">打开功能排序菜单</translation>
    </message>
    <message>
        <source>Open alphabetical list</source>
        <translation type="vanished">打开字母排序菜单</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/ViewItem/itemdelegate.cpp" line="176"/>
        <source>Open the function sort menu</source>
        <translation>བྱེད་ལས་རིགས་ཀྱི་འདེམས་པང་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/ViewItem/itemdelegate.cpp" line="178"/>
        <source>Open the alphabetical menu</source>
        <translation>དབྱངས་གསལ་ཡི་གེའི་འདེམས་པང་ཁ་ཕྱེ་བ།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="309"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="331"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ་ཉེར་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="449"/>
        <source>No recent files</source>
        <translation>ཉེ་དུས་ཀྱི་ཡིག་ཆ་མེད།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="313"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="490"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="551"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="1119"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="1120"/>
        <source>All</source>
        <translation>ཚང་མ།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="497"/>
        <source>collection</source>
        <translation>བསྡུ་ཉར།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="498"/>
        <source>recent</source>
        <translation>ཉེ་ཆར།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="499"/>
        <source>Max</source>
        <translation>ཆེར་སྐྱེད།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="501"/>
        <source>PowerOff</source>
        <translation>གློག་ཁུངས།</translation>
    </message>
    <message>
        <source>Search application</source>
        <translation type="vanished">搜索应用</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="553"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="1132"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="1133"/>
        <source>Letter</source>
        <translation>གསལ་བཤད་ཀྱི་གོ་རིམ།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="555"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="1145"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="1146"/>
        <source>Function</source>
        <translation>བྱེད་ནུས་རིགས་དབྱེ།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="73"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="29"/>
        <source>Office</source>
        <translation>གཞུང་ལས་ཁང་།</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="74"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="30"/>
        <source>Development</source>
        <translation>གསར་སྤེལ།</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="75"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="31"/>
        <source>Image</source>
        <translation>པར་རིས།</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="76"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="32"/>
        <source>Video</source>
        <translation>བརྙན་ཕབ།</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="77"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="33"/>
        <source>Internet</source>
        <translation>དྲ་སྦྲེལ།</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="78"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="34"/>
        <source>Game</source>
        <translation>རོལ་རྩེད།</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="79"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="35"/>
        <source>Education</source>
        <translation>སློབ་གསོ།</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="80"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="36"/>
        <source>Social</source>
        <translation>སྤྱི་ཚོགས།</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="81"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="37"/>
        <source>System</source>
        <translation>མ་ལག</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="82"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="38"/>
        <source>Safe</source>
        <translation>བདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="83"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="39"/>
        <source>Others</source>
        <translation>གཞན་དག</translation>
    </message>
</context>
<context>
    <name>RightClickMenu</name>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="213"/>
        <source>Pin to all</source>
        <translation>མཉེན་ཆས་ཡོད་ཚད་ལ་གཏན་འཁེལ་བྱས།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="216"/>
        <source>Unpin from all</source>
        <translation>མི་ཚང་མའི་ཁྲོད་ནས་བཀག་འགོག་བྱེད།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="226"/>
        <source>Pin to taskbar</source>
        <translation>ལས་འགན་སྒྲུབ་པར་ཁབ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="229"/>
        <source>Unpin from taskbar</source>
        <translation>ལས་འགན་སྒྲུབ་ས་ནས་ཁ་པར་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="232"/>
        <source>Add to desktop shortcuts</source>
        <translation>ཅོག་ཙེའི་སྟེང་གི་མགྱོགས་ལམ་ཁ་སྣོན་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="236"/>
        <source>Pin to collection</source>
        <translation>འཚོལ་སྡུད་བྱེད་པའི་ཁབ་སྐུད།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="239"/>
        <source>Remove from collection</source>
        <translation>བསྡུ་ཉར་དངོས་རྫས་ཁྲོད་ནས་ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="254"/>
        <source>Uninstall</source>
        <translation>ལེན་པ།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="274"/>
        <source>Switch user</source>
        <translation>སྤྱོད་མཁན་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="281"/>
        <source>Hibernate</source>
        <translation>མལ་གསོ།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="288"/>
        <source>Sleep</source>
        <translation>གཉིད་ཁུག་པ།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="292"/>
        <source>Lock Screen</source>
        <translation>ཟྭ་ངོས།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="297"/>
        <source>Log Out</source>
        <translation>ཕྱིར་འཐེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="304"/>
        <source>Restart</source>
        <translation>ཡང་བསྐྱར་འགོ་འཛུགས་པ།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="311"/>
        <source>Power Off</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="342"/>
        <source>Personalize this list</source>
        <translation>རང་གཤིས་ཅན་གྱི་བཀོད་སྒྲིག་རེའུ་མིག</translation>
    </message>
</context>
<context>
    <name>TabletRightClickMenu</name>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/tabletrightclickmenu.cpp" line="220"/>
        <source>Pin to taskbar</source>
        <translation>ལས་འགན་སྒྲུབ་པར་ཁབ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/tabletrightclickmenu.cpp" line="223"/>
        <source>Unpin from taskbar</source>
        <translation>ལས་འགན་སྒྲུབ་ས་ནས་ཁ་པར་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/tabletrightclickmenu.cpp" line="229"/>
        <source>Add to desktop shortcuts</source>
        <translation>ཅོག་ཙེའི་སྟེང་གི་མགྱོགས་ལམ་ཁ་སྣོན་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/tabletrightclickmenu.cpp" line="248"/>
        <source>Uninstall</source>
        <translation>ལེན་པ།</translation>
    </message>
</context>
</TS>
