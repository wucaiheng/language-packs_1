<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>Dialog</name>
    <message>
        <location filename="dialog.ui" line="14"/>
        <source>cast</source>
        <translatorcomment>投屏</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialog.ui" line="39"/>
        <location filename="dialog.cpp" line="15"/>
        <source>allow</source>
        <translatorcomment>允许</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialog.ui" line="52"/>
        <source>XX request cast</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialog.ui" line="71"/>
        <source>refuse</source>
        <translatorcomment>拒绝</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialog.ui" line="84"/>
        <source>12345678</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialog.cpp" line="35"/>
        <location filename="dialog.cpp" line="73"/>
        <source>ok</source>
        <translatorcomment>确认</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="src/widget.ui" line="26"/>
        <source>Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="67"/>
        <source>请求投屏</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="102"/>
        <source>无操作10S后自动退出</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="143"/>
        <source>禁止</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="166"/>
        <source>允许</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="230"/>
        <source>请输入如下PIN码：********</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="265"/>
        <source>配对中</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="288"/>
        <source>请直接重新连接，或重新开关投屏功能后再次连接</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="326"/>
        <location filename="src/widget.ui" line="439"/>
        <location filename="src/widget.ui" line="544"/>
        <source>退出</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="378"/>
        <source>多媒体协商中/成功/失败</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="401"/>
        <source>播放器准备启动</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="503"/>
        <source>投屏设备已断开</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/widget.cpp" line="113"/>
        <source>request cast.</source>
        <oldsource>request cast</oldsource>
        <translatorcomment>请求投屏。</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/widget.cpp" line="196"/>
        <source>The device is disconnected！</source>
        <translatorcomment>投屏设备已断开！</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/widget.cpp" line="224"/>
        <source>request cast.Please enter the pin code on mobile</source>
        <translatorcomment>请求投屏。请在手机端输入以下PIN码</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/widget.cpp" line="291"/>
        <source>Pairing failed, please reconnect...</source>
        <translatorcomment>配对失败,请重新连接...</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/widget.cpp" line="305"/>
        <source>The environment is initializing, please wait</source>
        <translatorcomment>投屏环境初始化中，请等待</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/widget.cpp" line="309"/>
        <source>request cast.Connecting......</source>
        <translatorcomment>请求投屏。连接中......</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/widget.cpp" line="320"/>
        <source>request cast.Multimedia connect failed</source>
        <translatorcomment>请求投屏。多媒体连接失败</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/widget.cpp" line="326"/>
        <source>request cast.Multimedia connect succeeded</source>
        <translatorcomment>请求投屏。多媒体连接成功</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>widget2</name>
    <message>
        <location filename="widget2.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widget2.ui" line="26"/>
        <source>PPO A5请求投屏.。。</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widget2.ui" line="39"/>
        <source>允许</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widget2.ui" line="52"/>
        <source>拒绝</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
