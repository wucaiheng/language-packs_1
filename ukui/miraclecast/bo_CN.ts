<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN" sourcelanguage="en">
<context>
    <name>MobileType</name>
    <message>
        <source>MobileType</source>
        <translation type="vanished">སྒུལ་བདེའི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="14"/>
        <source>cast</source>
        <translatorcomment>投屏</translatorcomment>
        <translation>འཆར་ངོས།</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="81"/>
        <source>Please confirm the device type</source>
        <translation>སྒྲིག་ཆས་ཀྱི་རིགས་དབྱིབས་གཏན་འཁེལ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="107"/>
        <source>huawei</source>
        <translation>ཧྭ་ཝེ།</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="112"/>
        <source>xiaomi</source>
        <translation>ཞའོ་སྨི།</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="117"/>
        <source>other</source>
        <translation>གཞན་དག</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="147"/>
        <source>Note: to replace the device model again, you need to delete the historical device record. Turn off and then turn on the screen projection function.</source>
        <oldsource>Note: to replace the mobile phone model again, you need to delete the historical device record. Turn off and then turn on the screen projection function.</oldsource>
        <translation>མཆན་འགྲེལ། ཡང་བསྐྱར་སྒྲིག་ཆས་ཀྱི་དཔེ་དབྱིབས་ཀྱི་ཚབ་བྱེད་དགོས་ན། ལོ་རྒྱུས་ཀྱི་སྒྲིག་ཆས་ཟིན་ཐོ་བསུབ་དགོས། སྒོ་བརྒྱབ་རྗེས་སླར་ཡང་བརྙན་ཤེལ་གྱི་རྣམ་གྲངས་ཀྱི་ནུས་པ་འདོན་སྤེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="198"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="217"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">གྲོན་ཆུང་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation type="vanished">སྣང་མེད་དུ་བཞག་</translation>
    </message>
    <message>
        <source>Incorrect selection will affect the use</source>
        <translation type="vanished">ཡང་དག་མིན་པའི་བསལ་འདེམས་ཀྱིས་བཀོལ་སྤྱོད་བྱེད་པར</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="118"/>
        <source>huawei</source>
        <translation>ཧྭ་ཝེ།</translation>
    </message>
    <message>
        <location filename="main.cpp" line="122"/>
        <source>xiaomi</source>
        <translation>ཞའོ་སྨི།</translation>
    </message>
    <message>
        <location filename="main.cpp" line="234"/>
        <source>projection</source>
        <translation>འཆར་ངོས་བསྡུ་ལེན་སྣེ།</translation>
    </message>
    <message>
        <location filename="main.cpp" line="237"/>
        <source>Note：device can not support uibc</source>
        <oldsource>Note：the access device does not support control return.</oldsource>
        <translation>མཉམ་འཇོག་བྱོས་སྒྲིག་ཆས་ཀྱིས་ཕྱིར་བསྐུར་བར་ཚོད་འཛིན་བྱེད་པར་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="main.cpp" line="238"/>
        <source>Can&apos;t be controlled by keyboard/mouse.</source>
        <oldsource>Access device cannot be controlled using the keyboard and mouse.</oldsource>
        <translation>མཐེབ་གཞོང་དང་ཙིག་རྟགས་ཚོད་འཛིན་སྒྲིག་ཆས་བཀོལ་མི་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>playWidget</name>
    <message>
        <location filename="playwidget.ui" line="17"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="playwidget.cpp" line="103"/>
        <location filename="playwidget.cpp" line="969"/>
        <location filename="playwidget.cpp" line="1001"/>
        <source>SD</source>
        <translation>མཉེན།</translation>
    </message>
    <message>
        <location filename="playwidget.cpp" line="981"/>
        <source>HD</source>
        <translation>མཁྲེགས།</translation>
    </message>
    <message>
        <location filename="playwidget.cpp" line="987"/>
        <source>warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="playwidget.cpp" line="987"/>
        <source>The current device does not support hard decoding</source>
        <translation>ད་ལྟའི་སྒྲིག་ཆས་ཀྱིས་སྲ་མཁྲེགས་ཅན་གྱི་སྒྲིག་ཆས་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="playwidget.cpp" line="987"/>
        <source>Yes</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
</context>
</TS>
