<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en">
<context>
    <name>MobileType</name>
    <message>
        <source>MobileType</source>
        <translation type="vanished">手机类型</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="14"/>
        <source>cast</source>
        <translatorcomment>投屏</translatorcomment>
        <translation>投屏</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="81"/>
        <source>Please confirm the device type</source>
        <translation>请确认设备类型</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="107"/>
        <source>huawei</source>
        <translation>华为</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="112"/>
        <source>xiaomi</source>
        <translation>小米</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="117"/>
        <source>other</source>
        <translation>其他</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="147"/>
        <source>Note: to replace the device model again, you need to delete the historical device record. Turn off and then turn on the screen projection function.</source>
        <oldsource>Note: to replace the mobile phone model again, you need to delete the historical device record. Turn off and then turn on the screen projection function.</oldsource>
        <translation type="unfinished">注意：重新更换手机型号需删除该历史设备记录，先关闭再开启投屏功能。</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="198"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="217"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation type="vanished">忽略</translation>
    </message>
    <message>
        <source>Incorrect selection will affect the use</source>
        <translation type="vanished">如果误选会影响使用</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="118"/>
        <source>huawei</source>
        <translation>华为</translation>
    </message>
    <message>
        <location filename="main.cpp" line="122"/>
        <source>xiaomi</source>
        <translation>小米</translation>
    </message>
    <message>
        <location filename="main.cpp" line="234"/>
        <source>projection</source>
        <translation>投屏接收端</translation>
    </message>
    <message>
        <location filename="main.cpp" line="237"/>
        <source>Note：device can not support uibc</source>
        <oldsource>Note：the access device does not support control return.</oldsource>
        <translation>注意：设备不支持控制回传</translation>
    </message>
    <message>
        <location filename="main.cpp" line="238"/>
        <source>Can&apos;t be controlled by keyboard/mouse.</source>
        <oldsource>Access device cannot be controlled using the keyboard and mouse.</oldsource>
        <translation>无法使用键盘和鼠标控制设备</translation>
    </message>
</context>
<context>
    <name>playWidget</name>
    <message>
        <location filename="playwidget.ui" line="17"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="playwidget.cpp" line="103"/>
        <location filename="playwidget.cpp" line="969"/>
        <location filename="playwidget.cpp" line="1001"/>
        <source>SD</source>
        <translation>软</translation>
    </message>
    <message>
        <location filename="playwidget.cpp" line="981"/>
        <source>HD</source>
        <translation>硬</translation>
    </message>
    <message>
        <location filename="playwidget.cpp" line="987"/>
        <source>warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="playwidget.cpp" line="987"/>
        <source>The current device does not support hard decoding</source>
        <translation>当前设备不支持硬解码</translation>
    </message>
    <message>
        <location filename="playwidget.cpp" line="987"/>
        <source>Yes</source>
        <translation>确定</translation>
    </message>
</context>
</TS>
