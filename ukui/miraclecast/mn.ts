<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn" sourcelanguage="en">
<context>
    <name>MobileType</name>
    <message>
        <source>MobileType</source>
        <translation type="vanished">手机类型</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="14"/>
        <source>cast</source>
        <translatorcomment>投屏</translatorcomment>
        <translation>ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="81"/>
        <source>Please confirm the device type</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="107"/>
        <source>huawei</source>
        <translation>ᠬᠤᠸᠠ ᠸᠸᠢ</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="112"/>
        <source>xiaomi</source>
        <translation>ᠱᠢᠶᠤᠤ ᠮᠢ</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="117"/>
        <source>other</source>
        <translation>ᠪᠤᠰᠤᠳ</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="147"/>
        <source>Note: to replace the device model again, you need to delete the historical device record. Turn off and then turn on the screen projection function.</source>
        <oldsource>Note: to replace the mobile phone model again, you need to delete the historical device record. Turn off and then turn on the screen projection function.</oldsource>
        <translation>ᠠᠩᠬᠠᠷᠬᠤ ᠨᠢ᠄ ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠬᠡᠯᠪᠡᠷᠢ ᠮᠠᠶᠢᠭ ᠢ᠋ ᠳᠠᠬᠢᠵᠤ ᠰᠤᠯᠢᠬᠤ ᠳ᠋ᠤ᠌ ᠳᠤᠰ ᠲᠡᠤᠬᠡᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ᠋ ᠬᠠᠰᠤᠬᠤ ᠴᠢᠬᠤᠯᠠᠳᠠᠢ᠂ ᠤᠷᠢᠳᠠᠪᠠᠷ ᠬᠠᠭᠠᠵᠤ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠭᠠᠬᠤ ᠴᠢᠳᠠᠮᠵᠢ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠨ᠎ᠡ.</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="198"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="mobiletype.ui" line="217"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation type="vanished">忽略</translation>
    </message>
    <message>
        <source>Incorrect selection will affect the use</source>
        <translation type="vanished">如果误选会影响使用</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="118"/>
        <source>huawei</source>
        <translation>ᠬᠤᠸᠠ ᠸᠸᠢ</translation>
    </message>
    <message>
        <location filename="main.cpp" line="122"/>
        <source>xiaomi</source>
        <translation>ᠱᠢᠶᠤᠤ ᠮᠢ</translation>
    </message>
    <message>
        <location filename="main.cpp" line="234"/>
        <source>projection</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠭᠠᠬᠤ ᠵᠢ ᠬᠦᠯᠢᠶᠡᠨ ᠠᠪᠬᠤ ᠦᠵᠦᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="main.cpp" line="237"/>
        <source>Note：device can not support uibc</source>
        <oldsource>Note：the access device does not support control return.</oldsource>
        <translation>ᠠᠩᠬᠠᠷᠬᠤ ᠨᠢ᠄ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠨᠢ ᠡᠵᠡᠮᠰᠢᠯ ᠤ᠋ᠨ ᠪᠤᠴᠠᠭᠠᠨ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="main.cpp" line="238"/>
        <source>Can&apos;t be controlled by keyboard/mouse.</source>
        <oldsource>Access device cannot be controlled using the keyboard and mouse.</oldsource>
        <translation>ᠳᠠᠷᠤᠭᠤᠯ ᠤ᠋ᠨ ᠲᠠᠪᠠᠭ ᠪᠤᠯᠤᠨ ᠬᠤᠯᠤᠭᠠᠨᠴᠢᠷ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠡᠵᠡᠮᠰᠢᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ.</translation>
    </message>
</context>
<context>
    <name>playWidget</name>
    <message>
        <location filename="playwidget.ui" line="17"/>
        <source>Form</source>
        <translation>ᠹᠣᠣᠮ</translation>
    </message>
    <message>
        <location filename="playwidget.cpp" line="103"/>
        <location filename="playwidget.cpp" line="969"/>
        <location filename="playwidget.cpp" line="1001"/>
        <source>SD</source>
        <translation>SD</translation>
    </message>
    <message>
        <location filename="playwidget.cpp" line="981"/>
        <source>HD</source>
        <translation>HD</translation>
    </message>
    <message>
        <location filename="playwidget.cpp" line="987"/>
        <source>warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="playwidget.cpp" line="987"/>
        <source>The current device does not support hard decoding</source>
        <translation>ᠤᠳᠤ ᠵᠢᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠬᠠᠳᠠᠭᠤᠨ ᠳᠠᠢᠯᠤᠮᠵᠢ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="playwidget.cpp" line="987"/>
        <source>Yes</source>
        <translation>ᠮᠦᠨ（ᠳᠡᠢᠮᠤ）</translation>
    </message>
</context>
</TS>
