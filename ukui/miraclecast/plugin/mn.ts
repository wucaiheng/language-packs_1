<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>ChangeProjectionName</name>
    <message>
        <source>Name is too long, change another one.</source>
        <translation type="vanished">名称过长，请更改</translation>
    </message>
    <message>
        <source>Change Username</source>
        <translation type="vanished">修改用户名</translation>
    </message>
    <message>
        <source>Change Name</source>
        <translation type="vanished">修改名称</translation>
    </message>
    <message>
        <source>Projection to this computer</source>
        <translation type="vanished">投屏到此电脑</translation>
    </message>
    <message>
        <location filename="../changeprojectionname.ui" line="26"/>
        <source>Projection to this pc</source>
        <translation>ᠳᠤᠰ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../changeprojectionname.ui" line="120"/>
        <source>Device Name</source>
        <translation>ᠲᠦᠭᠦᠭᠡᠷᠤᠮᠵᠢ ᠶ᠋ᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <source>Changename</source>
        <translation type="vanished">修改名称</translation>
    </message>
    <message>
        <source>ChangeProjectionname</source>
        <translation type="vanished">设备名称</translation>
    </message>
    <message>
        <location filename="../changeprojectionname.ui" line="244"/>
        <source>Save</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../changeprojectionname.ui" line="225"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Name is too long.</source>
        <translation type="vanished">名称过长</translation>
    </message>
    <message>
        <source>The input character length exceeds the limit.</source>
        <translation type="vanished">输入字符长度超出限制</translation>
    </message>
    <message>
        <location filename="../changeprojectionname.cpp" line="38"/>
        <source>The length must be 1-32 characters</source>
        <translation>ᠤᠷᠳᠤ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ 1-32 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ</translation>
    </message>
</context>
<context>
    <name>Projection</name>
    <message>
        <source>projection</source>
        <translation type="vanished">多屏协同</translation>
    </message>
    <message>
        <source>Projection to this computer</source>
        <translation type="vanished">投屏到此电脑</translation>
    </message>
    <message>
        <location filename="../projection.cpp" line="52"/>
        <source>Projection</source>
        <translation>ᠤᠯᠠᠨ ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠵᠢᠯᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../projection.cpp" line="659"/>
        <location filename="../projection.cpp" line="660"/>
        <location filename="../projection.cpp" line="694"/>
        <location filename="../projection.cpp" line="695"/>
        <source>(WLAN is not turned on. You need to turn on WLAN to use this function.)</source>
        <translation>(WLAN ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠬᠡ ᠦᠬᠡᠢ᠂ ᠳᠤᠰ ᠴᠢᠳᠠᠮᠵᠢ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠳ᠋ᠤ᠌WLAN ᠵᠢ/ ᠢ᠋ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ ᠴᠢᠬᠤᠯᠠᠳᠠᠢ.)</translation>
    </message>
    <message>
        <source>(No wireless network card is detected or the network card driver does not support it.).</source>
        <translation type="vanished">未检测到无线网卡或网卡驱动不可使用</translation>
    </message>
    <message>
        <source>Allow Android phones or computers to be projected to this computer</source>
        <translation type="vanished">允许Andriod手机或电脑投屏到此电脑</translation>
    </message>
    <message>
        <source>Projection to the computer</source>
        <translation type="vanished">投屏到此电脑</translation>
    </message>
    <message>
        <source>Allow android phone or pc to be projected to the pc.</source>
        <translation type="vanished">允许安卓手机或电脑投屏到此电脑</translation>
    </message>
    <message>
        <source>Wireless LAN transmission may be interrupted during projection</source>
        <translation type="vanished">投屏过程中可能会中断无线局域网络传输。</translation>
    </message>
    <message>
        <source>You need to match the PIN code when projecting the screen to this computer</source>
        <translation type="vanished">投屏到这台电脑时需要匹配PIN码</translation>
    </message>
    <message>
        <location filename="../projection.ui" line="117"/>
        <location filename="../projection.ui" line="719"/>
        <location filename="../projection.cpp" line="596"/>
        <location filename="../projection.cpp" line="597"/>
        <location filename="../projection.cpp" line="620"/>
        <location filename="../projection.cpp" line="621"/>
        <source>(No wireless network card is detected or the network card driver does not support it.)</source>
        <translation>(ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠺᠠᠷᠲ ᠪᠤᠶᠤ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠺᠠᠷᠲ ᠬᠦᠳᠡᠯᠭᠡᠬᠦᠷ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠤᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ.)</translation>
    </message>
    <message>
        <source>PIN code authentication projection is not supported temporarily.</source>
        <translation type="vanished">暂不支持PIN码认证投射。</translation>
    </message>
    <message>
        <source>You need to match the pin code when projecting the screen to this computer</source>
        <translation type="vanished">投屏到这台电脑时需要匹配pin码</translation>
    </message>
    <message>
        <location filename="../projection.ui" line="101"/>
        <source>Projection to the pc</source>
        <translation>ᠳᠤᠰ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Allow android phone or pc to be projected to the pc</source>
        <translation type="vanished">允许Android手机或电脑投屏到此电脑</translation>
    </message>
    <message>
        <location filename="../projection.ui" line="241"/>
        <source>Allow android phone or pc to be projected to the pc:</source>
        <translation>Andriod ᠭᠠᠷ ᠤᠳᠠᠰᠤ ᠪᠤᠶᠤ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ ᠢ᠋ ᠳᠤᠰ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠭᠠᠬᠤ ᠵᠢ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ:</translation>
    </message>
    <message>
        <source>Wireless LAN transmission may be interrupted during projection.</source>
        <translation type="vanished">投屏过程中可能会中断无线局域网络连接。</translation>
    </message>
    <message>
        <location filename="../projection.ui" line="326"/>
        <source>Wireless LAN connection may be interrupted during projection.</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠭᠠᠬᠤ ᠶᠠᠪᠤᠴᠠ ᠳ᠋ᠤ᠌ ᠬᠡᠰᠡᠭ ᠬᠡᠪᠴᠢᠶᠡᠨ ᠤ᠋ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠳᠠᠰᠤᠯᠤᠭᠳᠠᠵᠤ ᠮᠡᠳᠡᠨ᠎ᠡ.</translation>
    </message>
    <message>
        <location filename="../projection.ui" line="431"/>
        <source>You need to match the PIN code when projecting the screen to the pc</source>
        <translation>ᠳᠤᠰ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠬᠤ ᠦᠶᠡᠰ PIN ᠺᠤᠳ᠋ ᠲᠠᠢ ᠬᠤᠤᠰᠯᠠᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ</translation>
    </message>
    <message>
        <location filename="../projection.ui" line="531"/>
        <source>Historical equipment</source>
        <translation>ᠳᠡᠤᠬᠡᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../projection.ui" line="700"/>
        <source>Connect to wireless display</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠦᠵᠡᠬᠦᠷ ᠲᠤ᠌ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Open(PIN authentication and receiving end of SP1 version is not supported.)</source>
        <translation type="vanished">开启（此功能暂不支持PIN码认证和投射到SP1版本的接收端。）</translation>
    </message>
    <message>
        <source>Open(PIN authentication and receiving end of SP1 version is not supported temporarily.)</source>
        <translation type="vanished">开启（此功能暂不支持PIN码认证和投射到SP1版本的接收端。）</translation>
    </message>
    <message>
        <location filename="../projection.ui" line="822"/>
        <source>Open(PIN authentication is not supported temporarily.)</source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ( ᠳᠤᠰ ᠴᠢᠳᠠᠮᠵᠢ ᠨᠢ ᠤᠳᠤᠬᠠᠨ ᠳ᠋ᠤ᠌PIN ᠺᠤᠳ᠋ ᠵᠢᠡᠷ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ.)</translation>
    </message>
    <message>
        <location filename="../projection.ui" line="832"/>
        <source>When enabled,the project to this pc feature stops.</source>
        <translation>ᠡᠬᠢᠬᠦᠯᠦᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ《 ᠳᠤᠰ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠭᠠᠬᠤ》 ᠵᠢ ᠵᠤᠭᠰᠤᠭᠠᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <source>Open(The function doesn&apos;t support pin code authentication temporarily.)</source>
        <translation type="vanished">开启（此功能暂不支持PIN码认证。）</translation>
    </message>
    <message>
        <source>Open(This function does not support PIN code authentication temporarily.)</source>
        <translation type="vanished">开启（此功能暂不支持PIN码认证。）</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">开启</translation>
    </message>
    <message>
        <source>When enabled, the project to this PC feature stops.PIN code authentication projection is not supported temporarily.</source>
        <translation type="vanished">启用后会停止“投影到此电脑”，暂不支持PIN码认证</translation>
    </message>
    <message>
        <source>When enabled, the project to this PC feature stops.</source>
        <translation type="vanished">启用后会停止“投屏到此电脑”功能。</translation>
    </message>
    <message>
        <source>refreshBtn</source>
        <translation type="vanished">刷新</translation>
    </message>
    <message>
        <source>Open projection</source>
        <translation type="vanished">开启多屏协同</translation>
        <extra-contents_path>/projection/Open projection</extra-contents_path>
    </message>
    <message>
        <source>Disable Pin Code</source>
        <translation type="vanished">禁用Pin码</translation>
    </message>
    <message>
        <source>refresh</source>
        <translation type="vanished">刷新</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <source>Failed to remove device</source>
        <translation type="vanished">删除设备失败</translation>
    </message>
    <message>
        <source>Device deleted successfully</source>
        <translation type="vanished">删除设备成功</translation>
    </message>
    <message>
        <source>Multi screen collaboration</source>
        <translation type="vanished">多屏协同</translation>
    </message>
    <message>
        <location filename="../projection.cpp" line="93"/>
        <source>Find device</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠡᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <source>Please click refresh and try again</source>
        <translation type="vanished">请点击刷新后重新尝试</translation>
    </message>
    <message>
        <source>warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Please restart the screen projection button</source>
        <translation type="vanished">请重启投屏开关</translation>
    </message>
    <message>
        <source>tips</source>
        <translation type="vanished">提示</translation>
    </message>
    <message>
        <source>Please restart the screen projection switch button</source>
        <translation type="vanished">请重启投屏开关</translation>
    </message>
    <message>
        <source>Receive terminal is </source>
        <translation type="vanished">接收端已</translation>
    </message>
    <message>
        <source>Service exception,please restart the system</source>
        <translation type="vanished">服务异常，请重启系统</translation>
    </message>
    <message>
        <source>After deleting the device, you need to restart the projection to take effect</source>
        <translation type="vanished">删除设备后需要重启投屏才能生效</translation>
    </message>
    <message>
        <source>Please keep WLAN on;
Wireless-network functions will be invalid when the screen projection on</source>
        <translation type="vanished">使用时请保持WLAN处于开启状态；开启投屏后，无线网络相关功能会失效</translation>
    </message>
    <message>
        <source>Please keep WLAN on;
Wireless will be temporarily disconnected when the screen projection on</source>
        <translation type="vanished">使用时请保持WLAN处于开启状态；开启投屏会短暂中断无线连接</translation>
    </message>
    <message>
        <source>After opening the switch button,open the projection screen in the mobile phone drop-down menu,follow the prompts.See the user manual for details</source>
        <translation type="vanished">开启投屏后，在手机下拉菜单中打开投屏，根据提示操作。详见用户手册</translation>
    </message>
    <message>
        <source>List of connected devices</source>
        <translation type="vanished">已连接设备列表</translation>
    </message>
    <message>
        <source>It is recommended to restart the screen after deleting the device</source>
        <translation type="vanished">删除设备后需要重启投屏才能生效</translation>
    </message>
    <message>
        <source>WLAN is off, please turn on WLAN</source>
        <translation type="vanished">WLAN未开启，请打开WLAN开关</translation>
    </message>
    <message>
        <source>Wireless network card is busy. Please try again later</source>
        <translation type="vanished">无线网卡繁忙，请稍后再试</translation>
    </message>
    <message>
        <source>Projection is </source>
        <translation type="vanished">投屏功能已</translation>
    </message>
    <message>
        <source>on</source>
        <translation type="vanished">开启</translation>
    </message>
    <message>
        <source>off</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>The projection function is </source>
        <translation type="vanished">投屏功能已</translation>
    </message>
    <message>
        <source>tuen on</source>
        <translation type="vanished">打开</translation>
    </message>
    <message>
        <source>turn off</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>turn on</source>
        <translation type="obsolete">打开</translation>
    </message>
    <message>
        <source>Please enable or refresh the scan at the projection device</source>
        <translation type="vanished">请在投屏设备端开启或刷新扫描</translation>
    </message>
    <message>
        <source>You need to turn on the projection again</source>
        <translation type="vanished">需要投屏请再重新开启</translation>
    </message>
    <message>
        <source>You need to turn on the screen again</source>
        <translation type="vanished">需要投屏请再重新开启</translation>
    </message>
    <message>
        <source>Failed to execute. Please reopen the page later</source>
        <translation type="vanished">执行失败，请再次打开该页面查看</translation>
    </message>
    <message>
        <source>Projection Name</source>
        <translation type="vanished">投屏端名称</translation>
    </message>
    <message>
        <location filename="../projection.cpp" line="907"/>
        <source>Add Bluetooths</source>
        <translation>ᠯᠠᠨᠶᠠ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
</context>
</TS>
