<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>BasePopupTitle</name>
    <message>
        <source>Font Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>BasePreviewArea</name>
    <message>
        <source>Enter Text Content For Preview</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BaseSearchEdit</name>
    <message>
        <source>Search</source>
        <translation>ᠡᠷᠢᠬᠦ</translation>
    </message>
</context>
<context>
    <name>FontListView</name>
    <message>
        <source>Add Font</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Apply Font</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Remove Font</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠢ᠋ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Export Font</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠢ᠋ ᠭᠠᠷᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel Collection</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠵᠢ ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Check Font</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠦ᠋ᠨ ᠮᠡᠳᠡᠭᠡ</translation>
    </message>
    <message>
        <source>Collection</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Add Fonts</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <source>font(*.ttf *.fon *.ttc *.afm)</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠦ᠋ᠨ ᠹᠡᠢᠯ (*.ttf *.fon *.ttc *.afm)</translation>
    </message>
    <message>
        <source>Export Fonts</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠭᠠᠷᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Build the core strength of Chinese operating system</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FunctionWid</name>
    <message>
        <source>All Font</source>
        <translation>ᠪᠦᠭᠦᠳᠡ ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌</translation>
    </message>
    <message>
        <source>System Font</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠦ᠋ᠨ ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌</translation>
    </message>
    <message>
        <source>User Font</source>
        <translation>ᠮᠢᠨᠤ ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌</translation>
    </message>
    <message>
        <source>Collection Font</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <source>Font Viewer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PopupAbout</name>
    <message>
        <source>Version: </source>
        <translation>ᠬᠡᠪᠯᠡᠯ᠄ </translation>
    </message>
    <message>
        <source>Font Viewer is a tool to help users install and organize management; After installation, the font can be applied to self-developed applications, third-party pre installed applications and user self installed applications.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠬᠢᠬᠡᠳ ᠳᠡᠮᠵᠢᠯᠬᠡ᠄ </translation>
    </message>
    <message>
        <source>Font Viewer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PopupFontInfo</name>
    <message>
        <source>FontName</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠤᠯ</translation>
    </message>
    <message>
        <source>FontSeries</source>
        <translation>ᠱᠢᠷᠢᠰ</translation>
    </message>
    <message>
        <source>FontStyle</source>
        <translation>ᠶᠠᠩᠵᠤ</translation>
    </message>
    <message>
        <source>FontType</source>
        <translation>ᠲᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <source>FontVersion</source>
        <translation>ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <source>FontPath</source>
        <translation>ᠤᠷᠤᠨ ᠪᠠᠢᠷᠢ</translation>
    </message>
    <message>
        <source>FontCopyright</source>
        <translation>ᠬᠡᠪᠯᠡᠯ ᠦ᠋ᠨ ᠡᠷᠬᠡ</translation>
    </message>
    <message>
        <source>FontTrademark</source>
        <translation>ᠱᠣᠱᠢᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">继续</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>PopupInstallFail</name>
    <message>
        <source>There is a problem with the font file. Installation failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">继续</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
</context>
<context>
    <name>PopupInstallSuccess</name>
    <message>
        <source>already installed </source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠤᠭᠰᠠᠷᠠᠪᠠ </translation>
    </message>
    <message>
        <source> fonts!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> fonts!  </source>
        <translation type="vanished">款字体！</translation>
    </message>
</context>
<context>
    <name>PopupRemove</name>
    <message>
        <source>Are you sure you want to remove this font?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Add Font</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠦ᠋ᠨ ᠪᠠᠭ᠎ᠠ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation>ᠲᠣᠪᠶᠣᠭ</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>ᠬᠠᠪᠰᠤᠷᠬᠤ</translation>
    </message>
    <message>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠦ᠋ᠨ ᠶᠡᠬᠡ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation>ᠤᠤᠯ ᠬᠡᠪ ᠲᠦ᠍ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
</TS>
