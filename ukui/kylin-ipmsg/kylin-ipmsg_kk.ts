<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>ChatMsg</name>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="137"/>
        <source>Resend</source>
        <translation>Резенд</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="138"/>
        <source>Copy</source>
        <translation>Көшіру</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="139"/>
        <source>Open</source>
        <translation>Ашу</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="140"/>
        <source>Open Directory</source>
        <translation>Каталогты ашу</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="141"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="954"/>
        <source>Save As</source>
        <translation>Басқаша сақтау</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="142"/>
        <source>Delete</source>
        <translation>Өшіру</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="143"/>
        <source>Clear All</source>
        <translation>Барлығын жою</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="216"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="225"/>
        <source>Folder</source>
        <translation>Қапшық</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="234"/>
        <source>Screen Shot</source>
        <translation>Скриншот</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="243"/>
        <source>History Message</source>
        <translation>Журнал хабары</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="260"/>
        <source>Send</source>
        <translation>Жіберу</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="498"/>
        <source>Message send failed, please check whether IP connection is successful!</source>
        <translation>Хабар жіберілмеді, IP байланысының сәтті екенін тексеріңіз!</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="611"/>
        <source>Can not write file</source>
        <translation>Файлды жазу мүмкін емес</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="808"/>
        <source>No such file or directory!</source>
        <translation>Ондай файл немесе каталог жоқ!</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="974"/>
        <source>Delete the currently selected message?</source>
        <translation>Таңдалған хабарды жою қажет пе?</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="977"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1005"/>
        <source>No</source>
        <translation>Жоқ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1054"/>
        <source>folder</source>
        <translation>қапшық</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="976"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1004"/>
        <source>Yes</source>
        <translation>Иә</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1002"/>
        <source>Clear all current messages?</source>
        <translation>Барлық ағымдағы хабарларды тазаласын ба?</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1024"/>
        <source>Send Files</source>
        <translation>Файлдарды жіберу</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1051"/>
        <source>Send Folders</source>
        <translation>Қапшықтарды жіберу</translation>
    </message>
</context>
<context>
    <name>ChatSearch</name>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="289"/>
        <source>Delete</source>
        <translation>Өшіру</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation type="obsolete">清空聊天记录</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="175"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="187"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="317"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="369"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="477"/>
        <source>Search</source>
        <translation>Іздеу</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="198"/>
        <source>All</source>
        <translation>Барлығы</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="205"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="210"/>
        <source>Image/Video</source>
        <translation>Сурет/Бейне</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="218"/>
        <source>Link</source>
        <translation>Сілтеме</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="224"/>
        <source>canael</source>
        <translation>канаэль</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="229"/>
        <source>sure</source>
        <translation>сенімді</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="239"/>
        <source>DeleteMenu</source>
        <translation>DeleteMenu</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="131"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="250"/>
        <source>Batch delete</source>
        <translation>Партияны жою</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="47"/>
        <source>Chat content</source>
        <translation>Чат мазмұны</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="132"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="252"/>
        <source>Clear all messages</source>
        <translation>Барлық хабарларды тазалау</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="158"/>
        <source>Chat Content</source>
        <translation>Чат мазмұны</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="290"/>
        <source>Choose Delete</source>
        <translation>Жою пәрменін таңдау</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="291"/>
        <source>Open</source>
        <translation>Ашу</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="292"/>
        <source>Open Directory</source>
        <translation>Каталогты ашу</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="696"/>
        <source>No such file or directory!</source>
        <translation>Ондай файл немесе каталог жоқ!</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="757"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="967"/>
        <source>Delete the currently selected message?</source>
        <translation>Таңдалған хабарды жою қажет пе?</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="760"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="798"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="970"/>
        <source>No</source>
        <translation>Жоқ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="759"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="797"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="969"/>
        <source>Yes</source>
        <translation>Иә</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="795"/>
        <source>Clear all current messages?</source>
        <translation>Барлық ағымдағы хабарларды тазаласын ба?</translation>
    </message>
</context>
<context>
    <name>Control</name>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../../src/controller/control.cpp" line="284"/>
        <source>Anonymous</source>
        <translation>Анонимді</translation>
    </message>
</context>
<context>
    <name>FriendInfoWid</name>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="150"/>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="177"/>
        <source>Messages</source>
        <translation>Хабарлар</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="186"/>
        <source>Close</source>
        <translation>Жабу</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="215"/>
        <source>Username</source>
        <translation>Пайдаланушы аты</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="224"/>
        <source>IP Address</source>
        <translation>IP адресі</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="234"/>
        <source>Nickname</source>
        <translation>Лақап аты</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="332"/>
        <source>Add</source>
        <translation>Қосу</translation>
    </message>
</context>
<context>
    <name>FriendListView</name>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="96"/>
        <source>Start Chat</source>
        <translation>Чат бастау</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="97"/>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="150"/>
        <source>Set to Top</source>
        <translation>Жоғарыға орнату</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="98"/>
        <source>Change Nickname</source>
        <translation>Лақап атын өзгерту</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="99"/>
        <source>View Info</source>
        <translation>Ақпаратты қарау</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="100"/>
        <source>Delete Friend</source>
        <translation>Досты өшіру</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="153"/>
        <source>Cancel the Top</source>
        <translation>Жоғарғы жағынан бас тарту</translation>
    </message>
</context>
<context>
    <name>KyView</name>
    <message>
        <location filename="../../src/view/kyview.cpp" line="133"/>
        <source>Messages</source>
        <translation>Хабарлар</translation>
    </message>
</context>
<context>
    <name>LocalInfo</name>
    <message>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="174"/>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="187"/>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="262"/>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="349"/>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="378"/>
        <source>Search</source>
        <translation>Іздеу</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="160"/>
        <source>Modify Name</source>
        <translation>Атауын өзгерту</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="169"/>
        <source>Open Directory</source>
        <translation>Каталогты ашу</translation>
    </message>
</context>
<context>
    <name>LocalUpdateName</name>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="157"/>
        <source>Set Username</source>
        <translation>Пайдаланушы атын орнату</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="158"/>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="209"/>
        <source>Please enter username</source>
        <translation>Пайдаланушы атын енгізіңіз</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="161"/>
        <source>Change nickname</source>
        <translation>Лақап атын өзгерту</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="162"/>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="211"/>
        <source>Please enter friend nickname</source>
        <translation>Досыңыздың лақап атын енгізіңіз</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="165"/>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="170"/>
        <source>Confirm</source>
        <translation>Растау</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="177"/>
        <source>Skip</source>
        <translation>Өткізіп жіберу</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="214"/>
        <source>The length of user name is less than 20 words</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="216"/>
        <source>Please do not enter special characters</source>
        <translation>Арнайы таңбалар енгізбеуіңізді өтінемін</translation>
    </message>
</context>
<context>
    <name>SearchMsgDelegate</name>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/search_msg_delegate.cpp" line="44"/>
        <source> relevant chat records</source>
        <translation> тиісті чат жазбалары</translation>
    </message>
</context>
<context>
    <name>SearchPage</name>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="122"/>
        <source>Start Chat</source>
        <translation>Чат бастау</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="123"/>
        <source>Set to Top</source>
        <translation>Жоғарыға орнату</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="124"/>
        <source>Change Nickname</source>
        <translation>Лақап атын өзгерту</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="125"/>
        <source>View Info</source>
        <translation>Ақпаратты қарау</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="126"/>
        <source>Delete Friend</source>
        <translation>Досты өшіру</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="164"/>
        <source>Friend</source>
        <translation>Дос</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="168"/>
        <source>Chat Record</source>
        <translation>Чат жазбасы</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="142"/>
        <source>Minimize</source>
        <translation>Кішірейту</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="143"/>
        <source>Close</source>
        <translation>Жабу</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="146"/>
        <source>Messages</source>
        <translation>Хабарлар</translation>
    </message>
</context>
<context>
    <name>TitleSeting</name>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="103"/>
        <source>File Save Directory</source>
        <translation>Файлды сақтау каталогы</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="116"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="232"/>
        <source>Change Directory</source>
        <translation>Каталогты өзгерту</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="123"/>
        <source>Clear All Chat Messages</source>
        <translation>Барлық чат хабарларын тазалау</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="130"/>
        <source>Clear the Cache</source>
        <translation>Кэшті тазалау</translation>
    </message>
    <message>
        <source>Modified successfully</source>
        <translation type="vanished">修改成功</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="251"/>
        <source>Modified Successfully</source>
        <translation>Сәтті өзгертілген</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="292"/>
        <source>Clear all messages？</source>
        <translation>Барлық хабарларды тазалаңдар ма?</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="295"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="322"/>
        <source>No</source>
        <translation>Жоқ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="294"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="323"/>
        <source>Yes</source>
        <translation>Иә</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="238"/>
        <source>The save path can only be a dir under the home dir!</source>
        <translation>Сақтау жолы үй дирінің астындағы кір ғана болуы мүмкін!</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="306"/>
        <source>Cleared</source>
        <translation>Тазаланған</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="320"/>
        <source>Clean the cache information such as images/videos/documents?</source>
        <translation>Суреттер/бейнелер/құжаттар сияқты кэш ақпаратын тазаласын ба?</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="336"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="360"/>
        <source>Clean Up Complete</source>
        <translation>Аяқтауды тазалау</translation>
    </message>
    <message>
        <source>Clean up complete</source>
        <translation type="vanished">清理完成</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="387"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="394"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="402"/>
        <source>Please do not save the file in this directory</source>
        <translation>Файлды осы каталогта сақтамауыңызды өтінемін</translation>
    </message>
</context>
<context>
    <name>TrayIconWid</name>
    <message>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="157"/>
        <source>Ignore</source>
        <translation>Елемеу</translation>
    </message>
    <message>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="124"/>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="168"/>
        <source>Messages</source>
        <translation>Хабарлар</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="53"/>
        <source>Menu</source>
        <translation>Мәзір</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="67"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="148"/>
        <source>Settings</source>
        <translation>Параметрлер</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="69"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="146"/>
        <source>Theme</source>
        <translation>Тақырып</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="71"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="144"/>
        <source>Help</source>
        <translation>Анықтама</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="73"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="142"/>
        <source>About</source>
        <translation>Шамамен</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="81"/>
        <source>Follow the Theme</source>
        <translation>Тақырыпты бақылау</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="85"/>
        <source>Light Theme</source>
        <translation>Жарық тақырыбы</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="89"/>
        <source>Dark Theme</source>
        <translation>Қараңғы тақырып</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="301"/>
        <source>Version: </source>
        <translation>Нұсқасы: </translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="306"/>
        <source>Message provides text chat and file transfer functions in the LAN. There is no need to build a server. It supports multiple people to interact at the same time and send and receive in parallel.</source>
        <translation>Хабар LAN-да мәтіндік чат және файлды беру функцияларын қамтамасыз етеді. Сервер құрудың қажеті жоқ. Ол бірнеше адамның бір мезгілде өзара әрекеттесіп, параллель жіберіп, қабылдауын қолдайды.</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="411"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="431"/>
        <source>Service &amp; Support: </source>
        <translation>Қызмет және қолдау: </translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.h" line="65"/>
        <source>Messages</source>
        <translation>Хабарлар</translation>
    </message>
</context>
</TS>
