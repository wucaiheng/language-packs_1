<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>Touchpad</name>
    <message>
        <location filename="../touchpad-settings.cpp" line="37"/>
        <source>Touchpad</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TouchpadUI</name>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="63"/>
        <source>Touchpad</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="82"/>
        <source>Disable touchpad when using the mouse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="100"/>
        <source>Pointer Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="102"/>
        <source>Slow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="103"/>
        <source>Fast</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="127"/>
        <source>Disable touchpad when typing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="146"/>
        <source>Touch and click on the touchpad</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="165"/>
        <source>Scroll bar slides with finger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="183"/>
        <source>Scrolling area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="186"/>
        <source>Two-finger scrolling in the middle area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="187"/>
        <source>Edge scrolling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="188"/>
        <source>Disable scrolling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="202"/>
        <source>gesture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="233"/>
        <source>three fingers click</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="234"/>
        <source>show global search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="239"/>
        <source>swipe three fingers down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="240"/>
        <source>show desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="245"/>
        <source>swipe three fingers up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="246"/>
        <source>open the multitasking view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="251"/>
        <source>swipe three fingers horizontally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="252"/>
        <source>switch windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="257"/>
        <source>four fingers click</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="258"/>
        <source>show sidebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="265"/>
        <source>swipe four fingers horizontally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="266"/>
        <source>switch desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="294"/>
        <source>more gesture</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
