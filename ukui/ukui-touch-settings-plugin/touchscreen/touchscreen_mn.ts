<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>GestureGuidance</name>
    <message>
        <source>Gesture</source>
        <translation type="vanished">手势</translation>
    </message>
    <message>
        <location filename="../gestureguidance.ui" line="72"/>
        <source>Gesture Guidance</source>
        <translation>ᠭᠠᠷ ᠎ᠤᠨ ᠳᠤᠬᠢᠶ᠎ᠠ ᠎ᠪᠠᠷ ᠬᠦᠳᠦᠯᠬᠦ᠌</translation>
    </message>
</context>
<context>
    <name>ItemWidget</name>
    <message>
        <location filename="../item-widget/itemwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../item-widget/itemwidget.ui" line="80"/>
        <location filename="../item-widget/itemwidget.ui" line="195"/>
        <location filename="../item-widget/itemwidget.ui" line="230"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../item-widget/itemwidget.ui" line="112"/>
        <source>PushButton</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TouchScreen</name>
    <message>
        <location filename="../touchscreen.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../touchscreen.ui" line="37"/>
        <source>Label1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../touchscreen.ui" line="113"/>
        <source>Label2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../touchscreen.cpp" line="82"/>
        <source>touchscreen gesture</source>
        <translatorcomment>触摸屏手势</translatorcomment>
        <translation>ᠬᠦᠷᠦᠯᠴᠡᠬᠦᠢ ᠳᠡᠯᠬᠡᠴᠡ ᠎ᠶ᠋ᠢᠨ ᠭᠠᠷ ᠎ᠤᠨ ᠲᠤᠬᠢᠶ᠎ᠠ</translation>
        <extra-contents_path>/TouchScreen/touchscreen gesture</extra-contents_path>
    </message>
    <message>
        <location filename="../touchscreen.cpp" line="84"/>
        <source>more gesture</source>
        <translatorcomment>更多手势</translatorcomment>
        <translation>ᠨᠡᠩ ᠤᠯᠠᠨ ᠭᠠᠷ ᠎ᠤᠨ ᠲᠤᠬᠢᠶ᠎ᠠ</translation>
        <extra-contents_path>/TouchScreen/more gesture</extra-contents_path>
    </message>
    <message>
        <location filename="../touchscreen.cpp" line="92"/>
        <source>swipe up from the bottom edge</source>
        <translatorcomment>手指从屏幕底部边缘向上滑动</translatorcomment>
        <translation>ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠎ᠢᠶᠠᠨ ᠳᠡᠯᠬᠡᠴᠡᠨ ᠎ᠦ ᠳᠤᠤᠷ᠎ᠠ ᠵᠠᠬ᠎ᠠ ᠎ᠠᠴᠠ ᠳᠡᠭᠡᠭ᠌ᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchscreen.cpp" line="93"/>
        <source>open the multitasking view</source>
        <translatorcomment>打开多任务视图</translatorcomment>
        <translation>ᠤᠯᠠᠨ ᠡᠬᠦᠷᠬᠡᠳᠦ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ ᠎ᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../touchscreen.cpp" line="96"/>
        <source>swipe down from the top edge</source>
        <translatorcomment>单指从屏幕顶端向下滑动</translatorcomment>
        <translation>ᠭᠠᠭ᠍ᠴᠠ ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠳᠡᠯᠬᠡᠴᠡᠨ ᠎ᠦ ᠳᠡᠭᠡᠷ᠎ᠡ ᠵᠠᠬ᠎ᠠ ᠎ᠡᠴᠡ ᠳᠤᠷᠤᠭ᠍ᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchscreen.cpp" line="97"/>
        <source>show desktop</source>
        <translatorcomment>显示桌面</translatorcomment>
        <translation>ᠱᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠎ᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../touchscreen.cpp" line="100"/>
        <source>swipe left from the right edge</source>
        <translatorcomment>单指从屏幕右侧向左滑入</translatorcomment>
        <translation>ᠭᠠᠭ᠍ᠴᠠ ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠎ᠦ ᠪᠠᠷᠠᠭᠤᠨ ᠭᠠᠷ ᠲᠠᠯ᠎ᠠ ᠎ᠠᠴᠠ ᠵᠡᠬᠦᠨᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠵᠤ ᠤᠷᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchscreen.cpp" line="101"/>
        <source>show sidebar</source>
        <translatorcomment>呼出侧边栏</translatorcomment>
        <translation>ᠬᠠᠵᠠᠭᠤ ᠪᠠᠭᠠᠷ ᠎ᠢ ᠳᠠᠭᠤᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchscreen.cpp" line="104"/>
        <source>swipe four fingers down anywhere</source>
        <translatorcomment>四指屏幕任意位置下滑</translatorcomment>
        <translation>ᠳᠦᠷᠪᠡᠨ ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠳᠡᠯᠬᠡᠴᠡᠨ ᠎ᠦ ᠶᠠᠮᠠᠷᠪᠠ ᠪᠠᠢᠷᠢ ᠎ᠠᠴᠠ ᠳᠤᠷᠤᠭ᠍ᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchscreen.cpp" line="105"/>
        <source>show global search</source>
        <translatorcomment>呼出全局搜索</translatorcomment>
        <translation>ᠪᠦᠷᠢᠨ ᠬᠠᠢᠯᠲᠠ ᠎ᠶ᠋ᠢ ᠳᠠᠭᠤᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchscreen.cpp" line="108"/>
        <source>swipe four fingers horizontally</source>
        <translatorcomment>四指屏幕向左或向右滑动</translatorcomment>
        <translation>ᠳᠦᠷᠪᠡᠨ ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠳᠡᠯᠭᠡᠴᠡ ᠎ᠶ᠋ᠢ ᠵᠡᠬᠦᠨᠰᠢ ᠪᠤᠶᠤ ᠪᠠᠷᠠᠭᠤᠨᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchscreen.cpp" line="109"/>
        <source>switch windows</source>
        <translatorcomment>在打开的窗口之间切换</translatorcomment>
        <translation>ᠨᠡᠬᠡᠬᠡᠭ᠌ᠰᠡᠨ ᠴᠤᠩᠬᠤᠨ ᠎ᠤ ᠬᠤᠭᠤᠷᠤᠨᠳᠤ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
</context>
<context>
    <name>TouchscreenSettings</name>
    <message>
        <location filename="../touchscreen-settings.cpp" line="24"/>
        <location filename="../touchscreen-settings.cpp" line="28"/>
        <source>GestureGuidance</source>
        <translation>ᠭᠠᠷ ᠎ᠤᠨ ᠲᠤᠬᠢᠶ᠎ᠠ ᠎ᠪᠠᠷ ᠬᠦᠳᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../touchscreen-settings.cpp" line="26"/>
        <source>TouchScreen</source>
        <translatorcomment>触摸屏</translatorcomment>
        <translation>ᠬᠦᠷᠦᠯᠴᠡᠬᠦᠢ ᠳᠡᠯᠭᠡᠴᠡ</translation>
    </message>
</context>
</TS>
