<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Touchpad</name>
    <message>
        <location filename="../touchpad-settings.cpp" line="37"/>
        <source>Touchpad</source>
        <translatorcomment>触控板</translatorcomment>
        <translation>触控板</translation>
    </message>
</context>
<context>
    <name>TouchpadUI</name>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="63"/>
        <source>Touchpad</source>
        <translatorcomment>触控板</translatorcomment>
        <translation>触控板</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="82"/>
        <source>Disable touchpad when using the mouse</source>
        <translation>插入鼠标时禁用触控板</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="100"/>
        <source>Pointer Speed</source>
        <translation>指针速度</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="102"/>
        <source>Slow</source>
        <translation>慢</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="103"/>
        <source>Fast</source>
        <translation>快</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="127"/>
        <source>Disable touchpad when typing</source>
        <translation>打字时禁用触控板</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="146"/>
        <source>Touch and click on the touchpad</source>
        <translation>触控板轻触点击</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="165"/>
        <source>Scroll bar slides with finger</source>
        <translation>滚动条跟随手指滑动</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="183"/>
        <source>Scrolling area</source>
        <translation>滚动区域</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="186"/>
        <source>Two-finger scrolling in the middle area</source>
        <translation>中间区域滚动</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="187"/>
        <source>Edge scrolling</source>
        <translation>边界滚动</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="188"/>
        <source>Disable scrolling</source>
        <translation>禁止滚动</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="202"/>
        <source>gesture</source>
        <translation>手势</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="294"/>
        <source>more gesture</source>
        <translation>更多手势</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="233"/>
        <source>three fingers click</source>
        <translation>三指轻点</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="234"/>
        <source>show global search</source>
        <translation>呼出全局搜索</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="239"/>
        <source>swipe three fingers down</source>
        <translation>三指向下滑动</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="245"/>
        <source>swipe three fingers up</source>
        <translation>三指向上滑动</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="240"/>
        <source>show desktop</source>
        <translation>显示桌面</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="246"/>
        <source>open the multitasking view</source>
        <translation>打开多任务视图</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="251"/>
        <source>swipe three fingers horizontally</source>
        <translation>三指左右滑动</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="252"/>
        <source>switch windows</source>
        <translation>打开的窗口之间切换</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="257"/>
        <source>four fingers click</source>
        <translation>四指点击</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="258"/>
        <source>show sidebar</source>
        <translation>呼出侧边栏</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="265"/>
        <source>swipe four fingers horizontally</source>
        <translation>四指左右滑动</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="266"/>
        <source>switch desktop</source>
        <translation>桌面之间切换</translation>
    </message>
</context>
</TS>
