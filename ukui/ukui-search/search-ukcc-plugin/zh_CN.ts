<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Search</name>
    <message>
        <location filename="../search.cpp" line="13"/>
        <location filename="../search.cpp" line="121"/>
        <source>Search</source>
        <translation>全局搜索</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="145"/>
        <source>Create index</source>
        <translation>创建索引</translation>
        <extra-contents_path>/Search/Create index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="146"/>
        <source>Creating index can help you getting results quickly.</source>
        <translation>开启之后可以快速获取搜索结果</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="165"/>
        <source>Default web searching engine</source>
        <translation>默认互联网搜索引擎</translation>
        <extra-contents_path>/Search/Default web searching engine</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="169"/>
        <source>baidu</source>
        <translation>百度</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="170"/>
        <source>sougou</source>
        <translation>搜狗</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="171"/>
        <source>360</source>
        <translation>360</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="179"/>
        <source>Block Folders</source>
        <translation>排除的文件夹</translation>
        <extra-contents_path>/Search/Block Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="184"/>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation>搜索将不查看以下文件夹，通过添加和删除可以设置排除的文件夹位置</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="225"/>
        <source>Choose folder</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="338"/>
        <source>delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="393"/>
        <source>Directories</source>
        <translation>文件夹</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="394"/>
        <source>select blocked folder</source>
        <translation>选择排除的文件夹</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="395"/>
        <source>Select</source>
        <translation>选择</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="396"/>
        <source>Position: </source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="397"/>
        <source>FileName: </source>
        <translation>文件名</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="398"/>
        <source>FileType: </source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="399"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="415"/>
        <location filename="../search.cpp" line="419"/>
        <location filename="../search.cpp" line="423"/>
        <location filename="../search.cpp" line="427"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="415"/>
        <source>Add blocked folder failed, choosen path is empty!</source>
        <translation>添加失败，选择的路径为空！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="419"/>
        <source>Add blocked folder failed, it is not in home path!</source>
        <translation>添加失败，添加的路径不在家目录下！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="423"/>
        <source>Add blocked folder failed, its parent dir is exist!</source>
        <translation>添加失败，父目录已被添加！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="427"/>
        <source>Add blocked folder failed, it has been already blocked!</source>
        <translation>添加失败，这个文件夹已经被添加过了！</translation>
    </message>
</context>
</TS>
