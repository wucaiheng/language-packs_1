<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Search</name>
    <message>
        <location filename="../search.cpp" line="13"/>
        <location filename="../search.cpp" line="121"/>
        <source>Search</source>
        <translation>ཧྲིལ་བོ་འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="145"/>
        <source>Create index</source>
        <translation>གསར་འཛུགས་འཚོལ་ཞིབ་བྱེད་པར་ཁྲིད་སྟོན།</translation>
        <extra-contents_path>/Search/Create index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="146"/>
        <source>Creating index can help you getting results quickly.</source>
        <translation>སྟོན་གྲངས་གསར་སྐྲུན་བྱས་ན་ཁྱོད་ལ་མགྱོགས་མྱུར་ངང་གྲུབ་འབྲས་ཐོབ་པར་རོགས་རམ་བྱེད་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="165"/>
        <source>Default web searching engine</source>
        <translation>མཉམ་འབྲེལ་དྲ་རྒྱའི་འཚོལ་བཤེར་འཕྲུལ་ཆས་ཁས་བླངས།</translation>
        <extra-contents_path>/Search/Default web searching engine</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="169"/>
        <source>baidu</source>
        <translation>པའེ་ཏུའུ།</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="170"/>
        <source>sougou</source>
        <translation>སོའོ་གོའུ།</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="171"/>
        <source>360</source>
        <translation>360</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="179"/>
        <source>Block Folders</source>
        <translation>དོར་བའི་ཡིག་ཆའི་ཁུག་མ།</translation>
        <extra-contents_path>/Search/Block Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="184"/>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation>འཚོལ་ཞིབ་ཀྱིས་གཤམ་གྱི་ཡིག་ཁུག་ལ་ལྟ་ཞིབ་མི་བྱེད་པ་དང་།ཁ་སྣོན་དང་བསུབ་པ་བརྒྱུད་ནས་ཕྱིར་འབུད་བྱས་ཆོག་པའི་ཡིག་ཁུག་གི་གནས་སུ་འཇོག་དགོས།</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="225"/>
        <source>Choose folder</source>
        <translation>སྣོན་པ།</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="338"/>
        <source>delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="393"/>
        <source>Directories</source>
        <translation>ཡིག་ཁུག</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="394"/>
        <source>select blocked folder</source>
        <translation>བཀག་སྡོམ་བྱས་པའི་ཡིག་སྣོད་གདམ་གསེས།</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="395"/>
        <source>Select</source>
        <translation>བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="396"/>
        <source>Position: </source>
        <translation>གོ་གནས་ནི། </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="397"/>
        <source>FileName: </source>
        <translation>ཡིག་ཆའི་མིང་ནི། </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="398"/>
        <source>FileType: </source>
        <translation>ཡིག་ཆའི་རིགས་དབྱིབས་ནི། </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="399"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="415"/>
        <location filename="../search.cpp" line="419"/>
        <location filename="../search.cpp" line="423"/>
        <location filename="../search.cpp" line="427"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="415"/>
        <source>Add blocked folder failed, choosen path is empty!</source>
        <translation>ཁ་སྣོན་བྱས་པ་ཕམ་སོང་།་གདམ་གསེས་བྱས་པའི་ཐབས་ལམ་སྟོང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="419"/>
        <source>Add blocked folder failed, it is not in home path!</source>
        <translation>ཁ་སྣོན་བྱས་པ་ཕམ་སོང་།ཁ་སྣོན་བྱས་པའི་ལམ་བུ་ཁྱིམ་ན་མེད་པའི་དཀར་ཆག་འོག་ན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="423"/>
        <source>Add blocked folder failed, its parent dir is exist!</source>
        <translation>ཁ་སྣོན་བྱས་པ་ཕམ་སོང་།ཨ་ཕའི་དཀར་ཆག་ཁ་སྣོན་བྱས་སོང་།</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="427"/>
        <source>Add blocked folder failed, it has been already blocked!</source>
        <translation>སྦྱོར་རྟ་ལྐོག་བཀོད་མིང་ཐོ་ཕམ་ཁ་བསལ་འདེམས་ཀྱི་དཀར་ཆག་ནི་ལྐོག་བཀོད་མིང་ཐོ་འི་ཁྲོད་ཀྱི་དཀར་ཆག་འོག</translation>
    </message>
</context>
</TS>
