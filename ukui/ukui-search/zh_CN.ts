<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>UkuiSearch::BestListWidget</name>
    <message>
        <location filename="../../frontend/view/best-list-view.cpp" line="312"/>
        <source>Best Matches</source>
        <translation>最佳匹配</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::ContentWidget</name>
    <message>
        <source>Recently Opened</source>
        <translation type="vanished">最近</translation>
    </message>
    <message>
        <source>Open Quickly</source>
        <translation type="vanished">快速入口</translation>
    </message>
    <message>
        <source>Commonly Used</source>
        <translation type="vanished">常用</translation>
    </message>
    <message>
        <source>Apps</source>
        <translation type="vanished">应用</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">配置项</translation>
    </message>
    <message>
        <source>Files</source>
        <translation type="vanished">文件</translation>
    </message>
    <message>
        <source>Dirs</source>
        <translation type="vanished">文件夹</translation>
    </message>
    <message>
        <source>File Contents</source>
        <translation type="vanished">文件内容</translation>
    </message>
    <message>
        <source>Best Matches</source>
        <translation type="vanished">最佳匹配</translation>
    </message>
    <message>
        <source>Web Pages</source>
        <translation type="vanished">网页</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="vanished">未知</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::CreateIndexAskDialog</name>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="41"/>
        <source>ukui-search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="70"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="94"/>
        <source>Creating index can help you getting results quickly, whether to create or not?</source>
        <translation>创建索引可以快速获取搜索结果，是否创建？</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="105"/>
        <source>Don&apos;t remind</source>
        <translation>不再提醒</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="116"/>
        <source>No</source>
        <translation>否(N)</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="118"/>
        <source>Yes</source>
        <translation>是(Y)</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FolderListItem</name>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="538"/>
        <source>Delete the folder out of blacklist</source>
        <translation>删除</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::HomePage</name>
    <message>
        <source>Open Quickly</source>
        <translation type="vanished">快速入口</translation>
    </message>
    <message>
        <source>Recently Opened</source>
        <translation type="vanished">最近</translation>
    </message>
    <message>
        <source>Commonly Used</source>
        <translation type="vanished">常用</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MainWindow</name>
    <message>
        <location filename="../../frontend/mainwindow.cpp" line="69"/>
        <source>ukui-search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../../frontend/mainwindow.cpp" line="76"/>
        <source>Global Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::OptionView</name>
    <message>
        <source>Open</source>
        <translation type="vanished">打开</translation>
    </message>
    <message>
        <source>Add Shortcut to Desktop</source>
        <translation type="vanished">添加到桌面快捷方式</translation>
    </message>
    <message>
        <source>Add Shortcut to Panel</source>
        <translation type="vanished">添加到任务栏快捷方式</translation>
    </message>
    <message>
        <source>Open path</source>
        <translation type="vanished">打开文件所在路径</translation>
    </message>
    <message>
        <source>Copy path</source>
        <translation type="vanished">复制文件路径</translation>
    </message>
    <message>
        <source>Install</source>
        <translation type="vanished">安装</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchBarHLayout</name>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchDetailView</name>
    <message>
        <source>Introduction: %1</source>
        <translation type="vanished">软件介绍: %1</translation>
    </message>
    <message>
        <source>Application</source>
        <translation type="vanished">应用</translation>
    </message>
    <message>
        <source>Document</source>
        <translation type="vanished">文件</translation>
    </message>
    <message>
        <source>Preview is not avaliable</source>
        <translation type="vanished">当前预览不可用</translation>
    </message>
    <message>
        <source>Path</source>
        <translation type="vanished">路径</translation>
    </message>
    <message>
        <source>Last time modified</source>
        <translation type="vanished">上次修改时间</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchLineEdit</name>
    <message>
        <location filename="../../frontend/control/search-line-edit.cpp" line="56"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SettingsWidget</name>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="35"/>
        <source>ukui-search-settings</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="81"/>
        <location filename="../../frontend/control/settings-widget.cpp" line="299"/>
        <location filename="../../frontend/control/settings-widget.cpp" line="503"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="108"/>
        <source>&lt;h2&gt;Settings&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;设置&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="113"/>
        <source>&lt;h3&gt;Index State&lt;/h3&gt;</source>
        <translation>&lt;h3&gt;索引状态&lt;/h3&gt;</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="115"/>
        <location filename="../../frontend/control/settings-widget.cpp" line="117"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="125"/>
        <source>&lt;h3&gt;File Index Settings&lt;/h3&gt;</source>
        <translation>&lt;h3&gt;文件索引设置&lt;/h3&gt;</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="127"/>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation>搜索将不再查看以下文件夹。通过增加和删除文件夹可进行文件索引设置。</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="136"/>
        <source>Add ignored folders</source>
        <translation>添加文件夹至黑名单</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="157"/>
        <source>&lt;h3&gt;Search Engine Settings&lt;/h3&gt;</source>
        <translation>&lt;h3&gt;搜索引擎设置&lt;/h3&gt;</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="159"/>
        <source>Please select search engine you preferred.</source>
        <translation>设置互联网搜索引擎</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="172"/>
        <source>baidu</source>
        <translation>百度</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="174"/>
        <source>sougou</source>
        <translation>搜狗</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="176"/>
        <source>360</source>
        <translation>360</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="299"/>
        <source>Whether to delete this directory?</source>
        <translation>是否要删除此目录?</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="300"/>
        <source>Yes</source>
        <translation>是(Y)</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="301"/>
        <source>No</source>
        <translation>否(N)</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="359"/>
        <source>Creating ...</source>
        <translation>正在索引...</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="362"/>
        <source>Done</source>
        <translation>索引完成</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="370"/>
        <source>Index Entry: %1</source>
        <translation>索引项: %1</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="416"/>
        <source>Directories</source>
        <translation>文件夹</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="417"/>
        <source>select blocked folder</source>
        <translation>选择屏蔽文件夹</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="418"/>
        <source>Select</source>
        <translation>选择</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="419"/>
        <source>Position: </source>
        <translation>位置：</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="420"/>
        <source>FileName: </source>
        <translation>名称：</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="421"/>
        <source>FileType: </source>
        <translation>类型：</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="422"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="487"/>
        <source>Choosen path is Empty!</source>
        <translation>选择的路径不存在！</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="491"/>
        <source>Choosen path is not in &quot;home&quot;!</source>
        <translation>请选择家目录下的文件夹！</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="495"/>
        <source>Its&apos; parent folder has been blocked!</source>
        <translation>父文件夹已被屏蔽！</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="499"/>
        <source>Set blocked folder failed!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="504"/>
        <source>OK</source>
        <translation>好的</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::ShowMoreLabel</name>
    <message>
        <source>Show More...</source>
        <translation type="vanished">显示更多...</translation>
    </message>
    <message>
        <source>Retract</source>
        <translation type="vanished">收起</translation>
    </message>
    <message>
        <source>Loading</source>
        <translation type="vanished">加载中</translation>
    </message>
    <message>
        <source>Loading.</source>
        <translation type="vanished">加载中.</translation>
    </message>
    <message>
        <source>Loading..</source>
        <translation type="vanished">加载中..</translation>
    </message>
    <message>
        <source>Loading...</source>
        <translation type="vanished">加载中...</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::UkuiSearchGui</name>
    <message>
        <location filename="../../frontend/ukui-search-gui.cpp" line="88"/>
        <source>Quit ukui-search application</source>
        <translation>退出搜索应用</translation>
    </message>
    <message>
        <location filename="../../frontend/ukui-search-gui.cpp" line="91"/>
        <source>Show main window</source>
        <translation>显示主页面</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::WebSearchWidget</name>
    <message>
        <location filename="../../frontend/view/web-search-view.cpp" line="152"/>
        <source>Web Page</source>
        <translation>网页搜索</translation>
    </message>
</context>
</TS>
