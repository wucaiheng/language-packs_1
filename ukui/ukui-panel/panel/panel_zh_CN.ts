<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>UKUIPanel</name>
    <message>
        <location filename="../ukuipanel.cpp" line="837"/>
        <source>Small</source>
        <translation>小尺寸</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="839"/>
        <source>Medium</source>
        <translation>中尺寸</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="841"/>
        <source>Large</source>
        <translation>大尺寸</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="846"/>
        <source>Adjustment Size</source>
        <translation>调整大小</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="882"/>
        <source>Up</source>
        <translation>上</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="884"/>
        <source>Bottom</source>
        <translation>下</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="886"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="888"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="891"/>
        <source>Adjustment Position</source>
        <translation>调整位置</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="917"/>
        <source>Hide Panel</source>
        <translation>隐藏任务栏</translation>
    </message>
    <message>
        <source>Set up Panel</source>
        <translation type="vanished">设置任务栏</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1246"/>
        <source>Show Taskview</source>
        <translation>显示任务视图按钮</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1263"/>
        <source>Show Nightmode</source>
        <translation>显示夜间模式按钮</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1269"/>
        <source>Show Desktop</source>
        <translation>显示桌面</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1276"/>
        <source>Show System Monitor</source>
        <translation>系统监视器</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1301"/>
        <source>Lock This Panel</source>
        <translation>锁定任务栏</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1316"/>
        <source>About Kylin</source>
        <translation>关于麒麟</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="79"/>
        <source>Use alternate configuration file.</source>
        <translation>使用备用配置文件。</translation>
    </message>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="80"/>
        <source>Configuration file</source>
        <translation>配置文件</translation>
    </message>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="85"/>
        <source>ukui-panel set mode </source>
        <translation>任务栏设置模式 </translation>
    </message>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="86"/>
        <source>panel set option</source>
        <translation>面板设置选项</translation>
    </message>
</context>
</TS>
