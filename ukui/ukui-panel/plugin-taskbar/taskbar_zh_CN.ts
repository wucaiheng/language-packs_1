<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>UKUITaskBar</name>
    <message>
        <location filename="../ukuitaskbar.cpp" line="1082"/>
        <source>Drop Error</source>
        <translation>掉线错误</translation>
    </message>
    <message>
        <location filename="../ukuitaskbar.cpp" line="1083"/>
        <source>File/URL &apos;%1&apos; cannot be embedded into QuickLaunch for now</source>
        <translation>显示在window.File/URL &apos;%1&apos;上，暂时不能嵌入QuickLaunch中。</translation>
    </message>
</context>
<context>
    <name>UKUITaskButton</name>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="581"/>
        <source>Application</source>
        <translation>应用</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="614"/>
        <source>To &amp;Desktop</source>
        <translation>到桌面</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="616"/>
        <source>&amp;All Desktops</source>
        <translation>全部桌面</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="623"/>
        <source>Desktop &amp;%1</source>
        <translation>桌面%1</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="630"/>
        <source>&amp;To Current Desktop</source>
        <translation>至当前桌面</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="638"/>
        <source>&amp;Move</source>
        <translation>移动</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="641"/>
        <source>Resi&amp;ze</source>
        <translation>调整大小</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="648"/>
        <source>Ma&amp;ximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="654"/>
        <source>Maximize vertically</source>
        <translation>纵向最大化</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="659"/>
        <source>Maximize horizontally</source>
        <translation>横向最大化</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="665"/>
        <source>&amp;Restore</source>
        <translation>还原</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="669"/>
        <source>Mi&amp;nimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="674"/>
        <source>Roll down</source>
        <translation>往下</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="678"/>
        <source>Roll up</source>
        <translation>往上</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="686"/>
        <source>&amp;Layer</source>
        <translation>层数</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="688"/>
        <source>Always on &amp;top</source>
        <translation>始终在顶部</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="694"/>
        <source>&amp;Normal</source>
        <translation>默认</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="700"/>
        <source>Always on &amp;bottom</source>
        <translation>始终在底部</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="708"/>
        <source>&amp;Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="868"/>
        <source>delete from quicklaunch</source>
        <translation>从任务栏取消固定</translation>
    </message>
</context>
<context>
    <name>UKUITaskGroup</name>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="254"/>
        <source>Group</source>
        <translation>组</translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="261"/>
        <source>delete from taskbar</source>
        <translation>从任务栏取消固定</translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="263"/>
        <source>add to taskbar</source>
        <translation>添加到任务栏</translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="271"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>UKUITaskWidget</name>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="443"/>
        <source>Widget</source>
        <translation>小组件</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="446"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="447"/>
        <source>restore</source>
        <translation>恢复</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="449"/>
        <source>maximaze</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="452"/>
        <source>minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="453"/>
        <source>above</source>
        <translation>置顶</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="454"/>
        <source>clear</source>
        <translation>取消置顶</translation>
    </message>
</context>
</TS>
