<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>NightModeButton</name>
    <message>
        <source>Turn On NightMode</source>
        <translation type="vanished">夜间模式</translation>
    </message>
    <message>
        <source>Set Up NightMode</source>
        <translation type="vanished">设置夜间模式</translation>
    </message>
    <message>
        <location filename="../nightmode.cpp" line="189"/>
        <source>nightmode opened</source>
        <translation>ᠰᠦᠨᠢ᠎ᠶᠢᠨ ᠵᠠᠭᠪᠤᠷ᠎ᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../nightmode.cpp" line="195"/>
        <source>nightmode closed</source>
        <translation>ᠰᠦᠨᠢ᠎ᠶᠢᠨ ᠵᠠᠭᠪᠤᠷ᠎ᠢ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../nightmode.cpp" line="217"/>
        <source>night mode open</source>
        <translation>ᠰᠦᠨᠢ᠎ᠶᠢᠨ ᠵᠠᠭᠪᠤᠷ᠎ᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../nightmode.cpp" line="134"/>
        <location filename="../nightmode.cpp" line="223"/>
        <source>night mode close</source>
        <translation>ᠰᠦᠨᠢ᠎ᠶᠢᠨ ᠵᠠᠭᠪᠤᠷ᠎ᠢ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
</TS>
