<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>NightModeButton</name>
    <message>
        <location filename="../nightmode.cpp" line="134"/>
        <location filename="../nightmode.cpp" line="223"/>
        <source>night mode close</source>
        <translation>མཚན་མོའི་རྣམ་པ་ཁ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../nightmode.cpp" line="189"/>
        <source>nightmode opened</source>
        <translation>མཚན་མོའི་རྣམ་པ་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../nightmode.cpp" line="195"/>
        <source>nightmode closed</source>
        <translation>མཚན་མོའི་རྣམ་པ་ཁ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../nightmode.cpp" line="217"/>
        <source>night mode open</source>
        <translation>མཚན་མོའི་རྣམ་པ་ཁ་ཕྱེ་བ།</translation>
    </message>
</context>
</TS>
