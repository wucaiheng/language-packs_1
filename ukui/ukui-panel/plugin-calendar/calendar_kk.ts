<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>CalendarActiveLabel</name>
    <message>
        <source>Time and Date</source>
        <translation type="vanished">时间与日期</translation>
    </message>
    <message>
        <source>Time and Date Setting</source>
        <translation type="vanished">时间日期设置</translation>
    </message>
    <message>
        <source>Config panel</source>
        <translation type="vanished">设置任务栏</translation>
    </message>
</context>
<context>
    <name>CalendarButton</name>
    <message>
        <location filename="../calendarbutton.cpp" line="72"/>
        <source>Time and Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../calendarbutton.cpp" line="128"/>
        <source>Time and Date Setting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LunarCalendarItem</name>
    <message>
        <location filename="../lunarcalendarwidget/lunarcalendaritem.cpp" line="655"/>
        <source>消防宣传日</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/lunarcalendaritem.cpp" line="658"/>
        <source>志愿者服务日</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/lunarcalendaritem.cpp" line="661"/>
        <source>全国爱眼日</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/lunarcalendaritem.cpp" line="664"/>
        <source>抗战纪念日</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LunarCalendarWidget</name>
    <message>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="349"/>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="350"/>
        <source>Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="361"/>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="362"/>
        <source>Month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="374"/>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="375"/>
        <source>Today</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="604"/>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="618"/>
        <source>Sun</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="605"/>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="612"/>
        <source>Mon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="606"/>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="613"/>
        <source>Tue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="607"/>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="614"/>
        <source>Wed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="608"/>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="615"/>
        <source>Thur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="609"/>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="616"/>
        <source>Fri</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="610"/>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="617"/>
        <source>Sat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="693"/>
        <location filename="../lunarcalendarwidget/lunarcalendarwidget.cpp" line="957"/>
        <source>解析json文件错误！</source>
        <translation></translation>
    </message>
    <message>
        <source>Sunday</source>
        <translation type="obsolete">周日</translation>
    </message>
    <message>
        <source>Monday</source>
        <translation type="obsolete">周一</translation>
    </message>
    <message>
        <source>Tuesday</source>
        <translation type="obsolete">周二</translation>
    </message>
    <message>
        <source>Wednesday</source>
        <translation type="obsolete">周三</translation>
    </message>
    <message>
        <source>Thursday</source>
        <translation type="obsolete">周四</translation>
    </message>
    <message>
        <source>Friday</source>
        <translation type="obsolete">周五</translation>
    </message>
    <message>
        <source>Saturday</source>
        <translation type="obsolete">周六</translation>
    </message>
</context>
<context>
    <name>UkuiWebviewDialog</name>
    <message>
        <location filename="../ukuiwebviewdialog.ui" line="13"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>frmLunarCalendarWidget</name>
    <message>
        <location filename="../lunarcalendarwidget/frmlunarcalendarwidget.ui" line="14"/>
        <location filename="../lunarcalendarwidget/ui_frmlunarcalendarwidget.h" line="144"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/frmlunarcalendarwidget.ui" line="45"/>
        <source>整体样式</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/frmlunarcalendarwidget.ui" line="59"/>
        <source>红色风格</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/frmlunarcalendarwidget.ui" line="67"/>
        <source>选中样式</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/frmlunarcalendarwidget.ui" line="81"/>
        <source>矩形背景</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/frmlunarcalendarwidget.ui" line="86"/>
        <source>圆形背景</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/frmlunarcalendarwidget.ui" line="91"/>
        <source>角标背景</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/frmlunarcalendarwidget.ui" line="96"/>
        <source>图片背景</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/frmlunarcalendarwidget.ui" line="104"/>
        <source>星期格式</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/frmlunarcalendarwidget.ui" line="118"/>
        <source>短名称</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/frmlunarcalendarwidget.ui" line="123"/>
        <source>普通名称</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/frmlunarcalendarwidget.ui" line="128"/>
        <source>长名称</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/frmlunarcalendarwidget.ui" line="133"/>
        <source>英文名称</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lunarcalendarwidget/frmlunarcalendarwidget.ui" line="141"/>
        <source>显示农历</source>
        <translation></translation>
    </message>
</context>
</TS>
