<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>StartMenuButton</name>
    <message>
        <location filename="../startmenu_button.cpp" line="29"/>
        <source>UKUI Menu</source>
        <translation>འགོ་རྩོམ་འདེམས་བྱང་།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="69"/>
        <source>User Action</source>
        <translation>སྤྱོད་མཁན་གྱི་ཉེར་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="70"/>
        <source>Sleep or Hibernate</source>
        <translation>ངལ་གསོ་དང་གཉིད་པ།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="71"/>
        <source>Power Supply</source>
        <translation>གློག་ཁུངས།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="77"/>
        <source>Lock Screen</source>
        <translation>འཆར་ངོས་གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="82"/>
        <source>Switch User</source>
        <translation>སྤྱོད་མཁན་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="87"/>
        <source>Logout</source>
        <translation>ཐོ་ཁོངས་ནས་འདོར་བ།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="93"/>
        <source>Hibernate Mode</source>
        <translation>མལ་གསོ།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="100"/>
        <source>Sleep Mode</source>
        <translation>མལ་གསོ་བ་།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="105"/>
        <source>Restart</source>
        <translation>བསྐྱར་སློང་།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="111"/>
        <source>TimeShutdown</source>
        <translation>དུས་ཆད་སྒོ་རྒྱག་སྒྲིག་འགོད།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="115"/>
        <source>Power Off</source>
        <translation>འཁོར་ཁ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>TaskViewButton</name>
    <message>
        <location filename="../taskview_button.cpp" line="38"/>
        <source>Show Taskview</source>
        <translation>ལས་འགན་མཐོང་སྣེ་འཆར་བའི་གནོན་མཐེབ།</translation>
    </message>
</context>
</TS>
