<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>StartMenuButton</name>
    <message>
        <source>UKui Menu</source>
        <translation type="vanished">开始菜单</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="29"/>
        <source>UKUI Menu</source>
        <translation>ᠡᠬᠢᠯᠡᠯᠳᠡ᠎ᠶᠢᠨ ᠲᠤᠪᠶᠤᠭ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="69"/>
        <source>User Action</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶᠢᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="70"/>
        <source>Sleep or Hibernate</source>
        <translation>ᠢᠴᠡᠬᠡᠯᠡᠬᠦ᠌ ᠪᠤᠶᠤ ᠤᠨᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="71"/>
        <source>Power Supply</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠡᠬᠦᠰᠭᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="77"/>
        <source>Lock Screen</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="82"/>
        <source>Switch User</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="87"/>
        <source>Logout</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="93"/>
        <source>Hibernate Mode</source>
        <translation>ᠢᠴᠡᠭᠡᠯᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="100"/>
        <source>Sleep Mode</source>
        <translation>ᠤᠨᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="105"/>
        <source>Restart</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="111"/>
        <source>TimeShutdown</source>
        <translation>ᠳᠤᠭᠳᠠᠭᠰᠠᠨ ᠴᠠᠭ᠎ᠲᠤ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="115"/>
        <source>Power Off</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>TaskViewButton</name>
    <message>
        <location filename="../taskview_button.cpp" line="38"/>
        <source>Show Taskview</source>
        <translation>ᠡᠬᠦᠷᠭᠡ ᠵᠢᠨ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
</context>
</TS>
