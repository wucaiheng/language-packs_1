<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>StartMenuButton</name>
    <message>
        <source>UKui Menu</source>
        <translation type="vanished">开始菜单</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="29"/>
        <source>UKUI Menu</source>
        <translation>开始菜单</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="69"/>
        <source>User Action</source>
        <translation>用户操作</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="70"/>
        <source>Sleep or Hibernate</source>
        <translation>休眠或睡眠</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="71"/>
        <source>Power Supply</source>
        <translation>电源</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="77"/>
        <source>Lock Screen</source>
        <translation>锁定屏幕</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="82"/>
        <source>Switch User</source>
        <translation>切换用户</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="87"/>
        <source>Logout</source>
        <translation>注销</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="93"/>
        <source>Hibernate Mode</source>
        <translation>休眠</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="100"/>
        <source>Sleep Mode</source>
        <translation>睡眠</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="105"/>
        <source>Restart</source>
        <translation>重启</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="111"/>
        <source>TimeShutdown</source>
        <translation>定时关机</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="115"/>
        <source>Power Off</source>
        <translation>关机</translation>
    </message>
</context>
<context>
    <name>TaskViewButton</name>
    <message>
        <location filename="../taskview_button.cpp" line="38"/>
        <source>Show Taskview</source>
        <translation>显示任务视图</translation>
    </message>
</context>
</TS>
