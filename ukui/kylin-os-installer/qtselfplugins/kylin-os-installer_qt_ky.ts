<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="../src/gui/kernel/qapplication.cpp" line="+2316"/>
        <source>Services</source>
        <translation>Кызматтар</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide %1</source>
        <translation>%1 жашыруу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide Others</source>
        <translation>Башкаларды жашыруу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show All</source>
        <translation>Бардыгын көрсөтүү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Preferences...</source>
        <translation>Ырастоолор...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit %1</source>
        <translation>%1 чыгуу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>About %1</source>
        <translation>Жөнүндө %1</translation>
    </message>
</context>
<context>
    <name>AudioOutput</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/audiooutput.cpp" line="+375"/>
        <source>&lt;html&gt;The audio playback device &lt;b&gt;%1&lt;/b&gt; does not work.&lt;br/&gt;Falling back to &lt;b&gt;%2&lt;/b&gt;.&lt;/html&gt;</source>
        <translation>&lt;html&gt; &lt;b&gt;%1&lt;/b&gt; аудио ойнотуу аспагы иштебейт. &lt;br/&gt; Кайра түшүп &lt;b&gt;%2&lt;/b&gt;. &lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;Switching to the audio playback device &lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;which just became available and has higher preference.&lt;/html&gt;</source>
        <translation>&lt;html&gt; Жаңы эле жеткиликтүү болгон жана артыкчылыгы жогору болгон &lt;b&gt;%1&lt;/b&gt;&lt;br/&gt; аудио ойнотуу аспагына өтүү. &lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Revert back to device &apos;%1&apos;</source>
        <translation>«%1» түзмөккө кайтуу</translation>
    </message>
</context>
<context>
    <name>CloseButton</name>
    <message>
        <location filename="../src/gui/widgets/qtabbar.cpp" line="+2251"/>
        <source>Close Tab</source>
        <translation>Таблицаны жабуу</translation>
    </message>
</context>
<context>
    <name>Phonon::</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/phononnamespace.cpp" line="+55"/>
        <source>Notifications</source>
        <translation>Эскертүүлөр</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Music</source>
        <translation>Музыка</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Video</source>
        <translation>Видео</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Communication</source>
        <translation>Байланыш</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Games</source>
        <translation>Оюндар</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Accessibility</source>
        <translation>Жеткиликтүүлүк</translation>
    </message>
</context>
<context>
    <name>Phonon::Gstreamer::Backend</name>
    <message>
        <location filename="../src/3rdparty/phonon/gstreamer/backend.cpp" line="+171"/>
        <source>Warning: You do not seem to have the package gstreamer0.10-plugins-good installed.
          Some video features have been disabled.</source>
        <translation>Эскертүү: Сиз топтом gstreamer0.10-плагиндер-жакшы орнотулган жок көрүнөт.
          Кээ бир видео өзгөчөлүктөрү өчүрүлгөн.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Warning: You do not seem to have the base GStreamer plugins installed.
          All audio and video support has been disabled</source>
        <translation>Эскертүү: Сиз базалык GStreamer плагиндер орнотулган жок көрүнөт.
          Бардык аудио жана видео колдоо өчүрүлдү</translation>
    </message>
</context>
<context>
    <name>Phonon::Gstreamer::MediaObject</name>
    <message>
        <location filename="../src/3rdparty/phonon/gstreamer/mediaobject.cpp" line="+90"/>
        <source>Cannot start playback. 

Check your Gstreamer installation and make sure you 
have libgstreamer-plugins-base installed.</source>
        <translation>Ойнотууну баштоо мүмкүн эмес. 

Gstreamer орнотууңузду текшерип, текшерип көргүлө 
либгстреамер-плагиндер-база орнотулган бар.</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>A required codec is missing. You need to install the following codec(s) to play this content: %0</source>
        <translation>Талап кылынган код жок. Бул мазмунду ойнотуу үчүн төмөнкү кодду орнотуу керек: %0</translation>
    </message>
    <message>
        <location line="+676"/>
        <location line="+8"/>
        <location line="+15"/>
        <location line="+9"/>
        <location line="+6"/>
        <location line="+19"/>
        <location line="+335"/>
        <location line="+24"/>
        <source>Could not open media source.</source>
        <translation>Медиа-булак ачууга болбоду.</translation>
    </message>
    <message>
        <location line="-403"/>
        <source>Invalid source type.</source>
        <translation>Жараксыз булак түрү.</translation>
    </message>
    <message>
        <location line="+377"/>
        <source>Could not locate media source.</source>
        <translation>Медиа булагын табууга болбоду.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not open audio device. The device is already in use.</source>
        <translation>Аудио орнотмо ачууга болбоду. Аппарат буга чейин колдонулуп жатат.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Could not decode media source.</source>
        <translation>Медиа булагын коддоо мүмкүн эмес.</translation>
    </message>
</context>
<context>
    <name>Phonon::VolumeSlider</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/volumeslider.cpp" line="+42"/>
        <location line="+18"/>
        <source>Volume: %1%</source>
        <translation>Көлөмү: %1%</translation>
    </message>
    <message>
        <location line="-15"/>
        <location line="+18"/>
        <location line="+54"/>
        <source>Use this slider to adjust the volume. The leftmost position is 0%, the rightmost is %1%</source>
        <translation>Көлөмүн жөнгө салуу үчүн бул слайдер колдонуңуз. сол позициясы 0%, оңго %1% болуп саналат</translation>
    </message>
</context>
<context>
    <name>Q3Accel</name>
    <message>
        <location filename="../src/qt3support/other/q3accel.cpp" line="+481"/>
        <source>%1, %2 not defined</source>
        <translation>%1, %2 аныкталган жок</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Ambiguous %1 not handled</source>
        <translation>%1 иштетилбейт</translation>
    </message>
</context>
<context>
    <name>Q3DataTable</name>
    <message>
        <location filename="../src/qt3support/sql/q3datatable.cpp" line="+285"/>
        <source>True</source>
        <translation>Туура</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>False</source>
        <translation>Жалган</translation>
    </message>
    <message>
        <location line="+505"/>
        <source>Insert</source>
        <translation>Инерт</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Update</source>
        <translation>Жаңылоо</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>Жоготуу</translation>
    </message>
</context>
<context>
    <name>Q3FileDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="+864"/>
        <source>Copy or Move a File</source>
        <translation>Файлды көчүрүү же көчүрүү</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Read: %1</source>
        <translation>Оку: %1</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+30"/>
        <source>Write: %1</source>
        <translation>Жазуу: %1</translation>
    </message>
    <message>
        <location line="-22"/>
        <location line="+1575"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location line="-157"/>
        <location line="+49"/>
        <location line="+2153"/>
        <location filename="../src/qt3support/dialogs/q3filedialog_mac.cpp" line="+110"/>
        <source>All Files (*)</source>
        <translation>Бардык файлдар (*)</translation>
    </message>
    <message>
        <location line="-2089"/>
        <source>Name</source>
        <translation>Аты-жөнү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Size</source>
        <translation>Көлөмү</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <translation>Түрү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>Датасы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Attributes</source>
        <translation>Атрибуттар</translation>
    </message>
    <message>
        <location line="+35"/>
        <location line="+2031"/>
        <source>&amp;OK</source>
        <translation>&amp;amp, жарайды</translation>
    </message>
    <message>
        <location line="-1991"/>
        <source>Look &amp;in:</source>
        <translation>Мына &amp;amp:</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1981"/>
        <location line="+16"/>
        <source>File &amp;name:</source>
        <translation>Файл &amp; аты:</translation>
    </message>
    <message>
        <location line="-1996"/>
        <source>File &amp;type:</source>
        <translation>Файл &amp; түрү:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Back</source>
        <translation>Артка</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>One directory up</source>
        <translation>Бир каталог жогору</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Create New Folder</source>
        <translation>Жаңы папка түзүү</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>List View</source>
        <translation>Тизменин көрүнүшү</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Detail View</source>
        <translation>Майда - чүйдөсүнө чейин көрүү</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Preview File Info</source>
        <translation>Файлды алдын ала көрүү</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Preview File Contents</source>
        <translation>Файлдын мазмунун алдын ала көрүү</translation>
    </message>
    <message>
        <location line="+88"/>
        <source>Read-write</source>
        <translation>Окуу-жазуу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Read-only</source>
        <translation>Окуу-гана</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Write-only</source>
        <translation>Жазуу-гана</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Inaccessible</source>
        <translation>Жеткиликтүү эмес</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Symlink to File</source>
        <translation>Файлга симлинк</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Symlink to Directory</source>
        <translation>Каталогго симлинк</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Symlink to Special</source>
        <translation>Симлинк атайын</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dir</source>
        <translation>Дир</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Special</source>
        <translation>Өзгөчө</translation>
    </message>
    <message>
        <location line="+704"/>
        <location line="+2100"/>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="+337"/>
        <source>Open</source>
        <translation>Ачуу</translation>
    </message>
    <message>
        <location line="-1990"/>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="+84"/>
        <source>Save As</source>
        <translation>Куткаруу</translation>
    </message>
    <message>
        <location line="+642"/>
        <location line="+5"/>
        <location line="+355"/>
        <source>&amp;Open</source>
        <translation>&amp;amp, ачык</translation>
    </message>
    <message>
        <location line="-357"/>
        <location line="+341"/>
        <source>&amp;Save</source>
        <translation>&amp;amp, сактоо</translation>
    </message>
    <message>
        <location line="-334"/>
        <source>&amp;Rename</source>
        <translation>&amp;amp, атын алмаштыруу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Delete</source>
        <translation>Жоготуу</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>R&amp;eload</source>
        <translation>R &amp; eload</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Sort by &amp;Name</source>
        <translation>Аты-жөнү боюнча сорттоо</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sort by &amp;Size</source>
        <translation>Өлчөм боюнча сорттоо</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sort by &amp;Date</source>
        <translation>Дата боюнча сорттоо</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Unsorted</source>
        <translation>&amp;amp, түйшүксүз</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Sort</source>
        <translation>Сорттоо</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Show &amp;hidden files</source>
        <translation>Көрсөтүү &amp; жашыруун файлдар</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>the file</source>
        <translation>файл</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>the directory</source>
        <translation>каталог</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>the symlink</source>
        <translation>симлинк</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete %1</source>
        <translation>%1 жоготуу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;qt&gt;Are you sure you wish to delete %1 &quot;%2&quot;?&lt;/qt&gt;</source>
        <translation>&lt;qt&gt; %1 &quot;%2&quot; жоготууну каалайсызбы? &lt;/qt&gt;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Yes</source>
        <translation>Ооба</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;No</source>
        <translation>&amp;amp, жок</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>New Folder 1</source>
        <translation>Жаңы папка 1</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New Folder</source>
        <translation>Жаңы папка</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New Folder %1</source>
        <translation>%1 жаңы папкасы</translation>
    </message>
    <message>
        <location line="+98"/>
        <source>Find Directory</source>
        <translation>Каталогду табуу</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+108"/>
        <source>Directories</source>
        <translation>Каталогдор</translation>
    </message>
    <message>
        <location line="-2"/>
        <source>Directory:</source>
        <translation>Каталог:</translation>
    </message>
    <message>
        <location line="+40"/>
        <location line="+1110"/>
        <source>Error</source>
        <translation>Ката</translation>
    </message>
    <message>
        <location line="-1109"/>
        <source>%1
File not found.
Check path and filename.</source>
        <translation>%1
Файл табылган жок.
Жолду жана файлдын атын текшерүү.</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="-289"/>
        <source>All Files (*.*)</source>
        <translation>Бардык файлдар (*.*)</translation>
    </message>
    <message>
        <location line="+375"/>
        <source>Open </source>
        <translation>Ачуу </translation>
    </message>
    <message>
        <location line="+155"/>
        <source>Select a Directory</source>
        <translation>Каталогду тандоо</translation>
    </message>
</context>
<context>
    <name>Q3LocalFs</name>
    <message>
        <location filename="../src/qt3support/network/q3localfs.cpp" line="+130"/>
        <location line="+10"/>
        <source>Could not read directory
%1</source>
        <translation>Каталогду окууга болбоду
%1</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Could not create directory
%1</source>
        <translation>Каталогду түзүүгө болбоду
%1</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Could not remove file or directory
%1</source>
        <translation>Файлды же каталогду жоготууга болбоду
%1</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Could not rename
%1
to
%2</source>
        <translation>Атын алмаштыруу мүмкүн эмес
%1
%2</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Could not open
%1</source>
        <translation>Ачууга болбоду
%1</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Could not write
%1</source>
        <translation>Жазууга болбоду
%1</translation>
    </message>
</context>
<context>
    <name>Q3MainWindow</name>
    <message>
        <location filename="../src/qt3support/widgets/q3mainwindow.cpp" line="+2051"/>
        <source>Line up</source>
        <translation>Сап</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Customize...</source>
        <translation>Жекелештирүү...</translation>
    </message>
</context>
<context>
    <name>Q3NetworkProtocol</name>
    <message>
        <location filename="../src/qt3support/network/q3networkprotocol.cpp" line="+854"/>
        <source>Operation stopped by the user</source>
        <translation>Операцияны колдонуучу токтотту</translation>
    </message>
</context>
<context>
    <name>Q3ProgressDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3progressdialog.cpp" line="+224"/>
        <location line="+61"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
</context>
<context>
    <name>Q3TabDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3tabdialog.cpp" line="+189"/>
        <location line="+814"/>
        <source>OK</source>
        <translation>Макул</translation>
    </message>
    <message>
        <location line="-356"/>
        <source>Apply</source>
        <translation>Колдонуу</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Help</source>
        <translation>Жардам</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Defaults</source>
        <translation>Дефолт</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
</context>
<context>
    <name>Q3TextEdit</name>
    <message>
        <location filename="../src/qt3support/text/q3textedit.cpp" line="+5429"/>
        <source>&amp;Undo</source>
        <translation>&amp;amp, Ундо</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Redo</source>
        <translation>&amp;amp, Редо</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cu&amp;t</source>
        <translation>Ку&amp;т</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Copy</source>
        <translation>Көчүрмө</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Paste</source>
        <translation>&amp;amp, паста</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation>Тазалоо</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+2"/>
        <source>Select All</source>
        <translation>Бардыгын тандоо</translation>
    </message>
</context>
<context>
    <name>Q3TitleBar</name>
    <message>
        <location filename="../src/plugins/accessible/compat/q3complexwidgets.cpp" line="+246"/>
        <source>System</source>
        <translation>Система</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restore up</source>
        <translation>Калыбына келтирүү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Minimize</source>
        <translation>Минималдуу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restore down</source>
        <translation>Калыбына келтирүү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Maximize</source>
        <translation>Максималдуу</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Contains commands to manipulate the window</source>
        <translation>Терезени манипуляциялоо үчүн командалар камтылат</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Puts a minimized back to normal</source>
        <translation>Минималдуу кайра нормалдуу коёт</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Moves the window out of the way</source>
        <translation>Терезени жолдон чыгарып жылдырат</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Puts a maximized window back to normal</source>
        <translation>Максималдуу терезени кайрадан нормалдуу абалга коёт</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Makes the window full screen</source>
        <translation>Терезени толук экранга чыгарат</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Closes the window</source>
        <translation>Терезени жабуу</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Displays the name of the window and contains controls to manipulate it</source>
        <translation>Терезенин атын чагылдырат жана аны манипуляциялоо үчүн башкаруучуларды камтыйт</translation>
    </message>
</context>
<context>
    <name>Q3ToolBar</name>
    <message>
        <location filename="../src/qt3support/widgets/q3toolbar.cpp" line="+692"/>
        <source>More...</source>
        <translation>Кененирээк...</translation>
    </message>
</context>
<context>
    <name>Q3UrlOperator</name>
    <message>
        <location filename="../src/qt3support/network/q3urloperator.cpp" line="+386"/>
        <location line="+260"/>
        <location line="+4"/>
        <source>The protocol `%1&apos; is not supported</source>
        <translation>&quot;%1&quot; протоколу колдолбойт</translation>
    </message>
    <message>
        <location line="-260"/>
        <source>The protocol `%1&apos; does not support listing directories</source>
        <translation>«%1» протоколу листинг каталогдорун колдобойт</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support creating new directories</source>
        <translation>«%1» протоколу жаңы каталогдор түзүүнү колдобойт</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support removing files or directories</source>
        <translation>&quot;%1&quot; протоколу файлдарды же каталогдорду алып салууну колдобойт</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support renaming files or directories</source>
        <translation>«%1» протоколу файлдардын же каталогдордун атын өзгөртүүнү колдобойт</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support getting files</source>
        <translation>«%1» протоколу файлдарды алууда колдолбойт</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support putting files</source>
        <translation>«%1» протоколу файлдарды коюуну колдобойт</translation>
    </message>
    <message>
        <location line="+243"/>
        <location line="+4"/>
        <source>The protocol `%1&apos; does not support copying or moving files or directories</source>
        <translation>«%1» протоколу файлдарды же каталогдорду көчүрүүнү же көчүрүүнү колдобойт</translation>
    </message>
    <message>
        <location line="+237"/>
        <location line="+1"/>
        <source>(unknown)</source>
        <translation>(белгисиз)</translation>
    </message>
</context>
<context>
    <name>Q3Wizard</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3wizard.cpp" line="+177"/>
        <source>&amp;Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt; &amp;Back</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Next &gt;</source>
        <translation>&amp;amp, кийинки &gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Finish</source>
        <translation>Бүтүрүү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Help</source>
        <translation>Жардам</translation>
    </message>
</context>
<context>
    <name>QAbstractSocket</name>
    <message>
        <location filename="../src/network/socket/qabstractsocket.cpp" line="+868"/>
        <location filename="../src/network/socket/qhttpsocketengine.cpp" line="+615"/>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="+657"/>
        <location line="+26"/>
        <source>Host not found</source>
        <translation>Хост табылган жок</translation>
    </message>
    <message>
        <location line="+50"/>
        <location filename="../src/network/socket/qhttpsocketengine.cpp" line="+3"/>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="+4"/>
        <source>Connection refused</source>
        <translation>Туташуудан баш тартты</translation>
    </message>
    <message>
        <location line="+141"/>
        <source>Connection timed out</source>
        <translation>Туташуу убакыттын өтүшү менен</translation>
    </message>
    <message>
        <location line="-547"/>
        <location line="+787"/>
        <location line="+208"/>
        <source>Operation on socket is not supported</source>
        <translation>Розетка боюнча операция колдолбойт</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Socket operation timed out</source>
        <translation>Сокет операциясы убакыттын өтүшү менен</translation>
    </message>
    <message>
        <location line="+380"/>
        <source>Socket is not connected</source>
        <translation>Сокет туташпайт</translation>
    </message>
    <message>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="-8"/>
        <source>Network unreachable</source>
        <translation>Тармак жеткиликтүү эмес</translation>
    </message>
</context>
<context>
    <name>QAbstractSpinBox</name>
    <message>
        <location filename="../src/gui/widgets/qabstractspinbox.cpp" line="+1199"/>
        <source>&amp;Step up</source>
        <translation>Жогорулатуу</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Step &amp;down</source>
        <translation>Кадам &amp;amp, төмөндөө</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>&amp;Select All</source>
        <translation>Бардыгын тандап ал</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/gui/accessible/qaccessibleobject.cpp" line="+376"/>
        <source>Activate</source>
        <translation>Активдештирүү</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.h" line="+352"/>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation>Аткарыла турган &apos;%1&apos; Qt %2 талап кылынат, Qt %3 табылган.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Incompatible Qt Library Error</source>
        <translation>Qt китепканасынын шайкеш келбеген катасы</translation>
    </message>
    <message>
        <location filename="../src/gui/accessible/qaccessibleobject.cpp" line="+2"/>
        <source>Activates the program&apos;s main window</source>
        <translation>Программанын негизги терезесин активдештирүү</translation>
    </message>
</context>
<context>
    <name>QAxSelect</name>
    <message>
        <location filename="../src/activeqt/container/qaxselect.ui"/>
        <source>Select ActiveX Control</source>
        <translation>ActiveX башкарууну тандоо</translation>
    </message>
    <message>
        <location/>
        <source>OK</source>
        <translation>Макул</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location/>
        <source>COM &amp;Object:</source>
        <translation>COM &amp;amp:</translation>
    </message>
</context>
<context>
    <name>QCheckBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="+114"/>
        <source>Uncheck</source>
        <translation>Текшерүүсүз</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Check</source>
        <translation>Текшерүү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Toggle</source>
        <translation>Тогле</translation>
    </message>
</context>
<context>
    <name>QColorDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="+1253"/>
        <source>Hu&amp;e:</source>
        <translation>Ху &amp; е:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Sat:</source>
        <translation>&amp;amp:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Val:</source>
        <translation>&amp;amp:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Red:</source>
        <translation>&amp;amp:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Green:</source>
        <translation>&amp;amp, Жашыл:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bl&amp;ue:</source>
        <translation>Бл &amp;amp:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A&amp;lpha channel:</source>
        <translation>А &amp;amp, лфа каналы:</translation>
    </message>
    <message>
        <location line="+101"/>
        <source>Select Color</source>
        <translation>Түс тандоо</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>&amp;Basic colors</source>
        <translation>Негизги түстөр</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Custom colors</source>
        <translation>&amp;amp, түстөр</translation>
    </message>
    <message>
        <source>Cursor at %1, %2
Press ESC to cancel</source>
        <translation>%1, %2 боюнча курсор
ЭСКны басуу жокко чыгарат</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Add to Custom Colors</source>
        <translation>&amp;amp, өзгөчө түстөргө кошуу</translation>
    </message>
    <message>
        <source>Select color</source>
        <translation type="obsolete">选择颜色</translation>
    </message>
    <message>
        <source>&amp;Pick Screen Color</source>
        <translation>Экрандын түсүн тандоо</translation>
    </message>
</context>
<context>
    <name>QComboBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="+1771"/>
        <location line="+65"/>
        <source>Open</source>
        <translation>Ачуу</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qitemeditorfactory.cpp" line="+544"/>
        <source>False</source>
        <translation>Жалган</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>True</source>
        <translation>Туура</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="+0"/>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <source>%1: permission denied</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：权限被拒绝</translation>
    </message>
    <message>
        <source>%1: already exists</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：已经存在</translation>
    </message>
    <message>
        <source>%1: doesn&apos;t exists</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：不存在</translation>
    </message>
    <message>
        <source>%1: out of resources</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：资源耗尽了</translation>
    </message>
    <message>
        <source>%1: unknown error %2</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：未知错误 %2</translation>
    </message>
    <message>
        <location filename="../src/corelib/kernel/qsystemsemaphore_unix.cpp" line="+119"/>
        <source>%1: key is empty</source>
        <comment>QSystemSemaphore</comment>
        <translation>%1: ачкыч бош</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1: unable to make key</source>
        <comment>QSystemSemaphore</comment>
        <translation>%1: ачкыч жасай албай калды</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1: ftok failed</source>
        <comment>QSystemSemaphore</comment>
        <translation>%1: фток ишке ашпады</translation>
    </message>
</context>
<context>
    <name>QDB2Driver</name>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="+1276"/>
        <source>Unable to connect</source>
        <translation>Туташуу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+303"/>
        <source>Unable to commit transaction</source>
        <translation>Бүтүмдөрдү ишке ашыруу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback transaction</source>
        <translation>Транзакцияны кайтарууга мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to set autocommit</source>
        <translation>Автокоммуникацияны орнотууга мүмкүн эмес</translation>
    </message>
</context>
<context>
    <name>QDB2Result</name>
    <message>
        <location line="-1043"/>
        <location line="+243"/>
        <source>Unable to execute statement</source>
        <translation>Билдирүүнү аткарууга мүмкүн эмес</translation>
    </message>
    <message>
        <location line="-206"/>
        <source>Unable to prepare statement</source>
        <translation>Билдирүү даярдай албай калды</translation>
    </message>
    <message>
        <location line="+196"/>
        <source>Unable to bind variable</source>
        <translation>Өзгөргүчтү байлоо мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Unable to fetch record %1</source>
        <translation>%1 рекордун алып келүүгө болбоду</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to fetch next</source>
        <translation>Кийинки алып келүүгө мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to fetch first</source>
        <translation>Адегенде алып келүүгө мүмкүн эмес</translation>
    </message>
</context>
<context>
    <name>QDateTimeEdit</name>
    <message>
        <location filename="../src/gui/widgets/qdatetimeedit.cpp" line="+2295"/>
        <source>AM</source>
        <translation>АМ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>am</source>
        <translation>ам</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>PM</source>
        <translation>Премьер-Министр</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>pm</source>
        <translation>Премьер-Министр</translation>
    </message>
</context>
<context>
    <name>QDial</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+951"/>
        <source>QDial</source>
        <translation>QDial</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>SpeedoMeter</source>
        <translation>SpeedoMeter</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>SliderHandle</source>
        <translation>СлайдерХандл</translation>
    </message>
</context>
<context>
    <name>QDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qdialog.cpp" line="+597"/>
        <source>What&apos;s This?</source>
        <translation>Бул эмне?</translation>
    </message>
    <message>
        <location line="-115"/>
        <source>Done</source>
        <translation>Аткарылды</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.cpp" line="+1866"/>
        <location line="+464"/>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="+561"/>
        <source>OK</source>
        <translation>Макул</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="+3"/>
        <source>Save</source>
        <translation>Сактоо</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Save</source>
        <translation>&amp;amp, сактоо</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open</source>
        <translation>Ачуу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Close</source>
        <translation>&amp; жабуу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Apply</source>
        <translation>Колдонуу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reset</source>
        <translation>Калыбына келтирүү</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Help</source>
        <translation>Жардам</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Don&apos;t Save</source>
        <translation>Сактабагыла</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Discard</source>
        <translation>Ыргытуу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Yes</source>
        <translation>Ооба</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Yes to &amp;All</source>
        <translation>Ооба , баарына</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;No</source>
        <translation>&amp;amp, жок</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>N&amp;o to All</source>
        <translation>Н &amp;amp, баарына</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save All</source>
        <translation>Бардыгын сактап калуу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abort</source>
        <translation>Аборт</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Retry</source>
        <translation>Ретри</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ignore</source>
        <translation>Четке кагуу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restore Defaults</source>
        <translation>Дефолттарды калыбына келтирүү</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Close without Saving</source>
        <translation>Үнөмдөбөстөн жабуу</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>&amp;OK</source>
        <translation>&amp;amp, жарайды</translation>
    </message>
</context>
<context>
    <name>QDirModel</name>
    <message>
        <location filename="../src/gui/itemviews/qdirmodel.cpp" line="+453"/>
        <source>Name</source>
        <translation>Аты-жөнү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Size</source>
        <translation>Көлөмү</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation>Боорукердик</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation>Түрү</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Date Modified</source>
        <translation>Өзгөртүлгөн дата</translation>
    </message>
</context>
<context>
    <name>QDockWidget</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/qaccessiblewidgets.cpp" line="+1239"/>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dock</source>
        <translation>Док</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Float</source>
        <translation>Сүзгүч</translation>
    </message>
</context>
<context>
    <name>QDoubleSpinBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="-537"/>
        <source>More</source>
        <translation>Кененирээк</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Less</source>
        <translation>Аз</translation>
    </message>
</context>
<context>
    <name>QErrorMessage</name>
    <message>
        <location filename="../src/gui/dialogs/qerrormessage.cpp" line="+192"/>
        <source>Debug Message:</source>
        <translation>Дебуг билдирүү:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning:</source>
        <translation>Эскертүү:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Fatal Error:</source>
        <translation>Өлүмгө дуушар болгон ката:</translation>
    </message>
    <message>
        <location line="+193"/>
        <source>&amp;Show this message again</source>
        <translation>Бул кабарды кайрадан көрсөткүлө</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;OK</source>
        <translation>&amp;amp, жарайды</translation>
    </message>
</context>
<context>
    <name>QFile</name>
    <message>
        <location filename="../src/corelib/io/qfile.cpp" line="+708"/>
        <location line="+141"/>
        <source>Destination file exists</source>
        <translation>Багыт файлы бар</translation>
    </message>
    <message>
        <location line="-108"/>
        <source>Cannot remove source file</source>
        <translation>Булак файлын жоготуу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+120"/>
        <source>Cannot open %1 for input</source>
        <translation>Киргизүү үчүн %1 ачуу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Cannot open for output</source>
        <translation>Чыгаруу үчүн ачуу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Failure to write block</source>
        <translation>Блок жазылбагандыгы</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot create %1 for output</source>
        <translation>Чыгаруу үчүн %1 түзүү мүмкүн эмес</translation>
    </message>
</context>
<context>
    <name>QFileDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="+515"/>
        <location line="+444"/>
        <source>All Files (*)</source>
        <translation>Бардык файлдар (*)</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>Directories</source>
        <translation>Каталогдор</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+50"/>
        <location line="+1471"/>
        <location line="+75"/>
        <source>&amp;Open</source>
        <translation>&amp;amp, ачык</translation>
    </message>
    <message>
        <location line="-1596"/>
        <location line="+50"/>
        <source>&amp;Save</source>
        <translation>&amp;amp, сактоо</translation>
    </message>
    <message>
        <location line="-730"/>
        <source>Open</source>
        <translation>Ачуу</translation>
    </message>
    <message>
        <location line="+1515"/>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation>%1 буга чейин бар.
Аны алмаштыргыңар келеби?</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>%1
File not found.
Please verify the correct file name was given.</source>
        <translation>%1
Файл табылган жок.
Файлдын туура аты берилгендигин текшериңиз.</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qdirmodel.cpp" line="+402"/>
        <source>My Computer</source>
        <translation>Компьютерим</translation>
    </message>
    <message>
        <source>%1 File</source>
        <extracomment>%1 is a file name suffix, for example txt</extracomment>
        <translation>%1 файлы</translation>
    </message>
    <message>
        <source>%1 KiB</source>
        <translation>%1 КИБ</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="-1504"/>
        <source>&amp;Rename</source>
        <translation>&amp;amp, атын алмаштыруу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Delete</source>
        <translation>Жоготуу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show &amp;hidden files</source>
        <translation>Көрсөтүү &amp; жашыруун файлдар</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.ui"/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Back</source>
        <translation>Артка</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Parent Directory</source>
        <translation>Башкы каталог</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>List View</source>
        <translation>Тизменин көрүнүшү</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Detail View</source>
        <translation>Майда - чүйдөсүнө чейин көрүү</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Files of type:</source>
        <translation>Типтеги файлдар:</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="+6"/>
        <location line="+648"/>
        <source>Directory:</source>
        <translation>Каталог:</translation>
    </message>
    <message>
        <location line="+794"/>
        <location line="+862"/>
        <source>%1
Directory not found.
Please verify the correct directory name was given.</source>
        <translation>%1
Каталог табылган жок.
Туура каталогдун аты берилгенин текшериңиз.</translation>
    </message>
    <message>
        <location line="-218"/>
        <source>&apos;%1&apos; is write protected.
Do you want to delete it anyway?</source>
        <translation>&apos;%1&apos; корголгон жазуу болуп саналат.
Кандай болбосун, аны жоготууну каалайсызбы?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Are sure you want to delete &apos;%1&apos;?</source>
        <translation>&quot;%1&apos;ды&quot; жоготууну каалайсызбы?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Are you sure you want to delete &apos;%1&apos;?</source>
        <translation>Сиз &apos;%1&apos; жоготууга келет деп ишенесизби?</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Жоготуу</translation>
    </message>
    <message>
        <source>Folder</source>
        <translation>Папка</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Could not delete directory.</source>
        <translation>Каталогду жоготууга болбоду.</translation>
    </message>
    <message>
        <location line="+407"/>
        <source>Recent Places</source>
        <translation>Акыркы жерлер</translation>
    </message>
    <message>
        <location line="-2550"/>
        <source>Save As</source>
        <translation>Куткаруу</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qfileiconprovider.cpp" line="+411"/>
        <source>Drive</source>
        <translation>Диск</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+1"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Unknown</source>
        <translation>Белгисиз</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="-4"/>
        <source>Find Directory</source>
        <translation>Каталогду табуу</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Show </source>
        <translation>Көрсөтүү </translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.ui"/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Forward</source>
        <translation>Алдыга</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="+1970"/>
        <source>New Folder</source>
        <translation>Жаңы папка</translation>
    </message>
    <message>
        <location line="-1963"/>
        <source>&amp;New Folder</source>
        <translation>&amp;amp, жаңы папка</translation>
    </message>
    <message>
        <location line="+656"/>
        <location line="+38"/>
        <source>&amp;Choose</source>
        <translation>Тандоо</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qsidebar.cpp" line="+418"/>
        <source>Remove</source>
        <translation>Жоготуу</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="-687"/>
        <location line="+652"/>
        <source>File &amp;name:</source>
        <translation>Файл &amp; аты:</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.ui"/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Look in:</source>
        <translation>Кара:</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Create New Folder</source>
        <translation>Жаңы папка түзүү</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog_win.cpp" line="+160"/>
        <source>All Files (*.*)</source>
        <translation>Бардык файлдар (*.*)</translation>
    </message>
</context>
<context>
    <name>QFileSystemModel</name>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel.cpp" line="+744"/>
        <source>%1 TB</source>
        <translation>%1 туберкулин</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation>%1 ГБ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation>%1 МБ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation>%1 КБ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 bytes</source>
        <translation>%1 байт</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Invalid filename</source>
        <translation>Туура эмес файл аты</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;b&gt;The name &quot;%1&quot; can not be used.&lt;/b&gt;&lt;p&gt;Try using another name, with fewer characters or no punctuations marks.</source>
        <translation>&lt;b&gt;&quot;%1&quot; аты колдонулбайт.&lt;/b&gt; &lt;p&gt;Башка ысымды колдонууга аракет кылыңыз, символдор аз же тактоо белгилери жок.</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Name</source>
        <translation>Аты-жөнү</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Size</source>
        <translation>Көлөмү</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation>Боорукердик</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation>Түрү</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Date Modified</source>
        <translation>Өзгөртүлгөн дата</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel_p.h" line="+234"/>
        <source>My Computer</source>
        <translation>Компьютерим</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Computer</source>
        <translation>Компьютер</translation>
    </message>
    <message>
        <source>%1 KiB</source>
        <translation>%1 КИБ</translation>
    </message>
</context>
<context>
    <name>QFontDatabase</name>
    <message>
        <location filename="../src/gui/text/qfontdatabase.cpp" line="+90"/>
        <location line="+1176"/>
        <source>Normal</source>
        <translation>Нормалдуу</translation>
    </message>
    <message>
        <location line="-1173"/>
        <location line="+12"/>
        <location line="+1149"/>
        <source>Bold</source>
        <translation>Кайраттуу</translation>
    </message>
    <message>
        <location line="-1158"/>
        <location line="+1160"/>
        <source>Demi Bold</source>
        <translation>Деми Болд</translation>
    </message>
    <message>
        <location line="-1157"/>
        <location line="+18"/>
        <location line="+1135"/>
        <source>Black</source>
        <translation>Кара</translation>
    </message>
    <message>
        <location line="-1145"/>
        <source>Demi</source>
        <translation>Деми</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+1145"/>
        <source>Light</source>
        <translation>Жарык</translation>
    </message>
    <message>
        <location line="-1004"/>
        <location line="+1007"/>
        <source>Italic</source>
        <translation>Титалик</translation>
    </message>
    <message>
        <location line="-1004"/>
        <location line="+1006"/>
        <source>Oblique</source>
        <translation>Облик</translation>
    </message>
    <message>
        <location line="+705"/>
        <source>Any</source>
        <translation>Ар кандай</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Latin</source>
        <translation>Латын тили</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Greek</source>
        <translation>Грекче</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cyrillic</source>
        <translation>Кириллич</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Armenian</source>
        <translation>Армянча</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hebrew</source>
        <translation>Еврейче</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Arabic</source>
        <translation>Арабча</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Syriac</source>
        <translation>Сирия</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Thaana</source>
        <translation>Таана</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Devanagari</source>
        <translation>Деванагари</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Bengali</source>
        <translation>Бенгалча</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gurmukhi</source>
        <translation>Гурмуки</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gujarati</source>
        <translation>Гужаратиче</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Oriya</source>
        <translation>Ория</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Tamil</source>
        <translation>Тамилче</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Telugu</source>
        <translation>Телугуча</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Kannada</source>
        <translation>Каннадача</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Malayalam</source>
        <translation>Малаяламча</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sinhala</source>
        <translation>Синхала</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Thai</source>
        <translation>Тайча</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Lao</source>
        <translation>Лао</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Tibetan</source>
        <translation>Тибет</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Myanmar</source>
        <translation>Мьянма</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Georgian</source>
        <translation>Грузинче</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Khmer</source>
        <translation>Хмер</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Simplified Chinese</source>
        <translation>Жөнөкөйлөтүлгөн кытайлар</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Traditional Chinese</source>
        <translation>Салттуу кытайлар</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Japanese</source>
        <translation>Жапончо</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Korean</source>
        <translation>Корейче</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Vietnamese</source>
        <translation>Вьетнамча</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Symbol</source>
        <translation>Символ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ogham</source>
        <translation>Огам</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Runic</source>
        <translation>Чуркоо</translation>
    </message>
</context>
<context>
    <name>QFontDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qfontdialog.cpp" line="+772"/>
        <source>&amp;Font</source>
        <translation>&amp;amp, шрифт</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Font st&amp;yle</source>
        <translation>Шрифт сент-йл</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>&amp;amp, көлөмү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Effects</source>
        <translation>Эффекттер</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stri&amp;keout</source>
        <translation>Стрит&amp;кейт</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Underline</source>
        <translation>&amp;amp, сызык</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sample</source>
        <translation>Үлгү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wr&amp;iting System</source>
        <translation>Вр&amp; итинг системасы</translation>
    </message>
    <message>
        <location line="-604"/>
        <location line="+247"/>
        <source>Select Font</source>
        <translation>Шрифт тандоо</translation>
    </message>
</context>
<context>
    <name>QFtp</name>
    <message>
        <location filename="../src/network/access/qftp.cpp" line="+826"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+683"/>
        <source>Not connected</source>
        <translation>Туташтырылган жок</translation>
    </message>
    <message>
        <location line="+65"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+65"/>
        <source>Host %1 not found</source>
        <translation>%1 хост табылган жок</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+4"/>
        <source>Connection refused to host %1</source>
        <translation>Туташуу %1 өткөрүүдөн баш тартты</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection timed out to host %1</source>
        <translation>%1 хостуна туташуу</translation>
    </message>
    <message>
        <location line="+104"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+102"/>
        <location line="+1451"/>
        <source>Connected to host %1</source>
        <translation>%1 хостуна туташтырылган</translation>
    </message>
    <message>
        <location line="+219"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="-1290"/>
        <source>Connection refused for data connection</source>
        <translation>Маалыматтарды туташтыруу үчүн туташуудан баш тартты</translation>
    </message>
    <message>
        <location line="+178"/>
        <location line="+29"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+195"/>
        <location line="+728"/>
        <source>Unknown error</source>
        <translation>Белгисиз ката</translation>
    </message>
    <message>
        <location line="+889"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+77"/>
        <source>Connecting to host failed:
%1</source>
        <translation>Хостка туташуу ишке ашпады:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Login failed:
%1</source>
        <translation>Кирүү ишке ашпады:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Listing directory failed:
%1</source>
        <translation>Листинг каталдыгы ишке ашпады:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Changing directory failed:
%1</source>
        <translation>Каталогду өзгөртүү ишке ашпады:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Downloading file failed:
%1</source>
        <translation>Файлды жүктөө ишке ашпады:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Uploading file failed:
%1</source>
        <translation>Файлды жүктөө ишке ашпады:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Removing file failed:
%1</source>
        <translation>Файлды алып салуу ишке ашпады:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Creating directory failed:
%1</source>
        <translation>Каталогду түзүү ишке ашпады:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Removing directory failed:
%1</source>
        <translation>Каталогду алып салуу ишке ашпады:
%1</translation>
    </message>
    <message>
        <location line="+28"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+25"/>
        <location line="+250"/>
        <source>Connection closed</source>
        <translation>Туташуу жабылды</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="-11"/>
        <source>Host %1 found</source>
        <translation>%1 хост табылган</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection to %1 closed</source>
        <translation>%1 туташтыруу жабылды</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Host found</source>
        <translation>Хост табылган</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Connected to host</source>
        <translation>Хостка туташтырылган</translation>
    </message>
</context>
<context>
    <name>QGuiApplication</name>
    <message>
        <location filename="../src/gui/kernel/qapplication.cpp" line="+2248"/>
        <source>QT_LAYOUT_DIRECTION</source>
        <comment>Translate this string to the string &apos;LTR&apos; in left-to-right languages or to &apos;RTL&apos; in right-to-left languages (such as Hebrew and Arabic) to get proper widget layout.</comment>
        <translation>QT_LAYOUT_DIRECTION</translation>
    </message>
</context>
<context>
    <name>QHostInfo</name>
    <message>
        <location filename="../src/network/kernel/qhostinfo_p.h" line="+183"/>
        <source>Unknown error</source>
        <translation>Белгисиз ката</translation>
    </message>
</context>
<context>
    <name>QHostInfoAgent</name>
    <message>
        <location filename="../src/network/kernel/qhostinfo_unix.cpp" line="+178"/>
        <location line="+9"/>
        <location line="+64"/>
        <location line="+31"/>
        <location filename="../src/network/kernel/qhostinfo_win.cpp" line="+180"/>
        <location line="+9"/>
        <location line="+40"/>
        <location line="+27"/>
        <source>Host not found</source>
        <translation>Хост табылган жок</translation>
    </message>
    <message>
        <location line="-44"/>
        <location line="+39"/>
        <location filename="../src/network/kernel/qhostinfo_win.cpp" line="-34"/>
        <location line="+29"/>
        <source>Unknown address type</source>
        <translation>Даректин белгисиз түрү</translation>
    </message>
    <message>
        <location line="+8"/>
        <location filename="../src/network/kernel/qhostinfo_win.cpp" line="-19"/>
        <location line="+27"/>
        <source>Unknown error</source>
        <translation>Белгисиз ката</translation>
    </message>
</context>
<context>
    <name>QHttp</name>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="+1574"/>
        <location line="+820"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+1160"/>
        <location line="+567"/>
        <source>Unknown error</source>
        <translation>Белгисиз ката</translation>
    </message>
    <message>
        <location line="-568"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="-370"/>
        <source>Request aborted</source>
        <translation>Өтүнүч токтотулду</translation>
    </message>
    <message>
        <location line="+579"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+381"/>
        <source>No server set to connect to</source>
        <translation>Туташуу үчүн орнотулган сервер жок</translation>
    </message>
    <message>
        <location line="+164"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+56"/>
        <source>Wrong content length</source>
        <translation>Туура эмес мазмун узундугу</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+4"/>
        <source>Server closed connection unexpectedly</source>
        <translation>Сервер күтүлбөгөн жерден байланышты жапты</translation>
    </message>
    <message>
        <location line="+179"/>
        <source>Unknown authentication method</source>
        <translation>Белгисиз аутентификация ыкмасы</translation>
    </message>
    <message>
        <location line="+183"/>
        <source>Error writing response to device</source>
        <translation>Орнотмого жооп жазуу катасы</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="+876"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+38"/>
        <source>Connection refused</source>
        <translation>Туташуудан баш тартты</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="-304"/>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="-4"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+3"/>
        <source>Host %1 not found</source>
        <translation>%1 хост табылган жок</translation>
    </message>
    <message>
        <location line="+20"/>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="+10"/>
        <location line="+19"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+3"/>
        <source>HTTP request failed</source>
        <translation>HTTP өтүнүчү ишке ашпады</translation>
    </message>
    <message>
        <location line="+73"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+69"/>
        <source>Invalid HTTP response header</source>
        <translation>Жараксыз HTTP жооп баштык</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+48"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+40"/>
        <location line="+47"/>
        <source>Invalid HTTP chunked body</source>
        <translation>Жараксыз HTTP кесилген дене</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="+294"/>
        <source>Host %1 found</source>
        <translation>%1 хост табылган</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connected to host %1</source>
        <translation>%1 хостуна туташтырылган</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection to %1 closed</source>
        <translation>%1 туташтыруу жабылды</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Host found</source>
        <translation>Хост табылган</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connected to host</source>
        <translation>Хостка туташтырылган</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="-22"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+3"/>
        <source>Connection closed</source>
        <translation>Туташуу жабылды</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="-135"/>
        <source>Proxy authentication required</source>
        <translation>Прокси аутентификациясы талап кылынат</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Authentication required</source>
        <translation>Аутентификация талап кылынат</translation>
    </message>
    <message>
        <location line="-138"/>
        <source>Connection refused (or timed out)</source>
        <translation>Туташуудан баш тартты (же убакыттын өтүшү менен)</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="+6"/>
        <source>Proxy requires authentication</source>
        <translation>Прокси аутентификацияны талап кылат</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Host requires authentication</source>
        <translation>Хост аутентификацияны талап кылат</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Data corrupted</source>
        <translation>Маалыматтар бузулган</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown protocol specified</source>
        <translation>Аныкталбаган протокол көрсөтүлгөн</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>SSL handshake failed</source>
        <translation>SSL кол чабуу ишке ашпады</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="-2263"/>
        <source>HTTPS connection requested but SSL support not compiled in</source>
        <translation>HTTPS туташтыруу суралган, бирок SSL колдоосу түзүлбөгөн</translation>
    </message>
</context>
<context>
    <name>QHttpSocketEngine</name>
    <message>
        <location filename="../src/network/socket/qhttpsocketengine.cpp" line="-89"/>
        <source>Did not receive HTTP response from proxy</source>
        <translation>Проксиден HTTP жооп алган жок</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Error parsing authentication request from proxy</source>
        <translation>Проксиден аутентификациялык өтүнүчтү талдоо катасы</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Authentication required</source>
        <translation>Аутентификация талап кылынат</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Proxy denied connection</source>
        <translation>Прокси байланышты четке какты</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Error communicating with HTTP proxy</source>
        <translation>HTTP проксиси менен байланышуу катасы</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Proxy server not found</source>
        <translation>Прокси сервери табылган жок</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy connection refused</source>
        <translation>Прокси туташуудан баш тартты</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy server connection timed out</source>
        <translation>Прокси сервер туташтыруу убакыт</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy connection closed prematurely</source>
        <translation>Прокси туташтыруу мөөнөтүнөн мурда жабылды</translation>
    </message>
</context>
<context>
    <name>QIBaseDriver</name>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="+1428"/>
        <source>Error opening database</source>
        <translation>Маалымат базасын ачуу катасы</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Could not start transaction</source>
        <translation>Транзакцияны баштоо мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unable to commit transaction</source>
        <translation>Бүтүмдөрдү ишке ашыруу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unable to rollback transaction</source>
        <translation>Транзакцияны кайтарууга мүмкүн эмес</translation>
    </message>
</context>
<context>
    <name>QIBaseResult</name>
    <message>
        <location line="-1097"/>
        <source>Unable to create BLOB</source>
        <translation>БЛОБ түзүүгө мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to write BLOB</source>
        <translation>БЛОБ жазууга мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Unable to open BLOB</source>
        <translation>БЛОБ ачууга мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Unable to read BLOB</source>
        <translation>БЛОБ окууга мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+189"/>
        <source>Could not find array</source>
        <translation>Массивди табууга болбоду</translation>
    </message>
    <message>
        <location line="-157"/>
        <source>Could not get array data</source>
        <translation>Массивдик маалыматтарды алуу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+212"/>
        <source>Could not get query info</source>
        <translation>Суроо-маалымат алуу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Could not start transaction</source>
        <translation>Транзакцияны баштоо мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to commit transaction</source>
        <translation>Бүтүмдөрдү ишке ашыруу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Could not allocate statement</source>
        <translation>Билдирүү бөлүштүрүү мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Could not prepare statement</source>
        <translation>Билдирүү даярдай алган жок</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+7"/>
        <source>Could not describe input statement</source>
        <translation>Кирүү билдирүүсү сүрөттөлбөйт</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not describe statement</source>
        <translation>Билдирүүнүн сүрөттөлүшү мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Unable to close statement</source>
        <translation>Билдирүүнү жабуу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Unable to execute query</source>
        <translation>Сурамды аткарууга мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Could not fetch next item</source>
        <translation>Кийинки элементти алып келбей калды</translation>
    </message>
    <message>
        <location line="+160"/>
        <source>Could not get statement info</source>
        <translation>Маалымат алуу мүмкүн эмес</translation>
    </message>
</context>
<context>
    <name>QIODevice</name>
    <message>
        <location filename="../src/corelib/global/qglobal.cpp" line="+1869"/>
        <source>Permission denied</source>
        <translation>Уруксат берилди</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Too many open files</source>
        <translation>Өтө көп ачык файлдар</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>No such file or directory</source>
        <translation>Мындай файл же каталог жок</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>No space left on device</source>
        <translation>Аппаратта орун калган жок</translation>
    </message>
    <message>
        <location filename="../src/corelib/io/qiodevice.cpp" line="+1536"/>
        <source>Unknown error</source>
        <translation>Белгисиз ката</translation>
    </message>
</context>
<context>
    <name>QInputContext</name>
    <message>
        <location filename="../src/gui/inputmethod/qinputcontextfactory.cpp" line="+242"/>
        <source>XIM</source>
        <translation>XIM</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>XIM input method</source>
        <translation>XIM киргизүү ыкмасы</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Windows input method</source>
        <translation>Windows киргизүү ыкмасы</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Mac OS X input method</source>
        <translation>Mac OS X киргизүү ыкмасы</translation>
    </message>
</context>
<context>
    <name>QInputDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qinputdialog.cpp" line="+223"/>
        <source>Enter a value:</source>
        <translation>Бир наркты киргизүү:</translation>
    </message>
</context>
<context>
    <name>QLibrary</name>
    <message>
        <source>QLibrary::load_sys: Cannot load %1 (%2)</source>
        <translation type="obsolete">QLibrary::load_sys： 不能载入%1 (%2)</translation>
    </message>
    <message>
        <source>QLibrary::unload_sys: Cannot unload %1 (%2)</source>
        <translation type="obsolete">QLibrary::unload_sys：不能卸载%1 (%2)</translation>
    </message>
    <message>
        <source>QLibrary::resolve_sys: Symbol &quot;%1&quot; undefined in %2 (%3)</source>
        <translation type="obsolete">QLibrary::resolve_sys: 符号“%1”在%2（%3）没有被定义</translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary.cpp" line="+378"/>
        <source>Could not mmap &apos;%1&apos;: %2</source>
        <translation>&apos;%1&apos;: %2</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Plugin verification data mismatch in &apos;%1&apos;</source>
        <translation>«%1» плагин текшерүү маалыматтарынын шайкеш келбесин</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Could not unmap &apos;%1&apos;: %2</source>
        <translation>&apos;%1&apos;: %2</translation>
    </message>
    <message>
        <location line="+302"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (%2.%3.%4) [%5]</source>
        <translation>«%1» плагининде шайкеш келбеген Qt китепканасы колдонулат. (%2.%3.%4) [%5]</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. Expected build key &quot;%2&quot;, got &quot;%3&quot;</source>
        <translation>«%1» плагининде шайкеш келбеген Qt китепканасы колдонулат. Күтүлүүчү курулуш ачкычы &quot;%2&quot;, &quot;%3&quot; алды</translation>
    </message>
    <message>
        <location line="+340"/>
        <source>Unknown error</source>
        <translation>Белгисиз ката</translation>
    </message>
    <message>
        <location line="-377"/>
        <location filename="../src/corelib/plugin/qpluginloader.cpp" line="+280"/>
        <source>The shared library was not found.</source>
        <translation>Жалпы китепкана табылган жок.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>The file &apos;%1&apos; is not a valid Qt plugin.</source>
        <translation>&apos;%1&apos; файлы жарактуу Qt плагини эмес.</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (Cannot mix debug and release libraries.)</source>
        <translation>«%1» плагининде шайкеш келбеген Qt китепканасы колдонулат. (Дебют аралаштырып, китепканаларды чыгарууга болбойт.)</translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary_unix.cpp" line="+209"/>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="+99"/>
        <source>Cannot load library %1: %2</source>
        <translation>%1: %2 жүктөө мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+16"/>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="+26"/>
        <source>Cannot unload library %1: %2</source>
        <translation>%1: %2 китепкананы түшүрүү мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+31"/>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="+15"/>
        <source>Cannot resolve symbol &quot;%1&quot; in %2: %3</source>
        <translation>%2: %3 менен &quot;%1&quot; символун чечүү мүмкүн эмес</translation>
    </message>
</context>
<context>
    <name>QLineEdit</name>
    <message>
        <location filename="../src/gui/widgets/qlineedit.cpp" line="+2680"/>
        <source>&amp;Undo</source>
        <translation>&amp;amp, Ундо</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Redo</source>
        <translation>&amp;amp, Редо</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cu&amp;t</source>
        <translation>Ку&amp;т</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Copy</source>
        <translation>Көчүрмө</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Paste</source>
        <translation>&amp;amp, паста</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Delete</source>
        <translation>Жоготуу</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Select All</source>
        <translation>Бардыгын тандоо</translation>
    </message>
</context>
<context>
    <name>QLocalServer</name>
    <message>
        <location filename="../src/network/socket/qlocalserver.cpp" line="+226"/>
        <location filename="../src/network/socket/qlocalserver_unix.cpp" line="+231"/>
        <source>%1: Name error</source>
        <translation>%1: Аты катасы</translation>
    </message>
    <message>
        <location filename="../src/network/socket/qlocalserver_unix.cpp" line="-8"/>
        <source>%1: Permission denied</source>
        <translation>%1: Уруксат берилди</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1: Address in use</source>
        <translation>%1: Колдонулуп жаткан дарек</translation>
    </message>
    <message>
        <location line="+5"/>
        <location filename="../src/network/socket/qlocalserver_win.cpp" line="+158"/>
        <source>%1: Unknown error %2</source>
        <translation>%1: Белгисиз ката %2</translation>
    </message>
</context>
<context>
    <name>QLocalSocket</name>
    <message>
        <location filename="../src/network/socket/qlocalsocket_tcp.cpp" line="+132"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+134"/>
        <source>%1: Connection refused</source>
        <translation>%1: Туташуудан баш тартты</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Remote closed</source>
        <translation>%1: Алыстан жабылды</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_win.cpp" line="+80"/>
        <location line="+43"/>
        <source>%1: Invalid name</source>
        <translation>%1: Жараксыз аты</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket access error</source>
        <translation>%1: Сокетке кирүү катасы</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket resource error</source>
        <translation>%1: Сокет ресурстук ката</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket operation timed out</source>
        <translation>%1: Сокет операциясы убакыттын өтүшү менен</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Datagram too large</source>
        <translation>%1: Берилиштер граммы өтө чоң</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_win.cpp" line="-48"/>
        <source>%1: Connection error</source>
        <translation>%1: Туташуу катасы</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: The socket operation is not supported</source>
        <translation>%1: Сокет операциясы колдолбойт</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: Unknown error</source>
        <translation>%1: Белгисиз ката</translation>
    </message>
    <message>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+4"/>
        <location filename="../src/network/socket/qlocalsocket_win.cpp" line="+10"/>
        <source>%1: Unknown error %2</source>
        <translation>%1: Белгисиз ката %2</translation>
    </message>
</context>
<context>
    <name>QMYSQLDriver</name>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="+1231"/>
        <source>Unable to open database &apos;</source>
        <translation>Маалымат базасын ачууга мүмкүн эмес &apos;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to connect</source>
        <translation>Туташуу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+127"/>
        <source>Unable to begin transaction</source>
        <translation>Транзакцияны баштоо мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to commit transaction</source>
        <translation>Бүтүмдөрдү ишке ашыруу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback transaction</source>
        <translation>Транзакцияны кайтарууга мүмкүн эмес</translation>
    </message>
</context>
<context>
    <name>QMYSQLResult</name>
    <message>
        <location line="-922"/>
        <source>Unable to fetch data</source>
        <translation>Маалыматтарды алып келүүгө мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+176"/>
        <source>Unable to execute query</source>
        <translation>Сурамды аткарууга мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to store result</source>
        <translation>Натыйжаны сактай албай калды</translation>
    </message>
    <message>
        <location line="+190"/>
        <location line="+8"/>
        <source>Unable to prepare statement</source>
        <translation>Билдирүү даярдай албай калды</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Unable to reset statement</source>
        <translation>Билдирүүнү калыбына келтирүү мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Unable to bind value</source>
        <translation>Маанини байлоо мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Unable to execute statement</source>
        <translation>Билдирүүнү аткарууга мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+14"/>
        <location line="+21"/>
        <source>Unable to bind outvalues</source>
        <translation>Бааларды байлоо мүмкүн эмес</translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Unable to store statement results</source>
        <translation>Билдирүүнүн жыйынтыктарын сактай албай калды</translation>
    </message>
    <message>
        <location line="-253"/>
        <source>Unable to execute next query</source>
        <translation>Кийинки суроону аткарууга мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Unable to store next result</source>
        <translation>Кийинки натыйжаны сактай албай калды</translation>
    </message>
</context>
<context>
    <name>QMdiArea</name>
    <message>
        <location filename="../src/gui/widgets/qmdiarea.cpp" line="+290"/>
        <source>(Untitled)</source>
        <translation>(Чексиз)</translation>
    </message>
</context>
<context>
    <name>QMdiSubWindow</name>
    <message>
        <location filename="../src/gui/widgets/qmdisubwindow.cpp" line="+280"/>
        <source>%1 - [%2]</source>
        <translation>%1 - [%2]</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Minimize</source>
        <translation>Минималдуу</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Restore Down</source>
        <translation>Калыбына келтирүү</translation>
    </message>
    <message>
        <location line="+707"/>
        <source>&amp;Restore</source>
        <translation>Калыбына келтирүү</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Move</source>
        <translation>Көчүп баруу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>&amp;amp, көлөмү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mi&amp;nimize</source>
        <translation>Ми &amp;amp, нимиз</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ma&amp;ximize</source>
        <translation>Ма &amp;amp, симиз</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stay on &amp;Top</source>
        <translation>Жогоруда болгула</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Close</source>
        <translation>&amp; жабуу</translation>
    </message>
    <message>
        <location line="-787"/>
        <source>- [%1]</source>
        <translation>- [%1]</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Maximize</source>
        <translation>Максималдуу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unshade</source>
        <translation>Уншэйд</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Shade</source>
        <translation>Көлөкө</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Restore</source>
        <translation>Калыбына келтирүү</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Help</source>
        <translation>Жардам</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Menu</source>
        <translation>Меню</translation>
    </message>
</context>
<context>
    <name>QMenu</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/qaccessiblemenu.cpp" line="+157"/>
        <location line="+225"/>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
    <message>
        <location line="-224"/>
        <location line="+225"/>
        <source>Open</source>
        <translation>Ачуу</translation>
    </message>
    <message>
        <location line="-223"/>
        <location line="+225"/>
        <location line="+51"/>
        <source>Execute</source>
        <translation>Аткаруу</translation>
    </message>
</context>
<context>
    <name>QMenuBar</name>
    <message>
        <source>About</source>
        <translation type="obsolete">关于</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="obsolete">配置</translation>
    </message>
    <message>
        <source>Preference</source>
        <translation type="obsolete">首选项</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">选项</translation>
    </message>
    <message>
        <source>Setting</source>
        <translation type="obsolete">设置</translation>
    </message>
    <message>
        <source>Setup</source>
        <translation type="obsolete">安装</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="obsolete">退出</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="obsolete">退出</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation type="obsolete">关于%1</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation type="obsolete">关于Qt</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation type="obsolete">首选项</translation>
    </message>
    <message>
        <source>Quit %1</source>
        <translation type="obsolete">退出%1</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.cpp" line="-1111"/>
        <source>Help</source>
        <translation>Жардам</translation>
    </message>
    <message>
        <location line="-853"/>
        <location line="+852"/>
        <location filename="../src/gui/dialogs/qmessagebox.h" line="-52"/>
        <location line="+8"/>
        <source>OK</source>
        <translation>Макул</translation>
    </message>
    <message>
        <location line="+509"/>
        <source>About Qt</source>
        <translation>Qt жөнүндө</translation>
    </message>
    <message>
        <source>&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;这个程序使用的是Qt %1版。&lt;/p&gt;</translation>
    </message>
    <message>
        <location line="-1605"/>
        <source>Show Details...</source>
        <translation>Егжей-тегжейин</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide Details...</source>
        <translation>Маалымат жашыруу...</translation>
    </message>
    <message>
        <location line="+1570"/>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt for Embedded Linux and Qt for Windows CE.&lt;/p&gt;&lt;p&gt;Qt is available under three different licensing options designed to accommodate the needs of our various users.&lt;/p&gt;Qt licensed under our commercial license agreement is appropriate for development of proprietary/commercial software where you do not want to share any source code with third parties or otherwise cannot comply with the terms of the GNU LGPL version 2.1 or GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU LGPL version 2.1 is appropriate for the development of Qt applications (proprietary or open source) provided you can comply with the terms and conditions of the GNU LGPL version 2.1.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU General Public License version 3.0 is appropriate for the development of Qt applications where you wish to use such applications in combination with software subject to the terms of the GNU GPL version 3.0 or where you are otherwise willing to comply with the terms of the GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://qt.nokia.com/products/licensing&quot;&gt;qt.nokia.com/products/licensing&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;&lt;p&gt;Copyright (C) 2012 Nokia Corporation and/or its subsidiary(-ies).&lt;/p&gt;&lt;p&gt;Qt is a Nokia product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Qt жөнүндө&lt;/h3&gt;&lt;p&gt;Бул программада Qt версиясы %1 колдонулат.&lt;/p&gt; &lt;p&gt;Qt — кросс-платформалык колдонмо иштеп чыгуу үчүн C++ инструменти.&lt;/p&gt; &lt;p&gt;Qt MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux жана бардык негизги коммерциялык Unix варианттары аркылуу бир булактуу портативдүүлүк берет. Qt, ошондой эле Windows CE үчүн Камтылган Linux жана Qt үчүн Qt катары киргизилген түзмөктөр үчүн жеткиликтүү.&lt;/p&gt; &lt;p&gt;Qt биздин ар кандай колдонуучулардын муктаждыктарын канааттандыруу үчүн иштелип чыккан үч ар түрдүү лицензиялык параметрлери боюнча жеткиликтүү.&lt;/p&gt; Биздин коммерциялык лицензиялык келишим боюнча лицензияланган QT сиз үчүнчү жактар менен кандайдыр бир булак кодун бөлүшкүңөр келбеген же башка жол менен GNU LGPL версиясынын 2.1 же GNU GPL версиясынын 3.0 шарттарын аткара албаган жекече/ коммерциялык программалык камсыздоону иштеп чыгуу үчүн ылайыктуу.&lt;/p&gt; &lt;p&gt;GNU LGPL версиясы боюнча лицензияланган QT 2.1 GNU LGPL версиясынын шарттарын жана шарттарын аткара алсаңар, QT тиркемелерин (жекече же ачык булак) иштеп чыгуу үчүн ылайыктуу.&lt;/p&gt; &lt;p&gt;GNU General Public License 3.0 версиясы боюнча лицензияланган QT QT тиркемелерин иштеп чыгуу үчүн ылайыктуу, анда сиз мындай иштемелерди GNU GPL версиясынын 3.0 шарттарына ылайык программалык камсыздоо менен айкалыштыруу менен колдонууну каалайсыз же GNU GPL версиясынын 3.0 шарттарын аткарууга даяр болгон жерде.&lt;/p&gt; &lt;p&gt;Qt лицензиясы жөнүндө жалпы маалымат алуу үчүн &lt;a href=&quot;http://qt.nokia.com/products/licensing&quot;&gt;qt.nokia.com/products/licensing&lt;/a&gt;&lt;/p&gt; караңыз. &lt;p&gt;Автордук укук (С) 2012 Nokia Корпорациясы жана/же анын филиалы (-ies).&lt;/p&gt; &lt;p&gt;Qt — Nokia продукту. Көбүрөөк маалымат алуу үчүн &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt;&lt;/p&gt; караңыз.</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;%1&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt for Embedded Linux and Qt for Windows CE.&lt;/p&gt;&lt;p&gt;Qt is a Nokia product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokiae.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;h3&gt;关于Qt&lt;/h3&gt;%1&lt;p&gt;Qt是一个用于跨平台应用程序开发的C++工具包。&lt;/p&gt;&lt;p&gt;对于MS&amp;nbsp;Windows、Mac&amp;nbsp;OS&amp;nbsp;X、Linux和所有主流商业Unix，Qt提供了单一源程序的可移植性。Qt也有用于嵌入式Linux和Windows CE的版本。&lt;/p&gt;&lt;p&gt;Qt是Nokia的产品。有关更多信息，请参考&lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt;。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;This program uses Qt Open Source Edition version %1.&lt;/p&gt;&lt;p&gt;Qt Open Source Edition is intended for the development of Open Source applications. You need a commercial Qt license for development of proprietary (closed source) applications.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://qt.nokia.com/company/model/&quot;&gt;qt.nokia.com/company/model/&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;这个程序使用了Qt %1开源版本。&lt;/p&gt;&lt;p&gt;Qt开源版本只用于开源应用程序的开发。如果要开发私有（闭源）软件，你需要一个商业的Qt协议。&lt;/p&gt;&lt;p&gt;有关Qt协议的概览，请参考&lt;a href=&quot;http://qt.nokia.com/company/model/&quot;&gt;qt.nokia.com/company/model/&lt;/a&gt;。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;%1&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt Embedded.&lt;/p&gt;&lt;p&gt;Qt is a Trolltech product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;h3&gt;关于Qt&lt;/h3&gt;%1&lt;p&gt;Qt是一个用于跨平台应用程序开发的C++工具包。&lt;/p&gt;&lt;p&gt;对于MS&amp;nbsp;Windows、Mac&amp;nbsp;OS&amp;nbsp;X、Linux和所有主流商业Unix，Qt提供了单一源程序的可移植性。Qt对于嵌入式平台也是可用的，在嵌入式平台上它被称为Qt Embedded。&lt;/p&gt;&lt;p&gt;Qt是Trolltech的产品。有关更多信息，请参考&lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt;。&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>QMultiInputContext</name>
    <message>
        <location filename="../src/plugins/inputmethods/imsw-multi/qmultiinputcontext.cpp" line="+88"/>
        <source>Select IM</source>
        <translation>IM тандоо</translation>
    </message>
</context>
<context>
    <name>QMultiInputContextPlugin</name>
    <message>
        <location filename="../src/plugins/inputmethods/imsw-multi/qmultiinputcontextplugin.cpp" line="+95"/>
        <source>Multiple input method switcher</source>
        <translation>Бир нече киргизүү ыкмасы тандагыч</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Multiple input method switcher that uses the context menu of the text widgets</source>
        <translation>Тексттик виджеттердин контекст менюсун колдонгон бир нече киргизүү ыкмасы которуучу</translation>
    </message>
</context>
<context>
    <name>QNativeSocketEngine</name>
    <message>
        <location filename="../src/network/socket/qnativesocketengine.cpp" line="+206"/>
        <source>The remote host closed the connection</source>
        <translation>Алыскы хост байланышты жапты</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Network operation timed out</source>
        <translation>Тармактын иштеши убакыттын өтүшү менен</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Out of resources</source>
        <translation>Ресурстардан чыгып</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unsupported socket operation</source>
        <translation>Кабарланбаган розетка операциясы</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Protocol type not supported</source>
        <translation>Протокол түрү колдолбойт</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid socket descriptor</source>
        <translation>Жараксыз розетка дескриптору</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Network unreachable</source>
        <translation>Тармак жеткиликтүү эмес</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Permission denied</source>
        <translation>Уруксат берилди</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection timed out</source>
        <translation>Туташуу убакыттын өтүшү менен</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection refused</source>
        <translation>Туташуудан баш тартты</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The bound address is already in use</source>
        <translation>Чектелген дарек колдонулуп жатат</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The address is not available</source>
        <translation>Дареги жеткиликтүү эмес</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The address is protected</source>
        <translation>Дареги корголгон</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to send a message</source>
        <translation>Кабар жөнөтүү мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to receive a message</source>
        <translation>Кабар ала албай калды</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to write</source>
        <translation>Жазууга мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Network error</source>
        <translation>Тармактын катасы</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Another socket is already listening on the same port</source>
        <translation>Дагы бир розетка буга чейин ошол эле портунда угуп жатат</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>Unable to initialize non-blocking socket</source>
        <translation>Блоктоосуз розетканы иницалдаштыруу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to initialize broadcast socket</source>
        <translation>Трансляциялык розетканы иниализациялоо мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Attempt to use IPv6 socket on a platform with no IPv6 support</source>
        <translation>IPv6 колдоосу жок платформада IPv6 розеткасын колдонуу аракети</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Host unreachable</source>
        <translation>Хост жеткиликтүү эмес</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Datagram was too large to send</source>
        <translation>Datagram жөнөтүү үчүн өтө чоң болгон</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Operation on non-socket</source>
        <translation>Сокет эмес боюнча операция</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unknown error</source>
        <translation>Белгисиз ката</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>The proxy type is invalid for this operation</source>
        <translation>Прокси түрү бул операция үчүн жараксыз</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessCacheBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccesscachebackend.cpp" line="+65"/>
        <source>Error opening %1</source>
        <translation>%1 ачуу катасы</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessFileBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccessfilebackend.cpp" line="+99"/>
        <source>Request for opening non-local file %1</source>
        <translation>%1 жергиликтүү эмес файлды ачуу жөнүндө өтүнүч</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Error opening %1: %2</source>
        <translation>%1 ачуу катасы: %2</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Write error writing to %1: %2</source>
        <translation>%1: %2 жазуу катасын жазыңыз</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Cannot open %1: Path is a directory</source>
        <translation>%1 ачууга болбоду: Жол – каталог</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Read error reading from %1: %2</source>
        <translation>%1:2-ден ката окууну окугула</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessFtpBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccessftpbackend.cpp" line="+165"/>
        <source>No suitable proxy found</source>
        <translation>Ылайыктуу прокси табылган жок</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Cannot open %1: is a directory</source>
        <translation>%1 ачууга болбоду: каталог болуп саналат</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>Logging in to %1 failed: authentication required</source>
        <translation>%1-ге кирүү ишке ашпады: аутентификация талап кылынат</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Error while downloading %1: %2</source>
        <translation>%1 жүктөп жатканда ката: %2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error while uploading %1: %2</source>
        <translation>%1 жүктөп жатканда ката: %2</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessHttpBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccesshttpbackend.cpp" line="+597"/>
        <source>No suitable proxy found</source>
        <translation>Ылайыктуу прокси табылган жок</translation>
    </message>
</context>
<context>
    <name>QNetworkReply</name>
    <message>
        <location line="+128"/>
        <source>Error downloading %1 - server replied: %2</source>
        <translation>%1 жүктөө катасы - сервер мындай деп жооп берди: %2</translation>
    </message>
    <message>
        <location filename="../src/network/access/qnetworkreplyimpl.cpp" line="+68"/>
        <source>Protocol &quot;%1&quot; is unknown</source>
        <translation>&quot;%1&quot; протоколу белгисиз</translation>
    </message>
</context>
<context>
    <name>QNetworkReplyImpl</name>
    <message>
        <location line="+432"/>
        <location line="+22"/>
        <source>Operation canceled</source>
        <translation>Операция жокко чыгарылган</translation>
    </message>
</context>
<context>
    <name>QOCIDriver</name>
    <message>
        <location filename="../src/sql/drivers/oci/qsql_oci.cpp" line="+2069"/>
        <source>Unable to logon</source>
        <translation>Кирүүгө мүмкүн эмес</translation>
    </message>
    <message>
        <location line="-144"/>
        <source>Unable to initialize</source>
        <comment>QOCIDriver</comment>
        <translation>Иниализациялоо мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+215"/>
        <source>Unable to begin transaction</source>
        <translation>Транзакцияны баштоо мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to commit transaction</source>
        <translation>Бүтүмдөрдү ишке ашыруу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to rollback transaction</source>
        <translation>Транзакцияны кайтарууга мүмкүн эмес</translation>
    </message>
</context>
<context>
    <name>QOCIResult</name>
    <message>
        <location line="-963"/>
        <location line="+161"/>
        <location line="+15"/>
        <source>Unable to bind column for batch execute</source>
        <translation>Топту аткаруу үчүн колоннаны байлоо мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to execute batch statement</source>
        <translation>Партиялык билдирүүнү аткарууга мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+302"/>
        <source>Unable to goto next</source>
        <translation>Кийинки гото мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Unable to alloc statement</source>
        <translation>Билдирүүнү аллокациялоо мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to prepare statement</source>
        <translation>Билдирүү даярдай албай калды</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Unable to bind value</source>
        <translation>Маанини байлоо мүмкүн эмес</translation>
    </message>
    <message>
        <source>Unable to execute select statement</source>
        <translation type="obsolete">不能执行选择语句</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to execute statement</source>
        <translation>Билдирүүнү аткарууга мүмкүн эмес</translation>
    </message>
</context>
<context>
    <name>QODBCDriver</name>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="+1785"/>
        <source>Unable to connect</source>
        <translation>Туташуу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to connect - Driver doesn&apos;t support all needed functionality</source>
        <translation>Туташуу мүмкүн эмес - Драйвер бардык керектүү функционалдуулугун колдобойт</translation>
    </message>
    <message>
        <location line="+242"/>
        <source>Unable to disable autocommit</source>
        <translation>Автокоммуникацияны өчүрүү мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to commit transaction</source>
        <translation>Бүтүмдөрдү ишке ашыруу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback transaction</source>
        <translation>Транзакцияны кайтарууга мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to enable autocommit</source>
        <translation>Автокоммуникацияга мүмкүндүк берүү мүмкүн эмес</translation>
    </message>
</context>
<context>
    <name>QODBCResult</name>
    <message>
        <location line="-1218"/>
        <location line="+349"/>
        <source>QODBCResult::reset: Unable to set &apos;SQL_CURSOR_STATIC&apos; as statement attribute. Please check your ODBC driver configuration</source>
        <translation>QODBCResult::Калыбына келтирүү: Билдирүү атрибуты катары «SQL_CURSOR_STATIC» орното алган жок Сураныч, сиздин ODBC драйвер конфигурациясын текшерүү</translation>
    </message>
    <message>
        <location line="-332"/>
        <location line="+626"/>
        <source>Unable to execute statement</source>
        <translation>Билдирүүнү аткарууга мүмкүн эмес</translation>
    </message>
    <message>
        <location line="-555"/>
        <source>Unable to fetch next</source>
        <translation>Кийинки алып келүүгө мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+279"/>
        <source>Unable to prepare statement</source>
        <translation>Билдирүү даярдай албай калды</translation>
    </message>
    <message>
        <location line="+268"/>
        <source>Unable to bind variable</source>
        <translation>Өзгөргүчтү байлоо мүмкүн эмес</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="+194"/>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="-475"/>
        <location line="+578"/>
        <source>Unable to fetch last</source>
        <translation>Акыркы алып келүүгө мүмкүн эмес</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="-672"/>
        <source>Unable to fetch</source>
        <translation>Алып келүүгө мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Unable to fetch first</source>
        <translation>Адегенде алып келүүгө мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to fetch previous</source>
        <translation>Мурунку алып келүүгө мүмкүн эмес</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/gui/util/qdesktopservices_mac.cpp" line="+165"/>
        <source>Home</source>
        <translation>Үй</translation>
    </message>
    <message>
        <location filename="../src/network/access/qnetworkaccessdatabackend.cpp" line="+74"/>
        <source>Operation not supported on %1</source>
        <translation>%1 боюнча операция колдолбогон</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Invalid URI: %1</source>
        <translation>Жараксыз URI: %1</translation>
    </message>
    <message>
        <location filename="../src/network/access/qnetworkaccessdebugpipebackend.cpp" line="+175"/>
        <source>Write error writing to %1: %2</source>
        <translation>%1: %2 жазуу катасын жазыңыз</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Read error reading from %1: %2</source>
        <translation>%1:2-ден ката окууну окугула</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Socket error on %1: %2</source>
        <translation>%1 боюнча сокет катасы: %2</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Remote host closed the connection prematurely on %1</source>
        <translation>Алыскы хост %1 боюнча байланышты мөөнөтүнөн мурда жапты</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Protocol error: packet of size 0 received</source>
        <translation>Протоколдун катасы: 0 өлчөмүнүн пакети алынды</translation>
    </message>
    <message>
        <location filename="../src/network/kernel/qhostinfo.cpp" line="+177"/>
        <location line="+57"/>
        <source>No host name given</source>
        <translation>Эч кандай хост аты берилген</translation>
    </message>
</context>
<context>
    <name>QPPDOptionsModel</name>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="+1195"/>
        <source>Name</source>
        <translation>Аты-жөнү</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Value</source>
        <translation>Баалуулук</translation>
    </message>
</context>
<context>
    <name>QPSQLDriver</name>
    <message>
        <location filename="../src/sql/drivers/psql/qsql_psql.cpp" line="+763"/>
        <source>Unable to connect</source>
        <translation>Туташуу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Could not begin transaction</source>
        <translation>Транзакцияны баштоо мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Could not commit transaction</source>
        <translation>Транзакцияны жүргүзүүгө болбоду</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Could not rollback transaction</source>
        <translation>Транзакцияны кайтаруу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+358"/>
        <source>Unable to subscribe</source>
        <translation>Жазылуу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Unable to unsubscribe</source>
        <translation>Жазылуу мүмкүн эмес</translation>
    </message>
</context>
<context>
    <name>QPSQLResult</name>
    <message>
        <location line="-1058"/>
        <source>Unable to create query</source>
        <translation>Суроо түзүүгө мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+374"/>
        <source>Unable to prepare statement</source>
        <translation>Билдирүү даярдай албай калды</translation>
    </message>
</context>
<context>
    <name>QPageSetupWidget</name>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupdialog_unix.cpp" line="+304"/>
        <source>Centimeters (cm)</source>
        <translation>Сентиметрлер (см)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Millimeters (mm)</source>
        <translation>Миллиметрлер (мм)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Inches (in)</source>
        <translation>Дюйм (ин)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Points (pt)</source>
        <translation>Пункттар (ПТ)</translation>
    </message>
    <message>
        <source>Pica (P̸)</source>
        <translation>Пика (P̸)</translation>
    </message>
    <message>
        <source>Didot (DD)</source>
        <translation>Дидот (DD)</translation>
    </message>
    <message>
        <source>Cicero (CC)</source>
        <translation>Сицеро (КК)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupwidget.ui"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location/>
        <source>Paper</source>
        <translation>Кагаз</translation>
    </message>
    <message>
        <location/>
        <source>Page size:</source>
        <translation>Бет өлчөмү:</translation>
    </message>
    <message>
        <location/>
        <source>Width:</source>
        <translation>Туурасы:</translation>
    </message>
    <message>
        <location/>
        <source>Height:</source>
        <translation>Бийиктиги:</translation>
    </message>
    <message>
        <location/>
        <source>Paper source:</source>
        <translation>Кагаз булагы:</translation>
    </message>
    <message>
        <location/>
        <source>Orientation</source>
        <translation>Багыты</translation>
    </message>
    <message>
        <location/>
        <source>Portrait</source>
        <translation>Портрет</translation>
    </message>
    <message>
        <location/>
        <source>Landscape</source>
        <translation>Пейзаж</translation>
    </message>
    <message>
        <location/>
        <source>Reverse landscape</source>
        <translation>Кайтарым пейзаж</translation>
    </message>
    <message>
        <location/>
        <source>Reverse portrait</source>
        <translation>Кайтарым портрет</translation>
    </message>
    <message>
        <location/>
        <source>Margins</source>
        <translation>Маржалар</translation>
    </message>
    <message>
        <location/>
        <source>top margin</source>
        <translation>жогорку маржа</translation>
    </message>
    <message>
        <location/>
        <source>left margin</source>
        <translation>сол маржа</translation>
    </message>
    <message>
        <location/>
        <source>right margin</source>
        <translation>оң маржа</translation>
    </message>
    <message>
        <location/>
        <source>bottom margin</source>
        <translation>төмөнкү маржа</translation>
    </message>
    <message>
        <location/>
        <source>Page Layout</source>
        <translation>Бетти жайгаштыруу</translation>
    </message>
    <message>
        <location/>
        <source>Pages per sheet:</source>
        <translation>Баракчага баракчалар:</translation>
    </message>
    <message>
        <location/>
        <source>Page order:</source>
        <translation>Бет ордени:</translation>
    </message>
</context>
<context>
    <name>QCupsJobWidget</name>
    <message>
        <location/>
        <source>Job Control</source>
        <translation>Аюбду көзөмөлдөө</translation>
    </message>
    <message>
        <location/>
        <source>Scheduled printing:</source>
        <translation>Пландаштырылган басып чыгаруу:</translation>
    </message>
    <message>
        <location/>
        <source>Print Immediately</source>
        <translation>Дароо басып чыгаруу</translation>
    </message>
    <message>
        <location/>
        <source>Hold Indefinitely</source>
        <translation>Түбөлүккө кармагыла</translation>
    </message>
    <message>
        <location/>
        <source>Day (06:00 to 17:59)</source>
        <translation>Күнү (06:00 үчүн 17:59)</translation>
    </message>
    <message>
        <location/>
        <source>Night (18:00 to 05:59)</source>
        <translation>Түнү (18:00 үчүн 05:59)</translation>
    </message>
    <message>
        <location/>
        <source>Second Shift (16:00 to 23:59)</source>
        <translation>Экинчи кезеги (16:00 үчүн 23:59)</translation>
    </message>
    <message>
        <location/>
        <source>Third Shift (00:00 to 07:59)</source>
        <translation>Үчүнчү кезеги (00:00 үчүн 07:59)</translation>
    </message>
    <message>
        <location/>
        <source>Weekend (Saturday to Sunday)</source>
        <translation>Дем алыш күндөрү (ишембиден жекшембиге чейин)</translation>
    </message>
    <message>
        <location/>
        <source>Specific Time</source>
        <translation>Белгилүү бир убакыт</translation>
    </message>
    <message>
        <location/>
        <source>Billing information:</source>
        <translation>Биллинг маалыматы:</translation>
    </message>
    <message>
        <location/>
        <source>Job priority:</source>
        <translation>Аюб приоритети:</translation>
    </message>
    <message>
        <location/>
        <source>Banner Pages</source>
        <translation>Баннер барактары</translation>
    </message>
    <message>
        <location/>
        <source>Start:</source>
        <translation>Баштоо:</translation>
    </message>
    <message>
        <location/>
        <source>None</source>
        <translation>Жок</translation>
    </message>
    <message>
        <location/>
        <source>Standard</source>
        <translation>Стандарт</translation>
    </message>
    <message>
        <location/>
        <source>Unclassified</source>
        <translation>Классификацияланбаган</translation>
    </message>
    <message>
        <location/>
        <source>Confidential</source>
        <translation>Жашыруун</translation>
    </message>
    <message>
        <location/>
        <source>Classified</source>
        <translation>Классификацияланган</translation>
    </message>
    <message>
        <location/>
        <source>Secret</source>
        <translation>Сыры</translation>
    </message>
    <message>
        <location/>
        <source>Top Secret</source>
        <translation>Башкы сыр</translation>
    </message>
    <message>
        <location/>
        <source>End:</source>
        <translation>Аягы:</translation>
    </message>
</context>
<context>
    <name>QPluginLoader</name>
    <message>
        <location filename="../src/corelib/plugin/qpluginloader.cpp" line="+24"/>
        <source>Unknown error</source>
        <translation>Белгисиз ката</translation>
    </message>
    <message>
        <location line="-68"/>
        <source>The plugin was not loaded.</source>
        <translation>Плагин жүктөлгөн жок.</translation>
    </message>
</context>
<context>
    <name>QPrintDialog</name>
    <message>
        <location filename="../src/gui/painting/qprinterinfo_unix.cpp" line="+98"/>
        <source>locally connected</source>
        <translation>жергиликтүү туташтырылган</translation>
    </message>
    <message>
        <location line="+23"/>
        <location line="+225"/>
        <source>Aliases: %1</source>
        <translation>Алиасес: %1</translation>
    </message>
    <message>
        <location line="+223"/>
        <location line="+199"/>
        <source>unknown</source>
        <translation>белгисиз</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_qws.cpp" line="+375"/>
        <source>Print all</source>
        <translation>Бардыгын басып чыгаруу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print selection</source>
        <translation>Басып чыгаруу тандоо</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print range</source>
        <translation>Басып чыгаруу диапазону</translation>
    </message>
    <message>
        <location line="-48"/>
        <source>A0 (841 x 1189 mm)</source>
        <translation>А0 (841 × 1189 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A1 (594 x 841 mm)</source>
        <translation>А1 (594 × 841 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A2 (420 x 594 mm)</source>
        <translation>А2 (420 × 594 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A3 (297 x 420 mm)</source>
        <translation>А3 (297 × 420 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A4 (210 x 297 mm, 8.26 x 11.7 inches)</source>
        <translation>А4 (210 × 297 мм, 8.26 × 11,7 дюйм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A5 (148 x 210 mm)</source>
        <translation>А5 (148 × 210 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A6 (105 x 148 mm)</source>
        <translation>А6 (105 × 148 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A7 (74 x 105 mm)</source>
        <translation>А7 (74 × 105 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A8 (52 x 74 mm)</source>
        <translation>А8 (52 × 74 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A9 (37 x 52 mm)</source>
        <translation>А9 (37 × 52 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B0 (1000 x 1414 mm)</source>
        <translation>Б0 (1000 × 1414 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B1 (707 x 1000 mm)</source>
        <translation>В1 (707 × 1000 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B2 (500 x 707 mm)</source>
        <translation>В2 (500 × 707 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B3 (353 x 500 mm)</source>
        <translation>В3 (353 × 500 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B4 (250 x 353 mm)</source>
        <translation>В4 (250 × 353 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B5 (176 x 250 mm, 6.93 x 9.84 inches)</source>
        <translation>В5 (176 × 250 мм, 6.93 × 9,84 дюйм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B6 (125 x 176 mm)</source>
        <translation>В6 (125 × 176 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B7 (88 x 125 mm)</source>
        <translation>В7 (88 × 125 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B8 (62 x 88 mm)</source>
        <translation>В8 (62 × 88 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B9 (44 x 62 mm)</source>
        <translation>В9 (44 × 62 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B10 (31 x 44 mm)</source>
        <translation>В10 (31 × 44 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>C5E (163 x 229 mm)</source>
        <translation>С5Е (163 × 229 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>DLE (110 x 220 mm)</source>
        <translation>DLE (110 × 220 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Executive (7.5 x 10 inches, 191 x 254 mm)</source>
        <translation>Аткаруучу (7,5 × 10 дюйм, 191 × 254 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Folio (210 x 330 mm)</source>
        <translation>Фолио (210 × 330 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ledger (432 x 279 mm)</source>
        <translation>Леджер (432 × 279 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Legal (8.5 x 14 inches, 216 x 356 mm)</source>
        <translation>Юридикалык (8,5 × 14 дюйм, 216 × 356 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Letter (8.5 x 11 inches, 216 x 279 mm)</source>
        <translation>Кат (8,5 × 11 дюйм, 216 × 279 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tabloid (279 x 432 mm)</source>
        <translation>Таблоид (279 × 432 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>US Common #10 Envelope (105 x 241 mm)</source>
        <translation>US Common #10 Конверт (105 × 241 мм)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_win.cpp" line="+268"/>
        <source>OK</source>
        <translation>Макул</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qabstractprintdialog.cpp" line="+110"/>
        <location line="+13"/>
        <location filename="../src/gui/dialogs/qprintdialog_win.cpp" line="-2"/>
        <source>Print</source>
        <translation>Басып чыгаруу</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="-357"/>
        <source>Print To File ...</source>
        <translation>Файлга басып чыгаруу ...</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>File %1 is not writable.
Please choose a different file name.</source>
        <translation>%1 файлы жазылбайт.
Файлдын башка атын тандаңыз.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1 already exists.
Do you want to overwrite it?</source>
        <translation>%1 буга чейин бар.
Сиз аны ашыкча жазууну каалайсызбы?</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_qws.cpp" line="-210"/>
        <source>File exists</source>
        <translation>Файл бар</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;qt&gt;Do you want to overwrite it?&lt;/qt&gt;</source>
        <translation>&lt;qt&gt; Сиз аны ашыкча жазууну каалайсызбы? &lt;/qt&gt;</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="-8"/>
        <source>%1 is a directory.
Please choose a different file name.</source>
        <translation>%1 – каталог.
Файлдын башка атын тандаңыз.</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_win.cpp" line="+1"/>
        <source>The &apos;From&apos; value cannot be greater than the &apos;To&apos; value.</source>
        <translation>«Андан» наркы «То» наркынан жогору боло албайт.</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupdialog_unix.cpp" line="-232"/>
        <source>A0</source>
        <translation>А0</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A1</source>
        <translation>А1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A2</source>
        <translation>А2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A3</source>
        <translation>А3</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A4</source>
        <translation>А4</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A5</source>
        <translation>А5</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A6</source>
        <translation>А6</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A7</source>
        <translation>А7</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A8</source>
        <translation>А8</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A9</source>
        <translation>А9</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B0</source>
        <translation>Б0</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B1</source>
        <translation>В1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B2</source>
        <translation>В2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B3</source>
        <translation>В3</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B4</source>
        <translation>В4</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B5</source>
        <translation>В5</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B6</source>
        <translation>В6</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B7</source>
        <translation>В7</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B8</source>
        <translation>В8</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B9</source>
        <translation>Б9</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B10</source>
        <translation>В10</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>C5E</source>
        <translation>С5Е</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>DLE</source>
        <translation>DLE</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Executive</source>
        <translation>Аткаруучу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Folio</source>
        <translation>Фолио</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ledger</source>
        <translation>Леджер</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Legal</source>
        <translation>Мыйзамдуу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Letter</source>
        <translation>Кат</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tabloid</source>
        <translation>Таблоид</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>US Common #10 Envelope</source>
        <translation>US Common #10 Конверт</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Folio (8.27 x 13 in)</source>
        <translation>Фолио (8.27 × 13 в)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Custom</source>
        <translation>Салт</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="-522"/>
        <location line="+68"/>
        <source>&amp;Options &gt;&gt;</source>
        <translation>Опциялар &gt;&gt;</translation>
    </message>
    <message>
        <location line="-63"/>
        <source>&amp;Print</source>
        <translation>Басып чыгаруу</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>&amp;Options &lt;&lt;</source>
        <translation>&amp;amp, параметрлер &lt;&lt;</translation>
    </message>
    <message>
        <location line="+253"/>
        <source>Print to File (PDF)</source>
        <translation>Файлга басып чыгаруу (PDF)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print to File (Postscript)</source>
        <translation>Файлга басып чыгаруу (Постскрипт)</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Local file</source>
        <translation>Жергиликтүү файл</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Write %1 file</source>
        <translation>%1 файлын жазуу</translation>
    </message>
</context>
<context>
    <name>QPrintPreviewDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qabstractpagesetupdialog.cpp" line="+68"/>
        <location line="+12"/>
        <source>Page Setup</source>
        <translation>Бетти орнотуу</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintpreviewdialog.cpp" line="+252"/>
        <source>%1%</source>
        <translation>%1%</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Print Preview</source>
        <translation>Алдын ала көрүү басып чыгаруу</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Next page</source>
        <translation>Кийинки бет</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous page</source>
        <translation>Мурунку бет</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>First page</source>
        <translation>Биринчи бет</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last page</source>
        <translation>Акыркы бет</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Fit width</source>
        <translation>Туура туурасы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fit page</source>
        <translation>Туура бет</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Zoom in</source>
        <translation>Чоңойтуу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom out</source>
        <translation>Чоңойтуу</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Portrait</source>
        <translation>Портрет</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Landscape</source>
        <translation>Пейзаж</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Show single page</source>
        <translation>Бир бетти көрсөтүү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show facing pages</source>
        <translation>Бетме - беттерди көрсөтүү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show overview of all pages</source>
        <translation>Бардык барактарга жалпы маалымат көрсөтүү</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Print</source>
        <translation>Басып чыгаруу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page setup</source>
        <translation>Бетти орнотуу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
    <message>
        <location line="+151"/>
        <source>Export to PDF</source>
        <translation>ПДФга экспорттоо</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Export to PostScript</source>
        <translation>Постскриптке экспорттоо</translation>
    </message>
</context>
<context>
    <name>QPrintPropertiesWidget</name>
    <message>
        <location filename="../src/gui/dialogs/qprintpropertieswidget.ui"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location/>
        <source>Page</source>
        <translation>Бет</translation>
    </message>
    <message>
        <location/>
        <source>Advanced</source>
        <translation>Өнүккөн</translation>
    </message>
</context>
<context>
    <name>QPrintSettingsOutput</name>
    <message>
        <location filename="../src/gui/dialogs/qprintsettingsoutput.ui"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location/>
        <source>Copies</source>
        <translation>Көчүрмөлөр</translation>
    </message>
    <message>
        <location/>
        <source>Print range</source>
        <translation>Басып чыгаруу диапазону</translation>
    </message>
    <message>
        <location/>
        <source>Print all</source>
        <translation>Бардыгын басып чыгаруу</translation>
    </message>
    <message>
        <location/>
        <source>Pages from</source>
        <translation>Барактар</translation>
    </message>
    <message>
        <location/>
        <source>to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Pages</source>
        <translation>Барактар</translation>
    </message>
    <message>
        <location/>
        <source>Page Set:</source>
        <translation>Бет комплект:</translation>
    </message>
    <message>
        <location/>
        <source>Selection</source>
        <translation>Тандоо</translation>
    </message>
    <message>
        <location/>
        <source>Output Settings</source>
        <translation>Чыгуу параметрлери</translation>
    </message>
    <message>
        <location/>
        <source>Copies:</source>
        <translation>Көчүрмөлөрү:</translation>
    </message>
    <message>
        <location/>
        <source>Collate</source>
        <translation>Коллате</translation>
    </message>
    <message>
        <location/>
        <source>Reverse</source>
        <translation>Кайтарым</translation>
    </message>
    <message>
        <location/>
        <source>Options</source>
        <translation>Параметрлер</translation>
    </message>
    <message>
        <location/>
        <source>Color Mode</source>
        <translation>Түс режими</translation>
    </message>
    <message>
        <location/>
        <source>Color</source>
        <translation>Түс</translation>
    </message>
    <message>
        <location/>
        <source>Grayscale</source>
        <translation>Грэйскаль</translation>
    </message>
    <message>
        <location/>
        <source>Duplex Printing</source>
        <translation>Дуплекс басып чыгаруу</translation>
    </message>
    <message>
        <location/>
        <source>None</source>
        <translation>Жок</translation>
    </message>
    <message>
        <location/>
        <source>Long side</source>
        <translation>Узун жагы</translation>
    </message>
    <message>
        <location/>
        <source>Short side</source>
        <translation>Кыска жагы</translation>
    </message>
</context>
<context>
    <name>QPrintWidget</name>
    <message>
        <location filename="../src/gui/dialogs/qprintwidget.ui"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location/>
        <source>Printer</source>
        <translation>Принтер</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Name:</source>
        <translation>&amp;amp:</translation>
    </message>
    <message>
        <location/>
        <source>P&amp;roperties</source>
        <translation>П &amp;amp, роперттер</translation>
    </message>
    <message>
        <location/>
        <source>Location:</source>
        <translation>Жайгашкан жери:</translation>
    </message>
    <message>
        <location/>
        <source>Preview</source>
        <translation>Алдын ала көрүү</translation>
    </message>
    <message>
        <location/>
        <source>Type:</source>
        <translation>Түрү:</translation>
    </message>
    <message>
        <location/>
        <source>Output &amp;file:</source>
        <translation>Чыгаруу &amp; файл:</translation>
    </message>
    <message>
        <location/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>QProcess</name>
    <message>
        <location filename="../src/corelib/io/qprocess_unix.cpp" line="+475"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+147"/>
        <source>Could not open input redirection for reading</source>
        <translation>Окуу үчүн кирүү дисциплинасын ачууга болбоду</translation>
    </message>
    <message>
        <location line="+12"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+36"/>
        <source>Could not open output redirection for writing</source>
        <translation>Жазуу үчүн чыгууга кайра дисциплина ачууга болбоду</translation>
    </message>
    <message>
        <location line="+235"/>
        <source>Resource error (fork failure): %1</source>
        <translation>Ресурстук ката (шакектин жетишсиздиги): %1</translation>
    </message>
    <message>
        <location line="+259"/>
        <location line="+53"/>
        <location line="+74"/>
        <location line="+67"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+422"/>
        <location line="+50"/>
        <location line="+75"/>
        <location line="+42"/>
        <location line="+54"/>
        <source>Process operation timed out</source>
        <translation>Процесстин иштөө убактысы аяктады</translation>
    </message>
    <message>
        <location filename="../src/corelib/io/qprocess.cpp" line="+533"/>
        <location line="+52"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="-211"/>
        <location line="+50"/>
        <source>Error reading from process</source>
        <translation>Процесстен ката окуу</translation>
    </message>
    <message>
        <location line="+47"/>
        <location line="+779"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+140"/>
        <source>Error writing to process</source>
        <translation>Иштетүү үчүн ката жазуу</translation>
    </message>
    <message>
        <location line="-709"/>
        <source>Process crashed</source>
        <translation>Процесс кыйроого учурады</translation>
    </message>
    <message>
        <location line="+912"/>
        <source>No program defined</source>
        <translation>Программа аныкталган жок</translation>
    </message>
    <message>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="-341"/>
        <source>Process failed to start</source>
        <translation>Процесс баштала алган жок</translation>
    </message>
</context>
<context>
    <name>QProgressDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qprogressdialog.cpp" line="+182"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
</context>
<context>
    <name>QPushButton</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="-8"/>
        <source>Open</source>
        <translation>Ачуу</translation>
    </message>
</context>
<context>
    <name>QRadioButton</name>
    <message>
        <location line="+12"/>
        <source>Check</source>
        <translation>Текшерүү</translation>
    </message>
</context>
<context>
    <name>QRegExp</name>
    <message>
        <location filename="../src/corelib/tools/qregexp.cpp" line="+64"/>
        <source>no error occurred</source>
        <translation>ката болгон жок</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>disabled feature used</source>
        <translation>колдонулган майыптар өзгөчөлүгү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad char class syntax</source>
        <translation>жаман шар класс синтакс</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad lookahead syntax</source>
        <translation>жаман карап баш синтакс</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad repetition syntax</source>
        <translation>жаман кайталоо синтакс</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid octal value</source>
        <translation>жараксыз окталдык маани</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>missing left delim</source>
        <translation>жок сол делим</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected end</source>
        <translation>күтүлбөгөн аягы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>met internal limit</source>
        <translation>ички чектөөгө ылайык</translation>
    </message>
</context>
<context>
    <name>QSQLite2Driver</name>
    <message>
        <location filename="../src/sql/drivers/sqlite2/qsql_sqlite2.cpp" line="+396"/>
        <source>Error to open database</source>
        <translation>Маалымат базасын ачуу катасы</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Unable to begin transaction</source>
        <translation>Транзакцияны баштоо мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to commit transaction</source>
        <translation>Бүтүмдөрдү ишке ашыруу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback Transaction</source>
        <translation>Транзакцияны кайтарууга мүмкүн эмес</translation>
    </message>
</context>
<context>
    <name>QSQLite2Result</name>
    <message>
        <location line="-323"/>
        <source>Unable to fetch results</source>
        <translation>Натыйжаларды алып келүүгө мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>Unable to execute statement</source>
        <translation>Билдирүүнү аткарууга мүмкүн эмес</translation>
    </message>
</context>
<context>
    <name>QSQLiteDriver</name>
    <message>
        <location filename="../src/sql/drivers/sqlite/qsql_sqlite.cpp" line="+528"/>
        <source>Error opening database</source>
        <translation>Маалымат базасын ачуу катасы</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error closing database</source>
        <translation>Маалымат базасын жабуу катасы</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to begin transaction</source>
        <translation>Транзакцияны баштоо мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to commit transaction</source>
        <translation>Бүтүмдөрдү ишке ашыруу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to rollback transaction</source>
        <translation>Транзакцияны кайтарууга мүмкүн эмес</translation>
    </message>
</context>
<context>
    <name>QSQLiteResult</name>
    <message>
        <location line="-400"/>
        <location line="+66"/>
        <location line="+8"/>
        <source>Unable to fetch row</source>
        <translation>Катар алып келүүгө мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Unable to execute statement</source>
        <translation>Билдирүүнү аткарууга мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to reset statement</source>
        <translation>Билдирүүнү калыбына келтирүү мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Unable to bind parameters</source>
        <translation>Параметрлерди байлай албай калды</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Parameter count mismatch</source>
        <translation>Параметрди эсептөө дал келбестик</translation>
    </message>
    <message>
        <location line="-208"/>
        <source>No query</source>
        <translation>Суроо жок</translation>
    </message>
</context>
<context>
    <name>QScrollBar</name>
    <message>
        <location filename="../src/gui/widgets/qscrollbar.cpp" line="+448"/>
        <source>Scroll here</source>
        <translation>Бул жерде жылдыруу</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Left edge</source>
        <translation>Сол чети</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Top</source>
        <translation>Жогорку</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right edge</source>
        <translation>Оң чети</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Bottom</source>
        <translation>Төмөнкү</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Page left</source>
        <translation>Калтырылган бет</translation>
    </message>
    <message>
        <location line="+0"/>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+143"/>
        <source>Page up</source>
        <translation>Бетти жогорулатуу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page right</source>
        <translation>Бет оңдо</translation>
    </message>
    <message>
        <location line="+0"/>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+4"/>
        <source>Page down</source>
        <translation>Бетти төмөндөтүү</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scroll left</source>
        <translation>Жылдыруу калды</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll up</source>
        <translation>Жылдыруу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll right</source>
        <translation>Оңго жылдыруу</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll down</source>
        <translation>Ылдый жылдыруу</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="-6"/>
        <source>Line up</source>
        <translation>Сап</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Position</source>
        <translation>Позиция</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Line down</source>
        <translation>Сызык</translation>
    </message>
</context>
<context>
    <name>QSharedMemory</name>
    <message>
        <location filename="../src/corelib/kernel/qsharedmemory.cpp" line="+207"/>
        <source>%1: unable to set key on lock</source>
        <translation>%1: кулпуда ачкычты орното алган жок</translation>
    </message>
    <message>
        <location line="+81"/>
        <source>%1: create size is less then 0</source>
        <translation>%1: өлчөмүн түзүү аз, андан кийин 0</translation>
    </message>
    <message>
        <location line="+168"/>
        <location filename="../src/corelib/kernel/qsharedmemory_p.h" line="+148"/>
        <source>%1: unable to lock</source>
        <translation>%1: кулпулоо мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>%1: unable to unlock</source>
        <translation>%1: кулпусун ачууга болбоду</translation>
    </message>
    <message>
        <location filename="../src/corelib/kernel/qsharedmemory_unix.cpp" line="+78"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+87"/>
        <source>%1: permission denied</source>
        <translation>%1: уруксаттан баш тартылды</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="-22"/>
        <source>%1: already exists</source>
        <translation>%1: буга чейин бар</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+9"/>
        <source>%1: doesn&apos;t exists</source>
        <translation>%1: жок</translation>
    </message>
    <message>
        <location line="+6"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+9"/>
        <source>%1: out of resources</source>
        <translation>%1: ресурстардан чыгып</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+7"/>
        <source>%1: unknown error %2</source>
        <translation>%1: белгисиз ката %2</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>%1: key is empty</source>
        <translation>%1: ачкыч бош</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>%1: unix key file doesn&apos;t exists</source>
        <translation>%1: unix ачкыч файлы жок</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>%1: ftok failed</source>
        <translation>%1: фток ишке ашпады</translation>
    </message>
    <message>
        <location line="+51"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+15"/>
        <source>%1: unable to make key</source>
        <translation>%1: ачкыч жасай албай калды</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>%1: system-imposed size restrictions</source>
        <translation>%1: система тарабынан коюлган өлчөмдөгү чектөөлөр</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>%1: not attached</source>
        <translation>%1: бекитилбеген</translation>
    </message>
    <message>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="-27"/>
        <source>%1: invalid size</source>
        <translation>%1: жараксыз өлчөмү</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>%1: key error</source>
        <translation>%1: негизги ката</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>%1: size query failed</source>
        <translation>%1: өлчөмдөгү суроо ишке ашпады</translation>
    </message>
</context>
<context>
    <name>QShortcut</name>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="+373"/>
        <source>Space</source>
        <translation>Мейкиндик</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Esc</source>
        <translation>Эск</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tab</source>
        <translation>Таблица</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Backtab</source>
        <translation>Backtab</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Backspace</source>
        <translation>Арткы орун</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Return</source>
        <translation>Кайтып келүү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Enter</source>
        <translation>Кирүү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ins</source>
        <translation>Инс</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Del</source>
        <translation>Дель</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pause</source>
        <translation>Пауза</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print</source>
        <translation>Басып чыгаруу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>SysReq</source>
        <translation>SysReq</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Home</source>
        <translation>Үй</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>End</source>
        <translation>Аягы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Left</source>
        <translation>Солдо</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Up</source>
        <translation>Жогору</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right</source>
        <translation>Туура</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Down</source>
        <translation>Даун</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PgUp</source>
        <translation>PgUp</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PgDown</source>
        <translation>PgDown</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CapsLock</source>
        <translation>CapsLock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>NumLock</source>
        <translation>NumLock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ScrollLock</source>
        <translation>ScrollLock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Menu</source>
        <translation>Меню</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Help</source>
        <translation>Жардам</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Back</source>
        <translation>Артка</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Forward</source>
        <translation>Алдыга</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop</source>
        <translation>Токтотуу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Refresh</source>
        <translation>Сергитүү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Down</source>
        <translation>Томдун төмөндөшү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Mute</source>
        <translation>Томду өчүрүү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Up</source>
        <translation>Том жогорулатуу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Boost</source>
        <translation>Басс-Бус</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Up</source>
        <translation>Басс Уп</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Down</source>
        <translation>Басс Даун</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Treble Up</source>
        <translation>Титирөө</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Treble Down</source>
        <translation>Требл Даун</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Play</source>
        <translation>Медиа ойнотуу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Stop</source>
        <translation>Медиа-аялдама</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Previous</source>
        <translation>Медиа Мурунку</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Next</source>
        <translation>Медиа Кийинки</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Record</source>
        <translation>Медиа-рекорд</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Favorites</source>
        <translation>Жактыргандар</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search</source>
        <translation>Издөө</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Standby</source>
        <translation>Standby</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open URL</source>
        <translation>URL ачуу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch Mail</source>
        <translation>Иштетүү почтасы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch Media</source>
        <translation>Медианы иштетүү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (0)</source>
        <translation>Иштетүү (0)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (1)</source>
        <translation>Иштетүү (1)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (2)</source>
        <translation>Иштетүү (2)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (3)</source>
        <translation>Иштетүү (3)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (4)</source>
        <translation>Иштетүү (4)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (5)</source>
        <translation>Иштетүү (5)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (6)</source>
        <translation>Иштетүү (6)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (7)</source>
        <translation>Иштетүү (7)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (8)</source>
        <translation>Иштетүү (8)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (9)</source>
        <translation>Иштетүү (9)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (A)</source>
        <translation>Иштетүү (А)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (B)</source>
        <translation>Иштетүү (Б)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (C)</source>
        <translation>Иштетүү (С)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (D)</source>
        <translation>Иштетүү (D)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (E)</source>
        <translation>Иштетүү (Е)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (F)</source>
        <translation>Иштетүү (F)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Print Screen</source>
        <translation>Экранды басып чыгаруу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page Up</source>
        <translation>Бет жогору</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page Down</source>
        <translation>Ылдый бет</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Caps Lock</source>
        <translation>Caps lock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Num Lock</source>
        <translation>Нум Локк</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Number Lock</source>
        <translation>Номердик кулпу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll Lock</source>
        <translation>Кулпуну жылдыруу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insert</source>
        <translation>Инерт</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>Жоготуу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Escape</source>
        <translation>Качуу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>System Request</source>
        <translation>Системанын өтүнүчү</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Select</source>
        <translation>Тандоо</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Yes</source>
        <translation>Ооба</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>No</source>
        <translation>Жок</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Context1</source>
        <translation>Контекст1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context2</source>
        <translation>Контекст2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context3</source>
        <translation>Контекст3</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context4</source>
        <translation>Контекст4</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Call</source>
        <translation>Чалуу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hangup</source>
        <translation>Хангоп</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Flip</source>
        <translation>Флип</translation>
    </message>
    <message>
        <location line="+527"/>
        <location line="+122"/>
        <source>Ctrl</source>
        <translation>Ктрл</translation>
    </message>
    <message>
        <location line="-121"/>
        <location line="+125"/>
        <source>Shift</source>
        <translation>Жылдыруу</translation>
    </message>
    <message>
        <location line="-124"/>
        <location line="+122"/>
        <source>Alt</source>
        <translation>Альт</translation>
    </message>
    <message>
        <location line="-121"/>
        <location line="+117"/>
        <source>Meta</source>
        <translation>Мета</translation>
    </message>
    <message>
        <location line="-25"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>F%1</source>
        <translation>F%1</translation>
    </message>
    <message>
        <location line="-720"/>
        <source>Home Page</source>
        <translation>Башкы бет</translation>
    </message>
</context>
<context>
    <name>QSlider</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+151"/>
        <source>Page left</source>
        <translation>Калтырылган бет</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page up</source>
        <translation>Бетти жогорулатуу</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Position</source>
        <translation>Позиция</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Page right</source>
        <translation>Бет оңдо</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page down</source>
        <translation>Бетти төмөндөтүү</translation>
    </message>
</context>
<context>
    <name>QSocks5SocketEngine</name>
    <message>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="-67"/>
        <source>Connection to proxy refused</source>
        <translation>Проксиге туташуудан баш тартты</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection to proxy closed prematurely</source>
        <translation>Проксиге туташуу мөөнөтүнөн мурда жабылды</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Proxy host not found</source>
        <translation>Прокси-хост табылган жок</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Connection to proxy timed out</source>
        <translation>Проксиге туташуу убакыттын өтүшү менен</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Proxy authentication failed</source>
        <translation>Прокси аутентификациясы ишке ашпады</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Proxy authentication failed: %1</source>
        <translation>Прокси аутентификациясы ишке ашпады: %1</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>SOCKS version 5 protocol error</source>
        <translation>SOCKS версиясы 5 протокол катасы</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>General SOCKSv5 server failure</source>
        <translation>Жалпы SOCKSv5 серверинин ишсиздиги</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection not allowed by SOCKSv5 server</source>
        <translation>SOCKSv5 сервери менен туташуу жол берилбейт</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>TTL expired</source>
        <translation>ТТЛ мөөнөтү аяктады</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>SOCKSv5 command not supported</source>
        <translation>SOCKSv5 командасы колдолбойт</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Address type not supported</source>
        <translation>Дарек түрү колдолбойт</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown SOCKSv5 proxy error code 0x%1</source>
        <translation>Белгисиз SOCKSv5 прокси ката коду 0х%1</translation>
    </message>
    <message>
        <source>Socks5 timeout error connecting to socks server</source>
        <translation type="obsolete">连接到套接字服务器的时候，Socks5超时错误</translation>
    </message>
    <message>
        <location line="+685"/>
        <source>Network operation timed out</source>
        <translation>Тармактын иштеши убакыттын өтүшү менен</translation>
    </message>
</context>
<context>
    <name>QSpinBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="-574"/>
        <source>More</source>
        <translation>Кененирээк</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Less</source>
        <translation>Аз</translation>
    </message>
</context>
<context>
    <name>QSql</name>
    <message>
        <location filename="../src/qt3support/sql/q3sqlmanager_p.cpp" line="+890"/>
        <source>Delete</source>
        <translation>Жоготуу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete this record?</source>
        <translation>Бул жазууну жоготуу?</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+16"/>
        <location line="+36"/>
        <source>Yes</source>
        <translation>Ооба</translation>
    </message>
    <message>
        <location line="-51"/>
        <location line="+16"/>
        <location line="+36"/>
        <source>No</source>
        <translation>Жок</translation>
    </message>
    <message>
        <location line="-44"/>
        <source>Insert</source>
        <translation>Инерт</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Update</source>
        <translation>Жаңылоо</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save edits?</source>
        <translation>Редакцияларды сактоо?</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Confirm</source>
        <translation>Ырастоосу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel your edits?</source>
        <translation>Сиздин редакцияларды жокко чыгаруу?</translation>
    </message>
</context>
<context>
    <name>QSslSocket</name>
    <message>
        <location filename="../src/network/ssl/qsslsocket_openssl.cpp" line="+569"/>
        <source>Unable to write data: %1</source>
        <translation>Маалыматтарды жазууга болбоду: %1</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>Error while reading: %1</source>
        <translation>Окуу учурунда ката: %1</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>Error during SSL handshake: %1</source>
        <translation>ССЛ кол чабуу учурунда ката: %1</translation>
    </message>
    <message>
        <location line="-524"/>
        <source>Error creating SSL context (%1)</source>
        <translation>ССЛ контекстин түзүү катасы (%1)</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Invalid or empty cipher list (%1)</source>
        <translation>Жараксыз же бош цифер тизмеси (%1)</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Error creating SSL session, %1</source>
        <translation>ССЛ сессиясын түзүү катасы, %1</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Error creating SSL session: %1</source>
        <translation>ССЛ сессиясын түзүү катасы: %1</translation>
    </message>
    <message>
        <location line="-61"/>
        <source>Cannot provide a certificate with no key, %1</source>
        <translation>Ачкычы жок күбөлүк бере албайт, %1</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error loading local certificate, %1</source>
        <translation>Жергиликтүү күбөлүктү жүктөө катасы, %1</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Error loading private key, %1</source>
        <translation>Жеке ачкычты жүктөө катасы, %1</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Private key does not certificate public key, %1</source>
        <translation>Жеке ачкыч коомдук ачкыч жөнүндө күбөлүк бербейт, %1</translation>
    </message>
</context>
<context>
    <name>QSystemSemaphore</name>
    <message>
        <location filename="../src/corelib/kernel/qsystemsemaphore_unix.cpp" line="-41"/>
        <location filename="../src/corelib/kernel/qsystemsemaphore_win.cpp" line="+66"/>
        <source>%1: out of resources</source>
        <translation>%1: ресурстардан чыгып</translation>
    </message>
    <message>
        <location line="-13"/>
        <location filename="../src/corelib/kernel/qsystemsemaphore_win.cpp" line="+4"/>
        <source>%1: permission denied</source>
        <translation>%1: уруксаттан баш тартылды</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: already exists</source>
        <translation>%1: буга чейин бар</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: does not exist</source>
        <translation>%1: жок</translation>
    </message>
    <message>
        <location line="+9"/>
        <location filename="../src/corelib/kernel/qsystemsemaphore_win.cpp" line="+3"/>
        <source>%1: unknown error %2</source>
        <translation>%1: белгисиз ката %2</translation>
    </message>
</context>
<context>
    <name>QTDSDriver</name>
    <message>
        <location filename="../src/sql/drivers/tds/qsql_tds.cpp" line="+584"/>
        <source>Unable to open connection</source>
        <translation>Байланышты ачууга мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unable to use database</source>
        <translation>Маалымат базасын колдонууга мүмкүн эмес</translation>
    </message>
</context>
<context>
    <name>QTabBar</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="-326"/>
        <source>Scroll Left</source>
        <translation>Солго жылдыруу</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll Right</source>
        <translation>Оңго жылдыруу</translation>
    </message>
</context>
<context>
    <name>QTcpServer</name>
    <message>
        <location filename="../src/network/socket/qtcpserver.cpp" line="+282"/>
        <source>Operation on socket is not supported</source>
        <translation>Розетка боюнча операция колдолбойт</translation>
    </message>
</context>
<context>
    <name>QTextControl</name>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="+1973"/>
        <source>&amp;Undo</source>
        <translation>&amp;amp, Ундо</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Redo</source>
        <translation>&amp;amp, Редо</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cu&amp;t</source>
        <translation>Ку&amp;т</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Copy</source>
        <translation>Көчүрмө</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Copy &amp;Link Location</source>
        <translation>Көчүрмө &amp;amp, шилтеме жайгашкан жери</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Paste</source>
        <translation>&amp;amp, паста</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete</source>
        <translation>Жоготуу</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Select All</source>
        <translation>Бардыгын тандоо</translation>
    </message>
</context>
<context>
    <name>QToolButton</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="+254"/>
        <location line="+6"/>
        <source>Press</source>
        <translation>Пресс</translation>
    </message>
    <message>
        <location line="-4"/>
        <location line="+8"/>
        <source>Open</source>
        <translation>Ачуу</translation>
    </message>
</context>
<context>
    <name>QUdpSocket</name>
    <message>
        <location filename="../src/network/socket/qudpsocket.cpp" line="+169"/>
        <source>This platform does not support IPv6</source>
        <translation>Бул платформа IPv6 колдобойт</translation>
    </message>
</context>
<context>
    <name>QUndoGroup</name>
    <message>
        <location filename="../src/gui/util/qundogroup.cpp" line="+386"/>
        <source>Undo</source>
        <translation>Ундо</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Redo</source>
        <translation>Редо</translation>
    </message>
</context>
<context>
    <name>QUndoModel</name>
    <message>
        <location filename="../src/gui/util/qundoview.cpp" line="+101"/>
        <source>&lt;empty&gt;</source>
        <translation>&lt;empty&gt;</translation>
    </message>
</context>
<context>
    <name>QUndoStack</name>
    <message>
        <location filename="../src/gui/util/qundostack.cpp" line="+834"/>
        <source>Undo</source>
        <translation>Ундо</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Redo</source>
        <translation>Редо</translation>
    </message>
</context>
<context>
    <name>QUnicodeControlCharacterMenu</name>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="+884"/>
        <source>LRM Left-to-right mark</source>
        <translation>ЛРМ солдон оңго белги</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLM Right-to-left mark</source>
        <translation>РЛМ оң-сол белгиси</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWJ Zero width joiner</source>
        <translation>ZWJ Нөл туурасы кошулуучу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWNJ Zero width non-joiner</source>
        <translation>ZWNJ Нөл туурасы кошулбаган</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWSP Zero width space</source>
        <translation>ZWSP Нөл туурасы мейкиндиги</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>LRE Start of left-to-right embedding</source>
        <translation>ЛРЕ Солдон оңго егіптен чыгаруунун башталышы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLE Start of right-to-left embedding</source>
        <translation>РЛЕ Оңдон солго камтылуунун башталышы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>LRO Start of left-to-right override</source>
        <translation>ЛРО Солдон оңго өтүүнүн башталышы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLO Start of right-to-left override</source>
        <translation>Оңдон солго жылдыруу РЛО башталышы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PDF Pop directional formatting</source>
        <translation>PDF Pop багыттуу түзүмү</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Insert Unicode control character</source>
        <translation>Юникод башкаруу мүнөзүн коюу</translation>
    </message>
</context>
<context>
    <name>QWebFrame</name>
    <message>
        <location filename="../src/3rdparty/webkit/WebKit/qt/WebCoreSupport/FrameLoaderClientQt.cpp" line="+692"/>
        <source>Request cancelled</source>
        <translation>Өтүнүч жокко чыгарылган</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Request blocked</source>
        <translation>Өтүнүч блоктолду</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot show URL</source>
        <translation>URL көрсөтүү мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Frame load interruped by policy change</source>
        <translation>Саясаттын өзгөрүшү менен коштолгон кадр жүгү</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot show mimetype</source>
        <translation>Миметипти көрсөтүү мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>File does not exist</source>
        <translation>Файл жок</translation>
    </message>
</context>
<context>
    <name>QWebPage</name>
    <message>
        <location filename="../src/3rdparty/webkit/WebCore/platform/network/qt/QNetworkReplyHandler.cpp" line="+382"/>
        <source>Bad HTTP request</source>
        <translation>Жаман HTTP өтүнүчү</translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebCore/platform/qt/Localizations.cpp" line="+42"/>
        <source>Submit</source>
        <comment>default label for Submit buttons in forms on web pages</comment>
        <translation>Тапшыруу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Submit</source>
        <comment>Submit (input element) alt text for &lt;input&gt; elements with no alt, title, or value</comment>
        <translation>Тапшыруу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Reset</source>
        <comment>default label for Reset buttons in forms on web pages</comment>
        <translation>Калыбына келтирүү</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>This is a searchable index. Enter search keywords: </source>
        <comment>text that appears at the start of nearly-obsolete web pages in the form of a &apos;searchable index&apos;</comment>
        <translation>Бул издөөгө мүмкүн болгон индекс. Издөө ачкыч сөздөрүн киргизүү: </translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Choose File</source>
        <comment>title for file button used in HTML forms</comment>
        <translation>Файлды тандоо</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No file selected</source>
        <comment>text to display in file button used in HTML forms when no file is selected</comment>
        <translation>Файл тандалган жок</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open in New Window</source>
        <comment>Open in New Window context menu item</comment>
        <translation>Жаңы терезеде ачуу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save Link...</source>
        <comment>Download Linked File context menu item</comment>
        <translation>Шилтемени сактоо...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy Link</source>
        <comment>Copy Link context menu item</comment>
        <translation>Көчүрмө шилтеме</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open Image</source>
        <comment>Open Image in New Window context menu item</comment>
        <translation>Ачык сүрөт</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save Image</source>
        <comment>Download Image context menu item</comment>
        <translation>Сүрөттү сактоо</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy Image</source>
        <comment>Copy Link context menu item</comment>
        <translation>Көчүрмө сүрөтү</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open Frame</source>
        <comment>Open Frame in New Window context menu item</comment>
        <translation>Ачык кадр</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy</source>
        <comment>Copy context menu item</comment>
        <translation>Көчүрмө</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Go Back</source>
        <comment>Back context menu item</comment>
        <translation>Артка кайрыл</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Go Forward</source>
        <comment>Forward context menu item</comment>
        <translation>Алга илгерилетүү</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Stop</source>
        <comment>Stop context menu item</comment>
        <translation>Токтотуу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Reload</source>
        <comment>Reload context menu item</comment>
        <translation>Кайра жүктөө</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cut</source>
        <comment>Cut context menu item</comment>
        <translation>Кесүү</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Paste</source>
        <comment>Paste context menu item</comment>
        <translation>Паста</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No Guesses Found</source>
        <comment>No Guesses Found context menu item</comment>
        <translation>Божомолдоолор табылган жок</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ignore</source>
        <comment>Ignore Spelling context menu item</comment>
        <translation>Четке кагуу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Add To Dictionary</source>
        <comment>Learn Spelling context menu item</comment>
        <translation>Сөздүккө кошуу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Search The Web</source>
        <comment>Search The Web context menu item</comment>
        <translation>Интернетти издөө</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Look Up In Dictionary</source>
        <comment>Look Up in Dictionary context menu item</comment>
        <translation>Сөздүктү карагыла</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open Link</source>
        <comment>Open Link context menu item</comment>
        <translation>Ачык шилтеме</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ignore</source>
        <comment>Ignore Grammar context menu item</comment>
        <translation>Четке кагуу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Spelling</source>
        <comment>Spelling and Grammar context sub-menu item</comment>
        <translation>Орфография</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show Spelling and Grammar</source>
        <comment>menu item title</comment>
        <translation>Орфографияны жана грамматиканы көрсөтүү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide Spelling and Grammar</source>
        <comment>menu item title</comment>
        <translation>Орфографияны жана грамматиканы жашыруу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Check Spelling</source>
        <comment>Check spelling context menu item</comment>
        <translation>Орфографияны текшерүү</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Check Spelling While Typing</source>
        <comment>Check spelling while typing context menu item</comment>
        <translation>Терүүдө орфографияны текшерип көргүлө</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Check Grammar With Spelling</source>
        <comment>Check grammar with spelling context menu item</comment>
        <translation>Грамматиканы орфография менен текшерүү</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Fonts</source>
        <comment>Font context sub-menu item</comment>
        <translation>Шрифттер</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Bold</source>
        <comment>Bold context menu item</comment>
        <translation>Кайраттуу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Italic</source>
        <comment>Italic context menu item</comment>
        <translation>Титалик</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Underline</source>
        <comment>Underline context menu item</comment>
        <translation>Сызык</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Outline</source>
        <comment>Outline context menu item</comment>
        <translation>Контур</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Direction</source>
        <comment>Writing direction context sub-menu item</comment>
        <translation>Багыт</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Text Direction</source>
        <comment>Text direction context sub-menu item</comment>
        <translation>Текст багыты</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Default</source>
        <comment>Default writing direction context menu item</comment>
        <translation>Дефолт</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>LTR</source>
        <comment>Left to Right context menu item</comment>
        <translation>ЛТР</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>RTL</source>
        <comment>Right to Left context menu item</comment>
        <translation>RTL</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Inspect</source>
        <comment>Inspect Element context menu item</comment>
        <translation>Текшерүү</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No recent searches</source>
        <comment>Label for only item in menu that appears when clicking on the search field image, when no searches have been performed</comment>
        <translation>Акыркы издөөлөр жок</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Recent searches</source>
        <comment>label for first item in the menu that appears when clicking on the search field image, used as embedded menu title</comment>
        <translation>Акыркы издөөлөр</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Clear recent searches</source>
        <comment>menu item in Recent Searches menu that empties menu&apos;s contents</comment>
        <translation>Акыркы издөөлөрдү тазалоо</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>Unknown</source>
        <comment>Unknown filesize FTP directory listing item</comment>
        <translation>Белгисиз</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 (%2x%3 pixels)</source>
        <comment>Title string for images</comment>
        <translation>%1 (%2х%3 пиксел)</translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebKit/qt/WebCoreSupport/InspectorClientQt.cpp" line="+185"/>
        <source>Web Inspector - %2</source>
        <translation>Веб-инспектор - %2</translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebCore/platform/qt/ScrollbarQt.cpp" line="+58"/>
        <source>Scroll here</source>
        <translation>Бул жерде жылдыруу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Left edge</source>
        <translation>Сол чети</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Top</source>
        <translation>Жогорку</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right edge</source>
        <translation>Оң чети</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Bottom</source>
        <translation>Төмөнкү</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Page left</source>
        <translation>Калтырылган бет</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page up</source>
        <translation>Бетти жогорулатуу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page right</source>
        <translation>Бет оңдо</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page down</source>
        <translation>Бетти төмөндөтүү</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Scroll left</source>
        <translation>Жылдыруу калды</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll up</source>
        <translation>Жылдыруу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll right</source>
        <translation>Оңго жылдыруу</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll down</source>
        <translation>Ылдый жылдыруу</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/3rdparty/webkit/WebCore/platform/qt/FileChooserQt.cpp" line="+45"/>
        <source>%n file(s)</source>
        <comment>number of chosen file</comment>
        <translation>
            <numerusform>%n файлы</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebKit/qt/Api/qwebpage.cpp" line="+1322"/>
        <source>JavaScript Alert - %1</source>
        <translation>JavaScript эскертүүсү - %1</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>JavaScript Confirm - %1</source>
        <translation>JavaScript ырастайт - %1</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>JavaScript Prompt - %1</source>
        <translation>Яваскрипт шашылыш - %1</translation>
    </message>
    <message>
        <location line="+333"/>
        <source>Move the cursor to the next character</source>
        <translation>Курсорду кийинки мүнөзгө көчүрүү</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the previous character</source>
        <translation>Курсорду мурунку мүнөзгө көчүрүү</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the next word</source>
        <translation>Курсорду кийинки сөзгө көчүрүү</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the previous word</source>
        <translation>Курсорду мурунку сөзгө көчүрүү</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the next line</source>
        <translation>Курсорду кийинки сапка жылдыруу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the previous line</source>
        <translation>Курсорду мурунку сапка көчүрүү</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the start of the line</source>
        <translation>Курсорду саптын башталышына жылдыруу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the end of the line</source>
        <translation>Курсорду саптын аягына жылдыруу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the start of the block</source>
        <translation>Курсорду блоктун башталышына жылдыруу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the end of the block</source>
        <translation>Курсорду блоктун аягына жылдыруу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the start of the document</source>
        <translation>Курсорду документтин башталышына көчүрүү</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the end of the document</source>
        <translation>Курсорду документтин аягына жылдыруу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select all</source>
        <translation>Бардыгын тандоо</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the next character</source>
        <translation>Кийинки символго тандоо</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the previous character</source>
        <translation>Мурунку мүнөзгө тандоо</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the next word</source>
        <translation>Кийинки сөзгө тандоо</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the previous word</source>
        <translation>Мурунку сөзгө тандоо</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the next line</source>
        <translation>Кийинки сапка тандоо</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the previous line</source>
        <translation>Мурунку сапка тандоо</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the start of the line</source>
        <translation>Саптын башталышына тандоо</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the end of the line</source>
        <translation>Саптын аягына чейин тандоо</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the start of the block</source>
        <translation>Блоктун башталышына тандоо</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the end of the block</source>
        <translation>Блоктун аягына тандоо</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the start of the document</source>
        <translation>Документтин башталышына тандоо</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the end of the document</source>
        <translation>Документтин аягына чейин тандоо</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete to the start of the word</source>
        <translation>Сөздүн башталышына чейин жоготуу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete to the end of the word</source>
        <translation>Сөздүн аягына чейин жоготуу</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Insert a new paragraph</source>
        <translation>Жаңы абзацты жазды</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Insert a new line</source>
        <translation>Жаңы сапты коюшту</translation>
    </message>
</context>
<context>
    <name>QWhatsThisAction</name>
    <message>
        <location filename="../src/gui/kernel/qwhatsthis.cpp" line="+522"/>
        <source>What&apos;s This?</source>
        <translation>Бул эмне?</translation>
    </message>
</context>
<context>
    <name>QWidget</name>
    <message>
        <location filename="../src/gui/kernel/qwidget.cpp" line="+5326"/>
        <source>*</source>
        <translation>*</translation>
    </message>
</context>
<context>
    <name>QWizard</name>
    <message>
        <location filename="../src/gui/dialogs/qwizard.cpp" line="+637"/>
        <source>Go Back</source>
        <translation>Артка кайрыл</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Continue</source>
        <translation>Улантуу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Commit</source>
        <translation>Милдеттенме</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Done</source>
        <translation>Аткарылды</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="obsolete">退出</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Help</source>
        <translation>Жардам</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>&lt; &amp;Back</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Finish</source>
        <translation>Бүтүрүү</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Help</source>
        <translation>Жардам</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>&amp;Next</source>
        <translation>Кийинки</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Next &gt;</source>
        <translation>&amp;amp, кийинки &gt;</translation>
    </message>
</context>
<context>
    <name>QWorkspace</name>
    <message>
        <location filename="../src/gui/widgets/qworkspace.cpp" line="+1094"/>
        <source>&amp;Restore</source>
        <translation>Калыбына келтирүү</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Move</source>
        <translation>Көчүп баруу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>&amp;amp, көлөмү</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mi&amp;nimize</source>
        <translation>Ми &amp;amp, нимиз</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ma&amp;ximize</source>
        <translation>Ма &amp;amp, симиз</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Close</source>
        <translation>&amp; жабуу</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Stay on &amp;Top</source>
        <translation>Жогоруда болгула</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+1059"/>
        <source>Sh&amp;ade</source>
        <translation>Ш &amp;amp, ад</translation>
    </message>
    <message>
        <location line="-278"/>
        <location line="+60"/>
        <source>%1 - [%2]</source>
        <translation>%1 - [%2]</translation>
    </message>
    <message>
        <location line="-1837"/>
        <source>Minimize</source>
        <translation>Минималдуу</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Restore Down</source>
        <translation>Калыбына келтирүү</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
    <message>
        <location line="+2053"/>
        <source>&amp;Unshade</source>
        <translation>&amp;amp, уншод</translation>
    </message>
</context>
<context>
    <name>QXml</name>
    <message>
        <location filename="../src/xml/sax/qxml.cpp" line="+58"/>
        <source>no error occurred</source>
        <translation>ката болгон жок</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error triggered by consumer</source>
        <translation>керектөөчүнүн катасы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected end of file</source>
        <translation>файлдын күтүлбөгөн аягы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>more than one document type definition</source>
        <translation>бирден көп документ түрү аныктамасы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing element</source>
        <translation>элементти талдоо учурунда ката пайда болгон</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>tag mismatch</source>
        <translation>тег дал келбестик</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing content</source>
        <translation>мазмунду талдоо учурунда ката пайда болгон</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected character</source>
        <translation>күтүлбөгөн мүнөз</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid name for processing instruction</source>
        <translation>окууну иштетүү үчүн жараксыз аты</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>version expected while reading the XML declaration</source>
        <translation>XML декларациясын окуп жатканда күтүлгөн версиясы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>wrong value for standalone declaration</source>
        <translation>жалгыз декларациялоо үчүн туура эмес маани</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>encoding declaration or standalone declaration expected while reading the XML declaration</source>
        <translation>XML декларациясын окуп жатканда декларацияны же жалгыз декларацияны коддоо күтүлүүдө</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>standalone declaration expected while reading the XML declaration</source>
        <translation>XML декларациясын окуп жатканда жалгыз декларация күтүлүүдө</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing document type definition</source>
        <translation>документтин түрү аныктамасын талдоо учурунда ката пайда болгон</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>letter is expected</source>
        <translation>кат күтүлүүдө</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing comment</source>
        <translation>ката комментарий талдоо учурунда пайда болгон</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing reference</source>
        <translation>ката шилтемени талдоо учурунда пайда болгон</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>internal general entity reference not allowed in DTD</source>
        <translation>ДТДда жол берилбеген жалпы субъекттин ички шилтемеси</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>external parsed general entity reference not allowed in attribute value</source>
        <translation>атрибуттук мааниде жол берилбеген жалпы субъекттин тышкы талданган шилтемеси</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>external parsed general entity reference not allowed in DTD</source>
        <translation>ДТДда жол берилбеген жалпы субъекттин тышкы талданган шилтемеси</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unparsed entity reference in wrong context</source>
        <translation>туура эмес контекстте бейтараптуу субъект шилтеме</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>recursive entities</source>
        <translation>кайталануучу субъекттер</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error in the text declaration of an external entity</source>
        <translation>тышкы субъекттин тексттик декларациясында ката</translation>
    </message>
</context>
<context>
    <name>QXmlStream</name>
    <message>
        <location filename="../src/corelib/xml/qxmlstream.cpp" line="+592"/>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+1769"/>
        <source>Extra content at end of document.</source>
        <translation>Документтин аягында кошумча мазмун.</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>Invalid entity value.</source>
        <translation>Жараксыз субъекттин наркы.</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Invalid XML character.</source>
        <translation>Туура эмес XML мүнөзү.</translation>
    </message>
    <message>
        <location line="+259"/>
        <source>Sequence &apos;]]&gt;&apos; not allowed in content.</source>
        <translation>«]]&gt;» мазмунунда жол берилбейт.</translation>
    </message>
    <message>
        <location line="+309"/>
        <source>Namespace prefix &apos;%1&apos; not declared</source>
        <translation>Аты-жөнү &apos;%1&apos; аталышы жарыяланбайт</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Attribute redefined.</source>
        <translation>Атрибут кайра аныкталды.</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Unexpected character &apos;%1&apos; in public id literal.</source>
        <translation>Күтүлбөгөн мүнөзү &apos;%1&apos; коомдук идентификация түзмө-түз.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Invalid XML version string.</source>
        <translation>Туура эмес XML версиясы сап.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unsupported XML version.</source>
        <translation>Ырасталбаган XML версиясы.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>%1 is an invalid encoding name.</source>
        <translation>%1 — жараксыз коддоо аты.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Encoding %1 is unsupported</source>
        <translation>%1 коддоого мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Standalone accepts only yes or no.</source>
        <translation>Standalone гана &quot;ооба&quot; же &quot;жок&quot; деп кабыл алган.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid attribute in XML declaration.</source>
        <translation>XML декларациясында жараксыз атрибут.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Premature end of document.</source>
        <translation>Документтин мөөнөтүнөн мурда аякташы.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid document.</source>
        <translation>Жараксыз документ.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Expected </source>
        <translation>Күтүлүүдө </translation>
    </message>
    <message>
        <location line="+11"/>
        <source>, but got &apos;</source>
        <translation>бирок алдым &quot;</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Unexpected &apos;</source>
        <translation>Күтүлбөгөн жерден &apos;</translation>
    </message>
    <message>
        <location line="+210"/>
        <source>Expected character data.</source>
        <translation>Күтүлгөн мүнөз маалыматтары.</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="-995"/>
        <source>Recursive entity detected.</source>
        <translation>Кайталануучу субъект аныкталды.</translation>
    </message>
    <message>
        <location line="+516"/>
        <source>Start tag expected.</source>
        <translation>Баштоо теги күтүлүүдө.</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>XML declaration not at start of document.</source>
        <translation>XML декларациясы документтин башталышында эмес.</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>NDATA in parameter entity declaration.</source>
        <translation>Параметрдик субъект декларациясында NDATA.</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>%1 is an invalid processing instruction name.</source>
        <translation>%1 — жараксыз иштетүү окутуу аты.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Invalid processing instruction name.</source>
        <translation>Жараксыз иштетүү нускама аты.</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream.cpp" line="-521"/>
        <location line="+12"/>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+164"/>
        <location line="+53"/>
        <source>Illegal namespace declaration.</source>
        <translation>Мыйзамсыз аты-жөнү декларациясы.</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+15"/>
        <source>Invalid XML name.</source>
        <translation>Туура эмес XML аты.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Opening and ending tag mismatch.</source>
        <translation>Тег дал келбесин ачуу жана аяктоо.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Reference to unparsed entity &apos;%1&apos;.</source>
        <translation>«%1» бейтараптуу субъектиге шилтеме.</translation>
    </message>
    <message>
        <location line="-13"/>
        <location line="+61"/>
        <location line="+40"/>
        <source>Entity &apos;%1&apos; not declared.</source>
        <translation>Субъект &apos;%1&apos; жарыяланбайт.</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Reference to external entity &apos;%1&apos; in attribute value.</source>
        <translation>Атрибуттук мааниде тышкы субъектке «%1» шилтемеси.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Invalid character reference.</source>
        <translation>Жараксыз мүнөз шилтеме.</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream.cpp" line="-75"/>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="-823"/>
        <source>Encountered incorrectly encoded content.</source>
        <translation>Туура эмес коддолгон мазмунга туш болдум.</translation>
    </message>
    <message>
        <location line="+274"/>
        <source>The standalone pseudo attribute must appear after the encoding.</source>
        <translation>Автономдуу псевдо атрибуты коддоодон кийин пайда болушу керек.</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+562"/>
        <source>%1 is an invalid PUBLIC identifier.</source>
        <translation>%1 — жараксыз коомдук идентификат.</translation>
    </message>
</context>
<context>
    <name>QtXmlPatterns</name>
    <message>
        <location filename="../src/xmlpatterns/acceltree/qacceltreebuilder.cpp" line="+205"/>
        <source>An %1-attribute with value %2 has already been declared.</source>
        <translation>%2 наркы менен %1-атрибут жарыяланды.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>An %1-attribute must have a valid %2 as value, which %3 isn&apos;t.</source>
        <translation>%1-атрибут наркы катары жарактуу %2 болушу керек, ал %3 эмес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/api/qiodevicedelegate.cpp" line="+84"/>
        <source>Network timeout.</source>
        <translation>Тармактын убакыт өтүшү.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/api/qxmlserializer.cpp" line="+320"/>
        <source>Element %1 can&apos;t be serialized because it appears outside the document element.</source>
        <translation>%1 элементи документ элементинин сыртында пайда болгондуктан серияланбайт.</translation>
    </message>
    <message>
        <source>Attribute element %1 can&apos;t be serialized because it appears at the top level.</source>
        <translation type="obsolete">属性元素 %1 不能被串行化，因为它出现在最顶层。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qabstractdatetime.cpp" line="+80"/>
        <source>Year %1 is invalid because it begins with %2.</source>
        <translation>%1 жылы жараксыз, анткени ал %2 менен башталат.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Day %1 is outside the range %2..%3.</source>
        <translation>%1 күнү %2,3 диапазонунан тышкаркы.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Month %1 is outside the range %2..%3.</source>
        <translation>%1 айы % 2,3 диапазонунан тышкаркы.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Overflow: Can&apos;t represent date %1.</source>
        <translation>Толуп кетүү: %1 датасын чагылдыра албайт.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Day %1 is invalid for month %2.</source>
        <translation>%1 күнү %2 ай үчүн жараксыз.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Time 24:%1:%2.%3 is invalid. Hour is 24, but minutes, seconds, and milliseconds are not all 0; </source>
        <translation>Убакыт 24:1:2%3 жараксыз болуп саналат. саат 24, бирок мүнөт, секунд, жана миллисекунд бардык эмес 0; </translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Time %1:%2:%3.%4 is invalid.</source>
        <translation>Убакыт %1:2:2:3,4 жараксыз.</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Overflow: Date can&apos;t be represented.</source>
        <translation>Толуп кетүү: Датаны көрсөтүү мүмкүн эмес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qabstractduration.cpp" line="+99"/>
        <location line="+15"/>
        <source>At least one component must be present.</source>
        <translation>Жок дегенде бир компонент болушу керек.</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>At least one time component must appear after the %1-delimiter.</source>
        <translation>Жок дегенде бир убакыт компоненти %1-делимитерден кийин пайда болушу керек.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qabstractfloatmathematician.cpp" line="+64"/>
        <source>No operand in an integer division, %1, can be %2.</source>
        <translation>Бир интегер бөлүмүндө эч кандай операнд, %1, % 2 болушу мүмкүн эмес.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The first operand in an integer division, %1, cannot be infinity (%2).</source>
        <translation>&quot;%1&quot;, интегердик бөлүмдөгү биринчи интернат чексиздик (%2) болушу мүмкүн эмес.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The second operand in a division, %1, cannot be zero (%2).</source>
        <translation>Дивизиондогу экинчи опера , %1, нөл (%2) болушу мүмкүн эмес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qanyuri_p.h" line="+132"/>
        <source>%1 is not a valid value of type %2.</source>
        <translation>%1 % 2 түрү жарактуу наркы эмес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qatomiccasters_p.h" line="+223"/>
        <source>When casting to %1 from %2, the source value cannot be %3.</source>
        <translation>%2-ден %1-ге чейин кастинг жүргүзүүдө булактын наркы %3 болушу мүмкүн эмес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qatomicmathematicians.cpp" line="+65"/>
        <source>Integer division (%1) by zero (%2) is undefined.</source>
        <translation>Интегер бөлүнүшү (%1) нөлгө (%2) аныкталбайт.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Division (%1) by zero (%2) is undefined.</source>
        <translation>Бөлүштүрүү (%1) нөлгө (%2) аныкталбайт.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Modulus division (%1) by zero (%2) is undefined.</source>
        <translation>Модулдун бөлүнүшү (%1) нөлгө (%2) аныкталбайт.</translation>
    </message>
    <message>
        <location line="+122"/>
        <location line="+32"/>
        <source>Dividing a value of type %1 by %2 (not-a-number) is not allowed.</source>
        <translation>%1 түрүнүн наркын %2 (номери эмес) бөлүштүрүүгө жол берилбейт.</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Dividing a value of type %1 by %2 or %3 (plus or minus zero) is not allowed.</source>
        <translation>%1 түрүнүн наркын %2 же %3 (плюс же минус нөлгө) бөлүүгө жол берилбейт.</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Multiplication of a value of type %1 by %2 or %3 (plus or minus infinity) is not allowed.</source>
        <translation>%1 түрүнүн наркын %2 же %3 (плюс же минус чексиздик) көбөйтүүгө жол берилбейт.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qatomicvalue.cpp" line="+79"/>
        <source>A value of type %1 cannot have an Effective Boolean Value.</source>
        <translation>%1 түрүнүн наркы Эффективдүү Булеан наркына ээ боло албайт.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qboolean.cpp" line="+78"/>
        <source>Effective Boolean Value cannot be calculated for a sequence containing two or more atomic values.</source>
        <translation>Эффективдүү Булеан наркын эки же андан көп атомдук баалуулуктарды камтыган ырааттуулук менен эсептөө мүмкүн эмес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qderivedinteger_p.h" line="+402"/>
        <source>Value %1 of type %2 exceeds maximum (%3).</source>
        <translation>%2 түрү боюнча %1 наркы максималдуу (%3) ашып түшөт.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Value %1 of type %2 is below minimum (%3).</source>
        <translation>%2 түрү %1 наркы минималдуудан төмөн (%3).</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qhexbinary.cpp" line="+91"/>
        <source>A value of type %1 must contain an even number of digits. The value %2 does not.</source>
        <translation>%1 түрүнүн наркы бирдей санды камтышы керек. %2 наркы жок.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>%1 is not valid as a value of type %2.</source>
        <translation>%1 түрү %2 наркы катары жарактуу эмес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qarithmeticexpression.cpp" line="+207"/>
        <source>Operator %1 cannot be used on type %2.</source>
        <translation>%1 оператору %2 түрү боюнча колдонулбайт.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Operator %1 cannot be used on atomic values of type %2 and %3.</source>
        <translation>%1 оператору %2 жана %3 түрү атомдук баалуулуктар боюнча колдонулбайт.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qattributenamevalidator.cpp" line="+66"/>
        <source>The namespace URI in the name for a computed attribute cannot be %1.</source>
        <translation>Эсептөө атрибутунун аты менен URI аты %1 болушу мүмкүн эмес.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The name for a computed attribute cannot have the namespace URI %1 with the local name %2.</source>
        <translation>Эсептөө атрибутунун аты жергиликтүү аты %2 менен URI %1 аты-жөнү болушу мүмкүн эмес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcastas.cpp" line="+88"/>
        <source>Type error in cast, expected %1, received %2.</source>
        <translation>%1 күтүлгөн кастингте терүү катасы %2 алды.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>When casting to %1 or types derived from it, the source value must be of the same type, or it must be a string literal. Type %2 is not allowed.</source>
        <translation>% 1 же андан алынган түрлөрүн кастинг учурунда, булак наркы бир түрү болушу керек, же түзмө-түз сап болушу керек. %2 түрү жол берилбейт.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcastingplatform.cpp" line="+134"/>
        <source>No casting is possible with %1 as the target type.</source>
        <translation>Максаттуу түрү катары % 1 менен эч кандай кастинг мүмкүн эмес.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>It is not possible to cast from %1 to %2.</source>
        <translation>%1 -ден %2-ге чейин ыргытуу мүмкүн эмес.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Casting to %1 is not possible because it is an abstract type, and can therefore never be instantiated.</source>
        <translation>%1 үчүн кастинг мүмкүн эмес, анткени ал абстракттуу түрү болуп саналат, ошондуктан эч качан заматта болушу мүмкүн эмес.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>It&apos;s not possible to cast the value %1 of type %2 to %3</source>
        <translation>%2 % дан %3-ке чейинки типтеги %1 наркын ыргытуу мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Failure when casting from %1 to %2: %3</source>
        <translation>%1 -ден %2-ге чейин кастинг жүргүзүүдө ийгиликсиздик: %3</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcommentconstructor.cpp" line="+67"/>
        <source>A comment cannot contain %1</source>
        <translation>Комментарий %1 камтыла албайт</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>A comment cannot end with a %1.</source>
        <translation>Комментарий %1 менен аякташы мүмкүн эмес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcomparisonplatform.cpp" line="+167"/>
        <source>No comparisons can be done involving the type %1.</source>
        <translation>%1 түрү менен салыштыруулар жүргүзүлбөйт.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Operator %1 is not available between atomic values of type %2 and %3.</source>
        <translation>%1 оператору %2 жана %3 түрү атомдук баалуулуктардын ортосунда жеткиликтүү эмес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qdocumentcontentvalidator.cpp" line="+86"/>
        <source>An attribute node cannot be a child of a document node. Therefore, the attribute %1 is out of place.</source>
        <translation>Атрибут түйүнү документ түйүнүнүн баласы боло албайт. Ошондуктан %1 атрибуты орунсуз.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qexpressionfactory.cpp" line="+169"/>
        <source>A library module cannot be evaluated directly. It must be imported from a main module.</source>
        <translation>Китепкана модулун түздөн-түз баалоо мүмкүн эмес. Ал негизги модулдан импорттолушу керек.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>No template by name %1 exists.</source>
        <translation>%1 аты боюнча шаблон жок.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qgenericpredicate.cpp" line="+106"/>
        <source>A value of type %1 cannot be a predicate. A predicate must have either a numeric type or an Effective Boolean Value type.</source>
        <translation>%1 түрүнүн наркы предикат боло албайт. Предикаттын сандык түрү же Эффективдүү Булеан наркы түрү болушу керек.</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>A positional predicate must evaluate to a single numeric value.</source>
        <translation>Позициялык предикат бир сандык мааниге баа бериши керек.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qncnameconstructor_p.h" line="+113"/>
        <source>The target name in a processing instruction cannot be %1 in any combination of upper and lower case. Therefore, is %2 invalid.</source>
        <translation>кайра иштетүү нускамасында максаттуу аты жогорку жана төмөнкү учурда кандайдыр бир айкалышында % 1 болушу мүмкүн эмес. Ошондуктан % 2 жараксыз болуп саналат.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>%1 is not a valid target name in a processing instruction. It must be a %2 value, e.g. %3.</source>
        <translation>%1 кайра иштетүү көрсөтмөсүндө жарактуу максаттуу аты эмес. Бул %2 наркы, башкача айтканда, %3 болушу керек.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qpath.cpp" line="+109"/>
        <source>The last step in a path must contain either nodes or atomic values. It cannot be a mixture between the two.</source>
        <translation>Жолдогу акыркы кадам түйүндөрдү же атомдук баалуулуктарды камтышы керек. Бул экөөнүн ортосундагы аралашма болушу мүмкүн эмес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qprocessinginstructionconstructor.cpp" line="+84"/>
        <source>The data of a processing instruction cannot contain the string %1</source>
        <translation>Кайра иштетүү боюнча көрсөтмөнүн маалыматтары %1 сапты камтый албайт</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qqnameconstructor.cpp" line="+82"/>
        <source>No namespace binding exists for the prefix %1</source>
        <translation>%1 префиксинин аты-жөнү жок</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qqnameconstructor_p.h" line="+156"/>
        <source>No namespace binding exists for the prefix %1 in %2</source>
        <translation>%2 боюнча %1 префиксинин аты-жөнү жок</translation>
    </message>
    <message>
        <location line="+12"/>
        <location filename="../src/xmlpatterns/functions/qqnamefns.cpp" line="+69"/>
        <source>%1 is an invalid %2</source>
        <translation>%1 — жараксыз %2</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/xmlpatterns/functions/qabstractfunctionfactory.cpp" line="+77"/>
        <source>%1 takes at most %n argument(s). %2 is therefore invalid.</source>
        <translation>
            <numerusform>%1 көпчүлүк %n аргументин алат. Ошондуктан %2 жараксыз.</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%1 requires at least %n argument(s). %2 is therefore invalid.</source>
        <translation>
            <numerusform>%1 жок дегенде %n аргументин талап кылат. Ошондуктан %2 жараксыз.</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qaggregatefns.cpp" line="+120"/>
        <source>The first argument to %1 cannot be of type %2. It must be a numeric type, xs:yearMonthDuration or xs:dayTimeDuration.</source>
        <translation>%1 үчүн биринчи аргумент %2 түрү болушу мүмкүн эмес. Бул сандык түрү болушу керек, xs:yearMonthDuration же xs:dayTimeDuration.</translation>
    </message>
    <message>
        <location line="+74"/>
        <source>The first argument to %1 cannot be of type %2. It must be of type %3, %4, or %5.</source>
        <translation>%1 үчүн биринчи аргумент %2 түрү болушу мүмкүн эмес. %3, %4, же %5 түрү болушу керек.</translation>
    </message>
    <message>
        <location line="+91"/>
        <source>The second argument to %1 cannot be of type %2. It must be of type %3, %4, or %5.</source>
        <translation>%1 үчүн экинчи аргумент %2 түрү болушу мүмкүн эмес. %3, %4, же %5 түрү болушу керек.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qassemblestringfns.cpp" line="+88"/>
        <source>%1 is not a valid XML 1.0 character.</source>
        <translation>%1 жарактуу XML 1.0 мүнөзү эмес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qcomparingaggregator.cpp" line="+197"/>
        <source>The first argument to %1 cannot be of type %2.</source>
        <translation>%1 үчүн биринчи аргумент %2 түрү болушу мүмкүн эмес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qdatetimefn.cpp" line="+86"/>
        <source>If both values have zone offsets, they must have the same zone offset. %1 and %2 are not the same.</source>
        <translation>Эгерде эки баалуулукта да зоналык офсеттик офсеттер бар болсо, анда алар бир эле аймактын офсеттүү болушу керек. %1 жана %2 бирдей эмес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qerrorfn.cpp" line="+61"/>
        <source>%1 was called.</source>
        <translation>%1 чакырылды.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qpatternmatchingfns.cpp" line="+94"/>
        <source>%1 must be followed by %2 or %3, not at the end of the replacement string.</source>
        <translation>%1 алмаштыруу сапынын аягында эмес, %2 же %3 менен жүрүшү керек.</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>In the replacement string, %1 must be followed by at least one digit when not escaped.</source>
        <translation>Алмаштыруу сапында %1 качып кетпеген учурда, жок эле дегенде, бир сан менен жүрүшү керек.</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>In the replacement string, %1 can only be used to escape itself or %2, not %3</source>
        <translation>алмаштыруу сапында% 1 гана өзүнөн качуу үчүн колдонулушу мүмкүн же %3 эмес, %2</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qpatternplatform.cpp" line="+92"/>
        <source>%1 matches newline characters</source>
        <translation>%1 жаңы символдорго дал келет</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1 and %2 match the start and end of a line.</source>
        <translation>%1 жана %2 саптын башталышына жана аягына дал келет.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Matches are case insensitive</source>
        <translation>Матчтар сезимсиз болуп саналат</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Whitespace characters are removed, except when they appear in character classes</source>
        <translation>Ак мейкиндиктин символдору, мүнөз класстарында пайда болгондон башка, алынып салынат</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>%1 is an invalid regular expression pattern: %2</source>
        <translation>%1 — жараксыз үзгүлтүксүз билдирүү үлгүсү: %2</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>%1 is an invalid flag for regular expressions. Valid flags are:</source>
        <translation>%1 — үзгүлтүксүз сүйлөө үчүн жараксыз желек. Жарактуу желектер болуп саналат:</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qqnamefns.cpp" line="+17"/>
        <source>If the first argument is the empty sequence or a zero-length string (no namespace), a prefix cannot be specified. Prefix %1 was specified.</source>
        <translation>Эгерде биринчи аргумент бош ырааттуулук же нөл узундуктагы сап (аты-жөнү жок) болсо, префиксти көрсөтүү мүмкүн эмес. %1 префикси көрсөтүлгөн.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qsequencefns.cpp" line="+347"/>
        <source>It will not be possible to retrieve %1.</source>
        <translation>%1 алуу мүмкүн эмес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qcontextnodechecker.cpp" line="+54"/>
        <source>The root node of the second argument to function %1 must be a document node. %2 is not a document node.</source>
        <translation>%1 иштеши үчүн экинчи аргументтин тамыр түйүнү документ түйүнү болушу керек. %2 документ түйүнү эмес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qsequencegeneratingfns.cpp" line="+279"/>
        <source>The default collection is undefined</source>
        <translation>Дефолт чогултуу аныкталбаган</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>%1 cannot be retrieved</source>
        <translation>%1 алуу мүмкүн эмес</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qstringvaluefns.cpp" line="+252"/>
        <source>The normalization form %1 is unsupported. The supported forms are %2, %3, %4, and %5, and none, i.e. the empty string (no normalization).</source>
        <translation>%1 нормалдаштыруу формасы популярдуу эмес. Колдоого алынган формалар % 2, %3, %4 жана %5, жана эч ким, башкача айтканда, бош сап (нормалдаштыруу жок).</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qtimezonefns.cpp" line="+87"/>
        <source>A zone offset must be in the range %1..%2 inclusive. %3 is out of range.</source>
        <translation>Зоналык офсеттик %1,2 инклюзивдүү диапазонунда болушу керек. %3 диапазонунан чыгып жатат.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1 is not a whole number of minutes.</source>
        <translation>%1 мүнөттүн бүт саны эмес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/janitors/qcardinalityverifier.cpp" line="+58"/>
        <source>Required cardinality is %1; got cardinality %2.</source>
        <translation>Талап кылынган кардиналдык %1; кардиналдык %2 алды.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/janitors/qitemverifier.cpp" line="+67"/>
        <source>The item %1 did not match the required type %2.</source>
        <translation>%1 пункту талап кылынган түрү %2 дал келбейт.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qquerytransformparser.cpp" line="+379"/>
        <location line="+7253"/>
        <source>%1 is an unknown schema type.</source>
        <translation>%1 — белгисиз схема түрү.</translation>
    </message>
    <message>
        <location line="-6971"/>
        <source>Only one %1 declaration can occur in the query prolog.</source>
        <translation>Суроо прологунда бир гана %1 декларациясы болушу мүмкүн.</translation>
    </message>
    <message>
        <location line="+188"/>
        <source>The initialization of variable %1 depends on itself</source>
        <translation>%1 өзгөргүчтү инициализациялоо өзүнөн көз каранды</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>No variable by name %1 exists</source>
        <translation>%1 аты боюнча өзгөргүч жок</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qparsercontext.cpp" line="+93"/>
        <source>The variable %1 is unused</source>
        <translation>%1 өзгөрүлмөсү пайдаланылбайт</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qquerytransformparser.cpp" line="+2841"/>
        <source>Version %1 is not supported. The supported XQuery version is 1.0.</source>
        <translation>%1 версиясы колдолбойт. Колдоого алынган XQuery версиясы 1.0 болуп саналат.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>The encoding %1 is invalid. It must contain Latin characters only, must not contain whitespace, and must match the regular expression %2.</source>
        <translation>%1 коддоо жараксыз. Ал латынча символдорду гана камтышы керек, ак мейкиндикти камтыбашы керек жана %2 үзгүлтүксүз сөз айкашына дал келбеши керек.</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>No function with signature %1 is available</source>
        <translation>%1 кол коюу менен эч кандай функция жок</translation>
    </message>
    <message>
        <location line="+72"/>
        <location line="+10"/>
        <source>A default namespace declaration must occur before function, variable, and option declarations.</source>
        <translation>Дефолт аты-жөнү декларациясы функция, өзгөргүч жана опциондук декларациялар алдында болушу керек.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Namespace declarations must occur before function, variable, and option declarations.</source>
        <translation>Аты-жөнү декларациялар функция, өзгөргүч жана опциондук декларациялар алдында болушу керек.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Module imports must occur before function, variable, and option declarations.</source>
        <translation>Модулдун импорту функция, өзгөргүч жана опциондук декларациялар алдында болушу керек.</translation>
    </message>
    <message>
        <location line="+200"/>
        <source>It is not possible to redeclare prefix %1.</source>
        <translation>%1 префиксинин кайра классификацияланышы мүмкүн эмес.</translation>
    </message>
    <message>
        <source>Only the prefix %1 can be declared to bind the namespace %2. By default, it is already bound to the prefix %1.</source>
        <translation type="obsolete">至于前缀 %1 可以被声明为和命名空间 %2 绑定。默认情况下，它已经被绑定到前缀 %1。</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Prefix %1 is already declared in the prolog.</source>
        <translation>Прологдо %1 префикси жарыяланды.</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>The name of an option must have a prefix. There is no default namespace for options.</source>
        <translation>Опциянын аты префикс болушу керек. Опциялар үчүн дефолт аты-жөнү жок.</translation>
    </message>
    <message>
        <location line="+171"/>
        <source>The Schema Import feature is not supported, and therefore %1 declarations cannot occur.</source>
        <translation>Schema импортунун өзгөчөлүгү колдолбойт, ошондуктан %1 декларациялар болушу мүмкүн эмес.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The target namespace of a %1 cannot be empty.</source>
        <translation>%1 максаттуу аты-жөнү бош болушу мүмкүн эмес.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The module import feature is not supported</source>
        <translation>Модулду импорттоо өзгөчөлүгү колдолбойт</translation>
    </message>
    <message>
        <source>A variable by name %1 has already been declared in the prolog.</source>
        <translation type="obsolete">名称为 %1 的变量已经在序言中声明过了。</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>No value is available for the external variable by name %1.</source>
        <translation>%1 аты менен тышкы өзгөргүч үчүн эч кандай нарк жеткиликтүү эмес.</translation>
    </message>
    <message>
        <source>The namespace for a user defined function cannot be empty(try the predefined prefix %1 which exists for cases like this)</source>
        <translation type="obsolete">用户定义的函数的命名空间不能为空(请试试预定义的前缀 %1，它就是用于这种情况的)。</translation>
    </message>
    <message>
        <location line="-4154"/>
        <source>A construct was encountered which only is allowed in XQuery.</source>
        <translation>XQuery-да гана уруксат берилген курулуш жолугушту.</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>A template by name %1 has already been declared.</source>
        <translation>%1 аты менен шаблон жарыяланды.</translation>
    </message>
    <message>
        <location line="+3581"/>
        <source>The keyword %1 cannot occur with any other mode name.</source>
        <translation>%1 ачкычы башка режимдин аты менен пайда боло албайт.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>The value of attribute %1 must of type %2, which %3 isn&apos;t.</source>
        <translation>%1 атрибутунун наркы %2 түрүн түзүшү керек, ал %3 эмес.</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>The prefix %1 can not be bound. By default, it is already bound to the namespace %2.</source>
        <translation>%1 префиксин байлоо мүмкүн эмес. Дефолт боюнча ал %2 аты-жөнү менен чектелген.</translation>
    </message>
    <message>
        <location line="+312"/>
        <source>A variable by name %1 has already been declared.</source>
        <translation>%1 аты боюнча өзгөргүч жарыяланды.</translation>
    </message>
    <message>
        <location line="+135"/>
        <source>A stylesheet function must have a prefixed name.</source>
        <translation>Стилдер таблицасынын милдети алдын ала белгиленген атка ээ болушу керек.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The namespace for a user defined function cannot be empty (try the predefined prefix %1 which exists for cases like this)</source>
        <translation>Колдонуучу аныкталган функциянын аты-жөнү бош болушу мүмкүн эмес (ушул сыяктуу учурларда бар алдын ала аныкталган %1 фиксалын сынап көргүлө)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The namespace %1 is reserved; therefore user defined functions may not use it. Try the predefined prefix %2, which exists for these cases.</source>
        <translation>%1 аты-жөнү сакталган; ошондуктан колдонуучу аныкталган функциялар аны колдонбошу мүмкүн. Бул учурлар үчүн бар алдын ала аныкталган %2 префиксин аракет кылыңыз.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>The namespace of a user defined function in a library module must be equivalent to the module namespace. In other words, it should be %1 instead of %2</source>
        <translation>Китепкана модулунда колдонуучу аныкталган функциянын аты-жөнү модулдун аты-жөнүнө барабар болушу керек. Башкача айтканда, % 2 ордуна % 1 болушу керек</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>A function already exists with the signature %1.</source>
        <translation>%1 кол коюу менен бир функция бар.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>No external functions are supported. All supported functions can be used directly, without first declaring them as external</source>
        <translation>Эч кандай тышкы функциялар колдоого алынбайт. Бардык колдоого алынган милдеттери түздөн-түз колдонулушу мүмкүн, адегенде тышкы деп жарыялабай</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>An argument by name %1 has already been declared. Every argument name must be unique.</source>
        <translation>%1 аты менен аргумент жарыяланды. Ар бир аргументтин аты өзгөчө болушу керек.</translation>
    </message>
    <message>
        <location line="+179"/>
        <source>When function %1 is used for matching inside a pattern, the argument must be a variable reference or a string literal.</source>
        <translation>%1 функциясы үлгүнүн ичинде дал келүү үчүн колдонулганда, аргумент өзгөрмө шилтеме же түзмө-түз сап болушу керек.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>In an XSL-T pattern, the first argument to function %1 must be a string literal, when used for matching.</source>
        <translation>XSL-T үлгүсүндө% 1 иштеши үчүн биринчи аргумент түзмө-түз, дал келүү үчүн колдонулганда сап болушу керек.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>In an XSL-T pattern, the first argument to function %1 must be a literal or a variable reference, when used for matching.</source>
        <translation>XSL-T үлгүсүндө % 1 иштей турган биринчи аргумент дал келүү үчүн колдонулганда түзмө-түз же өзгөрмө шилтеме болушу керек.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>In an XSL-T pattern, function %1 cannot have a third argument.</source>
        <translation>XSL-T үлгүсүндө %1 функциясы үчүнчү аргументке ээ боло албайт.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>In an XSL-T pattern, only function %1 and %2, not %3, can be used for matching.</source>
        <translation>XSL-T үлгүсүндө % 3 эмес, %1 жана %2 гана иштей алат.</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>In an XSL-T pattern, axis %1 cannot be used, only axis %2 or %3 can.</source>
        <translation>XSL-T үлгүсүндө % 1 огу колдонулбайт, %2 же %3 огу гана колдоно алат.</translation>
    </message>
    <message>
        <location line="+126"/>
        <source>%1 is an invalid template mode name.</source>
        <translation>%1 — жараксыз шаблон режиминин аты.</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>The name of a variable bound in a for-expression must be different from the positional variable. Hence, the two variables named %1 collide.</source>
        <translation>Бир сөз айкашына байланган өзгөргүчтүн аты позициялык өзгөргүчтөн айырмаланышы керек. Демек, %1 деп аталган эки өзгөргүч бири-бири менен кагылышат.</translation>
    </message>
    <message>
        <location line="+758"/>
        <source>The Schema Validation Feature is not supported. Hence, %1-expressions may not be used.</source>
        <translation>Схеманы текшерүү өзгөчөлүгү колдолбойт. Демек, %1-сөз айкаштары колдонулбашы мүмкүн.</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>None of the pragma expressions are supported. Therefore, a fallback expression must be present</source>
        <translation>Прагма сөз айкаштарынын бири да колдолбайт. Ошондуктан, артка чегинүү сөз айкашы болушу керек</translation>
    </message>
    <message>
        <location line="+267"/>
        <source>Each name of a template parameter must be unique; %1 is duplicated.</source>
        <translation>Шаблон параметринин ар бир аты уникалдуу болушу керек; %1 дубликатталат.</translation>
    </message>
    <message>
        <location line="+129"/>
        <source>The %1-axis is unsupported in XQuery</source>
        <translation>%1-огу XQuery-да кабарланбайт</translation>
    </message>
    <message>
        <location line="+1150"/>
        <source>%1 is not a valid name for a processing-instruction.</source>
        <translation>%1 кайра иштетүү-инструкциялоо үчүн жарактуу аты эмес.</translation>
    </message>
    <message>
        <location line="-7029"/>
        <source>%1 is not a valid numeric literal.</source>
        <translation>%1 жарактуу сандык түзмө-түз эмес.</translation>
    </message>
    <message>
        <location line="+6165"/>
        <source>No function by name %1 is available.</source>
        <translation>%1 аты боюнча функция жок.</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>The namespace URI cannot be the empty string when binding to a prefix, %1.</source>
        <translation>URI аты-жөнү префикске милдеттүү болгондо бош сап боло албайт, %1.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>%1 is an invalid namespace URI.</source>
        <translation>%1 — жараксыз аты-жөнү URI.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>It is not possible to bind to the prefix %1</source>
        <translation>%1 префиксине байлоо мүмкүн эмес</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Namespace %1 can only be bound to %2 (and it is, in either case, pre-declared).</source>
        <translation>%1 аты-жөнү %2 менен гана чектелиши мүмкүн (жана бул, кандай болбосун, алдын ала жарыяланат).</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Prefix %1 can only be bound to %2 (and it is, in either case, pre-declared).</source>
        <translation>%1 префикси %2 менен гана чектелиши мүмкүн (жана бул, кандай болбосун, алдын ала жарыяланды).</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Two namespace declaration attributes have the same name: %1.</source>
        <translation>Эки аты-жөнү декларация атрибуттары бирдей аты бар: %1.</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>The namespace URI must be a constant and cannot use enclosed expressions.</source>
        <translation>URI аты-жөнү туруктуу болушу керек жана камтылган сөз айкаштарын колдоно албайт.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>An attribute by name %1 has already appeared on this element.</source>
        <translation>Бул элементте %1 аты менен атрибут пайда болду.</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>A direct element constructor is not well-formed. %1 is ended with %2.</source>
        <translation>Түз элемент конструктор жакшы калыптанган эмес. %1 %2 менен аяктайт.</translation>
    </message>
    <message>
        <location line="+458"/>
        <source>The name %1 does not refer to any schema type.</source>
        <translation>%1 аты схеманын кандайдыр бир түрүн билдирбейт.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 is an complex type. Casting to complex types is not possible. However, casting to atomic types such as %2 works.</source>
        <translation>%1 татаал түрү болуп саналат. Татаал түрлөрүнө кастинг мүмкүн эмес. Бирок, %2 сыяктуу атомдук түрлөрүнө кастинг.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1 is not an atomic type. Casting is only possible to atomic types.</source>
        <translation>%1 атомдук түрү эмес. Кастинг атомдук түрлөрү үчүн гана мүмкүн.</translation>
    </message>
    <message>
        <source>%1 is not a valid name for a processing-instruction. Therefore this name test will never match.</source>
        <translation type="obsolete">%1 不是处理指令的有效名称。因此这个名称测试永远不会匹配。</translation>
    </message>
    <message>
        <location line="+145"/>
        <location line="+71"/>
        <source>%1 is not in the in-scope attribute declarations. Note that the schema import feature is not supported.</source>
        <translation>%1 масштабдагы атрибут декларацияларында жок. Белгилей кетчү нерсе, схема импортунун өзгөчөлүгү колдолбойт.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The name of an extension expression must be in a namespace.</source>
        <translation>Кеңейтүү сөз айкашынын аты аты-жөнү орунда болушу керек.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/type/qcardinality.cpp" line="+55"/>
        <source>empty</source>
        <translation>бош</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>zero or one</source>
        <translation>нөл же бир</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>exactly one</source>
        <translation>так бир</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>one or more</source>
        <translation>бир же андан көп</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>zero or more</source>
        <translation>нөл же андан көп</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/type/qtypechecker.cpp" line="+63"/>
        <source>Required type is %1, but %2 was found.</source>
        <translation>Талап кылынган түрү % 1, ал эми %2 табылган.</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Promoting %1 to %2 may cause loss of precision.</source>
        <translation>%1 үчүн %2 илгерилетүү тактыкты жоготууга алып келиши мүмкүн.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>The focus is undefined.</source>
        <translation>Басым аныкталбайт.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/utils/qoutputvalidator.cpp" line="+86"/>
        <source>It&apos;s not possible to add attributes after any other kind of node.</source>
        <translation>Түйүндүн башка түрлөрүнөн кийин атрибуттарды кошуу мүмкүн эмес.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>An attribute by name %1 has already been created.</source>
        <translation>%1 аты менен атрибут түзүлгөн.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/utils/qxpathhelper_p.h" line="+120"/>
        <source>Only the Unicode Codepoint Collation is supported(%1). %2 is unsupported.</source>
        <translation>Юникод коддук пункту коллазиясы гана колдоого алынган (%1). %2 – бул башкарылгыс.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/api/qxmlserializer.cpp" line="+60"/>
        <source>Attribute %1 can&apos;t be serialized because it appears at the top level.</source>
        <translation>%1 атрибуты жогорку деңгээлде пайда болгондуктан серияланбайт.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/acceltree/qacceltreeresourceloader.cpp" line="+314"/>
        <source>%1 is an unsupported encoding.</source>
        <translation>%1 – бул кабарланбаган коддоо.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>%1 contains octets which are disallowed in the requested encoding %2.</source>
        <translation>%1 суралган коддоодо тыюу салынган октеттерди камтыйт %2.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The codepoint %1, occurring in %2 using encoding %3, is an invalid XML character.</source>
        <translation>%3 коддоосун колдонуу менен %2 менен пайда болгон %1 коддук пункту XML жараксыз мүнөзү болуп саналат.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qapplytemplate.cpp" line="+119"/>
        <source>Ambiguous rule match.</source>
        <translation>Түшүнүксүз эреже дал келүү.</translation>
    </message>
    <message>
        <source>In a namespace constructor, the value for a namespace value cannot be an empty string.</source>
        <translation type="obsolete">在一个命名空间构造中，命名空间的值不能为空字符串。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcomputednamespaceconstructor.cpp" line="+69"/>
        <source>In a namespace constructor, the value for a namespace cannot be an empty string.</source>
        <translation>Аты-жөнү конструктордо аты-жөнү үчүн наркы бош сап болушу мүмкүн эмес.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>The prefix must be a valid %1, which %2 is not.</source>
        <translation>Префикс %1 жарактуу болушу керек, ал %2 эмес.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>The prefix %1 cannot be bound.</source>
        <translation>%1 префиксин байлоо мүмкүн эмес.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Only the prefix %1 can be bound to %2 and vice versa.</source>
        <translation>%1 префикси гана %2 жана тескерисинче, чектелиши мүмкүн.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qevaluationcache.cpp" line="+117"/>
        <source>Circularity detected</source>
        <translation>Тегерек аныкталды</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qtemplate.cpp" line="+145"/>
        <source>The parameter %1 is required, but no corresponding %2 is supplied.</source>
        <translation>%1 параметри талап кылынат, бирок тиешелүү %2 берилбейт.</translation>
    </message>
    <message>
        <location line="-71"/>
        <source>The parameter %1 is passed, but no corresponding %2 exists.</source>
        <translation>%1 параметри өткөрүлөт, бирок тиешелүү %2 жок.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qunparsedtextfn.cpp" line="+65"/>
        <source>The URI cannot have a fragment</source>
        <translation>URI фрагменти болушу мүмкүн эмес</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qxslttokenizer.cpp" line="+519"/>
        <source>Element %1 is not allowed at this location.</source>
        <translation>%1 элементи бул жерде жол берилбейт.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Text nodes are not allowed at this location.</source>
        <translation>Бул жерде тексттик түйүндөргө жол берилбейт.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Parse error: %1</source>
        <translation>Талдоо катасы: %1</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>The value of the XSL-T version attribute must be a value of type %1, which %2 isn&apos;t.</source>
        <translation>XSL-T версиясынын атрибутунун наркы %1 түрүнүн наркы болушу керек, ал %2 эмес.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Running an XSL-T 1.0 stylesheet with a 2.0 processor.</source>
        <translation>2.0 процессору бар XSL-T 1.0 стилдер таблицасын иштетүү.</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Unknown XSL-T attribute %1.</source>
        <translation>Белгисиз XSL-T атрибуты %1.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Attribute %1 and %2 are mutually exclusive.</source>
        <translation>%1 жана %2 атрибуты өз ара өзгөчө.</translation>
    </message>
    <message>
        <location line="+166"/>
        <source>In a simplified stylesheet module, attribute %1 must be present.</source>
        <translation>Жөнөкөйлөтүлгөн стилдеги таблица модулунда %1 атрибуты болушу керек.</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>If element %1 has no attribute %2, it cannot have attribute %3 or %4.</source>
        <translation>%1 элементинин %2 атрибуты жок болсо, анда ал %3 же %4 атрибутына ээ боло албайт.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Element %1 must have at least one of the attributes %2 or %3.</source>
        <translation>%1 элементи % 2 же %3 атрибуттарынын, жок эле дегенде, бири болушу керек.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>At least one mode must be specified in the %1-attribute on element %2.</source>
        <translation>%2 элементи боюнча %1-атрибутта, жок эле дегенде, бир режим көрсөтүлүшү керек.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qmaintainingreader.cpp" line="+183"/>
        <source>Attribute %1 cannot appear on the element %2. Only the standard attributes can appear.</source>
        <translation>%1 атрибуты %2 элементинде пайда боло албайт. Стандарттык атрибуттар гана пайда болушу мүмкүн.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Attribute %1 cannot appear on the element %2. Only %3 is allowed, and the standard attributes.</source>
        <translation>%1 атрибуты %2 элементинде пайда боло албайт. Гана %3 уруксат берилет, жана стандарттык атрибуттар.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Attribute %1 cannot appear on the element %2. Allowed is %3, %4, and the standard attributes.</source>
        <translation>%1 атрибуты %2 элементинде пайда боло албайт. Уруксат берилген %3, %4 жана стандарттык атрибуттар.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Attribute %1 cannot appear on the element %2. Allowed is %3, and the standard attributes.</source>
        <translation>%1 атрибуты %2 элементинде пайда боло албайт. Уруксат берилген %3, ал эми стандарттык атрибуттар.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>XSL-T attributes on XSL-T elements must be in the null namespace, not in the XSL-T namespace which %1 is.</source>
        <translation>XSL-T элементтери боюнча XSL-T атрибуттары %1 болгон XSL-T аты-жөнү мейкиндигинде эмес, нул аты-жөнү мейкиндигинде болушу керек.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>The attribute %1 must appear on element %2.</source>
        <translation>%1 атрибуты %2 элементинде пайда болушу керек.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The element with local name %1 does not exist in XSL-T.</source>
        <translation>%1 жергиликтүү аты бар элемент XSL-T-да жок.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qxslttokenizer.cpp" line="+123"/>
        <source>Element %1 must come last.</source>
        <translation>Элемент %1 акыркы келиши керек.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>At least one %1-element must occur before %2.</source>
        <translation>Жок дегенде бир % 1-элементи % 2 чейин болушу керек.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Only one %1-element can appear.</source>
        <translation>Бир гана %1-элемент пайда болушу мүмкүн.</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>At least one %1-element must occur inside %2.</source>
        <translation>%2 ичинде, жок эле дегенде, бир %1-элемент пайда болушу керек.</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>When attribute %1 is present on %2, a sequence constructor cannot be used.</source>
        <translation>%1 атрибуты %2 боюнча берилген учурда, ырааттуу конструкторду колдонууга болбойт.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Element %1 must have either a %2-attribute or a sequence constructor.</source>
        <translation>%1 элементи %2-атрибут же ырааттуу конструктор болушу керек.</translation>
    </message>
    <message>
        <location line="+125"/>
        <source>When a parameter is required, a default value cannot be supplied through a %1-attribute or a sequence constructor.</source>
        <translation>Параметр талап кылынганда, дефолт наркын %1-атрибут же ырааттуу конструктор аркылуу камсыз кылуу мүмкүн эмес.</translation>
    </message>
    <message>
        <location line="+270"/>
        <source>Element %1 cannot have children.</source>
        <translation>%1 элементи балалуу боло албайт.</translation>
    </message>
    <message>
        <location line="+434"/>
        <source>Element %1 cannot have a sequence constructor.</source>
        <translation>Элемент %1 ырааттуу конструкторго ээ боло албайт.</translation>
    </message>
    <message>
        <location line="+86"/>
        <location line="+9"/>
        <source>The attribute %1 cannot appear on %2, when it is a child of %3.</source>
        <translation>%1 деген атрибут % 3 бала болгондо % 2 пайда боло албайт.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>A parameter in a function cannot be declared to be a tunnel.</source>
        <translation>Функциядагы параметрди туннель деп жарыялоо мүмкүн эмес.</translation>
    </message>
    <message>
        <location line="+149"/>
        <source>This processor is not Schema-aware and therefore %1 cannot be used.</source>
        <translation>Бул процессор Schema-маалым эмес, ошондуктан %1 колдонулбайт.</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Top level stylesheet elements must be in a non-null namespace, which %1 isn&apos;t.</source>
        <translation>Жогорку деңгээлдеги таблица элементтери %1 эмес, жок аты-жөнү орунда болушу керек.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The value for attribute %1 on element %2 must either be %3 or %4, not %5.</source>
        <translation>%2 элементи боюнча %1 атрибутунун наркы %5 эмес, %3 же %4 болушу керек.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Attribute %1 cannot have the value %2.</source>
        <translation>%1 атрибуты %2 наркына ээ боло албайт.</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>The attribute %1 can only appear on the first %2 element.</source>
        <translation>%1 атрибуты биринчи %2 элементинде гана пайда болушу мүмкүн.</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>At least one %1 element must appear as child of %2.</source>
        <translation>Жок дегенде бир % 1 элементи % 2 бала катары пайда болушу керек.</translation>
    </message>
</context>
<context>
    <name>VolumeSlider</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/volumeslider.cpp" line="+67"/>
        <source>Muted</source>
        <translation>Мутацияланган</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+15"/>
        <source>Volume: %1%</source>
        <translation>Көлөмү: %1%</translation>
    </message>
</context>
<context>
    <name>WebCore::PlatformScrollbar</name>
    <message>
        <source>Scroll here</source>
        <translation type="obsolete">滚动到这里</translation>
    </message>
    <message>
        <source>Left edge</source>
        <translation type="obsolete">左边缘</translation>
    </message>
    <message>
        <source>Top</source>
        <translation type="obsolete">顶部</translation>
    </message>
    <message>
        <source>Right edge</source>
        <translation type="obsolete">右边缘</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation type="obsolete">底部</translation>
    </message>
    <message>
        <source>Page left</source>
        <translation type="obsolete">左一页</translation>
    </message>
    <message>
        <source>Page up</source>
        <translation type="obsolete">上一页</translation>
    </message>
    <message>
        <source>Page right</source>
        <translation type="obsolete">右一页</translation>
    </message>
    <message>
        <source>Page down</source>
        <translation type="obsolete">下一页</translation>
    </message>
    <message>
        <source>Scroll left</source>
        <translation type="obsolete">向左滚动</translation>
    </message>
    <message>
        <source>Scroll up</source>
        <translation type="obsolete">向上滚动</translation>
    </message>
    <message>
        <source>Scroll right</source>
        <translation type="obsolete">向右滚动</translation>
    </message>
    <message>
        <source>Scroll down</source>
        <translation type="obsolete">向下滚动</translation>
    </message>
</context>
<context>
    <name>QPlatformTheme</name>
    <message>
        <source>OK</source>
        <translation>Макул</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Сактоо</translation>
    </message>
    <message>
        <source>Save All</source>
        <translation>Бардыгын сактап калуу</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Ачуу</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>Ооба</translation>
    </message>
    <message>
        <source>Yes to &amp;All</source>
        <translation>Ооба , баарына</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;amp, жок</translation>
    </message>
    <message>
        <source>N&amp;o to All</source>
        <translation>Н &amp;amp, баарына</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Аборт</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation>Ретри</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>Четке кагуу</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation>Ыргытуу</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Жардам</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Колдонуу</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>Калыбына келтирүү</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation>Дефолттарды калыбына келтирүү</translation>
    </message>
</context>
<context>
    <name>QGnomeTheme</name>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;amp, жарайды</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;amp, сактоо</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp; жабуу</translation>
    </message>
    <message>
        <source>Close without Saving</source>
        <translation>Үнөмдөбөстөн жабуу</translation>
    </message>
</context>
<context>
    <name>QWidgetTextControl</name>
    <message>
        <source>&amp;Undo</source>
        <translation>&amp;amp, Ундо</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation>&amp;amp, Редо</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation>Ку&amp;т</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>Көчүрмө</translation>
    </message>
    <message>
        <source>Copy &amp;Link Location</source>
        <translation>Көчүрмө &amp;amp, шилтеме жайгашкан жери</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation>&amp;amp, паста</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Жоготуу</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation>Бардыгын тандоо</translation>
    </message>
</context>
<context>
    <name>QPrintPropertiesDialog</name>
    <message>
        <source>Printer Properties</source>
        <translation>Принтер касиеттери</translation>
    </message>
    <message>
        <source>Job Options</source>
        <translation>Жумуш параметрлери</translation>
    </message>
    <message>
        <source>Page Setup Conflicts</source>
        <translation>Бетти орнотуу конфликттери</translation>
    </message>
    <message>
        <source>There are conflicts in page setup options. Do you want to fix them?</source>
        <translation>Барак орнотуу параметрлеринде келишпестиктер бар. Аларды оңдоону каалайсызбы?</translation>
    </message>
    <message>
        <source>Advanced Option Conflicts</source>
        <translation>Опциондун өнүккөн келишпестиктери</translation>
    </message>
    <message>
        <source>There are conflicts in some advanced options. Do you want to fix them?</source>
        <translation>Кээ бир өнүккөн варианттарда келишпестиктер бар. Аларды оңдоону каалайсызбы?</translation>
    </message>
</context>
<context>
    <name>QPrintDialog</name>
    <message>
        <source>Left to Right, Top to Bottom</source>
        <translation>Солдон оңго, жогоркудан төмөнкүгө чейин</translation>
    </message>
    <message>
        <source>Left to Right, Bottom to Top</source>
        <translation>Солдон оңго, төмөнкүдөн жогоруга</translation>
    </message>
    <message>
        <source>Right to Left, Bottom to Top</source>
        <translation>Оңдон солго, төмөнкүдөн жогоруга</translation>
    </message>
    <message>
        <source>Right to Left, Top to Bottom</source>
        <translation>Оңдон солго, жогоркудан төмөнкүгө чейин</translation>
    </message>
    <message>
        <source>Bottom to Top, Left to Right</source>
        <translation>Төмөнкүдөн жогоруга, солдон оңго</translation>
    </message>
    <message>
        <source>Bottom to Top, Right to Left</source>
        <translation>Төмөнкүдөн жогоруга, оңдон солго</translation>
    </message>
    <message>
        <source>Top to Bottom, Left to Right</source>
        <translation>Жогоркудан төмөнкүгө, солдон оңго</translation>
    </message>
    <message>
        <source>Top to Bottom, Right to Left</source>
        <translation>Жогоркудан төмөнкүгө, оңдон солго</translation>
    </message>
    <message>
        <source>1 (1x1)</source>
        <translation>1 (1х1)</translation>
    </message>
    <message>
        <source>2 (2x1)</source>
        <translation>2 (2х1)</translation>
    </message>
    <message>
        <source>4 (2x2)</source>
        <translation>4 (2х2)</translation>
    </message>
    <message>
        <source>6 (2x3)</source>
        <translation>6 (2х3)</translation>
    </message>
    <message>
        <source>9 (3x3)</source>
        <translation>9 (3х3)</translation>
    </message>
    <message>
        <source>16 (4x4)</source>
        <translation>16 (4х4)</translation>
    </message>
    <message>
        <source>All Pages</source>
        <translation>Бардык барактар</translation>
    </message>
    <message>
        <source>Odd Pages</source>
        <translation>Табиттүү барактар</translation>
    </message>
    <message>
        <source>Even Pages</source>
        <translation>Жадагалса барактар</translation>
    </message>
</context>
<context>
    <name>QPageSize</name>
    <message>
        <source>A0</source>
        <translation>А0</translation>
    </message>
    <message>
        <source>A1</source>
        <translation>А1</translation>
    </message>
    <message>
        <source>A2</source>
        <translation>А2</translation>
    </message>
    <message>
        <source>A3</source>
        <translation>А3</translation>
    </message>
    <message>
        <source>A4</source>
        <translation>А4</translation>
    </message>
    <message>
        <source>A5</source>
        <translation>А5</translation>
    </message>
    <message>
        <source>A6</source>
        <translation>А6</translation>
    </message>
    <message>
        <source>A7</source>
        <translation>А7</translation>
    </message>
    <message>
        <source>A8</source>
        <translation>А8</translation>
    </message>
    <message>
        <source>A9</source>
        <translation>А9</translation>
    </message>
    <message>
        <source>A10</source>
        <translation>А10</translation>
    </message>
    <message>
        <source>B0</source>
        <translation>Б0</translation>
    </message>
    <message>
        <source>B1</source>
        <translation>В1</translation>
    </message>
    <message>
        <source>B2</source>
        <translation>В2</translation>
    </message>
    <message>
        <source>B3</source>
        <translation>В3</translation>
    </message>
    <message>
        <source>B4</source>
        <translation>В4</translation>
    </message>
    <message>
        <source>B5</source>
        <translation>В5</translation>
    </message>
    <message>
        <source>B6</source>
        <translation>В6</translation>
    </message>
    <message>
        <source>B7</source>
        <translation>В7</translation>
    </message>
    <message>
        <source>B8</source>
        <translation>В8</translation>
    </message>
    <message>
        <source>B9</source>
        <translation>Б9</translation>
    </message>
    <message>
        <source>B10</source>
        <translation>В10</translation>
    </message>
    <message>
        <source>Executive (7.5 x 10 in)</source>
        <translation>Аткаруучу (7,5 × 10 в)</translation>
    </message>
    <message>
        <source>Executive (7.25 x 10.5 in)</source>
        <translation>Аткаруучу (7.25 × 10,5 в)</translation>
    </message>
    <message>
        <source>Folio (8.27 x 13 in)</source>
        <translation>Фолио (8.27 × 13 в)</translation>
    </message>
    <message>
        <source>Legal</source>
        <translation>Мыйзамдуу</translation>
    </message>
    <message>
        <source>Letter / ANSI A</source>
        <translation>Кат / АНСИ А</translation>
    </message>
    <message>
        <source>Tabloid / ANSI B</source>
        <translation>Таблица / АНСИ Б</translation>
    </message>
    <message>
        <source>Ledger / ANSI B</source>
        <translation>Леджер / АНСИ Б</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>Салт</translation>
    </message>
    <message>
        <source>A3 Extra</source>
        <translation>А3 Кошумча</translation>
    </message>
    <message>
        <source>A4 Extra</source>
        <translation>А4 Кошумча</translation>
    </message>
    <message>
        <source>A4 Plus</source>
        <translation>А4 Плюс</translation>
    </message>
    <message>
        <source>A4 Small</source>
        <translation>А4 Кичинекей</translation>
    </message>
    <message>
        <source>A5 Extra</source>
        <translation>А5 Кошумча</translation>
    </message>
    <message>
        <source>B5 Extra</source>
        <translation>В5 Кошумча</translation>
    </message>
    <message>
        <source>JIS B0</source>
        <translation>JIS B0</translation>
    </message>
    <message>
        <source>JIS B1</source>
        <translation>JIS B1</translation>
    </message>
    <message>
        <source>JIS B2</source>
        <translation>JIS B2</translation>
    </message>
    <message>
        <source>JIS B3</source>
        <translation>JIS B3</translation>
    </message>
    <message>
        <source>JIS B4</source>
        <translation>JIS B4</translation>
    </message>
    <message>
        <source>JIS B5</source>
        <translation>JIS B5</translation>
    </message>
    <message>
        <source>JIS B6</source>
        <translation>JIS B6</translation>
    </message>
    <message>
        <source>JIS B7</source>
        <translation>JIS B7</translation>
    </message>
    <message>
        <source>JIS B8</source>
        <translation>JIS B8</translation>
    </message>
    <message>
        <source>JIS B9</source>
        <translation>JIS B9</translation>
    </message>
    <message>
        <source>JIS B10</source>
        <translation>JIS B10</translation>
    </message>
    <message>
        <source>ANSI C</source>
        <translation>АНСИ С</translation>
    </message>
    <message>
        <source>ANSI D</source>
        <translation>АНСИ Д</translation>
    </message>
    <message>
        <source>ANSI E</source>
        <translation>АНСИ Е</translation>
    </message>
    <message>
        <source>Legal Extra</source>
        <translation>Юридикалык кошумча</translation>
    </message>
    <message>
        <source>Letter Extra</source>
        <translation>Кошумча кат</translation>
    </message>
    <message>
        <source>Letter Plus</source>
        <translation>Кат Плюс</translation>
    </message>
    <message>
        <source>Letter Small</source>
        <translation>Кичинекей кат</translation>
    </message>
    <message>
        <source>Tabloid Extra</source>
        <translation>Таблоид экстра</translation>
    </message>
    <message>
        <source>Architect A</source>
        <translation>Архитектор А</translation>
    </message>
    <message>
        <source>Architect B</source>
        <translation>Архитектор Б</translation>
    </message>
    <message>
        <source>Architect C</source>
        <translation>Архитектор С</translation>
    </message>
    <message>
        <source>Architect D</source>
        <translation>Архитектор Д</translation>
    </message>
    <message>
        <source>Architect E</source>
        <translation>Архитектор Е</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Эскертүү</translation>
    </message>
    <message>
        <source>Quarto</source>
        <translation>Кварто</translation>
    </message>
    <message>
        <source>Statement</source>
        <translation>Билдирүү</translation>
    </message>
    <message>
        <source>Super A</source>
        <translation>Супер А</translation>
    </message>
    <message>
        <source>Super B</source>
        <translation>Супер Б</translation>
    </message>
    <message>
        <source>Postcard</source>
        <translation>Открытка</translation>
    </message>
    <message>
        <source>Double Postcard</source>
        <translation>Кош открытка</translation>
    </message>
    <message>
        <source>PRC 16K</source>
        <translation>КР 16К</translation>
    </message>
    <message>
        <source>PRC 32K</source>
        <translation>КР 32К</translation>
    </message>
    <message>
        <source>PRC 32K Big</source>
        <translation>КРК 32К чоң</translation>
    </message>
    <message>
        <source>Fan-fold US (14.875 x 11 in)</source>
        <translation>Фан-бүктөлгөн АКШ (14.875 × 11 в)</translation>
    </message>
    <message>
        <source>Fan-fold German (8.5 x 12 in)</source>
        <translation>Фан-бүктөлгөн немис (8,5 × 12 в)</translation>
    </message>
    <message>
        <source>Fan-fold German Legal (8.5 x 13 in)</source>
        <translation>Фан-бүктөлгөн Немис Юридикалык (8.5 × 13 в)</translation>
    </message>
    <message>
        <source>Envelope B4</source>
        <translation>В4 конверт</translation>
    </message>
    <message>
        <source>Envelope B5</source>
        <translation>В5 конверт</translation>
    </message>
    <message>
        <source>Envelope B6</source>
        <translation>В6 конверт</translation>
    </message>
    <message>
        <source>Envelope C0</source>
        <translation>С0 конверт</translation>
    </message>
    <message>
        <source>Envelope C1</source>
        <translation>С1 конверт</translation>
    </message>
    <message>
        <source>Envelope C2</source>
        <translation>С2 конверт</translation>
    </message>
    <message>
        <source>Envelope C3</source>
        <translation>С3 конверт</translation>
    </message>
    <message>
        <source>Envelope C4</source>
        <translation>С4 конверт</translation>
    </message>
    <message>
        <source>Envelope C5</source>
        <translation>С5 конверт</translation>
    </message>
    <message>
        <source>Envelope C6</source>
        <translation>С6 конверт</translation>
    </message>
    <message>
        <source>Envelope C65</source>
        <translation>С65 конверт</translation>
    </message>
    <message>
        <source>Envelope C7</source>
        <translation>С7 конверт</translation>
    </message>
    <message>
        <source>Envelope DL</source>
        <translation>DL конверт</translation>
    </message>
    <message>
        <source>Envelope US 9</source>
        <translation>КОНВЕРТ АКШ 9</translation>
    </message>
    <message>
        <source>Envelope US 10</source>
        <translation>КОНВЕРТ АКШ 10</translation>
    </message>
    <message>
        <source>Envelope US 11</source>
        <translation>КОНВЕРТ АКШ 11</translation>
    </message>
    <message>
        <source>Envelope US 12</source>
        <translation>КОНВЕРТ АКШ 12</translation>
    </message>
    <message>
        <source>Envelope US 14</source>
        <translation>КОНВЕРТ АКШ 14</translation>
    </message>
    <message>
        <source>Envelope Monarch</source>
        <translation>Монарх конверт</translation>
    </message>
    <message>
        <source>Envelope Personal</source>
        <translation>Жеке конверт</translation>
    </message>
    <message>
        <source>Envelope Chou 3</source>
        <translation>Конверт Чоу 3</translation>
    </message>
    <message>
        <source>Envelope Chou 4</source>
        <translation>Конверт Чоу 4</translation>
    </message>
    <message>
        <source>Envelope Invite</source>
        <translation>Конверт чакыруу</translation>
    </message>
    <message>
        <source>Envelope Italian</source>
        <translation>Конверт итальянча</translation>
    </message>
    <message>
        <source>Envelope Kaku 2</source>
        <translation>Каку конверти</translation>
    </message>
    <message>
        <source>Envelope Kaku 3</source>
        <translation>Каку конверти</translation>
    </message>
    <message>
        <source>Envelope PRC 1</source>
        <translation>Конверт ПРК 1</translation>
    </message>
    <message>
        <source>Envelope PRC 2</source>
        <translation>Конверт ПРК 2</translation>
    </message>
    <message>
        <source>Envelope PRC 3</source>
        <translation>Конверт ПРК 3</translation>
    </message>
    <message>
        <source>Envelope PRC 4</source>
        <translation>Конверт ПРК 4</translation>
    </message>
    <message>
        <source>Envelope PRC 5</source>
        <translation>Конверт ПРК 5</translation>
    </message>
    <message>
        <source>Envelope PRC 6</source>
        <translation>Конверт ПРК 6</translation>
    </message>
    <message>
        <source>Envelope PRC 7</source>
        <translation>Конверт ПРК 7</translation>
    </message>
    <message>
        <source>Envelope PRC 8</source>
        <translation>Конверт ПРК 8</translation>
    </message>
    <message>
        <source>Envelope PRC 9</source>
        <translation>Конверт ПРК 9</translation>
    </message>
    <message>
        <source>Envelope PRC 10</source>
        <translation>Конверт ПРК 10</translation>
    </message>
    <message>
        <source>Envelope You 4</source>
        <translation>Конверт Сиз 4</translation>
    </message>
</context>
</TS>
