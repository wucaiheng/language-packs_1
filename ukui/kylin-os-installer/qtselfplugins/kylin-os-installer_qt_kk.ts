<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="../src/gui/kernel/qapplication.cpp" line="+2316"/>
        <source>Services</source>
        <translation>Қызметтер</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide %1</source>
        <translation>% 1 дегенді жасыру</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide Others</source>
        <translation>Басқалар жасырылсын</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show All</source>
        <translation>Барлығын көрсету</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Preferences...</source>
        <translation>Преференциялар...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit %1</source>
        <translation>% 1 дегенді шығу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>About %1</source>
        <translation>% 1 дегенге жуық</translation>
    </message>
</context>
<context>
    <name>AudioOutput</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/audiooutput.cpp" line="+375"/>
        <source>&lt;html&gt;The audio playback device &lt;b&gt;%1&lt;/b&gt; does not work.&lt;br/&gt;Falling back to &lt;b&gt;%2&lt;/b&gt;.&lt;/html&gt;</source>
        <translation>&lt;html&gt; Дыбысты ойнату құрылғысы &lt;b&gt;% 1&lt;/b&gt; жұмыс істемейді. &lt;br/&gt; &lt;b&gt;% 2&lt;/b&gt; дегенге қайта түсу. &lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;Switching to the audio playback device &lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;which just became available and has higher preference.&lt;/html&gt;</source>
        <translation>&lt;html&gt; Дыбысты ойнату құрылғысына ауысу &lt;b&gt;% 1&lt;/b&gt;&lt;br/&gt;, ол жай ғана қол жетімді болды және жоғары артықшылыққа ие болды. &lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Revert back to device &apos;%1&apos;</source>
        <translation>&apos;% 1&apos; құрылғысына қайта оралу</translation>
    </message>
</context>
<context>
    <name>CloseButton</name>
    <message>
        <location filename="../src/gui/widgets/qtabbar.cpp" line="+2251"/>
        <source>Close Tab</source>
        <translation>Қойындыны жабу</translation>
    </message>
</context>
<context>
    <name>Phonon::</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/phononnamespace.cpp" line="+55"/>
        <source>Notifications</source>
        <translation>Ескертулер</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Music</source>
        <translation>Музыка</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Video</source>
        <translation>Бейне</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Communication</source>
        <translation>Байланыс</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Games</source>
        <translation>Ойындар</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Accessibility</source>
        <translation>Арнайы мүмкіндіктер</translation>
    </message>
</context>
<context>
    <name>Phonon::Gstreamer::Backend</name>
    <message>
        <location filename="../src/3rdparty/phonon/gstreamer/backend.cpp" line="+171"/>
        <source>Warning: You do not seem to have the package gstreamer0.10-plugins-good installed.
          Some video features have been disabled.</source>
        <translation>Назар аударыңыз: сізде gstreamer0.10-плагин модульдері орнатылмаған сияқты.
          Кейбір бейне мүмкіндіктері ажыратылды.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Warning: You do not seem to have the base GStreamer plugins installed.
          All audio and video support has been disabled</source>
        <translation>Назар аударыңыз: Негізгі GStreamer плагин модульдері орнатылмаған сияқты.
          Барлық аудио және бейне қолдау ажыратылды</translation>
    </message>
</context>
<context>
    <name>Phonon::Gstreamer::MediaObject</name>
    <message>
        <location filename="../src/3rdparty/phonon/gstreamer/mediaobject.cpp" line="+90"/>
        <source>Cannot start playback. 

Check your Gstreamer installation and make sure you 
have libgstreamer-plugins-base installed.</source>
        <translation>Ойнатуды бастау мүмкін емес. 

Gstreamer орнатуын тексеріңіз және сізге көз жеткізіңіз 
libgstreamer-plugins-base орнатылған.</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>A required codec is missing. You need to install the following codec(s) to play this content: %0</source>
        <translation>Қажетті кодек жоқ. Осы мазмұнды ойнату үшін келесі кодек(лер) орнату керек:% 0</translation>
    </message>
    <message>
        <location line="+676"/>
        <location line="+8"/>
        <location line="+15"/>
        <location line="+9"/>
        <location line="+6"/>
        <location line="+19"/>
        <location line="+335"/>
        <location line="+24"/>
        <source>Could not open media source.</source>
        <translation>Медиа көзі ашылмады.</translation>
    </message>
    <message>
        <location line="-403"/>
        <source>Invalid source type.</source>
        <translation>Дереккөз түрі жарамсыз.</translation>
    </message>
    <message>
        <location line="+377"/>
        <source>Could not locate media source.</source>
        <translation>Медиа көзі табылмады.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not open audio device. The device is already in use.</source>
        <translation>Дыбыстық құрылғы ашылмады. Құрылғы қазірдің өзінде қолданыла тұр.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Could not decode media source.</source>
        <translation>Медиа көзін декодтау мүмкін болмады.</translation>
    </message>
</context>
<context>
    <name>Phonon::VolumeSlider</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/volumeslider.cpp" line="+42"/>
        <location line="+18"/>
        <source>Volume: %1%</source>
        <translation>Көлемі:% 1%</translation>
    </message>
    <message>
        <location line="-15"/>
        <location line="+18"/>
        <location line="+54"/>
        <source>Use this slider to adjust the volume. The leftmost position is 0%, the rightmost is %1%</source>
        <translation>Дыбысты реттеу үшін осы жүгірткіні пайдаланыңыз. Сол жақ позиция 0%, ең оң жағы % 1%</translation>
    </message>
</context>
<context>
    <name>Q3Accel</name>
    <message>
        <location filename="../src/qt3support/other/q3accel.cpp" line="+481"/>
        <source>%1, %2 not defined</source>
        <translation>% 1, % 2 анықталмаған</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Ambiguous %1 not handled</source>
        <translation>% 1 деген екі ұшты емес</translation>
    </message>
</context>
<context>
    <name>Q3DataTable</name>
    <message>
        <location filename="../src/qt3support/sql/q3datatable.cpp" line="+285"/>
        <source>True</source>
        <translation>Шын</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>False</source>
        <translation>Жалған</translation>
    </message>
    <message>
        <location line="+505"/>
        <source>Insert</source>
        <translation>Кірістіру</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Update</source>
        <translation>Жаңарту</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>Өшіру</translation>
    </message>
</context>
<context>
    <name>Q3FileDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="+864"/>
        <source>Copy or Move a File</source>
        <translation>Файлды көшіру немесе жылжыту</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Read: %1</source>
        <translation>Оқыңыз:% 1</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+30"/>
        <source>Write: %1</source>
        <translation>Жазу:% 1</translation>
    </message>
    <message>
        <location line="-22"/>
        <location line="+1575"/>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <location line="-157"/>
        <location line="+49"/>
        <location line="+2153"/>
        <location filename="../src/qt3support/dialogs/q3filedialog_mac.cpp" line="+110"/>
        <source>All Files (*)</source>
        <translation>Барлық файлдар (*)</translation>
    </message>
    <message>
        <location line="-2089"/>
        <source>Name</source>
        <translation>Атауы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Size</source>
        <translation>Өлшемі</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <translation>Түрі</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>Күні</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Attributes</source>
        <translation>Атрибуттар</translation>
    </message>
    <message>
        <location line="+35"/>
        <location line="+2031"/>
        <source>&amp;OK</source>
        <translation>Ð Ð°Ò</translation>
    </message>
    <message>
        <location line="-1991"/>
        <source>Look &amp;in:</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1981"/>
        <location line="+16"/>
        <source>File &amp;name:</source>
        <translation>Файлдың атауы:</translation>
    </message>
    <message>
        <location line="-1996"/>
        <source>File &amp;type:</source>
        <translation>Файлдың түрі:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Back</source>
        <translation>Артқа</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>One directory up</source>
        <translation>Бір каталог жоғары</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Create New Folder</source>
        <translation>Жаңа қалтаны жасау</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>List View</source>
        <translation>Тізім көрінісі</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Detail View</source>
        <translation>Егжей- тегжейлі көрініс</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Preview File Info</source>
        <translation>Файл ақпаратын алдын ала қарау</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Preview File Contents</source>
        <translation>Файл мазмұнын алдын ала қарау</translation>
    </message>
    <message>
        <location line="+88"/>
        <source>Read-write</source>
        <translation>Оқу-жазу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Read-only</source>
        <translation>Тек оқуға арналған</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Write-only</source>
        <translation>Тек жазу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Inaccessible</source>
        <translation>Қол жетімсіз</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Symlink to File</source>
        <translation>Файлға симлинк</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Symlink to Directory</source>
        <translation>Каталогқа симлинк</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Symlink to Special</source>
        <translation>Арнайыға симлинк</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dir</source>
        <translation>Дир</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Special</source>
        <translation>Арнайы</translation>
    </message>
    <message>
        <location line="+704"/>
        <location line="+2100"/>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="+337"/>
        <source>Open</source>
        <translation>Ашу</translation>
    </message>
    <message>
        <location line="-1990"/>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="+84"/>
        <source>Save As</source>
        <translation>Басқаша сақтау</translation>
    </message>
    <message>
        <location line="+642"/>
        <location line="+5"/>
        <location line="+355"/>
        <source>&amp;Open</source>
        <translation>Ашу</translation>
    </message>
    <message>
        <location line="-357"/>
        <location line="+341"/>
        <source>&amp;Save</source>
        <translation>Сақтау</translation>
    </message>
    <message>
        <location line="-334"/>
        <source>&amp;Rename</source>
        <translation>Атын өзгерту</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Delete</source>
        <translation>Өшіру</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>R&amp;eload</source>
        <translation>R&gt; жүктеу</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Sort by &amp;Name</source>
        <translation>&amp; Атауы бойынша сұрыптау</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sort by &amp;Size</source>
        <translation>&amp; Өлшемі бойынша сұрыптау</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sort by &amp;Date</source>
        <translation>&amp; Күн бойынша сұрыптау</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Unsorted</source>
        <translation>&lt; &gt; &gt; &gt; &gt; &gt;</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Sort</source>
        <translation>Сұрыптау</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Show &amp;hidden files</source>
        <translation>&amp; Жасырын файлдар көрсетілсін</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>the file</source>
        <translation>файл</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>the directory</source>
        <translation>каталог</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>the symlink</source>
        <translation>симлинк</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete %1</source>
        <translation>% 1 дегенді өшіру</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;qt&gt;Are you sure you wish to delete %1 &quot;%2&quot;?&lt;/qt&gt;</source>
        <translation>&lt;qt&gt; % 1 &quot;% 2&quot; дегенді жойғыңыз келе ме? &lt;/qt&gt;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Yes</source>
        <translation>Иә</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;No</source>
        <translation>&gt; Жоқ</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>New Folder 1</source>
        <translation>Жаңа қапшық 1</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New Folder</source>
        <translation>Жаңа қапшық</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New Folder %1</source>
        <translation>% 1 жаңа қапшығы</translation>
    </message>
    <message>
        <location line="+98"/>
        <source>Find Directory</source>
        <translation>Каталогты табу</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+108"/>
        <source>Directories</source>
        <translation>Каталогтар</translation>
    </message>
    <message>
        <location line="-2"/>
        <source>Directory:</source>
        <translation>Каталог:</translation>
    </message>
    <message>
        <location line="+40"/>
        <location line="+1110"/>
        <source>Error</source>
        <translation>Қате</translation>
    </message>
    <message>
        <location line="-1109"/>
        <source>%1
File not found.
Check path and filename.</source>
        <translation>%1
Файл табылмады.
Жол мен файл атауын тексеріңіз.</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="-289"/>
        <source>All Files (*.*)</source>
        <translation>Барлық файлдар (*.*)</translation>
    </message>
    <message>
        <location line="+375"/>
        <source>Open </source>
        <translation>Ашу </translation>
    </message>
    <message>
        <location line="+155"/>
        <source>Select a Directory</source>
        <translation>Каталогты таңдау</translation>
    </message>
</context>
<context>
    <name>Q3LocalFs</name>
    <message>
        <location filename="../src/qt3support/network/q3localfs.cpp" line="+130"/>
        <location line="+10"/>
        <source>Could not read directory
%1</source>
        <translation>Каталог оқылмады
%1</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Could not create directory
%1</source>
        <translation>Каталог жасалмады
%1</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Could not remove file or directory
%1</source>
        <translation>Файл немесе каталог өшірілмеді
%1</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Could not rename
%1
to
%2</source>
        <translation>Атын өзгерту мүмкін болмады
%1
to
%2</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Could not open
%1</source>
        <translation>Ашылмады
%1</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Could not write
%1</source>
        <translation>Жазу болмады
%1</translation>
    </message>
</context>
<context>
    <name>Q3MainWindow</name>
    <message>
        <location filename="../src/qt3support/widgets/q3mainwindow.cpp" line="+2051"/>
        <source>Line up</source>
        <translation>Сапқа тұрғызылсын</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Customize...</source>
        <translation>Реттеу...</translation>
    </message>
</context>
<context>
    <name>Q3NetworkProtocol</name>
    <message>
        <location filename="../src/qt3support/network/q3networkprotocol.cpp" line="+854"/>
        <source>Operation stopped by the user</source>
        <translation>Пайдаланушы тоқтатқан операция</translation>
    </message>
</context>
<context>
    <name>Q3ProgressDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3progressdialog.cpp" line="+224"/>
        <location line="+61"/>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
</context>
<context>
    <name>Q3TabDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3tabdialog.cpp" line="+189"/>
        <location line="+814"/>
        <source>OK</source>
        <translation>Жақсы</translation>
    </message>
    <message>
        <location line="-356"/>
        <source>Apply</source>
        <translation>Қолдану</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Help</source>
        <translation>Анықтама</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Defaults</source>
        <translation>Әдепкілер</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
</context>
<context>
    <name>Q3TextEdit</name>
    <message>
        <location filename="../src/qt3support/text/q3textedit.cpp" line="+5429"/>
        <source>&amp;Undo</source>
        <translation>&gt; Болдырмау</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Redo</source>
        <translation>&gt; Редо</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cu&amp;t</source>
        <translation>Cu&gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Copy</source>
        <translation>&amp; Көшіру</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Paste</source>
        <translation>&gt; Қою</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation>Тазалау</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+2"/>
        <source>Select All</source>
        <translation>Барлығын таңдау</translation>
    </message>
</context>
<context>
    <name>Q3TitleBar</name>
    <message>
        <location filename="../src/plugins/accessible/compat/q3complexwidgets.cpp" line="+246"/>
        <source>System</source>
        <translation>Жүйе</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restore up</source>
        <translation>Қалпына келтіру</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Minimize</source>
        <translation>Кішірейту</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restore down</source>
        <translation>Қалпына келтіру</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Maximize</source>
        <translation>Барынша көбейту</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close</source>
        <translation>Жабу</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Contains commands to manipulate the window</source>
        <translation>Терезені манипуляциялауға арналған пәрмендер бар</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Puts a minimized back to normal</source>
        <translation>Барынша азайтылғанды қалыпты жағдайға қайта қояды</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Moves the window out of the way</source>
        <translation>Терезені жолдан жылжыту</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Puts a maximized window back to normal</source>
        <translation>Барынша көп терезені қалыпты жағдайға қайта қояды</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Makes the window full screen</source>
        <translation>Терезені толық экранға айналдыру</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Closes the window</source>
        <translation>Терезені жабу</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Displays the name of the window and contains controls to manipulate it</source>
        <translation>Терезе атауын көрсетеді және оны манипуляциялау үшін басқару элементтерін қамтиды</translation>
    </message>
</context>
<context>
    <name>Q3ToolBar</name>
    <message>
        <location filename="../src/qt3support/widgets/q3toolbar.cpp" line="+692"/>
        <source>More...</source>
        <translation>Қосымша...</translation>
    </message>
</context>
<context>
    <name>Q3UrlOperator</name>
    <message>
        <location filename="../src/qt3support/network/q3urloperator.cpp" line="+386"/>
        <location line="+260"/>
        <location line="+4"/>
        <source>The protocol `%1&apos; is not supported</source>
        <translation>&apos;% 1&apos; хаттамасы қолдау көрсетілмеген</translation>
    </message>
    <message>
        <location line="-260"/>
        <source>The protocol `%1&apos; does not support listing directories</source>
        <translation>&apos;% 1&apos; хаттамасы тізім каталогтарын қолдамайды</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support creating new directories</source>
        <translation>&apos;% 1&apos; протоколы жаңа каталогтар жасауды қолдамайды</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support removing files or directories</source>
        <translation>&apos;% 1&apos; хаттамасы файлдарды немесе каталогтарды жоюды қолдамайды</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support renaming files or directories</source>
        <translation>&apos;% 1&apos; хаттамасы файлдардың немесе каталогтардың атын өзгертуді қолдамайды</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support getting files</source>
        <translation>&apos;% 1&apos; хаттамасы файлдарды алуды қолдамайды</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support putting files</source>
        <translation>&apos;% 1&apos; хаттамасы файлдарды қоюды қолдамайды</translation>
    </message>
    <message>
        <location line="+243"/>
        <location line="+4"/>
        <source>The protocol `%1&apos; does not support copying or moving files or directories</source>
        <translation>&apos;% 1&apos; хаттамасы файлдарды немесе каталогтарды көшіруді немесе жылжытуды қолдамайды</translation>
    </message>
    <message>
        <location line="+237"/>
        <location line="+1"/>
        <source>(unknown)</source>
        <translation>(беймәлім)</translation>
    </message>
</context>
<context>
    <name>Q3Wizard</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3wizard.cpp" line="+177"/>
        <source>&amp;Cancel</source>
        <translation>&amp; Бас тарту</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt; &amp;Back</source>
        <translation>&lt; артқа</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Next &gt;</source>
        <translation>Келесі &gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Finish</source>
        <translation>&gt; Аяқтау</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Help</source>
        <translation>Анықтама</translation>
    </message>
</context>
<context>
    <name>QAbstractSocket</name>
    <message>
        <location filename="../src/network/socket/qabstractsocket.cpp" line="+868"/>
        <location filename="../src/network/socket/qhttpsocketengine.cpp" line="+615"/>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="+657"/>
        <location line="+26"/>
        <source>Host not found</source>
        <translation>Хост табылмады</translation>
    </message>
    <message>
        <location line="+50"/>
        <location filename="../src/network/socket/qhttpsocketengine.cpp" line="+3"/>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="+4"/>
        <source>Connection refused</source>
        <translation>Қосылымнан бас тартылды</translation>
    </message>
    <message>
        <location line="+141"/>
        <source>Connection timed out</source>
        <translation>Қосылым уақыты бітті</translation>
    </message>
    <message>
        <location line="-547"/>
        <location line="+787"/>
        <location line="+208"/>
        <source>Operation on socket is not supported</source>
        <translation>Розеткадағы операцияға қолдау көрсетілмеген</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Socket operation timed out</source>
        <translation>Розетка операциясы уақыты</translation>
    </message>
    <message>
        <location line="+380"/>
        <source>Socket is not connected</source>
        <translation>Розетка қосылмаған</translation>
    </message>
    <message>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="-8"/>
        <source>Network unreachable</source>
        <translation>Желіге қол жетімді емес</translation>
    </message>
</context>
<context>
    <name>QAbstractSpinBox</name>
    <message>
        <location filename="../src/gui/widgets/qabstractspinbox.cpp" line="+1199"/>
        <source>&amp;Step up</source>
        <translation>&gt; Қадам басу</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Step &amp;down</source>
        <translation>Қадам &gt;</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>&amp;Select All</source>
        <translation>Барлығын таңдау</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/gui/accessible/qaccessibleobject.cpp" line="+376"/>
        <source>Activate</source>
        <translation>Белсендіру</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.h" line="+352"/>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation>Орындалатын &apos;% 1&apos; Qt% 2 дегенді қажет етеді, табылған Qt% 3.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Incompatible Qt Library Error</source>
        <translation>Үйлеспейтін Qt кітапхана қатесі</translation>
    </message>
    <message>
        <location filename="../src/gui/accessible/qaccessibleobject.cpp" line="+2"/>
        <source>Activates the program&apos;s main window</source>
        <translation>Бағдарламаның негізгі терезесін белсендіру</translation>
    </message>
</context>
<context>
    <name>QAxSelect</name>
    <message>
        <location filename="../src/activeqt/container/qaxselect.ui"/>
        <source>Select ActiveX Control</source>
        <translation>ActiveX басқару элементін таңдау</translation>
    </message>
    <message>
        <location/>
        <source>OK</source>
        <translation>Жақсы</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Cancel</source>
        <translation>&amp; Бас тарту</translation>
    </message>
    <message>
        <location/>
        <source>COM &amp;Object:</source>
        <translation>COM &gt; нысан:</translation>
    </message>
</context>
<context>
    <name>QCheckBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="+114"/>
        <source>Uncheck</source>
        <translation>Құсбелгіні алып тастаған</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Check</source>
        <translation>Тексеру</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Toggle</source>
        <translation>Қайыру</translation>
    </message>
</context>
<context>
    <name>QColorDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="+1253"/>
        <source>Hu&amp;e:</source>
        <translation>Hu&gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Sat:</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Val:</source>
        <translation>&amp;val:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Red:</source>
        <translation>&gt; Қызыл:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Green:</source>
        <translation>&gt; Жасыл</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bl&amp;ue:</source>
        <translation>Bl&gt; ue:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A&amp;lpha channel:</source>
        <translation>A&amp;lpha арнасы:</translation>
    </message>
    <message>
        <location line="+101"/>
        <source>Select Color</source>
        <translation>Түсті таңдау</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>&amp;Basic colors</source>
        <translation>Негізгі түстер</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Custom colors</source>
        <translation>&amp; Реттелетін түстер</translation>
    </message>
    <message>
        <source>Cursor at %1, %2
Press ESC to cancel</source>
        <translation>% 1, % 2 курсоры
Бас тарту үшін ESC пернесін басыңыз</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Add to Custom Colors</source>
        <translation>Реттелетін түстерге қосу</translation>
    </message>
    <message>
        <source>Select color</source>
        <translation type="obsolete">选择颜色</translation>
    </message>
    <message>
        <source>&amp;Pick Screen Color</source>
        <translation>Экран түсін таңдау</translation>
    </message>
</context>
<context>
    <name>QComboBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="+1771"/>
        <location line="+65"/>
        <source>Open</source>
        <translation>Ашу</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qitemeditorfactory.cpp" line="+544"/>
        <source>False</source>
        <translation>Жалған</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>True</source>
        <translation>Шын</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="+0"/>
        <source>Close</source>
        <translation>Жабу</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <source>%1: permission denied</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：权限被拒绝</translation>
    </message>
    <message>
        <source>%1: already exists</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：已经存在</translation>
    </message>
    <message>
        <source>%1: doesn&apos;t exists</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：不存在</translation>
    </message>
    <message>
        <source>%1: out of resources</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：资源耗尽了</translation>
    </message>
    <message>
        <source>%1: unknown error %2</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：未知错误 %2</translation>
    </message>
    <message>
        <location filename="../src/corelib/kernel/qsystemsemaphore_unix.cpp" line="+119"/>
        <source>%1: key is empty</source>
        <comment>QSystemSemaphore</comment>
        <translation>% 1: кілт бос</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1: unable to make key</source>
        <comment>QSystemSemaphore</comment>
        <translation>% 1: кілтті жасау мүмкін емес</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1: ftok failed</source>
        <comment>QSystemSemaphore</comment>
        <translation>% 1: ftok жаңылысы</translation>
    </message>
</context>
<context>
    <name>QDB2Driver</name>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="+1276"/>
        <source>Unable to connect</source>
        <translation>Қосылмады</translation>
    </message>
    <message>
        <location line="+303"/>
        <source>Unable to commit transaction</source>
        <translation>Мәміле жасау мүмкін емес</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback transaction</source>
        <translation>Транзакцияны кері қайтару мүмкін емес</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to set autocommit</source>
        <translation>Автокомит орнатылмады</translation>
    </message>
</context>
<context>
    <name>QDB2Result</name>
    <message>
        <location line="-1043"/>
        <location line="+243"/>
        <source>Unable to execute statement</source>
        <translation>Мәлімдемені орындау мүмкін емес</translation>
    </message>
    <message>
        <location line="-206"/>
        <source>Unable to prepare statement</source>
        <translation>Мәлімдеме дайындалмады</translation>
    </message>
    <message>
        <location line="+196"/>
        <source>Unable to bind variable</source>
        <translation>Айнымалыны байланыстыру мүмкін емес</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Unable to fetch record %1</source>
        <translation>% 1 жазбасын алу мүмкін емес</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to fetch next</source>
        <translation>Келесіні алу мүмкін емес</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to fetch first</source>
        <translation>Алдымен алу мүмкін емес</translation>
    </message>
</context>
<context>
    <name>QDateTimeEdit</name>
    <message>
        <location filename="../src/gui/widgets/qdatetimeedit.cpp" line="+2295"/>
        <source>AM</source>
        <translation>АМ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>am</source>
        <translation>ам</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>PM</source>
        <translation>Қр Премьер-Министрі</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>pm</source>
        <translation>pm</translation>
    </message>
</context>
<context>
    <name>QDial</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+951"/>
        <source>QDial</source>
        <translation>QDial</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>SpeedoMeter</source>
        <translation>СпидоМетр</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>SliderHandle</source>
        <translation>Жүгірткі</translation>
    </message>
</context>
<context>
    <name>QDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qdialog.cpp" line="+597"/>
        <source>What&apos;s This?</source>
        <translation>Бұл не?</translation>
    </message>
    <message>
        <location line="-115"/>
        <source>Done</source>
        <translation>Орындалды</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.cpp" line="+1866"/>
        <location line="+464"/>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="+561"/>
        <source>OK</source>
        <translation>Жақсы</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="+3"/>
        <source>Save</source>
        <translation>Сақтау</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Save</source>
        <translation>Сақтау</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open</source>
        <translation>Ашу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Cancel</source>
        <translation>&amp; Бас тарту</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Close</source>
        <translation>Жабу</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Close</source>
        <translation>Жабу@ action: inmenu</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Apply</source>
        <translation>Қолдану</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reset</source>
        <translation>Ысыру</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Help</source>
        <translation>Анықтама</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Don&apos;t Save</source>
        <translation>Сақтамаңыз</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Discard</source>
        <translation>Алып тастау</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Yes</source>
        <translation>Иә</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Yes to &amp;All</source>
        <translation>Иә &gt; Барлығына</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;No</source>
        <translation>&gt; Жоқ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>N&amp;o to All</source>
        <translation>N&gt; Барлығына</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save All</source>
        <translation>Барлығын сақтау</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abort</source>
        <translation>Доғару</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Retry</source>
        <translation>Ретри</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ignore</source>
        <translation>Елемеу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restore Defaults</source>
        <translation>Әдепкілерді қалпына келтіру</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Close without Saving</source>
        <translation>Сақтаусыз жабу</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>&amp;OK</source>
        <translation>Ð Ð°Ò</translation>
    </message>
</context>
<context>
    <name>QDirModel</name>
    <message>
        <location filename="../src/gui/itemviews/qdirmodel.cpp" line="+453"/>
        <source>Name</source>
        <translation>Атауы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Size</source>
        <translation>Өлшемі</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation>Kind</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation>Түрі</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Date Modified</source>
        <translation>Өзгертілген күн</translation>
    </message>
</context>
<context>
    <name>QDockWidget</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/qaccessiblewidgets.cpp" line="+1239"/>
        <source>Close</source>
        <translation>Жабу</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dock</source>
        <translation>Док</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Float</source>
        <translation>Қалтқы</translation>
    </message>
</context>
<context>
    <name>QDoubleSpinBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="-537"/>
        <source>More</source>
        <translation>Қосымша</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Less</source>
        <translation>Азырақ</translation>
    </message>
</context>
<context>
    <name>QErrorMessage</name>
    <message>
        <location filename="../src/gui/dialogs/qerrormessage.cpp" line="+192"/>
        <source>Debug Message:</source>
        <translation>&amp; Қате туралы хабар:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning:</source>
        <translation>Ескерту:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Fatal Error:</source>
        <translation>Кісі өлімі қатесі:</translation>
    </message>
    <message>
        <location line="+193"/>
        <source>&amp;Show this message again</source>
        <translation>Бұл хабарды қайта көрсету</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;OK</source>
        <translation>Ð Ð°Ò</translation>
    </message>
</context>
<context>
    <name>QFile</name>
    <message>
        <location filename="../src/corelib/io/qfile.cpp" line="+708"/>
        <location line="+141"/>
        <source>Destination file exists</source>
        <translation>Тағайындау файлы бар</translation>
    </message>
    <message>
        <location line="-108"/>
        <source>Cannot remove source file</source>
        <translation>Бастапқы файл өшірілмеді</translation>
    </message>
    <message>
        <location line="+120"/>
        <source>Cannot open %1 for input</source>
        <translation>% 1 енгізу үшін ашылмады</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Cannot open for output</source>
        <translation>Шығыс үшін ашылмады</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Failure to write block</source>
        <translation>Блок жазылмады</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot create %1 for output</source>
        <translation>% 1 шығарылымы үшін жасалмады</translation>
    </message>
</context>
<context>
    <name>QFileDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="+515"/>
        <location line="+444"/>
        <source>All Files (*)</source>
        <translation>Барлық файлдар (*)</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>Directories</source>
        <translation>Каталогтар</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+50"/>
        <location line="+1471"/>
        <location line="+75"/>
        <source>&amp;Open</source>
        <translation>Ашу</translation>
    </message>
    <message>
        <location line="-1596"/>
        <location line="+50"/>
        <source>&amp;Save</source>
        <translation>Сақтау</translation>
    </message>
    <message>
        <location line="-730"/>
        <source>Open</source>
        <translation>Ашу</translation>
    </message>
    <message>
        <location line="+1515"/>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation>% 1 деген бар ғой.
Оны алмастырғыңыз келе ме?</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>%1
File not found.
Please verify the correct file name was given.</source>
        <translation>%1
Файл табылмады.
Дұрыс файл атауы берілгенін тексеріңіз.</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qdirmodel.cpp" line="+402"/>
        <source>My Computer</source>
        <translation>Менің компьютерім</translation>
    </message>
    <message>
        <source>%1 File</source>
        <extracomment>%1 is a file name suffix, for example txt</extracomment>
        <translation>% 1 файл</translation>
    </message>
    <message>
        <source>%1 KiB</source>
        <translation>% 1 KiB</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="-1504"/>
        <source>&amp;Rename</source>
        <translation>Атын өзгерту</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Delete</source>
        <translation>Өшіру</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show &amp;hidden files</source>
        <translation>&amp; Жасырын файлдар көрсетілсін</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.ui"/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Back</source>
        <translation>Артқа</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Parent Directory</source>
        <translation>Бас каталог</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>List View</source>
        <translation>Тізім көрінісі</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Detail View</source>
        <translation>Егжей- тегжейлі көрініс</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Files of type:</source>
        <translation>Түр файлдары:</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="+6"/>
        <location line="+648"/>
        <source>Directory:</source>
        <translation>Каталог:</translation>
    </message>
    <message>
        <location line="+794"/>
        <location line="+862"/>
        <source>%1
Directory not found.
Please verify the correct directory name was given.</source>
        <translation>%1
Каталог табылмады.
Каталогтың дұрыс атауы берілгенін тексеріңіз.</translation>
    </message>
    <message>
        <location line="-218"/>
        <source>&apos;%1&apos; is write protected.
Do you want to delete it anyway?</source>
        <translation>&apos;% 1&apos; деп жазу қорғалған.
Сонда да жойғыңыз келе ме?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Are sure you want to delete &apos;%1&apos;?</source>
        <translation>&apos;% 1&apos; дегенді жою керек пе?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Are you sure you want to delete &apos;%1&apos;?</source>
        <translation>&apos;% 1&apos; дегенді жойғыңыз келе ме?</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Өшіру</translation>
    </message>
    <message>
        <source>Folder</source>
        <translation>Қапшық</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Could not delete directory.</source>
        <translation>Каталог өшірілмеді.</translation>
    </message>
    <message>
        <location line="+407"/>
        <source>Recent Places</source>
        <translation>Соңғы орындар</translation>
    </message>
    <message>
        <location line="-2550"/>
        <source>Save As</source>
        <translation>Басқаша сақтау</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qfileiconprovider.cpp" line="+411"/>
        <source>Drive</source>
        <translation>Дискі</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+1"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Unknown</source>
        <translation>Беймәлім</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="-4"/>
        <source>Find Directory</source>
        <translation>Каталогты табу</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Show </source>
        <translation>&amp; Көрсету </translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.ui"/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Forward</source>
        <translation>Қайта жіберу</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="+1970"/>
        <source>New Folder</source>
        <translation>Жаңа қапшық</translation>
    </message>
    <message>
        <location line="-1963"/>
        <source>&amp;New Folder</source>
        <translation>Жаңа қапшық</translation>
    </message>
    <message>
        <location line="+656"/>
        <location line="+38"/>
        <source>&amp;Choose</source>
        <translation>Таңдау</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qsidebar.cpp" line="+418"/>
        <source>Remove</source>
        <translation>Өшіру</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="-687"/>
        <location line="+652"/>
        <source>File &amp;name:</source>
        <translation>Файлдың атауы:</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.ui"/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Look in:</source>
        <translation>Қарап көріңізші:</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Create New Folder</source>
        <translation>Жаңа қалтаны жасау</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog_win.cpp" line="+160"/>
        <source>All Files (*.*)</source>
        <translation>Барлық файлдар (*.*)</translation>
    </message>
</context>
<context>
    <name>QFileSystemModel</name>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel.cpp" line="+744"/>
        <source>%1 TB</source>
        <translation>% 1 ТБ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation>% 1 Гб</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation>% 1 Мб</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation>% 1 КБ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 bytes</source>
        <translation>% 1 байт</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Invalid filename</source>
        <translation>Жарамсыз файл атауы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;b&gt;The name &quot;%1&quot; can not be used.&lt;/b&gt;&lt;p&gt;Try using another name, with fewer characters or no punctuations marks.</source>
        <translation>&lt;b&gt;&quot;% 1&quot; атауын пайдалануға болмайды.&lt;/b&gt; &lt;p&gt;Таңбалар саны аз немесе тыныс белгілері жоқ басқа атауды пайдаланып көріңіз.</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Name</source>
        <translation>Атауы</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Size</source>
        <translation>Өлшемі</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation>Kind</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation>Түрі</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Date Modified</source>
        <translation>Өзгертілген күн</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel_p.h" line="+234"/>
        <source>My Computer</source>
        <translation>Менің компьютерім</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Computer</source>
        <translation>Компьютер</translation>
    </message>
    <message>
        <source>%1 KiB</source>
        <translation>% 1 KiB</translation>
    </message>
</context>
<context>
    <name>QFontDatabase</name>
    <message>
        <location filename="../src/gui/text/qfontdatabase.cpp" line="+90"/>
        <location line="+1176"/>
        <source>Normal</source>
        <translation>Қалыпты</translation>
    </message>
    <message>
        <location line="-1173"/>
        <location line="+12"/>
        <location line="+1149"/>
        <source>Bold</source>
        <translation>Қалың</translation>
    </message>
    <message>
        <location line="-1158"/>
        <location line="+1160"/>
        <source>Demi Bold</source>
        <translation>Деми Болд</translation>
    </message>
    <message>
        <location line="-1157"/>
        <location line="+18"/>
        <location line="+1135"/>
        <source>Black</source>
        <translation>Қара</translation>
    </message>
    <message>
        <location line="-1145"/>
        <source>Demi</source>
        <translation>Деми</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+1145"/>
        <source>Light</source>
        <translation>Жарық</translation>
    </message>
    <message>
        <location line="-1004"/>
        <location line="+1007"/>
        <source>Italic</source>
        <translation>Италич</translation>
    </message>
    <message>
        <location line="-1004"/>
        <location line="+1006"/>
        <source>Oblique</source>
        <translation>Қиғаш</translation>
    </message>
    <message>
        <location line="+705"/>
        <source>Any</source>
        <translation>Кез келген</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Latin</source>
        <translation>Латынша</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Greek</source>
        <translation>Грек</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cyrillic</source>
        <translation>Кириллица</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Armenian</source>
        <translation>Армянша</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hebrew</source>
        <translation>Иврит</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Arabic</source>
        <translation>Арабша</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Syriac</source>
        <translation>Сирия</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Thaana</source>
        <translation>Таана</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Devanagari</source>
        <translation>Деванагари</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Bengali</source>
        <translation>Бенгали</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gurmukhi</source>
        <translation>Гурмухи</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gujarati</source>
        <translation>Гуджарати</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Oriya</source>
        <translation>Ория</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Tamil</source>
        <translation>Тамил тілі</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Telugu</source>
        <translation>Телугу тілі</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Kannada</source>
        <translation>Каннада тілі</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Malayalam</source>
        <translation>Малайалам</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sinhala</source>
        <translation>Синхала</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Thai</source>
        <translation>Тай тілі</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Lao</source>
        <translation>Лао</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Tibetan</source>
        <translation>Тибет</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Myanmar</source>
        <translation>Мьянма</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Georgian</source>
        <translation>Грузин</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Khmer</source>
        <translation>Кхмер</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Simplified Chinese</source>
        <translation>Жеңілдетілген қытайлықтар</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Traditional Chinese</source>
        <translation>Дәстүрлі қытай тілі</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Japanese</source>
        <translation>Жапон тілі</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Korean</source>
        <translation>Корей</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Vietnamese</source>
        <translation>Вьетнам тілі</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Symbol</source>
        <translation>Символ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ogham</source>
        <translation>Огам</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Runic</source>
        <translation>Руник</translation>
    </message>
</context>
<context>
    <name>QFontDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qfontdialog.cpp" line="+772"/>
        <source>&amp;Font</source>
        <translation>Қаріп</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Font st&amp;yle</source>
        <translation>Қаріп</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>Өлшемі</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Effects</source>
        <translation>Әсерлер</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stri&amp;keout</source>
        <translation>Стри-кью</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Underline</source>
        <translation>Астын сызу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sample</source>
        <translation>Үлгі</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wr&amp;iting System</source>
        <translation>Wr&amp;iting жүйесі</translation>
    </message>
    <message>
        <location line="-604"/>
        <location line="+247"/>
        <source>Select Font</source>
        <translation>Қаріпті таңдау</translation>
    </message>
</context>
<context>
    <name>QFtp</name>
    <message>
        <location filename="../src/network/access/qftp.cpp" line="+826"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+683"/>
        <source>Not connected</source>
        <translation>Қосылмаған</translation>
    </message>
    <message>
        <location line="+65"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+65"/>
        <source>Host %1 not found</source>
        <translation>% 1 хост табылмады</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+4"/>
        <source>Connection refused to host %1</source>
        <translation>% 1 дегенді қосудан бас тартылды</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection timed out to host %1</source>
        <translation>% 1 хостына байланыстыру уақыты</translation>
    </message>
    <message>
        <location line="+104"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+102"/>
        <location line="+1451"/>
        <source>Connected to host %1</source>
        <translation>% 1 хостына қосылған</translation>
    </message>
    <message>
        <location line="+219"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="-1290"/>
        <source>Connection refused for data connection</source>
        <translation>Деректер қосылымы үшін қосылымнан бас тартылды</translation>
    </message>
    <message>
        <location line="+178"/>
        <location line="+29"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+195"/>
        <location line="+728"/>
        <source>Unknown error</source>
        <translation>Беймәлім қате</translation>
    </message>
    <message>
        <location line="+889"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+77"/>
        <source>Connecting to host failed:
%1</source>
        <translation>Хостқа қосылу жаңылысы:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Login failed:
%1</source>
        <translation>Кіру жаңылысы:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Listing directory failed:
%1</source>
        <translation>Тізім каталогы жаңылысы:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Changing directory failed:
%1</source>
        <translation>Каталогты өзгерту жаңылысы:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Downloading file failed:
%1</source>
        <translation>Файлды жүктеу жаңылысы:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Uploading file failed:
%1</source>
        <translation>Файлды кері жүктеу жаңылысы:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Removing file failed:
%1</source>
        <translation>Файлды жою жаңылысы:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Creating directory failed:
%1</source>
        <translation>Каталогты жасау жаңылысы:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Removing directory failed:
%1</source>
        <translation>Каталогты жою жаңылысы:
%1</translation>
    </message>
    <message>
        <location line="+28"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+25"/>
        <location line="+250"/>
        <source>Connection closed</source>
        <translation>Қосылым жабылды</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="-11"/>
        <source>Host %1 found</source>
        <translation>% 1 хост табылды</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection to %1 closed</source>
        <translation>% 1 қосылымы жабылды</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Host found</source>
        <translation>Хост табылды</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Connected to host</source>
        <translation>Хостқа қосылған</translation>
    </message>
</context>
<context>
    <name>QGuiApplication</name>
    <message>
        <location filename="../src/gui/kernel/qapplication.cpp" line="+2248"/>
        <source>QT_LAYOUT_DIRECTION</source>
        <comment>Translate this string to the string &apos;LTR&apos; in left-to-right languages or to &apos;RTL&apos; in right-to-left languages (such as Hebrew and Arabic) to get proper widget layout.</comment>
        <translation>QT_LAYOUT_DIRECTION</translation>
    </message>
</context>
<context>
    <name>QHostInfo</name>
    <message>
        <location filename="../src/network/kernel/qhostinfo_p.h" line="+183"/>
        <source>Unknown error</source>
        <translation>Беймәлім қате</translation>
    </message>
</context>
<context>
    <name>QHostInfoAgent</name>
    <message>
        <location filename="../src/network/kernel/qhostinfo_unix.cpp" line="+178"/>
        <location line="+9"/>
        <location line="+64"/>
        <location line="+31"/>
        <location filename="../src/network/kernel/qhostinfo_win.cpp" line="+180"/>
        <location line="+9"/>
        <location line="+40"/>
        <location line="+27"/>
        <source>Host not found</source>
        <translation>Хост табылмады</translation>
    </message>
    <message>
        <location line="-44"/>
        <location line="+39"/>
        <location filename="../src/network/kernel/qhostinfo_win.cpp" line="-34"/>
        <location line="+29"/>
        <source>Unknown address type</source>
        <translation>Беймәлім мекенжай түрі</translation>
    </message>
    <message>
        <location line="+8"/>
        <location filename="../src/network/kernel/qhostinfo_win.cpp" line="-19"/>
        <location line="+27"/>
        <source>Unknown error</source>
        <translation>Беймәлім қате</translation>
    </message>
</context>
<context>
    <name>QHttp</name>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="+1574"/>
        <location line="+820"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+1160"/>
        <location line="+567"/>
        <source>Unknown error</source>
        <translation>Беймәлім қате</translation>
    </message>
    <message>
        <location line="-568"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="-370"/>
        <source>Request aborted</source>
        <translation>Сұрау доғарылды</translation>
    </message>
    <message>
        <location line="+579"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+381"/>
        <source>No server set to connect to</source>
        <translation>Қосылатын сервер жиыны жоқ</translation>
    </message>
    <message>
        <location line="+164"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+56"/>
        <source>Wrong content length</source>
        <translation>Дұрыс емес мазмұн ұзындығы</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+4"/>
        <source>Server closed connection unexpectedly</source>
        <translation>Сервер қосылымы күтпеген жерден жабылды</translation>
    </message>
    <message>
        <location line="+179"/>
        <source>Unknown authentication method</source>
        <translation>Беймәлім аутентификация әдісі</translation>
    </message>
    <message>
        <location line="+183"/>
        <source>Error writing response to device</source>
        <translation>Құрылғыға жауап жазу қатесі</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="+876"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+38"/>
        <source>Connection refused</source>
        <translation>Қосылымнан бас тартылды</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="-304"/>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="-4"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+3"/>
        <source>Host %1 not found</source>
        <translation>% 1 хост табылмады</translation>
    </message>
    <message>
        <location line="+20"/>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="+10"/>
        <location line="+19"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+3"/>
        <source>HTTP request failed</source>
        <translation>HTTP сұрауы орындалмады</translation>
    </message>
    <message>
        <location line="+73"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+69"/>
        <source>Invalid HTTP response header</source>
        <translation>Жарамсыз HTTP жауабының тақырыбы</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+48"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+40"/>
        <location line="+47"/>
        <source>Invalid HTTP chunked body</source>
        <translation>Жарамсыз HTTP шанағы</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="+294"/>
        <source>Host %1 found</source>
        <translation>% 1 хост табылды</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connected to host %1</source>
        <translation>% 1 хостына қосылған</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection to %1 closed</source>
        <translation>% 1 қосылымы жабылды</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Host found</source>
        <translation>Хост табылды</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connected to host</source>
        <translation>Хостқа қосылған</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="-22"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+3"/>
        <source>Connection closed</source>
        <translation>Қосылым жабылды</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="-135"/>
        <source>Proxy authentication required</source>
        <translation>Прокси аутентификациясы қажет</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Authentication required</source>
        <translation>Аутентификация қажет</translation>
    </message>
    <message>
        <location line="-138"/>
        <source>Connection refused (or timed out)</source>
        <translation>Қосылымнан бас тартылды (немесе уақытылы)</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="+6"/>
        <source>Proxy requires authentication</source>
        <translation>Прокси аутентификацияны қажет етеді</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Host requires authentication</source>
        <translation>Хост аутентификацияны қажет етеді</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Data corrupted</source>
        <translation>Деректер бүлінген</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown protocol specified</source>
        <translation>Беймәлім хаттама көрсетілген</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>SSL handshake failed</source>
        <translation>SCL қол алысу сәтсіз аяқталды</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="-2263"/>
        <source>HTTPS connection requested but SSL support not compiled in</source>
        <translation>HTTPS қосылымы сұралды, бірақ SCL қолдауы құрастырылмаған</translation>
    </message>
</context>
<context>
    <name>QHttpSocketEngine</name>
    <message>
        <location filename="../src/network/socket/qhttpsocketengine.cpp" line="-89"/>
        <source>Did not receive HTTP response from proxy</source>
        <translation>Сенімхаттан HTTP жауабы алынбады</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Error parsing authentication request from proxy</source>
        <translation>Проксиден аутентификация сұрауын талдау қатесі</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Authentication required</source>
        <translation>Аутентификация қажет</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Proxy denied connection</source>
        <translation>Прокси қосылымнан бас тартты</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Error communicating with HTTP proxy</source>
        <translation>HTTP проксиімен байланысу қатесі</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Proxy server not found</source>
        <translation>Прокси сервері табылмады</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy connection refused</source>
        <translation>Прокси қосылымы бас тартылды</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy server connection timed out</source>
        <translation>Прокси сервер қосылымы уақытында</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy connection closed prematurely</source>
        <translation>Прокси қосылымы уақытынан бұрын жабылды</translation>
    </message>
</context>
<context>
    <name>QIBaseDriver</name>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="+1428"/>
        <source>Error opening database</source>
        <translation>Дерекқорды ашу қатесі</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Could not start transaction</source>
        <translation>Транзакция басталмады</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unable to commit transaction</source>
        <translation>Мәміле жасау мүмкін емес</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unable to rollback transaction</source>
        <translation>Транзакцияны кері қайтару мүмкін емес</translation>
    </message>
</context>
<context>
    <name>QIBaseResult</name>
    <message>
        <location line="-1097"/>
        <source>Unable to create BLOB</source>
        <translation>BLOB құрылмады</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to write BLOB</source>
        <translation>BLOB жазуы мүмкін емес</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Unable to open BLOB</source>
        <translation>BLOB ашылмады</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Unable to read BLOB</source>
        <translation>BLOB оқылмады</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+189"/>
        <source>Could not find array</source>
        <translation>Массив табылмады</translation>
    </message>
    <message>
        <location line="-157"/>
        <source>Could not get array data</source>
        <translation>Массив деректері алынбады</translation>
    </message>
    <message>
        <location line="+212"/>
        <source>Could not get query info</source>
        <translation>Сұрау ақпараты алынбады</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Could not start transaction</source>
        <translation>Транзакция басталмады</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to commit transaction</source>
        <translation>Мәміле жасау мүмкін емес</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Could not allocate statement</source>
        <translation>Мәлімдеме бөлінбеді</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Could not prepare statement</source>
        <translation>Мәлімдеме дайындалмады</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+7"/>
        <source>Could not describe input statement</source>
        <translation>Енгізу мәлімдемесін сипаттау мүмкін болмады</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not describe statement</source>
        <translation>Мәлімдемені сипаттау мүмкін болмады</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Unable to close statement</source>
        <translation>Мәлімдемені жабу мүмкін емес</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Unable to execute query</source>
        <translation>Сұрау орындалмады</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Could not fetch next item</source>
        <translation>Келесі элементті алу мүмкін болмады</translation>
    </message>
    <message>
        <location line="+160"/>
        <source>Could not get statement info</source>
        <translation>Мәлімдеме ақпараты алынбады</translation>
    </message>
</context>
<context>
    <name>QIODevice</name>
    <message>
        <location filename="../src/corelib/global/qglobal.cpp" line="+1869"/>
        <source>Permission denied</source>
        <translation>Рұқсат беруден бас тартылды</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Too many open files</source>
        <translation>Тым көп ашық файлдар</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>No such file or directory</source>
        <translation>Бұндай файл немесе каталог жоқ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>No space left on device</source>
        <translation>Құрылғыда бос орын қалмады</translation>
    </message>
    <message>
        <location filename="../src/corelib/io/qiodevice.cpp" line="+1536"/>
        <source>Unknown error</source>
        <translation>Беймәлім қате</translation>
    </message>
</context>
<context>
    <name>QInputContext</name>
    <message>
        <location filename="../src/gui/inputmethod/qinputcontextfactory.cpp" line="+242"/>
        <source>XIM</source>
        <translation>XIM</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>XIM input method</source>
        <translation>XIM енгізу әдісі</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Windows input method</source>
        <translation>Windows енгізу әдісі</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Mac OS X input method</source>
        <translation>Mac OS X енгізу әдісі</translation>
    </message>
</context>
<context>
    <name>QInputDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qinputdialog.cpp" line="+223"/>
        <source>Enter a value:</source>
        <translation>Мәнді енгізіңіз:</translation>
    </message>
</context>
<context>
    <name>QLibrary</name>
    <message>
        <source>QLibrary::load_sys: Cannot load %1 (%2)</source>
        <translation type="obsolete">QLibrary::load_sys： 不能载入%1 (%2)</translation>
    </message>
    <message>
        <source>QLibrary::unload_sys: Cannot unload %1 (%2)</source>
        <translation type="obsolete">QLibrary::unload_sys：不能卸载%1 (%2)</translation>
    </message>
    <message>
        <source>QLibrary::resolve_sys: Symbol &quot;%1&quot; undefined in %2 (%3)</source>
        <translation type="obsolete">QLibrary::resolve_sys: 符号“%1”在%2（%3）没有被定义</translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary.cpp" line="+378"/>
        <source>Could not mmap &apos;%1&apos;: %2</source>
        <translation>&apos;% 1&apos; дегенді жамылған жоқ:% 2</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Plugin verification data mismatch in &apos;%1&apos;</source>
        <translation>Плагин модулін тексеру деректері &apos;% 1&apos; дегенде сәйкес келмейді</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Could not unmap &apos;%1&apos;: %2</source>
        <translation>&apos;% 1&apos; дегенді ашылмады:% 2</translation>
    </message>
    <message>
        <location line="+302"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (%2.%3.%4) [%5]</source>
        <translation>&apos;% 1&apos; плагин модулі үйлеспейтін Qt кітапханасын пайдаланады. (%2.%3.%4) [%5]</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. Expected build key &quot;%2&quot;, got &quot;%3&quot;</source>
        <translation>&apos;% 1&apos; плагин модулі үйлеспейтін Qt кітапханасын пайдаланады. Күтілетін &quot;% 2&quot; тұрғызу кілті, &quot;% 3&quot;</translation>
    </message>
    <message>
        <location line="+340"/>
        <source>Unknown error</source>
        <translation>Беймәлім қате</translation>
    </message>
    <message>
        <location line="-377"/>
        <location filename="../src/corelib/plugin/qpluginloader.cpp" line="+280"/>
        <source>The shared library was not found.</source>
        <translation>Ортақ кітапхана табылмады.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>The file &apos;%1&apos; is not a valid Qt plugin.</source>
        <translation>&apos;% 1&apos; файлы жарамды Qt плагин модулі емес.</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (Cannot mix debug and release libraries.)</source>
        <translation>&apos;% 1&apos; плагин модулі үйлеспейтін Qt кітапханасын пайдаланады. (Өшіру және шығару кітапханаларын араластыру мүмкін емес.)</translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary_unix.cpp" line="+209"/>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="+99"/>
        <source>Cannot load library %1: %2</source>
        <translation>% 1 кітапханасы жүктелмеді:% 2</translation>
    </message>
    <message>
        <location line="+16"/>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="+26"/>
        <source>Cannot unload library %1: %2</source>
        <translation>% 1 кітапханасын жүктеу болмады:% 2</translation>
    </message>
    <message>
        <location line="+31"/>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="+15"/>
        <source>Cannot resolve symbol &quot;%1&quot; in %2: %3</source>
        <translation>% 2:% 3 дегендегі &quot;% 1&quot; таңбасы шешілмеді</translation>
    </message>
</context>
<context>
    <name>QLineEdit</name>
    <message>
        <location filename="../src/gui/widgets/qlineedit.cpp" line="+2680"/>
        <source>&amp;Undo</source>
        <translation>&gt; Болдырмау</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Redo</source>
        <translation>&gt; Редо</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cu&amp;t</source>
        <translation>Cu&gt;</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Copy</source>
        <translation>&amp; Көшіру</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Paste</source>
        <translation>&gt; Қою</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Delete</source>
        <translation>Өшіру</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Select All</source>
        <translation>Барлығын таңдау</translation>
    </message>
</context>
<context>
    <name>QLocalServer</name>
    <message>
        <location filename="../src/network/socket/qlocalserver.cpp" line="+226"/>
        <location filename="../src/network/socket/qlocalserver_unix.cpp" line="+231"/>
        <source>%1: Name error</source>
        <translation>% 1: Атау қатесі</translation>
    </message>
    <message>
        <location filename="../src/network/socket/qlocalserver_unix.cpp" line="-8"/>
        <source>%1: Permission denied</source>
        <translation>% 1: Рұқсат жоққа шығарылды</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1: Address in use</source>
        <translation>% 1: Қолданыстағы адрес</translation>
    </message>
    <message>
        <location line="+5"/>
        <location filename="../src/network/socket/qlocalserver_win.cpp" line="+158"/>
        <source>%1: Unknown error %2</source>
        <translation>% 1: Беймәлім қате% 2</translation>
    </message>
</context>
<context>
    <name>QLocalSocket</name>
    <message>
        <location filename="../src/network/socket/qlocalsocket_tcp.cpp" line="+132"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+134"/>
        <source>%1: Connection refused</source>
        <translation>% 1: Қосылым бас тартты</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Remote closed</source>
        <translation>% 1: Қашықтан жабық</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_win.cpp" line="+80"/>
        <location line="+43"/>
        <source>%1: Invalid name</source>
        <translation>% 1: Атауы жарамсыз</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket access error</source>
        <translation>% 1: Розеткаға кіру қатесі</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket resource error</source>
        <translation>% 1: Розетка ресурсының қатесі</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket operation timed out</source>
        <translation>% 1: Розетка операциясы уақыты бітті</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Datagram too large</source>
        <translation>% 1: Деректер диаграммасы тым үлкен</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_win.cpp" line="-48"/>
        <source>%1: Connection error</source>
        <translation>% 1: Қосылым қатесі</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: The socket operation is not supported</source>
        <translation>% 1: Розетка операциясына қолдау көрсетілмеген</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: Unknown error</source>
        <translation>% 1: Беймәлім қате</translation>
    </message>
    <message>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+4"/>
        <location filename="../src/network/socket/qlocalsocket_win.cpp" line="+10"/>
        <source>%1: Unknown error %2</source>
        <translation>% 1: Беймәлім қате% 2</translation>
    </message>
</context>
<context>
    <name>QMYSQLDriver</name>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="+1231"/>
        <source>Unable to open database &apos;</source>
        <translation>Дерекқор ашылмады&apos;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to connect</source>
        <translation>Қосылмады</translation>
    </message>
    <message>
        <location line="+127"/>
        <source>Unable to begin transaction</source>
        <translation>Транзакцияны бастау мүмкін емес</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to commit transaction</source>
        <translation>Мәміле жасау мүмкін емес</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback transaction</source>
        <translation>Транзакцияны кері қайтару мүмкін емес</translation>
    </message>
</context>
<context>
    <name>QMYSQLResult</name>
    <message>
        <location line="-922"/>
        <source>Unable to fetch data</source>
        <translation>Деректерді алу мүмкін емес</translation>
    </message>
    <message>
        <location line="+176"/>
        <source>Unable to execute query</source>
        <translation>Сұрау орындалмады</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to store result</source>
        <translation>Нәтиже сақталмады</translation>
    </message>
    <message>
        <location line="+190"/>
        <location line="+8"/>
        <source>Unable to prepare statement</source>
        <translation>Мәлімдеме дайындалмады</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Unable to reset statement</source>
        <translation>Мәлімдемені қалпына келтіру мүмкін емес</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Unable to bind value</source>
        <translation>Мәнді байланыстыру мүмкін емес</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Unable to execute statement</source>
        <translation>Мәлімдемені орындау мүмкін емес</translation>
    </message>
    <message>
        <location line="+14"/>
        <location line="+21"/>
        <source>Unable to bind outvalues</source>
        <translation>Шығыс бағаларын байланыстыру мүмкін емес</translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Unable to store statement results</source>
        <translation>Мәлімдеме нәтижелері сақталмады</translation>
    </message>
    <message>
        <location line="-253"/>
        <source>Unable to execute next query</source>
        <translation>Келесі сұрау орындалмады</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Unable to store next result</source>
        <translation>Келесі нәтиже сақталмады</translation>
    </message>
</context>
<context>
    <name>QMdiArea</name>
    <message>
        <location filename="../src/gui/widgets/qmdiarea.cpp" line="+290"/>
        <source>(Untitled)</source>
        <translation>(Атақсыз)</translation>
    </message>
</context>
<context>
    <name>QMdiSubWindow</name>
    <message>
        <location filename="../src/gui/widgets/qmdisubwindow.cpp" line="+280"/>
        <source>%1 - [%2]</source>
        <translation>%1 - [%2]</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Close</source>
        <translation>Жабу</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Minimize</source>
        <translation>Кішірейту</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Restore Down</source>
        <translation>Қалпына келтіру</translation>
    </message>
    <message>
        <location line="+707"/>
        <source>&amp;Restore</source>
        <translation>Қалпына келтіру</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Move</source>
        <translation>Жылжыту</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>Өлшемі</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mi&amp;nimize</source>
        <translation>Ми-нимиз</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ma&amp;ximize</source>
        <translation>Ma&gt; ximize</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stay on &amp;Top</source>
        <translation>Жоғарыда қалу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Close</source>
        <translation>Жабу@ action: inmenu</translation>
    </message>
    <message>
        <location line="-787"/>
        <source>- [%1]</source>
        <translation>- [%1]</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Maximize</source>
        <translation>Барынша көбейту</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unshade</source>
        <translation>Unshade</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Shade</source>
        <translation>Көлеңке</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Restore</source>
        <translation>Қалпына келтіру</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Help</source>
        <translation>Анықтама</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Menu</source>
        <translation>Мәзір</translation>
    </message>
</context>
<context>
    <name>QMenu</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/qaccessiblemenu.cpp" line="+157"/>
        <location line="+225"/>
        <source>Close</source>
        <translation>Жабу</translation>
    </message>
    <message>
        <location line="-224"/>
        <location line="+225"/>
        <source>Open</source>
        <translation>Ашу</translation>
    </message>
    <message>
        <location line="-223"/>
        <location line="+225"/>
        <location line="+51"/>
        <source>Execute</source>
        <translation>Орындау</translation>
    </message>
</context>
<context>
    <name>QMenuBar</name>
    <message>
        <source>About</source>
        <translation type="obsolete">关于</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="obsolete">配置</translation>
    </message>
    <message>
        <source>Preference</source>
        <translation type="obsolete">首选项</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">选项</translation>
    </message>
    <message>
        <source>Setting</source>
        <translation type="obsolete">设置</translation>
    </message>
    <message>
        <source>Setup</source>
        <translation type="obsolete">安装</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="obsolete">退出</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="obsolete">退出</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation type="obsolete">关于%1</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation type="obsolete">关于Qt</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation type="obsolete">首选项</translation>
    </message>
    <message>
        <source>Quit %1</source>
        <translation type="obsolete">退出%1</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.cpp" line="-1111"/>
        <source>Help</source>
        <translation>Анықтама</translation>
    </message>
    <message>
        <location line="-853"/>
        <location line="+852"/>
        <location filename="../src/gui/dialogs/qmessagebox.h" line="-52"/>
        <location line="+8"/>
        <source>OK</source>
        <translation>Жақсы</translation>
    </message>
    <message>
        <location line="+509"/>
        <source>About Qt</source>
        <translation>Qt туралы</translation>
    </message>
    <message>
        <source>&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;这个程序使用的是Qt %1版。&lt;/p&gt;</translation>
    </message>
    <message>
        <location line="-1605"/>
        <source>Show Details...</source>
        <translation>Егжей-тегжейін көрсету...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide Details...</source>
        <translation>Мәліметтерді жасыру...</translation>
    </message>
    <message>
        <location line="+1570"/>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt for Embedded Linux and Qt for Windows CE.&lt;/p&gt;&lt;p&gt;Qt is available under three different licensing options designed to accommodate the needs of our various users.&lt;/p&gt;Qt licensed under our commercial license agreement is appropriate for development of proprietary/commercial software where you do not want to share any source code with third parties or otherwise cannot comply with the terms of the GNU LGPL version 2.1 or GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU LGPL version 2.1 is appropriate for the development of Qt applications (proprietary or open source) provided you can comply with the terms and conditions of the GNU LGPL version 2.1.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU General Public License version 3.0 is appropriate for the development of Qt applications where you wish to use such applications in combination with software subject to the terms of the GNU GPL version 3.0 or where you are otherwise willing to comply with the terms of the GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://qt.nokia.com/products/licensing&quot;&gt;qt.nokia.com/products/licensing&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;&lt;p&gt;Copyright (C) 2012 Nokia Corporation and/or its subsidiary(-ies).&lt;/p&gt;&lt;p&gt;Qt is a Nokia product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Qt туралы&lt;/h3&gt;&lt;p&gt;Бұл бағдарламада% 1 Qt нұсқасы қолданылсын.&lt;/p&gt; &lt;p&gt;Qt — Кросс-платформа қолданбасын әзірлеуге арналған C++ құралдар жиынтығы.&lt;/p&gt; &lt;p&gt;Qt MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux және барлық негізгі коммерциялық Unix нұсқалары арқылы бір көзді тасымалдылықты қамтамасыз етеді. Qt ендірілген құрылғылар үшін ендірілген Linux үшін Qt және Windows CE жүйесіне арналған Qt сияқты қол жетімді.&lt;/p&gt; &lt;p&gt;Qt әр түрлі пайдаланушыларымыздың қажеттіліктерін қанағаттандыруға арналған лицензиялаудың үш түрлі нұсқасы бойынша қолжетімді.&lt;/p&gt; Біздің коммерциялық лицензиялық келісіміміз бойынша лицензияланған Qt үшінші тұлғалармен қандай да бір бастапқы кодты бөліскіңіз келмейтін немесе басқаша түрде GNU LGPL 2.1 нұсқасының немесе GNU GPL 3.0 нұсқасының шарттарын орындай алмайтын жеке меншік/коммерциялық бағдарламалық қамтамасыз етуді әзірлеу үшін орынды.&lt;/p&gt; &lt;p&gt;GNU LGPL 2.1 нұсқасы бойынша лицензияланған Qt GNU LGPL 2.1 нұсқасының шарттары мен шарттарын сақтауға болатын жағдайда Qt қосымшаларын (жеке немесе ашық көз) әзірлеу үшін орынды.&lt;/p&gt; &lt;p&gt;GNU General Public License 3.0 нұсқасы бойынша лицензияланған Qt GNU GPL 3.0 нұсқасының шарттарына бағынатын немесе GNU GPL 3.0 нұсқасының шарттарын орындауға дайын болған жағдайда, осындай қосымшаларды бағдарламалық қамтамасыз етумен үйлесімде пайдаланғыңыз келетін Qt қосымшаларын әзірлеу үшін орынды.&lt;/p&gt; &lt;p&gt;Qt лицензиялауына шолу жасау үшін &lt;а href=&quot;http://qt.nokia.com/products/licensing&quot;&gt;qt.nokia.com/products/licensing&lt;/a&gt; бөлімін қараңыз.&lt;/p&gt; &lt;p&gt;Copyright (C) 2012 Nokia Corporation және/немесе оның еншілес ұйымы (-лары).&lt;/p&gt; &lt;p&gt;Qt — Nokia өнімі. Қосымша ақпарат алу үшін &lt;а href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt; бөлімін қараңыз.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;%1&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt for Embedded Linux and Qt for Windows CE.&lt;/p&gt;&lt;p&gt;Qt is a Nokia product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokiae.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;h3&gt;关于Qt&lt;/h3&gt;%1&lt;p&gt;Qt是一个用于跨平台应用程序开发的C++工具包。&lt;/p&gt;&lt;p&gt;对于MS&amp;nbsp;Windows、Mac&amp;nbsp;OS&amp;nbsp;X、Linux和所有主流商业Unix，Qt提供了单一源程序的可移植性。Qt也有用于嵌入式Linux和Windows CE的版本。&lt;/p&gt;&lt;p&gt;Qt是Nokia的产品。有关更多信息，请参考&lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt;。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;This program uses Qt Open Source Edition version %1.&lt;/p&gt;&lt;p&gt;Qt Open Source Edition is intended for the development of Open Source applications. You need a commercial Qt license for development of proprietary (closed source) applications.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://qt.nokia.com/company/model/&quot;&gt;qt.nokia.com/company/model/&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;这个程序使用了Qt %1开源版本。&lt;/p&gt;&lt;p&gt;Qt开源版本只用于开源应用程序的开发。如果要开发私有（闭源）软件，你需要一个商业的Qt协议。&lt;/p&gt;&lt;p&gt;有关Qt协议的概览，请参考&lt;a href=&quot;http://qt.nokia.com/company/model/&quot;&gt;qt.nokia.com/company/model/&lt;/a&gt;。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;%1&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt Embedded.&lt;/p&gt;&lt;p&gt;Qt is a Trolltech product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;h3&gt;关于Qt&lt;/h3&gt;%1&lt;p&gt;Qt是一个用于跨平台应用程序开发的C++工具包。&lt;/p&gt;&lt;p&gt;对于MS&amp;nbsp;Windows、Mac&amp;nbsp;OS&amp;nbsp;X、Linux和所有主流商业Unix，Qt提供了单一源程序的可移植性。Qt对于嵌入式平台也是可用的，在嵌入式平台上它被称为Qt Embedded。&lt;/p&gt;&lt;p&gt;Qt是Trolltech的产品。有关更多信息，请参考&lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt;。&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>QMultiInputContext</name>
    <message>
        <location filename="../src/plugins/inputmethods/imsw-multi/qmultiinputcontext.cpp" line="+88"/>
        <source>Select IM</source>
        <translation>Жылдам хабарды таңдау</translation>
    </message>
</context>
<context>
    <name>QMultiInputContextPlugin</name>
    <message>
        <location filename="../src/plugins/inputmethods/imsw-multi/qmultiinputcontextplugin.cpp" line="+95"/>
        <source>Multiple input method switcher</source>
        <translation>Бірнеше енгізу әдісін ауыстырып қосқыш</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Multiple input method switcher that uses the context menu of the text widgets</source>
        <translation>Мәтін виджеттерінің контекст мәзірін пайдаланатын бірнеше енгізу әдісін ауыстырып қосқыш</translation>
    </message>
</context>
<context>
    <name>QNativeSocketEngine</name>
    <message>
        <location filename="../src/network/socket/qnativesocketengine.cpp" line="+206"/>
        <source>The remote host closed the connection</source>
        <translation>Қашықтағы хост қосылымды жапты</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Network operation timed out</source>
        <translation>Желілік жұмыс уақыты бітті</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Out of resources</source>
        <translation>Ресурстардан тыс</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unsupported socket operation</source>
        <translation>Қолдау көрсетілмейтін розетка операциясы</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Protocol type not supported</source>
        <translation>Хаттама түріне қолдау көрсетілмеген</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid socket descriptor</source>
        <translation>Жарамсыз розетка дескрипторы</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Network unreachable</source>
        <translation>Желіге қол жетімді емес</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Permission denied</source>
        <translation>Рұқсат беруден бас тартылды</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection timed out</source>
        <translation>Қосылым уақыты бітті</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection refused</source>
        <translation>Қосылымнан бас тартылды</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The bound address is already in use</source>
        <translation>Байланысқан мекенжай қазірдің өзінде пайдаланылып жатыр</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The address is not available</source>
        <translation>Мекен-жайы жоқ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The address is protected</source>
        <translation>Мекен-жайы қорғалған</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to send a message</source>
        <translation>Хабар жіберілмеді</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to receive a message</source>
        <translation>Хабарды алу мүмкін емес</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to write</source>
        <translation>Жазу мүмкін емес</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Network error</source>
        <translation>Желі қатесі</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Another socket is already listening on the same port</source>
        <translation>Тағы бір розетка қазірдің өзінде сол портта тыңдалып жатыр</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>Unable to initialize non-blocking socket</source>
        <translation>Бұғатталмаған розетканы бастапқылау мүмкін емес</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to initialize broadcast socket</source>
        <translation>Хабар тарату розеткасын бастапқылау мүмкін емес</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Attempt to use IPv6 socket on a platform with no IPv6 support</source>
        <translation>IPv6 қолдауы жоқ платформада IPv6 розеткасын пайдалану әрекеті</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Host unreachable</source>
        <translation>Хост қол жетімді емес</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Datagram was too large to send</source>
        <translation>Деректер диаграммасы жіберу үшін тым үлкен болды</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Operation on non-socket</source>
        <translation>Розеткасыз операция</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unknown error</source>
        <translation>Беймәлім қате</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>The proxy type is invalid for this operation</source>
        <translation>Бұл операция үшін прокси түрі жарамсыз</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessCacheBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccesscachebackend.cpp" line="+65"/>
        <source>Error opening %1</source>
        <translation>% 1 дегенді ашу қатесі</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessFileBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccessfilebackend.cpp" line="+99"/>
        <source>Request for opening non-local file %1</source>
        <translation>% 1 деген жергілікті емес файлды ашуға сұрау</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Error opening %1: %2</source>
        <translation>% 1 дегенді ашу қатесі:% 2</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Write error writing to %1: %2</source>
        <translation>% 1 деген қатені жазу:% 2</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Cannot open %1: Path is a directory</source>
        <translation>% 1 ашылмады: Path — каталог</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Read error reading from %1: %2</source>
        <translation>% 1 деген қатені оқу:% 2</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessFtpBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccessftpbackend.cpp" line="+165"/>
        <source>No suitable proxy found</source>
        <translation>Лайықты прокси табылмады</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Cannot open %1: is a directory</source>
        <translation>% 1 ашылмады: каталог</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>Logging in to %1 failed: authentication required</source>
        <translation>% 1 дегенге кіру жаңылысы: аутентификация қажет</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Error while downloading %1: %2</source>
        <translation>% 1 дегенді жүктеу қатесі:% 2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error while uploading %1: %2</source>
        <translation>% 1 дегенді кері жүктеу қатесі:% 2</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessHttpBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccesshttpbackend.cpp" line="+597"/>
        <source>No suitable proxy found</source>
        <translation>Лайықты прокси табылмады</translation>
    </message>
</context>
<context>
    <name>QNetworkReply</name>
    <message>
        <location line="+128"/>
        <source>Error downloading %1 - server replied: %2</source>
        <translation>% 1 дегенді жүктеу қатесі - сервер жауап берді:% 2</translation>
    </message>
    <message>
        <location filename="../src/network/access/qnetworkreplyimpl.cpp" line="+68"/>
        <source>Protocol &quot;%1&quot; is unknown</source>
        <translation>&quot;% 1&quot; хаттамасы белгісіз</translation>
    </message>
</context>
<context>
    <name>QNetworkReplyImpl</name>
    <message>
        <location line="+432"/>
        <location line="+22"/>
        <source>Operation canceled</source>
        <translation>Операциядан бас тартылды</translation>
    </message>
</context>
<context>
    <name>QOCIDriver</name>
    <message>
        <location filename="../src/sql/drivers/oci/qsql_oci.cpp" line="+2069"/>
        <source>Unable to logon</source>
        <translation>Кіру мүмкін болмады</translation>
    </message>
    <message>
        <location line="-144"/>
        <source>Unable to initialize</source>
        <comment>QOCIDriver</comment>
        <translation>Басталмады</translation>
    </message>
    <message>
        <location line="+215"/>
        <source>Unable to begin transaction</source>
        <translation>Транзакцияны бастау мүмкін емес</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to commit transaction</source>
        <translation>Мәміле жасау мүмкін емес</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to rollback transaction</source>
        <translation>Транзакцияны кері қайтару мүмкін емес</translation>
    </message>
</context>
<context>
    <name>QOCIResult</name>
    <message>
        <location line="-963"/>
        <location line="+161"/>
        <location line="+15"/>
        <source>Unable to bind column for batch execute</source>
        <translation>Партияны орындау үшін бағанды байланыстыру мүмкін емес</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to execute batch statement</source>
        <translation>Партияның мәлімдемесі орындалмады</translation>
    </message>
    <message>
        <location line="+302"/>
        <source>Unable to goto next</source>
        <translation>Келесі гото мүмкін емес</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Unable to alloc statement</source>
        <translation>Аллок мәлімдемесі мүмкін емес</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to prepare statement</source>
        <translation>Мәлімдеме дайындалмады</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Unable to bind value</source>
        <translation>Мәнді байланыстыру мүмкін емес</translation>
    </message>
    <message>
        <source>Unable to execute select statement</source>
        <translation type="obsolete">不能执行选择语句</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to execute statement</source>
        <translation>Мәлімдемені орындау мүмкін емес</translation>
    </message>
</context>
<context>
    <name>QODBCDriver</name>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="+1785"/>
        <source>Unable to connect</source>
        <translation>Қосылмады</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to connect - Driver doesn&apos;t support all needed functionality</source>
        <translation>Қосыла алмады - Драйвер барлық қажетті функционалға қолдау көрсетпейді</translation>
    </message>
    <message>
        <location line="+242"/>
        <source>Unable to disable autocommit</source>
        <translation>Автокомит өшірілмеді</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to commit transaction</source>
        <translation>Мәміле жасау мүмкін емес</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback transaction</source>
        <translation>Транзакцияны кері қайтару мүмкін емес</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to enable autocommit</source>
        <translation>Автокомитті қосу мүмкін болмады</translation>
    </message>
</context>
<context>
    <name>QODBCResult</name>
    <message>
        <location line="-1218"/>
        <location line="+349"/>
        <source>QODBCResult::reset: Unable to set &apos;SQL_CURSOR_STATIC&apos; as statement attribute. Please check your ODBC driver configuration</source>
        <translation>QODBCResult:ысырып тастау: «SQL_CURSOR_STATIC» дегенді мәлімдеме атрибуты ретінде орната алмадым. ODBC драйверінің конфигурациясын тексеріңіз</translation>
    </message>
    <message>
        <location line="-332"/>
        <location line="+626"/>
        <source>Unable to execute statement</source>
        <translation>Мәлімдемені орындау мүмкін емес</translation>
    </message>
    <message>
        <location line="-555"/>
        <source>Unable to fetch next</source>
        <translation>Келесіні алу мүмкін емес</translation>
    </message>
    <message>
        <location line="+279"/>
        <source>Unable to prepare statement</source>
        <translation>Мәлімдеме дайындалмады</translation>
    </message>
    <message>
        <location line="+268"/>
        <source>Unable to bind variable</source>
        <translation>Айнымалыны байланыстыру мүмкін емес</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="+194"/>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="-475"/>
        <location line="+578"/>
        <source>Unable to fetch last</source>
        <translation>Соңғысын алу мүмкін емес</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="-672"/>
        <source>Unable to fetch</source>
        <translation>Алып шығу мүмкін емес</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Unable to fetch first</source>
        <translation>Алдымен алу мүмкін емес</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to fetch previous</source>
        <translation>Алдыңғысын алу мүмкін емес</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/gui/util/qdesktopservices_mac.cpp" line="+165"/>
        <source>Home</source>
        <translation>Үй</translation>
    </message>
    <message>
        <location filename="../src/network/access/qnetworkaccessdatabackend.cpp" line="+74"/>
        <source>Operation not supported on %1</source>
        <translation>% 1 дегендегі операцияға қолдау көрсетілмеген</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Invalid URI: %1</source>
        <translation>Жарамсыз URI:% 1</translation>
    </message>
    <message>
        <location filename="../src/network/access/qnetworkaccessdebugpipebackend.cpp" line="+175"/>
        <source>Write error writing to %1: %2</source>
        <translation>% 1 деген қатені жазу:% 2</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Read error reading from %1: %2</source>
        <translation>% 1 деген қатені оқу:% 2</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Socket error on %1: %2</source>
        <translation>% 1 дегендегі розетка қатесі:% 2</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Remote host closed the connection prematurely on %1</source>
        <translation>Қашықтағы хост% 1 қосылымын уақытынан бұрын жапты</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Protocol error: packet of size 0 received</source>
        <translation>Хаттама қатесі: 0 өлшемді пакет алынды</translation>
    </message>
    <message>
        <location filename="../src/network/kernel/qhostinfo.cpp" line="+177"/>
        <location line="+57"/>
        <source>No host name given</source>
        <translation>Хост атауы берілмеген</translation>
    </message>
</context>
<context>
    <name>QPPDOptionsModel</name>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="+1195"/>
        <source>Name</source>
        <translation>Атауы</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Value</source>
        <translation>Мән</translation>
    </message>
</context>
<context>
    <name>QPSQLDriver</name>
    <message>
        <location filename="../src/sql/drivers/psql/qsql_psql.cpp" line="+763"/>
        <source>Unable to connect</source>
        <translation>Қосылмады</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Could not begin transaction</source>
        <translation>Мәмілені бастау мүмкін болмады</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Could not commit transaction</source>
        <translation>Мәміле жасау мүмкін болмады</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Could not rollback transaction</source>
        <translation>Кері қайтару транзакциясы болмады</translation>
    </message>
    <message>
        <location line="+358"/>
        <source>Unable to subscribe</source>
        <translation>Жазылмады</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Unable to unsubscribe</source>
        <translation>Жазылмады</translation>
    </message>
</context>
<context>
    <name>QPSQLResult</name>
    <message>
        <location line="-1058"/>
        <source>Unable to create query</source>
        <translation>Сұрау жасалмады</translation>
    </message>
    <message>
        <location line="+374"/>
        <source>Unable to prepare statement</source>
        <translation>Мәлімдеме дайындалмады</translation>
    </message>
</context>
<context>
    <name>QPageSetupWidget</name>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupdialog_unix.cpp" line="+304"/>
        <source>Centimeters (cm)</source>
        <translation>Сантиметр (см)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Millimeters (mm)</source>
        <translation>Миллиметр (мм)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Inches (in)</source>
        <translation>Дюйм (дюйм)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Points (pt)</source>
        <translation>Нүктелер (pt)</translation>
    </message>
    <message>
        <source>Pica (P̸)</source>
        <translation>Пика (P̸)</translation>
    </message>
    <message>
        <source>Didot (DD)</source>
        <translation>Дидот (DD)</translation>
    </message>
    <message>
        <source>Cicero (CC)</source>
        <translation>Cicero (CC)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupwidget.ui"/>
        <source>Form</source>
        <translation>Пішін</translation>
    </message>
    <message>
        <location/>
        <source>Paper</source>
        <translation>Қағаз</translation>
    </message>
    <message>
        <location/>
        <source>Page size:</source>
        <translation>Бет өлшемі:</translation>
    </message>
    <message>
        <location/>
        <source>Width:</source>
        <translation>Ені:</translation>
    </message>
    <message>
        <location/>
        <source>Height:</source>
        <translation>Биіктігі:</translation>
    </message>
    <message>
        <location/>
        <source>Paper source:</source>
        <translation>Қағаз көзі:</translation>
    </message>
    <message>
        <location/>
        <source>Orientation</source>
        <translation>Бағдарлау</translation>
    </message>
    <message>
        <location/>
        <source>Portrait</source>
        <translation>Портрет</translation>
    </message>
    <message>
        <location/>
        <source>Landscape</source>
        <translation>Ландшафт</translation>
    </message>
    <message>
        <location/>
        <source>Reverse landscape</source>
        <translation>Кері ландшафт</translation>
    </message>
    <message>
        <location/>
        <source>Reverse portrait</source>
        <translation>Кері портрет</translation>
    </message>
    <message>
        <location/>
        <source>Margins</source>
        <translation>Шеттер</translation>
    </message>
    <message>
        <location/>
        <source>top margin</source>
        <translation>жоғарғы маржа</translation>
    </message>
    <message>
        <location/>
        <source>left margin</source>
        <translation>сол жақ шеті</translation>
    </message>
    <message>
        <location/>
        <source>right margin</source>
        <translation>оң жақ шет</translation>
    </message>
    <message>
        <location/>
        <source>bottom margin</source>
        <translation>төменгі маржа</translation>
    </message>
    <message>
        <location/>
        <source>Page Layout</source>
        <translation>Беттің орналасуы</translation>
    </message>
    <message>
        <location/>
        <source>Pages per sheet:</source>
        <translation>Параққа арналған беттер:</translation>
    </message>
    <message>
        <location/>
        <source>Page order:</source>
        <translation>Бет реті:</translation>
    </message>
</context>
<context>
    <name>QCupsJobWidget</name>
    <message>
        <location/>
        <source>Job Control</source>
        <translation>Тапсырмаларды бақылау</translation>
    </message>
    <message>
        <location/>
        <source>Scheduled printing:</source>
        <translation>Жоспарлы басу:</translation>
    </message>
    <message>
        <location/>
        <source>Print Immediately</source>
        <translation>Бірден басып шығару</translation>
    </message>
    <message>
        <location/>
        <source>Hold Indefinitely</source>
        <translation>Белгісіз мерзімге ұстап тұру</translation>
    </message>
    <message>
        <location/>
        <source>Day (06:00 to 17:59)</source>
        <translation>Күн (06:00 - 17:59)</translation>
    </message>
    <message>
        <location/>
        <source>Night (18:00 to 05:59)</source>
        <translation>Түнгі (18:00 - 05:59)</translation>
    </message>
    <message>
        <location/>
        <source>Second Shift (16:00 to 23:59)</source>
        <translation>Екінші ауысым (16:00 - 23:59)</translation>
    </message>
    <message>
        <location/>
        <source>Third Shift (00:00 to 07:59)</source>
        <translation>Үшінші ауысым (00:00 -ден 07:59-ға дейін)</translation>
    </message>
    <message>
        <location/>
        <source>Weekend (Saturday to Sunday)</source>
        <translation>Демалыс күндері (сенбіден жексенбіге дейін)</translation>
    </message>
    <message>
        <location/>
        <source>Specific Time</source>
        <translation>Нақты уақыт</translation>
    </message>
    <message>
        <location/>
        <source>Billing information:</source>
        <translation>Есепшот туралы мәліметтер:</translation>
    </message>
    <message>
        <location/>
        <source>Job priority:</source>
        <translation>Жұмыс орнының басымдылығы:</translation>
    </message>
    <message>
        <location/>
        <source>Banner Pages</source>
        <translation>«Баннер» санатындағы беттер</translation>
    </message>
    <message>
        <location/>
        <source>Start:</source>
        <translation>Бастау:</translation>
    </message>
    <message>
        <location/>
        <source>None</source>
        <translation>Ешқайсысы</translation>
    </message>
    <message>
        <location/>
        <source>Standard</source>
        <translation>Стандарт</translation>
    </message>
    <message>
        <location/>
        <source>Unclassified</source>
        <translation>Құпияланбаған</translation>
    </message>
    <message>
        <location/>
        <source>Confidential</source>
        <translation>Құпия</translation>
    </message>
    <message>
        <location/>
        <source>Classified</source>
        <translation>Жіктелген</translation>
    </message>
    <message>
        <location/>
        <source>Secret</source>
        <translation>Құпия</translation>
    </message>
    <message>
        <location/>
        <source>Top Secret</source>
        <translation>Ð Ð3/4Ñ Ð1/</translation>
    </message>
    <message>
        <location/>
        <source>End:</source>
        <translation>Аяқтау:</translation>
    </message>
</context>
<context>
    <name>QPluginLoader</name>
    <message>
        <location filename="../src/corelib/plugin/qpluginloader.cpp" line="+24"/>
        <source>Unknown error</source>
        <translation>Беймәлім қате</translation>
    </message>
    <message>
        <location line="-68"/>
        <source>The plugin was not loaded.</source>
        <translation>Плагин модулі жүктелмеді.</translation>
    </message>
</context>
<context>
    <name>QPrintDialog</name>
    <message>
        <location filename="../src/gui/painting/qprinterinfo_unix.cpp" line="+98"/>
        <source>locally connected</source>
        <translation>жергілікті байланысқан</translation>
    </message>
    <message>
        <location line="+23"/>
        <location line="+225"/>
        <source>Aliases: %1</source>
        <translation>Бүркеншік аттары:% 1</translation>
    </message>
    <message>
        <location line="+223"/>
        <location line="+199"/>
        <source>unknown</source>
        <translation>беймәлім</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_qws.cpp" line="+375"/>
        <source>Print all</source>
        <translation>Барлығын басып шығару</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print selection</source>
        <translation>Басып шығару таңдауы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print range</source>
        <translation>Басып шығару ауқымы</translation>
    </message>
    <message>
        <location line="-48"/>
        <source>A0 (841 x 1189 mm)</source>
        <translation>А0 (841 х 1189 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A1 (594 x 841 mm)</source>
        <translation>А1 (594 х 841 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A2 (420 x 594 mm)</source>
        <translation>А2 (420 х 594 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A3 (297 x 420 mm)</source>
        <translation>А3 (297 х 420 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A4 (210 x 297 mm, 8.26 x 11.7 inches)</source>
        <translation>А4 (210 х 297 мм, 8,26 х 11,7 дюйм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A5 (148 x 210 mm)</source>
        <translation>А5 (148 х 210 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A6 (105 x 148 mm)</source>
        <translation>А6 (105 х 148 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A7 (74 x 105 mm)</source>
        <translation>А7 (74 х 105 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A8 (52 x 74 mm)</source>
        <translation>А8 (52 х 74 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A9 (37 x 52 mm)</source>
        <translation>А9 (37 х 52 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B0 (1000 x 1414 mm)</source>
        <translation>B0 (1000 х 1414 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B1 (707 x 1000 mm)</source>
        <translation>В1 (707 х 1000 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B2 (500 x 707 mm)</source>
        <translation>В2 (500 х 707 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B3 (353 x 500 mm)</source>
        <translation>B3 (353 х 500 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B4 (250 x 353 mm)</source>
        <translation>B4 (250 х 353 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B5 (176 x 250 mm, 6.93 x 9.84 inches)</source>
        <translation>В5 (176 х 250 мм, 6,93 х 9,84 дюйм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B6 (125 x 176 mm)</source>
        <translation>B6 (125 х 176 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B7 (88 x 125 mm)</source>
        <translation>B7 (88 х 125 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B8 (62 x 88 mm)</source>
        <translation>B8 (62 х 88 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B9 (44 x 62 mm)</source>
        <translation>B9 (44 х 62 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B10 (31 x 44 mm)</source>
        <translation>В10 (31 х 44 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>C5E (163 x 229 mm)</source>
        <translation>C5E (163 х 229 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>DLE (110 x 220 mm)</source>
        <translation>DLE (110 х 220 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Executive (7.5 x 10 inches, 191 x 254 mm)</source>
        <translation>Атқарушылық (7,5 х 10 дюйм, 191 х 254 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Folio (210 x 330 mm)</source>
        <translation>Фолио (210 х 330 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ledger (432 x 279 mm)</source>
        <translation>Бухгалтерлік кiрiс (432 х 279 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Legal (8.5 x 14 inches, 216 x 356 mm)</source>
        <translation>Заңды (8,5 х 14 дюйм, 216 х 356 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Letter (8.5 x 11 inches, 216 x 279 mm)</source>
        <translation>Әріп (8,5 х 11 дюйм, 216 х 279 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tabloid (279 x 432 mm)</source>
        <translation>Таблоид (279 х 432 мм)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>US Common #10 Envelope (105 x 241 mm)</source>
        <translation>АҚШ жалпы #10 конверті (105 х 241 мм)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_win.cpp" line="+268"/>
        <source>OK</source>
        <translation>Жақсы</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qabstractprintdialog.cpp" line="+110"/>
        <location line="+13"/>
        <location filename="../src/gui/dialogs/qprintdialog_win.cpp" line="-2"/>
        <source>Print</source>
        <translation>Басып шығару</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="-357"/>
        <source>Print To File ...</source>
        <translation>Файлға басып шығару...</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>File %1 is not writable.
Please choose a different file name.</source>
        <translation>% 1 файлы жазылмайды.
Басқа файл атауын таңдаңыз.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1 already exists.
Do you want to overwrite it?</source>
        <translation>% 1 деген бар ғой.
Оны қайта жазу керек пе?</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_qws.cpp" line="-210"/>
        <source>File exists</source>
        <translation>Файл бар</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;qt&gt;Do you want to overwrite it?&lt;/qt&gt;</source>
        <translation>&lt;qt&gt; Оны қайта жазу керек пе? &lt;/qt&gt;</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="-8"/>
        <source>%1 is a directory.
Please choose a different file name.</source>
        <translation>% 1 - каталог.
Басқа файл атауын таңдаңыз.</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_win.cpp" line="+1"/>
        <source>The &apos;From&apos; value cannot be greater than the &apos;To&apos; value.</source>
        <translation>«Кімнен» мәні «Кімге» мәнінен артық бола алмайды.</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupdialog_unix.cpp" line="-232"/>
        <source>A0</source>
        <translation>А0</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A1</source>
        <translation>А1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A2</source>
        <translation>А2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A3</source>
        <translation>A3</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A4</source>
        <translation>A4</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A5</source>
        <translation>A5</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A6</source>
        <translation>A6</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A7</source>
        <translation>A7</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A8</source>
        <translation>А8</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A9</source>
        <translation>A9</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B0</source>
        <translation>B0</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B1</source>
        <translation>B1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B2</source>
        <translation>B2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B3</source>
        <translation>B3</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B4</source>
        <translation>B4</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B5</source>
        <translation>B5</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B6</source>
        <translation>B6</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B7</source>
        <translation>B7</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B8</source>
        <translation>B8</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B9</source>
        <translation>B9</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B10</source>
        <translation>B10</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>C5E</source>
        <translation>C5E</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>DLE</source>
        <translation>DLE</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Executive</source>
        <translation>Атқарушы орган</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Folio</source>
        <translation>Фолио</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ledger</source>
        <translation>Леджер</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Legal</source>
        <translation>Заңды</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Letter</source>
        <translation>Хат</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tabloid</source>
        <translation>Таблоид</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>US Common #10 Envelope</source>
        <translation>АҚШ ортақ #10 конверті</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Folio (8.27 x 13 in)</source>
        <translation>Фолио (8,27 х 13 в)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Custom</source>
        <translation>Реттелетін</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="-522"/>
        <location line="+68"/>
        <source>&amp;Options &gt;&gt;</source>
        <translation>Параметрлер &gt;&gt;</translation>
    </message>
    <message>
        <location line="-63"/>
        <source>&amp;Print</source>
        <translation>&gt; Басып шығару</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>&amp;Options &lt;&lt;</source>
        <translation>&gt; Параметрлері &lt;&lt;</translation>
    </message>
    <message>
        <location line="+253"/>
        <source>Print to File (PDF)</source>
        <translation>Файлға басып шығару (PDF)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print to File (Postscript)</source>
        <translation>Файлға басып шығару (Postscript)</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Local file</source>
        <translation>Жергілікті файл</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Write %1 file</source>
        <translation>% 1 файлын жазу</translation>
    </message>
</context>
<context>
    <name>QPrintPreviewDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qabstractpagesetupdialog.cpp" line="+68"/>
        <location line="+12"/>
        <source>Page Setup</source>
        <translation>Бетті баптау</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintpreviewdialog.cpp" line="+252"/>
        <source>%1%</source>
        <translation>%1%</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Print Preview</source>
        <translation>Басып шығаруды алдын ала қарау</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Next page</source>
        <translation>Келесі бет</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous page</source>
        <translation>Алдыңғы бет</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>First page</source>
        <translation>Бірінші бет</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last page</source>
        <translation>Соңғы бет</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Fit width</source>
        <translation>Енін қиыстыру</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fit page</source>
        <translation>Бетті қиыстыру</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Zoom in</source>
        <translation>Үлкейту</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom out</source>
        <translation>Үлкейту</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Portrait</source>
        <translation>Портрет</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Landscape</source>
        <translation>Ландшафт</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Show single page</source>
        <translation>Бір бетті көрсету</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show facing pages</source>
        <translation>Бетпе-бетті көрсету</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show overview of all pages</source>
        <translation>Барлық беттерге шолу көрсету</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Print</source>
        <translation>Басып шығару</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page setup</source>
        <translation>Бетті баптау</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close</source>
        <translation>Жабу</translation>
    </message>
    <message>
        <location line="+151"/>
        <source>Export to PDF</source>
        <translation>PDF- ке экспорттау</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Export to PostScript</source>
        <translation>PostScript бағдарламасына экспорттау</translation>
    </message>
</context>
<context>
    <name>QPrintPropertiesWidget</name>
    <message>
        <location filename="../src/gui/dialogs/qprintpropertieswidget.ui"/>
        <source>Form</source>
        <translation>Пішін</translation>
    </message>
    <message>
        <location/>
        <source>Page</source>
        <translation>Бет</translation>
    </message>
    <message>
        <location/>
        <source>Advanced</source>
        <translation>Қосымша</translation>
    </message>
</context>
<context>
    <name>QPrintSettingsOutput</name>
    <message>
        <location filename="../src/gui/dialogs/qprintsettingsoutput.ui"/>
        <source>Form</source>
        <translation>Пішін</translation>
    </message>
    <message>
        <location/>
        <source>Copies</source>
        <translation>Көшірмелер</translation>
    </message>
    <message>
        <location/>
        <source>Print range</source>
        <translation>Басып шығару ауқымы</translation>
    </message>
    <message>
        <location/>
        <source>Print all</source>
        <translation>Барлығын басып шығару</translation>
    </message>
    <message>
        <location/>
        <source>Pages from</source>
        <translation>«Бет» санатындағы беттер</translation>
    </message>
    <message>
        <location/>
        <source>to</source>
        <translation>to</translation>
    </message>
    <message>
        <location/>
        <source>Pages</source>
        <translation>«Бет» санатындағы беттер</translation>
    </message>
    <message>
        <location/>
        <source>Page Set:</source>
        <translation>Бет жиыны:</translation>
    </message>
    <message>
        <location/>
        <source>Selection</source>
        <translation>Таңдау</translation>
    </message>
    <message>
        <location/>
        <source>Output Settings</source>
        <translation>Шығыс параметрлері</translation>
    </message>
    <message>
        <location/>
        <source>Copies:</source>
        <translation>Көшірмелер:</translation>
    </message>
    <message>
        <location/>
        <source>Collate</source>
        <translation>Коллате</translation>
    </message>
    <message>
        <location/>
        <source>Reverse</source>
        <translation>Кері</translation>
    </message>
    <message>
        <location/>
        <source>Options</source>
        <translation>Параметрлер</translation>
    </message>
    <message>
        <location/>
        <source>Color Mode</source>
        <translation>Түс режімі</translation>
    </message>
    <message>
        <location/>
        <source>Color</source>
        <translation>Түс</translation>
    </message>
    <message>
        <location/>
        <source>Grayscale</source>
        <translation>Сұр реңк</translation>
    </message>
    <message>
        <location/>
        <source>Duplex Printing</source>
        <translation>Duplex басып шығару</translation>
    </message>
    <message>
        <location/>
        <source>None</source>
        <translation>Ешқайсысы</translation>
    </message>
    <message>
        <location/>
        <source>Long side</source>
        <translation>Ұзын жағы</translation>
    </message>
    <message>
        <location/>
        <source>Short side</source>
        <translation>Қысқа жағы</translation>
    </message>
</context>
<context>
    <name>QPrintWidget</name>
    <message>
        <location filename="../src/gui/dialogs/qprintwidget.ui"/>
        <source>Form</source>
        <translation>Пішін</translation>
    </message>
    <message>
        <location/>
        <source>Printer</source>
        <translation>Принтер</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Name:</source>
        <translation>&gt; Атауы:</translation>
    </message>
    <message>
        <location/>
        <source>P&amp;roperties</source>
        <translation>P&gt; roperties</translation>
    </message>
    <message>
        <location/>
        <source>Location:</source>
        <translation>Орналасқан жері:</translation>
    </message>
    <message>
        <location/>
        <source>Preview</source>
        <translation>Алдын ала қарау</translation>
    </message>
    <message>
        <location/>
        <source>Type:</source>
        <translation>Түрі:</translation>
    </message>
    <message>
        <location/>
        <source>Output &amp;file:</source>
        <translation>Шығыс &amp; файлы:</translation>
    </message>
    <message>
        <location/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>QProcess</name>
    <message>
        <location filename="../src/corelib/io/qprocess_unix.cpp" line="+475"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+147"/>
        <source>Could not open input redirection for reading</source>
        <translation>Оқу үшін кірісті қайта бағыттау ашылмады</translation>
    </message>
    <message>
        <location line="+12"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+36"/>
        <source>Could not open output redirection for writing</source>
        <translation>Жазу үшін шығысты қайта бағыттау ашылмады</translation>
    </message>
    <message>
        <location line="+235"/>
        <source>Resource error (fork failure): %1</source>
        <translation>Ресурс қатесі (форк істен шығуы):% 1</translation>
    </message>
    <message>
        <location line="+259"/>
        <location line="+53"/>
        <location line="+74"/>
        <location line="+67"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+422"/>
        <location line="+50"/>
        <location line="+75"/>
        <location line="+42"/>
        <location line="+54"/>
        <source>Process operation timed out</source>
        <translation>Уақыт өткен процесс операциясы</translation>
    </message>
    <message>
        <location filename="../src/corelib/io/qprocess.cpp" line="+533"/>
        <location line="+52"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="-211"/>
        <location line="+50"/>
        <source>Error reading from process</source>
        <translation>Процестен оқу қатесі</translation>
    </message>
    <message>
        <location line="+47"/>
        <location line="+779"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+140"/>
        <source>Error writing to process</source>
        <translation>Өңдеуге қате жазу</translation>
    </message>
    <message>
        <location line="-709"/>
        <source>Process crashed</source>
        <translation>Процесс қирауға ұшырады</translation>
    </message>
    <message>
        <location line="+912"/>
        <source>No program defined</source>
        <translation>Бағдарлама анықталмаған</translation>
    </message>
    <message>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="-341"/>
        <source>Process failed to start</source>
        <translation>Процесс басталмады</translation>
    </message>
</context>
<context>
    <name>QProgressDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qprogressdialog.cpp" line="+182"/>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
</context>
<context>
    <name>QPushButton</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="-8"/>
        <source>Open</source>
        <translation>Ашу</translation>
    </message>
</context>
<context>
    <name>QRadioButton</name>
    <message>
        <location line="+12"/>
        <source>Check</source>
        <translation>Тексеру</translation>
    </message>
</context>
<context>
    <name>QRegExp</name>
    <message>
        <location filename="../src/corelib/tools/qregexp.cpp" line="+64"/>
        <source>no error occurred</source>
        <translation>қате болған жоқ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>disabled feature used</source>
        <translation>пайдаланылатын ажырату мүмкіндігі</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad char class syntax</source>
        <translation>жаман шар класының синтаксисі</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad lookahead syntax</source>
        <translation>жаман көріністі синтаксис</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad repetition syntax</source>
        <translation>жаман қайталау синтаксисі</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid octal value</source>
        <translation>жарамсыз сегіздік мән</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>missing left delim</source>
        <translation>жоқ сол жақ делим</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected end</source>
        <translation>күтпеген соңы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>met internal limit</source>
        <translation>қанағаттандырыл- ған ішкі шегі</translation>
    </message>
</context>
<context>
    <name>QSQLite2Driver</name>
    <message>
        <location filename="../src/sql/drivers/sqlite2/qsql_sqlite2.cpp" line="+396"/>
        <source>Error to open database</source>
        <translation>Дерекқорды ашу қатесі</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Unable to begin transaction</source>
        <translation>Транзакцияны бастау мүмкін емес</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to commit transaction</source>
        <translation>Мәміле жасау мүмкін емес</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback Transaction</source>
        <translation>Транзакцияны кері қайтару мүмкін емес</translation>
    </message>
</context>
<context>
    <name>QSQLite2Result</name>
    <message>
        <location line="-323"/>
        <source>Unable to fetch results</source>
        <translation>Нәтижелерді алу мүмкін емес</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>Unable to execute statement</source>
        <translation>Мәлімдемені орындау мүмкін емес</translation>
    </message>
</context>
<context>
    <name>QSQLiteDriver</name>
    <message>
        <location filename="../src/sql/drivers/sqlite/qsql_sqlite.cpp" line="+528"/>
        <source>Error opening database</source>
        <translation>Дерекқорды ашу қатесі</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error closing database</source>
        <translation>Дерекқорды жабу қатесі</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to begin transaction</source>
        <translation>Транзакцияны бастау мүмкін емес</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to commit transaction</source>
        <translation>Мәміле жасау мүмкін емес</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to rollback transaction</source>
        <translation>Транзакцияны кері қайтару мүмкін емес</translation>
    </message>
</context>
<context>
    <name>QSQLiteResult</name>
    <message>
        <location line="-400"/>
        <location line="+66"/>
        <location line="+8"/>
        <source>Unable to fetch row</source>
        <translation>Жолды алу мүмкін емес</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Unable to execute statement</source>
        <translation>Мәлімдемені орындау мүмкін емес</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to reset statement</source>
        <translation>Мәлімдемені қалпына келтіру мүмкін емес</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Unable to bind parameters</source>
        <translation>Параметрлерді байланыстыру мүмкін емес</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Parameter count mismatch</source>
        <translation>Параметр санының сәйкес келмеуі</translation>
    </message>
    <message>
        <location line="-208"/>
        <source>No query</source>
        <translation>Сұрау жоқ</translation>
    </message>
</context>
<context>
    <name>QScrollBar</name>
    <message>
        <location filename="../src/gui/widgets/qscrollbar.cpp" line="+448"/>
        <source>Scroll here</source>
        <translation>Осында айналдыру</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Left edge</source>
        <translation>Сол жақ шеті</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Top</source>
        <translation>Жоғарғы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right edge</source>
        <translation>Оң жақ шеті</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Bottom</source>
        <translation>Төменгі жағы</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Page left</source>
        <translation>Сол жақтағы бет</translation>
    </message>
    <message>
        <location line="+0"/>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+143"/>
        <source>Page up</source>
        <translation>Бетті жоғарыға</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page right</source>
        <translation>Бет оң жақта</translation>
    </message>
    <message>
        <location line="+0"/>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+4"/>
        <source>Page down</source>
        <translation>Бетті төмен қаратып</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scroll left</source>
        <translation>Солға жылжыту</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll up</source>
        <translation>Жоғары айналдыру</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll right</source>
        <translation>Оңға жылжыту</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll down</source>
        <translation>Төмен айналдыру</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="-6"/>
        <source>Line up</source>
        <translation>Сапқа тұрғызылсын</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Position</source>
        <translation>Лауазымы</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Line down</source>
        <translation>Төмен сызық</translation>
    </message>
</context>
<context>
    <name>QSharedMemory</name>
    <message>
        <location filename="../src/corelib/kernel/qsharedmemory.cpp" line="+207"/>
        <source>%1: unable to set key on lock</source>
        <translation>% 1: кілтті құлыптауға орната алмады</translation>
    </message>
    <message>
        <location line="+81"/>
        <source>%1: create size is less then 0</source>
        <translation>% 1: жасау өлшемі 0-ден кем</translation>
    </message>
    <message>
        <location line="+168"/>
        <location filename="../src/corelib/kernel/qsharedmemory_p.h" line="+148"/>
        <source>%1: unable to lock</source>
        <translation>% 1: құлыптау мүмкін емес</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>%1: unable to unlock</source>
        <translation>% 1: құлыпты ашу мүмкін емес</translation>
    </message>
    <message>
        <location filename="../src/corelib/kernel/qsharedmemory_unix.cpp" line="+78"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+87"/>
        <source>%1: permission denied</source>
        <translation>% 1: рұқсаты жоқ</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="-22"/>
        <source>%1: already exists</source>
        <translation>% 1: бұрыннан бар</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+9"/>
        <source>%1: doesn&apos;t exists</source>
        <translation>% 1: жоқ</translation>
    </message>
    <message>
        <location line="+6"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+9"/>
        <source>%1: out of resources</source>
        <translation>% 1: ресурстардан тыс</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+7"/>
        <source>%1: unknown error %2</source>
        <translation>% 1: беймәлім қате% 2</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>%1: key is empty</source>
        <translation>% 1: кілт бос</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>%1: unix key file doesn&apos;t exists</source>
        <translation>% 1: unix кілт файлы жоқ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>%1: ftok failed</source>
        <translation>% 1: ftok жаңылысы</translation>
    </message>
    <message>
        <location line="+51"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+15"/>
        <source>%1: unable to make key</source>
        <translation>% 1: кілтті жасау мүмкін емес</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>%1: system-imposed size restrictions</source>
        <translation>% 1: жүйеге енгізілген өлшем шектеулері</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>%1: not attached</source>
        <translation>% 1: тіркелмеген</translation>
    </message>
    <message>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="-27"/>
        <source>%1: invalid size</source>
        <translation>% 1: өлшемі жарамсыз</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>%1: key error</source>
        <translation>% 1: кілт қатесі</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>%1: size query failed</source>
        <translation>% 1: өлшемді сұрау жаңылысы</translation>
    </message>
</context>
<context>
    <name>QShortcut</name>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="+373"/>
        <source>Space</source>
        <translation>Бос орын</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Esc</source>
        <translation>Эск</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tab</source>
        <translation>Қойынды</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Backtab</source>
        <translation>Backtab</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Backspace</source>
        <translation>Backspace</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Return</source>
        <translation>Қайтарым</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Enter</source>
        <translation>Enter</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ins</source>
        <translation>Инс</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Del</source>
        <translation>Дель</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pause</source>
        <translation>Кідіріс</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print</source>
        <translation>Басып шығару</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>SysReq</source>
        <translation>SysReq</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Home</source>
        <translation>Үй</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>End</source>
        <translation>Аяқтау</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Left</source>
        <translation>Сол жақта</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Up</source>
        <translation>Жоғары</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right</source>
        <translation>Оң жақта</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Down</source>
        <translation>Төмен</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PgUp</source>
        <translation>PGUP</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PgDown</source>
        <translation>PGDown</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CapsLock</source>
        <translation>Капслок</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>NumLock</source>
        <translation>NumLock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ScrollLock</source>
        <translation>ScrollLock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Menu</source>
        <translation>Мәзір</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Help</source>
        <translation>Анықтама</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Back</source>
        <translation>Артқа</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Forward</source>
        <translation>Қайта жіберу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop</source>
        <translation>Тоқтау</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Refresh</source>
        <translation>Жаңарту</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Down</source>
        <translation>Дыбыстың төмен түсуі</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Mute</source>
        <translation>Дыбыс дыбысын өшіру</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Up</source>
        <translation>Дыбыстың жоғары көтерілгені</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Boost</source>
        <translation>Bass Boost</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Up</source>
        <translation>Басс-Уп</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Down</source>
        <translation>Басс-Даун</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Treble Up</source>
        <translation>Требл Уп</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Treble Down</source>
        <translation>Требл-Даун</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Play</source>
        <translation>Мультимедиа ойнату</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Stop</source>
        <translation>Мультимедиа аялдамасы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Previous</source>
        <translation>Алдыңғы медиа</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Next</source>
        <translation>Келесі медиа</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Record</source>
        <translation>Мультимедиа жазбасы</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Favorites</source>
        <translation>Таңдаулылар</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search</source>
        <translation>Іздеу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Standby</source>
        <translation>Күту</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open URL</source>
        <translation>URL мекенжайын ашу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch Mail</source>
        <translation>Поштаны іске қосу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch Media</source>
        <translation>Медианы іске қосу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (0)</source>
        <translation>Іске қосу (0)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (1)</source>
        <translation>Іске қосу (1)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (2)</source>
        <translation>Іске қосу (2)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (3)</source>
        <translation>Іске қосу (3)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (4)</source>
        <translation>Іске қосу (4)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (5)</source>
        <translation>Іске қосу (5)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (6)</source>
        <translation>Іске қосу (6)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (7)</source>
        <translation>Іске қосу (7)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (8)</source>
        <translation>Іске қосу (8)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (9)</source>
        <translation>Іске қосу (9)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (A)</source>
        <translation>Іске қосу (А)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (B)</source>
        <translation>Іске қосу (B)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (C)</source>
        <translation>Іске қосу (C)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (D)</source>
        <translation>Іске қосу (D)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (E)</source>
        <translation>Іске қосу (Е)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (F)</source>
        <translation>Іске қосу (F)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Print Screen</source>
        <translation>Басып шығару экраны</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page Up</source>
        <translation>Бетті жоғарыға</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page Down</source>
        <translation>Бетті төмен қаратып</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Caps Lock</source>
        <translation>Жоғарғы регистр</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Num Lock</source>
        <translation>Num Lock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Number Lock</source>
        <translation>Санды бұғаттау</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll Lock</source>
        <translation>Бұғаттау</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insert</source>
        <translation>Кірістіру</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>Өшіру</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Escape</source>
        <translation>Қашу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>System Request</source>
        <translation>Жүйелік сұрау</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Select</source>
        <translation>Таңдау</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Yes</source>
        <translation>Иә</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>No</source>
        <translation>Жоқ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Context1</source>
        <translation>Контекст1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context2</source>
        <translation>Контекст2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context3</source>
        <translation>Контекст3</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context4</source>
        <translation>Контекст4</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Call</source>
        <translation>Қоңырау шалу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hangup</source>
        <translation>Хангюп</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Flip</source>
        <translation>Слип</translation>
    </message>
    <message>
        <location line="+527"/>
        <location line="+122"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location line="-121"/>
        <location line="+125"/>
        <source>Shift</source>
        <translation>Ығыстыру</translation>
    </message>
    <message>
        <location line="-124"/>
        <location line="+122"/>
        <source>Alt</source>
        <translation>Альт</translation>
    </message>
    <message>
        <location line="-121"/>
        <location line="+117"/>
        <source>Meta</source>
        <translation>Мета</translation>
    </message>
    <message>
        <location line="-25"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>F%1</source>
        <translation>F% 1</translation>
    </message>
    <message>
        <location line="-720"/>
        <source>Home Page</source>
        <translation>Басты бет</translation>
    </message>
</context>
<context>
    <name>QSlider</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+151"/>
        <source>Page left</source>
        <translation>Сол жақтағы бет</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page up</source>
        <translation>Бетті жоғарыға</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Position</source>
        <translation>Лауазымы</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Page right</source>
        <translation>Бет оң жақта</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page down</source>
        <translation>Бетті төмен қаратып</translation>
    </message>
</context>
<context>
    <name>QSocks5SocketEngine</name>
    <message>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="-67"/>
        <source>Connection to proxy refused</source>
        <translation>Сенімхатқа қосылудан бас тартылды</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection to proxy closed prematurely</source>
        <translation>Проксиге қосылу мерзімінен бұрын жабылды</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Proxy host not found</source>
        <translation>Прокси хосты табылмады</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Connection to proxy timed out</source>
        <translation>Проксиге қосылу уақыты</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Proxy authentication failed</source>
        <translation>Прокси аутентификациясы жаңылысы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Proxy authentication failed: %1</source>
        <translation>Прокси аутентификациясы жаңылысы:% 1</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>SOCKS version 5 protocol error</source>
        <translation>SOCKS 5 хаттамасының қатесі</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>General SOCKSv5 server failure</source>
        <translation>Жалпы SOCKSv5 серверінің істен шығуы</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection not allowed by SOCKSv5 server</source>
        <translation>SOCKSv5 серверімен қосылым рұқсат етілмеген</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>TTL expired</source>
        <translation>TTL мерзімі бітті</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>SOCKSv5 command not supported</source>
        <translation>SOCKSv5 пәрмені қолдау көрсетілмеген</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Address type not supported</source>
        <translation>Адрес түріне қолдау көрсетілмеген</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown SOCKSv5 proxy error code 0x%1</source>
        <translation>Беймәлім SOCKSv5 прокси коды 0x% 1</translation>
    </message>
    <message>
        <source>Socks5 timeout error connecting to socks server</source>
        <translation type="obsolete">连接到套接字服务器的时候，Socks5超时错误</translation>
    </message>
    <message>
        <location line="+685"/>
        <source>Network operation timed out</source>
        <translation>Желілік жұмыс уақыты бітті</translation>
    </message>
</context>
<context>
    <name>QSpinBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="-574"/>
        <source>More</source>
        <translation>Қосымша</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Less</source>
        <translation>Азырақ</translation>
    </message>
</context>
<context>
    <name>QSql</name>
    <message>
        <location filename="../src/qt3support/sql/q3sqlmanager_p.cpp" line="+890"/>
        <source>Delete</source>
        <translation>Өшіру</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete this record?</source>
        <translation>Бұл жазбаны жойыңыз ба?</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+16"/>
        <location line="+36"/>
        <source>Yes</source>
        <translation>Иә</translation>
    </message>
    <message>
        <location line="-51"/>
        <location line="+16"/>
        <location line="+36"/>
        <source>No</source>
        <translation>Жоқ</translation>
    </message>
    <message>
        <location line="-44"/>
        <source>Insert</source>
        <translation>Кірістіру</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Update</source>
        <translation>Жаңарту</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save edits?</source>
        <translation>Өңдеулерді сақтаңыз ба?</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Confirm</source>
        <translation>Растау</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel your edits?</source>
        <translation>Өңдеуден бас тартыңыз ба?</translation>
    </message>
</context>
<context>
    <name>QSslSocket</name>
    <message>
        <location filename="../src/network/ssl/qsslsocket_openssl.cpp" line="+569"/>
        <source>Unable to write data: %1</source>
        <translation>Деректерді жазу мүмкін емес:% 1</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>Error while reading: %1</source>
        <translation>Оқу кезіндегі қате:% 1</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>Error during SSL handshake: %1</source>
        <translation>SCL қол алысу кезіндегі қате:% 1</translation>
    </message>
    <message>
        <location line="-524"/>
        <source>Error creating SSL context (%1)</source>
        <translation>SCL контекстін жасау қатесі (% 1)</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Invalid or empty cipher list (%1)</source>
        <translation>Жарамсыз немесе бос шифрлар тізімі (% 1)</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Error creating SSL session, %1</source>
        <translation>SSL сеансын жасау қатесі, % 1</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Error creating SSL session: %1</source>
        <translation>SSL сеансын жасау қатесі:% 1</translation>
    </message>
    <message>
        <location line="-61"/>
        <source>Cannot provide a certificate with no key, %1</source>
        <translation>% 1 кілті жоқ сертификат берілмеген</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error loading local certificate, %1</source>
        <translation>Жергілікті сертификатты жүктеу қатесі, % 1</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Error loading private key, %1</source>
        <translation>Жеке кілтті жүктеу қатесі, % 1</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Private key does not certificate public key, %1</source>
        <translation>Жеке кілт жалпыға қолжетімді кілтті сертификаттамайды, % 1</translation>
    </message>
</context>
<context>
    <name>QSystemSemaphore</name>
    <message>
        <location filename="../src/corelib/kernel/qsystemsemaphore_unix.cpp" line="-41"/>
        <location filename="../src/corelib/kernel/qsystemsemaphore_win.cpp" line="+66"/>
        <source>%1: out of resources</source>
        <translation>% 1: ресурстардан тыс</translation>
    </message>
    <message>
        <location line="-13"/>
        <location filename="../src/corelib/kernel/qsystemsemaphore_win.cpp" line="+4"/>
        <source>%1: permission denied</source>
        <translation>% 1: рұқсаты жоқ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: already exists</source>
        <translation>% 1: бұрыннан бар</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: does not exist</source>
        <translation>% 1: жоқ</translation>
    </message>
    <message>
        <location line="+9"/>
        <location filename="../src/corelib/kernel/qsystemsemaphore_win.cpp" line="+3"/>
        <source>%1: unknown error %2</source>
        <translation>% 1: беймәлім қате% 2</translation>
    </message>
</context>
<context>
    <name>QTDSDriver</name>
    <message>
        <location filename="../src/sql/drivers/tds/qsql_tds.cpp" line="+584"/>
        <source>Unable to open connection</source>
        <translation>Қосылым ашылмады</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unable to use database</source>
        <translation>Дерекқорды пайдалану мүмкін емес</translation>
    </message>
</context>
<context>
    <name>QTabBar</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="-326"/>
        <source>Scroll Left</source>
        <translation>Солға жылжыту</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll Right</source>
        <translation>Оңға жылжыту</translation>
    </message>
</context>
<context>
    <name>QTcpServer</name>
    <message>
        <location filename="../src/network/socket/qtcpserver.cpp" line="+282"/>
        <source>Operation on socket is not supported</source>
        <translation>Розеткадағы операцияға қолдау көрсетілмеген</translation>
    </message>
</context>
<context>
    <name>QTextControl</name>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="+1973"/>
        <source>&amp;Undo</source>
        <translation>&gt; Болдырмау</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Redo</source>
        <translation>&gt; Редо</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cu&amp;t</source>
        <translation>Cu&gt;</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Copy</source>
        <translation>&amp; Көшіру</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Copy &amp;Link Location</source>
        <translation>Орынды көшіру</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Paste</source>
        <translation>&gt; Қою</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete</source>
        <translation>Өшіру</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Select All</source>
        <translation>Барлығын таңдау</translation>
    </message>
</context>
<context>
    <name>QToolButton</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="+254"/>
        <location line="+6"/>
        <source>Press</source>
        <translation>Пресс</translation>
    </message>
    <message>
        <location line="-4"/>
        <location line="+8"/>
        <source>Open</source>
        <translation>Ашу</translation>
    </message>
</context>
<context>
    <name>QUdpSocket</name>
    <message>
        <location filename="../src/network/socket/qudpsocket.cpp" line="+169"/>
        <source>This platform does not support IPv6</source>
        <translation>Бұл платформа IPv6-ны қолдамайды</translation>
    </message>
</context>
<context>
    <name>QUndoGroup</name>
    <message>
        <location filename="../src/gui/util/qundogroup.cpp" line="+386"/>
        <source>Undo</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Redo</source>
        <translation>Редо</translation>
    </message>
</context>
<context>
    <name>QUndoModel</name>
    <message>
        <location filename="../src/gui/util/qundoview.cpp" line="+101"/>
        <source>&lt;empty&gt;</source>
        <translation>&lt;empty&gt;</translation>
    </message>
</context>
<context>
    <name>QUndoStack</name>
    <message>
        <location filename="../src/gui/util/qundostack.cpp" line="+834"/>
        <source>Undo</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Redo</source>
        <translation>Редо</translation>
    </message>
</context>
<context>
    <name>QUnicodeControlCharacterMenu</name>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="+884"/>
        <source>LRM Left-to-right mark</source>
        <translation>LRM солдан оңға қарай белгісі</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLM Right-to-left mark</source>
        <translation>RLM оңнан солға қарай белгісі</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWJ Zero width joiner</source>
        <translation>ZWJ Zero еніне қосылу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWNJ Zero width non-joiner</source>
        <translation>ZWNJ Zero ені қосылмаған</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWSP Zero width space</source>
        <translation>ZWSP Нөл ені кеңістігі</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>LRE Start of left-to-right embedding</source>
        <translation>LRE Солдан оңға қарай ендіруді бастау</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLE Start of right-to-left embedding</source>
        <translation>RLE Оңнан солға ендіруді бастау</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>LRO Start of left-to-right override</source>
        <translation>LRO Солдан оңға қарай басымдылықтың басталуы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLO Start of right-to-left override</source>
        <translation>RLO Оңнан солға қарай басталу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PDF Pop directional formatting</source>
        <translation>PDF поп бағытын пішімдеу</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Insert Unicode control character</source>
        <translation>Юникодты басқару таңбасын кірістіру</translation>
    </message>
</context>
<context>
    <name>QWebFrame</name>
    <message>
        <location filename="../src/3rdparty/webkit/WebKit/qt/WebCoreSupport/FrameLoaderClientQt.cpp" line="+692"/>
        <source>Request cancelled</source>
        <translation>Сұрау күшін жойды</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Request blocked</source>
        <translation>Сұрау бұғатталған</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot show URL</source>
        <translation>URL мекенжайы көрсетілмеді</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Frame load interruped by policy change</source>
        <translation>Саясаттың өзгеруімен өзара әрекеттесіп орналасқан рамалық жүктеме</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot show mimetype</source>
        <translation>Миметип көрсетілмеді</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>File does not exist</source>
        <translation>Файл жоқ</translation>
    </message>
</context>
<context>
    <name>QWebPage</name>
    <message>
        <location filename="../src/3rdparty/webkit/WebCore/platform/network/qt/QNetworkReplyHandler.cpp" line="+382"/>
        <source>Bad HTTP request</source>
        <translation>Жаман HTTP сұрауы</translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebCore/platform/qt/Localizations.cpp" line="+42"/>
        <source>Submit</source>
        <comment>default label for Submit buttons in forms on web pages</comment>
        <translation>Жіберу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Submit</source>
        <comment>Submit (input element) alt text for &lt;input&gt; elements with no alt, title, or value</comment>
        <translation>Жіберу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Reset</source>
        <comment>default label for Reset buttons in forms on web pages</comment>
        <translation>Ысыру</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>This is a searchable index. Enter search keywords: </source>
        <comment>text that appears at the start of nearly-obsolete web pages in the form of a &apos;searchable index&apos;</comment>
        <translation>Бұл іздеуге болатын индекс. Іздеу кілт сөздерін енгізіңіз: </translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Choose File</source>
        <comment>title for file button used in HTML forms</comment>
        <translation>Файлды таңдау</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No file selected</source>
        <comment>text to display in file button used in HTML forms when no file is selected</comment>
        <translation>Файл таңдалмаған</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open in New Window</source>
        <comment>Open in New Window context menu item</comment>
        <translation>Жаңа терезеде ашу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save Link...</source>
        <comment>Download Linked File context menu item</comment>
        <translation>Сілтемені сақтау...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy Link</source>
        <comment>Copy Link context menu item</comment>
        <translation>Сілтемені көшіру</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open Image</source>
        <comment>Open Image in New Window context menu item</comment>
        <translation>Кескінді ашу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save Image</source>
        <comment>Download Image context menu item</comment>
        <translation>Кескінді сақтау</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy Image</source>
        <comment>Copy Link context menu item</comment>
        <translation>Кескінді көшіру</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open Frame</source>
        <comment>Open Frame in New Window context menu item</comment>
        <translation>Жақтауды ашу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy</source>
        <comment>Copy context menu item</comment>
        <translation>Көшіру</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Go Back</source>
        <comment>Back context menu item</comment>
        <translation>Қайтып бар</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Go Forward</source>
        <comment>Forward context menu item</comment>
        <translation>Алға жылжу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Stop</source>
        <comment>Stop context menu item</comment>
        <translation>Тоқтау</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Reload</source>
        <comment>Reload context menu item</comment>
        <translation>Қайта жүктеу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cut</source>
        <comment>Cut context menu item</comment>
        <translation>Қиып алу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Paste</source>
        <comment>Paste context menu item</comment>
        <translation>Қою</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No Guesses Found</source>
        <comment>No Guesses Found context menu item</comment>
        <translation>Жорамал табылмады</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ignore</source>
        <comment>Ignore Spelling context menu item</comment>
        <translation>Елемеу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Add To Dictionary</source>
        <comment>Learn Spelling context menu item</comment>
        <translation>Сөздікке қосу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Search The Web</source>
        <comment>Search The Web context menu item</comment>
        <translation>Вебті іздеу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Look Up In Dictionary</source>
        <comment>Look Up in Dictionary context menu item</comment>
        <translation>Сөздікте іздеу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open Link</source>
        <comment>Open Link context menu item</comment>
        <translation>Сілтемені ашу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ignore</source>
        <comment>Ignore Grammar context menu item</comment>
        <translation>Елемеу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Spelling</source>
        <comment>Spelling and Grammar context sub-menu item</comment>
        <translation>Емле</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show Spelling and Grammar</source>
        <comment>menu item title</comment>
        <translation>Емлені және грамматиканы көрсету</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide Spelling and Grammar</source>
        <comment>menu item title</comment>
        <translation>Емле мен грамматиканы жасыру</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Check Spelling</source>
        <comment>Check spelling context menu item</comment>
        <translation>Емлені тексеру</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Check Spelling While Typing</source>
        <comment>Check spelling while typing context menu item</comment>
        <translation>Теру кезінде емлені тексеру</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Check Grammar With Spelling</source>
        <comment>Check grammar with spelling context menu item</comment>
        <translation>Грамматиканы емлемен тексеру</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Fonts</source>
        <comment>Font context sub-menu item</comment>
        <translation>Қаріптер</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Bold</source>
        <comment>Bold context menu item</comment>
        <translation>Қалың</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Italic</source>
        <comment>Italic context menu item</comment>
        <translation>Италич</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Underline</source>
        <comment>Underline context menu item</comment>
        <translation>Асты сызылсын</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Outline</source>
        <comment>Outline context menu item</comment>
        <translation>Контур</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Direction</source>
        <comment>Writing direction context sub-menu item</comment>
        <translation>Бағыттау</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Text Direction</source>
        <comment>Text direction context sub-menu item</comment>
        <translation>Мәтін бағыты</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Default</source>
        <comment>Default writing direction context menu item</comment>
        <translation>Әдетті</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>LTR</source>
        <comment>Left to Right context menu item</comment>
        <translation>LTR</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>RTL</source>
        <comment>Right to Left context menu item</comment>
        <translation>RTL</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Inspect</source>
        <comment>Inspect Element context menu item</comment>
        <translation>Қарап шығу</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No recent searches</source>
        <comment>Label for only item in menu that appears when clicking on the search field image, when no searches have been performed</comment>
        <translation>Соңғы іздеулер жоқ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Recent searches</source>
        <comment>label for first item in the menu that appears when clicking on the search field image, used as embedded menu title</comment>
        <translation>Соңғы іздеулер</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Clear recent searches</source>
        <comment>menu item in Recent Searches menu that empties menu&apos;s contents</comment>
        <translation>Соңғы ізденістерді тазалау</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>Unknown</source>
        <comment>Unknown filesize FTP directory listing item</comment>
        <translation>Беймәлім</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 (%2x%3 pixels)</source>
        <comment>Title string for images</comment>
        <translation>% 1 (% 2x% 3 пиксел)</translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebKit/qt/WebCoreSupport/InspectorClientQt.cpp" line="+185"/>
        <source>Web Inspector - %2</source>
        <translation>Веб-инспектор - % 2</translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebCore/platform/qt/ScrollbarQt.cpp" line="+58"/>
        <source>Scroll here</source>
        <translation>Осында айналдыру</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Left edge</source>
        <translation>Сол жақ шеті</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Top</source>
        <translation>Жоғарғы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right edge</source>
        <translation>Оң жақ шеті</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Bottom</source>
        <translation>Төменгі жағы</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Page left</source>
        <translation>Сол жақтағы бет</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page up</source>
        <translation>Бетті жоғарыға</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page right</source>
        <translation>Бет оң жақта</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page down</source>
        <translation>Бетті төмен қаратып</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Scroll left</source>
        <translation>Солға жылжыту</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll up</source>
        <translation>Жоғары айналдыру</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll right</source>
        <translation>Оңға жылжыту</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll down</source>
        <translation>Төмен айналдыру</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/3rdparty/webkit/WebCore/platform/qt/FileChooserQt.cpp" line="+45"/>
        <source>%n file(s)</source>
        <comment>number of chosen file</comment>
        <translation>
            <numerusform>% n файлы</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebKit/qt/Api/qwebpage.cpp" line="+1322"/>
        <source>JavaScript Alert - %1</source>
        <translation>JavaScript ескертуі - % 1</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>JavaScript Confirm - %1</source>
        <translation>JavaScript растауы - % 1</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>JavaScript Prompt - %1</source>
        <translation>JavaScript сұрауы - % 1</translation>
    </message>
    <message>
        <location line="+333"/>
        <source>Move the cursor to the next character</source>
        <translation>Курсорды келесі таңбаға жылжыту</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the previous character</source>
        <translation>Курсорды алдыңғы таңбаға жылжыту</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the next word</source>
        <translation>Курсорды келесі сөзге жылжыту</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the previous word</source>
        <translation>Курсорды алдыңғы сөзге жылжыту</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the next line</source>
        <translation>Курсорды келесі жолға жылжыту</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the previous line</source>
        <translation>Курсорды алдыңғы жолға жылжыту</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the start of the line</source>
        <translation>Курсорды жолдың басына жылжыту</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the end of the line</source>
        <translation>Курсорды жолдың соңына жылжыту</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the start of the block</source>
        <translation>Курсорды блоктың басына жылжыту</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the end of the block</source>
        <translation>Курсорды блоктың соңына жылжыту</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the start of the document</source>
        <translation>Курсорды құжаттың басына жылжыту</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the end of the document</source>
        <translation>Курсорды құжаттың соңына жылжыту</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select all</source>
        <translation>Барлығын таңдау</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the next character</source>
        <translation>Келесі таңбаға таңдау</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the previous character</source>
        <translation>Алдыңғы таңбаға таңдау</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the next word</source>
        <translation>Келесі сөзге таңдау</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the previous word</source>
        <translation>Алдыңғы сөзге таңдау</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the next line</source>
        <translation>Келесі жолға таңдау</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the previous line</source>
        <translation>Алдыңғы жолға таңдау</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the start of the line</source>
        <translation>Жолдың басына дейін таңдау</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the end of the line</source>
        <translation>Жолдың соңына дейін таңдау</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the start of the block</source>
        <translation>Блоктың басына таңдау</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the end of the block</source>
        <translation>Блоктың соңына дейін таңдау</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the start of the document</source>
        <translation>Құжаттың басына дейін таңдау</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the end of the document</source>
        <translation>Құжаттың соңына дейін таңдау</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete to the start of the word</source>
        <translation>Сөздің басына жою</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete to the end of the word</source>
        <translation>Сөздің соңына дейін жою</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Insert a new paragraph</source>
        <translation>Жаңа абзацты кірістіру</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Insert a new line</source>
        <translation>Жаңа жолды кірістіру</translation>
    </message>
</context>
<context>
    <name>QWhatsThisAction</name>
    <message>
        <location filename="../src/gui/kernel/qwhatsthis.cpp" line="+522"/>
        <source>What&apos;s This?</source>
        <translation>Бұл не?</translation>
    </message>
</context>
<context>
    <name>QWidget</name>
    <message>
        <location filename="../src/gui/kernel/qwidget.cpp" line="+5326"/>
        <source>*</source>
        <translation>*</translation>
    </message>
</context>
<context>
    <name>QWizard</name>
    <message>
        <location filename="../src/gui/dialogs/qwizard.cpp" line="+637"/>
        <source>Go Back</source>
        <translation>Қайтып бар</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Continue</source>
        <translation>Жалғастыру</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Commit</source>
        <translation>Жаңылыс</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Done</source>
        <translation>Орындалды</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="obsolete">退出</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Help</source>
        <translation>Анықтама</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>&lt; &amp;Back</source>
        <translation>&lt; артқа</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Finish</source>
        <translation>&gt; Аяқтау</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Help</source>
        <translation>Анықтама</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>&amp;Next</source>
        <translation>Келесі</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Next &gt;</source>
        <translation>Келесі &gt;</translation>
    </message>
</context>
<context>
    <name>QWorkspace</name>
    <message>
        <location filename="../src/gui/widgets/qworkspace.cpp" line="+1094"/>
        <source>&amp;Restore</source>
        <translation>Қалпына келтіру</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Move</source>
        <translation>Жылжыту</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>Өлшемі</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mi&amp;nimize</source>
        <translation>Ми-нимиз</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ma&amp;ximize</source>
        <translation>Ma&gt; ximize</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Close</source>
        <translation>Жабу@ action: inmenu</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Stay on &amp;Top</source>
        <translation>Жоғарыда қалу</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+1059"/>
        <source>Sh&amp;ade</source>
        <translation>Ш-аде</translation>
    </message>
    <message>
        <location line="-278"/>
        <location line="+60"/>
        <source>%1 - [%2]</source>
        <translation>%1 - [%2]</translation>
    </message>
    <message>
        <location line="-1837"/>
        <source>Minimize</source>
        <translation>Кішірейту</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Restore Down</source>
        <translation>Қалпына келтіру</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Close</source>
        <translation>Жабу</translation>
    </message>
    <message>
        <location line="+2053"/>
        <source>&amp;Unshade</source>
        <translation>&gt; &gt; &gt;</translation>
    </message>
</context>
<context>
    <name>QXml</name>
    <message>
        <location filename="../src/xml/sax/qxml.cpp" line="+58"/>
        <source>no error occurred</source>
        <translation>қате болған жоқ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error triggered by consumer</source>
        <translation>тұтынушы іске қосқан қате</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected end of file</source>
        <translation>күтпеген файл соңы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>more than one document type definition</source>
        <translation>бірнеше құжат түрінің анықтамасы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing element</source>
        <translation>қате элементті талдау кезінде пайда болды</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>tag mismatch</source>
        <translation>тег сәйкес келмеді</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing content</source>
        <translation>қатесі мазмұнды талдау кезінде пайда болды</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected character</source>
        <translation>күтпеген таңба</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid name for processing instruction</source>
        <translation>өңдеу жөніндегі нұсқаулықтың жарамсыз атауы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>version expected while reading the XML declaration</source>
        <translation>XML декларациясын оқып жатқанда күтілетін нұсқа</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>wrong value for standalone declaration</source>
        <translation>автономды декларация үшін дұрыс емес мән</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>encoding declaration or standalone declaration expected while reading the XML declaration</source>
        <translation>XML декларациясын оқу кезінде кодтау декларациясы немесе автономды декларация күтілуде</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>standalone declaration expected while reading the XML declaration</source>
        <translation>XML декларациясын оқып жатқанда автономды декларация күтілуде</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing document type definition</source>
        <translation>қате құжат түрінің анықтамасын талдау кезінде пайда болды</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>letter is expected</source>
        <translation>хат күтілуде</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing comment</source>
        <translation>қате түсініктемені талдау кезінде пайда болды</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing reference</source>
        <translation>сілтемені талдау кезінде қате пайда болды</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>internal general entity reference not allowed in DTD</source>
        <translation>ЖКД-да рұқсат етiлмеген жалпы субъектiнiң iшкi анықтамасы</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>external parsed general entity reference not allowed in attribute value</source>
        <translation>атрибуттық мәнде рұқсат етілмеген жалпы субъектінің сыртқы талдау сілтемесі</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>external parsed general entity reference not allowed in DTD</source>
        <translation>DTD-де рұқсат етілмеген жалпы субъектінің сыртқы талдау сілтемесі</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unparsed entity reference in wrong context</source>
        <translation>қате контексте беймәлім субъект сілтемесі</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>recursive entities</source>
        <translation>рекурсивтiк субъектiлер</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error in the text declaration of an external entity</source>
        <translation>сыртқы заңды тұлғаның мәтіндік декларациясындағы қате</translation>
    </message>
</context>
<context>
    <name>QXmlStream</name>
    <message>
        <location filename="../src/corelib/xml/qxmlstream.cpp" line="+592"/>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+1769"/>
        <source>Extra content at end of document.</source>
        <translation>Құжаттың соңындағы қосымша мазмұн.</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>Invalid entity value.</source>
        <translation>Заңды тұлғаның жарамсыз құны.</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Invalid XML character.</source>
        <translation>Жарамсыз XML таңбасы.</translation>
    </message>
    <message>
        <location line="+259"/>
        <source>Sequence &apos;]]&gt;&apos; not allowed in content.</source>
        <translation>&apos;]&gt;&quot; реті мазмұнда рұқсат етілмеген.</translation>
    </message>
    <message>
        <location line="+309"/>
        <source>Namespace prefix &apos;%1&apos; not declared</source>
        <translation>&apos;% 1&apos; деген атаулар кеңістігінің префиксі жарияланбады</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Attribute redefined.</source>
        <translation>Атрибут қайта анықталған.</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Unexpected character &apos;%1&apos; in public id literal.</source>
        <translation>Жалпыға қолжетімді идентификат литеральды &apos;% 1&apos; деген күтпеген таңба.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Invalid XML version string.</source>
        <translation>XML нұсқасының жолы жарамсыз.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unsupported XML version.</source>
        <translation>Қолдау көрсетілмейтін XML нұсқасы.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>%1 is an invalid encoding name.</source>
        <translation>% 1 - кодтаудың жарамсыз атауы.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Encoding %1 is unsupported</source>
        <translation>% 1 кодтау қолдау көрсетілмеді</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Standalone accepts only yes or no.</source>
        <translation>Автономды тек иә немесе жоқ деп қабылдайды.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid attribute in XML declaration.</source>
        <translation>XML декларациясындағы жарамсыз атрибут.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Premature end of document.</source>
        <translation>Құжаттың уақытынан бұрын аяқталуы.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid document.</source>
        <translation>Құжат жарамсыз.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Expected </source>
        <translation>Күтілуде </translation>
    </message>
    <message>
        <location line="+11"/>
        <source>, but got &apos;</source>
        <translation>, бірақ алдым &apos;</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Unexpected &apos;</source>
        <translation>Күтпеген жерден &apos;</translation>
    </message>
    <message>
        <location line="+210"/>
        <source>Expected character data.</source>
        <translation>Күтілетін таңба деректері.</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="-995"/>
        <source>Recursive entity detected.</source>
        <translation>Рекурсивті субъект анықталды.</translation>
    </message>
    <message>
        <location line="+516"/>
        <source>Start tag expected.</source>
        <translation>Тегті бастаңыз деп күтілуде.</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>XML declaration not at start of document.</source>
        <translation>XML декларациясы құжаттың басында емес.</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>NDATA in parameter entity declaration.</source>
        <translation>Параметрі бойынша декларациядағы НДАТА.</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>%1 is an invalid processing instruction name.</source>
        <translation>% 1 - өңдеу нұсқауының жарамсыз атауы.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Invalid processing instruction name.</source>
        <translation>Өңдеу жөніндегі нұсқаулықтың атауы жарамсыз.</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream.cpp" line="-521"/>
        <location line="+12"/>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+164"/>
        <location line="+53"/>
        <source>Illegal namespace declaration.</source>
        <translation>Заңсыз атау кеңістігін декларациялау.</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+15"/>
        <source>Invalid XML name.</source>
        <translation>XML атауы жарамсыз.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Opening and ending tag mismatch.</source>
        <translation>Тег сәйкес келмеуін ашу және аяқтау.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Reference to unparsed entity &apos;%1&apos;.</source>
        <translation>&apos;% 1&apos; деген беймәлім субъектіге сілтеме.</translation>
    </message>
    <message>
        <location line="-13"/>
        <location line="+61"/>
        <location line="+40"/>
        <source>Entity &apos;%1&apos; not declared.</source>
        <translation>&apos;% 1&apos; деген субъект жарияланбаған.</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Reference to external entity &apos;%1&apos; in attribute value.</source>
        <translation>Атрибут мәнінде &apos;% 1&apos; сыртқы субъектіге сілтеме.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Invalid character reference.</source>
        <translation>Таңба сілтемесі жарамсыз.</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream.cpp" line="-75"/>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="-823"/>
        <source>Encountered incorrectly encoded content.</source>
        <translation>Қате кодталған мазмұн кездескен.</translation>
    </message>
    <message>
        <location line="+274"/>
        <source>The standalone pseudo attribute must appear after the encoding.</source>
        <translation>Автономды жалған атрибут кодтаудан кейін пайда болуы тиіс.</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+562"/>
        <source>%1 is an invalid PUBLIC identifier.</source>
        <translation>% 1 - жарамсыз жалпыға қолжетімді идентификатор.</translation>
    </message>
</context>
<context>
    <name>QtXmlPatterns</name>
    <message>
        <location filename="../src/xmlpatterns/acceltree/qacceltreebuilder.cpp" line="+205"/>
        <source>An %1-attribute with value %2 has already been declared.</source>
        <translation>% 2 мәні бар% 1 атрибуты жарияланды.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>An %1-attribute must have a valid %2 as value, which %3 isn&apos;t.</source>
        <translation>% 1-атрибуттың% 2 деген мәні болуы керек, ол % 3 емес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/api/qiodevicedelegate.cpp" line="+84"/>
        <source>Network timeout.</source>
        <translation>Желі тайм-таймы.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/api/qxmlserializer.cpp" line="+320"/>
        <source>Element %1 can&apos;t be serialized because it appears outside the document element.</source>
        <translation>% 1 элементі құжат элементінен тыс пайда болғандықтан, оны сериялау мүмкін емес.</translation>
    </message>
    <message>
        <source>Attribute element %1 can&apos;t be serialized because it appears at the top level.</source>
        <translation type="obsolete">属性元素 %1 不能被串行化，因为它出现在最顶层。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qabstractdatetime.cpp" line="+80"/>
        <source>Year %1 is invalid because it begins with %2.</source>
        <translation>% 1 жылы жарамсыз, себебі ол% 2 дегеннен басталады.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Day %1 is outside the range %2..%3.</source>
        <translation>% 1 күні% 2.%3 ауқымынан тыс.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Month %1 is outside the range %2..%3.</source>
        <translation>% 1 айы% 2.% 3 ауқымынан тыс.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Overflow: Can&apos;t represent date %1.</source>
        <translation>Толып кету: % 1 күнін көрсету мүмкін емес.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Day %1 is invalid for month %2.</source>
        <translation>% 1 күні% 2 ай үшін жарамсыз.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Time 24:%1:%2.%3 is invalid. Hour is 24, but minutes, seconds, and milliseconds are not all 0; </source>
        <translation>24:% 1:% 2.% 3 уақыты жарамсыз. Сағат — 24, бірақ минут, секунд, ал миллисекунд барлық 0 емес; </translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Time %1:%2:%3.%4 is invalid.</source>
        <translation>% 1:% 2:% 3.% 4 уақыты жарамсыз.</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Overflow: Date can&apos;t be represented.</source>
        <translation>Толып кету: Күні көрсетілмейді.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qabstractduration.cpp" line="+99"/>
        <location line="+15"/>
        <source>At least one component must be present.</source>
        <translation>Кем дегенде бiр компонент болуы тиiс.</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>At least one time component must appear after the %1-delimiter.</source>
        <translation>% 1-делимитрден кейін кемінде бір рет компонент пайда болуы тиіс.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qabstractfloatmathematician.cpp" line="+64"/>
        <source>No operand in an integer division, %1, can be %2.</source>
        <translation>Бүтін санның бөлінуіндегі жұмыстың ешқайсысы, % 1, % 2 болуы мүмкін емес.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The first operand in an integer division, %1, cannot be infinity (%2).</source>
        <translation>Бүтін сан бөлінуіндегі бірінші операнд, % 1, шексіздік (% 2) бола алмайды.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The second operand in a division, %1, cannot be zero (%2).</source>
        <translation>Дивизиондағы екінші операнд, % 1, нөлге тең бола алмайды (% 2).</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qanyuri_p.h" line="+132"/>
        <source>%1 is not a valid value of type %2.</source>
        <translation>% 1 дегені% 2 түрінің дұрыс мәні емес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qatomiccasters_p.h" line="+223"/>
        <source>When casting to %1 from %2, the source value cannot be %3.</source>
        <translation>% 2 дегеннен% 1-ге құйғанда бастапқы мән% 3 бола алмайды.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qatomicmathematicians.cpp" line="+65"/>
        <source>Integer division (%1) by zero (%2) is undefined.</source>
        <translation>Бүтін санның бөлінуі (% 1) нөлге (% 2) анықталмаған.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Division (%1) by zero (%2) is undefined.</source>
        <translation>Бөліну (% 1) нөлге (% 2) анықталмаған.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Modulus division (%1) by zero (%2) is undefined.</source>
        <translation>Модульдің нөлге (% 2) бөлінуі анықталмаған.</translation>
    </message>
    <message>
        <location line="+122"/>
        <location line="+32"/>
        <source>Dividing a value of type %1 by %2 (not-a-number) is not allowed.</source>
        <translation>% 1 типінің мәнін% 2 (сан емес) деп бөлуге жол берілмейді.</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Dividing a value of type %1 by %2 or %3 (plus or minus zero) is not allowed.</source>
        <translation>% 1 типінің мәнін% 2 немесе% 3 (қосу немесе минус нөл) деп бөлуге жол берілмейді.</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Multiplication of a value of type %1 by %2 or %3 (plus or minus infinity) is not allowed.</source>
        <translation>% 1 түрінің мәнін% 2 немесе% 3 (қосу немесе минус шексіздік) көбейтуге жол берілмейді.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qatomicvalue.cpp" line="+79"/>
        <source>A value of type %1 cannot have an Effective Boolean Value.</source>
        <translation>% 1 типінің мәні тиімді буырқанған мәнге ие бола алмайды.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qboolean.cpp" line="+78"/>
        <source>Effective Boolean Value cannot be calculated for a sequence containing two or more atomic values.</source>
        <translation>Тиімді бoolean мәнін екі немесе одан да көп атомдық мәндерден тұратын тізбек үшін есептеу мүмкін емес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qderivedinteger_p.h" line="+402"/>
        <source>Value %1 of type %2 exceeds maximum (%3).</source>
        <translation>% 2 түрінің % 1 мәні ең жоғары мәннен (% 3) асып түседі.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Value %1 of type %2 is below minimum (%3).</source>
        <translation>% 2 түрінің % 1 мәні минимумнан төмен (% 3).</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qhexbinary.cpp" line="+91"/>
        <source>A value of type %1 must contain an even number of digits. The value %2 does not.</source>
        <translation>% 1 түрінің мәні санның жұп санын қамтуы тиіс. % 2 деген мән жоқ.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>%1 is not valid as a value of type %2.</source>
        <translation>% 1 дегені% 2 түрінің мәні ретінде жарамсыз.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qarithmeticexpression.cpp" line="+207"/>
        <source>Operator %1 cannot be used on type %2.</source>
        <translation>% 1 деген операторды% 2 типінде пайдалану мүмкін емес.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Operator %1 cannot be used on atomic values of type %2 and %3.</source>
        <translation>% 1 операторы% 2 және% 3 типіндегі атомдық мәндерде қолданыла алмайды.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qattributenamevalidator.cpp" line="+66"/>
        <source>The namespace URI in the name for a computed attribute cannot be %1.</source>
        <translation>Есептеу атрибутының атауындағы URI атау кеңістігі% 1 болуы мүмкін емес.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The name for a computed attribute cannot have the namespace URI %1 with the local name %2.</source>
        <translation>Есептеу атрибутының атауында% 2 жергілікті атауы бар URI% 1 атау кеңістігі болмайды.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcastas.cpp" line="+88"/>
        <source>Type error in cast, expected %1, received %2.</source>
        <translation>% 1, % 2 дегенді алған құймадағы қатені теріңіз.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>When casting to %1 or types derived from it, the source value must be of the same type, or it must be a string literal. Type %2 is not allowed.</source>
        <translation>% 1 немесе одан алынған түрлерге құйғанда бастапқы мән бір түрде болуы керек немесе ол жол литеральды болуы тиіс. % 2 дегенді теруге болмайды.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcastingplatform.cpp" line="+134"/>
        <source>No casting is possible with %1 as the target type.</source>
        <translation>Мақсатты түрі ретінде% 1 құйып алу мүмкін емес.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>It is not possible to cast from %1 to %2.</source>
        <translation>% 1- ден% 2-ге дейін құю мүмкін емес.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Casting to %1 is not possible because it is an abstract type, and can therefore never be instantiated.</source>
        <translation>% 1 дегенге құю мүмкін емес, себебі ол абстрактілі түр, сондықтан ешқашан лезде бола алмайды.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>It&apos;s not possible to cast the value %1 of type %2 to %3</source>
        <translation>% 1 дегеннің% 2 дегеннен % 3 дегенге дейінгі мәнін құю мүмкін емес</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Failure when casting from %1 to %2: %3</source>
        <translation>% 1-ден% 2-ге құйған кезде істен шығу:% 3</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcommentconstructor.cpp" line="+67"/>
        <source>A comment cannot contain %1</source>
        <translation>% 1 деген түсініктеме болмады</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>A comment cannot end with a %1.</source>
        <translation>Түсініктеме% 1 дегенмен аяқталмайды.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcomparisonplatform.cpp" line="+167"/>
        <source>No comparisons can be done involving the type %1.</source>
        <translation>% 1 деген түрімен салыстырулар жасалмайды.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Operator %1 is not available between atomic values of type %2 and %3.</source>
        <translation>% 1 операторы% 2 және% 3 типінің атомдық мәндері арасында қолжетімсіз.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qdocumentcontentvalidator.cpp" line="+86"/>
        <source>An attribute node cannot be a child of a document node. Therefore, the attribute %1 is out of place.</source>
        <translation>Атрибут түйіні құжат торабының баласы бола алмайды. Сондықтан% 1 атрибуты орнынан шығып тұр.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qexpressionfactory.cpp" line="+169"/>
        <source>A library module cannot be evaluated directly. It must be imported from a main module.</source>
        <translation>Кітапхана модулін тікелей бағалау мүмкін емес. Ол негізгі модульден әкелінуі тиіс.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>No template by name %1 exists.</source>
        <translation>% 1 атауы бойынша үлгі жоқ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qgenericpredicate.cpp" line="+106"/>
        <source>A value of type %1 cannot be a predicate. A predicate must have either a numeric type or an Effective Boolean Value type.</source>
        <translation>% 1 түрінің мәні баяндауыш бола алмайды. Баяндауыштың сандық түрі немесе тиімді буырқанған мән түрі болуы тиіс.</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>A positional predicate must evaluate to a single numeric value.</source>
        <translation>Позициялық баяндауыш бірыңғай сандық мәнге баға беруі тиіс.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qncnameconstructor_p.h" line="+113"/>
        <source>The target name in a processing instruction cannot be %1 in any combination of upper and lower case. Therefore, is %2 invalid.</source>
        <translation>Өңдеу нұсқауындағы мақсатты атау жоғарғы және төменгі корпустың кез келген үйлесімінде% 1 бола алмайды. Сондықтан% 2 жарамсыз.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>%1 is not a valid target name in a processing instruction. It must be a %2 value, e.g. %3.</source>
        <translation>% 1 - өңдеу нұсқаулығындағы дұрыс емес мақсатты атау. Ол% 2 мәні болуы керек, мысалы, % 3.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qpath.cpp" line="+109"/>
        <source>The last step in a path must contain either nodes or atomic values. It cannot be a mixture between the two.</source>
        <translation>Жолдағы соңғы қадамда не түйіндер, не атомдық шамалар болуы тиіс. Ол екеуінің арасында қоспа бола алмайды.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qprocessinginstructionconstructor.cpp" line="+84"/>
        <source>The data of a processing instruction cannot contain the string %1</source>
        <translation>Өңдеу нұсқаулығының деректері% 1 жолын қамтымады</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qqnameconstructor.cpp" line="+82"/>
        <source>No namespace binding exists for the prefix %1</source>
        <translation>% 1 префиксі үшін атау кеңістігінің байланыстыруы жоқ</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qqnameconstructor_p.h" line="+156"/>
        <source>No namespace binding exists for the prefix %1 in %2</source>
        <translation>% 1 дегеннің% 2 деген префиксі үшін атау кеңістігінің байланыстыруы жоқ</translation>
    </message>
    <message>
        <location line="+12"/>
        <location filename="../src/xmlpatterns/functions/qqnamefns.cpp" line="+69"/>
        <source>%1 is an invalid %2</source>
        <translation>% 1 - жарамсыз% 2</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/xmlpatterns/functions/qabstractfunctionfactory.cpp" line="+77"/>
        <source>%1 takes at most %n argument(s). %2 is therefore invalid.</source>
        <translation>
            <numerusform>% 1 деген аргументтің % 1 дегенін алады. % 2 деген дұрыс емес.</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%1 requires at least %n argument(s). %2 is therefore invalid.</source>
        <translation>
            <numerusform>% 1 дегенде% n аргументі қажет. % 2 деген дұрыс емес.</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qaggregatefns.cpp" line="+120"/>
        <source>The first argument to %1 cannot be of type %2. It must be a numeric type, xs:yearMonthDuration or xs:dayTimeDuration.</source>
        <translation>% 1 дегеннің бірінші аргументі% 2 типі бола алмайды. Ол сандық түрі, xs:yearMonthDuration немесе xs:dayTimeDuration болуы тиіс.</translation>
    </message>
    <message>
        <location line="+74"/>
        <source>The first argument to %1 cannot be of type %2. It must be of type %3, %4, or %5.</source>
        <translation>% 1 дегеннің бірінші аргументі% 2 типі бола алмайды. Ол% 3, % 4 немесе% 5 түрі болуы керек.</translation>
    </message>
    <message>
        <location line="+91"/>
        <source>The second argument to %1 cannot be of type %2. It must be of type %3, %4, or %5.</source>
        <translation>% 1 дегеннің екінші аргументі% 2 түрі болуы мүмкін емес. Ол% 3, % 4 немесе% 5 түрі болуы керек.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qassemblestringfns.cpp" line="+88"/>
        <source>%1 is not a valid XML 1.0 character.</source>
        <translation>% 1 деген дұрыс XML 1.0 таңбасы емес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qcomparingaggregator.cpp" line="+197"/>
        <source>The first argument to %1 cannot be of type %2.</source>
        <translation>% 1 дегеннің бірінші аргументі% 2 типі бола алмайды.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qdatetimefn.cpp" line="+86"/>
        <source>If both values have zone offsets, they must have the same zone offset. %1 and %2 are not the same.</source>
        <translation>Егер екі мәндерде де аймақ офсеттері болса, онда оларда бірдей аймақ офсеті болуы тиіс. % 1 және% 2 дегендер бірдей емес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qerrorfn.cpp" line="+61"/>
        <source>%1 was called.</source>
        <translation>% 1 деп аталды.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qpatternmatchingfns.cpp" line="+94"/>
        <source>%1 must be followed by %2 or %3, not at the end of the replacement string.</source>
        <translation>% 1 дегенді ауыстыру жолының соңында емес, % 2 немесе% 3 дегенді ұстанған жөн.</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>In the replacement string, %1 must be followed by at least one digit when not escaped.</source>
        <translation>% 1 дегенді ауыстыру жолында қашып кетпеген кезде кем дегенде бір санды ұстанған жөн.</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>In the replacement string, %1 can only be used to escape itself or %2, not %3</source>
        <translation>Ауыстырылатын жолда% 1 дегенді тек қана қашу үшін пайдалануға болады немесе% 2, % 3 емес</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qpatternplatform.cpp" line="+92"/>
        <source>%1 matches newline characters</source>
        <translation>% 1 жаңа сызық таңбаларымен сәйкес келеді</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1 and %2 match the start and end of a line.</source>
        <translation>% 1 және% 2 деген жолдың басы мен соңына сәйкес.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Matches are case insensitive</source>
        <translation>Сіріңкелер кейс сезімтал емес</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Whitespace characters are removed, except when they appear in character classes</source>
        <translation>Whitespace таңбалары таңбалар кластарында пайда болған жағдайларды қоспағанда, жойылады</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>%1 is an invalid regular expression pattern: %2</source>
        <translation>% 1 - дұрыс емес тұрақты өрнек үлгісі:% 2</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>%1 is an invalid flag for regular expressions. Valid flags are:</source>
        <translation>% 1 - тұрақты өрнектер үшін жарамсыз жалауша. Жарамды жалаушалар:</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qqnamefns.cpp" line="+17"/>
        <source>If the first argument is the empty sequence or a zero-length string (no namespace), a prefix cannot be specified. Prefix %1 was specified.</source>
        <translation>Егер бірінші аргумент бос тізбек немесе нөлдік ұзындықтағы жол (атау кеңістігі жоқ) болса, префиксті көрсету мүмкін емес. % 1 префиксі көрсетілді.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qsequencefns.cpp" line="+347"/>
        <source>It will not be possible to retrieve %1.</source>
        <translation>% 1 дегенді алу мүмкін емес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qcontextnodechecker.cpp" line="+54"/>
        <source>The root node of the second argument to function %1 must be a document node. %2 is not a document node.</source>
        <translation>% 1 функциясына арналған екінші аргументтің түбірлік түйіні құжат түйіні болуы тиіс. % 2 деген құжаттың түйіні емес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qsequencegeneratingfns.cpp" line="+279"/>
        <source>The default collection is undefined</source>
        <translation>Әдепкі жиын анықталмаған</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>%1 cannot be retrieved</source>
        <translation>% 1 дегенді алу мүмкін емес</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qstringvaluefns.cpp" line="+252"/>
        <source>The normalization form %1 is unsupported. The supported forms are %2, %3, %4, and %5, and none, i.e. the empty string (no normalization).</source>
        <translation>% 1 қалыпқа келтіру пішіні қолдау көрсетілмеді. Қолдау көрсетілетін пішіндер% 2, % 3, % 4 және% 5, және жоқ, яғни бос жол (қалыпқа келтіру жоқ).</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qtimezonefns.cpp" line="+87"/>
        <source>A zone offset must be in the range %1..%2 inclusive. %3 is out of range.</source>
        <translation>Аймақтың офсеті% 1.% 2 ауқымында болуы керек. % 3 деген ауқымнан тыс.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1 is not a whole number of minutes.</source>
        <translation>% 1 деген бүтін минут саны емес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/janitors/qcardinalityverifier.cpp" line="+58"/>
        <source>Required cardinality is %1; got cardinality %2.</source>
        <translation>Қажет кардинализм % 1; % 2 cardinality дегенді алды.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/janitors/qitemverifier.cpp" line="+67"/>
        <source>The item %1 did not match the required type %2.</source>
        <translation>% 1 деген элемент қажетті% 2 түріне сәйкес келмеді.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qquerytransformparser.cpp" line="+379"/>
        <location line="+7253"/>
        <source>%1 is an unknown schema type.</source>
        <translation>% 1 деген белгісіз схема түрі.</translation>
    </message>
    <message>
        <location line="-6971"/>
        <source>Only one %1 declaration can occur in the query prolog.</source>
        <translation>Сұрау прологында тек бір% 1 декларациясы ғана орын алуы мүмкін.</translation>
    </message>
    <message>
        <location line="+188"/>
        <source>The initialization of variable %1 depends on itself</source>
        <translation>% 1 айнымалысының инициализациясы өзіне байланысты</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>No variable by name %1 exists</source>
        <translation>% 1 атауы бойынша айнымалы жоқ</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qparsercontext.cpp" line="+93"/>
        <source>The variable %1 is unused</source>
        <translation>% 1 айнымалысы пайдаланылмаған</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qquerytransformparser.cpp" line="+2841"/>
        <source>Version %1 is not supported. The supported XQuery version is 1.0.</source>
        <translation>% 1 нұсқасына қолдау көрсетілмеген. Қолдау көрсетілетін XQuery нұсқасы — 1.0.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>The encoding %1 is invalid. It must contain Latin characters only, must not contain whitespace, and must match the regular expression %2.</source>
        <translation>% 1 кодтауы жарамсыз. Онда тек латын таңбалары болуы керек, ақ кеңістік болмауы керек және% 2 деген тұрақты өрнекке сәйкес келуі керек.</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>No function with signature %1 is available</source>
        <translation>% 1 қолтаңбасы бар функция жоқ</translation>
    </message>
    <message>
        <location line="+72"/>
        <location line="+10"/>
        <source>A default namespace declaration must occur before function, variable, and option declarations.</source>
        <translation>Әдепкі атаулар кеңістігінің декларациясы функция, айнымалы және опция декларациялары алдында орын алуы тиіс.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Namespace declarations must occur before function, variable, and option declarations.</source>
        <translation>Атаулар кеңістігінің декларациялары функция, айнымалы және опция декларациялары алдында болуы тиіс.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Module imports must occur before function, variable, and option declarations.</source>
        <translation>Модуль импорты функция, айнымалы және опциондық декларациялар алдында орын алуы тиіс.</translation>
    </message>
    <message>
        <location line="+200"/>
        <source>It is not possible to redeclare prefix %1.</source>
        <translation>% 1 префиксын қайта анықтау мүмкін емес.</translation>
    </message>
    <message>
        <source>Only the prefix %1 can be declared to bind the namespace %2. By default, it is already bound to the prefix %1.</source>
        <translation type="obsolete">至于前缀 %1 可以被声明为和命名空间 %2 绑定。默认情况下，它已经被绑定到前缀 %1。</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Prefix %1 is already declared in the prolog.</source>
        <translation>% 1 префиксі прологта жарияланды ғой.</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>The name of an option must have a prefix. There is no default namespace for options.</source>
        <translation>Опцион атауының префиксі болуы тиіс. Параметрлер үшін әдепкі атаулар кеңістігі жоқ.</translation>
    </message>
    <message>
        <location line="+171"/>
        <source>The Schema Import feature is not supported, and therefore %1 declarations cannot occur.</source>
        <translation>Schema импорттау мүмкіндігіне қолдау көрсетілмеген, сондықтан% 1 декларациялары орын алуы мүмкін емес.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The target namespace of a %1 cannot be empty.</source>
        <translation>% 1 мақсатты атау кеңістігі бос болмайды.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The module import feature is not supported</source>
        <translation>Модульді импорттау мүмкіндігіне қолдау көрсетілмеген</translation>
    </message>
    <message>
        <source>A variable by name %1 has already been declared in the prolog.</source>
        <translation type="obsolete">名称为 %1 的变量已经在序言中声明过了。</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>No value is available for the external variable by name %1.</source>
        <translation>% 1 атауы бойынша сыртқы айнымалы үшін ешқандай мән қол жетімді емес.</translation>
    </message>
    <message>
        <source>The namespace for a user defined function cannot be empty(try the predefined prefix %1 which exists for cases like this)</source>
        <translation type="obsolete">用户定义的函数的命名空间不能为空(请试试预定义的前缀 %1，它就是用于这种情况的)。</translation>
    </message>
    <message>
        <location line="-4154"/>
        <source>A construct was encountered which only is allowed in XQuery.</source>
        <translation>Тек XQuery-де ғана рұқсат етілген құрылыс кездесті.</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>A template by name %1 has already been declared.</source>
        <translation>% 1 атауы бойынша үлгі жарияланды.</translation>
    </message>
    <message>
        <location line="+3581"/>
        <source>The keyword %1 cannot occur with any other mode name.</source>
        <translation>% 1 кілт сөзі басқа режим атауымен орын алуы мүмкін емес.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>The value of attribute %1 must of type %2, which %3 isn&apos;t.</source>
        <translation>% 1 атрибутының% 2 деген түрі болуы керек, ол% 3 емес.</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>The prefix %1 can not be bound. By default, it is already bound to the namespace %2.</source>
        <translation>% 1 префиксі байланыстырыла алмайды. Әдепкі бойынша, ол қазірдің өзінде% 2 атау кеңістігіне байланысты.</translation>
    </message>
    <message>
        <location line="+312"/>
        <source>A variable by name %1 has already been declared.</source>
        <translation>% 1 атауы бойынша айнымалы жарияланды.</translation>
    </message>
    <message>
        <location line="+135"/>
        <source>A stylesheet function must have a prefixed name.</source>
        <translation>Мәнерлер парағы функциясының алдын ала жазылған атауы болуы тиіс.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The namespace for a user defined function cannot be empty (try the predefined prefix %1 which exists for cases like this)</source>
        <translation>Пайдаланушы анықтаған функцияға арналған атаулар кеңістігі бос бола алмайды (осы сияқты жағдайлар үшін бар алдын ала анықталған% 1 префиксін байқап көріңіз)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The namespace %1 is reserved; therefore user defined functions may not use it. Try the predefined prefix %2, which exists for these cases.</source>
        <translation>% 1 атау кеңістігі сақталған; сондықтан пайдаланушы анықтаған функциялар оны пайдаланбауы мүмкін. Осы жағдайлар үшін бар алдын ала анықталған% 2 префиксті байқап көріңіз.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>The namespace of a user defined function in a library module must be equivalent to the module namespace. In other words, it should be %1 instead of %2</source>
        <translation>Кітапхана модуліндегі пайдаланушы анықтаған функцияның атау кеңістігі модульдің атау кеңістігіне баламалы болуы тиіс. Басқаша айтқанда, ол% 2 орнына% 1 болуы керек</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>A function already exists with the signature %1.</source>
        <translation>% 1 деген қолтаңбасы бар функция бар ғой.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>No external functions are supported. All supported functions can be used directly, without first declaring them as external</source>
        <translation>Сыртқы функциялар қолдау көрсетілмеген. Барлық қолдау көрсетілген функцияларды тікелей, оларды сыртқы деп жарияламай-ақ пайдалануға болады</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>An argument by name %1 has already been declared. Every argument name must be unique.</source>
        <translation>% 1 атауымен аргумент жарияланды. Әр аргумент атауы бірегей болуы керек.</translation>
    </message>
    <message>
        <location line="+179"/>
        <source>When function %1 is used for matching inside a pattern, the argument must be a variable reference or a string literal.</source>
        <translation>% 1 функциясы үлгі ішіндегі сәйкестік үшін пайдаланылса, аргумент айнымалы сілтеме немесе жол литеральды болуы тиіс.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>In an XSL-T pattern, the first argument to function %1 must be a string literal, when used for matching.</source>
        <translation>XSL-T үлгісінде% 1 функциясының бірінші аргументі сәйкестік үшін пайдаланылған кезде жол литеральды болуы керек.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>In an XSL-T pattern, the first argument to function %1 must be a literal or a variable reference, when used for matching.</source>
        <translation>XSL-T үлгісінде% 1 функциясының бірінші аргументі сәйкестік үшін пайдаланылған кезде литеральды немесе айнымалы сілтеме болуы тиіс.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>In an XSL-T pattern, function %1 cannot have a third argument.</source>
        <translation>XSL-T үлгісінде% 1 функциясының үшінші аргументі болмайды.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>In an XSL-T pattern, only function %1 and %2, not %3, can be used for matching.</source>
        <translation>XSL-T үлгісінде тек% 1 және % 2 функциясын, % 3 емес, сәйкестік үшін пайдалануға болады.</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>In an XSL-T pattern, axis %1 cannot be used, only axis %2 or %3 can.</source>
        <translation>XSL-T үлгісінде% 1 осі пайдаланылмайды, тек% 2 немесе% 3 осі ғана мүмкін.</translation>
    </message>
    <message>
        <location line="+126"/>
        <source>%1 is an invalid template mode name.</source>
        <translation>% 1 - үлгінің жарамсыз режім атауы.</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>The name of a variable bound in a for-expression must be different from the positional variable. Hence, the two variables named %1 collide.</source>
        <translation>Өрнек үшін байланысты айнымалының атауы позициялық айнымалыдан өзгеше болуы тиіс. Осыдан келіп, % 1 деп аталатын екі айнымалы соқтығысады.</translation>
    </message>
    <message>
        <location line="+758"/>
        <source>The Schema Validation Feature is not supported. Hence, %1-expressions may not be used.</source>
        <translation>Схеманы тексеру мүмкіндігіне қолдау көрсетілмеген. Осыдан келіп, % 1-өрнектер пайдаланылмауы мүмкін.</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>None of the pragma expressions are supported. Therefore, a fallback expression must be present</source>
        <translation>Прагма өрнектерінің ешқайсысы қолдалмайды. Сондықтан кері шегініс өрнегі болуы керек</translation>
    </message>
    <message>
        <location line="+267"/>
        <source>Each name of a template parameter must be unique; %1 is duplicated.</source>
        <translation>Үлгі параметрінің әрбір атауы бірегей болуы керек; % 1 деген қайталанған.</translation>
    </message>
    <message>
        <location line="+129"/>
        <source>The %1-axis is unsupported in XQuery</source>
        <translation>% 1-ось XQuery бағдарламасында қолдау көрсетілмеді</translation>
    </message>
    <message>
        <location line="+1150"/>
        <source>%1 is not a valid name for a processing-instruction.</source>
        <translation>% 1 - өңдеу-нұсқаудың дұрыс емес атауы.</translation>
    </message>
    <message>
        <location line="-7029"/>
        <source>%1 is not a valid numeric literal.</source>
        <translation>% 1 деген дұрыс сан емес.</translation>
    </message>
    <message>
        <location line="+6165"/>
        <source>No function by name %1 is available.</source>
        <translation>% 1 атауы бойынша функция жоқ.</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>The namespace URI cannot be the empty string when binding to a prefix, %1.</source>
        <translation>URI атау кеңістігі префикспен байланыстыру кезінде бос жол бола алмайды, % 1.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>%1 is an invalid namespace URI.</source>
        <translation>% 1 - жарамсыз атау кеңістігі URI.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>It is not possible to bind to the prefix %1</source>
        <translation>% 1 префиксіне байланыстыру мүмкін емес</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Namespace %1 can only be bound to %2 (and it is, in either case, pre-declared).</source>
        <translation>% 1 атау кеңістігін тек% 2 деп байланыстыруға болады (және ол, кез келген жағдайда, алдын ала жарияланған).</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Prefix %1 can only be bound to %2 (and it is, in either case, pre-declared).</source>
        <translation>% 1 префиксі тек% 2 дегенмен байланыстырылуы мүмкін (және ол, кез келген жағдайда, алдын ала жарияланған).</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Two namespace declaration attributes have the same name: %1.</source>
        <translation>Екі атау кеңістігін декларациялау атрибуттарының атауы бірдей:% 1.</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>The namespace URI must be a constant and cannot use enclosed expressions.</source>
        <translation>URI атау кеңістігі тұрақты болуы тиіс және жабық өрнектерді пайдалана алмайды.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>An attribute by name %1 has already appeared on this element.</source>
        <translation>% 1 деген атрибут осы элементте пайда болды.</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>A direct element constructor is not well-formed. %1 is ended with %2.</source>
        <translation>Тікелей элементті құрастырушы жақсы қалыптаспаған. % 1 деген% 2 дегенмен аяқталды.</translation>
    </message>
    <message>
        <location line="+458"/>
        <source>The name %1 does not refer to any schema type.</source>
        <translation>% 1 атауы ешбір схема түріне сілтеме жасамайды.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 is an complex type. Casting to complex types is not possible. However, casting to atomic types such as %2 works.</source>
        <translation>% 1 - күрделі түрі. Күрделі типтерге құю мүмкін емес. Дегенмен, % 2 сияқты атом түрлеріне құю жұмыс істейді.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1 is not an atomic type. Casting is only possible to atomic types.</source>
        <translation>% 1 деген атом түрі емес. Құйма тек атом түрлеріне ғана мүмкін.</translation>
    </message>
    <message>
        <source>%1 is not a valid name for a processing-instruction. Therefore this name test will never match.</source>
        <translation type="obsolete">%1 不是处理指令的有效名称。因此这个名称测试永远不会匹配。</translation>
    </message>
    <message>
        <location line="+145"/>
        <location line="+71"/>
        <source>%1 is not in the in-scope attribute declarations. Note that the schema import feature is not supported.</source>
        <translation>% 1 ауқымдағы атрибут декларацияларында жоқ. Схема импорттау мүмкіндігіне қолдау көрсетілмегенін ескеріңіз.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The name of an extension expression must be in a namespace.</source>
        <translation>Кеңейтім өрнегінің атауы атау кеңістігінде болуы керек.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/type/qcardinality.cpp" line="+55"/>
        <source>empty</source>
        <translation>бос</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>zero or one</source>
        <translation>нөл немесе бір</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>exactly one</source>
        <translation>дәл бір</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>one or more</source>
        <translation>бір немесе бірнеше</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>zero or more</source>
        <translation>нөл немесе одан көп</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/type/qtypechecker.cpp" line="+63"/>
        <source>Required type is %1, but %2 was found.</source>
        <translation>Қажет түрі% 1, бірақ% 2 дегені табылды.</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Promoting %1 to %2 may cause loss of precision.</source>
        <translation>% 1- ден% 2-ге дейін жылжыту дәлдікті жоғалтуы мүмкін.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>The focus is undefined.</source>
        <translation>Назар анықталмаған.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/utils/qoutputvalidator.cpp" line="+86"/>
        <source>It&apos;s not possible to add attributes after any other kind of node.</source>
        <translation>Қандай да бір басқа түйіннен кейін атрибуттарды қосу мүмкін емес.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>An attribute by name %1 has already been created.</source>
        <translation>% 1 атауы бойынша атрибут жасалған.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/utils/qxpathhelper_p.h" line="+120"/>
        <source>Only the Unicode Codepoint Collation is supported(%1). %2 is unsupported.</source>
        <translation>Тек Юникод кодтық нүктенің коллажына қолдау көрсетіледі(% 1). % 2 деген қолдау көрсетілмеді.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/api/qxmlserializer.cpp" line="+60"/>
        <source>Attribute %1 can&apos;t be serialized because it appears at the top level.</source>
        <translation>% 1 атрибуты жоғарғы деңгейде пайда болғандықтан, оны сериялау мүмкін емес.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/acceltree/qacceltreeresourceloader.cpp" line="+314"/>
        <source>%1 is an unsupported encoding.</source>
        <translation>% 1 - қолдау көрсетілмейтін кодтау.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>%1 contains octets which are disallowed in the requested encoding %2.</source>
        <translation>% 1 дегенде% 2 дегенді кодтауға жарамсыз октеттер бар.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The codepoint %1, occurring in %2 using encoding %3, is an invalid XML character.</source>
        <translation>% 2- де% 3 кодтауды пайдаланып пайда болған% 1 кодтық нүктесі жарамсыз XML таңбасы болып табылады.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qapplytemplate.cpp" line="+119"/>
        <source>Ambiguous rule match.</source>
        <translation>Екі ұшты ереже сәйкестігі.</translation>
    </message>
    <message>
        <source>In a namespace constructor, the value for a namespace value cannot be an empty string.</source>
        <translation type="obsolete">在一个命名空间构造中，命名空间的值不能为空字符串。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcomputednamespaceconstructor.cpp" line="+69"/>
        <source>In a namespace constructor, the value for a namespace cannot be an empty string.</source>
        <translation>Атау кеңістігінің конструкторында атаулар кеңістігінің мәні бос жол бола алмайды.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>The prefix must be a valid %1, which %2 is not.</source>
        <translation>Префикс% 1 болуы керек, ол% 2 емес.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>The prefix %1 cannot be bound.</source>
        <translation>% 1 префиксі байланыстырыла алмайды.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Only the prefix %1 can be bound to %2 and vice versa.</source>
        <translation>% 1 префиксі ғана% 2 және керісінше байланыстырылуы мүмкін.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qevaluationcache.cpp" line="+117"/>
        <source>Circularity detected</source>
        <translation>Циркулярлық анықталды</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qtemplate.cpp" line="+145"/>
        <source>The parameter %1 is required, but no corresponding %2 is supplied.</source>
        <translation>% 1 параметрі қажет, бірақ% 2 дегенге сәйкес келмейді.</translation>
    </message>
    <message>
        <location line="-71"/>
        <source>The parameter %1 is passed, but no corresponding %2 exists.</source>
        <translation>% 1 параметрі өтті, бірақ% 2 дегенге сәйкес келмейді.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qunparsedtextfn.cpp" line="+65"/>
        <source>The URI cannot have a fragment</source>
        <translation>URI фрагменті бола алмайды</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qxslttokenizer.cpp" line="+519"/>
        <source>Element %1 is not allowed at this location.</source>
        <translation>% 1 элементі бұл орында рұқсат етілмеген.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Text nodes are not allowed at this location.</source>
        <translation>Бұл жерде мәтіндік тораптарды пайдалануға жол берілмейді.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Parse error: %1</source>
        <translation>Талдау қатесі:% 1</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>The value of the XSL-T version attribute must be a value of type %1, which %2 isn&apos;t.</source>
        <translation>XSL-T нұсқасы атрибутының мәні% 1 түрінің мәні болуы керек, ол % 2 емес.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Running an XSL-T 1.0 stylesheet with a 2.0 processor.</source>
        <translation>XSL-T 1.0 мәнер парағын 2.0 процессорымен іске қосу.</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Unknown XSL-T attribute %1.</source>
        <translation>% 1 белгісіз XSL-T атрибуты.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Attribute %1 and %2 are mutually exclusive.</source>
        <translation>% 1 және% 2 атрибуты өзара ерекше.</translation>
    </message>
    <message>
        <location line="+166"/>
        <source>In a simplified stylesheet module, attribute %1 must be present.</source>
        <translation>Жеңілдетілген мәнерлер парағы модулінде% 1 атрибуты болуы керек.</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>If element %1 has no attribute %2, it cannot have attribute %3 or %4.</source>
        <translation>Егер% 1 элементінде% 2 атрибуты болмаса, онда оның% 3 немесе% 4 атрибуты болмайды.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Element %1 must have at least one of the attributes %2 or %3.</source>
        <translation>% 1 элементінде% 2 немесе% 3 атрибуттарының біреуі болуы керек.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>At least one mode must be specified in the %1-attribute on element %2.</source>
        <translation>% 1-атрибутында% 2 элементінде кем дегенде бір режим көрсетілуге тиіс.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qmaintainingreader.cpp" line="+183"/>
        <source>Attribute %1 cannot appear on the element %2. Only the standard attributes can appear.</source>
        <translation>% 1 атрибуты% 2 элементінде көрінбейді. Тек стандартты атрибуттар ғана пайда болуы мүмкін.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Attribute %1 cannot appear on the element %2. Only %3 is allowed, and the standard attributes.</source>
        <translation>% 1 атрибуты% 2 элементінде көрінбейді. Тек% 3 рұқсат етіледі және стандартты атрибуттар.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Attribute %1 cannot appear on the element %2. Allowed is %3, %4, and the standard attributes.</source>
        <translation>% 1 атрибуты% 2 элементінде көрінбейді. % 3, % 4 және стандартты атрибуттар рұқсат етілген.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Attribute %1 cannot appear on the element %2. Allowed is %3, and the standard attributes.</source>
        <translation>% 1 атрибуты% 2 элементінде көрінбейді. Рұқсат етілгені% 3 және стандартты атрибуттар.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>XSL-T attributes on XSL-T elements must be in the null namespace, not in the XSL-T namespace which %1 is.</source>
        <translation>XSL-T элементтеріндегі XSL-T атрибуттары% 1 деген XSL-T атау кеңістігінде емес, нөлдік атау кеңістігінде болуы тиіс.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>The attribute %1 must appear on element %2.</source>
        <translation>% 1 атрибуты% 2 элементінде пайда болуы керек.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The element with local name %1 does not exist in XSL-T.</source>
        <translation>% 1 жергілікті атауы бар элемент XSL-T бағдарламасында жоқ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qxslttokenizer.cpp" line="+123"/>
        <source>Element %1 must come last.</source>
        <translation>% 1 элементі соңғы болып келуі керек.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>At least one %1-element must occur before %2.</source>
        <translation>% 2 дегенге дейін кемінде бір% 1 элемент пайда болуы керек.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Only one %1-element can appear.</source>
        <translation>Тек бір% 1 элементі ғана пайда болуы мүмкін.</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>At least one %1-element must occur inside %2.</source>
        <translation>% 2 дегенде бір% 1 элементі болуы керек.</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>When attribute %1 is present on %2, a sequence constructor cannot be used.</source>
        <translation>% 1 атрибуты% 2 дегенде болса, тізбекті құрастырғышты пайдалану мүмкін емес.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Element %1 must have either a %2-attribute or a sequence constructor.</source>
        <translation>% 1 элементінде% 2 атрибуты немесе тізбекті конструктор болуы керек.</translation>
    </message>
    <message>
        <location line="+125"/>
        <source>When a parameter is required, a default value cannot be supplied through a %1-attribute or a sequence constructor.</source>
        <translation>Параметр қажет болғанда, әдепкі мән% 1-атрибуты немесе тізбекті құрастырғыш арқылы берілуі мүмкін емес.</translation>
    </message>
    <message>
        <location line="+270"/>
        <source>Element %1 cannot have children.</source>
        <translation>% 1 элементінің балалары болмайды.</translation>
    </message>
    <message>
        <location line="+434"/>
        <source>Element %1 cannot have a sequence constructor.</source>
        <translation>% 1 элементінде тізбекті конструктор болмайды.</translation>
    </message>
    <message>
        <location line="+86"/>
        <location line="+9"/>
        <source>The attribute %1 cannot appear on %2, when it is a child of %3.</source>
        <translation>% 1 атрибуты% 2 дегенде, ол % 3 бала болғанда пайда бола алмайды.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>A parameter in a function cannot be declared to be a tunnel.</source>
        <translation>Функциядағы параметрді туннель деп жариялауға болмайды.</translation>
    </message>
    <message>
        <location line="+149"/>
        <source>This processor is not Schema-aware and therefore %1 cannot be used.</source>
        <translation>Бұл процессор Schema-хабардар емес, сондықтан% 1 қолданыла алмайды.</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Top level stylesheet elements must be in a non-null namespace, which %1 isn&apos;t.</source>
        <translation>Жоғарғы деңгейлі мәнерлер парағының элементтері нөлдік емес атаулар кеңістігінде болуы керек, ол% 1 емес.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The value for attribute %1 on element %2 must either be %3 or %4, not %5.</source>
        <translation>% 2 элементіндегі% 1 атрибутының мәні% 5 емес, % 3 немесе% 4 болуы тиіс.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Attribute %1 cannot have the value %2.</source>
        <translation>% 1 атрибуты% 2 деген мәнге ие болмады.</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>The attribute %1 can only appear on the first %2 element.</source>
        <translation>% 1 атрибуты тек бірінші% 2 элементінде пайда болуы мүмкін.</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>At least one %1 element must appear as child of %2.</source>
        <translation>Кем дегенде бір% 1 элементі% 2 бала ретінде пайда болуы керек.</translation>
    </message>
</context>
<context>
    <name>VolumeSlider</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/volumeslider.cpp" line="+67"/>
        <source>Muted</source>
        <translation>Өшірілген</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+15"/>
        <source>Volume: %1%</source>
        <translation>Көлемі:% 1%</translation>
    </message>
</context>
<context>
    <name>WebCore::PlatformScrollbar</name>
    <message>
        <source>Scroll here</source>
        <translation type="obsolete">滚动到这里</translation>
    </message>
    <message>
        <source>Left edge</source>
        <translation type="obsolete">左边缘</translation>
    </message>
    <message>
        <source>Top</source>
        <translation type="obsolete">顶部</translation>
    </message>
    <message>
        <source>Right edge</source>
        <translation type="obsolete">右边缘</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation type="obsolete">底部</translation>
    </message>
    <message>
        <source>Page left</source>
        <translation type="obsolete">左一页</translation>
    </message>
    <message>
        <source>Page up</source>
        <translation type="obsolete">上一页</translation>
    </message>
    <message>
        <source>Page right</source>
        <translation type="obsolete">右一页</translation>
    </message>
    <message>
        <source>Page down</source>
        <translation type="obsolete">下一页</translation>
    </message>
    <message>
        <source>Scroll left</source>
        <translation type="obsolete">向左滚动</translation>
    </message>
    <message>
        <source>Scroll up</source>
        <translation type="obsolete">向上滚动</translation>
    </message>
    <message>
        <source>Scroll right</source>
        <translation type="obsolete">向右滚动</translation>
    </message>
    <message>
        <source>Scroll down</source>
        <translation type="obsolete">向下滚动</translation>
    </message>
</context>
<context>
    <name>QPlatformTheme</name>
    <message>
        <source>OK</source>
        <translation>Жақсы</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Сақтау</translation>
    </message>
    <message>
        <source>Save All</source>
        <translation>Барлығын сақтау</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Ашу</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>Иә</translation>
    </message>
    <message>
        <source>Yes to &amp;All</source>
        <translation>Иә &gt; Барлығына</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&gt; Жоқ</translation>
    </message>
    <message>
        <source>N&amp;o to All</source>
        <translation>N&gt; Барлығына</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Доғару</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation>Ретри</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>Елемеу</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Жабу</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation>Алып тастау</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Анықтама</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Қолдану</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>Ысыру</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation>Әдепкілерді қалпына келтіру</translation>
    </message>
</context>
<context>
    <name>QGnomeTheme</name>
    <message>
        <source>&amp;OK</source>
        <translation>Ð Ð°Ò</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>Сақтау</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp; Бас тарту</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>Жабу@ action: inmenu</translation>
    </message>
    <message>
        <source>Close without Saving</source>
        <translation>Сақтаусыз жабу</translation>
    </message>
</context>
<context>
    <name>QWidgetTextControl</name>
    <message>
        <source>&amp;Undo</source>
        <translation>&gt; Болдырмау</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation>&gt; Редо</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation>Cu&gt;</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>&amp; Көшіру</translation>
    </message>
    <message>
        <source>Copy &amp;Link Location</source>
        <translation>Орынды көшіру</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation>&gt; Қою</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Өшіру</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation>Барлығын таңдау</translation>
    </message>
</context>
<context>
    <name>QPrintPropertiesDialog</name>
    <message>
        <source>Printer Properties</source>
        <translation>Принтер сипаттары</translation>
    </message>
    <message>
        <source>Job Options</source>
        <translation>Жұмыс параметрлері</translation>
    </message>
    <message>
        <source>Page Setup Conflicts</source>
        <translation>Бетті баптау қайшылықтары</translation>
    </message>
    <message>
        <source>There are conflicts in page setup options. Do you want to fix them?</source>
        <translation>Бетті орнату параметрлерінде қайшылықтар бар. Оларды түзеткіңіз келе ме?</translation>
    </message>
    <message>
        <source>Advanced Option Conflicts</source>
        <translation>Қосымша параметрлер қайшылықтары</translation>
    </message>
    <message>
        <source>There are conflicts in some advanced options. Do you want to fix them?</source>
        <translation>Кейбір жетілдірілген нұсқаларда қайшылықтар бар. Оларды түзеткіңіз келе ме?</translation>
    </message>
</context>
<context>
    <name>QPrintDialog</name>
    <message>
        <source>Left to Right, Top to Bottom</source>
        <translation>Солдан оңға, жоғарыдан төменге</translation>
    </message>
    <message>
        <source>Left to Right, Bottom to Top</source>
        <translation>Солдан оңға, төменнен жоғарыға</translation>
    </message>
    <message>
        <source>Right to Left, Bottom to Top</source>
        <translation>Оңнан солға, төменнен жоғарыға</translation>
    </message>
    <message>
        <source>Right to Left, Top to Bottom</source>
        <translation>Оңнан солға, жоғарыдан төменге</translation>
    </message>
    <message>
        <source>Bottom to Top, Left to Right</source>
        <translation>Төменнен жоғарыға, солдан оңға қарай</translation>
    </message>
    <message>
        <source>Bottom to Top, Right to Left</source>
        <translation>Төменнен жоғарыға, оңнан солға қарай</translation>
    </message>
    <message>
        <source>Top to Bottom, Left to Right</source>
        <translation>Жоғарыдан төменге, солдан оңға қарай</translation>
    </message>
    <message>
        <source>Top to Bottom, Right to Left</source>
        <translation>Жоғарыдан төменге, оңнан солға қарай</translation>
    </message>
    <message>
        <source>1 (1x1)</source>
        <translation>1 (1x1)</translation>
    </message>
    <message>
        <source>2 (2x1)</source>
        <translation>2 (2x1)</translation>
    </message>
    <message>
        <source>4 (2x2)</source>
        <translation>4 (2x2)</translation>
    </message>
    <message>
        <source>6 (2x3)</source>
        <translation>6 (2x3)</translation>
    </message>
    <message>
        <source>9 (3x3)</source>
        <translation>9 (3x3)</translation>
    </message>
    <message>
        <source>16 (4x4)</source>
        <translation>16 (4х4)</translation>
    </message>
    <message>
        <source>All Pages</source>
        <translation>Барлық беттер</translation>
    </message>
    <message>
        <source>Odd Pages</source>
        <translation>Тақ беттер</translation>
    </message>
    <message>
        <source>Even Pages</source>
        <translation>Жұп беттер</translation>
    </message>
</context>
<context>
    <name>QPageSize</name>
    <message>
        <source>A0</source>
        <translation>А0</translation>
    </message>
    <message>
        <source>A1</source>
        <translation>А1</translation>
    </message>
    <message>
        <source>A2</source>
        <translation>А2</translation>
    </message>
    <message>
        <source>A3</source>
        <translation>A3</translation>
    </message>
    <message>
        <source>A4</source>
        <translation>A4</translation>
    </message>
    <message>
        <source>A5</source>
        <translation>A5</translation>
    </message>
    <message>
        <source>A6</source>
        <translation>A6</translation>
    </message>
    <message>
        <source>A7</source>
        <translation>A7</translation>
    </message>
    <message>
        <source>A8</source>
        <translation>А8</translation>
    </message>
    <message>
        <source>A9</source>
        <translation>A9</translation>
    </message>
    <message>
        <source>A10</source>
        <translation>A10</translation>
    </message>
    <message>
        <source>B0</source>
        <translation>B0</translation>
    </message>
    <message>
        <source>B1</source>
        <translation>B1</translation>
    </message>
    <message>
        <source>B2</source>
        <translation>B2</translation>
    </message>
    <message>
        <source>B3</source>
        <translation>B3</translation>
    </message>
    <message>
        <source>B4</source>
        <translation>B4</translation>
    </message>
    <message>
        <source>B5</source>
        <translation>B5</translation>
    </message>
    <message>
        <source>B6</source>
        <translation>B6</translation>
    </message>
    <message>
        <source>B7</source>
        <translation>B7</translation>
    </message>
    <message>
        <source>B8</source>
        <translation>B8</translation>
    </message>
    <message>
        <source>B9</source>
        <translation>B9</translation>
    </message>
    <message>
        <source>B10</source>
        <translation>B10</translation>
    </message>
    <message>
        <source>Executive (7.5 x 10 in)</source>
        <translation>Атқарушы (7,5 х 10 в)</translation>
    </message>
    <message>
        <source>Executive (7.25 x 10.5 in)</source>
        <translation>Атқарушы (7,25 х 10,5 в)</translation>
    </message>
    <message>
        <source>Folio (8.27 x 13 in)</source>
        <translation>Фолио (8,27 х 13 в)</translation>
    </message>
    <message>
        <source>Legal</source>
        <translation>Заңды</translation>
    </message>
    <message>
        <source>Letter / ANSI A</source>
        <translation>Хат / ANSI A</translation>
    </message>
    <message>
        <source>Tabloid / ANSI B</source>
        <translation>Таблоид / ANSI B</translation>
    </message>
    <message>
        <source>Ledger / ANSI B</source>
        <translation>Ledger / ANSI B</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>Реттелетін</translation>
    </message>
    <message>
        <source>A3 Extra</source>
        <translation>A3 қосымша</translation>
    </message>
    <message>
        <source>A4 Extra</source>
        <translation>A4 қосымша</translation>
    </message>
    <message>
        <source>A4 Plus</source>
        <translation>A4 Плюс</translation>
    </message>
    <message>
        <source>A4 Small</source>
        <translation>A4 кішкентай</translation>
    </message>
    <message>
        <source>A5 Extra</source>
        <translation>A5 қосымша</translation>
    </message>
    <message>
        <source>B5 Extra</source>
        <translation>B5 қосымша</translation>
    </message>
    <message>
        <source>JIS B0</source>
        <translation>JIS B0</translation>
    </message>
    <message>
        <source>JIS B1</source>
        <translation>JIS B1</translation>
    </message>
    <message>
        <source>JIS B2</source>
        <translation>JIS B2</translation>
    </message>
    <message>
        <source>JIS B3</source>
        <translation>JIS B3</translation>
    </message>
    <message>
        <source>JIS B4</source>
        <translation>JIS B4</translation>
    </message>
    <message>
        <source>JIS B5</source>
        <translation>JIS B5</translation>
    </message>
    <message>
        <source>JIS B6</source>
        <translation>JIS B6</translation>
    </message>
    <message>
        <source>JIS B7</source>
        <translation>JIS B7</translation>
    </message>
    <message>
        <source>JIS B8</source>
        <translation>JIS B8</translation>
    </message>
    <message>
        <source>JIS B9</source>
        <translation>JIS B9</translation>
    </message>
    <message>
        <source>JIS B10</source>
        <translation>JIS B10</translation>
    </message>
    <message>
        <source>ANSI C</source>
        <translation>ANSI C</translation>
    </message>
    <message>
        <source>ANSI D</source>
        <translation>АНСИ Д</translation>
    </message>
    <message>
        <source>ANSI E</source>
        <translation>АНСИ Е</translation>
    </message>
    <message>
        <source>Legal Extra</source>
        <translation>Legal Extra</translation>
    </message>
    <message>
        <source>Letter Extra</source>
        <translation>Әріптен тыс</translation>
    </message>
    <message>
        <source>Letter Plus</source>
        <translation>Әріп плюс</translation>
    </message>
    <message>
        <source>Letter Small</source>
        <translation>Кіші әріп</translation>
    </message>
    <message>
        <source>Tabloid Extra</source>
        <translation>Tabloid Extra</translation>
    </message>
    <message>
        <source>Architect A</source>
        <translation>Сәулетші А</translation>
    </message>
    <message>
        <source>Architect B</source>
        <translation>Сәулетші Б</translation>
    </message>
    <message>
        <source>Architect C</source>
        <translation>Сәулетші С</translation>
    </message>
    <message>
        <source>Architect D</source>
        <translation>Сәулетші Д</translation>
    </message>
    <message>
        <source>Architect E</source>
        <translation>Сәулетші Е</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Ескерту</translation>
    </message>
    <message>
        <source>Quarto</source>
        <translation>Кварто</translation>
    </message>
    <message>
        <source>Statement</source>
        <translation>Мәлімдеме</translation>
    </message>
    <message>
        <source>Super A</source>
        <translation>Супер А</translation>
    </message>
    <message>
        <source>Super B</source>
        <translation>Супер Б</translation>
    </message>
    <message>
        <source>Postcard</source>
        <translation>Ашық хат</translation>
    </message>
    <message>
        <source>Double Postcard</source>
        <translation>Қос ашықхат</translation>
    </message>
    <message>
        <source>PRC 16K</source>
        <translation>ҚХР 16К</translation>
    </message>
    <message>
        <source>PRC 32K</source>
        <translation>ҚХР 32К</translation>
    </message>
    <message>
        <source>PRC 32K Big</source>
        <translation>ҚХР 32K Big</translation>
    </message>
    <message>
        <source>Fan-fold US (14.875 x 11 in)</source>
        <translation>Фан-бүктемелі АҚШ (14,875 х 11 в)</translation>
    </message>
    <message>
        <source>Fan-fold German (8.5 x 12 in)</source>
        <translation>Неміс фан-бүктемелі (8,5 х 12 в)</translation>
    </message>
    <message>
        <source>Fan-fold German Legal (8.5 x 13 in)</source>
        <translation>Фан-бүктемелі неміс заңы (8,5 х 13 в)</translation>
    </message>
    <message>
        <source>Envelope B4</source>
        <translation>B4 конверті</translation>
    </message>
    <message>
        <source>Envelope B5</source>
        <translation>В5 конверті</translation>
    </message>
    <message>
        <source>Envelope B6</source>
        <translation>B6 конверті</translation>
    </message>
    <message>
        <source>Envelope C0</source>
        <translation>С0 конверті</translation>
    </message>
    <message>
        <source>Envelope C1</source>
        <translation>С1 конверті</translation>
    </message>
    <message>
        <source>Envelope C2</source>
        <translation>С2 конверті</translation>
    </message>
    <message>
        <source>Envelope C3</source>
        <translation>С3 конверті</translation>
    </message>
    <message>
        <source>Envelope C4</source>
        <translation>С4 конверті</translation>
    </message>
    <message>
        <source>Envelope C5</source>
        <translation>С5 конверті</translation>
    </message>
    <message>
        <source>Envelope C6</source>
        <translation>С6 конверті</translation>
    </message>
    <message>
        <source>Envelope C65</source>
        <translation>С65 конверті</translation>
    </message>
    <message>
        <source>Envelope C7</source>
        <translation>С7 конверті</translation>
    </message>
    <message>
        <source>Envelope DL</source>
        <translation>DL конверті</translation>
    </message>
    <message>
        <source>Envelope US 9</source>
        <translation>US 9 конверті</translation>
    </message>
    <message>
        <source>Envelope US 10</source>
        <translation>US 10 конверті</translation>
    </message>
    <message>
        <source>Envelope US 11</source>
        <translation>US 11 конверті</translation>
    </message>
    <message>
        <source>Envelope US 12</source>
        <translation>US 12 конверті</translation>
    </message>
    <message>
        <source>Envelope US 14</source>
        <translation>US 14 конверті</translation>
    </message>
    <message>
        <source>Envelope Monarch</source>
        <translation>Монарх конверті</translation>
    </message>
    <message>
        <source>Envelope Personal</source>
        <translation>Жеке конверт</translation>
    </message>
    <message>
        <source>Envelope Chou 3</source>
        <translation>Chou 3 конверті</translation>
    </message>
    <message>
        <source>Envelope Chou 4</source>
        <translation>Chou 4 конверті</translation>
    </message>
    <message>
        <source>Envelope Invite</source>
        <translation>Хатқалта шақыру</translation>
    </message>
    <message>
        <source>Envelope Italian</source>
        <translation>Итальян конверті</translation>
    </message>
    <message>
        <source>Envelope Kaku 2</source>
        <translation>Каку 2 конверті</translation>
    </message>
    <message>
        <source>Envelope Kaku 3</source>
        <translation>Каку 3 конверті</translation>
    </message>
    <message>
        <source>Envelope PRC 1</source>
        <translation>ҚХР 1 конверті</translation>
    </message>
    <message>
        <source>Envelope PRC 2</source>
        <translation>ҚХР 2 конверті</translation>
    </message>
    <message>
        <source>Envelope PRC 3</source>
        <translation>ҚХР 3 конверті</translation>
    </message>
    <message>
        <source>Envelope PRC 4</source>
        <translation>ҚХР 4 конверті</translation>
    </message>
    <message>
        <source>Envelope PRC 5</source>
        <translation>ҚХР 5 конверті</translation>
    </message>
    <message>
        <source>Envelope PRC 6</source>
        <translation>ҚХР 6 конверті</translation>
    </message>
    <message>
        <source>Envelope PRC 7</source>
        <translation>ҚХР 7 конверті</translation>
    </message>
    <message>
        <source>Envelope PRC 8</source>
        <translation>ҚХР 8 конверті</translation>
    </message>
    <message>
        <source>Envelope PRC 9</source>
        <translation>ҚХР 9 конверті</translation>
    </message>
    <message>
        <source>Envelope PRC 10</source>
        <translation>ҚХР 10 конверті</translation>
    </message>
    <message>
        <source>Envelope You 4</source>
        <translation>You 4 конверті</translation>
    </message>
</context>
</TS>
