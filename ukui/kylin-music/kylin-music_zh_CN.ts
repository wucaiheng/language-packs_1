<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>CustomToolButton</name>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="202"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="212"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="203"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="214"/>
        <source>Pause</source>
        <translation>暂停</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="204"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="215"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="205"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="217"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="222"/>
        <source>Song List</source>
        <translation>歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="222"/>
        <source>I Love</source>
        <translation>我喜欢</translation>
    </message>
</context>
<context>
    <name>MiniWidget</name>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="34"/>
        <location filename="../UI/player/miniWidget.cpp" line="328"/>
        <location filename="../UI/player/miniWidget.cpp" line="679"/>
        <location filename="../UI/player/miniWidget.cpp" line="804"/>
        <source>Music Player</source>
        <translation>音乐</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="49"/>
        <location filename="../UI/player/miniWidget.cpp" line="50"/>
        <location filename="../UI/player/miniWidget.cpp" line="394"/>
        <location filename="../UI/player/miniWidget.cpp" line="787"/>
        <source>Loop</source>
        <translation>列表循环</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="51"/>
        <location filename="../UI/player/miniWidget.cpp" line="402"/>
        <location filename="../UI/player/miniWidget.cpp" line="792"/>
        <source>Random</source>
        <translation>随机播放</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="52"/>
        <source>Sequence</source>
        <translation>顺序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="329"/>
        <source>00:00/00:00</source>
        <translation>00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="53"/>
        <location filename="../UI/player/miniWidget.cpp" line="386"/>
        <location filename="../UI/player/miniWidget.cpp" line="782"/>
        <source>CurrentItemInLoop</source>
        <translation>单曲循环</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="514"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="505"/>
        <source>Pause</source>
        <translation>暂停</translation>
    </message>
    <message>
        <source>Favourite</source>
        <translation type="vanished">收藏</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="418"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="427"/>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="558"/>
        <location filename="../UI/player/miniWidget.cpp" line="573"/>
        <source>I Love</source>
        <translation>我喜欢</translation>
    </message>
</context>
<context>
    <name>MusicDataBase</name>
    <message>
        <source>Prompt information</source>
        <translation type="obsolete">提示信息</translation>
    </message>
    <message>
        <source>Single song name is already exists</source>
        <translation type="obsolete">歌单名已存在</translation>
    </message>
</context>
<context>
    <name>MusicFileInformation</name>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="220"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="262"/>
        <source>Unknown singer</source>
        <translation>未知歌手</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="221"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="268"/>
        <source>Unknown album</source>
        <translation>未知专辑</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="422"/>
        <source>Prompt information</source>
        <translation>提示信息</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="422"/>
        <source>Add failed, no valid music file found</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MusicInfoDialog</name>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="204"/>
        <source>Music Information</source>
        <translation>歌曲信息</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="179"/>
        <source>Music Player</source>
        <translation>音乐</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="225"/>
        <source>Confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="227"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="257"/>
        <source>Song Name : </source>
        <translation>歌曲名：</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="258"/>
        <source>Singer : </source>
        <translation>歌手：</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="259"/>
        <source>Album : </source>
        <translation>专辑：</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="260"/>
        <source>File Type : </source>
        <translation>文件类型：</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="261"/>
        <source>File Size : </source>
        <translation>文件大小：</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="262"/>
        <source>File Time : </source>
        <translation>歌曲时长：</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="263"/>
        <source>File Path : </source>
        <translation>文件路径：</translation>
    </message>
</context>
<context>
    <name>MusicListModel</name>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="8"/>
        <source>Song</source>
        <translation>歌曲</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="9"/>
        <source>Singer</source>
        <translation>歌手</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="10"/>
        <source>Album</source>
        <translation>专辑</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="11"/>
        <source>Time</source>
        <translation>时长</translation>
    </message>
</context>
<context>
    <name>PlayBackModeWidget</name>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="29"/>
        <source>Loop</source>
        <translation>列表循环</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="44"/>
        <source>Random</source>
        <translation>随机播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="60"/>
        <source>Sequential</source>
        <translation>顺序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="75"/>
        <source>CurrentItemInLoop</source>
        <translation>单曲循环</translation>
    </message>
</context>
<context>
    <name>PlaySongArea</name>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="34"/>
        <source>Previous</source>
        <translation>上一首</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="40"/>
        <location filename="../UI/player/playsongarea.cpp" line="525"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="45"/>
        <source>Next</source>
        <translation>下一首</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="50"/>
        <source>Volume</source>
        <translation>音量</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="103"/>
        <source>Favourite</source>
        <translation>收藏</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="127"/>
        <location filename="../UI/player/playsongarea.cpp" line="408"/>
        <source>Loop</source>
        <translation>列表循环</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="152"/>
        <source>Play List</source>
        <translation>播放历史</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="169"/>
        <location filename="../UI/player/playsongarea.cpp" line="435"/>
        <location filename="../UI/player/playsongarea.cpp" line="437"/>
        <location filename="../UI/player/playsongarea.cpp" line="579"/>
        <location filename="../UI/player/playsongarea.cpp" line="912"/>
        <location filename="../UI/player/playsongarea.cpp" line="937"/>
        <source>Music Player</source>
        <translation>音乐</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="170"/>
        <source>00:00/00:00</source>
        <translation>00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="354"/>
        <location filename="../UI/player/playsongarea.cpp" line="366"/>
        <source>I Love</source>
        <translation>我喜欢</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="515"/>
        <source>Pause</source>
        <translation>暂停</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="135"/>
        <location filename="../UI/player/playsongarea.cpp" line="413"/>
        <source>Random</source>
        <translation>随机播放</translation>
    </message>
    <message>
        <source>Sequential</source>
        <translation type="vanished">顺序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="119"/>
        <location filename="../UI/player/playsongarea.cpp" line="403"/>
        <source>CurrentItemInLoop</source>
        <translation>单曲循环</translation>
    </message>
</context>
<context>
    <name>PopupDialog</name>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="47"/>
        <location filename="../UI/base/popupDialog.cpp" line="82"/>
        <source>Prompt information</source>
        <translation>提示信息</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="47"/>
        <source>Could not contain characters:  / : * ? &quot; &amp; &lt; &gt; |</source>
        <translation>不能包含以下字符：\ / : * ? &quot; &amp; &lt; &gt; |</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="55"/>
        <source>Reached upper character limit</source>
        <translation>已达到字符上限</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="64"/>
        <source>Music Player</source>
        <translation>音乐</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="109"/>
        <source>Confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="113"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="121"/>
        <source>Please input playlist name:</source>
        <translation>请输入歌单名称：</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../UIControl/base/musicDataBase.cpp" line="60"/>
        <source>Database Error</source>
        <translation>数据库错误</translation>
    </message>
</context>
<context>
    <name>SearchEdit</name>
    <message>
        <location filename="../UI/player/searchedit.cpp" line="111"/>
        <source>Search Result</source>
        <translation>搜索结果</translation>
    </message>
</context>
<context>
    <name>SearchResult</name>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="29"/>
        <source>Music</source>
        <translation>歌曲</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="45"/>
        <source>Singer</source>
        <translation>歌手</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="61"/>
        <source>Album</source>
        <translation>专辑</translation>
    </message>
</context>
<context>
    <name>SideBarWidget</name>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="56"/>
        <source>Music Player</source>
        <translation>音乐</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="79"/>
        <source>Library</source>
        <translation>音乐库</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="86"/>
        <source>Song List</source>
        <translation>歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="109"/>
        <source>My PlayList</source>
        <translation>我的歌单</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="139"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="352"/>
        <source>New Playlist</source>
        <translation>新建歌单</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="143"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="251"/>
        <source>I Love</source>
        <translation>我喜欢</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="306"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="428"/>
        <source>Prompt information</source>
        <translation>提示信息</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="306"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="428"/>
        <source>Single song name already exists!!!</source>
        <translation>歌单名已存在！</translation>
    </message>
</context>
<context>
    <name>TableHistory</name>
    <message>
        <source>HistoryPlayList</source>
        <translation type="vanished">播放列表</translation>
    </message>
    <message>
        <source>PlayList</source>
        <translation type="vanished">播放历史</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="49"/>
        <source>History</source>
        <translation>播放历史</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="56"/>
        <source>Empty</source>
        <translation>清空</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="80"/>
        <source>The playlist has no songs</source>
        <translation>播放列表还没有歌曲哦</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="136"/>
        <source>Prompt information</source>
        <translation>提示信息</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="136"/>
        <source>Clear the playlist?</source>
        <translation>清空播放列表？</translation>
    </message>
    <message>
        <source>song</source>
        <translation type="vanished">首</translation>
    </message>
    <message>
        <source>total</source>
        <translation type="vanished">共</translation>
    </message>
    <message>
        <source>songs</source>
        <translation type="vanished">首</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="429"/>
        <location filename="../UI/tableview/tablehistory.cpp" line="457"/>
        <source>path does not exist</source>
        <translation>路径不存在</translation>
    </message>
    <message>
        <source> song</source>
        <translation type="vanished"> 首歌曲</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <source> songs</source>
        <translation type="vanished"> 首歌曲</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="323"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="324"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="325"/>
        <source>Play the next one</source>
        <translation>下一首播放</translation>
    </message>
    <message>
        <source>The song doesn&apos;t exist</source>
        <translation type="vanished">歌曲不存在！</translation>
    </message>
</context>
<context>
    <name>TableOne</name>
    <message>
        <source>Add</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="vanished">歌曲</translation>
    </message>
    <message>
        <source>Singer</source>
        <translation type="vanished">歌手</translation>
    </message>
    <message>
        <source>Album</source>
        <translation type="vanished">专辑</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">时长</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="279"/>
        <location filename="../UI/tableview/tableone.cpp" line="1167"/>
        <location filename="../UI/tableview/tableone.cpp" line="1227"/>
        <location filename="../UI/tableview/tableone.cpp" line="1318"/>
        <source>Song List</source>
        <translation>歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="205"/>
        <source>There are no songs!</source>
        <translation>还没有歌曲！</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="148"/>
        <source>Play All</source>
        <translation>播放全部</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="151"/>
        <source>Add Music</source>
        <translation>添加音乐</translation>
    </message>
    <message>
        <source>Add local songs</source>
        <translation type="vanished">添加本地音乐</translation>
    </message>
    <message>
        <source>Add local folders</source>
        <translation type="vanished">添加本地文件夹</translation>
    </message>
    <message>
        <source>Add Local Songs</source>
        <translation type="vanished">添加本地音乐</translation>
    </message>
    <message>
        <source>Add Local Folder</source>
        <translation type="vanished">添加本地文件夹</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="157"/>
        <location filename="../UI/tableview/tableone.cpp" line="207"/>
        <source>Open File</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="160"/>
        <location filename="../UI/tableview/tableone.cpp" line="208"/>
        <source>Open Folder</source>
        <translation>打开文件夹</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="281"/>
        <location filename="../UI/tableview/tableone.cpp" line="374"/>
        <location filename="../UI/tableview/tableone.cpp" line="376"/>
        <location filename="../UI/tableview/tableone.cpp" line="700"/>
        <location filename="../UI/tableview/tableone.cpp" line="1169"/>
        <location filename="../UI/tableview/tableone.cpp" line="1229"/>
        <source>I Love</source>
        <translation>我喜欢</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="283"/>
        <location filename="../UI/tableview/tableone.cpp" line="1171"/>
        <location filename="../UI/tableview/tableone.cpp" line="1231"/>
        <location filename="../UI/tableview/tableone.cpp" line="1277"/>
        <location filename="../UI/tableview/tableone.cpp" line="1344"/>
        <location filename="../UI/tableview/tableone.cpp" line="1373"/>
        <source>Search Result</source>
        <translation>搜索结果</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="312"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <source>Delete from list</source>
        <translation type="vanished">删除选中项</translation>
    </message>
    <message>
        <source>Remove from local</source>
        <translation type="vanished">从本地删除</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="313"/>
        <source>Delete Selected Items</source>
        <translation>删除选中项</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="315"/>
        <source>Song Information</source>
        <translation>歌曲信息</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="316"/>
        <source>Add To Songlist</source>
        <translation>添加歌曲到</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="317"/>
        <source>Open The Folder</source>
        <translation>打开所在文件夹</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="410"/>
        <source>Confirm that the selected song will be deleted from the song list?</source>
        <translation>确认将选中的歌曲从歌单中删除？</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="428"/>
        <source>After the song is deleted from the local, it cannot be resumed. Is it sure to delete?</source>
        <translation>歌曲从本地删除后不可恢复，是否确定删除？</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="782"/>
        <source>Audio File</source>
        <translation>音频文件</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="891"/>
        <source>Are you sure to clear the list?</source>
        <translation>确认清空歌单列表？</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="972"/>
        <source>This format file is not supported</source>
        <translation>不支持此格式文件</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="981"/>
        <location filename="../UI/tableview/tableone.cpp" line="1010"/>
        <source>Success add %1 song</source>
        <translation>成功添加%1首歌曲</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="1015"/>
        <source>Repeat add</source>
        <translation>重复添加</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="1054"/>
        <source>path does not exist</source>
        <translation>路径不存在</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="1096"/>
        <source> song</source>
        <translation> 首歌曲</translation>
    </message>
    <message>
        <source>Are you sure you want to delete it locally?</source>
        <translation type="vanished">您确定从本地删除吗？</translation>
    </message>
    <message>
        <source>Success add %1 songs</source>
        <translation type="vanished">成功添加%1首歌曲</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="992"/>
        <source>Add failed</source>
        <translation>添加失败</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <source>View song information</source>
        <translation type="vanished">歌曲信息</translation>
    </message>
    <message>
        <source>Add to songlist</source>
        <translation type="vanished">添加歌曲到</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="318"/>
        <source>Clear List</source>
        <translation>清空列表</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="410"/>
        <location filename="../UI/tableview/tableone.cpp" line="428"/>
        <location filename="../UI/tableview/tableone.cpp" line="891"/>
        <location filename="../UI/tableview/tableone.cpp" line="981"/>
        <location filename="../UI/tableview/tableone.cpp" line="992"/>
        <location filename="../UI/tableview/tableone.cpp" line="1010"/>
        <location filename="../UI/tableview/tableone.cpp" line="1015"/>
        <source>Prompt information</source>
        <translation>提示信息</translation>
    </message>
    <message>
        <source>Add failed!</source>
        <translation type="vanished">添加失败！</translation>
    </message>
    <message>
        <source>Failed to add song file!</source>
        <translation type="vanished">添加歌曲文件失败！</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="1098"/>
        <source> songs</source>
        <translation> 首歌曲</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="57"/>
        <source>back</source>
        <translation>后退</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="68"/>
        <source>forward</source>
        <translation>前进</translation>
    </message>
    <message>
        <source>Search for music, singers</source>
        <translation type="vanished">搜索音乐，歌手</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="91"/>
        <source>Not logged in</source>
        <translation>未登录</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">主菜单</translation>
    </message>
    <message>
        <source>menu</source>
        <translation type="vanished">主菜单</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="159"/>
        <source>mini model</source>
        <translation>精简模式</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="167"/>
        <source>To minimize the</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="177"/>
        <source>maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="187"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../UI/mainwidget.cpp" line="695"/>
        <source>Music Player</source>
        <translation>音乐</translation>
    </message>
    <message>
        <source>Song List</source>
        <translation type="vanished">歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="955"/>
        <location filename="../UI/mainwidget.cpp" line="1208"/>
        <source>reduction</source>
        <translation>还原</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="959"/>
        <location filename="../UI/mainwidget.cpp" line="1194"/>
        <source>maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="1334"/>
        <source>Prompt information</source>
        <translation>提示信息</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Menu</source>
        <translation type="vanished">菜单</translation>
    </message>
    <message>
        <source>Option</source>
        <translation type="vanished">选项</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="24"/>
        <source>Options</source>
        <translation>选项</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="37"/>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="39"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="125"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="41"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="123"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="43"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="121"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="51"/>
        <source>Auto</source>
        <translation>跟随主题</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="56"/>
        <source>Light</source>
        <translation>浅色</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="59"/>
        <source>Dark</source>
        <translation>深色</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <source>Music Player</source>
        <translation type="vanished">音乐</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="189"/>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,fast and simple.</source>
        <translation>音乐播放器是用于播放各种音乐格式的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单。</translation>
    </message>
    <message>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,Fast and simple.</source>
        <translation type="vanished">音乐播放器是一种用于播放各种音乐文件的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单。</translation>
    </message>
    <message>
        <source>Music Player is a kind of multimedia player software for playing various music files.It covers a variety of music formats play tool,easy to operate.</source>
        <translation type="vanished">音乐播放器是一种用于播放各种音乐文件的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="188"/>
        <source>Version: </source>
        <translation>版本：</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.h" line="63"/>
        <source>kylin-music</source>
        <translation>音乐</translation>
    </message>
</context>
</TS>
