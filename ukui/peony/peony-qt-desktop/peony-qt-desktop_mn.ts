<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Peony::DesktopIconView</name>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.h" line="81"/>
        <source>Desktop Icon View</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠢᠺᠦᠨ ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="648"/>
        <source>New Folder</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="740"/>
        <source>set background</source>
        <translation>ᠠᠷᠤ ᠦᠵᠡᠭᠳᠡᠯ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="973"/>
        <source>Open failed</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="974"/>
        <source>Open directory failed, you have no permission!</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ, ᠲᠠᠨ ᠳ᠋ᠤ᠌ ᠳᠤᠰ ᠭᠠᠷᠴᠠᠭ ᠤ᠋ᠨ ᠡᠷᠬᠡ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="957"/>
        <source>Open Link failed</source>
        <translation>ᠲᠦᠳᠡ ᠠᠷᠭ᠎ᠠ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="958"/>
        <source>File not exist, do you want to delete the link file?</source>
        <translation>ᠬᠠᠷᠠᠯᠳᠠᠳᠤ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ ᠤᠷᠤᠰᠢᠬᠤ ᠦᠬᠡᠢ, ᠳᠤᠰ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠲᠦᠳᠡ ᠠᠷᠭ᠎ᠠ ᠵᠢ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
</context>
<context>
    <name>Peony::DesktopItemModel</name>
    <message>
        <location filename="../../peony-qt-desktop/desktop-item-model.cpp" line="582"/>
        <source>Computer</source>
        <translation>ᠺᠤᠮᠫᠢᠦᠲᠸᠷ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-item-model.cpp" line="584"/>
        <source>Trash</source>
        <translation>ᠬᠤᠭᠯᠠᠭᠤᠷ</translation>
    </message>
</context>
<context>
    <name>Peony::DesktopMenu</name>
    <message>
        <source>&amp;Open in new Window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <source>Select &amp;All</source>
        <translation type="vanished">全选(&amp;A)</translation>
    </message>
    <message>
        <source>&amp;Open &quot;%1&quot;</source>
        <translation type="vanished">打开 &quot;%1&quot;(&amp;O)</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; &amp;with...</source>
        <translation type="vanished">打开方式(&amp;W)...</translation>
    </message>
    <message>
        <source>&amp;More applications...</source>
        <translation type="vanished">更多应用(&amp;M)...</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; with...</source>
        <translation type="vanished">打开方式...</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="vanished">打开(&amp;O)</translation>
    </message>
    <message>
        <source>Open &amp;with...</source>
        <translation type="vanished">打开方式(&amp;W)...</translation>
    </message>
    <message>
        <source>&amp;Open %1 selected files</source>
        <translation type="vanished">打开%1个选中文件(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="235"/>
        <source>Reverse Select</source>
        <translation>ᠤᠷᠪᠠᠭᠤ ᠪᠡᠷ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <source>&amp;New...</source>
        <translation type="vanished">新建(&amp;N)</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="249"/>
        <source>New...</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ...</translation>
    </message>
    <message>
        <source>Empty &amp;File</source>
        <translation type="vanished">空文件(&amp;E)</translation>
    </message>
    <message>
        <source>&amp;Folder</source>
        <translation type="vanished">文件夹(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="332"/>
        <source>New Folder</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="349"/>
        <source>View Type...</source>
        <translation>ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ...</translation>
    </message>
    <message>
        <source>&amp;Small</source>
        <translation type="vanished">小图标(&amp;S)</translation>
    </message>
    <message>
        <source>&amp;Normal</source>
        <translation type="vanished">中图标(&amp;N)</translation>
    </message>
    <message>
        <source>&amp;Large</source>
        <translation type="vanished">大图标(&amp;L)</translation>
    </message>
    <message>
        <source>&amp;Huge</source>
        <translation type="vanished">超大图标(&amp;H)</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="107"/>
        <source>Open in new Window</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠴᠤᠩᠬᠤᠨ ᠳᠡᠭᠡᠷ᠎ᠡ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="112"/>
        <source>Select All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="135"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="163"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="197"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="208"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="143"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="168"/>
        <source>Open with...</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ ᠠᠷᠭ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="156"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="191"/>
        <source>More applications...</source>
        <translation>ᠤᠯᠠᠮ ᠤᠯᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ...</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="211"/>
        <source>Open %1 selected files</source>
        <translation>%1 ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="318"/>
        <source>Empty File</source>
        <translation>ᠬᠤᠭᠤᠰᠤᠨ ᠲᠸᠺᠰᠲ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="328"/>
        <source>Folder</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="355"/>
        <source>Small</source>
        <translation>ᠵᠢᠵᠢᠭ ᠢᠺᠦᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="358"/>
        <source>Normal</source>
        <translation>ᠳᠤᠮᠳᠠ ᠵᠡᠷᠭᠡ ᠵᠢᠨ ᠢᠺᠦᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="361"/>
        <source>Large</source>
        <translation>ᠲᠤᠮᠤ ᠢᠺᠦᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="364"/>
        <source>Huge</source>
        <translation>ᠮᠠᠰᠢ ᠲᠤᠮᠤ ᠢᠺᠦᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="392"/>
        <source>Sort By...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="397"/>
        <source>Name</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="399"/>
        <source>File Type</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="400"/>
        <source>File Size</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="472"/>
        <source>Clean the trash</source>
        <translation>ᠬᠤᠭᠯᠠᠭᠤᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="486"/>
        <source>Copy</source>
        <translation>ᠺᠤᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="511"/>
        <source>Delete to trash</source>
        <translation>ᠬᠠᠰᠤᠵᠤ ᠬᠤᠭᠯᠠᠭᠤᠷ ᠲᠤ᠌ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>Can not show trash properties with other files together!</source>
        <translation type="vanished">不能将回收站与其他文件一起查看属性!</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="398"/>
        <source>Modified Date</source>
        <translation>ᠵᠠᠰᠠᠭᠰᠠᠨ ᠡᠳᠦᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="490"/>
        <source>Cut</source>
        <translation>ᠬᠠᠢᠴᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>&amp;Delete to trash</source>
        <translation type="vanished">删除到回收站(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="520"/>
        <source>Delete forever</source>
        <translation>ᠦᠨᠢᠳᠡ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="528"/>
        <source>Rename</source>
        <translation>ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="535"/>
        <source>Paste</source>
        <translation>ᠨᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="541"/>
        <source>Refresh</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="555"/>
        <source>Properties</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <source>P&amp;roperties</source>
        <translation type="vanished">属性(&amp;R)</translation>
    </message>
    <message>
        <source>Sort Order...</source>
        <translation type="vanished">排列顺序...</translation>
    </message>
    <message>
        <source>Ascending Order</source>
        <translation type="vanished">升序</translation>
    </message>
    <message>
        <source>Descending Order</source>
        <translation type="vanished">降序</translation>
    </message>
    <message>
        <source>Zoom &amp;In</source>
        <translation type="vanished">放大(&amp;I)</translation>
    </message>
    <message>
        <source>Zoom &amp;Out</source>
        <translation type="vanished">缩小(&amp;O)</translation>
    </message>
    <message>
        <source>&amp;Restore all</source>
        <translation type="vanished">恢复全部(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Clean the trash</source>
        <translation type="vanished">清空回收站(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="474"/>
        <source>Delete Permanently</source>
        <translation>ᠦᠨᠢᠳᠡ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="474"/>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation>ᠲᠠ ᠨᠡᠬᠡᠷᠡᠨ ᠡᠳᠡᠭᠡᠷ ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠰᠤᠵᠤ ᠬᠠᠶᠠᠬᠤ ᠤᠤ? ᠬᠠᠰᠤᠵᠤ ᠡᠬᠢᠯᠡᠭᠰᠡᠬᠡᠷ ᠡᠳᠡᠭᠡᠷ ᠹᠠᠢᠯ ᠢ᠋ ᠳᠠᠬᠢᠵᠤ ᠰᠡᠷᠬᠦᠬᠡᠵᠤ ᠳᠡᠢᠯᠬᠦ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="vanished">复制(&amp;C)</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">剪切(&amp;T)</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">删除(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="vanished">重命名(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="vanished">粘贴(&amp;P)</translation>
    </message>
    <message>
        <source>&amp;Refresh</source>
        <translation type="vanished">刷新(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Properties</source>
        <translation type="vanished">属性(&amp;P)</translation>
    </message>
</context>
<context>
    <name>Peony::DesktopWindow</name>
    <message>
        <source>Desktop</source>
        <translation type="vanished">桌面</translation>
    </message>
    <message>
        <source>set background</source>
        <translation type="vanished">设置壁纸</translation>
    </message>
    <message>
        <source>New Folder</source>
        <translation type="vanished">新建文件夹</translation>
    </message>
</context>
<context>
    <name>PeonyDesktopApplication</name>
    <message>
        <source>Close the peony-qt desktop window</source>
        <translation type="vanished">关闭桌面并退出</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="180"/>
        <source>peony-qt-desktop</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ</translation>
    </message>
    <message>
        <source>Peony-Qt Desktop</source>
        <translation type="vanished">桌面</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation type="vanished">桌面</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="454"/>
        <source>Close the peony desktop window</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠫᠡᠷᠦᠭᠷᠡᠮ ᠢ᠋ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="457"/>
        <source>Take over the dbus service.</source>
        <translation>dbus ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠵᠢ ᠵᠠᠯᠭᠠᠵᠤ ᠬᠠᠮᠢᠶᠠᠷᠬᠤ.</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="460"/>
        <source>Take over the desktop displaying</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠢ᠋ ᠵᠠᠯᠭᠠᠵᠤ ᠬᠠᠮᠢᠶᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="463"/>
        <source>Setup backgrounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="466"/>
        <source>Clear standard icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>set background</source>
        <translation type="vanished">设置背景</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../peony-qt-desktop/desktopbackgroundwindow.cpp" line="70"/>
        <source>set background</source>
        <translation>ᠠᠷᠤ ᠦᠵᠡᠭᠳᠡᠯ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktopbackgroundwindow.cpp" line="75"/>
        <source>set resolution</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
