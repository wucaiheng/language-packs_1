<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="32"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="88"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="115"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Offical Website: </source>
        <translation type="vanished">官方网站: </translation>
    </message>
    <message>
        <source>Service &amp; Technology Support: </source>
        <translation type="vanished">服务与技术支持: </translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="132"/>
        <source>Service &amp; Support: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="62"/>
        <location filename="../../src/windows/about-dialog.cpp" line="78"/>
        <source>Peony</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>peony</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="93"/>
        <source>Peony is a graphical software to help users manage system files. It provides common file operation functions for users, such as file viewing, file copy, paste, cut, delete, rename, file selection, application opening, file search, file sorting, file preview, etc. it is convenient for users to manage system files intuitively on the interface.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hot Service: </source>
        <translation type="vanished">服务热线: </translation>
    </message>
    <message>
        <source>File Manager</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="81"/>
        <source>Version number: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File manager is a graphical software to help users manage system files. It provides common file operation functions for users, such as file viewing, file copy, paste, cut, delete, rename, file selection, application opening, file search, file sorting, file preview, etc. it is convenient for users to manage system files intuitively on the interface.</source>
        <translation type="vanished">文件管理器是一款帮助用户管理系统文件的图形化的软件，为用户提供常用的文件操作功能，比如文件查看，文件复制、粘贴、剪切、删除、重命名，文件打开方式选择，文件搜索，文件排序，文件预览等，方便用户在界面上直观地管理系统文件。</translation>
    </message>
    <message>
        <source>none</source>
        <translation type="vanished">无</translation>
    </message>
</context>
<context>
    <name>FileLabelBox</name>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="68"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="73"/>
        <source>Edit Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="84"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="89"/>
        <source>Create New Label</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HeaderBar</name>
    <message>
        <source>Create Folder</source>
        <translation type="vanished">新建文件夹</translation>
    </message>
    <message>
        <source>Open Terminal</source>
        <translation type="vanished">打开终端</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="139"/>
        <source>Go Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="148"/>
        <source>Go Forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="210"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="376"/>
        <source>View Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="402"/>
        <source>Sort Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="437"/>
        <source>Option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="276"/>
        <source>Operate Tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="277"/>
        <source>Don&apos;t find any terminal, please install at least one terminal!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="595"/>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="598"/>
        <source>Maximize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">最小化</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
</context>
<context>
    <name>HeaderBarContainer</name>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="755"/>
        <source>Minimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maximize/Restore</source>
        <translation type="vanished">最大化/还原</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation type="vanished">还原</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="780"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="742"/>
        <source>File Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="321"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="328"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="676"/>
        <source>warn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="676"/>
        <source>This operation is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="740"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tips info</source>
        <translation type="vanished">温馨提示</translation>
    </message>
    <message>
        <source>Trash has no file need to be cleaned.</source>
        <translation type="vanished">回收站没有文件需要被清空！</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <source>Delete Permanently</source>
        <translation type="vanished">永久删除</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="vanished">您确定要删除这些文件吗？一旦开始删除，这些文件将不可再恢复。</translation>
    </message>
    <message>
        <source>Peony Qt</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="755"/>
        <source>New Folder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NavigationSideBar</name>
    <message>
        <source>All tags...</source>
        <translation type="vanished">所有标记...</translation>
    </message>
    <message>
        <source>Open In &amp;New Window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="368"/>
        <source>warn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="368"/>
        <source>This operation is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="194"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="406"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="419"/>
        <source>Tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="194"/>
        <source>The device is in busy state, please perform this operation later.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="406"/>
        <source>This is an abnormal Udisk, please fix it or format it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="419"/>
        <source>This is an empty drive, please insert a Disc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="222"/>
        <source>Open In New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="238"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="272"/>
        <source>Can not open %1, %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="256"/>
        <source>Open In New Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open In New &amp;Tab</source>
        <translation type="vanished">在新标签页中打开(&amp;T)</translation>
    </message>
</context>
<context>
    <name>NavigationSideBarContainer</name>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="564"/>
        <source>All tags...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NavigationTabBar</name>
    <message>
        <source>Computer</source>
        <translation type="vanished">计算机</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-tab-bar.cpp" line="126"/>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OperationMenu</name>
    <message>
        <source>Advance Search</source>
        <translation type="vanished">高级搜索</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="69"/>
        <source>Keep Allow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="78"/>
        <source>Show Hidden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="86"/>
        <source>Show File Extension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="91"/>
        <source>Forbid thumbnailing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="100"/>
        <source>Resident in Backend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="109"/>
        <source>Parallel Operations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="118"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="122"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OperationMenuEditWidget</name>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="169"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="180"/>
        <source>copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="189"/>
        <source>paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="198"/>
        <source>cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="207"/>
        <source>trash</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PeonyApplication</name>
    <message>
        <source>Peony-Qt</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="144"/>
        <source>peony-qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="151"/>
        <source>Files or directories to open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="151"/>
        <source>[FILE1, FILE2,...]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="191"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="191"/>
        <source>Peony-Qt can not get the system&apos;s icon theme. There are 2 reasons might lead to this problem:

1. Peony-Qt might be running as root, that means you have the higher permission and can do some things which normally forbidden. But, you should learn that if you were in a root, the virtual file system will lose some featrue such as you can not use &quot;My Computer&quot;, the theme and icons might also went wrong. So, run peony-qt in a root is not recommended.

2. You are using a non-qt theme for your system but you didn&apos;t install the platform theme plugin for qt&apos;s applications. If you are using gtk-theme, try installing the qt5-gtk2-platformtheme package to resolve this problem.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="450"/>
        <source>Peony Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="451"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2020, KylinSoft Co., Ltd.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,麒麟软件有限公司.</translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,麒麟软件有限公司.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,麒麟软件有限公司.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="67"/>
        <source>Close all peony-qt windows and quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="68"/>
        <source>Show items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="69"/>
        <source>Show folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="70"/>
        <source>Show properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>Can not open %1.</source>
        <translation type="vanished">无法打开 %1.</translation>
    </message>
</context>
<context>
    <name>SortTypeMenu</name>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="34"/>
        <source>File Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="46"/>
        <source>File Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="50"/>
        <source>Original Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="82"/>
        <source>Use global sorting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="42"/>
        <source>File Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="38"/>
        <source>Modified Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Modified Data</source>
        <translation type="vanished">修改日期</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="71"/>
        <source>Ascending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="66"/>
        <source>Descending</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabStatusBar</name>
    <message>
        <source>Current path has:</source>
        <translation type="vanished">当前路径包含：</translation>
    </message>
    <message>
        <source>%1 folders, %2 files</source>
        <translation type="vanished">%1 文件夹，%2 文件</translation>
    </message>
    <message>
        <source>%1 folders</source>
        <translation type="vanished">%1 文件夹</translation>
    </message>
    <message>
        <source>%1 files</source>
        <translation type="vanished">%1 文件</translation>
    </message>
    <message>
        <source>; %1 folders</source>
        <translation type="vanished">; %1 个文件夹</translation>
    </message>
    <message>
        <source>; %1 files, %2 total</source>
        <translation type="vanished">; %1 个文件, 共%2</translation>
    </message>
    <message>
        <source>; %1 folder</source>
        <translation type="vanished">; %1 个文件夹</translation>
    </message>
    <message>
        <source>; %1 file, %2</source>
        <translation type="vanished">; %1 个文件, %2</translation>
    </message>
    <message>
        <source>%1 selected</source>
        <translation type="vanished">选中%1个</translation>
    </message>
    <message>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation type="vanished">在%2中搜索%1</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="105"/>
        <source> %1 items </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="141"/>
        <source> selected %1 items    %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="143"/>
        <source> selected %1 items</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabWidget</name>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="297"/>
        <source>Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="301"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="306"/>
        <source>Recover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="418"/>
        <source>Close Filter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="430"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="527"/>
        <source>Select Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1630"/>
        <source>Open failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1631"/>
        <source>Open directory failed, you have no permission!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close advance search.</source>
        <translation type="vanished">关闭高级搜索。</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="441"/>
        <source>Choose other path to search.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="450"/>
        <source>Search recursively</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>more options</source>
        <translation type="vanished">更多选项</translation>
    </message>
    <message>
        <source>Show/hide advance search</source>
        <translation type="vanished">显示/隐藏高级搜索</translation>
    </message>
    <message>
        <source>Select path</source>
        <translation type="vanished">选择路径</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="585"/>
        <location filename="../../src/control/tab-widget.cpp" line="746"/>
        <source>is</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="628"/>
        <source>Please input key words...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please input kay words...</source>
        <translation type="vanished">请输入关键词...</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="707"/>
        <location filename="../../src/control/tab-widget.cpp" line="730"/>
        <source>contains</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="273"/>
        <source>name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="273"/>
        <source>type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="273"/>
        <source>modify time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="273"/>
        <source>file size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="274"/>
        <location filename="../../src/control/tab-widget.h" line="276"/>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="274"/>
        <source>file folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="274"/>
        <source>image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="274"/>
        <source>video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="275"/>
        <source>text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="275"/>
        <source>audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="275"/>
        <source>others</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="275"/>
        <source>wps file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="276"/>
        <source>today</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="276"/>
        <source>this week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="276"/>
        <source>this month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="276"/>
        <source>this year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="276"/>
        <source>year ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>tiny(0-16K)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>small(16k-1M)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>empty(0K)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>medium(1M-128M)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>big(128M-1G)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>large(1-4G)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>great(&gt;4G)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>medium(1M-100M)</source>
        <translation type="vanished">中等(1M-100M)</translation>
    </message>
    <message>
        <source>big(100M-1G)</source>
        <translation type="vanished">很大(100M-1G)</translation>
    </message>
    <message>
        <source>large(&gt;1G)</source>
        <translation type="vanished">极大(&gt;1G)</translation>
    </message>
</context>
</TS>
