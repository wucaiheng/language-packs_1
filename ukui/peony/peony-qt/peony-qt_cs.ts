<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="32"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="88"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="115"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="132"/>
        <source>Service &amp; Support: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="93"/>
        <source>Peony is a graphical software to help users manage system files. It provides common file operation functions for users, such as file viewing, file copy, paste, cut, delete, rename, file selection, application opening, file search, file sorting, file preview, etc. it is convenient for users to manage system files intuitively on the interface.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="81"/>
        <source>Version number: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="62"/>
        <location filename="../../src/windows/about-dialog.cpp" line="78"/>
        <source>Peony</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="159"/>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileLabelBox</name>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="68"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="73"/>
        <source>Edit Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="81"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="86"/>
        <source>Create New Label</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HeaderBar</name>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="139"/>
        <source>Go Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="148"/>
        <source>Go Forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="210"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="373"/>
        <source>View Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="399"/>
        <source>Sort Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="426"/>
        <source>Option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="276"/>
        <source>Operate Tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="277"/>
        <source>Don&apos;t find any terminal, please install at least one terminal!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="584"/>
        <source>Restore</source>
        <translation type="unfinished">Onar</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="587"/>
        <source>Maximize</source>
        <translation type="unfinished">Büyüt</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">Küçült</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Kapat</translation>
    </message>
</context>
<context>
    <name>HeaderBarContainer</name>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="744"/>
        <source>Minimize</source>
        <translation type="unfinished">Küçült</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation type="vanished">Onar</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="vanished">Büyüt</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="769"/>
        <source>Close</source>
        <translation type="unfinished">Kapat</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="318"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="325"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="668"/>
        <source>warn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="668"/>
        <source>This operation is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="732"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="734"/>
        <source>File Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="1590"/>
        <source>Delete Permanently</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="1591"/>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="1601"/>
        <source>Tips info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="1602"/>
        <source>Trash has no file need to be cleaned.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Peony Qt</source>
        <translation type="vanished">Peony Qt</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="747"/>
        <source>New Folder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NavigationSideBar</name>
    <message>
        <source>All tags...</source>
        <translation type="vanished">Tüm etiketler...</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="358"/>
        <source>warn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="358"/>
        <source>This operation is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="396"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="409"/>
        <source>Tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="396"/>
        <source>This is an abnormal Udisk, please fix it or format it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="409"/>
        <source>This is an empty drive, please insert a Disc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="216"/>
        <source>Open In New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="232"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="266"/>
        <source>Can not open %1, %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="250"/>
        <source>Open In New Tab</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NavigationSideBarContainer</name>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="553"/>
        <source>All tags...</source>
        <translation type="unfinished">Tüm etiketler...</translation>
    </message>
</context>
<context>
    <name>NavigationTabBar</name>
    <message>
        <location filename="../../src/control/navigation-tab-bar.cpp" line="126"/>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OperationMenu</name>
    <message>
        <source>Advance Search</source>
        <translation type="vanished">Gelişmiş Arama</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="69"/>
        <source>Keep Allow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="78"/>
        <source>Show Hidden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="86"/>
        <source>Show File Extension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="91"/>
        <source>Forbid thumbnailing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="100"/>
        <source>Resident in Backend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="109"/>
        <source>Parallel Operations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="118"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="122"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OperationMenuEditWidget</name>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="169"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="180"/>
        <source>copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="189"/>
        <source>paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="198"/>
        <source>cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="207"/>
        <source>trash</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PeonyApplication</name>
    <message>
        <source>Peony-Qt</source>
        <translation type="vanished">Dosya Yöneticisi</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="144"/>
        <source>peony-qt</source>
        <translation>Dosya Yöneticisi</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="151"/>
        <source>Files or directories to open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="151"/>
        <source>[FILE1, FILE2,...]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="191"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="191"/>
        <source>Peony-Qt can not get the system&apos;s icon theme. There are 2 reasons might lead to this problem:

1. Peony-Qt might be running as root, that means you have the higher permission and can do some things which normally forbidden. But, you should learn that if you were in a root, the virtual file system will lose some featrue such as you can not use &quot;My Computer&quot;, the theme and icons might also went wrong. So, run peony-qt in a root is not recommended.

2. You are using a non-qt theme for your system but you didn&apos;t install the platform theme plugin for qt&apos;s applications. If you are using gtk-theme, try installing the qt5-gtk2-platformtheme package to resolve this problem.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="450"/>
        <source>Peony Qt</source>
        <translation type="unfinished">Peony Qt</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="451"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2020, KylinSoft Co., Ltd.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Telif Hakkı(C): 2019-2020, KYLIN Software Co., Ltd.</translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Telif (C): 2019-2020, KYLIN Software Co., Ltd.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Telif (C): 2019-2020, KYLIN Software Co., Ltd.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Telif (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="67"/>
        <source>Close all peony-qt windows and quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="68"/>
        <source>Show items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="69"/>
        <source>Show folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="70"/>
        <source>Show properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SortTypeMenu</name>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="34"/>
        <source>File Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="46"/>
        <source>File Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="50"/>
        <source>Original Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="82"/>
        <source>Use global sorting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="42"/>
        <source>File Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="38"/>
        <source>Modified Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Modified Data</source>
        <translation type="vanished">Düzenlenen Veri</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="71"/>
        <source>Ascending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="66"/>
        <source>Descending</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabStatusBar</name>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="102"/>
        <source> %1 items </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="138"/>
        <source> selected %1 items    %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="140"/>
        <source> selected %1 items</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabWidget</name>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="297"/>
        <source>Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="301"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="306"/>
        <source>Recover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="418"/>
        <source>Close Filter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="430"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="441"/>
        <source>Choose other path to search.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="450"/>
        <source>Search recursively</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="527"/>
        <source>Select Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="584"/>
        <location filename="../../src/control/tab-widget.cpp" line="720"/>
        <source>is</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="602"/>
        <source>Please input key words...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1574"/>
        <source>Open failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1575"/>
        <source>Open directory failed, you have no permission!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please input kay words...</source>
        <translation type="vanished">Lütfen anahtar kelimeler girin ...</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="681"/>
        <location filename="../../src/control/tab-widget.cpp" line="704"/>
        <source>contains</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="273"/>
        <source>name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="273"/>
        <source>type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="273"/>
        <source>modify time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="273"/>
        <source>file size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="274"/>
        <location filename="../../src/control/tab-widget.h" line="276"/>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="274"/>
        <source>file folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="274"/>
        <source>image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="274"/>
        <source>video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="275"/>
        <source>text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="275"/>
        <source>audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="275"/>
        <source>others</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="275"/>
        <source>wps file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="276"/>
        <source>today</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="276"/>
        <source>this week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="276"/>
        <source>this month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="276"/>
        <source>this year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="276"/>
        <source>year ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>tiny(0-16K)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>small(16k-1M)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>empty(0K)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>medium(1M-128M)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>big(128M-1G)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>large(1-4G)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>great(&gt;4G)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
