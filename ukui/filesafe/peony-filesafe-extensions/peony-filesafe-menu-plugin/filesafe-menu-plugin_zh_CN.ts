<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Peony::FilesafeMenuPlugin</name>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="75"/>
        <source>Create New FileSafe</source>
        <translation>创建保护箱</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="107"/>
        <source>Password Setting</source>
        <translation>修改保护箱密码</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="130"/>
        <source>FileSafe Lock</source>
        <translation>锁定保护箱</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="156"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="182"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.h" line="21"/>
        <source>Peony-Qt filesafe menu Extension</source>
        <translation>文件保护箱扩展</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.h" line="22"/>
        <source>filesafe Menu Extension.</source>
        <translation>保护箱菜单插件</translation>
    </message>
</context>
</TS>
