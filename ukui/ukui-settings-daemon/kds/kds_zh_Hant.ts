<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>KDSWidget</name>
    <message>
        <location filename="../kdswidget.ui" line="90"/>
        <source>System Screen Projection</source>
        <translation>系統螢幕投影</translation>
    </message>
    <message>
        <location filename="../kdswidget.ui" line="121"/>
        <source>FirstOutput:</source>
        <translation>首次輸出：</translation>
    </message>
    <message>
        <location filename="../kdswidget.cpp" line="223"/>
        <source>First Display</source>
        <translation>首次顯示</translation>
    </message>
    <message>
        <location filename="../kdswidget.cpp" line="228"/>
        <source>Mirror Display</source>
        <translation>鏡面顯示</translation>
    </message>
    <message>
        <location filename="../kdswidget.cpp" line="233"/>
        <source>Extend Display</source>
        <translation>擴展顯示</translation>
    </message>
    <message>
        <location filename="../kdswidget.cpp" line="238"/>
        <source>Vice Display</source>
        <translation>副顯示幕</translation>
    </message>
    <message>
        <source>N/A</source>
        <translation type="vanished">N/A</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.ui" line="14"/>
        <source>KDS</source>
        <translation>斷續器</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="95"/>
        <source>System Screen Projection</source>
        <translation>系統螢幕投影</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="126"/>
        <source>FirstOutput:</source>
        <translation>首次輸出：</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="137"/>
        <source>First Screen</source>
        <translation>第一個螢幕</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="145"/>
        <source>Clone Screen</source>
        <translation>克隆螢幕</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="153"/>
        <source>Extend Screen</source>
        <translation>擴展螢幕</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="161"/>
        <source>Vice Screen</source>
        <translation>副屏</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="161"/>
        <source>None</source>
        <translation>沒有</translation>
    </message>
</context>
</TS>
