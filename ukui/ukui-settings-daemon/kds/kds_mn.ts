<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>KDSWidget</name>
    <message>
        <location filename="../kdswidget.ui" line="90"/>
        <source>System Screen Projection</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠢ᠋ ᠳᠡᠯᠬᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../kdswidget.ui" line="121"/>
        <source>FirstOutput:</source>
        <translation>ᠠᠨᠭᠬᠠᠨ᠎ᠤ᠋ ᠭᠠᠷᠭᠠᠯᠳᠠ:</translation>
    </message>
    <message>
        <location filename="../kdswidget.cpp" line="223"/>
        <source>First Display</source>
        <translation>ᠵᠦᠪᠬᠡᠨ ᠺᠤᠮᠫᠢᠤᠲᠸᠷ᠎ᠤ᠋ᠨ ᠳᠡᠯᠬᠡᠴᠡᠨ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../kdswidget.cpp" line="228"/>
        <source>Mirror Display</source>
        <translation>ᠳᠤᠯᠢᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../kdswidget.cpp" line="233"/>
        <source>Extend Display</source>
        <translation>ᠦᠷᠬᠡᠳᠬᠡᠯ</translation>
    </message>
    <message>
        <location filename="../kdswidget.cpp" line="238"/>
        <source>Vice Display</source>
        <translation>ᠵᠦᠪᠬᠡᠨ ᠬᠤᠶᠠᠳᠤᠭᠠᠷ ᠳᠡᠯᠭᠡᠴᠡ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>N/A</source>
        <translation type="vanished">N/A</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.ui" line="14"/>
        <source>KDS</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠢ᠋ ᠳᠡᠯᠬᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="95"/>
        <source>System Screen Projection</source>
        <translation type="unfinished">ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠢ᠋ ᠳᠡᠯᠬᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="126"/>
        <source>FirstOutput:</source>
        <translation>ᠠᠨᠭᠬᠠᠨ᠎ᠤ᠋ ᠳᠡᠯᠭᠡᠴᠡ:</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="137"/>
        <source>First Screen</source>
        <translation>ᠵᠦᠪᠬᠡᠨ ᠺᠤᠮᠫᠢᠤᠲᠸᠷ᠎ᠤ᠋ᠨ ᠳᠡᠯᠬᠡᠴᠡ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="145"/>
        <source>Clone Screen</source>
        <translation>ᠳᠤᠯᠢᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="153"/>
        <source>Extend Screen</source>
        <translation>ᠦᠷᠬᠡᠳᠬᠡᠯ</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="161"/>
        <source>Vice Screen</source>
        <translation>ᠵᠦᠪᠬᠡᠨ ᠬᠤᠶᠠᠳᠤᠭᠠᠷ ᠳᠡᠯᠭᠡᠴᠡ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="161"/>
        <source>None</source>
        <translation>No</translation>
    </message>
</context>
</TS>
