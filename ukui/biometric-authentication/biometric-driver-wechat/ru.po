# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the biometric-driver-eyecool-facedetect package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: biometric-driver-eyecool-facedetect 0.0.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-21 15:38+0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../libwechat/wechat_realization.cpp:384
#, c-format
msgid "_Verify face feature data successful"
msgstr ""

#. 没有匹配内容
#: ../libwechat/wechat_realization.cpp:395
#, c-format
msgid "_Verify face feature data fail"
msgstr ""

#: ../libwechat/wechat_realization.cpp:517
#, c-format
msgid "_identify face feature successful, its Minimum UID is %d"
msgstr ""

#: ../libwechat/wechat_realization.cpp:525
#, c-format
msgid "_identify face feature fail"
msgstr ""

#: ../libwechat/wechat_realization.cpp:602
#, c-format
msgid "_search face feature successful"
msgstr ""

#: ../libwechat/wechat_realization.cpp:610
#, c-format
msgid "_search face feature fail"
msgstr ""

#. 设置状态
#: ../libwechat/wechat_realization.cpp:693
#, c-format
msgid "_get_feature_list face feature seccessful"
msgstr ""

#: ../libwechat/wechat_realization.cpp:733
#, c-format
msgid "_Device %s[%d] received interrupt request\n"
msgstr ""

#: ../libwechat/wechat_realization.cpp:778
msgid "The account has been bound to a WeChat account"
msgstr ""

#: ../libwechat/wechat_realization.cpp:781
msgid "Network failed"
msgstr ""

#: ../libwechat/wechat_realization.cpp:803
msgid "Waiting for scanning qr image"
msgstr ""

#: ../libwechat/wechat_realization.cpp:805
msgid "Network error "
msgstr ""
