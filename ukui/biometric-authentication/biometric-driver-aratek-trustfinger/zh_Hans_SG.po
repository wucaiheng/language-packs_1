# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#
msgid ""
msgstr ""
"Project-Id-Version: biometric-driver-em1600 0.1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-11 09:57+0800\n"
"PO-Revision-Date: 2019-05-21 14:03+0800\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: zh_Hans_SG\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../libaratek/aratek_driver.c:134
#, c-format
msgid "Detecting %s device ...\n"
msgstr ""

#: ../libaratek/aratek_driver.c:140
#, c-format
msgid "Detect %s device error, error code: %d\n"
msgstr ""

#: ../libaratek/aratek_driver.c:142
#, c-format
msgid "No %s device detected\n"
msgstr ""

#: ../libaratek/aratek_driver.c:144
#, c-format
msgid "There is %d %s fingerprint device detected\n"
msgstr ""

#: ../libaratek/aratek_driver.c:262
#, c-format
msgid ""
"[%d/%d] fingerprint is being sampled. Please press different parts of your "
"finger"
msgstr ""

#: ../libaratek/aratek_driver.c:285
#, c-format
msgid "fingerprint has been registered. Please switch your finger"
msgstr ""

#: ../libaratek/aratek_driver.c:311
#, c-format
msgid "[%d/%d] Fingerprint repeat, please press different parts of finger"
msgstr ""

#: ../libaratek/aratek_driver.c:322
#, c-format
msgid ""
"Generate fingerprint characteristics error (%d), please lift your finger and "
"press it again"
msgstr ""

#: ../libaratek/aratek_driver.c:366
#, c-format
msgid "Generate fingerprint template error, error code: %d"
msgstr ""

#: ../libaratek/aratek_driver.c:444 ../libaratek/aratek_driver.c:522
#: ../libaratek/aratek_driver.c:604
#, c-format
msgid "Generate fingerprint characteristics error, error code: %d"
msgstr ""

#: ../libaratek/aratek_driver.c:713
#, c-format
msgid "Device %s[%d] received interrupt request\n"
msgstr ""

#: ../libaratek/aratek_driver.c:769
msgid "Please press your finger"
msgstr ""

#: ../libaratek/aratek_driver.c:772
msgid "Please lift your finger"
msgstr ""

#: ../libaratek/aratek_driver.c:775
msgid "Generating fingerprint characteristic data. Please wait..."
msgstr ""

#: ../libaratek/aratek_driver.c:778
msgid "Identifying fingerprint, please wait..."
msgstr ""

#: ../libaratek/aratek_internal.c:45
#, c-format
msgid "[%s] Security Level is not set. Use default values: %d\n"
msgstr ""

#: ../libaratek/aratek_internal.c:55
#, c-format
msgid "[%s] Fingerprint Quality Threshold is not set. Use default values: %d\n"
msgstr ""

#: ../libaratek/aratek_internal.c:62
#, c-format
msgid "[%s] Fingerprint Quality Threshold = %d\n"
msgstr ""

#: ../libaratek/aratek_internal.c:177
#, c-format
msgid "Analyze fingerprint image quality error, error code: %d"
msgstr ""

#: ../libaratek/aratek_internal.c:194
#, c-format
msgid ""
"The fingerprint image quality is poor[%d], please press your finger again\n"
msgstr ""

#: ../libaratek/aratek_internal.c:294
#, c-format
msgid "similarity = %d\n"
msgstr ""

#: ../libaratek/aratek_internal.c:304
#, c-format
msgid "Verify feature error, error code: %d"
msgstr ""

#: ../libaratek/aratek_internal.c:338
#, c-format
msgid "[%s] similarity = %d (UID:%d, INDEX[%d]:%s, SNO:%d)\n"
msgstr ""

#: ../id_tables.h:6
msgid "Aratek EM1600 Fingerprint Module"
msgstr ""

#: ../id_tables.h:9
msgid "Aratek EM1920 Fingerprint Module"
msgstr ""

#: ../id_tables.h:12
msgid "Aratek EM2010 Fingerprint Module"
msgstr ""

#: ../id_tables.h:15
msgid "Aratek EM3011 Fingerprint Module"
msgstr ""

#: ../id_tables.h:18
msgid "Aratek EM4010 Fingerprint Module"
msgstr ""

#: ../id_tables.h:21
msgid "Aratek EM03-3011 Fingerprint Module"
msgstr ""

#: ../id_tables.h:24
msgid "Aratek EM03-4010 Fingerprint Module"
msgstr ""

#: ../id_tables.h:27
msgid "Aratek A400 Fingerprint Module"
msgstr ""

#: ../id_tables.h:30
msgid "Aratek A600 Fingerprint Module"
msgstr ""

#: ../id_tables.h:33
msgid "Aratek CID4000 Fingerprint Module"
msgstr ""

#: ../id_tables.h:36
msgid "Aratek CID7000 Fingerprint Module"
msgstr ""

#: ../id_tables.h:39
msgid "Aratek FRT610 Fingerprint Module"
msgstr ""

#: ../id_tables.h:42
msgid "Aratek FPK402 Fingerprint Module"
msgstr ""

#: ../id_tables.h:45
msgid "Aratek trustfinger Module"
msgstr ""

#: ../id_tables.h:48
msgid "Aratek Fingerprint Module"
msgstr ""

#~ msgid "Generate fingerprint characteristics for the %d time\n"
#~ msgstr "第 %d 次生成指纹特征\n"

#~ msgid "[%s] Security Level = %d\n"
#~ msgstr "[%s] 安全等级 = %d\n"
