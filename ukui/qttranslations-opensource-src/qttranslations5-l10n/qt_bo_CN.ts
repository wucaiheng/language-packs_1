<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="../src/gui/kernel/qapplication.cpp" line="+2316"/>
        <source>Services</source>
        <translation>ཞབས་ཞུ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide %1</source>
        <translation>%1སྦས་སྐུང་བྱས་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide Others</source>
        <translation>གཞན་ལ་སྦས་སྐུང་བྱེད</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show All</source>
        <translation>ཚང་མ་མངོན་པར་བྱས་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Preferences...</source>
        <translation>གཟིགས་སྐྱོང་སྲིད་གླེང་།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit %1</source>
        <translation>%1ལས་གནས་ནས་ཕྱིར་འཐེན་བྱ་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>About %1</source>
        <translation>ཕལ་ཆེར་བརྒྱ་ཆ་1ཡས་མས་ཡིན།</translation>
    </message>
</context>
<context>
    <name>AudioOutput</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/audiooutput.cpp" line="+375"/>
        <source>&lt;html&gt;The audio playback device &lt;b&gt;%1&lt;/b&gt; does not work.&lt;br/&gt;Falling back to &lt;b&gt;%2&lt;/b&gt;.&lt;/html&gt;</source>
        <translation>&lt;html&gt; སྒྲ་ཕབ་ཕྱིར་ལྡོག་སྒྲིག་ཆས&lt;b&gt;%1&lt;/b&gt;བཀོལ་སྤྱོད་བྱེད་མི་ཐུབ། &lt;br/&gt; &lt;b&gt;%2&lt;/b&gt;ལ་ཕྱིར་ལོག་པ་རེད། &lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;Switching to the audio playback device &lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;which just became available and has higher preference.&lt;/html&gt;</source>
        <translation>&lt;html&gt; ད་སོ་མ་བཀོལ་སྤྱོད་བྱས་ཆོག་པ་མ་ཟད་དེ་བས་ཀྱང་དགའ་པོ་བྱེད་པའི་སྒྲ་ཕབ་ཕྱིར་ལྡོག་སྒྲིག་ཆས་&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;ལ་བརྗེ་སྤོར་བྱས་པ་རེད། &lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Revert back to device &apos;%1&apos;</source>
        <translation>སླར་ཡང་སྒྲིག་ཆས་&apos;%1&apos;ལ་ཕྱིར་ལོག་པ།</translation>
    </message>
</context>
<context>
    <name>CloseButton</name>
    <message>
        <location filename="../src/gui/widgets/qtabbar.cpp" line="+2251"/>
        <source>Close Tab</source>
        <translation>སྒོ་རྒྱག་པའི་རེའུ་མིག</translation>
    </message>
</context>
<context>
    <name>Phonon::</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/phononnamespace.cpp" line="+55"/>
        <source>Notifications</source>
        <translation>བརྡ་ཐོ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Music</source>
        <translation>རོལ་མོ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Video</source>
        <translation>བརྙན་ཕབ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Communication</source>
        <translation>འཕྲིན་གཏོང་།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Games</source>
        <translation>རོལ་རྩེད་</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Accessibility</source>
        <translation>ཞུགས་ཆོག་པའི་རང་བཞིན།</translation>
    </message>
</context>
<context>
    <name>Phonon::Gstreamer::Backend</name>
    <message>
        <location filename="../src/3rdparty/phonon/gstreamer/backend.cpp" line="+171"/>
        <source>Warning: You do not seem to have the package gstreamer0.10-plugins-good installed.
          Some video features have been disabled.</source>
        <translation>ཉེན་བརྡ། ཁྱེད་ཚོར་ཕལ་ཆེར་སྒྲིག་སྦྱོར་བྱས་པའི་ཐུམ་སྒྲིལ་gstreamer0.10-plugins-goodཡོད་པ་འདྲ།
          བརྙན་ཕབ་ཀྱི་ཁྱད་ཆོས་ཁ་ཤས་ལ་སྐྱོན་ཤོར་བ་རེད།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Warning: You do not seem to have the base GStreamer plugins installed.
          All audio and video support has been disabled</source>
        <translation>ཉེན་བརྡ། ཁྱེད་ཚོར་གཞི་རྩའི་GStreamer pluginསྒྲིག་སྦྱོར་བྱས་མེད་པ་འདྲ།
          སྒྲ་ཕབ་བརྙན་ཕབ་ཀྱི་རྒྱབ་སྐྱོར་ཚང་མ་དབང་པོ་སྐྱོན་ཅན་དུ་གྱུར་ཡོད།</translation>
    </message>
</context>
<context>
    <name>Phonon::Gstreamer::MediaObject</name>
    <message>
        <location filename="../src/3rdparty/phonon/gstreamer/mediaobject.cpp" line="+90"/>
        <source>Cannot start playback. 

Check your Gstreamer installation and make sure you 
have libgstreamer-plugins-base installed.</source>
        <translation>ཕྱིར་ལྡོག་བྱེད་འགོ་འཛུགས་མི་ཐུབ། 

ཁྱེད་ཀྱི་Gstreamerསྒྲིགས་སྦྱོར་ལ་ཞིབ་བཤེར་བྱས་ནས་ཁྱེད་ལ་ཁག་ཐེག་བྱེད 
ཚང་མར་དཔེ་མཛོད་ཁང་གི་ཆུ་རྒྱུན་གྱི་རྨང་གཞི་སྒྲིག་སྦྱོར་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>A required codec is missing. You need to install the following codec(s) to play this content: %0</source>
        <translation>དགོས་ངེས་ཀྱི་ཚབ་རྟགས་ཤིག་ཆད་འདུག ཁྱེད་ཚོས་གཤམ་གྱི་ཨང་ཀི་སྒྲིག་སྦྱོར་བྱས་ནས་ནང་དོན་འདི་འདོན་སྤེལ་བྱ་དགོས། %0</translation>
    </message>
    <message>
        <location line="+676"/>
        <location line="+8"/>
        <location line="+15"/>
        <location line="+9"/>
        <location line="+6"/>
        <location line="+19"/>
        <location line="+335"/>
        <location line="+24"/>
        <source>Could not open media source.</source>
        <translation>སྨྱན་སྦྱོར་གྱི་འབྱུང་ཁུངས་ཁ་ཕྱེ་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="-403"/>
        <source>Invalid source type.</source>
        <translation>གོ་མི་ཆོད་པའི་འབྱུང་ཁུངས་ཀྱི་རིགས་དབྱིབས་</translation>
    </message>
    <message>
        <location line="+377"/>
        <source>Could not locate media source.</source>
        <translation>སྨྱན་སྦྱོར་གྱི་འབྱུང་ཁུངས་གཏན་འཁེལ་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not open audio device. The device is already in use.</source>
        <translation>སྒྲ་ཕབ་སྒྲིག་ཆས་ཀྱི་ཁ་ཕྱེ་མི་ཐུབ། སྒྲིག་ཆས་འདི་བཀོལ་སྤྱོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Could not decode media source.</source>
        <translation>སྨྱན་སྦྱོར་གྱི་འབྱུང་ཁུངས་ལ་གསང་གྲངས་འགོད་མི་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>Phonon::VolumeSlider</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/volumeslider.cpp" line="+42"/>
        <location line="+18"/>
        <source>Volume: %1%</source>
        <translation>བོངས་ཚད: %1%</translation>
    </message>
    <message>
        <location line="-15"/>
        <location line="+18"/>
        <location line="+54"/>
        <source>Use this slider to adjust the volume. The leftmost position is 0%, the rightmost is %1%</source>
        <translation>འདི་བཀོལ་ནས་སྒྲ་གདངས་སྙོམས་སྒྲིག་བྱེད་དགོས། ཆེས་གཡོན་ཕྱོགས་ཀྱི་གནས་བབ་ནི་0%ཡིན་པ་དང་། ཆེས་གཡས་ཕྱོགས་པ་ནི་%1%ཡིན།</translation>
    </message>
</context>
<context>
    <name>Q3Accel</name>
    <message>
        <location filename="../src/qt3support/other/q3accel.cpp" line="+481"/>
        <source>%1, %2 not defined</source>
        <translation>%1,%2གནམ་བདག་གིས་གཏན་འཁེལ་བྱས་མེད།</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Ambiguous %1 not handled</source>
        <translation>ཐག་གཅོད་མ་བྱས་པའི་གསལ་ལ་མི་གསལ་བའི་%1</translation>
    </message>
</context>
<context>
    <name>Q3DataTable</name>
    <message>
        <location filename="../src/qt3support/sql/q3datatable.cpp" line="+285"/>
        <source>True</source>
        <translation>ངོ་མ་རེད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>False</source>
        <translation>རྫུན་བཟོ་བྱས་པ</translation>
    </message>
    <message>
        <location line="+505"/>
        <source>Insert</source>
        <translation>ནང་དུ་འཇུག་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
</context>
<context>
    <name>Q3FileDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="+864"/>
        <source>Copy or Move a File</source>
        <translation>འདྲ་བཤུས་སམ་ཡང་ན་ཡིག་ཆ་ཞིག་སྤོར་</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Read: %1</source>
        <translation>ཀློག་དེབ། %1</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+30"/>
        <source>Write: %1</source>
        <translation>བྲིས་པ། %1</translation>
    </message>
    <message>
        <location line="-22"/>
        <location line="+1575"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location line="-157"/>
        <location line="+49"/>
        <location line="+2153"/>
        <location filename="../src/qt3support/dialogs/q3filedialog_mac.cpp" line="+110"/>
        <source>All Files (*)</source>
        <translation>ཡིག་ཆ་ཡོད་ཚད།(*)</translation>
    </message>
    <message>
        <location line="-2089"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Size</source>
        <translation>ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <translation>རིགས་དབྱིབས་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>དུས་ཚོད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Attributes</source>
        <translation>ངོ་བོ་དང་གཟུགས་ཀྱི་ཁྱད</translation>
    </message>
    <message>
        <location line="+35"/>
        <location line="+2031"/>
        <source>&amp;OK</source>
        <translation>&amp; ཆོག</translation>
    </message>
    <message>
        <location line="-1991"/>
        <source>Look &amp;in:</source>
        <translation>ལྟོས་དང་།</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1981"/>
        <location line="+16"/>
        <source>File &amp;name:</source>
        <translation>ཡིག་ཆ ་དང་མིང་།</translation>
    </message>
    <message>
        <location line="-1996"/>
        <source>File &amp;type:</source>
        <translation>ཡིག་ཆ ་དང་རིགས་དབྱིབས་ནི།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Back</source>
        <translation>ཕྱིར་ལོག་པ།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>One directory up</source>
        <translation>དཀར་ཆག་གཅིག་གི་སྟེང་དུ་ཡོད།</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Create New Folder</source>
        <translation>ཡིག་སྣོད་གསར་པ་གཏོད་དགོས།</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>List View</source>
        <translation>མིང་ཐོའི་ལྟ་ཚུལ།</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Detail View</source>
        <translation>ཞིབ་ཕྲའི་ལྟ་ཚུལ།</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Preview File Info</source>
        <translation>སྔོན་སྦྱོང་ཡིག་ཆའི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Preview File Contents</source>
        <translation>སྔོན་སྦྱོང་ཡིག་ཆའི་ནང་དོན།</translation>
    </message>
    <message>
        <location line="+88"/>
        <source>Read-write</source>
        <translation>དཔེ་ཆ་ཀློག་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Read-only</source>
        <translation>ཀློག་འདོན་ཁོ་ན་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Write-only</source>
        <translation>རྩོམ་ཡིག་ཁོ་ན་འབྲི་བ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Inaccessible</source>
        <translation>འགྲོ་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Symlink to File</source>
        <translation>ཡིག་ཆ་དང་འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Symlink to Directory</source>
        <translation>དཀར་ཆག་དང་འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Symlink to Special</source>
        <translation>དམིགས་བསལ་ཅན་ལ་འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>File</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dir</source>
        <translation>ཏའེ་ཨར།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Special</source>
        <translation>དམིགས་བསལ་ཅན།</translation>
    </message>
    <message>
        <location line="+704"/>
        <location line="+2100"/>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="+337"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location line="-1990"/>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="+84"/>
        <source>Save As</source>
        <translation>གྲོན་ཆུང་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+642"/>
        <location line="+5"/>
        <location line="+355"/>
        <source>&amp;Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location line="-357"/>
        <location line="+341"/>
        <source>&amp;Save</source>
        <translation>&amp;ཉར་ཚགས་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="-334"/>
        <source>&amp;Rename</source>
        <translation>&amp;མིང་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Delete</source>
        <translation>&amp;བསུབ་པ།</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>R&amp;eload</source>
        <translation>R&amp;eload</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Sort by &amp;Name</source>
        <translation>མིང་གཡར་ནས་རིགས་འབྱེད་པ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sort by &amp;Size</source>
        <translation>ཆེ་ཆུང་ལྟར་རིགས་འབྱེད་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sort by &amp;Date</source>
        <translation>དུས་ཚོད་ལྟར་ལེགས་སྒྲིག་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Unsorted</source>
        <translation>་་་</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Sort</source>
        <translation>རིགས་འབྱེད་པ།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Show &amp;hidden files</source>
        <translation>སྦས་སྐུང་བྱས་པའི་ཡིག་ཆ་མངོན</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>the file</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>the directory</source>
        <translation>དཀར་ཆག</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>the symlink</source>
        <translation>མཉམ་འབྲེལ་རྒྱལ་ཚོགས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete %1</source>
        <translation>%1 བསུབ་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;qt&gt;Are you sure you wish to delete %1 &quot;%2&quot;?&lt;/qt&gt;</source>
        <translation>&lt;qt&gt; ཁྱོད་ཀྱིས་དངོས་གནས་%1&quot;%2&quot;བསུབ་འདོད་དམ། &lt;/qt&gt;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Yes</source>
        <translation>&amp;རེད།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;No</source>
        <translation>&gt; མི་ཆོག</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>New Folder 1</source>
        <translation>ཡིག་སྣོད་གསར་པ་1</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New Folder</source>
        <translation>ཡིག་སྣོད་གསར་པ།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New Folder %1</source>
        <translation>གསར་དུ་བལ་བའི་ཡིག་སྣོད་%1</translation>
    </message>
    <message>
        <location line="+98"/>
        <source>Find Directory</source>
        <translation>དཀར་ཆག་འཚོལ་བ།</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+108"/>
        <source>Directories</source>
        <translation>དཀར་ཆག</translation>
    </message>
    <message>
        <location line="-2"/>
        <source>Directory:</source>
        <translation>དཀར་ཆག་ནི།</translation>
    </message>
    <message>
        <location line="+40"/>
        <location line="+1110"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location line="-1109"/>
        <source>%1
File not found.
Check path and filename.</source>
        <translation>%1
ཡིག་ཆ་མ་རྙེད་པ།
ལམ་ཕྲན་དང་ཡིག་ཆའི་མིང་ལ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="-289"/>
        <source>All Files (*.*)</source>
        <translation>ཡིག་ཆ་ཡོད་ཚད་(*.* )</translation>
    </message>
    <message>
        <location line="+375"/>
        <source>Open </source>
        <translation>སྒོ་ཕྱེ་བ། </translation>
    </message>
    <message>
        <location line="+155"/>
        <source>Select a Directory</source>
        <translation>དཀར་ཆག་ཅིག་བདམས་པ།</translation>
    </message>
</context>
<context>
    <name>Q3LocalFs</name>
    <message>
        <location filename="../src/qt3support/network/q3localfs.cpp" line="+130"/>
        <location line="+10"/>
        <source>Could not read directory
%1</source>
        <translation>དཀར་ཆག་ཀློག་མི་ཐུབ་པ།
%1</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Could not create directory
%1</source>
        <translation>དཀར་ཆག་གསར་སྐྲུན་བྱེད་མི་ཐུབ་པ།
%1</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Could not remove file or directory
%1</source>
        <translation>ཡིག་ཆ་དང་དཀར་ཆག་མེད་པར་བཟོ་མི་ཐུབ།
%1</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Could not rename
%1
to
%2</source>
        <translation>མིང་བསྒྱུར་མི་ཐུབ་པ།
%1
དེ་ལྟར་བྱས་ན་
%2</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Could not open
%1</source>
        <translation>ཁ་ཕྱེ་མི་ཐུབ་པ།
%1</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Could not write
%1</source>
        <translation>ཡི་གེ་འབྲི་མི་ཐུབ་པ།
%1</translation>
    </message>
</context>
<context>
    <name>Q3MainWindow</name>
    <message>
        <location filename="../src/qt3support/widgets/q3mainwindow.cpp" line="+2051"/>
        <source>Line up</source>
        <translation>གྲལ་བསྒྲིགས་ནས་གྲལ་བསྒྲིགས</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Customize...</source>
        <translation>ཡུལ་སྲོལ་གོམས་གཤིས་</translation>
    </message>
</context>
<context>
    <name>Q3NetworkProtocol</name>
    <message>
        <location filename="../src/qt3support/network/q3networkprotocol.cpp" line="+854"/>
        <source>Operation stopped by the user</source>
        <translation>སྤྱོད་མཁན་གྱིས་བཀག་འགོག་བྱས་པའི་བཀོལ་སྤྱོད།</translation>
    </message>
</context>
<context>
    <name>Q3ProgressDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3progressdialog.cpp" line="+224"/>
        <location line="+61"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
</context>
<context>
    <name>Q3TabDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3tabdialog.cpp" line="+189"/>
        <location line="+814"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location line="-356"/>
        <source>Apply</source>
        <translation>རེ་ཞུ་</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Defaults</source>
        <translation>ཁ་ཆད་དང་འགལ་བའི་</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
</context>
<context>
    <name>Q3TextEdit</name>
    <message>
        <location filename="../src/qt3support/text/q3textedit.cpp" line="+5429"/>
        <source>&amp;Undo</source>
        <translation>་་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Redo</source>
        <translation>ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་བསྐྱར་དུ་</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cu&amp;t</source>
        <translation>Cu&gt;t</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Copy</source>
        <translation>&amp; འདྲ་བཤུས།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Paste</source>
        <translation>&amp; paste</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation>གསལ་པོར་བཤད་ན།</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+2"/>
        <source>Select All</source>
        <translation>ཚང་མ་བདམས་པ་</translation>
    </message>
</context>
<context>
    <name>Q3TitleBar</name>
    <message>
        <location filename="../src/plugins/accessible/compat/q3complexwidgets.cpp" line="+246"/>
        <source>System</source>
        <translation>ལམ་ལུགས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restore up</source>
        <translation>སླར་གསོ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Minimize</source>
        <translation>ཉུང་དུ་གཏོང་གང་ཐུབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restore down</source>
        <translation>སླར་གསོ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Maximize</source>
        <translation>ཚད་གཞི་མཐོ་ཤོས་ཀྱི་སྒོ་ནས</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Contains commands to manipulate the window</source>
        <translation>དེའི་ནང་དུ་སྒེའུ་ཁུང་གི་ཁ་ལོ་སྒྱུར་བའི་བཀའ་རྒྱ་འདུས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Puts a minimized back to normal</source>
        <translation>ཚད་གཞི་ཆུང་དུ་བཏང་ནས་རྒྱུན་ལྡན་དུ་སླར་གསོ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Moves the window out of the way</source>
        <translation>སྒེའུ་ཁུང་ཕྱི་ལ་སྤོར་བ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Puts a maximized window back to normal</source>
        <translation>ཚད་གཞི་མཐོ་ཤོས་ཀྱི་སྒེའུ་ཁུང་དེ་རྒྱུན་ལྡན་ལྟར་སླར་གསོ་བྱ་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Makes the window full screen</source>
        <translation>སྒེའུ་ཁུང་གི་བརྙན་ཤེལ་ཧྲིལ་བོ་བཟོ་བ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Closes the window</source>
        <translation>སྒེའུ་ཁུང་གི་སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Displays the name of the window and contains controls to manipulate it</source>
        <translation>སྒེའུ་ཁུང་གི་མིང་མངོན་པ་མ་ཟད། དེའི་ནང་དུ་ཚོད་འཛིན་བྱས་ནས་ཁ་ལོ་སྒྱུར་བ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>Q3ToolBar</name>
    <message>
        <location filename="../src/qt3support/widgets/q3toolbar.cpp" line="+692"/>
        <source>More...</source>
        <translation>དེ་བས་ཀྱང་མང་བ་རེད</translation>
    </message>
</context>
<context>
    <name>Q3UrlOperator</name>
    <message>
        <location filename="../src/qt3support/network/q3urloperator.cpp" line="+386"/>
        <location line="+260"/>
        <location line="+4"/>
        <source>The protocol `%1&apos; is not supported</source>
        <translation>གྲོས་མཐུན་&apos;%1&apos;ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location line="-260"/>
        <source>The protocol `%1&apos; does not support listing directories</source>
        <translation>གྲོས་མཐུན་&apos;%1&apos;ཡིས་ཚོང་རར་བཏོན་པའི་དཀར་ཆག་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support creating new directories</source>
        <translation>གྲོས་མཐུན་&apos;%1&apos;ཡིས་དཀར་ཆག་གསར་པ་གཏོད་རྒྱུར་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support removing files or directories</source>
        <translation>གྲོས་མཐུན་&apos;%1&apos;ཡིས་ཡིག་ཆ་དང་དཀར་ཆག་མེད་པར་བཟོ་བར་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support renaming files or directories</source>
        <translation>གྲོས་མཐུན་&apos;%1&apos;ཡིས་ཡིག་ཆ་དང་དཀར་ཆག་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support getting files</source>
        <translation>གྲོས་མཐུན་&apos;%1&apos;ཡིས་ཡིག་ཆ་ཐོབ་པར་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support putting files</source>
        <translation>གྲོས་མཐུན་&apos;%1&apos;ཡིས་ཡིག་ཆ་འཇོག་པར་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location line="+243"/>
        <location line="+4"/>
        <source>The protocol `%1&apos; does not support copying or moving files or directories</source>
        <translation>གྲོས་མཐུན་&apos;%1&apos;ཡིས་ཡིག་ཆ་དང་དཀར་ཆག་འདྲ་བཟོ་དང་སྤོ་འགུལ་བྱེད་པར་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location line="+237"/>
        <location line="+1"/>
        <source>(unknown)</source>
        <translation>(ཤེས་མེད་པ)</translation>
    </message>
</context>
<context>
    <name>Q3Wizard</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3wizard.cpp" line="+177"/>
        <source>&amp;Cancel</source>
        <translation>&gt; མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt; &amp;Back</source>
        <translation>&lt;ཕྱིར་འཐེན་བྱས་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Next &gt;</source>
        <translation>&amp;&gt;་རྗེས་མ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Finish</source>
        <translation>མཇུག་སྒྲིལ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Help</source>
        <translation>རོགས་རམ་བྱེད་དགོས།</translation>
    </message>
</context>
<context>
    <name>QAbstractSocket</name>
    <message>
        <location filename="../src/network/socket/qabstractsocket.cpp" line="+868"/>
        <location filename="../src/network/socket/qhttpsocketengine.cpp" line="+615"/>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="+657"/>
        <location line="+26"/>
        <source>Host not found</source>
        <translation>བདག་པོས་མ་རྙེད་པ།</translation>
    </message>
    <message>
        <location line="+50"/>
        <location filename="../src/network/socket/qhttpsocketengine.cpp" line="+3"/>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="+4"/>
        <source>Connection refused</source>
        <translation>འབྲེལ་མཐུད་དང་ལེན་མ་བྱས་</translation>
    </message>
    <message>
        <location line="+141"/>
        <source>Connection timed out</source>
        <translation>དུས་ཐོག་ཏུ་འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="-547"/>
        <location line="+787"/>
        <location line="+208"/>
        <source>Operation on socket is not supported</source>
        <translation>བསྒར་ཁུང་སྟེང་གི་བྱ་སྤྱོད་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Socket operation timed out</source>
        <translation>བསྒར་ཁུང་གི་གཤག་བཅོས་དུས་ཐོག་ཏུ་ཕྱིར་འཐེན་བྱས</translation>
    </message>
    <message>
        <location line="+380"/>
        <source>Socket is not connected</source>
        <translation>བསྒར་ཁུང་ལ་འབྲེལ་མཐུད་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="-8"/>
        <source>Network unreachable</source>
        <translation>དྲ་རྒྱ་མངོན་འགྱུར་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
</context>
<context>
    <name>QAbstractSpinBox</name>
    <message>
        <location filename="../src/gui/widgets/qabstractspinbox.cpp" line="+1199"/>
        <source>&amp;Step up</source>
        <translation>&gt; གོམ་སྟབས་ཇེ་མགྱོགས་སུ་</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Step &amp;down</source>
        <translation>གོམ་པ་རིམ་བགྲོད་ཀྱིས་གོམ་པ</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>&amp;Select All</source>
        <translation>&gt; ཚང་མ་བདམས་པ</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/gui/accessible/qaccessibleobject.cpp" line="+376"/>
        <source>Activate</source>
        <translation>སྐུལ་སློང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.h" line="+352"/>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation>ལག་བསྟར་བྱེད་ཆོག་པའི་&apos;%1&apos;ལ་Qt %2,Qt%3རྙེད་པ་རེད།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Incompatible Qt Library Error</source>
        <translation>ཕན་ཚུན་མཐུན་ཐབས་མེད་པའི་Qt དཔེ་མཛོད་ཁང་གི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../src/gui/accessible/qaccessibleobject.cpp" line="+2"/>
        <source>Activates the program&apos;s main window</source>
        <translation>གོ་རིམ་གྱི་སྒེའུ་ཁུང་གཙོ་བོ་སྐུལ་སློང་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>QAxSelect</name>
    <message>
        <location filename="../src/activeqt/container/qaxselect.ui"/>
        <source>Select ActiveX Control</source>
        <translation>འགུལ་སྐྱོད་Xཡི་ཚོད་འཛིན་གདམ་གསེས་</translation>
    </message>
    <message>
        <location/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Cancel</source>
        <translation>&gt; མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location/>
        <source>COM &amp;Object:</source>
        <translation>COM &gt; Object:</translation>
    </message>
</context>
<context>
    <name>QCheckBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="+114"/>
        <source>Uncheck</source>
        <translation>ཞིབ་བཤེར་མ་བྱས་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Check</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Toggle</source>
        <translation>འཁྱུད་པ།</translation>
    </message>
</context>
<context>
    <name>QColorDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="+1253"/>
        <source>Hu&amp;e:</source>
        <translation>ཧུའུ་ཨའེ་ཡིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Sat:</source>
        <translation>&gt; གནས་ཚུལ་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Val:</source>
        <translation>&amp;Val:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Red:</source>
        <translation>&amp;དམར་པོ་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Green:</source>
        <translation>&gt; ལྗང་མདོག་ནི།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bl&amp;ue:</source>
        <translation>པུ་ལུ་ཐེ་ཡིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A&amp;lpha channel:</source>
        <translation>A&amp;lphaའཕྲིན་ལམ་ནི།</translation>
    </message>
    <message>
        <location line="+101"/>
        <source>Select Color</source>
        <translation>ཁ་དོག་གདམ་གསེས་བྱེད་</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>&amp;Basic colors</source>
        <translation>གཞི་རྩའི་ཁ་དོག</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Custom colors</source>
        <translation>»ཡུལ་སྲོལ་གོམས་གཤིས་ཀྱི་ཁ་དོག</translation>
    </message>
    <message>
        <source>Cursor at %1, %2
Press ESC to cancel</source>
        <translation>%1, %2
ཨེ་ཤེ་ཡ་དང་ཞི་བདེ་རྒྱ་རྒྱ་ཆེ་མོ་ཁུལ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Add to Custom Colors</source>
        <translation>»ཡུལ་སྲོལ་གོམས་གཤིས་ཀྱི་ཁ་དོག་ཁ་སྣོན་བྱས་</translation>
    </message>
    <message>
        <source>Select color</source>
        <translation type="obsolete">选择颜色</translation>
    </message>
    <message>
        <source>&amp;Pick Screen Color</source>
        <translation>»བརྙན་ཤེལ་གྱི་ཁ་དོག་འདེམས་པ།</translation>
    </message>
</context>
<context>
    <name>QComboBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="+1771"/>
        <location line="+65"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qitemeditorfactory.cpp" line="+544"/>
        <source>False</source>
        <translation>རྫུན་བཟོ་བྱས་པ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>True</source>
        <translation>ངོ་མ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="+0"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <source>%1: permission denied</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：权限被拒绝</translation>
    </message>
    <message>
        <source>%1: already exists</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：已经存在</translation>
    </message>
    <message>
        <source>%1: doesn&apos;t exists</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：不存在</translation>
    </message>
    <message>
        <source>%1: out of resources</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：资源耗尽了</translation>
    </message>
    <message>
        <source>%1: unknown error %2</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：未知错误 %2</translation>
    </message>
    <message>
        <location filename="../src/corelib/kernel/qsystemsemaphore_unix.cpp" line="+119"/>
        <source>%1: key is empty</source>
        <comment>QSystemSemaphore</comment>
        <translation>%1:ལྡེ་མིག་ནི་སྟོང་བ་ཡིན།</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1: unable to make key</source>
        <comment>QSystemSemaphore</comment>
        <translation>%1:ལྡེ་མིག་བཟོ་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1: ftok failed</source>
        <comment>QSystemSemaphore</comment>
        <translation>%1:ftokལ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>QDB2Driver</name>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="+1276"/>
        <source>Unable to connect</source>
        <translation>འབྲེལ་མཐུད་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+303"/>
        <source>Unable to commit transaction</source>
        <translation>ཉོ་ཚོང་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback transaction</source>
        <translation>ཕྱིར་ལྡོག་བྱེད་ཐབས་བྲལ་བའི་ཉོ་ཚོང་།</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to set autocommit</source>
        <translation>རང་འགུལ་གྱིས་ཚོང་ཟོག་ཉོ་མི་ཐུབ་པ།</translation>
    </message>
</context>
<context>
    <name>QDB2Result</name>
    <message>
        <location line="-1043"/>
        <location line="+243"/>
        <source>Unable to execute statement</source>
        <translation>གསལ་བསྒྲགས་ལག་བསྟར་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="-206"/>
        <source>Unable to prepare statement</source>
        <translation>གསལ་བསྒྲགས་གྲ་སྒྲིག་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+196"/>
        <source>Unable to bind variable</source>
        <translation>ཚོད་འཛིན་བྱེད་ཐབས་བྲལ་བའི་འགྱུར་ལྡོག</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Unable to fetch record %1</source>
        <translation>ཟིན་ཐོ་འགོད་ཐབས་བྲལ་བའི་ཟིན་ཐོ་ཨང་གྲངས་1ཟིན་པ།</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to fetch next</source>
        <translation>གོམ་སྟབས་རྗེས་མར་ལེན་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to fetch first</source>
        <translation>སྔོན་ལ་ལེན་མི་ཐུབ་པ།</translation>
    </message>
</context>
<context>
    <name>QDateTimeEdit</name>
    <message>
        <location filename="../src/gui/widgets/qdatetimeedit.cpp" line="+2295"/>
        <source>AM</source>
        <translation>AM</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>am</source>
        <translation>am am</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>PM</source>
        <translation>PM</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>pm</source>
        <translation>pm</translation>
    </message>
</context>
<context>
    <name>QDial</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+951"/>
        <source>QDial</source>
        <translation>QDial</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>SpeedoMeter</source>
        <translation>མྱུར་ཚད་འཇལ་ཆས།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>SliderHandle</source>
        <translation>སྲ་གཟུགས་ལག་ཤེས་འཕྲུལ་ཆས།</translation>
    </message>
</context>
<context>
    <name>QDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qdialog.cpp" line="+597"/>
        <source>What&apos;s This?</source>
        <translation>དེ་གང་རེད་དམ།?</translation>
    </message>
    <message>
        <location line="-115"/>
        <source>Done</source>
        <translation>བསྒྲུབས་ཚར།</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.cpp" line="+1866"/>
        <location line="+464"/>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="+561"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="+3"/>
        <source>Save</source>
        <translation>གྲོན་ཆུང་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Save</source>
        <translation>&amp;ཉར་ཚགས་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Cancel</source>
        <translation>&gt; མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Apply</source>
        <translation>རེ་ཞུ་</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reset</source>
        <translation>བསྐྱར་དུ་བཀོད་སྒྲིག་བྱེད་པ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Don&apos;t Save</source>
        <translation>སྐྱོབ་མི་རུང་།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Discard</source>
        <translation>བེད་མེད་དུ་བསྐྱུར་བ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Yes</source>
        <translation>&amp;རེད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Yes to &amp;All</source>
        <translation>ཚང་མ་ཁས་བླངས་པ་རེད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;No</source>
        <translation>&gt; མི་ཆོག</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>N&amp;o to All</source>
        <translation>N&amp;oཡིས་མི་ཚང་མར་སྤྲད་པ་རེད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save All</source>
        <translation>ཚང་མ་ཉར་ཚགས་ཡག་པོ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abort</source>
        <translation>མངལ་ཤོར་དུ་འཇུག་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Retry</source>
        <translation>བསྐྱར་དུ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ignore</source>
        <translation>སྣང་མེད་དུ་བཞག་</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restore Defaults</source>
        <translation>སྔོན་ཆད་ཀྱི་གནས་ཚུལ་སླར་གསོ་</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Close without Saving</source>
        <translation>གྲོན་ཆུང་མ་བྱས་པར་སྒོ་རྒྱག་</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>&amp;OK</source>
        <translation>&amp; ཆོག</translation>
    </message>
</context>
<context>
    <name>QDirModel</name>
    <message>
        <location filename="../src/gui/itemviews/qdirmodel.cpp" line="+453"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Size</source>
        <translation>ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation>བྱམས་བརྩེའི་ངང་</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation>རིགས་དབྱིབས་</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Date Modified</source>
        <translation>བཟོ་བཅོས་བརྒྱབ་པའི་དུས་ཚོད།</translation>
    </message>
</context>
<context>
    <name>QDockWidget</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/qaccessiblewidgets.cpp" line="+1239"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dock</source>
        <translation>གྲུ་ཁ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Float</source>
        <translation>ཆུ་ངོས་སུ་གཡེང་</translation>
    </message>
</context>
<context>
    <name>QDoubleSpinBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="-537"/>
        <source>More</source>
        <translation>དེ་བས་ཀྱང་མང་</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Less</source>
        <translation>ཉུང་ཙམ་ལས་མེད་</translation>
    </message>
</context>
<context>
    <name>QErrorMessage</name>
    <message>
        <location filename="../src/gui/dialogs/qerrormessage.cpp" line="+192"/>
        <source>Debug Message:</source>
        <translation>ཚོད་ལྟ་བྱས་པའི་ཆ་འཕྲིན་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning:</source>
        <translation>ཐ་ཚིག་སྒྲོག་རྒྱུར།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Fatal Error:</source>
        <translation>སྲོག་ལ་ཐུག་པའི་ནོར་འཁྲུལ་</translation>
    </message>
    <message>
        <location line="+193"/>
        <source>&amp;Show this message again</source>
        <translation>ཡང་བསྐྱར་ཆ་འཕྲིན་འདི་སྟོན་རྒྱུ་ཡིན།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;OK</source>
        <translation>&amp; ཆོག</translation>
    </message>
</context>
<context>
    <name>QFile</name>
    <message>
        <location filename="../src/corelib/io/qfile.cpp" line="+708"/>
        <location line="+141"/>
        <source>Destination file exists</source>
        <translation>དམིགས་ཡུལ་གྱི་ཡིག་ཆ་གནས་ཡོད།</translation>
    </message>
    <message>
        <location line="-108"/>
        <source>Cannot remove source file</source>
        <translation>འབྱུང་ཁུངས་ཀྱི་ཡིག་ཆ་མེད་པར་བཟོ་མི་</translation>
    </message>
    <message>
        <location line="+120"/>
        <source>Cannot open %1 for input</source>
        <translation>མ་དངུལ་འཇོག་པའི་ཆེད་དུ་%1བ་ཕྱེ་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Cannot open for output</source>
        <translation>ཐོན་ཚད་ལ་སྒོ་འབྱེད་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Failure to write block</source>
        <translation>ཡི་གེ་འབྲི་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot create %1 for output</source>
        <translation>ཐོན་ཚད་ལ་%1གསར་སྐྲུན་བྱེད་མི་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>QFileDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="+515"/>
        <location line="+444"/>
        <source>All Files (*)</source>
        <translation>ཡིག་ཆ་ཡོད་ཚད།(*)</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>Directories</source>
        <translation>དཀར་ཆག</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+50"/>
        <location line="+1471"/>
        <location line="+75"/>
        <source>&amp;Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location line="-1596"/>
        <location line="+50"/>
        <source>&amp;Save</source>
        <translation>&amp;ཉར་ཚགས་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="-730"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location line="+1515"/>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation>%1གནས་ཡོད།
ཁྱོད་ཀྱིས་དེའི་ཚབ་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>%1
File not found.
Please verify the correct file name was given.</source>
        <translation>%1
ཡིག་ཆ་མ་རྙེད་པ།
ཁྱེད་ཀྱིས་ཡང་དག་པའི་ཡིག་ཆའི་མིང་ལ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qdirmodel.cpp" line="+402"/>
        <source>My Computer</source>
        <translation>ངའི་གློག་ཀླད་</translation>
    </message>
    <message>
        <source>%1 File</source>
        <extracomment>%1 is a file name suffix, for example txt</extracomment>
        <translation>%1 ཡིག་ཆ།</translation>
    </message>
    <message>
        <source>%1 KiB</source>
        <translation>%1 KiB</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="-1504"/>
        <source>&amp;Rename</source>
        <translation>&amp;མིང་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Delete</source>
        <translation>&amp;བསུབ་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show &amp;hidden files</source>
        <translation>སྦས་སྐུང་བྱས་པའི་ཡིག་ཆ་མངོན</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.ui"/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Back</source>
        <translation>ཕྱིར་ལོག་པ།</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Parent Directory</source>
        <translation>ཁྱིམ་བདག་གི་དཀར་ཆག</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>List View</source>
        <translation>མིང་ཐོའི་ལྟ་ཚུལ།</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Detail View</source>
        <translation>ཞིབ་ཕྲའི་ལྟ་ཚུལ།</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Files of type:</source>
        <translation>རིགས་དབྱིབས་ཀྱི་ཡིག་ཆ་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="+6"/>
        <location line="+648"/>
        <source>Directory:</source>
        <translation>དཀར་ཆག་ནི།</translation>
    </message>
    <message>
        <location line="+794"/>
        <location line="+862"/>
        <source>%1
Directory not found.
Please verify the correct directory name was given.</source>
        <translation>%1
དཀར་ཆག་མ་རྙེད་པ།
ཡང་དག་པའི་དཀར་ཆག་གི་མིང་ལ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location line="-218"/>
        <source>&apos;%1&apos; is write protected.
Do you want to delete it anyway?</source>
        <translation>&apos;%1&apos;ལ་སྲུང་སྐྱོབ་ཐོབ་པའི་ཡི་གེ་འབྲི་དགོས།
གང་ལྟར་ཁྱོད་ཀྱིས་དེ་བསུབ་དགོས་སམ།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Are sure you want to delete &apos;%1&apos;?</source>
        <translation>ཁྱོད་ཀྱིས་&quot;%1&quot;ཞེས་པ་དེ་བསུབ་འདོད་པ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Are you sure you want to delete &apos;%1&apos;?</source>
        <translation>ཁྱོད་ཀྱིས་&quot;%1&quot;ཞེས་པ་དེ་བསུབ་དགོས་པ་གཏན་འཁེལ་བྱེད་ཐུབ་བམ།</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <source>Folder</source>
        <translation>ཡིག་སྣོད་</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Could not delete directory.</source>
        <translation>དཀར་ཆག་བསུབ་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+407"/>
        <source>Recent Places</source>
        <translation>ཉེ་དུས་ཀྱི་ས་ཆ།</translation>
    </message>
    <message>
        <location line="-2550"/>
        <source>Save As</source>
        <translation>གྲོན་ཆུང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qfileiconprovider.cpp" line="+411"/>
        <source>Drive</source>
        <translation>སྒུལ་ཤུགས་</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+1"/>
        <source>File</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Unknown</source>
        <translation>ཤེས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="-4"/>
        <source>Find Directory</source>
        <translation>དཀར་ཆག་འཚོལ་བ།</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Show </source>
        <translation>འགྲེམས་སྟོན། </translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.ui"/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Forward</source>
        <translation>མདུན་དུ་སྐྱོད་པ</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="+1970"/>
        <source>New Folder</source>
        <translation>ཡིག་སྣོད་གསར་པ།</translation>
    </message>
    <message>
        <location line="-1963"/>
        <source>&amp;New Folder</source>
        <translation>&amp;གསར་དུ་བ ཡིག་སྣོད་</translation>
    </message>
    <message>
        <location line="+656"/>
        <location line="+38"/>
        <source>&amp;Choose</source>
        <translation>»གདམ་གསེས་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qsidebar.cpp" line="+418"/>
        <source>Remove</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="-687"/>
        <location line="+652"/>
        <source>File &amp;name:</source>
        <translation>ཡིག་ཆ ་དང་མིང་།</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.ui"/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Look in:</source>
        <translation>ནང་དུ་ལྟོས་དང་།</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Create New Folder</source>
        <translation>ཡིག་སྣོད་གསར་པ་གཏོད་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog_win.cpp" line="+160"/>
        <source>All Files (*.*)</source>
        <translation>ཡིག་ཆ་ཡོད་ཚད་(*.* )</translation>
    </message>
</context>
<context>
    <name>QFileSystemModel</name>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel.cpp" line="+744"/>
        <source>%1 TB</source>
        <translation>%1 TB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 bytes</source>
        <translation>%1 字节</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Invalid filename</source>
        <translation>གོ་མི་ཆོད་པའི་ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;b&gt;The name &quot;%1&quot; can not be used.&lt;/b&gt;&lt;p&gt;Try using another name, with fewer characters or no punctuations marks.</source>
        <translation>&lt;b&gt;&quot;%1&quot;ཞེས་པའི་མིང་འདི་བཀོལ་སྤྱོད་བྱེད་མི་ཐུབ།&lt;/b&gt; &lt;p&gt;མིང་གཞན་ཞིག་བཀོལ་ནས་ཚོད་ལྟ་ཞིག་བྱས་ན་ཡི་གེ་ཉུང་བའམ་ཡང་ན་ཚེག་རྡར་བྱས་མེད་པའི་རྟགས་མཚན་ཡོད།</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Size</source>
        <translation>ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation>བྱམས་བརྩེའི་ངང་</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation>རིགས་དབྱིབས་</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Date Modified</source>
        <translation>བཟོ་བཅོས་བརྒྱབ་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel_p.h" line="+234"/>
        <source>My Computer</source>
        <translation>ངའི་གློག་ཀླད་</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Computer</source>
        <translation>རྩིས་འཁོར།</translation>
    </message>
    <message>
        <source>%1 KiB</source>
        <translation>%1 KiB</translation>
    </message>
</context>
<context>
    <name>QFontDatabase</name>
    <message>
        <location filename="../src/gui/text/qfontdatabase.cpp" line="+90"/>
        <location line="+1176"/>
        <source>Normal</source>
        <translation>རྒྱུན་ལྡན་གྱི་གནས་</translation>
    </message>
    <message>
        <location line="-1173"/>
        <location line="+12"/>
        <location line="+1149"/>
        <source>Bold</source>
        <translation>སྤོབས་པ་ཆེ་བ།</translation>
    </message>
    <message>
        <location line="-1158"/>
        <location line="+1160"/>
        <source>Demi Bold</source>
        <translation>ཏེ་མའེ་པའོ་ཐི།</translation>
    </message>
    <message>
        <location line="-1157"/>
        <location line="+18"/>
        <location line="+1135"/>
        <source>Black</source>
        <translation>ནག་པོ།</translation>
    </message>
    <message>
        <location line="-1145"/>
        <source>Demi</source>
        <translation>ཏེ་མའེ།</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+1145"/>
        <source>Light</source>
        <translation>འོད་སྣང་།</translation>
    </message>
    <message>
        <location line="-1004"/>
        <location line="+1007"/>
        <source>Italic</source>
        <translation>དབྱི་སི་ལན་ཆོས་ཁང་།</translation>
    </message>
    <message>
        <location line="-1004"/>
        <location line="+1006"/>
        <source>Oblique</source>
        <translation>གསེག་པ།</translation>
    </message>
    <message>
        <location line="+705"/>
        <source>Any</source>
        <translation>གང་ཞིག་ཡིན་རུང་</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Latin</source>
        <translation>ལ་ཏིང་སྐད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Greek</source>
        <translation>གྷི་རིག</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cyrillic</source>
        <translation>ཁེ་ལའེ་ལི་ཆི།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Armenian</source>
        <translation>ཨར་མེ་ནི་ཡའི་སྐད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hebrew</source>
        <translation>ཧིབ་བྷུ་རུ་སྐད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Arabic</source>
        <translation>ཨ་རབ་བྷི་ཡའི་སྐད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Syriac</source>
        <translation>སི་རི་ཡ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Thaana</source>
        <translation>ས་ན་ཡ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Devanagari</source>
        <translation>ཏེ་ཝན་ན་ཀ་ལི།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Bengali</source>
        <translation>བྷེང་གྷ་ལིའི་སྐད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gurmukhi</source>
        <translation>ཀུའུ་མུའུ་ཆི།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gujarati</source>
        <translation>གུ་ཇུ་ར་ཐིའི་སྐད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Oriya</source>
        <translation>ཨོ་ལི་ཡ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Tamil</source>
        <translation>ཏ་མིལ་སྐད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Telugu</source>
        <translation>ཏེལ་ལི་གུ་སྐད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Kannada</source>
        <translation>ཀ་ན་ཌ་སྐད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Malayalam</source>
        <translation>མ་ལེ་ཡ་ལམ་སྐད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sinhala</source>
        <translation>ཞིན་ཧ་ལ་ཡིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Thai</source>
        <translation>ཐཡེ་ལྣེཌ་ཡི་སྐད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Lao</source>
        <translation>ལའོ་ཨའོ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Tibetan</source>
        <translation>བོད་ཡིག</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Myanmar</source>
        <translation>འབར་མ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Georgian</source>
        <translation>ཇོར་ཡི་ཡའི་སྐད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Khmer</source>
        <translation>ཁའེ་མའེ་ཨར།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Simplified Chinese</source>
        <translation>རྒྱ་ཡིག་སྟབས་བདེ་རུ་གཏོང་བ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Traditional Chinese</source>
        <translation>སྲོལ་རྒྱུན་གྱི་རྒྱ་ཡིག</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Japanese</source>
        <translation>ཉི་ཧོང་སྐད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Korean</source>
        <translation>ཀོ་རི་ཡའི་མི།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Vietnamese</source>
        <translation>ཝེ་ཐི་ནམ་སྐད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Symbol</source>
        <translation>མཚོན་རྟགས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ogham</source>
        <translation>ཨའོ་ཀེ་ཧན།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Runic</source>
        <translation>བྲོས་བྱོལ་དུ་སོང་བ།</translation>
    </message>
</context>
<context>
    <name>QFontDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qfontdialog.cpp" line="+772"/>
        <source>&amp;Font</source>
        <translation>&gt; Font</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Font st&amp;yle</source>
        <translation>ཡིག་གཟུགས་st&amp;yle</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>&gt;ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Effects</source>
        <translation>ཕན་འབྲས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stri&amp;keout</source>
        <translation>སི་ཐི་ལེ་ཐི་ཡིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Underline</source>
        <translation>གསལ་བཤད་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sample</source>
        <translation>དཔེ་མཚོན་ཐོན་རྫས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wr&amp;iting System</source>
        <translation>Wr&gt;མ་ལག</translation>
    </message>
    <message>
        <location line="-604"/>
        <location line="+247"/>
        <source>Select Font</source>
        <translation>ཡིག་གཟུགས་བདམས་པ།</translation>
    </message>
</context>
<context>
    <name>QFtp</name>
    <message>
        <location filename="../src/network/access/qftp.cpp" line="+826"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+683"/>
        <source>Not connected</source>
        <translation>འབྲེལ་མཐུད་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+65"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+65"/>
        <source>Host %1 not found</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན་གྱིས་རྙེད་མེད་པའི་%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+4"/>
        <source>Connection refused to host %1</source>
        <translation>འབྲེལ་མཐུད་བྱེད་མཁན་གྱིས་གཙོ་གཉེར་བྱེད་པར་དང་ལེན་མ་བྱས།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection timed out to host %1</source>
        <translation>དུས་ཐོག་ཏུ་འབྲེལ་མཐུད་བྱས་ནས་གཙོ་གཉེར་བྱེད་མཁན་%1</translation>
    </message>
    <message>
        <location line="+104"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+102"/>
        <location line="+1451"/>
        <source>Connected to host %1</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན་དང་འབྲེལ་མཐུད་བྱེད་པའི་%1</translation>
    </message>
    <message>
        <location line="+219"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="-1290"/>
        <source>Connection refused for data connection</source>
        <translation>གཞི་གྲངས་སྦྲེལ་མཐུད་བྱེད་པར་ཁས་མ་བླངས་པའི་འབྲེལ་</translation>
    </message>
    <message>
        <location line="+178"/>
        <location line="+29"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+195"/>
        <location line="+728"/>
        <source>Unknown error</source>
        <translation>ཤེས་མེད་པའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location line="+889"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+77"/>
        <source>Connecting to host failed:
%1</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན་དང་འབྲེལ་མཐུད་བྱེད་པར་ཕམ་ཉེས་བྱུང
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Login failed:
%1</source>
        <translation>ཐོ་འགོད་བྱེད་པར་ཕམ་ཉེས་བྱུང་བ་སྟེ།
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Listing directory failed:
%1</source>
        <translation>ཚོང་རར་བཏོན་པའི་དཀར་ཆག་ལ་ཕམ་ཉེས་བྱུང་བ
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Changing directory failed:
%1</source>
        <translation>དཀར་ཆག་བསྒྱུར་བཅོས་བྱེད་པར་ཕམ་ཉེས་བྱུང་བ་སྟེ།
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Downloading file failed:
%1</source>
        <translation>ཕབ་ལེན་བྱས་པའི་ཡིག་ཆ་ཕམ་སོང་བ་སྟེ།
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Uploading file failed:
%1</source>
        <translation>ཡིག་ཆ་ཕབ་ལེན་བྱེད་པར་ཕམ་ཉེས་བྱུང་བ་སྟེ།
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Removing file failed:
%1</source>
        <translation>2004-2003-2003-2
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Creating directory failed:
%1</source>
        <translation>དཀར་ཆག་གསར་སྐྲུན་བྱེད་པར་ཕམ་ཉེས་བྱུང་བ་སྟེ།
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Removing directory failed:
%1</source>
        <translation>དཀར་ཆག་མེད་པར་བཟོ་བར་ཕམ་ཉེས་བྱུང་བ་སྟེ།
%1</translation>
    </message>
    <message>
        <location line="+28"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+25"/>
        <location line="+250"/>
        <source>Connection closed</source>
        <translation>འབྲེལ་མཐུད་བྱེད་མཚམས་བཞག་པ།</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="-11"/>
        <source>Host %1 found</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན་གྱིས་ཤེས་རྟོགས་བྱུང་བའི་%</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection to %1 closed</source>
        <translation>%1སྒོ་བརྒྱབ་ནས་སྒོ་བརྒྱབ་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Host found</source>
        <translation>མདོ་འཛིན་པས་ཤེས་རྟོགས་བྱུང</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Connected to host</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན་དང་འབྲེལ་མཐུད</translation>
    </message>
</context>
<context>
    <name>QGuiApplication</name>
    <message>
        <location filename="../src/gui/kernel/qapplication.cpp" line="+2248"/>
        <source>QT_LAYOUT_DIRECTION</source>
        <comment>Translate this string to the string &apos;LTR&apos; in left-to-right languages or to &apos;RTL&apos; in right-to-left languages (such as Hebrew and Arabic) to get proper widget layout.</comment>
        <translation>QT_LAYOUT_DIRECTION</translation>
    </message>
</context>
<context>
    <name>QHostInfo</name>
    <message>
        <location filename="../src/network/kernel/qhostinfo_p.h" line="+183"/>
        <source>Unknown error</source>
        <translation>ཤེས་མེད་པའི་ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>QHostInfoAgent</name>
    <message>
        <location filename="../src/network/kernel/qhostinfo_unix.cpp" line="+178"/>
        <location line="+9"/>
        <location line="+64"/>
        <location line="+31"/>
        <location filename="../src/network/kernel/qhostinfo_win.cpp" line="+180"/>
        <location line="+9"/>
        <location line="+40"/>
        <location line="+27"/>
        <source>Host not found</source>
        <translation>བདག་པོས་མ་རྙེད་པ།</translation>
    </message>
    <message>
        <location line="-44"/>
        <location line="+39"/>
        <location filename="../src/network/kernel/qhostinfo_win.cpp" line="-34"/>
        <location line="+29"/>
        <source>Unknown address type</source>
        <translation>ས་གནས་ཀྱི་རིགས་དབྱིབས་མི་ཤེས</translation>
    </message>
    <message>
        <location line="+8"/>
        <location filename="../src/network/kernel/qhostinfo_win.cpp" line="-19"/>
        <location line="+27"/>
        <source>Unknown error</source>
        <translation>ཤེས་མེད་པའི་ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>QHttp</name>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="+1574"/>
        <location line="+820"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+1160"/>
        <location line="+567"/>
        <source>Unknown error</source>
        <translation>ཤེས་མེད་པའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location line="-568"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="-370"/>
        <source>Request aborted</source>
        <translation>མངལ་ཤོར་བའི་རེ་བ་ཞུ་ཡིག</translation>
    </message>
    <message>
        <location line="+579"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+381"/>
        <source>No server set to connect to</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པའི་ཞབས་ཞུའི་ཡོ་བྱད་མེད་པ།</translation>
    </message>
    <message>
        <location line="+164"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+56"/>
        <source>Wrong content length</source>
        <translation>ནོར་འཁྲུལ་གྱི་ནང་དོན་རིང་</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+4"/>
        <source>Server closed connection unexpectedly</source>
        <translation>བསམ་ཡུལ་ལས་འདས་པའི་སྒོ་ནས་ཞབས་ཞུའི་</translation>
    </message>
    <message>
        <location line="+179"/>
        <source>Unknown authentication method</source>
        <translation>ཤེས་མེད་པའི་བདེན་དཔང་ར་སྤྲོད་བྱེད་ཐབས</translation>
    </message>
    <message>
        <location line="+183"/>
        <source>Error writing response to device</source>
        <translation>སྒྲིག་ཆས་ལ་ནོར་འཁྲུལ་གྱི་རྩོམ་ཡིག་འབྲི་བའི་</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="+876"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+38"/>
        <source>Connection refused</source>
        <translation>འབྲེལ་མཐུད་དང་ལེན་མ་བྱས་</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="-304"/>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="-4"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+3"/>
        <source>Host %1 not found</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན་གྱིས་རྙེད་མེད་པའི་%1</translation>
    </message>
    <message>
        <location line="+20"/>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="+10"/>
        <location line="+19"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+3"/>
        <source>HTTP request failed</source>
        <translation>HTTPཡི་རེ་བ་ལ་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
    <message>
        <location line="+73"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+69"/>
        <source>Invalid HTTP response header</source>
        <translation>གོ་མི་ཆོད་པའི་HTTP Response header</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+48"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+40"/>
        <location line="+47"/>
        <source>Invalid HTTP chunked body</source>
        <translation>གོ་མི་ཆོད་པའི་HTTPཡི་ལུས་ཕུང་།</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="+294"/>
        <source>Host %1 found</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན་གྱིས་ཤེས་རྟོགས་བྱུང་བའི་%</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connected to host %1</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན་དང་འབྲེལ་མཐུད་བྱེད་པའི་%1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection to %1 closed</source>
        <translation>%1སྒོ་བརྒྱབ་ནས་སྒོ་བརྒྱབ་པ།</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Host found</source>
        <translation>མདོ་འཛིན་པས་ཤེས་རྟོགས་བྱུང</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connected to host</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན་དང་འབྲེལ་མཐུད</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="-22"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+3"/>
        <source>Connection closed</source>
        <translation>འབྲེལ་མཐུད་བྱེད་མཚམས་བཞག་པ།</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="-135"/>
        <source>Proxy authentication required</source>
        <translation>མཁོ་བའི་ངོ་ཚབ་ཀྱི་བདེན་དཔང་ར་སྤྲོད་</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Authentication required</source>
        <translation>མཁོ་བའི་བདེན་དཔང་ར་སྤྲོད་བྱ་དགོས</translation>
    </message>
    <message>
        <location line="-138"/>
        <source>Connection refused (or timed out)</source>
        <translation>འབྲེལ་མཐུད་དང་ལེན་མ་བྱས་པ་(ཡང་ན་དུས་ཐོག་ཏུ་ཕྱིར་འཐེན་བྱས་པ</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="+6"/>
        <source>Proxy requires authentication</source>
        <translation>ངོ་ཚབ་ལ་བདེན་དཔང་ར་སྤྲོད་བྱ་དགོས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Host requires authentication</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན་གྱིས་བདེན་དཔང་ར་སྤྲོད་བྱ་</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Data corrupted</source>
        <translation>གཞི་གྲངས་རུལ་སུངས་སུ་གྱུར</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown protocol specified</source>
        <translation>གཏན་འབེབས་བྱས་པའི་ཤེས་མེད་པའི་གྲོས་མཐུན</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>SSL handshake failed</source>
        <translation>SSLལག་འཇུ་བྱས་ཀྱང་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="-2263"/>
        <source>HTTPS connection requested but SSL support not compiled in</source>
        <translation>HTTPSའབྲེལ་མཐུད་བྱ་རྒྱུའི་བླང་བྱ་བཏོན་ཡོད་ཀྱང་SSLཡི་རྒྱབ་སྐྱོར་དེ་ཕྱོགས་བསྒྲིགས་བྱས་མེད།</translation>
    </message>
</context>
<context>
    <name>QHttpSocketEngine</name>
    <message>
        <location filename="../src/network/socket/qhttpsocketengine.cpp" line="-89"/>
        <source>Did not receive HTTP response from proxy</source>
        <translation>ངོ་ཚབ་ཀྱི་ཁྲོད་ནས་HTTPཡི་ཚུར་སྣང་མ་འབྱོར།</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Error parsing authentication request from proxy</source>
        <translation>ཚབ་བྱེད་མི་སྣས་བདེན་དཔང་ར་སྤྲོད་བྱ་རྒྱུའི་བླང་བྱ་བཏོན་པར་ནོར་འཁྲུལ</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Authentication required</source>
        <translation>མཁོ་བའི་བདེན་དཔང་ར་སྤྲོད་བྱ་དགོས</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Proxy denied connection</source>
        <translation>ངོ་ཚབ་ཀྱིས་འབྲེལ་མཐུད་བྱས་པ་ཁས་</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Error communicating with HTTP proxy</source>
        <translation>HTTPཡི་ཚབ་བྱེད་མི་སྣ་དང་འབྲེལ་འདྲིས་བྱེད་ནོར་</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Proxy server not found</source>
        <translation>ཚབ་བྱེད་ཞབས་ཞུའི་ཡོ་བྱད་མ་རྙེད་པ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy connection refused</source>
        <translation>ངོ་ཚབ་ཀྱི་འབྲེལ་མཐུད་དང་ལེན་མ་</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy server connection timed out</source>
        <translation>ཚབ་བྱེད་ཞབས་ཞུའི་ཡོ་བྱད་སྦྲེལ་མཐུད་དུས་ཐོག་ཏུ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy connection closed prematurely</source>
        <translation>ཚབ་བྱེད་འབྲེལ་མཐུད་སྔ་མོ་ནས་སྒོ་བརྒྱབ་པ།</translation>
    </message>
</context>
<context>
    <name>QIBaseDriver</name>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="+1428"/>
        <source>Error opening database</source>
        <translation>ནོར་འཁྲུལ་གྱི་སྒོ་འབྱེད་གཞི་གྲངས་</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Could not start transaction</source>
        <translation>ཉོ་ཚོང་བྱེད་འགོ་འཛུགས་མི་ཐུབ་</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unable to commit transaction</source>
        <translation>ཉོ་ཚོང་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unable to rollback transaction</source>
        <translation>ཕྱིར་ལྡོག་བྱེད་ཐབས་བྲལ་བའི་ཉོ་ཚོང་།</translation>
    </message>
</context>
<context>
    <name>QIBaseResult</name>
    <message>
        <location line="-1097"/>
        <source>Unable to create BLOB</source>
        <translation>BLOBགསར་སྐྲུན་བྱེད་ཐབས་བྲལ།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to write BLOB</source>
        <translation>BLOBཡི་རྩོམ་ཡིག་འབྲི་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Unable to open BLOB</source>
        <translation>BLOBཡི་ཁ་ཕྱེ་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Unable to read BLOB</source>
        <translation>BLOBལ་དཔེ་ཆ་ཀློག་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+189"/>
        <source>Could not find array</source>
        <translation>སྒྲིག་ཆས་རྙེད་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="-157"/>
        <source>Could not get array data</source>
        <translation>སྒྲིག་གཞིའི་གཞི་གྲངས་ཐོབ་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+212"/>
        <source>Could not get query info</source>
        <translation>འདྲི་རྩད་ཀྱི་ཆ་འཕྲིན་ཐོབ་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Could not start transaction</source>
        <translation>ཉོ་ཚོང་བྱེད་འགོ་འཛུགས་མི་ཐུབ་</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to commit transaction</source>
        <translation>ཉོ་ཚོང་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Could not allocate statement</source>
        <translation>གསལ་བསྒྲགས་བགོ་བཤའ་རྒྱག་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Could not prepare statement</source>
        <translation>གསལ་བསྒྲགས་གྲ་སྒྲིག་བྱེད་མི་ཐུབ་</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+7"/>
        <source>Could not describe input statement</source>
        <translation>མ་དངུལ་འཇོག་པའི་གསལ་བསྒྲགས་ཞིབ་བརྗོད་བྱེད་མི</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not describe statement</source>
        <translation>གསལ་བསྒྲགས་བརྗོད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Unable to close statement</source>
        <translation>གསལ་བསྒྲགས་མཇུག་སྒྲིལ་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Unable to execute query</source>
        <translation>འདྲི་རྩད་ལག་བསྟར་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Could not fetch next item</source>
        <translation>རྣམ་གྲངས་རྗེས་མ་ལེན་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+160"/>
        <source>Could not get statement info</source>
        <translation>གསལ་བསྒྲགས་ཀྱི་ཆ་འཕྲིན་ཐོབ་མི་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>QIODevice</name>
    <message>
        <location filename="../src/corelib/global/qglobal.cpp" line="+1869"/>
        <source>Permission denied</source>
        <translation>ཆོག་མཆན་མ་ཐོབ་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Too many open files</source>
        <translation>ཁ་ཕྱེ་བའི་ཡིག་ཆ་མང་དྲགས་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>No such file or directory</source>
        <translation>དེ་ལྟ་བུའི་ཡིག་ཆ་དང་དཀར་ཆག་མེད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>No space left on device</source>
        <translation>སྒྲིག་ཆས་སྟེང་གི་བར་སྟོང་ལྷག་མ་མེད།</translation>
    </message>
    <message>
        <location filename="../src/corelib/io/qiodevice.cpp" line="+1536"/>
        <source>Unknown error</source>
        <translation>ཤེས་མེད་པའི་ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>QInputContext</name>
    <message>
        <location filename="../src/gui/inputmethod/qinputcontextfactory.cpp" line="+242"/>
        <source>XIM</source>
        <translation>XIM</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>XIM input method</source>
        <translation>XIMཡི་ནང་འཇུག་བྱེད་ཐབས།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Windows input method</source>
        <translation>སྒེའུ་ཁུང་གི་ནང་འཇུག་བྱེད་ཐབས།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Mac OS X input method</source>
        <translation>Mac OS Xཡི་ནང་འཇུག་བྱེད་ཐབས།</translation>
    </message>
</context>
<context>
    <name>QInputDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qinputdialog.cpp" line="+223"/>
        <source>Enter a value:</source>
        <translation>རིན་ཐང་ཞིག་ནང་འཇུག་བྱ་རྒྱུ་སྟེ།</translation>
    </message>
</context>
<context>
    <name>QLibrary</name>
    <message>
        <source>QLibrary::load_sys: Cannot load %1 (%2)</source>
        <translation type="obsolete">QLibrary::load_sys： 不能载入%1 (%2)</translation>
    </message>
    <message>
        <source>QLibrary::unload_sys: Cannot unload %1 (%2)</source>
        <translation type="obsolete">QLibrary::unload_sys：不能卸载%1 (%2)</translation>
    </message>
    <message>
        <source>QLibrary::resolve_sys: Symbol &quot;%1&quot; undefined in %2 (%3)</source>
        <translation type="obsolete">QLibrary::resolve_sys: 符号“%1”在%2（%3）没有被定义</translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary.cpp" line="+378"/>
        <source>Could not mmap &apos;%1&apos;: %2</source>
        <translation>mmap &apos;%1&apos;: %2</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Plugin verification data mismatch in &apos;%1&apos;</source>
        <translation>&apos;%1&apos;ནང་གི་Plugin ཞིབ་བཤེར་གཞི་གྲངས་ཆ་འགྲིག་མིན་པ།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Could not unmap &apos;%1&apos;: %2</source>
        <translation>&apos;%1&apos;:%2</translation>
    </message>
    <message>
        <location line="+302"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (%2.%3.%4) [%5]</source>
        <translation>plugin &apos;%1&apos;བཀོལ་སྤྱོད་བྱེད་སྐབས་ཕན་ཚུན་མཐུན་ཐབས་མེད་པའི་Qtདགོ་མཛོད་སྤྱད་པ་རེད། (%2.%3.%4) [%5]</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. Expected build key &quot;%2&quot;, got &quot;%3&quot;</source>
        <translation>plugin &apos;%1&apos;བཀོལ་སྤྱོད་བྱེད་སྐབས་ཕན་ཚུན་མཐུན་ཐབས་མེད་པའི་Qtདགོ་མཛོད་སྤྱད་པ་རེད། སྔོན་དཔག་བྱས་ན་འཛུགས་སྐྲུན་གྱི་འགག་རྩའི་&quot;%2&quot;ལ་&quot;%3&quot;ཐོབ་ཡོད།</translation>
    </message>
    <message>
        <location line="+340"/>
        <source>Unknown error</source>
        <translation>ཤེས་མེད་པའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location line="-377"/>
        <location filename="../src/corelib/plugin/qpluginloader.cpp" line="+280"/>
        <source>The shared library was not found.</source>
        <translation>མཉམ་སྤྱོད་དཔེ་མཛོད་ཁང་མ་རྙེད།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>The file &apos;%1&apos; is not a valid Qt plugin.</source>
        <translation>&apos;%1&apos;ནི་གོ་ཆོད་པའི་Qt pluginམི་ཡིན།</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (Cannot mix debug and release libraries.)</source>
        <translation>plugin &apos;%1&apos;བཀོལ་སྤྱོད་བྱེད་སྐབས་ཕན་ཚུན་མཐུན་ཐབས་མེད་པའི་Qtདགོ་མཛོད་སྤྱད་པ་རེད། (དཔེ་མཛོད་ཁང་མཉམ་བསྲེས་དང་འགྲེམ་སྤེལ་བྱེད་མི་ཐུབ།)</translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary_unix.cpp" line="+209"/>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="+99"/>
        <source>Cannot load library %1: %2</source>
        <translation>དཔེ་མཛོད་ཁང་གི་ཨང་གྲངས་1:%2</translation>
    </message>
    <message>
        <location line="+16"/>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="+26"/>
        <source>Cannot unload library %1: %2</source>
        <translation>དཔེ་མཛོད་ཁང་གི་ཨང་གྲངས་1:%2</translation>
    </message>
    <message>
        <location line="+31"/>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="+15"/>
        <source>Cannot resolve symbol &quot;%1&quot; in %2: %3</source>
        <translation>%2:%3ནང་གི་&quot;%1&quot;མཚོན་རྟགས་ཐག་གཅོད་བྱེད་མི་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>QLineEdit</name>
    <message>
        <location filename="../src/gui/widgets/qlineedit.cpp" line="+2680"/>
        <source>&amp;Undo</source>
        <translation>་་</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Redo</source>
        <translation>ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་བསྐྱར་དུ་</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cu&amp;t</source>
        <translation>Cu&gt;t</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Copy</source>
        <translation>&amp; འདྲ་བཤུས།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Paste</source>
        <translation>&amp; paste</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Select All</source>
        <translation>ཚང་མ་བདམས་པ་</translation>
    </message>
</context>
<context>
    <name>QLocalServer</name>
    <message>
        <location filename="../src/network/socket/qlocalserver.cpp" line="+226"/>
        <location filename="../src/network/socket/qlocalserver_unix.cpp" line="+231"/>
        <source>%1: Name error</source>
        <translation>%1:མིང་ནོར་འཁྲུལ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../src/network/socket/qlocalserver_unix.cpp" line="-8"/>
        <source>%1: Permission denied</source>
        <translation>%1:ཆོག་མཆན་མ་ཐོབ་པ།</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1: Address in use</source>
        <translation>%1:བཀོལ་སྤྱོད་བྱེད་བཞིན་པའི་སྡོད་གནས།</translation>
    </message>
    <message>
        <location line="+5"/>
        <location filename="../src/network/socket/qlocalserver_win.cpp" line="+158"/>
        <source>%1: Unknown error %2</source>
        <translation>%1:ཤེས་མེད་པའི་ནོར་འཁྲུལ་གྱི་%2</translation>
    </message>
</context>
<context>
    <name>QLocalSocket</name>
    <message>
        <location filename="../src/network/socket/qlocalsocket_tcp.cpp" line="+132"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+134"/>
        <source>%1: Connection refused</source>
        <translation>%1:འབྲེལ་མཐུད་དང་ལེན་མ་བྱས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Remote closed</source>
        <translation>%1:རྒྱང་རིང་གི་སྒོ་བརྒྱབ་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_win.cpp" line="+80"/>
        <location line="+43"/>
        <source>%1: Invalid name</source>
        <translation>%1:གོ་མི་ཆོད་པའི་མིང་།</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket access error</source>
        <translation>%1:བསྒར་ཁུང་གི་ཆོག་འཐུས་ནོར་འཁྲུལ་</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket resource error</source>
        <translation>%1:བསྒར་ཁུང་ཐོན་ཁུངས་ཀྱི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket operation timed out</source>
        <translation>%1:བསྒར་ཁུང་གི་གཤག་བཅོས་དུས་ཐོག་ཏུ་ཕྱིར་འཐེན་བྱས་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Datagram too large</source>
        <translation>%1:གཞི་གྲངས་རི་མོ་ཆེ་དྲགས་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_win.cpp" line="-48"/>
        <source>%1: Connection error</source>
        <translation>%1:连接错误</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: The socket operation is not supported</source>
        <translation>%1:བསྒར་ཁུང་གི་གཤག་བཅོས་ལ་རྒྱབ་སྐྱོར་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: Unknown error</source>
        <translation>%1:ཤེས་མེད་པའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+4"/>
        <location filename="../src/network/socket/qlocalsocket_win.cpp" line="+10"/>
        <source>%1: Unknown error %2</source>
        <translation>%1:ཤེས་མེད་པའི་ནོར་འཁྲུལ་གྱི་%2</translation>
    </message>
</context>
<context>
    <name>QMYSQLDriver</name>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="+1231"/>
        <source>Unable to open database &apos;</source>
        <translation>གཞི་གྲངས་མཛོད་ཀྱི་སྒོ་འབྱེད་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to connect</source>
        <translation>འབྲེལ་མཐུད་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+127"/>
        <source>Unable to begin transaction</source>
        <translation>ཉོ་ཚོང་བྱེད་འགོ་འཛུགས་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to commit transaction</source>
        <translation>ཉོ་ཚོང་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback transaction</source>
        <translation>ཕྱིར་ལྡོག་བྱེད་ཐབས་བྲལ་བའི་ཉོ་ཚོང་།</translation>
    </message>
</context>
<context>
    <name>QMYSQLResult</name>
    <message>
        <location line="-922"/>
        <source>Unable to fetch data</source>
        <translation>གཞི་གྲངས་ལེན་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+176"/>
        <source>Unable to execute query</source>
        <translation>འདྲི་རྩད་ལག་བསྟར་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to store result</source>
        <translation>གྲུབ་འབྲས་གསོག་ཉར་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+190"/>
        <location line="+8"/>
        <source>Unable to prepare statement</source>
        <translation>གསལ་བསྒྲགས་གྲ་སྒྲིག་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Unable to reset statement</source>
        <translation>གསལ་བསྒྲགས་བསྐྱར་དུ་བཀོད་སྒྲིག་བྱེད་ཐབས་བྲལ།</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Unable to bind value</source>
        <translation>རིན་ཐང་ལ་ཚོད་འཛིན་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Unable to execute statement</source>
        <translation>གསལ་བསྒྲགས་ལག་བསྟར་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+14"/>
        <location line="+21"/>
        <source>Unable to bind outvalues</source>
        <translation>རིན་གོང་ཚོད་འཛིན་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Unable to store statement results</source>
        <translation>གསལ་བསྒྲགས་བྱས་འབྲས་གསོག་ཉར་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="-253"/>
        <source>Unable to execute next query</source>
        <translation>ཐེངས་རྗེས་མའི་འདྲི་རྩད་ལག་བསྟར་བྱེད་ཐབས་བྲལ།</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Unable to store next result</source>
        <translation>རྗེས་མའི་མཇུག་འབྲས་གསོག་ཉར་བྱེད་ཐབས་བྲལ།</translation>
    </message>
</context>
<context>
    <name>QMdiArea</name>
    <message>
        <location filename="../src/gui/widgets/qmdiarea.cpp" line="+290"/>
        <source>(Untitled)</source>
        <translation>(ཡིག་ཐོག་ནས་གསལ་བཤད་བྱས་མེད། )</translation>
    </message>
</context>
<context>
    <name>QMdiSubWindow</name>
    <message>
        <location filename="../src/gui/widgets/qmdisubwindow.cpp" line="+280"/>
        <source>%1 - [%2]</source>
        <translation>%1 - [%2]</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Minimize</source>
        <translation>ཉུང་དུ་གཏོང་གང་ཐུབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Restore Down</source>
        <translation>སླར་གསོ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="+707"/>
        <source>&amp;Restore</source>
        <translation>&amp;སླར་གསོ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Move</source>
        <translation>&gt; འགུལ་སྐྱོད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>&gt;ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mi&amp;nimize</source>
        <translation>སྨི་ནེ་སྨི་ཙི།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ma&amp;ximize</source>
        <translation>མཱ་ཞི་མའེ་ཙི།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stay on &amp;Top</source>
        <translation>མུ་མཐུད་དུ་རྒྱུན་འཁྱོངས་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location line="-787"/>
        <source>- [%1]</source>
        <translation>- [%1]</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Maximize</source>
        <translation>ཚད་གཞི་མཐོ་ཤོས་ཀྱི་སྒོ་ནས</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unshade</source>
        <translation>ཁ་ཡ་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Shade</source>
        <translation>གྲིབ་བསིལ།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Restore</source>
        <translation>སླར་གསོ་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Menu</source>
        <translation>ཟས་ཐོ།</translation>
    </message>
</context>
<context>
    <name>QMenu</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/qaccessiblemenu.cpp" line="+157"/>
        <location line="+225"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location line="-224"/>
        <location line="+225"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location line="-223"/>
        <location line="+225"/>
        <location line="+51"/>
        <source>Execute</source>
        <translation>ལག་བསྟར་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>QMenuBar</name>
    <message>
        <source>About</source>
        <translation type="obsolete">关于</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="obsolete">配置</translation>
    </message>
    <message>
        <source>Preference</source>
        <translation type="obsolete">首选项</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">选项</translation>
    </message>
    <message>
        <source>Setting</source>
        <translation type="obsolete">设置</translation>
    </message>
    <message>
        <source>Setup</source>
        <translation type="obsolete">安装</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="obsolete">退出</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="obsolete">退出</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation type="obsolete">关于%1</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation type="obsolete">关于Qt</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation type="obsolete">首选项</translation>
    </message>
    <message>
        <source>Quit %1</source>
        <translation type="obsolete">退出%1</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.cpp" line="-1111"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="-853"/>
        <location line="+852"/>
        <location filename="../src/gui/dialogs/qmessagebox.h" line="-52"/>
        <location line="+8"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location line="+509"/>
        <source>About Qt</source>
        <translation>Qtཡི་སྐོར།</translation>
    </message>
    <message>
        <source>&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;这个程序使用的是Qt %1版。&lt;/p&gt;</translation>
    </message>
    <message>
        <location line="-1605"/>
        <source>Show Details...</source>
        <translation>ཞིབ་ཕྲའི་གནས་ཚུལ་གསལ་བཤད་བྱ་དགོས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide Details...</source>
        <translation>གནས་ཚུལ་ཞིབ་ཕྲ་སྦས་སྐུང་བྱེད་</translation>
    </message>
    <message>
        <location line="+1570"/>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt for Embedded Linux and Qt for Windows CE.&lt;/p&gt;&lt;p&gt;Qt is available under three different licensing options designed to accommodate the needs of our various users.&lt;/p&gt;Qt licensed under our commercial license agreement is appropriate for development of proprietary/commercial software where you do not want to share any source code with third parties or otherwise cannot comply with the terms of the GNU LGPL version 2.1 or GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU LGPL version 2.1 is appropriate for the development of Qt applications (proprietary or open source) provided you can comply with the terms and conditions of the GNU LGPL version 2.1.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU General Public License version 3.0 is appropriate for the development of Qt applications where you wish to use such applications in combination with software subject to the terms of the GNU GPL version 3.0 or where you are otherwise willing to comply with the terms of the GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://qt.nokia.com/products/licensing&quot;&gt;qt.nokia.com/products/licensing&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;&lt;p&gt;Copyright (C) 2012 Nokia Corporation and/or its subsidiary(-ies).&lt;/p&gt;&lt;p&gt;Qt is a Nokia product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Qtཡས་མས་ཀྱི་གོ་&lt;/h3&gt;&lt;p&gt;རིམ་འདིས་Qtཔར་གཞི་1བཀོལ་སྤྱོད་བྱས་ཡོད།&lt;/p&gt; &lt;p&gt;Qtནི་སྟེགས་བུ་ལས་བརྒལ་བའི་ཉེར་སྤྱོད་གསར་སྤེལ་བྱེད་པའི་C++ཡོ་བྱད་སྒམ་ཞིག་ཡིན།&lt;/p&gt; &lt;p&gt;Qtཡིས་MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux,དེ་བཞིན་ཚོང་ལས་ཀྱི་མཉམ་འབྲེལ་འགྱུར་ལྡོག་གཙོ་བོ་ཡོད་ཚད་ལས་བརྒལ་ནས་ཁེར་རྐྱང་གི་སྟབས་བདེའི་རང་བཞིན་འདོན་སྤྲོད་བྱས་ཡོད། Qtཡིས་ད་དུང་ནང་འཇུག་སྒྲིག་ཆས་དེ་སྒེའུ་ཁུང་གི་དབུ་བཞུགས་ལག་ཁྱེར་གྱི་Qtདང་Qtཡི་Qtཡིན།&lt;/p&gt; &lt;p&gt;Qtནི་ང་ཚོའི་སྤྱོད་མཁན་སྣ་ཚོགས་ཀྱི་དགོས་མཁོ་སྐོང་ཆེད་ཇུས་འགོད་བྱས་པའི་ཆོག་འཐུས་གདམ་ག་མི་འདྲ་བ་གསུམ་གྱི་འོག་ཏུ་འདོན་སྤྲོད་བྱས་ཆོག&lt;/p&gt; ང་ཚོའི་ཚོང་ལས་ལག་ཁྱེར་གྱི་གྲོས་མཐུན་གཞིར་བཟུང་ནས་ཆོག་མཆན་ཐོབ་པའི་Qtནི་ཆེད་སྤྱོད་དང་ཚོང་ལས་མཉེན་ཆས་གསར་སྤེལ་བྱེད་པར་འཚམ་ཞིང་། དེ་ལྟར་མ་བྱས་ན་GNU LGPLཔར་གཞི་2.1དང་GNU GPLཔར་གཞི་3.0ཡི་དོན་ཚན།&lt;/p&gt; &lt;p&gt;GNU LGPLཔར་གཞི་2.1མན་གྱི་Qtནི་Qtབཀོལ་སྤྱོད་(ཆེད་སྤྱོད་ལམ་ཡང་ན་སྒོ་འབྱེད་ཀྱི་འབྱུང་ཁུངས་)གསར་སྤེལ་བྱེད་པར་འཚམ་མོད། འོན་ཀྱང་GNU LGPLཔར་གཞི་2.1གི་དོན་ཚན་དང་ཆ་རྐྱེན་ལ་བརྩི་སྲུང་བྱས་ཆོག&lt;/p&gt; &lt;p&gt;GNUསྤྱིར་བཏང་གི་སྤྱི་སྤྱོད་ཆོག་འཐུས་ལག་ཁྱེར་གྱི་པར་གཞི་3.0པའི་འོག་ཆོག་མཆན་ཐོབ་པའི་Qtནི་Qtཡི་ཉེར་སྤྱོད&lt;/p&gt;་གོ་རིམ་གསར་སྤེལ་བྱེད་པར་འཚམ་པོ་ཡོད། &lt;p&gt;Qtཡི་ཆོག་འཐུས་གནས་ཚུལ་མདོར་བསྡུས&lt;ལ་གཟིགས་དང་། &quot;http://qt.nokia.com/products/licensing&quot;&gt;qt.nokia.com/products/licensing&lt;/a&gt;་ལ་གཟིགས་དང་།&lt;/p&gt; &lt;p&gt;པར་དབང་(C)2012ལོའི་ནོ་ཁི་ཡ་ཀུང་སི་དང་དེའི་ཡན་ལག་ཀུང་སི་(-ies)ཡིན།&lt;/p&gt; &lt;p&gt;Qtནི་ནོ་ཁི་ཡའི་ཐོན་རྫས་ཤིག་ཡིན། དཔྱད&lt;གཞི་སྔར་ལས་མང་བ་མཁོ་འདོན http://qt.nokia.com/ བྱེད་པར་&lt;/a&gt;&gt;qt.nokia.com་བ་ཡོད།&quot;ཞེས་པར་གསལ།&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;%1&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt for Embedded Linux and Qt for Windows CE.&lt;/p&gt;&lt;p&gt;Qt is a Nokia product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokiae.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;h3&gt;关于Qt&lt;/h3&gt;%1&lt;p&gt;Qt是一个用于跨平台应用程序开发的C++工具包。&lt;/p&gt;&lt;p&gt;对于MS&amp;nbsp;Windows、Mac&amp;nbsp;OS&amp;nbsp;X、Linux和所有主流商业Unix，Qt提供了单一源程序的可移植性。Qt也有用于嵌入式Linux和Windows CE的版本。&lt;/p&gt;&lt;p&gt;Qt是Nokia的产品。有关更多信息，请参考&lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt;。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;This program uses Qt Open Source Edition version %1.&lt;/p&gt;&lt;p&gt;Qt Open Source Edition is intended for the development of Open Source applications. You need a commercial Qt license for development of proprietary (closed source) applications.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://qt.nokia.com/company/model/&quot;&gt;qt.nokia.com/company/model/&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;这个程序使用了Qt %1开源版本。&lt;/p&gt;&lt;p&gt;Qt开源版本只用于开源应用程序的开发。如果要开发私有（闭源）软件，你需要一个商业的Qt协议。&lt;/p&gt;&lt;p&gt;有关Qt协议的概览，请参考&lt;a href=&quot;http://qt.nokia.com/company/model/&quot;&gt;qt.nokia.com/company/model/&lt;/a&gt;。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;%1&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt Embedded.&lt;/p&gt;&lt;p&gt;Qt is a Trolltech product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;h3&gt;关于Qt&lt;/h3&gt;%1&lt;p&gt;Qt是一个用于跨平台应用程序开发的C++工具包。&lt;/p&gt;&lt;p&gt;对于MS&amp;nbsp;Windows、Mac&amp;nbsp;OS&amp;nbsp;X、Linux和所有主流商业Unix，Qt提供了单一源程序的可移植性。Qt对于嵌入式平台也是可用的，在嵌入式平台上它被称为Qt Embedded。&lt;/p&gt;&lt;p&gt;Qt是Trolltech的产品。有关更多信息，请参考&lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt;。&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>QMultiInputContext</name>
    <message>
        <location filename="../src/plugins/inputmethods/imsw-multi/qmultiinputcontext.cpp" line="+88"/>
        <source>Select IM</source>
        <translation>IMབདམས་པ།</translation>
    </message>
</context>
<context>
    <name>QMultiInputContextPlugin</name>
    <message>
        <location filename="../src/plugins/inputmethods/imsw-multi/qmultiinputcontextplugin.cpp" line="+95"/>
        <source>Multiple input method switcher</source>
        <translation>ཐེངས་མང་པོར་ནང་འཇུག་བྱེད་ཐབས་ཀྱི་གློག་སྒོ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Multiple input method switcher that uses the context menu of the text widgets</source>
        <translation>ཡི་གེའི་ཞེང་ཚད་ཀྱི་རྒྱབ་ལྗོངས་ཀྱི་ཟས་ཐོ་བཀོལ་སྤྱོད་བྱེད་པའི་ནང་འཇུག་བྱེད་ཐབས་མང་པོ་ཞིག་བརྗེ་རེས་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>QNativeSocketEngine</name>
    <message>
        <location filename="../src/network/socket/qnativesocketengine.cpp" line="+206"/>
        <source>The remote host closed the connection</source>
        <translation>རྒྱང་རིང་བདག་པོས་འབྲེལ་མཐུད་བྱེད་མཚམས་བཞག་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Network operation timed out</source>
        <translation>དྲ་རྒྱའི་འཁོར་སྐྱོད་དུས་ཚོད་ཕྱིར་འཐེན་བྱས</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Out of resources</source>
        <translation>ཐོན་ཁུངས་ལས་བརྒལ་བ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unsupported socket operation</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་བསྒར་ཁུང་གི་གཤག་བཅོས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Protocol type not supported</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་གྲོས་མཐུན་གྱི་རིགས</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid socket descriptor</source>
        <translation>གོ་མི་ཆོད་པའི་བསྒར་ཁུང་ཞིབ་འབྲི་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Network unreachable</source>
        <translation>དྲ་རྒྱ་མངོན་འགྱུར་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Permission denied</source>
        <translation>ཆོག་མཆན་མ་ཐོབ་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection timed out</source>
        <translation>དུས་ཐོག་ཏུ་འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection refused</source>
        <translation>འབྲེལ་མཐུད་དང་ལེན་མ་བྱས་</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The bound address is already in use</source>
        <translation>ས་མཚམས་ཀྱི་སྡོད་གནས་བཀོལ་སྤྱོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The address is not available</source>
        <translation>སྡོད་གནས་བཀོལ་སྤྱོད་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The address is protected</source>
        <translation>སྡོད་གནས་ལ་སྲུང་སྐྱོབ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to send a message</source>
        <translation>ཆ་འཕྲིན་བརྒྱུད་སྤྲོད་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to receive a message</source>
        <translation>ཆ་འཕྲིན་འབྱོར་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to write</source>
        <translation>ཡི་གེ་འབྲི་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Network error</source>
        <translation>དྲ་རྒྱའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Another socket is already listening on the same port</source>
        <translation>ད་དུང་བསྒར་ཁུང་གཞན་ཞིག་གིས་གྲུ་ཁ་གཅིག་གི་སྟེང་ནས་ཉན་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>Unable to initialize non-blocking socket</source>
        <translation>བཀག་འགོག་མི་བྱེད་པའི་བསྒར་ཁུང་ལ་ཐོག་མའི་བྱེད་ཐབས་བྲལ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to initialize broadcast socket</source>
        <translation>རྒྱང་བསྒྲགས་ཀྱི་བསྒར་ཁུང་ཐོག་མར་གཏོང་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Attempt to use IPv6 socket on a platform with no IPv6 support</source>
        <translation>IPv6ཡི་རྒྱབ་སྐྱོར་མེད་པའི་སྟེགས་བུའི་སྟེང་ནས་IPv6 བསྒར་ཁུང་བཀོལ་སྤྱོད་བྱེད་རྩིས་ཡོད།</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Host unreachable</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན་གྱིས་འབྲེལ་མཐུད་བྱེད་ཐབས་བྲལ</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Datagram was too large to send</source>
        <translation>གཞི་གྲངས་ཆེ་དྲགས་པས་སྐུར་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Operation on non-socket</source>
        <translation>བསྒར་ཁུང་མིན་པའི་བྱ་སྤྱོད།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unknown error</source>
        <translation>ཤེས་མེད་པའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>The proxy type is invalid for this operation</source>
        <translation>ཐེངས་འདིའི་བྱ་སྤྱོད་ལ་ངོ་ཚབ་ཀྱི་རིགས་དབྱིབས་གོ་མི་ཆོད་པ་རེད།</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessCacheBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccesscachebackend.cpp" line="+65"/>
        <source>Error opening %1</source>
        <translation>%1%ཁ་ཕྱེ་བའི་ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessFileBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccessfilebackend.cpp" line="+99"/>
        <source>Request for opening non-local file %1</source>
        <translation>ས་གནས་མིན་པའི་ཡིག་ཆ་ཁ་ཕྱེ་བའི་རེ་བ་ཞུ་ཡིག%1</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Error opening %1: %2</source>
        <translation>%1:%2</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Write error writing to %1: %2</source>
        <translation>ནོར་འཁྲུལ་གྱི་ཡི་གེ་བྲིས་ནས་%1:%2</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Cannot open %1: Path is a directory</source>
        <translation>%1:ལམ་ནི་དཀར་ཆག་ཡིན་པ་གསལ་བཤད་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Read error reading from %1: %2</source>
        <translation>%1:%2%ནས་ནོར་འཁྲུལ་གྱི་ཀློག་དེབ་ཀློག་པ།</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessFtpBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccessftpbackend.cpp" line="+165"/>
        <source>No suitable proxy found</source>
        <translation>འོས་འཚམ་གྱི་ངོ་ཚབ་མ་རྙེད་པ།</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Cannot open %1: is a directory</source>
        <translation>%1:དེ་ནི་དཀར་ཆག་ཅིག་ཡིན་པ་གསལ་བཤད་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>Logging in to %1 failed: authentication required</source>
        <translation>%1ལ་ཐོ་འགོད་བྱས་ནས་ཕམ་ཁ་བྱུང་བ་སྟེ། བདེན་དཔང་ར་སྤྲོད་བྱེད་པར་མཁོ་བའི་བདེན་དཔང་ར་སྤྲོད་</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Error while downloading %1: %2</source>
        <translation>%1:%2下载错误</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error while uploading %1: %2</source>
        <translation>%1:%2</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessHttpBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccesshttpbackend.cpp" line="+597"/>
        <source>No suitable proxy found</source>
        <translation>འོས་འཚམ་གྱི་ངོ་ཚབ་མ་རྙེད་པ།</translation>
    </message>
</context>
<context>
    <name>QNetworkReply</name>
    <message>
        <location line="+128"/>
        <source>Error downloading %1 - server replied: %2</source>
        <translation>%1 -server 2</translation>
    </message>
    <message>
        <location filename="../src/network/access/qnetworkreplyimpl.cpp" line="+68"/>
        <source>Protocol &quot;%1&quot; is unknown</source>
        <translation>གྲོས་མཐུན་གྱི་&quot;%1&quot;མི་ཤེས།</translation>
    </message>
</context>
<context>
    <name>QNetworkReplyImpl</name>
    <message>
        <location line="+432"/>
        <location line="+22"/>
        <source>Operation canceled</source>
        <translation>བཀོལ་སྤྱོད་མེད་པར་བཟོས་པ།</translation>
    </message>
</context>
<context>
    <name>QOCIDriver</name>
    <message>
        <location filename="../src/sql/drivers/oci/qsql_oci.cpp" line="+2069"/>
        <source>Unable to logon</source>
        <translation>ཐོ་</translation>
    </message>
    <message>
        <location line="-144"/>
        <source>Unable to initialize</source>
        <comment>QOCIDriver</comment>
        <translation>ཐོག་མའི་དུས་སུ་སླེབས་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+215"/>
        <source>Unable to begin transaction</source>
        <translation>ཉོ་ཚོང་བྱེད་འགོ་འཛུགས་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to commit transaction</source>
        <translation>ཉོ་ཚོང་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to rollback transaction</source>
        <translation>ཕྱིར་ལྡོག་བྱེད་ཐབས་བྲལ་བའི་ཉོ་ཚོང་།</translation>
    </message>
</context>
<context>
    <name>QOCIResult</name>
    <message>
        <location line="-963"/>
        <location line="+161"/>
        <location line="+15"/>
        <source>Unable to bind column for batch execute</source>
        <translation>ཁག་བགོས་ནས་ལག་བསྟར་བྱེད་པའི་ཆེད་བསྒྲིགས་ལེ་ཚན་ལ་ཚོད་འཛིན་བྱེད་ཐབས་བྲལ།</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to execute batch statement</source>
        <translation>ཁག་བགོས་ཀྱི་གསལ་བསྒྲགས་ལག་བསྟར་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+302"/>
        <source>Unable to goto next</source>
        <translation>གོམ་སྟབས་རྗེས་མར་ཐོབ་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Unable to alloc statement</source>
        <translation>གསལ་བསྒྲགས་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to prepare statement</source>
        <translation>གསལ་བསྒྲགས་གྲ་སྒྲིག་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Unable to bind value</source>
        <translation>རིན་ཐང་ལ་ཚོད་འཛིན་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <source>Unable to execute select statement</source>
        <translation type="obsolete">不能执行选择语句</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to execute statement</source>
        <translation>གསལ་བསྒྲགས་ལག་བསྟར་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
</context>
<context>
    <name>QODBCDriver</name>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="+1785"/>
        <source>Unable to connect</source>
        <translation>འབྲེལ་མཐུད་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to connect - Driver doesn&apos;t support all needed functionality</source>
        <translation>འབྲེལ་མཐུད་བྱེད་ཐབས་བྲལ་བ། ཁ་ལོ་བས་དགོས་ངེས་ཀྱི་ནུས་པ་ཡོད་ཚད་ལ་རྒྱབ་སྐྱོར་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+242"/>
        <source>Unable to disable autocommit</source>
        <translation>རང་འགུལ་གྱིས་ཚོང་ཟོག་ཉོ་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to commit transaction</source>
        <translation>ཉོ་ཚོང་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback transaction</source>
        <translation>ཕྱིར་ལྡོག་བྱེད་ཐབས་བྲལ་བའི་ཉོ་ཚོང་།</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to enable autocommit</source>
        <translation>རང་འགུལ་གྱིས་ཚོང་ཟོག་ཉོ་མི་ཐུབ་པ།</translation>
    </message>
</context>
<context>
    <name>QODBCResult</name>
    <message>
        <location line="-1218"/>
        <location line="+349"/>
        <source>QODBCResult::reset: Unable to set &apos;SQL_CURSOR_STATIC&apos; as statement attribute. Please check your ODBC driver configuration</source>
        <translation>QODBCResult::reset:&apos;SQL_CURSOR_STATIC&apos;དེ་གསལ་བསྒྲགས་ཀྱི་ངོ་བོར་གཏན་འཁེལ་བྱེད་མི་ཐུབ། ཁྱེད་ཀྱི་ODBCཁ་ལོ་བའི་བཀོད་སྒྲིག་ལ་ཞིབ་བཤེར་གནང་རོགས།</translation>
    </message>
    <message>
        <location line="-332"/>
        <location line="+626"/>
        <source>Unable to execute statement</source>
        <translation>གསལ་བསྒྲགས་ལག་བསྟར་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="-555"/>
        <source>Unable to fetch next</source>
        <translation>གོམ་སྟབས་རྗེས་མར་ལེན་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+279"/>
        <source>Unable to prepare statement</source>
        <translation>གསལ་བསྒྲགས་གྲ་སྒྲིག་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+268"/>
        <source>Unable to bind variable</source>
        <translation>ཚོད་འཛིན་བྱེད་ཐབས་བྲལ་བའི་འགྱུར་ལྡོག</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="+194"/>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="-475"/>
        <location line="+578"/>
        <source>Unable to fetch last</source>
        <translation>མཇུག་མཐར་ལེན་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="-672"/>
        <source>Unable to fetch</source>
        <translation>ལེན་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Unable to fetch first</source>
        <translation>སྔོན་ལ་ལེན་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to fetch previous</source>
        <translation>སྔོན་ཆད་ཀྱི་གོ་སྐབས་དང་ལེན་བྱེད་མི་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/gui/util/qdesktopservices_mac.cpp" line="+165"/>
        <source>Home</source>
        <translation>ཁྱིམ་ཚང་།</translation>
    </message>
    <message>
        <location filename="../src/network/access/qnetworkaccessdatabackend.cpp" line="+74"/>
        <source>Operation not supported on %1</source>
        <translation>%1ལ་རྒྱབ་སྐྱོར་མི་བྱེད་པའི་བྱ་སྤྱོད།</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Invalid URI: %1</source>
        <translation>གོ་མི་ཆོད་པའི་URI: %1</translation>
    </message>
    <message>
        <location filename="../src/network/access/qnetworkaccessdebugpipebackend.cpp" line="+175"/>
        <source>Write error writing to %1: %2</source>
        <translation>ནོར་འཁྲུལ་གྱི་ཡི་གེ་བྲིས་ནས་%1:%2</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Read error reading from %1: %2</source>
        <translation>%1:%2%ནས་ནོར་འཁྲུལ་གྱི་ཀློག་དེབ་ཀློག་པ།</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Socket error on %1: %2</source>
        <translation>%1:%2 སྟེང་གི་བསྒར་ཁུང་གི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Remote host closed the connection prematurely on %1</source>
        <translation>རྒྱང་རིང་གི་མདོ་འཛིན་པས་%1སྟོབ་ནས་སྔ་མོ་ནས་འབྲེལ་མཐུད་བྱེད་མཚམས་བཞག་པ</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Protocol error: packet of size 0 received</source>
        <translation>གྲོས་མཐུན་གྱི་ནོར་འཁྲུལ། གཞི་ཁྱོན་0ལག་འབྱོར་བྱུང་བའི་ཁུག་མ།</translation>
    </message>
    <message>
        <location filename="../src/network/kernel/qhostinfo.cpp" line="+177"/>
        <location line="+57"/>
        <source>No host name given</source>
        <translation>བདག་པོའི་མིང་མི་བཤད་པ།</translation>
    </message>
</context>
<context>
    <name>QPPDOptionsModel</name>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="+1195"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Value</source>
        <translation>རིན་ཐང་།</translation>
    </message>
</context>
<context>
    <name>QPSQLDriver</name>
    <message>
        <location filename="../src/sql/drivers/psql/qsql_psql.cpp" line="+763"/>
        <source>Unable to connect</source>
        <translation>འབྲེལ་མཐུད་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Could not begin transaction</source>
        <translation>ཉོ་ཚོང་བྱེད་འགོ་འཛུགས་མི་ཐུབ་</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Could not commit transaction</source>
        <translation>ཉོ་ཚོང་བྱེད་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Could not rollback transaction</source>
        <translation>ཕྱིར་ལྡོག་བྱེད་མི་ཐུབ་པའི་ཉོ་ཚོང་།</translation>
    </message>
    <message>
        <location line="+358"/>
        <source>Unable to subscribe</source>
        <translation>མངག་ཉོ་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Unable to unsubscribe</source>
        <translation>གཏན་འབེབས་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
</context>
<context>
    <name>QPSQLResult</name>
    <message>
        <location line="-1058"/>
        <source>Unable to create query</source>
        <translation>འདྲི་རྩད་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+374"/>
        <source>Unable to prepare statement</source>
        <translation>གསལ་བསྒྲགས་གྲ་སྒྲིག་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
</context>
<context>
    <name>QPageSetupWidget</name>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupdialog_unix.cpp" line="+304"/>
        <source>Centimeters (cm)</source>
        <translation>ལི་སྨི་(cm)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Millimeters (mm)</source>
        <translation>ཧའོ་སྨི་(mm)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Inches (in)</source>
        <translation>དབྱིན་ཚུན་(ནང་དུ་)།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Points (pt)</source>
        <translation>གནད་དོན་(pt)</translation>
    </message>
    <message>
        <source>Pica (P̸)</source>
        <translation>ཕེ་ཅ་(P⃼͵ྑྑ</translation>
    </message>
    <message>
        <source>Didot (DD)</source>
        <translation>Didot (DD)</translation>
    </message>
    <message>
        <source>Cicero (CC)</source>
        <translation>ཁི་སི་ལོ་(CC)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupwidget.ui"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location/>
        <source>Paper</source>
        <translation>ཤོག་བུ།</translation>
    </message>
    <message>
        <location/>
        <source>Page size:</source>
        <translation>ཤོག་ངོས་ཆེ་ཆུང་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location/>
        <source>Width:</source>
        <translation>ཞེང་ཚད་ནི།</translation>
    </message>
    <message>
        <location/>
        <source>Height:</source>
        <translation>མཐོ་ཚད་ནི།</translation>
    </message>
    <message>
        <location/>
        <source>Paper source:</source>
        <translation>ཤོག་བུའི་འབྱུང་ཁུངས་ནི།</translation>
    </message>
    <message>
        <location/>
        <source>Orientation</source>
        <translation>ཁ་ཕྱོགས་</translation>
    </message>
    <message>
        <location/>
        <source>Portrait</source>
        <translation>འདྲ་པར།</translation>
    </message>
    <message>
        <location/>
        <source>Landscape</source>
        <translation>མཛེས་ལྗོངས།</translation>
    </message>
    <message>
        <location/>
        <source>Reverse landscape</source>
        <translation>ལྡོག་ཕྱོགས་ཀྱི་ཡུལ་ལྗོངས།</translation>
    </message>
    <message>
        <location/>
        <source>Reverse portrait</source>
        <translation>ལྡོག་ཕྱོགས་ཀྱི་འདྲ་པར།</translation>
    </message>
    <message>
        <location/>
        <source>Margins</source>
        <translation>མ་དངུལ་འཇོག་གྲངས།</translation>
    </message>
    <message>
        <location/>
        <source>top margin</source>
        <translation>ཆེས་མཐོ་བའི་ཁག་ཐེག་</translation>
    </message>
    <message>
        <location/>
        <source>left margin</source>
        <translation>གཡོན་ཕྱོགས་ཀྱི་ལྷག་གྲངས།</translation>
    </message>
    <message>
        <location/>
        <source>right margin</source>
        <translation>གཡས་ཕྱོགས་ཀྱི་ལྷག་གྲངས།</translation>
    </message>
    <message>
        <location/>
        <source>bottom margin</source>
        <translation>མ་མཐའི་ཁེ་སྤོགས་ཀྱི་</translation>
    </message>
    <message>
        <location/>
        <source>Page Layout</source>
        <translation>ཤོག་ངོས་ཀྱི་བཀོད་པ།</translation>
    </message>
    <message>
        <location/>
        <source>Pages per sheet:</source>
        <translation>ཤོག་ལྷེ་རེ་རེའི་ཤོག་ལྷེ་རེ་རེ་</translation>
    </message>
    <message>
        <location/>
        <source>Page order:</source>
        <translation>ཤོག་ངོས་ཀྱི་གོ་རིམ་ནི།</translation>
    </message>
</context>
<context>
    <name>QCupsJobWidget</name>
    <message>
        <location/>
        <source>Job Control</source>
        <translation>ལས་གནས་ཚོད་འཛིན།</translation>
    </message>
    <message>
        <location/>
        <source>Scheduled printing:</source>
        <translation>དུས་བཀག་ལྟར་པར་སྐྲུན་བྱས་པ་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location/>
        <source>Print Immediately</source>
        <translation>འཕྲལ་མར་པར་སྐྲུན་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location/>
        <source>Hold Indefinitely</source>
        <translation>དུས་ཚད་མེད་པར་རྒྱུན་འཁྱོངས་</translation>
    </message>
    <message>
        <location/>
        <source>Day (06:00 to 17:59)</source>
        <translation>ཉིན་མོ།(06:00 ནས་17:59)</translation>
    </message>
    <message>
        <location/>
        <source>Night (18:00 to 05:59)</source>
        <translation>མཚན་མོ།(18:00 to 05:59)</translation>
    </message>
    <message>
        <location/>
        <source>Second Shift (16:00 to 23:59)</source>
        <translation>ལས་རེས་གཉིས་པ། (16:00 to 23:59)</translation>
    </message>
    <message>
        <location/>
        <source>Third Shift (00:00 to 07:59)</source>
        <translation>ལས་རེས་གསུམ་པ། (00:00 to 07:59)</translation>
    </message>
    <message>
        <location/>
        <source>Weekend (Saturday to Sunday)</source>
        <translation>གཟའ་མཇུག་(གཟའ་སྤེན་པ་ནས་གཟའ་ཉི་མར་)</translation>
    </message>
    <message>
        <location/>
        <source>Specific Time</source>
        <translation>བྱེ་བྲག་གི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location/>
        <source>Billing information:</source>
        <translation>དངུལ་འཛིན་གྱི་ཆ་འཕྲིན་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location/>
        <source>Job priority:</source>
        <translation>ལས་ཀའི་གཙོ་གནད་ནི།</translation>
    </message>
    <message>
        <location/>
        <source>Banner Pages</source>
        <translation>འཕྲེད་འགེལ་ཤོག་ལྷེ།</translation>
    </message>
    <message>
        <location/>
        <source>Start:</source>
        <translation>འགོ་འཛུགས་རྒྱུ་སྟེ།</translation>
    </message>
    <message>
        <location/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location/>
        <source>Standard</source>
        <translation>ཚད་གཞི།</translation>
    </message>
    <message>
        <location/>
        <source>Unclassified</source>
        <translation>གསང་བའི་སྒོ་ནས་གསང་བ་ཕྱིར་</translation>
    </message>
    <message>
        <location/>
        <source>Confidential</source>
        <translation>གསང་བ་དམ་སྲུང་</translation>
    </message>
    <message>
        <location/>
        <source>Classified</source>
        <translation>རིགས་དབྱེ།</translation>
    </message>
    <message>
        <location/>
        <source>Secret</source>
        <translation>གསང་བ།</translation>
    </message>
    <message>
        <location/>
        <source>Top Secret</source>
        <translation>ཆེས་མཐོ་བའི་གསང་བ།</translation>
    </message>
    <message>
        <location/>
        <source>End:</source>
        <translation>མཇུག་སྒྲིལ་བ་སྟེ།</translation>
    </message>
</context>
<context>
    <name>QPluginLoader</name>
    <message>
        <location filename="../src/corelib/plugin/qpluginloader.cpp" line="+24"/>
        <source>Unknown error</source>
        <translation>ཤེས་མེད་པའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location line="-68"/>
        <source>The plugin was not loaded.</source>
        <translation>གློག་སྒུལ་འཕྲུལ་འཁོར་ནང་དུ་བཅུག་མེད།</translation>
    </message>
</context>
<context>
    <name>QPrintDialog</name>
    <message>
        <location filename="../src/gui/painting/qprinterinfo_unix.cpp" line="+98"/>
        <source>locally connected</source>
        <translation>ས་གནས་དེ་གའི་འབྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location line="+23"/>
        <location line="+225"/>
        <source>Aliases: %1</source>
        <translation>ཨ་ལ་སི། %1</translation>
    </message>
    <message>
        <location line="+223"/>
        <location line="+199"/>
        <source>unknown</source>
        <translation>ཤེས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_qws.cpp" line="+375"/>
        <source>Print all</source>
        <translation>ཚང་མ་པར་སྐྲུན་བྱེད་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print selection</source>
        <translation>པར་སྐྲུན་གདམ་གསེས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print range</source>
        <translation>པར་སྐྲུན་ཁྱབ་ཁོངས།</translation>
    </message>
    <message>
        <location line="-48"/>
        <source>A0 (841 x 1189 mm)</source>
        <translation>A0 (841 x 1189 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A1 (594 x 841 mm)</source>
        <translation>A1 (594 x 841 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A2 (420 x 594 mm)</source>
        <translation>A2 (420 x 594 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A3 (297 x 420 mm)</source>
        <translation>A3 (297 x 420 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A4 (210 x 297 mm, 8.26 x 11.7 inches)</source>
        <translation>A4 (210 x 297 mm, 8.26 x 11.7 英寸)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A5 (148 x 210 mm)</source>
        <translation>A5 (148 x 210 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A6 (105 x 148 mm)</source>
        <translation>A6 (105 x 148 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A7 (74 x 105 mm)</source>
        <translation>A7 (74 x 105 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A8 (52 x 74 mm)</source>
        <translation>A8 (52 x 74 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A9 (37 x 52 mm)</source>
        <translation>A9 (37 x 52 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B0 (1000 x 1414 mm)</source>
        <translation>B0 (1000 x 1414 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B1 (707 x 1000 mm)</source>
        <translation>B1 (707 x 1000 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B2 (500 x 707 mm)</source>
        <translation>B2 (500 x 707 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B3 (353 x 500 mm)</source>
        <translation>B3 (353 x 500 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B4 (250 x 353 mm)</source>
        <translation>B4 (250 x 353 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B5 (176 x 250 mm, 6.93 x 9.84 inches)</source>
        <translation>B5 (176 x 250 mm, 6.93 x 9.84 英寸)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B6 (125 x 176 mm)</source>
        <translation>B6 (125 x 176 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B7 (88 x 125 mm)</source>
        <translation>B7 (88 x 125 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B8 (62 x 88 mm)</source>
        <translation>B8 (62 x 88 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B9 (44 x 62 mm)</source>
        <translation>B9 (44 x 62 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B10 (31 x 44 mm)</source>
        <translation>B10 (31 x 44 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>C5E (163 x 229 mm)</source>
        <translation>C5E (163 x 229 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>DLE (110 x 220 mm)</source>
        <translation>DLE (110 x 220 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Executive (7.5 x 10 inches, 191 x 254 mm)</source>
        <translation>ལག་བསྟར་མི་སྣ། (7.5 x 10英寸, 191 x 254 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Folio (210 x 330 mm)</source>
        <translation>ཧྥུ་ལི་ཨོ་(210 x 330 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ledger (432 x 279 mm)</source>
        <translation>རྩིས་ཐོ།(432 x 279 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Legal (8.5 x 14 inches, 216 x 356 mm)</source>
        <translation>ཁྲིམས་མཐུན་གྱི་རིགས། (8.5 x 14英寸, 216 x 356 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Letter (8.5 x 11 inches, 216 x 279 mm)</source>
        <translation>འཕྲིན་ཡིག(8.5 x 11英寸, 216 x 279 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tabloid (279 x 432 mm)</source>
        <translation>ཚགས་པར་ཆུང་ཆུང་(279 x 432 mm)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>US Common #10 Envelope (105 x 241 mm)</source>
        <translation>ཨ་མེ་རི་ཁའི་ཐུན་མོང་གི་#10 ཡིག་ཤུབས་(105 x 241mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_win.cpp" line="+268"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qabstractprintdialog.cpp" line="+110"/>
        <location line="+13"/>
        <location filename="../src/gui/dialogs/qprintdialog_win.cpp" line="-2"/>
        <source>Print</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས།</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="-357"/>
        <source>Print To File ...</source>
        <translation>པར་སྐྲུན་ཡིག་ཆ་ ...</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>File %1 is not writable.
Please choose a different file name.</source>
        <translation>%1ནི་རྩོམ་ཡིག་འབྲི་མི་ཐུབ་པ་ཞིག་རེད།
ཁྱེད་ཀྱིས་ཡིག་ཆའི་མིང་གཞན་ཞིག་འདེམས་རོགས།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1 already exists.
Do you want to overwrite it?</source>
        <translation>%1གནས་ཡོད།
ཁྱོད་ཀྱིས་དེ་བྲིས་ན་ཆོག་གམ།</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_qws.cpp" line="-210"/>
        <source>File exists</source>
        <translation>ཡིག་ཆ་གནས་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;qt&gt;Do you want to overwrite it?&lt;/qt&gt;</source>
        <translation>&lt;qt&gt; ཁྱོད་ཀྱིས་དེ་བྲིས་ན་ཆོག་གམ། &lt;/qt&gt;</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="-8"/>
        <source>%1 is a directory.
Please choose a different file name.</source>
        <translation>%1ནི་དཀར་ཆག་ཅིག་ཡིན།
ཁྱེད་ཀྱིས་ཡིག་ཆའི་མིང་གཞན་ཞིག་འདེམས་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_win.cpp" line="+1"/>
        <source>The &apos;From&apos; value cannot be greater than the &apos;To&apos; value.</source>
        <translation>&apos;ནས་ཡོང་བའི་&apos;རིན་ཐང་དེ་&apos;To&apos;རིན་ཐང་ལས་ཆེ་བ་ཡོང་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupdialog_unix.cpp" line="-232"/>
        <source>A0</source>
        <translation>A0</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A1</source>
        <translation>A1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A2</source>
        <translation>A2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A3</source>
        <translation>A3</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A4</source>
        <translation>A4</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A5</source>
        <translation>A5</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A6</source>
        <translation>A6</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A7</source>
        <translation>A7</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A8</source>
        <translation>A8</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A9</source>
        <translation>A9</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B0</source>
        <translation>B0</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B1</source>
        <translation>B1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B2</source>
        <translation>B2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B3</source>
        <translation>B3</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B4</source>
        <translation>B4</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B5</source>
        <translation>B5</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B6</source>
        <translation>B6</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B7</source>
        <translation>B7</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B8</source>
        <translation>B8</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B9</source>
        <translation>B9</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B10</source>
        <translation>B10</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>C5E</source>
        <translation>C5E</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>DLE</source>
        <translation>DLE</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Executive</source>
        <translation>ལག་བསྟར་མི་སྣ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Folio</source>
        <translation>ཧྥུ་ལི་ཨོ་ཡིས་བཤད་རྒྱུར</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ledger</source>
        <translation>རྩིས་ཐོ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Legal</source>
        <translation>བཅའ་ཁྲིམས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Letter</source>
        <translation>འཕྲིན་ཡིག</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tabloid</source>
        <translation>ཚགས་པར་ཆུང་ཆུང་།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>US Common #10 Envelope</source>
        <translation>ཨ་མེ་རི་ཁའི་ཐུན་མོང་གི་ཡིག་ཤུབས་10</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Folio (8.27 x 13 in)</source>
        <translation>ཧྥུ་ལི་ཨོ་(8.27 x 13 in)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Custom</source>
        <translation>གོམས་སྲོལ།</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="-522"/>
        <location line="+68"/>
        <source>&amp;Options &gt;&gt;</source>
        <translation>&gt; བསལ་འདེམས་ཀྱི་&gt;&gt;</translation>
    </message>
    <message>
        <location line="-63"/>
        <source>&amp;Print</source>
        <translation>»པར་སྐྲུན་ཡིག་རིགས།</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>&amp;Options &lt;&lt;</source>
        <translation>»བསལ་འདེམས་ཀྱི་དབང་ཆ། &lt;&lt;</translation>
    </message>
    <message>
        <location line="+253"/>
        <source>Print to File (PDF)</source>
        <translation>ཡིག་ཚགས་སུ་དཔར་དགོས། (PDF)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print to File (Postscript)</source>
        <translation>ཡིག་ཚགས་སུ་དཔར་དགོས། (Postscript)</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Local file</source>
        <translation>ས་གནས་དེ་གའི་ཡིག་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Write %1 file</source>
        <translation>%1 文件</translation>
    </message>
</context>
<context>
    <name>QPrintPreviewDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qabstractpagesetupdialog.cpp" line="+68"/>
        <location line="+12"/>
        <source>Page Setup</source>
        <translation>ཤོག་ངོས་སྒྲིག་པ།</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintpreviewdialog.cpp" line="+252"/>
        <source>%1%</source>
        <translation>%1%</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Print Preview</source>
        <translation>པར་སྐྲུན་སྔོན་བརྡ།</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Next page</source>
        <translation>ཤོག་ངོས་རྗེས་མ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous page</source>
        <translation>སྔོན་གྱི་ཤོག་ངོས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>First page</source>
        <translation>ཤོག་ངོས་དང་པོ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last page</source>
        <translation>ཤོག་ངོས་སྔོན་མ།</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Fit width</source>
        <translation>ཞེང་ཚད་དང་འཚམ་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fit page</source>
        <translation>ཤོག་ངོས་དང་པོ།</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Zoom in</source>
        <translation>ཆེ་རུ་གཏོང་བ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom out</source>
        <translation>ཆེ་རུ་གཏོང་དགོས།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Portrait</source>
        <translation>འདྲ་པར།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Landscape</source>
        <translation>མཛེས་ལྗོངས།</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Show single page</source>
        <translation>ཤོག་ངོས་རྐྱང་པ་མངོན་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show facing pages</source>
        <translation>མདུན་དུ་ལྷགས་པའི་ཤོག་ལྷེ་མངོན་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show overview of all pages</source>
        <translation>ཤོག་ངོས་ཡོད་ཚད་ཀྱི་གནས་ཚུལ་མདོར་བསྡུས་མངོན་པར་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Print</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page setup</source>
        <translation>ཤོག་ངོས་སྒྲིག་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location line="+151"/>
        <source>Export to PDF</source>
        <translation>PDFལ་ཕྱིར་གཏོང་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Export to PostScript</source>
        <translation>ཡིག་ཆ་ཕྱིར་གཏོང་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>QPrintPropertiesWidget</name>
    <message>
        <location filename="../src/gui/dialogs/qprintpropertieswidget.ui"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location/>
        <source>Page</source>
        <translation>ཤོག་ངོས།</translation>
    </message>
    <message>
        <location/>
        <source>Advanced</source>
        <translation>སྔོན་ཐོན་རང་བཞིན།</translation>
    </message>
</context>
<context>
    <name>QPrintSettingsOutput</name>
    <message>
        <location filename="../src/gui/dialogs/qprintsettingsoutput.ui"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location/>
        <source>Copies</source>
        <translation>འདྲ་བཤུས་</translation>
    </message>
    <message>
        <location/>
        <source>Print range</source>
        <translation>པར་སྐྲུན་ཁྱབ་ཁོངས།</translation>
    </message>
    <message>
        <location/>
        <source>Print all</source>
        <translation>ཚང་མ་པར་སྐྲུན་བྱེད་</translation>
    </message>
    <message>
        <location/>
        <source>Pages from</source>
        <translation>ཤོག་ངོས་ནས་ཡོང་བ་ཡིན།</translation>
    </message>
    <message>
        <location/>
        <source>to</source>
        <translation>དེ་ལྟར་བྱས་ན་</translation>
    </message>
    <message>
        <location/>
        <source>Pages</source>
        <translation>ཤོག་ངོས།</translation>
    </message>
    <message>
        <location/>
        <source>Page Set:</source>
        <translation>ཤོག་ངོས་གཏན་འཁེལ་བྱས་དོན།</translation>
    </message>
    <message>
        <location/>
        <source>Selection</source>
        <translation>འདེམས་སྒྲུག</translation>
    </message>
    <message>
        <location/>
        <source>Output Settings</source>
        <translation>ཐོན་རྫས་ཀྱི་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location/>
        <source>Copies:</source>
        <translation>འདྲ་བཤུས་བྱས་པ་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location/>
        <source>Collate</source>
        <translation>ལེགས་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location/>
        <source>Reverse</source>
        <translation>ལྡོག་ཕྱོགས་སུ་འགྱུར་</translation>
    </message>
    <message>
        <location/>
        <source>Options</source>
        <translation>བསལ་འདེམས་ཀྱི་དབང་ཆ།</translation>
    </message>
    <message>
        <location/>
        <source>Color Mode</source>
        <translation>ཁ་དོག་གི་རྣམ་པ།</translation>
    </message>
    <message>
        <location/>
        <source>Color</source>
        <translation>ཁ་དོག</translation>
    </message>
    <message>
        <location/>
        <source>Grayscale</source>
        <translation>ཐལ་མདོག་གི་གཞི་ཁྱོན།</translation>
    </message>
    <message>
        <location/>
        <source>Duplex Printing</source>
        <translation>གཉིས་ལྡན་པར་སྐྲུན་ཡིག་རིགས།</translation>
    </message>
    <message>
        <location/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location/>
        <source>Long side</source>
        <translation>ཕྱོགས་རིང་པོ།</translation>
    </message>
    <message>
        <location/>
        <source>Short side</source>
        <translation>ཕྱོགས་ཐུང་བ།</translation>
    </message>
</context>
<context>
    <name>QPrintWidget</name>
    <message>
        <location filename="../src/gui/dialogs/qprintwidget.ui"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location/>
        <source>Printer</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Name:</source>
        <translation>&amp;མིང་།</translation>
    </message>
    <message>
        <location/>
        <source>P&amp;roperties</source>
        <translation>P&amp;ཐག་པ་ཉི་མ།</translation>
    </message>
    <message>
        <location/>
        <source>Location:</source>
        <translation>གནས་ཡུལ་ནི།</translation>
    </message>
    <message>
        <location/>
        <source>Preview</source>
        <translation>སྔོན་བརྡ།</translation>
    </message>
    <message>
        <location/>
        <source>Type:</source>
        <translation>རིགས་དབྱིབས་ནི།</translation>
    </message>
    <message>
        <location/>
        <source>Output &amp;file:</source>
        <translation>ཐོན་ཚད ་དང་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>QProcess</name>
    <message>
        <location filename="../src/corelib/io/qprocess_unix.cpp" line="+475"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+147"/>
        <source>Could not open input redirection for reading</source>
        <translation>ཀློག་འདོན་བྱེད་པར་མ་དངུལ་འཇོག་པའི་ཁ་ཕྱེ་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+12"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+36"/>
        <source>Could not open output redirection for writing</source>
        <translation>རྩོམ་ཡིག་འབྲི་བའི་ཆེད་དུ་ཐོན་ཚད་བསྐྱར་དུ་བཀོད་སྒྲིག་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+235"/>
        <source>Resource error (fork failure): %1</source>
        <translation>ཐོན་ཁུངས་ཀྱི་ནོར་འཁྲུལ་(ཁ་དབྲག་ལ་སྐྱོན་ཤོར་བ་):%1</translation>
    </message>
    <message>
        <location line="+259"/>
        <location line="+53"/>
        <location line="+74"/>
        <location line="+67"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+422"/>
        <location line="+50"/>
        <location line="+75"/>
        <location line="+42"/>
        <location line="+54"/>
        <source>Process operation timed out</source>
        <translation>བཟོ་རྩལ་འཁོར་སྐྱོད་དུས་ཚོད་ལས་བརྒལ་བ།</translation>
    </message>
    <message>
        <location filename="../src/corelib/io/qprocess.cpp" line="+533"/>
        <location line="+52"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="-211"/>
        <location line="+50"/>
        <source>Error reading from process</source>
        <translation>གོ་རིམ་ཁྲོད་ནས་ནོར་འཁྲུལ་གྱི་ཀློག་</translation>
    </message>
    <message>
        <location line="+47"/>
        <location line="+779"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+140"/>
        <source>Error writing to process</source>
        <translation>ཐག་གཅོད་བྱེད་པར་ནོར་འཁྲུལ་གྱི་ཡི་</translation>
    </message>
    <message>
        <location line="-709"/>
        <source>Process crashed</source>
        <translation>བཟོ་རྩལ་ཐོར་ཞིག་ཏུ་སོང་</translation>
    </message>
    <message>
        <location line="+912"/>
        <source>No program defined</source>
        <translation>གཏན་འབེབས་བྱས་པའི་གོ་རིམ་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="-341"/>
        <source>Process failed to start</source>
        <translation>གོ་རིམ་མགོ་བརྩམས་མ་ཐུབ་པ།</translation>
    </message>
</context>
<context>
    <name>QProgressDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qprogressdialog.cpp" line="+182"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
</context>
<context>
    <name>QPushButton</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="-8"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
</context>
<context>
    <name>QRadioButton</name>
    <message>
        <location line="+12"/>
        <source>Check</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>QRegExp</name>
    <message>
        <location filename="../src/corelib/tools/qregexp.cpp" line="+64"/>
        <source>no error occurred</source>
        <translation>ནོར་འཁྲུལ་གང་ཡང་བྱུང་མེད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>disabled feature used</source>
        <translation>བཀོལ་སྤྱོད་བྱས་པའི་དབང་པོ་སྐྱོན་ཅན</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad char class syntax</source>
        <translation>སོལ་བ་མི་ལེགས་པའི་གྲལ་རིམ་གྱི་བརྡ་སྤྲོད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad lookahead syntax</source>
        <translation>ཕྱི་ཚུལ་མི་ལེགས་པའི་བརྡ་སྤྲོད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad repetition syntax</source>
        <translation>བསྐྱར་ཟློས་ཀྱི་བརྡ་སྤྲོད་མི་ལེགས་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid octal value</source>
        <translation>གོ་མི་ཆོད་པའི་བརྒྱད་ལྡན་གྱི་རིན་ཐང་།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>missing left delim</source>
        <translation>གཡོན་ཕྱོགས་ཀྱི་ཏི་ལི་མུའུ་གར་སོང་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected end</source>
        <translation>བསམ་ཡུལ་ལས་འདས་པའི་མཐའ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>met internal limit</source>
        <translation>ནང་ཁུལ་གྱི་མཚམས་ཚད་ལ་</translation>
    </message>
</context>
<context>
    <name>QSQLite2Driver</name>
    <message>
        <location filename="../src/sql/drivers/sqlite2/qsql_sqlite2.cpp" line="+396"/>
        <source>Error to open database</source>
        <translation>གཞི་གྲངས་མཛོད་ཀྱི་ཁ་ཕྱེ་བའི་ནོར་</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Unable to begin transaction</source>
        <translation>ཉོ་ཚོང་བྱེད་འགོ་འཛུགས་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to commit transaction</source>
        <translation>ཉོ་ཚོང་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback Transaction</source>
        <translation>ཕྱིར་ལྡོག་བྱེད་ཐབས་བྲལ་བའི་ཉོ་ཚོང་།</translation>
    </message>
</context>
<context>
    <name>QSQLite2Result</name>
    <message>
        <location line="-323"/>
        <source>Unable to fetch results</source>
        <translation>གྲུབ་འབྲས་ཐོབ་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>Unable to execute statement</source>
        <translation>གསལ་བསྒྲགས་ལག་བསྟར་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
</context>
<context>
    <name>QSQLiteDriver</name>
    <message>
        <location filename="../src/sql/drivers/sqlite/qsql_sqlite.cpp" line="+528"/>
        <source>Error opening database</source>
        <translation>ནོར་འཁྲུལ་གྱི་སྒོ་འབྱེད་གཞི་གྲངས་</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error closing database</source>
        <translation>ནོར་འཁྲུལ་གྱི་སྒོ་རྒྱག་པའི་གཞི་</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to begin transaction</source>
        <translation>ཉོ་ཚོང་བྱེད་འགོ་འཛུགས་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to commit transaction</source>
        <translation>ཉོ་ཚོང་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to rollback transaction</source>
        <translation>ཕྱིར་ལྡོག་བྱེད་ཐབས་བྲལ་བའི་ཉོ་ཚོང་།</translation>
    </message>
</context>
<context>
    <name>QSQLiteResult</name>
    <message>
        <location line="-400"/>
        <location line="+66"/>
        <location line="+8"/>
        <source>Unable to fetch row</source>
        <translation>གྲལ་བསྒྲིགས་ནས་གྲལ་བསྒྲིགས་ནས་གྲལ་བསྒྲིགས་ནས་གྲལ་བསྒྲིགས་</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Unable to execute statement</source>
        <translation>གསལ་བསྒྲགས་ལག་བསྟར་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to reset statement</source>
        <translation>གསལ་བསྒྲགས་བསྐྱར་དུ་བཀོད་སྒྲིག་བྱེད་ཐབས་བྲལ།</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Unable to bind parameters</source>
        <translation>གྲངས་ཀ་ཚོད་འཛིན་བྱེད་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Parameter count mismatch</source>
        <translation>གྲངས་ཀ་རྩིས་རྒྱག་ནོར་ཤོར་བ།</translation>
    </message>
    <message>
        <location line="-208"/>
        <source>No query</source>
        <translation>འདྲི་རྩད་བྱས་མི་ཆོག།</translation>
    </message>
</context>
<context>
    <name>QScrollBar</name>
    <message>
        <location filename="../src/gui/widgets/qscrollbar.cpp" line="+448"/>
        <source>Scroll here</source>
        <translation>འདི་ནས་ཕར་འགྲོ་ཚུར་འོང་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Left edge</source>
        <translation>གཡོན་གྱི་མཐའ་རུ།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Top</source>
        <translation>སྟོད་ཆ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right edge</source>
        <translation>གཡས་ཀྱི་མཐའ་རུ།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Bottom</source>
        <translation>མཐིལ་རྟོལ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Page left</source>
        <translation>གཡོན་ངོས་ཀྱི་ཤོག་ལྷེ</translation>
    </message>
    <message>
        <location line="+0"/>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+143"/>
        <source>Page up</source>
        <translation>ཤོག་ངོས་ཡར་བཀྱགས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page right</source>
        <translation>ཤོག་ངོས་གཡས་པ།</translation>
    </message>
    <message>
        <location line="+0"/>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+4"/>
        <source>Page down</source>
        <translation>ཤོག་ངོས་མར་ཕབ་པ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scroll left</source>
        <translation>གཡོན་ཕྱོགས་ཀྱི་ཤོག་ལྷེ།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll up</source>
        <translation>ཡར་བཀྱགས་ནས་ཡར་བཀྱགས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll right</source>
        <translation>གཡས་ཕྱོགས་སུ་དཀྱ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll down</source>
        <translation>མར་འགྲིལ་ནས་མར་འགྲིལ་བ།</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="-6"/>
        <source>Line up</source>
        <translation>གྲལ་བསྒྲིགས་ནས་གྲལ་བསྒྲིགས</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Position</source>
        <translation>གོ་གནས།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Line down</source>
        <translation>གྲལ་བསྒྲིགས་ནས་མར་འབབ་</translation>
    </message>
</context>
<context>
    <name>QSharedMemory</name>
    <message>
        <location filename="../src/corelib/kernel/qsharedmemory.cpp" line="+207"/>
        <source>%1: unable to set key on lock</source>
        <translation>%1:ཟྭ་སྟེང་གི་ལྡེ་མིག་གཏན་འཁེལ་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+81"/>
        <source>%1: create size is less then 0</source>
        <translation>%1:གསར་སྐྲུན་གྱི་གཞི་ཁྱོན་དེ་སྐབས་དེར་0ཉུང་དུ་ཕྱིན་ཡོད།</translation>
    </message>
    <message>
        <location line="+168"/>
        <location filename="../src/corelib/kernel/qsharedmemory_p.h" line="+148"/>
        <source>%1: unable to lock</source>
        <translation>%1:ཟྭ་རྒྱག་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>%1: unable to unlock</source>
        <translation>%1:ཟྭ་རྒྱག་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../src/corelib/kernel/qsharedmemory_unix.cpp" line="+78"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+87"/>
        <source>%1: permission denied</source>
        <translation>%1:ཆོག་མཆན་མ་ཐོབ་པ།</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="-22"/>
        <source>%1: already exists</source>
        <translation>%1:གནས་ཟིན་པ།</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+9"/>
        <source>%1: doesn&apos;t exists</source>
        <translation>%1:གནས་མེད་པ།</translation>
    </message>
    <message>
        <location line="+6"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+9"/>
        <source>%1: out of resources</source>
        <translation>%1:ཐོན་ཁུངས་ལས་བརྒལ་བ།</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+7"/>
        <source>%1: unknown error %2</source>
        <translation>%1:ཤེས་མེད་པའི་ནོར་འཁྲུལ་གྱི་%2</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>%1: key is empty</source>
        <translation>%1:ལྡེ་མིག་ནི་སྟོང་བ་ཡིན།</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>%1: unix key file doesn&apos;t exists</source>
        <translation>%1:unix key file མེད་པ།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>%1: ftok failed</source>
        <translation>%1:ftokལ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location line="+51"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+15"/>
        <source>%1: unable to make key</source>
        <translation>%1:ལྡེ་མིག་བཟོ་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>%1: system-imposed size restrictions</source>
        <translation>%1:ལམ་ལུགས་ཀྱིས་ལག་བསྟར་བྱེད་པའི་གཞི་ཁྱོན་ལ་ཚོད་འཛིན་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>%1: not attached</source>
        <translation>%1:ཟུར་བཀོད་བྱས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="-27"/>
        <source>%1: invalid size</source>
        <translation>%1:གོ་མི་ཆོད་པའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>%1: key error</source>
        <translation>%1:འགག་རྩའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>%1: size query failed</source>
        <translation>%1:ཆེ་ཆུང་འདྲི་རྩད་ལ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>QShortcut</name>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="+373"/>
        <source>Space</source>
        <translation>བར་སྟོང་།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Esc</source>
        <translation>ཨེ་སི་ཁོ་ཡིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tab</source>
        <translation>རེའུ་མིག</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Backtab</source>
        <translation>རྒྱབ་རྟེན་དངོས་པོ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Backspace</source>
        <translation>རྒྱབ་ཕྱོགས་ཀྱི་བར་སྟོང་།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Return</source>
        <translation>ཕྱིར་སློག་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Enter</source>
        <translation>ནང་དུ་འཛུལ་བ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ins</source>
        <translation>ནང་ཁུལ་དུ་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Del</source>
        <translation>ཏེ་ཨར།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pause</source>
        <translation>མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>SysReq</source>
        <translation>SysReq</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Home</source>
        <translation>ཁྱིམ་ཚང་།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>End</source>
        <translation>མཇུག་སྒྲིལ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Left</source>
        <translation>གཡོན་ཕྱོགས་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Up</source>
        <translation>ཡར་ལངས་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right</source>
        <translation>གཡས་ཕྱོགས་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Down</source>
        <translation>མར་ཕབ་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PgUp</source>
        <translation>PgUp</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PgDown</source>
        <translation>PgDown</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CapsLock</source>
        <translation>ཁ་ལོ་བ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>NumLock</source>
        <translation>ནུའུ་མུའུ་ལོ་ཁེ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ScrollLock</source>
        <translation>ཤོག་ལྷེ་གཅིག་གི་ནང་དུ་བཅུག་ཡོད</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Menu</source>
        <translation>ཟས་ཐོ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Back</source>
        <translation>ཕྱིར་ལོག་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Forward</source>
        <translation>མདུན་དུ་སྐྱོད་པ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop</source>
        <translation>མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Refresh</source>
        <translation>དྭངས་ཤིང་གཙང་བ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Down</source>
        <translation>སྒྲ་གདངས་མར་ཆག་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Mute</source>
        <translation>སྒྲ་གདངས་ཀྱི་སྒྲ་གདངས་མི་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Up</source>
        <translation>སྒྲ་གདངས་ཇེ་མཐོར་གཏོང་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Boost</source>
        <translation>པ་སེ་ཡིས་སྐུལ་འདེད་བྱས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Up</source>
        <translation>པ་སེ་ཡར་བཀྱགས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Down</source>
        <translation>པ་སེ་མར་ཕབ་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Treble Up</source>
        <translation>འདར་སིག་སིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Treble Down</source>
        <translation>འདར་སིག་སིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Play</source>
        <translation>སྨྱན་སྦྱོར་གྱི་རྩེད་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Stop</source>
        <translation>སྨྱན་སྦྱོར་མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Previous</source>
        <translation>སྨྱན་སྦྱོར་གྱི་སྔོན་ཆད</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Next</source>
        <translation>སྨྱན་སྦྱོར་གྱི་རྗེས་མ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Record</source>
        <translation>སྨྱན་སྦྱོར་གྱི་ཟིན་ཐོ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Favorites</source>
        <translation>དགའ་པོ་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Standby</source>
        <translation>བཀའ་སྒུག་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open URL</source>
        <translation>ཁ་ཕྱེ་བའི་URL</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch Mail</source>
        <translation>སྦྲག་རྫས་སྐུར་སྐྱེལ་བྱེད་འགོ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch Media</source>
        <translation>སྨྱན་སྦྱོར་སྤེལ་འགོ་ཚུགས་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (0)</source>
        <translation>སྐུལ་སློང་བྱེད་འགོ་ཚུགས་པ། (0)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (1)</source>
        <translation>སྐུལ་སློང་བྱེད་འགོ་ཚུགས་པ། (1)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (2)</source>
        <translation>སྐུལ་སློང་བྱེད་འགོ་ཚུགས་པ། (2)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (3)</source>
        <translation>སྐུལ་སློང་བྱེད་འགོ་ཚུགས་པ། (3)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (4)</source>
        <translation>འཕེན་གཏོང་བྱེད་འགོ་ཚུགས་པ། (4)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (5)</source>
        <translation>འཕེན་གཏོང་བྱེད་འགོ་ཚུགས་པ། (5)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (6)</source>
        <translation>སྐུལ་སློང་བྱེད་འགོ་ཚུགས་པ། (6)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (7)</source>
        <translation>སྐུལ་སློང་བྱེད་འགོ་ཚུགས་པ། (བདུན། )</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (8)</source>
        <translation>སྐུལ་སློང་བྱེད་འགོ་ཚུགས་པ། (8)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (9)</source>
        <translation>སྐུལ་སློང་བྱེད་འགོ་ཚུགས་པ། (9)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (A)</source>
        <translation>འཕེན་གཏོང་བྱེད་འགོ་ཚུགས་པ། (A)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (B)</source>
        <translation>འཕེན་གཏོང་བྱེད་འགོ་ཚུགས་པ། (B)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (C)</source>
        <translation>འཕེན་གཏོང་བྱེད་འགོ་ཚུགས་པ། (C)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (D)</source>
        <translation>འཕེན་གཏོང་(D)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (E)</source>
        <translation>འཕེན་གཏོང་(E)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (F)</source>
        <translation>འཕེན་གཏོང་(F)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Print Screen</source>
        <translation>པར་སྐྲུན་བརྙན་ཤེལ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page Up</source>
        <translation>ཤོག་ངོས་ཡར་བཀྱགས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page Down</source>
        <translation>ཤོག་ངོས་མར་ཕབ་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Caps Lock</source>
        <translation>ཡིག་ཆེན།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Num Lock</source>
        <translation>ནུའུ་མུའུ་ཟྭ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Number Lock</source>
        <translation>ཨང་རྟགས་ཀྱི་སྒོ་ལྕགས</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll Lock</source>
        <translation>སྒོ་ལྕགས་རྒྱག་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insert</source>
        <translation>ནང་དུ་འཇུག་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Escape</source>
        <translation>བྲོས་བྱོལ་དུ་སོང་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>System Request</source>
        <translation>ལམ་ལུགས་ཀྱི་བླང་བྱ།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Select</source>
        <translation>བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>No</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Context1</source>
        <translation>རྒྱབ་ལྗོངས་1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context2</source>
        <translation>རྒྱབ་ལྗོངས་2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context3</source>
        <translation>རྒྱབ་ལྗོངས་3</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context4</source>
        <translation>རྒྱབ་ལྗོངས་4</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Call</source>
        <translation>ཁ་པར་གཏོང་བ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hangup</source>
        <translation>ཁ་པར་རྒྱག་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Flip</source>
        <translation>ཕར་སློག་ཚུར་སློག་</translation>
    </message>
    <message>
        <location line="+527"/>
        <location line="+122"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location line="-121"/>
        <location line="+125"/>
        <source>Shift</source>
        <translation>ལས་རེས་བརྗེ་བ།</translation>
    </message>
    <message>
        <location line="-124"/>
        <location line="+122"/>
        <source>Alt</source>
        <translation>ཨོ་ཐེ་ཨར།</translation>
    </message>
    <message>
        <location line="-121"/>
        <location line="+117"/>
        <source>Meta</source>
        <translation>ལྕགས་རིགས།</translation>
    </message>
    <message>
        <location line="-25"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>F%1</source>
        <translation>F%1</translation>
    </message>
    <message>
        <location line="-720"/>
        <source>Home Page</source>
        <translation>ཁྱིམ་ཚང་གི་ཤོག་ལྷེ</translation>
    </message>
</context>
<context>
    <name>QSlider</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+151"/>
        <source>Page left</source>
        <translation>གཡོན་ངོས་ཀྱི་ཤོག་ལྷེ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page up</source>
        <translation>ཤོག་ངོས་ཡར་བཀྱགས།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Position</source>
        <translation>གོ་གནས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Page right</source>
        <translation>ཤོག་ངོས་གཡས་པ།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page down</source>
        <translation>ཤོག་ངོས་མར་ཕབ་པ།</translation>
    </message>
</context>
<context>
    <name>QSocks5SocketEngine</name>
    <message>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="-67"/>
        <source>Connection to proxy refused</source>
        <translation>ངོ་ཚབ་དང་འབྲེལ་བ་ཡོད་པའི་འབྲེལ་མཐུད</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection to proxy closed prematurely</source>
        <translation>སྔ་མོ་ནས་བཀག་སྡོམ་བྱས་པའི་ཚབ་བྱེད་མི་སྣ་དང</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Proxy host not found</source>
        <translation>ཚབ་བྱེད་བདག་པོས་མ་རྙེད་པ།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Connection to proxy timed out</source>
        <translation>དུས་ཐོག་ཏུ་ཕྱིར་འཐེན་བྱས་པའི་ཚབ་བྱེད་མི་སྣ་</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Proxy authentication failed</source>
        <translation>ངོ་ཚབ་ཀྱི་བདེན་དཔང་ར་སྤྲོད་བྱེད་མ་ཐུབ་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Proxy authentication failed: %1</source>
        <translation>ངོ་ཚབ་ཀྱི་བདེན་དཔང་ར་སྤྲོད་བྱེད་མ་ཐུབ་པ། %1</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>SOCKS version 5 protocol error</source>
        <translation>SOCKSཔར་གཞི་5ཡི་གྲོས་མཐུན་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>General SOCKSv5 server failure</source>
        <translation>སྤྱིར་བཏང་གི་SOCKSv5ཞབས་ཞུའི་ཡོ་བྱད་ལ་སྐྱོན་ཤོར་བ།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection not allowed by SOCKSv5 server</source>
        <translation>SOCKSv5服务器་ཀྱིས་འབྲེལ་མཐུད་བྱས་མི་ཆོག།</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>TTL expired</source>
        <translation>TTL དུས་ལས་ཡོལ་ཟིན།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>SOCKSv5 command not supported</source>
        <translation>SOCKSv5ཡི་བཀོད་འདོམས་ལ་རྒྱབ་སྐྱོར་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Address type not supported</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་སྡོད་གནས་ཀྱི་རིགས</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown SOCKSv5 proxy error code 0x%1</source>
        <translation>SOCKSv5代理错误代码 0x%1</translation>
    </message>
    <message>
        <source>Socks5 timeout error connecting to socks server</source>
        <translation type="obsolete">连接到套接字服务器的时候，Socks5超时错误</translation>
    </message>
    <message>
        <location line="+685"/>
        <source>Network operation timed out</source>
        <translation>དྲ་རྒྱའི་འཁོར་སྐྱོད་དུས་ཚོད་ཕྱིར་འཐེན་བྱས</translation>
    </message>
</context>
<context>
    <name>QSpinBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="-574"/>
        <source>More</source>
        <translation>དེ་བས་ཀྱང་མང་</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Less</source>
        <translation>ཉུང་ཙམ་ལས་མེད་</translation>
    </message>
</context>
<context>
    <name>QSql</name>
    <message>
        <location filename="../src/qt3support/sql/q3sqlmanager_p.cpp" line="+890"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete this record?</source>
        <translation>ཟིན་ཐོ་འདི་བསུབ་དགོས་སམ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+16"/>
        <location line="+36"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location line="-51"/>
        <location line="+16"/>
        <location line="+36"/>
        <source>No</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location line="-44"/>
        <source>Insert</source>
        <translation>ནང་དུ་འཇུག་པ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save edits?</source>
        <translation>རྩོམ་སྒྲིག་གྲོན་ཆུང་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel your edits?</source>
        <translation>ཁྱོད་ཀྱི་རྩོམ་སྒྲིག་མེད་པར་བཟོ་དགོས་སམ།</translation>
    </message>
</context>
<context>
    <name>QSslSocket</name>
    <message>
        <location filename="../src/network/ssl/qsslsocket_openssl.cpp" line="+569"/>
        <source>Unable to write data: %1</source>
        <translation>གཞི་གྲངས་འབྲི་ཐབས་བྲལ་བ། %1</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>Error while reading: %1</source>
        <translation>ཀློག་ཞོར་དུ་ནོར་འཁྲུལ་བྱུང་བ་སྟེ། %1</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>Error during SSL handshake: %1</source>
        <translation>SSLལག་འཇུ་བྱེད་སྐབས་ནོར་འཁྲུལ་བྱུང་བ་སྟེ། %1</translation>
    </message>
    <message>
        <location line="-524"/>
        <source>Error creating SSL context (%1)</source>
        <translation>SSLཡི་རྒྱབ་ལྗོངས་གསར་སྐྲུན་བྱེད་པར་ནོར་འཁྲུལ་བྱུང་བ་རེད། (%1)</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Invalid or empty cipher list (%1)</source>
        <translation>གོ་མི་ཆོད་པའམ་ཡང་ན་སྟོང་བའི་ཡི་གེའི་རེའུ་མིག(%1)</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Error creating SSL session, %1</source>
        <translation>SSLཚོགས་འདུ་གསར་སྐྲུན་བྱེད་པར་ནོར་འཁྲུལ་བྱུང་བ།%1</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Error creating SSL session: %1</source>
        <translation>SSLཚོགས་འདུ་གསར་སྐྲུན་བྱེད་པར་ནོར་འཁྲུལ་བྱུང་བ་སྟེ། %1</translation>
    </message>
    <message>
        <location line="-61"/>
        <source>Cannot provide a certificate with no key, %1</source>
        <translation>ལྡེ་མིག་མེད་པའི་དཔང་ཡིག་འདོན་སྤྲོད་བྱེད་མི་ཐུབ།%1</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error loading local certificate, %1</source>
        <translation>ས་གནས་དེ་གའི་དཔང་ཡིག་ལ་ནོར་འཁྲུལ་བྱུང་ན་%1</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Error loading private key, %1</source>
        <translation>སྒེར་གྱི་ལྡེ་མིག་ནང་དུ་བཅུག་པའི་ནོར་འཁྲུལ།%1</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Private key does not certificate public key, %1</source>
        <translation>སྒེར་གྱི་ལྡེ་མིག་གིས་སྤྱི་པའི་ལྡེ་མིག་ལ་དཔང་ཡིག་མི་སྤྲོད་པ་དང་། %1</translation>
    </message>
</context>
<context>
    <name>QSystemSemaphore</name>
    <message>
        <location filename="../src/corelib/kernel/qsystemsemaphore_unix.cpp" line="-41"/>
        <location filename="../src/corelib/kernel/qsystemsemaphore_win.cpp" line="+66"/>
        <source>%1: out of resources</source>
        <translation>%1:ཐོན་ཁུངས་ལས་བརྒལ་བ།</translation>
    </message>
    <message>
        <location line="-13"/>
        <location filename="../src/corelib/kernel/qsystemsemaphore_win.cpp" line="+4"/>
        <source>%1: permission denied</source>
        <translation>%1:ཆོག་མཆན་མ་ཐོབ་པ།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: already exists</source>
        <translation>%1:གནས་ཟིན་པ།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: does not exist</source>
        <translation>%1:གནས་མེད་པ།</translation>
    </message>
    <message>
        <location line="+9"/>
        <location filename="../src/corelib/kernel/qsystemsemaphore_win.cpp" line="+3"/>
        <source>%1: unknown error %2</source>
        <translation>%1:ཤེས་མེད་པའི་ནོར་འཁྲུལ་གྱི་%2</translation>
    </message>
</context>
<context>
    <name>QTDSDriver</name>
    <message>
        <location filename="../src/sql/drivers/tds/qsql_tds.cpp" line="+584"/>
        <source>Unable to open connection</source>
        <translation>འབྲེལ་མཐུད་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unable to use database</source>
        <translation>གཞི་གྲངས་མཛོད་བཀོལ་སྤྱོད་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
</context>
<context>
    <name>QTabBar</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="-326"/>
        <source>Scroll Left</source>
        <translation>གཡོན་ཕྱོགས་སུ་ཤོག་ལྷེ་གཅིག</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll Right</source>
        <translation>གཡས་ཕྱོགས་སུ་དཀྱ</translation>
    </message>
</context>
<context>
    <name>QTcpServer</name>
    <message>
        <location filename="../src/network/socket/qtcpserver.cpp" line="+282"/>
        <source>Operation on socket is not supported</source>
        <translation>བསྒར་ཁུང་སྟེང་གི་བྱ་སྤྱོད་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
</context>
<context>
    <name>QTextControl</name>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="+1973"/>
        <source>&amp;Undo</source>
        <translation>་་</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Redo</source>
        <translation>ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་བསྐྱར་དུ་</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cu&amp;t</source>
        <translation>Cu&gt;t</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Copy</source>
        <translation>&amp; འདྲ་བཤུས།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Copy &amp;Link Location</source>
        <translation>འདྲ་བཤུས་དང་སྦྲེལ་མཐུད་བྱེད་ས།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Paste</source>
        <translation>&amp; paste</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Select All</source>
        <translation>ཚང་མ་བདམས་པ་</translation>
    </message>
</context>
<context>
    <name>QToolButton</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="+254"/>
        <location line="+6"/>
        <source>Press</source>
        <translation>གསར་འགྱུར་དཔེ་སྐྲུན་</translation>
    </message>
    <message>
        <location line="-4"/>
        <location line="+8"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
</context>
<context>
    <name>QUdpSocket</name>
    <message>
        <location filename="../src/network/socket/qudpsocket.cpp" line="+169"/>
        <source>This platform does not support IPv6</source>
        <translation>སྟེགས་བུ་འདིས་IPv6ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
</context>
<context>
    <name>QUndoGroup</name>
    <message>
        <location filename="../src/gui/util/qundogroup.cpp" line="+386"/>
        <source>Undo</source>
        <translation>ཁ་ཕྱིར་འཐེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Redo</source>
        <translation>ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་བསྐྱར</translation>
    </message>
</context>
<context>
    <name>QUndoModel</name>
    <message>
        <location filename="../src/gui/util/qundoview.cpp" line="+101"/>
        <source>&lt;empty&gt;</source>
        <translation>&lt;empty&gt;</translation>
    </message>
</context>
<context>
    <name>QUndoStack</name>
    <message>
        <location filename="../src/gui/util/qundostack.cpp" line="+834"/>
        <source>Undo</source>
        <translation>ཁ་ཕྱིར་འཐེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Redo</source>
        <translation>ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་བསྐྱར</translation>
    </message>
</context>
<context>
    <name>QUnicodeControlCharacterMenu</name>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="+884"/>
        <source>LRM Left-to-right mark</source>
        <translation>LRM གཡོན་ནས་གཡས་བར་གྱི་རྟགས་རྒྱག་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLM Right-to-left mark</source>
        <translation>མི་དམངས་བཅིངས་འགྲོལ་དམག་གི་གཡས་གཡོན་གྱི་རྟགས་མཚན།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWJ Zero width joiner</source>
        <translation>ZWJཡི་ཞེང་ཚད་ཀླད་ཀོར་གྱི་ཞེང་ཚད་དང་མཉམ་འབྲེལ་བྱེད</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWNJ Zero width non-joiner</source>
        <translation>ZWNJཡི་ཞེང་ཚད་ནི་མཉམ་འབྲེལ་བྱེད་མཁན་མིན་པ་ཞིག་ཡིན།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWSP Zero width space</source>
        <translation>ZWSPཡི་ཞེང་ཚད་ཀླད་ཀོར་གྱི་བར་སྟོང་།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>LRE Start of left-to-right embedding</source>
        <translation>གཡས་གཡོན་དུ་ནང་འཇུག་བྱེད་པའི་LREགནས་འགོ་ཚུགས་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLE Start of right-to-left embedding</source>
        <translation>གཡས་གཡོན་དུ་ནང་འཇུག་བྱེད་པའི་RLEའགར་འགོ་ཚུགས་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>LRO Start of left-to-right override</source>
        <translation>གཡས་གཡོན་གྱི་མགོ་མཇུག་བར་གསུམ་དུ་LROཡི་མགོ་བརྩམས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLO Start of right-to-left override</source>
        <translation>གཡས་གཡོན་དུ་མགོ་མཇུག་སློག་པའི་RLOའགས་འགོ་ཚུགས་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PDF Pop directional formatting</source>
        <translation>PDFདམར་ཁྱབ་ཆེ་བའི་ཁ་ཕྱོགས་རྣམ་གཞག</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Insert Unicode control character</source>
        <translation>Unicode ཚོད་འཛིན་གྱི་ཡི་གེ་ནང་འཇུག་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>QWebFrame</name>
    <message>
        <location filename="../src/3rdparty/webkit/WebKit/qt/WebCoreSupport/FrameLoaderClientQt.cpp" line="+692"/>
        <source>Request cancelled</source>
        <translation>ཕྱིར་འཐེན་བྱ་རྒྱུའི་རེ་བ་</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Request blocked</source>
        <translation>བཀག་སྡོམ་བྱས་པའི་རེ་བ་</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot show URL</source>
        <translation>URLམ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Frame load interruped by policy change</source>
        <translation>སྲིད་ཇུས་ཀྱི་འགྱུར་ལྡོག་གིས་ཐེ་གཏོགས་བྱས་པའི་སྒྲོམ་གཞིའི་ཐེག་ཚད།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot show mimetype</source>
        <translation>ལད་མོ་བྱེད་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>File does not exist</source>
        <translation>ཡིག་ཆ་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>QWebPage</name>
    <message>
        <location filename="../src/3rdparty/webkit/WebCore/platform/network/qt/QNetworkReplyHandler.cpp" line="+382"/>
        <source>Bad HTTP request</source>
        <translation>HTTPཡི་རེ་བ་མི་ལེགས་པ</translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebCore/platform/qt/Localizations.cpp" line="+42"/>
        <source>Submit</source>
        <comment>default label for Submit buttons in forms on web pages</comment>
        <translation>གོང་འབུལ་ཞུས།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Submit</source>
        <comment>Submit (input element) alt text for &lt;input&gt; elements with no alt, title, or value</comment>
        <translation>གོང་འབུལ་ཞུས།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Reset</source>
        <comment>default label for Reset buttons in forms on web pages</comment>
        <translation>བསྐྱར་དུ་བཀོད་སྒྲིག་བྱེད་པ</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>This is a searchable index. Enter search keywords: </source>
        <comment>text that appears at the start of nearly-obsolete web pages in the form of a &apos;searchable index&apos;</comment>
        <translation>འདི་ནི་འཚོལ་ཞིབ་བྱས་ཆོག་པའི་སྟོན་གྲངས་ཤིག་རེད། འཚོལ་བཤེར་གྱི་འགག་རྩའི་ཐ་སྙད་ནང་འཇུག་བྱ་རྒྱུ་སྟེ </translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Choose File</source>
        <comment>title for file button used in HTML forms</comment>
        <translation>ཡིག་ཆ་འདེམས་པ།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No file selected</source>
        <comment>text to display in file button used in HTML forms when no file is selected</comment>
        <translation>ཡིག་ཆ་བདམས་མེད་པ།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open in New Window</source>
        <comment>Open in New Window context menu item</comment>
        <translation>སྒེའུ་ཁུང་གསར་པའི་ནང་སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save Link...</source>
        <comment>Download Linked File context menu item</comment>
        <translation>སྦྲེལ་མཐུད་གྲོན་ཆུང་བྱ་དགོས།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy Link</source>
        <comment>Copy Link context menu item</comment>
        <translation>འདྲ་བཤུས་སྦྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open Image</source>
        <comment>Open Image in New Window context menu item</comment>
        <translation>པར་རིས་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save Image</source>
        <comment>Download Image context menu item</comment>
        <translation>པར་རིས་ཉར་ཚགས་བྱེད་</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy Image</source>
        <comment>Copy Link context menu item</comment>
        <translation>འདྲ་བཤུས་ཀྱི་པར་རིས</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open Frame</source>
        <comment>Open Frame in New Window context menu item</comment>
        <translation>ཁ་ཕྱེ་བའི་སྒྲོམ་གཞི།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy</source>
        <comment>Copy context menu item</comment>
        <translation>འདྲ་བཤུས་</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Go Back</source>
        <comment>Back context menu item</comment>
        <translation>ཕྱིར་རྒྱུགས།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Go Forward</source>
        <comment>Forward context menu item</comment>
        <translation>མདུན་དུ་བསྐྱོད་དགོས།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Stop</source>
        <comment>Stop context menu item</comment>
        <translation>མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Reload</source>
        <comment>Reload context menu item</comment>
        <translation>བསྐྱར་དུ་ཕབ་ལེན་བྱ་དགོས</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cut</source>
        <comment>Cut context menu item</comment>
        <translation>འབྲེག་པ།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Paste</source>
        <comment>Paste context menu item</comment>
        <translation>འབྱར་བག་ཅན།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No Guesses Found</source>
        <comment>No Guesses Found context menu item</comment>
        <translation>ཚོད་དཔག་བྱས་པ་ལྟར་ན་</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ignore</source>
        <comment>Ignore Spelling context menu item</comment>
        <translation>སྣང་མེད་དུ་བཞག་</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Add To Dictionary</source>
        <comment>Learn Spelling context menu item</comment>
        <translation>ཚིག་མཛོད་ནང་དུ་ཁ་སྣོན་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Search The Web</source>
        <comment>Search The Web context menu item</comment>
        <translation>དྲ་རྒྱ་འཚོལ་ཞིབ་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Look Up In Dictionary</source>
        <comment>Look Up in Dictionary context menu item</comment>
        <translation>ཚིག་མཛོད་ནང་དུ་མགོ་བོ་ཡར་བཀྱགས་</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open Link</source>
        <comment>Open Link context menu item</comment>
        <translation>སྒོ་འབྱེད་སྦྲེལ་མཐུད</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ignore</source>
        <comment>Ignore Grammar context menu item</comment>
        <translation>སྣང་མེད་དུ་བཞག་</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Spelling</source>
        <comment>Spelling and Grammar context sub-menu item</comment>
        <translation>ཚེག་རྡར་བྱས་ནས་ཚེ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show Spelling and Grammar</source>
        <comment>menu item title</comment>
        <translation>ཡི་གེ་འབྲི་བ་དང་བརྡ་སྤྲོད་ཀྱི་རྣམ་འགྱུར་སྟོན་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide Spelling and Grammar</source>
        <comment>menu item title</comment>
        <translation>ཡིབ་པའི་ཡི་གེ་དང་བརྡ་སྤྲོད།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Check Spelling</source>
        <comment>Check spelling context menu item</comment>
        <translation>ཞིབ་བཤེར་བྱས་ནས་ཚེག་རྡར་</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Check Spelling While Typing</source>
        <comment>Check spelling while typing context menu item</comment>
        <translation>ཡི་གེ་འབྲི་ཞོར་དུ་ཚེག་རྡར་བྱས་ནས་ཚེག་</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Check Grammar With Spelling</source>
        <comment>Check grammar with spelling context menu item</comment>
        <translation>ཚེག་རྡར་བྱས་ནས་བརྡ་སྤྲོད་ལ་ཞིབ་བཤེར་བྱས།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Fonts</source>
        <comment>Font context sub-menu item</comment>
        <translation>ཡིག་གཟུགས།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Bold</source>
        <comment>Bold context menu item</comment>
        <translation>སྤོབས་པ་ཆེ་བ།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Italic</source>
        <comment>Italic context menu item</comment>
        <translation>དབྱི་སི་ལན་ཆོས་ཁང་།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Underline</source>
        <comment>Underline context menu item</comment>
        <translation>གསལ་བཤད་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Outline</source>
        <comment>Outline context menu item</comment>
        <translation>རྩ་གནད།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Direction</source>
        <comment>Writing direction context sub-menu item</comment>
        <translation>ཁ་ཕྱོགས།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Text Direction</source>
        <comment>Text direction context sub-menu item</comment>
        <translation>ཡི་གེའི་ཁ་ཕྱོགས།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Default</source>
        <comment>Default writing direction context menu item</comment>
        <translation>ཁ་ཆད་དང་འགལ་</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>LTR</source>
        <comment>Left to Right context menu item</comment>
        <translation>LTR</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>RTL</source>
        <comment>Right to Left context menu item</comment>
        <translation>RTL</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Inspect</source>
        <comment>Inspect Element context menu item</comment>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No recent searches</source>
        <comment>Label for only item in menu that appears when clicking on the search field image, when no searches have been performed</comment>
        <translation>ཉེ་ཆར་སྔོག་བཤེར་བྱས་མེད།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Recent searches</source>
        <comment>label for first item in the menu that appears when clicking on the search field image, used as embedded menu title</comment>
        <translation>ཉེ་ཆར་འཚོལ་ཞིབ་བྱས།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Clear recent searches</source>
        <comment>menu item in Recent Searches menu that empties menu&apos;s contents</comment>
        <translation>ཉེ་ཆར་འཚོལ་ཞིབ་བྱས་པ་</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>Unknown</source>
        <comment>Unknown filesize FTP directory listing item</comment>
        <translation>ཤེས་མེད་པ།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 (%2x%3 pixels)</source>
        <comment>Title string for images</comment>
        <translation>%1 (%2x%3 pixels)</translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebKit/qt/WebCoreSupport/InspectorClientQt.cpp" line="+185"/>
        <source>Web Inspector - %2</source>
        <translation>དྲ་རྒྱའི་ལྟ་ཞིབ་པ་ - %2</translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebCore/platform/qt/ScrollbarQt.cpp" line="+58"/>
        <source>Scroll here</source>
        <translation>འདི་ནས་ཕར་འགྲོ་ཚུར་འོང་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Left edge</source>
        <translation>གཡོན་གྱི་མཐའ་རུ།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Top</source>
        <translation>སྟོད་ཆ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right edge</source>
        <translation>གཡས་ཀྱི་མཐའ་རུ།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Bottom</source>
        <translation>མཐིལ་རྟོལ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Page left</source>
        <translation>གཡོན་ངོས་ཀྱི་ཤོག་ལྷེ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page up</source>
        <translation>ཤོག་ངོས་ཡར་བཀྱགས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page right</source>
        <translation>ཤོག་ངོས་གཡས་པ།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page down</source>
        <translation>ཤོག་ངོས་མར་ཕབ་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Scroll left</source>
        <translation>གཡོན་ཕྱོགས་ཀྱི་ཤོག་ལྷེ།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll up</source>
        <translation>ཡར་བཀྱགས་ནས་ཡར་བཀྱགས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll right</source>
        <translation>གཡས་ཕྱོགས་སུ་དཀྱ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll down</source>
        <translation>མར་འགྲིལ་ནས་མར་འགྲིལ་བ།</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/3rdparty/webkit/WebCore/platform/qt/FileChooserQt.cpp" line="+45"/>
        <source>%n file(s)</source>
        <comment>number of chosen file</comment>
        <translation>
            <numerusform>%n ཡིག་ཆ།</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebKit/qt/Api/qwebpage.cpp" line="+1322"/>
        <source>JavaScript Alert - %1</source>
        <translation>JavaScript Alert - %1</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>JavaScript Confirm - %1</source>
        <translation>JavaScript Confirm - %1</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>JavaScript Prompt - %1</source>
        <translation>JavaScript Prompt - %1</translation>
    </message>
    <message>
        <location line="+333"/>
        <source>Move the cursor to the next character</source>
        <translation>མི་སྣ་རྗེས་མའི་ཁྲོད་དུ་སོང་ནས་འཁྲབ་སྟོན་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the previous character</source>
        <translation>༄༅།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the next word</source>
        <translation>༄༅།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the previous word</source>
        <translation>༄༅།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the next line</source>
        <translation>འཛིན་ཆས་དེ་ཐིག་རྗེས་མར་སྤོར་དགོས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the previous line</source>
        <translation>གསལ་བྱེད་དེ་སྔོན་གྱི་ཐིག་ལ་སྤོར་དགོས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the start of the line</source>
        <translation>གསལ་བྱེད་དེ་ཐིག་གི་མགོ་བརྩམས་སར་སྤོར་དགོས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the end of the line</source>
        <translation>གསལ་བྱེད་དེ་ཐིག་གི་མཇུག་ཏུ་སྤོར་དགོས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the start of the block</source>
        <translation>འཛིན་ཆས་དེ་སྲང་ལམ་གྱི་མགོ་བརྩམས་སར་སྤོར་དགོས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the end of the block</source>
        <translation>འཛིན་ཆས་དེ་སྲང་ལམ་གྱི་མཐའ་རུ་སྤོར་དགོས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the start of the document</source>
        <translation>ཡིག་ཆའི་མགོ་བརྩམས་པའི་དུས་སུ་འཁྱོག་ཐིག་སྤོར་དགོས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the end of the document</source>
        <translation>འཛིན་ཡིག་གི་མཇུག་ཏུ་སྤོར་དགོས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select all</source>
        <translation>ཚང་མ་བདམས་ཐོན་བྱུང་</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the next character</source>
        <translation>མི་སྣ་རྗེས་མར་གདམ་གསེས་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the previous character</source>
        <translation>སྔོན་ཆད་ཀྱི་མི་སྣར་གདམ་གསེས་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the next word</source>
        <translation>ཡི་གེ་རྗེས་མར་གདམ་གསེས་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the previous word</source>
        <translation>སྔོན་ཆད་ཀྱི་ཡི་གེ་བདམས་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the next line</source>
        <translation>རྗེས་མའི་ལམ་ཐིག་ལ་བདམས་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the previous line</source>
        <translation>སྔོན་གྱི་ལམ་ཐིག་ལ་བདམས་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the start of the line</source>
        <translation>ལམ་ཐིག་གི་མགོ་བརྩམས་པར་གདམ་གསེས་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the end of the line</source>
        <translation>ལམ་ཐིག་གི་མཇུག་ཏུ་བདམས་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the start of the block</source>
        <translation>འདེམས་སྒྲུག་བྱས་ནས་བཀག་སྡོམ་བྱེད་འགོ་ཚུགས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the end of the block</source>
        <translation>སྲང་ལམ་གྱི་མཐའ་རུ་གདམ་གསེས་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the start of the document</source>
        <translation>ཡིག་ཆའི་འགོ་རྩོམ་སར་གདམ་གསེས་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the end of the document</source>
        <translation>ཡིག་ཆའི་མཇུག་ཏུ་བདམས་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete to the start of the word</source>
        <translation>མིང་ཚིག་གི་མགོ་རྩོམ་སར་བསུབ་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete to the end of the word</source>
        <translation>མིང་ཚིག་གི་མཇུག་ཏུ་བསུབ་པ།</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Insert a new paragraph</source>
        <translation>ནང་གསེས་དོན་ཚན་གསར་པ་ཞིག་གི་ནང་དུ་འཇུག་དགོས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Insert a new line</source>
        <translation>སྐུད་པ་གསར་བ་ཞིག་གི་ནང་དུ་འཇུག་དགོས།</translation>
    </message>
</context>
<context>
    <name>QWhatsThisAction</name>
    <message>
        <location filename="../src/gui/kernel/qwhatsthis.cpp" line="+522"/>
        <source>What&apos;s This?</source>
        <translation>དེ་གང་རེད་དམ།?</translation>
    </message>
</context>
<context>
    <name>QWidget</name>
    <message>
        <location filename="../src/gui/kernel/qwidget.cpp" line="+5326"/>
        <source>*</source>
        <translation>*</translation>
    </message>
</context>
<context>
    <name>QWizard</name>
    <message>
        <location filename="../src/gui/dialogs/qwizard.cpp" line="+637"/>
        <source>Go Back</source>
        <translation>ཕྱིར་རྒྱུགས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Continue</source>
        <translation>མུ་མཐུད་དུ་རྒྱུན་</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Commit</source>
        <translation>བསྒྲུབ་རྒྱུ་ཁས་ལེན་</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Done</source>
        <translation>བསྒྲུབས་ཚར།</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="obsolete">退出</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>&lt; &amp;Back</source>
        <translation>&lt;ཕྱིར་འཐེན་བྱས་པ།</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Finish</source>
        <translation>མཇུག་སྒྲིལ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Help</source>
        <translation>རོགས་རམ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>&amp;Next</source>
        <translation>&gt; རྗེས་མ།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Next &gt;</source>
        <translation>&amp;&gt;་རྗེས་མ།</translation>
    </message>
</context>
<context>
    <name>QWorkspace</name>
    <message>
        <location filename="../src/gui/widgets/qworkspace.cpp" line="+1094"/>
        <source>&amp;Restore</source>
        <translation>&amp;སླར་གསོ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Move</source>
        <translation>&gt; འགུལ་སྐྱོད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>&gt;ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mi&amp;nimize</source>
        <translation>སྨི་ནེ་སྨི་ཙི།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ma&amp;ximize</source>
        <translation>མཱ་ཞི་མའེ་ཙི།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Stay on &amp;Top</source>
        <translation>མུ་མཐུད་དུ་རྒྱུན་འཁྱོངས་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+1059"/>
        <source>Sh&amp;ade</source>
        <translation>ཧྲ་ཧྲི་ཏི།</translation>
    </message>
    <message>
        <location line="-278"/>
        <location line="+60"/>
        <source>%1 - [%2]</source>
        <translation>%1 - [%2]</translation>
    </message>
    <message>
        <location line="-1837"/>
        <source>Minimize</source>
        <translation>ཉུང་དུ་གཏོང་གང་ཐུབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Restore Down</source>
        <translation>སླར་གསོ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location line="+2053"/>
        <source>&amp;Unshade</source>
        <translation>»ཁ་ཡ་མི་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>QXml</name>
    <message>
        <location filename="../src/xml/sax/qxml.cpp" line="+58"/>
        <source>no error occurred</source>
        <translation>ནོར་འཁྲུལ་གང་ཡང་བྱུང་མེད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error triggered by consumer</source>
        <translation>འཛད་སྤྱོད་བྱེད་མཁན་གྱིས་བསླངས་པའི་ནོར་འཁྲུལ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected end of file</source>
        <translation>བསམ་ཡུལ་ལས་འདས་པའི་ཡིག་ཆ་མཇུག</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>more than one document type definition</source>
        <translation>ཡིག་ཆའི་རིགས་དབྱིབས་ཀྱི་མཚན་ཉིད་གཅིག་ལས་བརྒལ་ཡོད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing element</source>
        <translation>རྒྱུ་རྐྱེན་གཙོ་བོར་ཞིབ་འཇུག་བྱེད་སྐབས་ནོར་འཁྲུལ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>tag mismatch</source>
        <translation>ཤོག་བྱང་ནོར་འཁྲུལ་བྱུང་བ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing content</source>
        <translation>ནང་དོན་ལ་ཞིབ་འཇུག་བྱེད་སྐབས་ནོར་འཁྲུལ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected character</source>
        <translation>བསམ་ཡུལ་ལས་འདས་པའི་གཤིས</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid name for processing instruction</source>
        <translation>ལས་སྣོན་གསལ་བཤད་ཡི་གེ་རྩིས་འགྲོ་མེད་པའི་མིང་།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>version expected while reading the XML declaration</source>
        <translation>XML declaration ཀློག་ཞོར་དུ་སྔོན་དཔག་བྱས་པའི་པར་གཞི།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>wrong value for standalone declaration</source>
        <translation>རང་ཚུགས་ཀྱིས་བསྒྲགས་གཏམ་སྤེལ་བའི་ནོར་འཁྲུལ་གྱི་རིན</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>encoding declaration or standalone declaration expected while reading the XML declaration</source>
        <translation>XMLཡི་བསྒྲགས་གཏམ་ཀློག་སྐབས་སྔོན་དཔག་བྱས་པའི་བསྒྲགས་གཏམ་དང་ཡང་ན་ཁེར་རྐྱང་གི་བསྒྲགས་གཏམ་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>standalone declaration expected while reading the XML declaration</source>
        <translation>XMLས བསྒྲགས་གཏམ་ཀློག་སྐབས་སྔོན་དཔག་བྱས་པའི་རང་ཚུགས་ཀྱི་བསྒྲགས་གཏམ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing document type definition</source>
        <translation>ཡིག་ཆའི་རིགས་དབྱིབས་ཀྱི་མཚན་ཉིད་ལ་ཞིབ་འཇུག་བྱེད་སྐབས་ནོར་འཁྲུལ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>letter is expected</source>
        <translation>སྔོན་དཔག་བྱས་ན་འཕྲིན་ཡིག</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing comment</source>
        <translation>དཔྱད་གཏམ་ལ་དཔྱད་གཏམ་བརྗོད་སྐབས་ནོར་འཁྲུལ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing reference</source>
        <translation>དཔྱད་གཞིར་ཞིབ་འཇུག་བྱེད་སྐབས་ནོར་འཁྲུལ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>internal general entity reference not allowed in DTD</source>
        <translation>DTDནང་དུ་མི་ཆོག་པའི་ནང་ཁུལ་གྱི་སྤྱིར་བཏང་གི་དངོས་གཟུགས་དཔྱད་གཞིར་འཛིན་མི་ཆོག</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>external parsed general entity reference not allowed in attribute value</source>
        <translation>ངོ་བོ་དང་གཟུགས་པོའི་རིན་ཐང་ཁྲོད་དུ་ཕྱི་རོལ་དུ་ཕྱིར་གཏོང་བྱས་མི་ཆོག་པའི་སྤྱིར་བཏང་གི་དངོས་གཟུགས་དཔྱད་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>external parsed general entity reference not allowed in DTD</source>
        <translation>DTDནང་དུ་ཕྱི་རོལ་དུ་སྤྱིར་བཏང་གི་དངོས་གཟུགས་དཔྱད་གཞིར་མི་ཆོག་པའི་དཔྱད་གཞིར་འཛིན་དགོས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unparsed entity reference in wrong context</source>
        <translation>ནོར་འཁྲུལ་གྱི་རྒྱབ་ལྗོངས་འོག་གི་དངོས་གཟུགས་དཔྱད་གཞིར་འཛིན་མི་རུང་།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>recursive entities</source>
        <translation>སླར་གསོ་བྱུང་བའི་དངོས་གཟུགས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error in the text declaration of an external entity</source>
        <translation>ཕྱི་རོལ་གྱི་དངོས་ཚན་གྱི་ཡི་གེའི་བསྒྲགས་གཏམ་ནང་གི་ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>QXmlStream</name>
    <message>
        <location filename="../src/corelib/xml/qxmlstream.cpp" line="+592"/>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+1769"/>
        <source>Extra content at end of document.</source>
        <translation>ཡིག་ཆའི་མཇུག་ཏུ་ནང་དོན་ལྷག་མ་ཡོད།</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>Invalid entity value.</source>
        <translation>རྩིས་འགྲོ་མེད་པའི་དངོས་གཟུགས་ཀྱི་རིན་ཐང་།</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Invalid XML character.</source>
        <translation>གོ་མི་ཆོད་པའི་XMLཡི་གཤིས་ཀ་</translation>
    </message>
    <message>
        <location line="+259"/>
        <source>Sequence &apos;]]&gt;&apos; not allowed in content.</source>
        <translation>ནང་དོན་ཁྲོད་དུ་གོ་རིམ་&apos;]&gt;་མི་ཆོག་པ་རེད།</translation>
    </message>
    <message>
        <location line="+309"/>
        <source>Namespace prefix &apos;%1&apos; not declared</source>
        <translation>མིང་གི་བར་སྟོང་གི་སྔོན་འཇུག&apos;%1&apos;ཁྱབ་བསྒྲགས་མ་བྱས་པ།</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Attribute redefined.</source>
        <translation>ངོ་བོ་དང་གཟུགས་ཀྱི་ཁྱད་ཆོས་བསྐྱར་དུ་གཏན</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Unexpected character &apos;%1&apos; in public id literal.</source>
        <translation>སྤྱི་པའི་ཐོབ་ཐང་ཡིག་ཐོག་གི་བསམ་ཡུལ་ལས་འདས་པའི་མི་སྣ་&apos;%1&apos;ཡིན།</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Invalid XML version string.</source>
        <translation>གོ་མི་ཆོད་པའི་XMLཔར་གཞི་ཡི་གེ་ཞིག་རེད།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unsupported XML version.</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་XMLཔར་གཞི།</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>%1 is an invalid encoding name.</source>
        <translation>%1ནི་གོ་མི་ཆོད་པའི་ཨང་སྒྲིག་གི་མིང་ཞིག་ཡིན།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Encoding %1 is unsupported</source>
        <translation>ཨང་སྒྲིག་ཨང་གྲངས་1ནི་རྒྱབ་སྐྱོར་མི་བྱེད་པ་ཞིག་རེད།</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Standalone accepts only yes or no.</source>
        <translation>རང་ཚུགས་ཀྱིས་ཁས་མི་ལེན་པ་ཁོ་ན་དང་ལེན་བྱེད་ཀྱིན་ཡོད།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid attribute in XML declaration.</source>
        <translation>XML བསྒྲགས་གཏམ་ཁྲོད་ཀྱི་ནུས་པ་མེད་པའི་ངོ་བོ་</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Premature end of document.</source>
        <translation>ཡིག་ཆ་སྔ་དྲགས་པའི་མཇུག་སྒྲིལ་བ་རེད།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid document.</source>
        <translation>གོ་མི་ཆོད་པའི་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Expected </source>
        <translation>སྔོན་དཔག་བྱས་པ། </translation>
    </message>
    <message>
        <location line="+11"/>
        <source>, but got &apos;</source>
        <translation>འོན་ཀྱང་ཐོབ་པ་&apos;དེ་རེད།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Unexpected &apos;</source>
        <translation>བསམ་ཡུལ་ལས་འདས་པ་ཞིག</translation>
    </message>
    <message>
        <location line="+210"/>
        <source>Expected character data.</source>
        <translation>སྔོན་དཔག་བྱས་པའི་གཤིས་ཀའི་གཞི་གྲངས</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="-995"/>
        <source>Recursive entity detected.</source>
        <translation>ཞིབ་དཔྱད་ཚད་ལེན་བྱས་པའི་སླར་གསོ་རང་བཞིན་གྱི་དངོས་གཟུགས་ཤིག་ཡིན།</translation>
    </message>
    <message>
        <location line="+516"/>
        <source>Start tag expected.</source>
        <translation>སྔོན་དཔག་བྱས་པའི་ཤོག་བྱང་འགོ་འཛུགས</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>XML declaration not at start of document.</source>
        <translation>XMLཡི་བསྒྲགས་གཏམ་དེ་ཡིག་ཆ་འགོ་རྩོམ་པའི་དུས་སུ་མེད།</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>NDATA in parameter entity declaration.</source>
        <translation>NDATAནི་གྲངས་ཀ་དངོས་གཞིའི་བསྒྲགས་གཏམ་ཁྲོད།</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>%1 is an invalid processing instruction name.</source>
        <translation>%1ནི་གོ་མི་ཆོད་པའི་ཐག་གཅོད་མཛུབ་སྟོན་གྱི་མིང་ཞིག་ཡིན།</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Invalid processing instruction name.</source>
        <translation>གོ་མི་ཆོད་པའི་ཐག་གཅོད་གསལ་བཤད་ཡི་གེའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream.cpp" line="-521"/>
        <location line="+12"/>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+164"/>
        <location line="+53"/>
        <source>Illegal namespace declaration.</source>
        <translation>ཁྲིམས་དང་མི་མཐུན་པའི་མིང་བར་སྟོང་གི་</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+15"/>
        <source>Invalid XML name.</source>
        <translation>གོ་མི་ཆོད་པའི་XMLཡི་མིང་།</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Opening and ending tag mismatch.</source>
        <translation>ཁ་ཕྱེ་བ་དང་མཇུག་སྒྲིལ་བའི་ཤོག་བྱང་ལ་ནོར་འཁྲུལ་བྱུང་བ</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Reference to unparsed entity &apos;%1&apos;.</source>
        <translation>ཚད་ལྡན་མིན་པའི་དངོས་གཟུགས་&apos;%1&apos;ལ་དཔྱད་གཞིར་འཛིན་དགོས།</translation>
    </message>
    <message>
        <location line="-13"/>
        <location line="+61"/>
        <location line="+40"/>
        <source>Entity &apos;%1&apos; not declared.</source>
        <translation>དངོས་ཚན་&apos;%1&apos;ཁྱབ་བསྒྲགས་མ་བྱས་པ།</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Reference to external entity &apos;%1&apos; in attribute value.</source>
        <translation>ངོ་བོ་དང་གཟུགས་པོའི་རིན་ཐང་ཁྲོད་ཀྱི་ཕྱི་རོལ་གྱི་དངོས་གཟུགས་&apos;%1&apos;ལ་དཔྱད་གཞིར་འཛིན་དགོས།</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Invalid character reference.</source>
        <translation>ནུས་པ་མེད་པའི་གཤིས་ཀའི་དཔྱད་གཞི།</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream.cpp" line="-75"/>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="-823"/>
        <source>Encountered incorrectly encoded content.</source>
        <translation>ཨང་སྒྲིག་གི་ནང་དོན་ཡང་དག་མིན་པར་འཕྲད་པ་རེད།</translation>
    </message>
    <message>
        <location line="+274"/>
        <source>The standalone pseudo attribute must appear after the encoding.</source>
        <translation>ངེས་པར་དུ་ཚབ་སྒྲིག་བྱས་རྗེས་ངེས་པར་དུ་རང་ཚུགས་ཀྱི་རྫུས་མའི་ངོ་བོ་མངོན་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+562"/>
        <source>%1 is an invalid PUBLIC identifier.</source>
        <translation>%1ནི་གོ་མི་ཆོད་པའི་སྤྱི་དམངས་ཀྱི་ཐོབ་ཐང་ལག་འཛིན་བྱེད་མཁན་ཡིན།</translation>
    </message>
</context>
<context>
    <name>QtXmlPatterns</name>
    <message>
        <location filename="../src/xmlpatterns/acceltree/qacceltreebuilder.cpp" line="+205"/>
        <source>An %1-attribute with value %2 has already been declared.</source>
        <translation>%2值%1-ངོ་བོ་ལྡན་པའི་%1-ངོ་བོ་ཞིག་སྔ་མོ་ནས་ཁྱབ་བསྒྲགས་བྱས་ཟིན།</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>An %1-attribute must have a valid %2 as value, which %3 isn&apos;t.</source>
        <translation>%1-attribute值,2值,%3值,%3值。。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/api/qiodevicedelegate.cpp" line="+84"/>
        <source>Network timeout.</source>
        <translation>དྲ་རྒྱའི་དུས་ཚོད་ཕྱིར་གཏོང་།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/api/qxmlserializer.cpp" line="+320"/>
        <source>Element %1 can&apos;t be serialized because it appears outside the document element.</source>
        <translation>元素%1ནི་ཡིག་ཆའི་རྒྱུ་རྐྱེན་གཙོ་བོའི་ཕྱི་རོལ་དུ་བྱུང་བའི་རྐྱེན་གྱིས་མུ་འབྲེལ་གོ་རིམ་སྒྲིག་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Attribute element %1 can&apos;t be serialized because it appears at the top level.</source>
        <translation type="obsolete">属性元素 %1 不能被串行化，因为它出现在最顶层。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qabstractdatetime.cpp" line="+80"/>
        <source>Year %1 is invalid because it begins with %2.</source>
        <translation>ལོ་གཅིག་གི་ནང་དུ་%1ནི་གོ་མི་ཆོད་པ་ཞིག་རེད། རྒྱུ་མཚན་ནི་དེ་ནི་%2ནས་འགོ་བརྩམས་པས་རེད།</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Day %1 is outside the range %2..%3.</source>
        <translation>%1%ནི་ཁྱབ་ཁོངས་ལས་བརྒལ་ཡོད། %2.%3</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Month %1 is outside the range %2..%3.</source>
        <translation>%1%2.%3.3%ཡི་ཁྱབ་ཁོངས་ལས་བརྒལ་ཡོད།</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Overflow: Can&apos;t represent date %1.</source>
        <translation>ཆུ་ལོག་བྱུང་བ་སྟེ། དུས་ཚོད་ཀྱི་ཨང་གྲངས་1མི་མཚོན་ཐབས་མེད་རེད།</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Day %1 is invalid for month %2.</source>
        <translation>%1%ཟླ་རེར་རྩིས་འགྲོ་མེད་པའི་ཟླ་རེར་རྩིས་འགྲོ་མེད།</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Time 24:%1:%2.%3 is invalid. Hour is 24, but minutes, seconds, and milliseconds are not all 0; </source>
        <translation>དུས་ཚོད་24:%1:%2.3ནི་གོ་མི་ཆོད་པ་རེད། ཆུ་ཚོད་ནི་24ཡིན་མོད། འོན་ཀྱང་སྐར་མ་དང་། སྐར་ཆ། སྐར་ཆ་བཅས་ཚང་མ་0མེད་པ་དང་། </translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Time %1:%2:%3.%4 is invalid.</source>
        <translation>%1:%2:%3.%4%གོ་མི་ཆོད་པ་རེད།</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Overflow: Date can&apos;t be represented.</source>
        <translation>ཆུ་ལོག་བྱུང་བ་སྟེ། དུས་ཚོད་ཀྱི་ཚབ་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qabstractduration.cpp" line="+99"/>
        <location line="+15"/>
        <source>At least one component must be present.</source>
        <translation>ངེས་པར་དུ་ཉུང་མཐར་ཡང་ལྷུ་ལག་གཅིག་ཡོད་དགོས།</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>At least one time component must appear after the %1-delimiter.</source>
        <translation>ཉུང་མཐར་ཡང་ཐེངས་གཅིག་ལ་ངེས་པར་དུ་%1-delimiterཡི་རྗེས་སུ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qabstractfloatmathematician.cpp" line="+64"/>
        <source>No operand in an integer division, %1, can be %2.</source>
        <translation>ཆ་ཚང་བའི་དབྱེ་འབྱེད་ཁྲོད་ཀྱི་འཁོར་རིགས་གང་ཞིག་ཡིན་རུང་ཚང་མ་%2ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The first operand in an integer division, %1, cannot be infinity (%2).</source>
        <translation>ཆ་ཚང་བའི་ཡན་ལག་ཀུང་སིའི་ནང་གི་བཀོལ་སྤྱོད་ཐེངས་དང་པོ་ནི་ཚད་མེད་པ་ཞིག་ཡིན་མི་སྲིད། (%2)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The second operand in a division, %1, cannot be zero (%2).</source>
        <translation>ཧྲི་ཤོག་གཅིག་གི་ནང་གི་བཀོལ་སྤྱོད་ཐེངས་གཉིས་པ་ནི་བརྒྱ་ཆ་1ཡིན་པ་དེ་ཀླད་ཀོར་(%2)ཡིན་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qanyuri_p.h" line="+132"/>
        <source>%1 is not a valid value of type %2.</source>
        <translation>%1ནི་རིགས་དབྱིབས་2ཀྱི་གོ་ཆོད་པའི་རིན་ཐང་ཞིག་མིན།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qatomiccasters_p.h" line="+223"/>
        <source>When casting to %1 from %2, the source value cannot be %3.</source>
        <translation>%2%ནས་%1བར་འདེམས་སྐབས་འབྱུང་ཁུངས་ཀྱི་རིན་ཐང་ནི་%3ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qatomicmathematicians.cpp" line="+65"/>
        <source>Integer division (%1) by zero (%2) is undefined.</source>
        <translation>(%2)ཡིས་ཆ་ཚང་བའི་དབྱེ་བ་(%1)ལ་མཚན་ཉིད་བཞག་མེད།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Division (%1) by zero (%2) is undefined.</source>
        <translation>(%1)ཡིས་ཀླད་ཀོར་(%2)ལ་མཚན་ཉིད་བཞག་མེད།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Modulus division (%1) by zero (%2) is undefined.</source>
        <translation>Modulus division (%1) by zero (%2) ནི་གཏན་འཁེལ་བྱས་མེད་པ་ཞིག་རེད།</translation>
    </message>
    <message>
        <location line="+122"/>
        <location line="+32"/>
        <source>Dividing a value of type %1 by %2 (not-a-number) is not allowed.</source>
        <translation>%1རིགས་ཀྱི་རིན་ཐང་དེ་%2(ཨང་གྲངས་མིན་པ་)ལ་བགོས་མི་ཆོག</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Dividing a value of type %1 by %2 or %3 (plus or minus zero) is not allowed.</source>
        <translation>%1རིགས་ཀྱི་རིན་ཐང་དེ་%2དང་ཡང་ན་%3(ལྷག་མའམ་ཡང་ན་ཀླད་ཀོར་འོག་གི་ཀླད་ཀོར་)ལ་བགོས་མི་ཆོག</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Multiplication of a value of type %1 by %2 or %3 (plus or minus infinity) is not allowed.</source>
        <translation>%1类型%1%2或%3(+or minus infinity)乘་པ་གཏན་ནས་བྱས་མི་ཆོག</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qatomicvalue.cpp" line="+79"/>
        <source>A value of type %1 cannot have an Effective Boolean Value.</source>
        <translation>%1型值,1ལ་ཕན་ནུས་ལྡན་པའི་པོ་ཨར་རིན་ཐང་ཡོད་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qboolean.cpp" line="+78"/>
        <source>Effective Boolean Value cannot be calculated for a sequence containing two or more atomic values.</source>
        <translation>རྡུལ་ཕྲན་གྱི་རིན་ཐང་གཉིས་སམ་ཡང་ན་དེ་ལས་མང་བ་ཡོད་པའི་གོ་རིམ་ཞིག་ལ་ཕན་ནུས་ལྡན་པའི་པོ་ཨར་རིན་ཐང་རྩིས་རྒྱག་མི་</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qderivedinteger_p.h" line="+402"/>
        <source>Value %1 of type %2 exceeds maximum (%3).</source>
        <translation>%2类型%1值最高(%3)ལས་བརྒལ་ཡོད།</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Value %1 of type %2 is below minimum (%3).</source>
        <translation>%2 རིགས་དབྱིབས་ཀྱི་རིན་ཐང་%1ནི་ཆེས་དམའ་ཤོས་(%3)ལས་དམའ་བ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qhexbinary.cpp" line="+91"/>
        <source>A value of type %1 must contain an even number of digits. The value %2 does not.</source>
        <translation>%1型值,1值 ངེས་པར་དུ་གྲངས་ཀ་ཆ་སྙོམས་ཀྱི་གྲངས་ཀ་ཞིག་ཡོད་དགོས། རིན་ཐང་ནི་%2མི་རེད།</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>%1 is not valid as a value of type %2.</source>
        <translation>%1ནི་རིགས་དབྱིབས་གཉིས་ཀྱི་རིན་ཐང་ལྟར་རྩིས་འགྲོ་མེད།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qarithmeticexpression.cpp" line="+207"/>
        <source>Operator %1 cannot be used on type %2.</source>
        <translation>%1类型%2 སྟེང་བཀོལ་སྤྱོད་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Operator %1 cannot be used on atomic values of type %2 and %3.</source>
        <translation>%1类型%2和%3ཡི་རྡུལ་ཕྲན་རིན་ཐང་སྟེང་བཀོལ་སྤྱོད་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qattributenamevalidator.cpp" line="+66"/>
        <source>The namespace URI in the name for a computed attribute cannot be %1.</source>
        <translation>计算,名称</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The name for a computed attribute cannot have the namespace URI %1 with the local name %2.</source>
        <translation>计算,URI%1名称,名称%2。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcastas.cpp" line="+88"/>
        <source>Type error in cast, expected %1, received %2.</source>
        <translation>རིགས་དབྱིབས་ཀྱི་ནོར་འཁྲུལ་བྱུང་ན་སྔོན་དཔག་བྱས་ན་%1ལ་2%ཐོབ་རྒྱུ་རེད།</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>When casting to %1 or types derived from it, the source value must be of the same type, or it must be a string literal. Type %2 is not allowed.</source>
        <translation>%1或类型,1或类型,1,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,0 %2 རིགས་དབྱིབས་གཏན་ནས་བྱས་མི་ཆོག</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcastingplatform.cpp" line="+134"/>
        <source>No casting is possible with %1 as the target type.</source>
        <translation>དམིགས་འབེན་གྱི་རིགས་དབྱིབས་སུ་བརྩིས་ནས་%1བཀག་སྤྱོད་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>It is not possible to cast from %1 to %2.</source>
        <translation>%1ནས་%2བར་འཐེན་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Casting to %1 is not possible because it is an abstract type, and can therefore never be instantiated.</source>
        <translation>%1ལ་ཁ་ལོ་བ་ནི་སྤྱི་མཚན་དུ་གྱུར་པའི་རིགས་དབྱིབས་ཤིག་ཡིན་པས་ནམ་ཡང་སྐད་ཆ་བཤད་མི་ཐུབ་པའི་རྐྱེན་གྱིས་རེད།</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>It&apos;s not possible to cast the value %1 of type %2 to %3</source>
        <translation>%2%ནས་3%བར་གྱི་རིན་ཐང་%1བཏབ་ན་མི་འགྲིག</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Failure when casting from %1 to %2: %3</source>
        <translation>%1%ནས་2%བར་གྱི་དུས་སུ་ཕམ་ཁ་བྱུང་བ་སྟེ། %3</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcommentconstructor.cpp" line="+67"/>
        <source>A comment cannot contain %1</source>
        <translation>དཔྱད་གཏམ་ཞིག་གི་ནང་དུ་%1འབ་མི་ཆོག།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>A comment cannot end with a %1.</source>
        <translation>དཔྱད་གཏམ་ཞིག་གིས་%1གིས་མཇུག་སྒྲིལ་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcomparisonplatform.cpp" line="+167"/>
        <source>No comparisons can be done involving the type %1.</source>
        <translation>རིགས་དབྱིབས་དང་འབྲེལ་བ་ཡོད་པའི་ཞིབ་བསྡུར་གང་ཡང་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Operator %1 is not available between atomic values of type %2 and %3.</source>
        <translation>%2 和 %3 རིགས་དབྱིབས་ཀྱི་རྡུལ་ཕྲན་རིན་ཐང་བར་དུ་བཀོལ་སྤྱོད་བྱེད་མཁན་%1མཁོ་འདོན་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qdocumentcontentvalidator.cpp" line="+86"/>
        <source>An attribute node cannot be a child of a document node. Therefore, the attribute %1 is out of place.</source>
        <translation>2004 2003 100000000000000000000000000000000000000000000000 དེའི་རྐྱེན་གྱིས་ངོ་བོ་དང་གཟུགས་པོའི་རིང་ཚད་ནི་མི་འགྲིག་པ་ཞིག་རེད།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qexpressionfactory.cpp" line="+169"/>
        <source>A library module cannot be evaluated directly. It must be imported from a main module.</source>
        <translation>དཔེ་མཛོད་ཁང་གི་དཔེ་དབྱིབས་ལ་ཐད་ཀར་གདེང་འཇོག་བྱེད་མི་ཐུབ། ངེས་པར་དུ་མ་དཔེ་གཙོ་བོ་ཞིག་ནས་ནང་འདྲེན་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>No template by name %1 exists.</source>
        <translation>%1名称模板</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qgenericpredicate.cpp" line="+106"/>
        <source>A value of type %1 cannot be a predicate. A predicate must have either a numeric type or an Effective Boolean Value type.</source>
        <translation>%1 རིགས་དབྱིབས་ཀྱི་རིན་ཐང་ནི་སྔོན་དཔག་བྱེད་ཐབས་ཤིག་ཡིན་མི་སྲིད། སྔོན་དཔག་ལ་ངེས་པར་དུ་གྲངས་ཀའི་རིགས་དབྱིབས་སམ་ཡང་ན་ཕན་ནུས་ལྡན་པའི་པོ་ཨར་རིན་ཐང་གི་རིགས་དབྱིབས་ཡོད་དགོས།</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>A positional predicate must evaluate to a single numeric value.</source>
        <translation>གནས་གཏན་འཁེལ་གྱི་སྔོན་དཔག་ཅིག་གིས་ངེས་པར་དུ་གྲངས་ཀའི་རིན་ཐང་རྐྱང་པར་གདེང་འཇོག་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qncnameconstructor_p.h" line="+113"/>
        <source>The target name in a processing instruction cannot be %1 in any combination of upper and lower case. Therefore, is %2 invalid.</source>
        <translation>ཐག་གཅོད་མཛུབ་སྟོན་ཁྲོད་ཀྱི་དམིགས་འབེན་གྱི་མིང་ནི་གོང་འོག་གི་གྱོད་གཞི་གང་རུང་གི་ཁྲོད་དུ་བརྒྱ་ཆའི་གཅིག་ཡིན་མི་སྲིད། དེའི་རྐྱེན་གྱིས་%2ལ་ནུས་པ་མེད།</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>%1 is not a valid target name in a processing instruction. It must be a %2 value, e.g. %3.</source>
        <translation>%1ནི་ཐག་གཅོད་གསལ་བཤད་ཡི་གེའི་ནང་གི་གོ་ཆོད་པའི་དམིགས་འབེན་གྱི་མིང་ཞིག་མིན། ངེས་པར་དུ་%2རི་བའི་རིན་ཐང་ཡིན་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qpath.cpp" line="+109"/>
        <source>The last step in a path must contain either nodes or atomic values. It cannot be a mixture between the two.</source>
        <translation>ལམ་ཕྲན་སྟེང་གི་ཆེས་མཐའ་མཇུག་གི་གོམ་རིམ་ལ་ངེས་པར་དུ་ས་ཆ་གང་རུང་ཞིག་གམ་ཡང་ན་རྡུལ་ཕྲན་གྱི་རིན་ཐང་འདུས་དགོས དེ་གཉིས་མཉམ་བསྲེས་བྱེད་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qprocessinginstructionconstructor.cpp" line="+84"/>
        <source>The data of a processing instruction cannot contain the string %1</source>
        <translation>1.1.1.1.1.1.1.1.1.1.1.1.1.1.1.1.1</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qqnameconstructor.cpp" line="+82"/>
        <source>No namespace binding exists for the prefix %1</source>
        <translation>%1.1.1.1%1%</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qqnameconstructor_p.h" line="+156"/>
        <source>No namespace binding exists for the prefix %1 in %2</source>
        <translation>%2%1%ཡི་སྔོན་ལ་མིང་གི་བར་སྟོང་གི་ཚོད་འཛིན་མེད།</translation>
    </message>
    <message>
        <location line="+12"/>
        <location filename="../src/xmlpatterns/functions/qqnamefns.cpp" line="+69"/>
        <source>%1 is an invalid %2</source>
        <translation>%1ནི་གོ་མི་ཆོད་པའི་%2</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/xmlpatterns/functions/qabstractfunctionfactory.cpp" line="+77"/>
        <source>%1 takes at most %n argument(s). %2 is therefore invalid.</source>
        <translation>
            <numerusform>%1ནི་ཆེས་མང་ན་བརྒྱ་ཆའི་གཅིག་གི་སྒྲུབ་བྱེད་ཡིན། དེར་བརྟེན་%2ནི་གོ་མི་ཆོད་པ་ཞིག་རེད།</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%1 requires at least %n argument(s). %2 is therefore invalid.</source>
        <translation>
            <numerusform>%1འདི་ལ་མ་མཐར་ཡང་%n argumentདགོས། དེར་བརྟེན་%2ནི་གོ་མི་ཆོད་པ་ཞིག་རེད།</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qaggregatefns.cpp" line="+120"/>
        <source>The first argument to %1 cannot be of type %2. It must be a numeric type, xs:yearMonthDuration or xs:dayTimeDuration.</source>
        <translation>%1%1%ལ་ཐོག་མའི་སྒྲུབ་བྱེད་ནི་རིགས་དབྱིབས་2ཡིན་མི་སྲིད། ངེས་པར་དུ་གྲངས་ཀའི་རིགས་ཤིག་ཡིན་དགོས། xs:yearMonthDuration or xs:dayTimeDuration</translation>
    </message>
    <message>
        <location line="+74"/>
        <source>The first argument to %1 cannot be of type %2. It must be of type %3, %4, or %5.</source>
        <translation>%1%1%ལ་ཐོག་མའི་སྒྲུབ་བྱེད་ནི་རིགས་དབྱིབས་2ཡིན་མི་སྲིད། ངེས་པར་དུ་རིགས་དབྱིབས་ཀྱི་ཨང་3པ་དང་། %4འམ་ཡང་ན་%5ཡིན་དགོས།</translation>
    </message>
    <message>
        <location line="+91"/>
        <source>The second argument to %1 cannot be of type %2. It must be of type %3, %4, or %5.</source>
        <translation>%1.1ལ་བཏོན་པའི་སྒྲུབ་བྱེད་གཉིས་པ་ནི་རིགས་དབྱིབས་2ཡིན་མི་སྲིད། ངེས་པར་དུ་རིགས་དབྱིབས་ཀྱི་ཨང་3པ་དང་། %4འམ་ཡང་ན་%5ཡིན་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qassemblestringfns.cpp" line="+88"/>
        <source>%1 is not a valid XML 1.0 character.</source>
        <translation>%1ནི་གོ་ཆོད་པའི་XML 1.0ཡི་ཡི་གེ་ཞིག་མིན།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qcomparingaggregator.cpp" line="+197"/>
        <source>The first argument to %1 cannot be of type %2.</source>
        <translation>%1%1%ལ་ཐོག་མའི་སྒྲུབ་བྱེད་ནི་རིགས་དབྱིབས་2ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qdatetimefn.cpp" line="+86"/>
        <source>If both values have zone offsets, they must have the same zone offset. %1 and %2 are not the same.</source>
        <translation>གལ་ཏེ་རིན་ཐང་འདི་གཉིས་ལ་ས་ཁོངས་ཀྱི་ཁ་འཐབ་བཏང་ཡོད་ན། ངེས་པར་དུ་དེ་དང་འདྲ་བའི་ས་ཁོངས་ཀྱི་ཁ་འཐབ %1和 %2 གཉིས་མི་འདྲ།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qerrorfn.cpp" line="+61"/>
        <source>%1 was called.</source>
        <translation>%1ལ་ཁ་པར་བརྒྱབ་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qpatternmatchingfns.cpp" line="+94"/>
        <source>%1 must be followed by %2 or %3, not at the end of the replacement string.</source>
        <translation>%1 ངེས་པར་དུ་%2%འམ་ཡང་ན་%3གྱི་རྗེས་སུ་འབྲང་དགོས་པ་ལས་བརྗེ་སྤོར་གྱི་སྐུད་པ་མཇུག་རྫོགས་སྐབས་བརྩི་སྲུང་བྱེད་མི་རུང་།</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>In the replacement string, %1 must be followed by at least one digit when not escaped.</source>
        <translation>བརྗེ་སྤོར་བྱེད་སྐབས་ངེས་པར་དུ་བྲོས་བྱོལ་དུ་སོང་མེད་པའི་སྐབས་སུ་ངེས་པར་དུ་མ་མཐར་ཡང་གྲངས་ཀ་གཅིག་གི་རྗེས་སུ་འབྲང་དགོས།</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>In the replacement string, %1 can only be used to escape itself or %2, not %3</source>
        <translation>བརྗེ་སྤོར་བྱས་པའི་སྐུད་པའི་ནང་དུ་%1ནི་རང་ཉིད་ལས་གཡོལ་ཐབས་བྱེད་པའམ་ཡང་ན་%2ལས་གཡོལ་ཐབས་བྱེད་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qpatternplatform.cpp" line="+92"/>
        <source>%1 matches newline characters</source>
        <translation>%1དང་སྐུད་ལམ་གསར་པའི་ཡི་གེ་གཉིས་ཆ་མཐུན་ཡིན།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1 and %2 match the start and end of a line.</source>
        <translation>%1和%2འགྲན་བསྡུར་བྱས་ནས་སྐུད་པ་གཅིག་གི་མགོ་མཇུག་བར་གསུམ་དུ་ཆ་འགྲིག་པ་རེད།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Matches are case insensitive</source>
        <translation>འབར་ཞུན་ནི་དཔེ་མཚོན་གྱི་གོ་མེད་ཚོར་མེད་ཅིག་རེད།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Whitespace characters are removed, except when they appear in character classes</source>
        <translation>དཀར་པོའི་བར་སྟོང་གི་ཡི་གེ་མེད་པར་བཟོས་པ་ཕུད། འོན་ཀྱང་ཡི་གེའི་འཛིན་གྲྭའི་ནང་དུ་བྱུང་བ་ཕུད།</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>%1 is an invalid regular expression pattern: %2</source>
        <translation>%1ནི་གོ་མི་ཆོད་པའི་རྒྱུན་ལྡན་གྱི་མཚོན་སྟངས་ཤིག་ཡིན། %2</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>%1 is an invalid flag for regular expressions. Valid flags are:</source>
        <translation>%1ནི་རྒྱུན་ལྡན་གྱི་མཚོན་སྟངས་ལ་ནུས་པ་མེད་པའི་དར་ཆ་ཞིག་ཡིན། གོ་ཆོད་པའི་དར་ཆ་ནི།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qqnamefns.cpp" line="+17"/>
        <source>If the first argument is the empty sequence or a zero-length string (no namespace), a prefix cannot be specified. Prefix %1 was specified.</source>
        <translation>གལ་ཏེ་སྒྲུབ་བྱེད་དང་པོ་ནི་གོ་རིམ་སྟོང་པ་ཡིན་ནམ་ཡང་ན་རིང་ཚད་ཀླད་ཀོར་གྱི་རྒྱུད་སྐུད་(མིང་གི་བར་སྟོང་མེད་)ཡིན་ན། སྔོན་འཇུག་གི་ཡི་གེ་ཞིག་གཏན་འཁེལ་བྱེད་མི་ཐུབ། སྔོན་ཚུད་ནས་%1གནམ་དུ་གཏན་འཁེལ་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qsequencefns.cpp" line="+347"/>
        <source>It will not be possible to retrieve %1.</source>
        <translation>%1ཕབ་ལེན་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qcontextnodechecker.cpp" line="+54"/>
        <source>The root node of the second argument to function %1 must be a document node. %2 is not a document node.</source>
        <translation>&quot;%1&quot;ནུས་པ་འདོན་སྤེལ་བྱེད་པའི་སྒྲུབ་བྱེད་གཉིས་པའི་རྩ་བའི་གཞི་འཛིན་ས་ནི་ངེས་པར་དུ་ཡིག་ཆའི་གཞི་གྲངས་ཤིག་ཡིན་དགོས། %2ནི་ཡིག་ཆ་ཞིག་མ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qsequencegeneratingfns.cpp" line="+279"/>
        <source>The default collection is undefined</source>
        <translation>ཁ་ཆད་དང་འགལ་བའི་བསྡུ་ཉར་དངོས་རྫས་ལ་མཚན་</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>%1 cannot be retrieved</source>
        <translation>%1ཕན་ཕྱིར་འཐེན་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qstringvaluefns.cpp" line="+252"/>
        <source>The normalization form %1 is unsupported. The supported forms are %2, %3, %4, and %5, and none, i.e. the empty string (no normalization).</source>
        <translation>%1 རྒྱུན་ལྡན་ཅན་དུ་འགྱུར་བའི་རྣམ་པ་ནི་རྒྱབ་སྐྱོར་མི་བྱེད་པ་ཞིག་རེད། རྒྱབ་སྐྱོར་བྱས་པའི་རེའུ་མིག་ནི་%2,%3,%4,%5,གཅིག་ཀྱང་མེད། དེ་ནི་སྟོང་བའི་སྐུད་པ་(རྒྱུན་ལྡན་ཅན་དུ་འགྱུར་མི་ཐུབ་པ་)ཡིན།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qtimezonefns.cpp" line="+87"/>
        <source>A zone offset must be in the range %1..%2 inclusive. %3 is out of range.</source>
        <translation>ས་ཁོངས་ཀྱི་ཁ་འཐབ་བཏང་བ་དེ་ངེས་པར་དུ་ཁྱབ་ཁོངས་ནང་དུ་བརྒྱ་ཆ་1.2ཀྱི་བཟོད་སྒོམ་བྱེད་དགོས། %3ནི་ཁྱབ་ཁོངས་ལས་བརྒལ་ཡོད།</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1 is not a whole number of minutes.</source>
        <translation>%1ནི་སྐར་མ་ཧྲིལ་པོ་ཞིག་མ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/janitors/qcardinalityverifier.cpp" line="+58"/>
        <source>Required cardinality is %1; got cardinality %2.</source>
        <translation>དགོས་ངེས་ཀྱི་གཞི་རྩའི་ཚད་ནི་%1ཡིན། ཐོབ་པའི་ཐོབ་ཆ་ནི་2.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/janitors/qitemverifier.cpp" line="+67"/>
        <source>The item %1 did not match the required type %2.</source>
        <translation>%1%དང་གཏན་འབེབས་བྱས་པའི་རིགས་དབྱིབས་གཉིས་མི་མཐུན་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qquerytransformparser.cpp" line="+379"/>
        <location line="+7253"/>
        <source>%1 is an unknown schema type.</source>
        <translation>%1ནི་ཤེས་མེད་པའི་རྣམ་པའི་རིགས་ཤིག་ཡིན།</translation>
    </message>
    <message>
        <location line="-6971"/>
        <source>Only one %1 declaration can occur in the query prolog.</source>
        <translation>འདྲི་རྩད་ཀྱི་གོ་རིམ་ཁྲོད་དུ་བརྒྱ་ཆའི་གཅིག་གི་བསྒྲགས་གཏམ་མ་གཏོགས་འབྱུང་མི་སྲིད།</translation>
    </message>
    <message>
        <location line="+188"/>
        <source>The initialization of variable %1 depends on itself</source>
        <translation>འགྱུར་ཡོད་ཀྱི་%1གོ་རིམ་དུ་འགྱུར་བ་ནི་རང་ཉིད་ལ་རག་ལས་པ་ཡིན།</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>No variable by name %1 exists</source>
        <translation>%1ལ་མིང་གཡར་ནས་འགྱུར་ལྡོག་གང་ཡང་མེད།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qparsercontext.cpp" line="+93"/>
        <source>The variable %1 is unused</source>
        <translation>འགྱུར་ཡོད་ཀྱི་%1བམ་སྤྱོད་མ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qquerytransformparser.cpp" line="+2841"/>
        <source>Version %1 is not supported. The supported XQuery version is 1.0.</source>
        <translation>པར་གཞི་%1ལ་རྒྱབ་སྐྱོར་མི་བྱེད། རྒྱབ་སྐྱོར་བྱས་པའི་XQueryཔར་གཞི་ནི་1.0ཡིན།</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>The encoding %1 is invalid. It must contain Latin characters only, must not contain whitespace, and must match the regular expression %2.</source>
        <translation>ཨང་སྒྲིག་གི་ཨང་གྲངས་%1ནི་གོ་མི་ཆོད་པ་རེད། དེའི་ནང་དུ་ངེས་པར་དུ་ལ་ཏིང་ཡི་གེ་ཁོ་ན་འདུས་དགོས་པ་ལས་དཀར་པོའི་བར་སྟོང་གཏན་ནས་འདུས་མི་རུང་བར་མ་ཟད། ངེས་པར་དུ་རྒྱུན་ལྡན་གྱི་མཚོན་ཚུལ་དང</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>No function with signature %1 is available</source>
        <translation>མིང་རྟགས་འགོད་པའི་ནུས་པ་གང་ཡང་འདོན་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+72"/>
        <location line="+10"/>
        <source>A default namespace declaration must occur before function, variable, and option declarations.</source>
        <translation>默认情况下,名称的 བསྒྲགས་གཏམ་དེ་ངེས་པར་དུ་འགན་ནུས་དང་། འགྱུར་ལྡོགགདམ་གསེས་བཅས་ཀྱི་བསྒྲགས་གཏམ་མ་སྤེལ་གོང་དུ་འབྱུང་དགོས།</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Namespace declarations must occur before function, variable, and option declarations.</source>
        <translation>མིང་བར་སྟོང་གི་བསྒྲགས་གཏམ་དེ་ངེས་པར་དུ་བྱེད་ལས་དང་། འགྱུར་ལྡོགགདམ་གསེས་བཅས་ཀྱི་བསྒྲགས་གཏམ་གྱི་སྔོན་དུ་འབྱུང་</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Module imports must occur before function, variable, and option declarations.</source>
        <translation>མ་དཔེ་ནང་འདྲེན་བྱེད་པར་ངེས་པར་དུ་བྱེད་ནུས་དང་། འགྱུར་ལྡོགགདམ་གསེས་བཅས་ཀྱི་བསྒྲགས་གཏམ་མ་སྤེལ་གོང་</translation>
    </message>
    <message>
        <location line="+200"/>
        <source>It is not possible to redeclare prefix %1.</source>
        <translation>བསྐྱར་དུ་སྔོན་འཇུག་གི་གྲངས་ཀ་1ལ་བསྐྱར་དུ་བགོ་བཤའ་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Only the prefix %1 can be declared to bind the namespace %2. By default, it is already bound to the prefix %1.</source>
        <translation type="obsolete">至于前缀 %1 可以被声明为和命名空间 %2 绑定。默认情况下，它已经被绑定到前缀 %1。</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Prefix %1 is already declared in the prolog.</source>
        <translation>སྔོན་ཚུད་ནས་%1ནི་མ་རབས་ཀྱི་ནང་དུ་ཁྱབ་བསྒྲགས་བྱས་ཟིན།</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>The name of an option must have a prefix. There is no default namespace for options.</source>
        <translation>བསལ་འདེམས་ཀྱི་མིང་ལ་ངེས་པར་དུ་སྔོན་འགྲོའི་ཆ་རྐྱེན་ཡོད་དགོས། བསལ་འདེམས་ལ་ཁ་ཆད་དང་འགལ་བའི་མིང་གི་བར་སྟོང་མེད།</translation>
    </message>
    <message>
        <location line="+171"/>
        <source>The Schema Import feature is not supported, and therefore %1 declarations cannot occur.</source>
        <translation>Schema Import 功能,རྒྱབ་སྐྱོར་མི་བྱེད་པས། %1 བསྒྲགས་གཏམ་ཡང་འབྱུང་མི་སྲིད།</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The target namespace of a %1 cannot be empty.</source>
        <translation>%1%1%ཡི་དམིགས་འབེན་གྱི་མིང་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The module import feature is not supported</source>
        <translation>དཔེ་དབྱིབས་ནང་འདྲེན་གྱི་ཁྱད་ཆོས་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <source>A variable by name %1 has already been declared in the prolog.</source>
        <translation type="obsolete">名称为 %1 的变量已经在序言中声明过了。</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>No value is available for the external variable by name %1.</source>
        <translation>%1名称%1名称,1.0%ཡི་ཕྱི་རོལ་གྱི་འགྱུར་ལྡོག་ལ་རིན་ཐང་གང་ཡང་མེད།</translation>
    </message>
    <message>
        <source>The namespace for a user defined function cannot be empty(try the predefined prefix %1 which exists for cases like this)</source>
        <translation type="obsolete">用户定义的函数的命名空间不能为空(请试试预定义的前缀 %1，它就是用于这种情况的)。</translation>
    </message>
    <message>
        <location line="-4154"/>
        <source>A construct was encountered which only is allowed in XQuery.</source>
        <translation>XQueryནང་དུ་ད་གཟོད་ཆོག་མཆན་ཐོབ་པའི་འཛུགས་སྐྲུན་ཞིག་ལ་འཕྲད་པ་རེད།</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>A template by name %1 has already been declared.</source>
        <translation>%1名称模板,1.1.0.0.0.0.0.0.0.0.0.0.0.0</translation>
    </message>
    <message>
        <location line="+3581"/>
        <source>The keyword %1 cannot occur with any other mode name.</source>
        <translation>关键字 %1,其他模式名称</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>The value of attribute %1 must of type %2, which %3 isn&apos;t.</source>
        <translation>%1%2%ཡི་ངོ་བོ་དང་གཟུགས་དབྱིབས་ཀྱི་རིན་ཐང་ནི་%3ཡིན་པ་ལས་མེད།</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>The prefix %1 can not be bound. By default, it is already bound to the namespace %2.</source>
        <translation>%1%ཡི་སྔོན་འགྲོའི་ཆ་རྐྱེན་ལ་ཚོད་འཛིན་བྱེད་མི་ཐུབ། 默认情况下,名称%2。。</translation>
    </message>
    <message>
        <location line="+312"/>
        <source>A variable by name %1 has already been declared.</source>
        <translation>%1 མིང་ཐོག་ནས་འགྱུར་ལྡོག་བྱུང་ཚུལ་ཁྱབ་བསྒྲགས་བྱས་ཟིན།</translation>
    </message>
    <message>
        <location line="+135"/>
        <source>A stylesheet function must have a prefixed name.</source>
        <translation>བཟོ་དབྱིབས་རེའུ་མིག་གི་བྱེད་ལས་ལ་ངེས་པར་དུ་སྔོན་སྒྲིག་གི་མིང་ཞིག་ཡོད་དགོས།</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The namespace for a user defined function cannot be empty (try the predefined prefix %1 which exists for cases like this)</source>
        <translation>用户定义的功能名称空(ཚོད་ལྟ་འདི་ལྟ་བུའི་གནས་ཚུལ་འོག་གནས་པའི་སྔོན་འགྲོའི་ཆ་རྐྱེན་འོག་ཏུ། )</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The namespace %1 is reserved; therefore user defined functions may not use it. Try the predefined prefix %2, which exists for these cases.</source>
        <translation>མིང་གི་བར་སྟོང་%1 སོར་ཉར་བྱས་ཡོད། དེའི་རྐྱེན་གྱིས་སྤྱོད་མཁན་གྱིས་གཏན་འཁེལ་བྱས་པའི་བྱེད་ལས་ཀྱིས་དེ་བཀོལ་སྤྱོད་བྱས་མི་ གནས་ཚུལ་འདི་དག་གི་འོག་ཏུ་གནས་པའི་སྔོན་ཚུད་ནས་གཏན་འཁེལ་བྱས་པའི་སྔོན་འཇུག་གི་ཨང་གྲངས་2ལ་ཚོད་ལྟ་ཞིག་བྱས།</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>The namespace of a user defined function in a library module must be equivalent to the module namespace. In other words, it should be %1 instead of %2</source>
        <translation>དཔེ་མཛོད་ཁང་གི་དཔེ་མཛོད་ཁང་གི་མ་དཔེའི་ནང་དུ་གཏན་འཁེལ་བྱས་པའི་རྟེན་འབྱུང་གྲངས་ཀྱི་མིང་བར་སྟོང་དེ་ངེས་པར་དུ་དཔེ་མཛོད་ཁང་གི་མིང་བར་སྟོང་དང ཡང་གཅིག་བཤད་ན་དེ་ནི་%1ཡིན་དགོས་པ་ལས་%2མིན།</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>A function already exists with the signature %1.</source>
        <translation>མིང་རྟགས་འགོད་པའི་%1ཡོད་པའི་རྟེན་འབྱུང་གྲངས་ཤིག་ཡོད།</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>No external functions are supported. All supported functions can be used directly, without first declaring them as external</source>
        <translation>ཕྱི་རོལ་གྱི་བྱེད་ནུས་ལ་རྒྱབ་སྐྱོར་མི་བྱེད། འདེགས་སྐྱོར་གྱི་ནུས་པ་ཡོད་ཚད་ཐད་ཀར་བཀོལ་སྤྱོད་བྱས་ཆོག་པ་ལས་ཐོག་མར་ཕྱི་རོལ་དུ་ཁྱབ་བསྒྲགས་མ་བྱས་ན།</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>An argument by name %1 has already been declared. Every argument name must be unique.</source>
        <translation>%1རང་མིང་མི་འགོད་པའི་སྒྲུབ་བྱེད་ཅིག་ཁྱབ་བསྒྲགས་བྱས་ཟིན། སྒྲུབ་བྱེད་རེ་རེའི་མིང་ཚང་མ་ངེས་པར་དུ་ཐུན་མོང་</translation>
    </message>
    <message>
        <location line="+179"/>
        <source>When function %1 is used for matching inside a pattern, the argument must be a variable reference or a string literal.</source>
        <translation>%1功能,1模式 ནང་ཁུལ་དུ་ཆ་འགྲིག་པར་སྤྱོད་སྐབས། སྒྲུབ་བྱེད་ནི་ངེས་པར་དུ་འགྱུར་ཡོད་ཀྱི་དཔྱད་གཞི་ཞིག་གམ་ཡང་ན་ཡི་གེ་ཞིག་ཡིན་དགོས།</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>In an XSL-T pattern, the first argument to function %1 must be a string literal, when used for matching.</source>
        <translation>XSL-Tཡི་དཔེ་དབྱིབས་ཁྲོད་དུ་ཨང་དང་པོའི་ནུས་པ་འདོན་སྤེལ་བྱེད་པའི་སྒྲུབ་བྱེད་ནི་ངེས་པར་དུ་ཡི་གེ་ཡི་གེ་ཞིག་ཡིན་དགོས། སྐབས་དེར་ཆ་འགྲིག་པར་སྤྱོད་དགོས།</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>In an XSL-T pattern, the first argument to function %1 must be a literal or a variable reference, when used for matching.</source>
        <translation>XSL-Tཡི་དཔེ་དབྱིབས་ཁྲོད།%1功能་པའི་སྒྲུབ་བྱེད་དང་པོ་ནི་ངེས་པར་དུ་ཡིག་ཐོག་གམ་ཡང་ན་འགྱུར་ཡོད་ཀྱི་དཔྱད་གཞི་ཞིག་ཡིན་དགོས།</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>In an XSL-T pattern, function %1 cannot have a third argument.</source>
        <translation>XSL-Tཡི་དཔེ་དབྱིབས་ཁྲོད་དུ་ནུས་པ་%1ལ་སྒྲུབ་བྱེད་གསུམ་པ་ཡོད་མི་སྲིད།</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>In an XSL-T pattern, only function %1 and %2, not %3, can be used for matching.</source>
        <translation>XSL-Tཡི་དཔེ་དབྱིབས་ཁྲོད་དུ་ནུས་པ་%1དང་%2མ་གཏོགས་%3མ་གཏོགས་ཆ་འགྲིག་ལ་སྤྱོད་མི་རུང་།</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>In an XSL-T pattern, axis %1 cannot be used, only axis %2 or %3 can.</source>
        <translation>XSL-T模式,轴%1བཀོལ་སྤྱོད་བྱེད་མི་ཐུབ་པས། འཁོར་མདའ་2%འམ་ཡང་ན་%3མ་གཏོགས་བཀོལ་སྤྱོད་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+126"/>
        <source>%1 is an invalid template mode name.</source>
        <translation>%1ནི་གོ་མི་ཆོད་པའི་དཔེ་དབྱིབས་ཀྱི་མིང་ཞིག་ཡིན།</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>The name of a variable bound in a for-expression must be different from the positional variable. Hence, the two variables named %1 collide.</source>
        <translation>མཚོན་སྟངས་ཁྲོད་ཀྱི་འགྱུར་ཡོད་ཀྱི་མིང་དེ་ངེས་པར་དུ་གནས་པའི་འགྱུར་ལྡོག་དང་མི་འདྲ། དེའི་རྐྱེན་གྱིས་མིང་ལ་%1 ཟེར་བའི་འགྱུར་ལྡོག་དེ་གཉིས་ཕན་ཚུན་གདོང་གཏུག་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location line="+758"/>
        <source>The Schema Validation Feature is not supported. Hence, %1-expressions may not be used.</source>
        <translation>Schema Validation 功能 རྒྱབ་སྐྱོར་མི་བྱེད། དེའི་རྐྱེན་གྱིས་%1-མཚོན་ཚུལ་བཀོལ་སྤྱོད་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>None of the pragma expressions are supported. Therefore, a fallback expression must be present</source>
        <translation>དངོས་དོན་གཉེར་བའི་རྣམ་འགྱུར་ཚང་མར་རྒྱབ་སྐྱོར་མི་བྱེད། དེར་བརྟེན་ངེས་པར་དུ་ལྡོག་ཕྱོགས་ཀྱི་རྣམ་འགྱུར་ཞིག་ཡོད་དགོས།</translation>
    </message>
    <message>
        <location line="+267"/>
        <source>Each name of a template parameter must be unique; %1 is duplicated.</source>
        <translation>དཔེ་དབྱིབས་ཀྱི་གྲངས་ཀ་རེ་རེའི་མིང་ཚང་མ་ངེས་པར་དུ་ཐུན་མོང་མ་ཡིན་པ་ཞིག་ཡིན %1ནི་བསྐྱར་ཟློས་བྱས་པ་ཡིན།</translation>
    </message>
    <message>
        <location line="+129"/>
        <source>The %1-axis is unsupported in XQuery</source>
        <translation>XQueryཡི་ཁྲོད་དུ་%1-འཁོར་མདའ་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location line="+1150"/>
        <source>%1 is not a valid name for a processing-instruction.</source>
        <translation>%1ནི་ལས་སྣོན་མཛུབ་སྟོན་གྱི་གོ་ཆོད་པའི་མིང་ཞིག་མིན།</translation>
    </message>
    <message>
        <location line="-7029"/>
        <source>%1 is not a valid numeric literal.</source>
        <translation>%1ནི་གོ་ཆོད་པའི་གྲངས་ཀའི་ཡིག་ཐོག་གི་ཡི་གེ་ཞིག་མིན།</translation>
    </message>
    <message>
        <location line="+6165"/>
        <source>No function by name %1 is available.</source>
        <translation>%1ཡི་མིང་ཐོག་ནས་ནུས་པ་འདོན་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>The namespace URI cannot be the empty string when binding to a prefix, %1.</source>
        <translation>%1.1.0.1.1.1.1.1.1.1.1.1.1.1.1.1.1.1.1.1.1.1.1.1.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>%1 is an invalid namespace URI.</source>
        <translation>%1ནི་གོ་མི་ཆོད་པའི་མིང་བར་སྟོང་URIཡིན།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>It is not possible to bind to the prefix %1</source>
        <translation>སྔོན་འཇུག%1ལ་ཚོད་འཛིན་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Namespace %1 can only be bound to %2 (and it is, in either case, pre-declared).</source>
        <translation>名称%1ནི་%2(གནས་ཚུལ་གང་འདྲ་ཞིག་གི་འོག་ཏུའང་སྔོན་ཚུད་ནས་ཁྱབ་བསྒྲགས་བྱས་པ་རེད)ལ་ཚོད་འཛིན་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Prefix %1 can only be bound to %2 (and it is, in either case, pre-declared).</source>
        <translation>Prefix %1ནི་%2(གནས་ཚུལ་གང་འདྲ་ཞིག་གི་འོག་ཏུའང་སྔོན་ཚུད་ནས་ཁྱབ་བསྒྲགས་བྱས་པ་ཡིན་)ལ་ཚོད་འཛིན་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Two namespace declaration attributes have the same name: %1.</source>
        <translation>名称-1:%1</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>The namespace URI must be a constant and cannot use enclosed expressions.</source>
        <translation>名称区URI ངེས་པར་དུ་འགྱུར་བ་མེད་པ་ཞིག་ཡིན་དགོས་པ་ལས་བཀག་སྡོམ་བྱས་པའི་མཚོན་ཚུལ་སྤྱོད་མི་རུང་།</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>An attribute by name %1 has already appeared on this element.</source>
        <translation>%1名称%1名称,元素,名称%1,100000000000000000000000000000000000000</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>A direct element constructor is not well-formed. %1 is ended with %2.</source>
        <translation>ཐད་ཀའི་རྒྱུ་རྐྱེན་གཙོ་བོ་ཞིག་གྲུབ་ཚུལ་ལེགས་པོ་ཞིག་མ་རེད། %1ནི་%2ལ་བརྟེན་ནས་མཇུག་འགྲིལ་བ་རེད།</translation>
    </message>
    <message>
        <location line="+458"/>
        <source>The name %1 does not refer to any schema type.</source>
        <translation>%1名称,1 ཟེར་བ་ནི་རྣམ་པ་གང་རུང་གི་རིགས་དབྱིབས་ལ་ཟེར་བ་མིན།</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 is an complex type. Casting to complex types is not possible. However, casting to atomic types such as %2 works.</source>
        <translation>%1ནི་རྙོག་འཛིང་ཆེ་བའི་རིགས་ཤིག་ཡིན། རྙོག་འཛིང་ཆེ་བའི་རིགས་དབྱིབས་ལ་བཟོ་བཅོས་རྒྱག་མི་ཐུབ། འོན་ཀྱང་%2སོགས་རྡུལ་ཕྲན་གྱི་རིགས་དབྱིབས་ལ་བཟོ་བཅོས་རྒྱག་པའི་ནུས་པ་ཐོན་གྱི་ཡོད།</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1 is not an atomic type. Casting is only possible to atomic types.</source>
        <translation>%1ནི་རྡུལ་ཕྲན་གྱི་རིགས་ཤིག་མ་རེད། རྡུལ་ཕྲན་གྱི་རིགས་དབྱིབས་ཁོ་ནར་མ་གཏོགས་བཟོ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>%1 is not a valid name for a processing-instruction. Therefore this name test will never match.</source>
        <translation type="obsolete">%1 不是处理指令的有效名称。因此这个名称测试永远不会匹配。</translation>
    </message>
    <message>
        <location line="+145"/>
        <location line="+71"/>
        <source>%1 is not in the in-scope attribute declarations. Note that the schema import feature is not supported.</source>
        <translation>%1ནི་ཁྱབ་ཁོངས་ནང་གི་ཁྱད་ཆོས་ལྡན་པའི་བསྒྲགས་གཏམ་ནང་དུ་མེད། དོ་སྣང་བྱེད་དགོས་པ་ཞིག་ནི་རི་མོ་ནང་འདྲེན་གྱི་ཁྱད་ཆོས་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The name of an extension expression must be in a namespace.</source>
        <translation>ཁྱབ་གདལ་དུ་གཏོང་བའི་མཚོན་སྟངས་ཀྱི་མིང་དེ་ངེས་པར་དུ་མིང་གི་བར་སྟོང་དུ་ཡོད་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/type/qcardinality.cpp" line="+55"/>
        <source>empty</source>
        <translation>སྟོང་པ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>zero or one</source>
        <translation>ཀླད་ཀོར་དང་ཡང་ན་གཅིག</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>exactly one</source>
        <translation>གནད་ལ་འཁེལ་བའི་སྒོ་ནས་བཤད་</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>one or more</source>
        <translation>གཅིག་གམ་ཡང་ན་དེ་ལས་མང་</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>zero or more</source>
        <translation>ཀླད་ཀོར་དང་ཡང་ན་དེ་ལས</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/type/qtypechecker.cpp" line="+63"/>
        <source>Required type is %1, but %2 was found.</source>
        <translation>དགོས་ངེས་ཀྱི་རིགས་དབྱིབས་ནི་%1ཡིན་མོད། འོན་ཀྱང་%2རྙེད་པ་རེད།</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Promoting %1 to %2 may cause loss of precision.</source>
        <translation>%1%ནས་2%བར་ཁྱབ་གདལ་བཏང་ན་གནད་ལ་འཁེལ་བའི་གྱོང་གུན་བཟོ་སྲིད།</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>The focus is undefined.</source>
        <translation>གཙོ་གནད་ནི་གཏན་འཁེལ་བྱས་མེད་པ་ཞིག་རེད།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/utils/qoutputvalidator.cpp" line="+86"/>
        <source>It&apos;s not possible to add attributes after any other kind of node.</source>
        <translation>ས་ཆ་གཞན་པ་གང་དང་གང་གི་རྗེས་སུ་ངོ་བོ་དང་གཟུགས་ཀྱི་ཁྱད་ཆོས་ཁ་སྣོན་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>An attribute by name %1 has already been created.</source>
        <translation>%1名称%1 名称,名称%1,10000000000000000000000000000000000</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/utils/qxpathhelper_p.h" line="+120"/>
        <source>Only the Unicode Codepoint Collation is supported(%1). %2 is unsupported.</source>
        <translation>Unicode Codepoint ལེགས་སྒྲིག་བྱེད་པར་རྒྱབ་སྐྱོར་བྱེད་ཀྱིན་ཡོད། (%1) %2ནི་རྒྱབ་སྐྱོར་མི་བྱེད་པ་ཞིག་རེད།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/api/qxmlserializer.cpp" line="+60"/>
        <source>Attribute %1 can&apos;t be serialized because it appears at the top level.</source>
        <translation>%1%1ནི་ཆེས་མཐོ་བའི་རིམ་པ་ཞིག་ཏུ་བྱུང་བའི་རྐྱེན་གྱིས་གོ་རིམ་སྒྲིག་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/acceltree/qacceltreeresourceloader.cpp" line="+314"/>
        <source>%1 is an unsupported encoding.</source>
        <translation>%1ནི་རྒྱབ་སྐྱོར་མི་བྱེད་པའི་ཨང་སྒྲིག་ཅིག་ཡིན།</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>%1 contains octets which are disallowed in the requested encoding %2.</source>
        <translation>%1ནང་དུ་རེ་འདུན་ཞུ་ཡིག་གི་ཨང་སྒྲིག་ཁྲོད་དུ་མེད་པར་བཟོས་པའི་ཨང་ཀི་བརྒྱད་པ་འདུས་ཡོད།</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The codepoint %1, occurring in %2 using encoding %3, is an invalid XML character.</source>
        <translation>%1,2.3,代码%3,3,གོ་མི་ཆོད་པའི་XML ཡི་གེ་ཞིག་རེད།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qapplytemplate.cpp" line="+119"/>
        <source>Ambiguous rule match.</source>
        <translation>གསལ་ལ་མི་གསལ་བའི་སྒྲིག་སྲོལ་དང་མཐུན་པ་རེད།</translation>
    </message>
    <message>
        <source>In a namespace constructor, the value for a namespace value cannot be an empty string.</source>
        <translation type="obsolete">在一个命名空间构造中，命名空间的值不能为空字符串。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcomputednamespaceconstructor.cpp" line="+69"/>
        <source>In a namespace constructor, the value for a namespace cannot be an empty string.</source>
        <translation>མིང་གི་བར་སྟོང་འཛུགས་སྐྲུན་བྱེད་མཁན་ཁྲོད་དུ་མིང་གི་བར་སྟོང་གི་རིན་ཐང་ནི་སྟོང་བའི་རྒྱུད་སྐུད་ཅིག་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>The prefix must be a valid %1, which %2 is not.</source>
        <translation>སྔོན་མ་ནི་ངེས་པར་དུ་གོ་ཆོད་པའི་%1ཡིན་དགོས།%2ནི་དེ་འདྲ་མ་རེད།</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>The prefix %1 cannot be bound.</source>
        <translation>%1%ཡི་སྔོན་འཇུག་ལ་ཚོད་འཛིན་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Only the prefix %1 can be bound to %2 and vice versa.</source>
        <translation>སྔོན་གྱི་%1ཁོ་ནས་ད་གཟོད་%2ལ་ཚོད་འཛིན་བྱེད་ཐུབ་པ་དང་དེ་ལས་ལྡོག་སྟེ་དེ་ལས་ལྡོག་སྟེ་དེ་ལས་ལྡོག་སྟེ་དེ་ལས་ལྡོག་སྟེ</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qevaluationcache.cpp" line="+117"/>
        <source>Circularity detected</source>
        <translation>ཞིབ་དཔྱད་ཚད་ལེན་བྱས་པའི་འཁོར་རྒྱུག་རང་བཞིན།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qtemplate.cpp" line="+145"/>
        <source>The parameter %1 is required, but no corresponding %2 is supplied.</source>
        <translation>参数%1དགོས་མོད། འོན་ཀྱང་དེ་མཚུངས་ཀྱི་%2མ་མཁོ་སྤྲོད་བྱས་མེད།</translation>
    </message>
    <message>
        <location line="-71"/>
        <source>The parameter %1 is passed, but no corresponding %2 exists.</source>
        <translation>参数%1 གྲོས་འཆམ་བྱུང་མོད། འོན་ཀྱང་དེ་མཚུངས་ཀྱི་%2གནས་མེད།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qunparsedtextfn.cpp" line="+65"/>
        <source>The URI cannot have a fragment</source>
        <translation>URIལ་དུམ་བུ་ཞིག་ཡོད་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qxslttokenizer.cpp" line="+519"/>
        <source>Element %1 is not allowed at this location.</source>
        <translation>ས་ཆ་འདི་ནས་རྒྱུ་རྐྱེན་གཙོ་བོ་%1བློ་མི་ཆོག</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Text nodes are not allowed at this location.</source>
        <translation>ས་ཆ་འདི་ནས་ཡི་གེའི་ས་ཐིག་གཏན་ནས་བྱས་མི་ཆོག</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Parse error: %1</source>
        <translation>ནོར་འཁྲུལ་ལ་གཞིགས་ནས་བཤད་ན། %1</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>The value of the XSL-T version attribute must be a value of type %1, which %2 isn&apos;t.</source>
        <translation>XSL-T版本 ངོ་བོ་ལྡན་པའི་རིན་ཐང་ནི་ངེས་པར་དུ་རིགས་དབྱིབས་ཀྱི་རིན་ཐང་ནི་%1ཡིན་དགོས་ཤིང་། %2ནི་དེ་འདྲ་མ་རེད།</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Running an XSL-T 1.0 stylesheet with a 2.0 processor.</source>
        <translation>2.0 处理器,XSL-T 1.0རྟགས་ཅན་གྱི་རེའུ་མིག་ཅིག་འཁོར་སྐྱོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Unknown XSL-T attribute %1.</source>
        <translation>%1.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Attribute %1 and %2 are mutually exclusive.</source>
        <translation>%1和 %2 གཉིས་ནི་ཕན་ཚུན་ཟུར་འབུད་བྱས་པ་ཡིན།</translation>
    </message>
    <message>
        <location line="+166"/>
        <source>In a simplified stylesheet module, attribute %1 must be present.</source>
        <translation>སྟབས་བདེའི་བཟོ་དབྱིབས་རེའུ་མིག་གི་མ་དཔེའི་ནང་དུ་ངེས་པར་དུ་ངོ་བོ་དང་གཟུགས་དབྱིབས་ཀྱི་1ཡོད་དགོས།</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>If element %1 has no attribute %2, it cannot have attribute %3 or %4.</source>
        <translation>%1%1%2,%1%1%1%1%1%1%1%</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Element %1 must have at least one of the attributes %2 or %3.</source>
        <translation>%1元素%1 ངེས་པར་དུ་མ་མཐར་ཡང་དེའི་ནང་གི་གཅིག་ཡོད་དགོས།%2或%3。</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>At least one mode must be specified in the %1-attribute on element %2.</source>
        <translation>&quot;%2&quot;སྟེང་གི་%1-attributeནང་དུ་ཉུང་མཐར་ཡང་དཔེ་དབྱིབས་གཅིག་གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qmaintainingreader.cpp" line="+183"/>
        <source>Attribute %1 cannot appear on the element %2. Only the standard attributes can appear.</source>
        <translation>%2 元素%2 ཚད་ལྡན་གྱི་ཁྱད་ཆོས་མ་གཏོགས་འབྱུང་མི་སྲིད།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Attribute %1 cannot appear on the element %2. Only %3 is allowed, and the standard attributes.</source>
        <translation>%2 元素%2 %3མ་གཏོགས་མི་ཆོག་པ་དང་། ཚད་ལྡན་གྱི་ཁྱད་ཆོས་ཀྱང་ཡོད།</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Attribute %1 cannot appear on the element %2. Allowed is %3, %4, and the standard attributes.</source>
        <translation>%2 元素%2 ཆོག་མཆན་ཐོབ་པ་ནི་%3%དང་། %4། ཚད་ལྡན་གྱི་ཁྱད་ཆོས་བཅས་ཡིན།</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Attribute %1 cannot appear on the element %2. Allowed is %3, and the standard attributes.</source>
        <translation>%2 元素%2 ཆོག་མཆན་ཐོབ་པ་ནི་%3ཡིན་པ་དང་། ཚད་ལྡན་གྱི་ཁྱད་ཆོས་ཡིན།</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>XSL-T attributes on XSL-T elements must be in the null namespace, not in the XSL-T namespace which %1 is.</source>
        <translation>XSL-T元素 སྟེང་གི་XSL-Tཡི་ཁྱད་ཆོས་ནི་ངེས་པར་དུ་མིང་སྟོང་བའི་བར་སྟོང་དུ་ཡོད་དགོས་པ་ལས། %1ནི་XSL-Tཡི་མིང་གི་བར་སྟོང་དུ་ཡོད་མི་རུང་།</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>The attribute %1 must appear on element %2.</source>
        <translation>&quot;%1&quot;&quot;%1&quot;&quot;%1&quot;%2&quot;%2%</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The element with local name %1 does not exist in XSL-T.</source>
        <translation>ཆ་ཤས་ཀྱི་མིང་ལ་%1ཡོད་པའི་རྒྱུ་རྐྱེན་གཙོ་བོ་ནི་XSL-Tནང་དུ་གནས་མེད།</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qxslttokenizer.cpp" line="+123"/>
        <source>Element %1 must come last.</source>
        <translation>གཞི་རྒྱུ་%1ནི་ངེས་པར་དུ་རྒྱུན་མཐུད་ཐུབ་པ་ཞིག་ཡིན</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>At least one %1-element must occur before %2.</source>
        <translation>ཉུང་མཐར་ཡང་%1-elementནི་ངེས་པར་དུ་%2ཀྱི་སྔོན་དུ་འབྱུང་དགོས།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Only one %1-element can appear.</source>
        <translation>%1-元素 གཅིག་མ་གཏོགས་འབྱུང་མི་སྲིད།</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>At least one %1-element must occur inside %2.</source>
        <translation>ཉུང་མཐར་ཡང་%1-elementནི་ངེས་པར་དུ་%2ནང་ཁུལ་དུ་འབྱུང་དགོས།</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>When attribute %1 is present on %2, a sequence constructor cannot be used.</source>
        <translation>%2 སྟེང་གི་ངོ་བོ་དང་གཟུགས་ཚད་ནི་1ཡིན་པའི་སྐབས་སུ་གོ་རིམ་སྒྲིག་ཆས་བཀོལ་སྤྱོད་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Element %1 must have either a %2-attribute or a sequence constructor.</source>
        <translation>元素%1 ངེས་པར་དུ་%2-attributeའམ་ཡང་ན་གོ་རིམ་སྒྲིག་ཆས་ཡོད་དགོས།</translation>
    </message>
    <message>
        <location line="+125"/>
        <source>When a parameter is required, a default value cannot be supplied through a %1-attribute or a sequence constructor.</source>
        <translation>参数 དགོས་པའི་སྐབས་སུ་%1-attribute或序列 འཛུགས་སྐྲུན་ཡོ་བྱད་བརྒྱུད་ནས་ཁ་ཆད་དང་འགལ་བའི་རིན་ཐང་འདོན་སྤྲོད་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+270"/>
        <source>Element %1 cannot have children.</source>
        <translation>གཞི་རྒྱུ་%1ལ་ཕྲུ་གུ་སྐྱེ་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+434"/>
        <source>Element %1 cannot have a sequence constructor.</source>
        <translation>元素%1ལ་གོ་རིམ་སྒྲིག་ཆས་ཡོད་མི་སྲིད།</translation>
    </message>
    <message>
        <location line="+86"/>
        <location line="+9"/>
        <source>The attribute %1 cannot appear on %2, when it is a child of %3.</source>
        <translation>%1%2%ཡི་སྟེང་དུ་བྱུང་མི་སྲིད། སྐབས་དེར་དེ་ནི་%3གྱི་བྱིས་པ་ཡིན།</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>A parameter in a function cannot be declared to be a tunnel.</source>
        <translation>རྟེན་འབྱུང་གྲངས་ཁྲོད་ཀྱི་གྲངས་ཀ་ནི་ཕུག་ལམ་ཡིན་པ་ཁྱབ་བསྒྲགས་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+149"/>
        <source>This processor is not Schema-aware and therefore %1 cannot be used.</source>
        <translation>处理器 འདི་ནི་Schema-aware མིན་པས་%1བཀོལ་སྤྱོད་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Top level stylesheet elements must be in a non-null namespace, which %1 isn&apos;t.</source>
        <translation>1%1的非 མིང་གི་བར་སྟོང་དུ་ངེས་པར་དུ་རིམ་པ་མཐོ་ཤོས་ཀྱི་བཟོ་དབྱིབས་རེའུ་མིག་གི་རྒྱུ་རྐྱེན་གཙོ་བོ་ཡོད་དགོས།</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The value for attribute %1 on element %2 must either be %3 or %4, not %5.</source>
        <translation>元素%2 སྟེང་གི་ངོ་བོ་དང་གཟུགས་པོའི་རིན་ཐང་ནི་ངེས་པར་དུ་%3%འམ་ཡང་ན་%4ཡིན་དགོས་པ་ལས་%5མི་རུང་།</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Attribute %1 cannot have the value %2.</source>
        <translation>%1%1值%2</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>The attribute %1 can only appear on the first %2 element.</source>
        <translation>%1&quot;%1&quot;&quot;%1&quot;%2&quot;%2&quot;%2%%1%1%1%1%1%1%</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>At least one %1 element must appear as child of %2.</source>
        <translation>ཉུང་མཐར་ཡང་%1%ཡི་རྒྱུ་རྐྱེན་གཙོ་བོ་ནི་ངེས་པར་དུ་%2ཀྱི་བྱིས་པར་བརྩི་དགོས།</translation>
    </message>
</context>
<context>
    <name>VolumeSlider</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/volumeslider.cpp" line="+67"/>
        <source>Muted</source>
        <translation>གཞན་འགྱུར་བྱུང་བ།</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+15"/>
        <source>Volume: %1%</source>
        <translation>བོངས་ཚད: %1%</translation>
    </message>
</context>
<context>
    <name>WebCore::PlatformScrollbar</name>
    <message>
        <source>Scroll here</source>
        <translation type="obsolete">滚动到这里</translation>
    </message>
    <message>
        <source>Left edge</source>
        <translation type="obsolete">左边缘</translation>
    </message>
    <message>
        <source>Top</source>
        <translation type="obsolete">顶部</translation>
    </message>
    <message>
        <source>Right edge</source>
        <translation type="obsolete">右边缘</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation type="obsolete">底部</translation>
    </message>
    <message>
        <source>Page left</source>
        <translation type="obsolete">左一页</translation>
    </message>
    <message>
        <source>Page up</source>
        <translation type="obsolete">上一页</translation>
    </message>
    <message>
        <source>Page right</source>
        <translation type="obsolete">右一页</translation>
    </message>
    <message>
        <source>Page down</source>
        <translation type="obsolete">下一页</translation>
    </message>
    <message>
        <source>Scroll left</source>
        <translation type="obsolete">向左滚动</translation>
    </message>
    <message>
        <source>Scroll up</source>
        <translation type="obsolete">向上滚动</translation>
    </message>
    <message>
        <source>Scroll right</source>
        <translation type="obsolete">向右滚动</translation>
    </message>
    <message>
        <source>Scroll down</source>
        <translation type="obsolete">向下滚动</translation>
    </message>
</context>
<context>
    <name>QPlatformTheme</name>
    <message>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>གྲོན་ཆུང་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>Save All</source>
        <translation>ཚང་མ་ཉར་ཚགས་ཡག་པོ</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;རེད།</translation>
    </message>
    <message>
        <source>Yes to &amp;All</source>
        <translation>ཚང་མ་ཁས་བླངས་པ་རེད།</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&gt; མི་ཆོག</translation>
    </message>
    <message>
        <source>N&amp;o to All</source>
        <translation>N&amp;oཡིས་མི་ཚང་མར་སྤྲད་པ་རེད།</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>མངལ་ཤོར་དུ་འཇུག་པ།</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation>བསྐྱར་དུ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>སྣང་མེད་དུ་བཞག་</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation>བེད་མེད་དུ་བསྐྱུར་བ།</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>རེ་ཞུ་</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>བསྐྱར་དུ་བཀོད་སྒྲིག་བྱེད་པ</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation>སྔོན་ཆད་ཀྱི་གནས་ཚུལ་སླར་གསོ་</translation>
    </message>
</context>
<context>
    <name>QGnomeTheme</name>
    <message>
        <source>&amp;OK</source>
        <translation>&amp; ཆོག</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;ཉར་ཚགས་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&gt; མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Close without Saving</source>
        <translation>གྲོན་ཆུང་མ་བྱས་པར་སྒོ་རྒྱག་</translation>
    </message>
</context>
<context>
    <name>QWidgetTextControl</name>
    <message>
        <source>&amp;Undo</source>
        <translation>་་</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation>ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་བསྐྱར་དུ་</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation>Cu&gt;t</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>&amp; འདྲ་བཤུས།</translation>
    </message>
    <message>
        <source>Copy &amp;Link Location</source>
        <translation>འདྲ་བཤུས་དང་སྦྲེལ་མཐུད་བྱེད་ས།</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation>&amp; paste</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation>ཚང་མ་བདམས་པ་</translation>
    </message>
</context>
<context>
    <name>QPrintPropertiesDialog</name>
    <message>
        <source>Printer Properties</source>
        <translation>打印机属性</translation>
    </message>
    <message>
        <source>Job Options</source>
        <translation>ལས་གནས་ཀྱི་གདམ་ག</translation>
    </message>
    <message>
        <source>Page Setup Conflicts</source>
        <translation>ཤོག་ངོས་སྒྲིག་འཛུགས་ཀྱི་འགལ་བ།</translation>
    </message>
    <message>
        <source>There are conflicts in page setup options. Do you want to fix them?</source>
        <translation>ཤོག་ངོས་སྒྲིག་འཛུགས་ཀྱི་བསལ་འདེམས་ཁྲོད་དུ་འགལ་བ་ཡོད། ཁྱོད་ཀྱིས་དེ་དག་ཞིག་གསོ་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <source>Advanced Option Conflicts</source>
        <translation>སྔོན་ཐོན་གྱི་བསལ་འདེམས་ཀྱི་འགལ་བ།</translation>
    </message>
    <message>
        <source>There are conflicts in some advanced options. Do you want to fix them?</source>
        <translation>སྔོན་ཐོན་གྱི་བསལ་འདེམས་ཁ་ཤས་ལ་འགལ་བ་ཡོད། ཁྱོད་ཀྱིས་དེ་དག་ཞིག་གསོ་བྱེད་དགོས་སམ།</translation>
    </message>
</context>
<context>
    <name>QPrintDialog</name>
    <message>
        <source>Left to Right, Top to Bottom</source>
        <translation>གཡོན་ནས་གཡས་སུ་འགྲོ་བ་དང་། གོང་ནས་འོག་བར་དུ</translation>
    </message>
    <message>
        <source>Left to Right, Bottom to Top</source>
        <translation>གཡོན་ནས་གཡས་སུ་འགྲོ་བ་དང་། འོག་ནས་གོང་བར་དུ</translation>
    </message>
    <message>
        <source>Right to Left, Bottom to Top</source>
        <translation>གཡས་གཡོན་དང་འོག་ནས་གོང་བར།</translation>
    </message>
    <message>
        <source>Right to Left, Top to Bottom</source>
        <translation>གཡས་གཡོན་དང་། གོང་ནས་འོག་བར།</translation>
    </message>
    <message>
        <source>Bottom to Top, Left to Right</source>
        <translation>འོག་ནས་གོང་བར་དང་། གཡོན་ནས་གཡས་སུ་བསྐྱོད།</translation>
    </message>
    <message>
        <source>Bottom to Top, Right to Left</source>
        <translation>འོག་ནས་གོང་བར་དང་། གཡས་གཡོན་དུ་ཡོད།</translation>
    </message>
    <message>
        <source>Top to Bottom, Left to Right</source>
        <translation>གོང་ནས་འོག་བར་དང་། གཡོན་ནས་གཡས་སུ་འགྲོ་བ།</translation>
    </message>
    <message>
        <source>Top to Bottom, Right to Left</source>
        <translation>གོང་ནས་འོག་བར་དང་། གཡས་གཡོན་དུ་ཡོད།</translation>
    </message>
    <message>
        <source>1 (1x1)</source>
        <translation>1(1x1)</translation>
    </message>
    <message>
        <source>2 (2x1)</source>
        <translation>2(2x1)</translation>
    </message>
    <message>
        <source>4 (2x2)</source>
        <translation>4(2x2)</translation>
    </message>
    <message>
        <source>6 (2x3)</source>
        <translation>6(2x3)</translation>
    </message>
    <message>
        <source>9 (3x3)</source>
        <translation>9(3x3)</translation>
    </message>
    <message>
        <source>16 (4x4)</source>
        <translation>16(4x4)</translation>
    </message>
    <message>
        <source>All Pages</source>
        <translation>ཤོག་ངོས་ཡོད་ཚད།</translation>
    </message>
    <message>
        <source>Odd Pages</source>
        <translation>ཡ་མཚན་ཆེ་བའི་ཤོག་ལྷེ།</translation>
    </message>
    <message>
        <source>Even Pages</source>
        <translation>ཐ་ན་ཤོག་ལྷེ་ཡང་ཡོད།</translation>
    </message>
</context>
<context>
    <name>QPageSize</name>
    <message>
        <source>A0</source>
        <translation>A0</translation>
    </message>
    <message>
        <source>A1</source>
        <translation>A1</translation>
    </message>
    <message>
        <source>A2</source>
        <translation>A2</translation>
    </message>
    <message>
        <source>A3</source>
        <translation>A3</translation>
    </message>
    <message>
        <source>A4</source>
        <translation>A4</translation>
    </message>
    <message>
        <source>A5</source>
        <translation>A5</translation>
    </message>
    <message>
        <source>A6</source>
        <translation>A6</translation>
    </message>
    <message>
        <source>A7</source>
        <translation>A7</translation>
    </message>
    <message>
        <source>A8</source>
        <translation>A8</translation>
    </message>
    <message>
        <source>A9</source>
        <translation>A9</translation>
    </message>
    <message>
        <source>A10</source>
        <translation>A10</translation>
    </message>
    <message>
        <source>B0</source>
        <translation>B0</translation>
    </message>
    <message>
        <source>B1</source>
        <translation>B1</translation>
    </message>
    <message>
        <source>B2</source>
        <translation>B2</translation>
    </message>
    <message>
        <source>B3</source>
        <translation>B3</translation>
    </message>
    <message>
        <source>B4</source>
        <translation>B4</translation>
    </message>
    <message>
        <source>B5</source>
        <translation>B5</translation>
    </message>
    <message>
        <source>B6</source>
        <translation>B6</translation>
    </message>
    <message>
        <source>B7</source>
        <translation>B7</translation>
    </message>
    <message>
        <source>B8</source>
        <translation>B8</translation>
    </message>
    <message>
        <source>B9</source>
        <translation>B9</translation>
    </message>
    <message>
        <source>B10</source>
        <translation>B10</translation>
    </message>
    <message>
        <source>Executive (7.5 x 10 in)</source>
        <translation>ལག་བསྟར་མི་སྣ། (7.5 x 10 in)</translation>
    </message>
    <message>
        <source>Executive (7.25 x 10.5 in)</source>
        <translation>ལག་བསྟར་མི་སྣ། (7.25 x 10.5 in)</translation>
    </message>
    <message>
        <source>Folio (8.27 x 13 in)</source>
        <translation>ཧྥུ་ལི་ཨོ་(8.27 x 13 in)</translation>
    </message>
    <message>
        <source>Legal</source>
        <translation>བཅའ་ཁྲིམས།</translation>
    </message>
    <message>
        <source>Letter / ANSI A</source>
        <translation>འཕྲིན་ཡིག / ANSI A</translation>
    </message>
    <message>
        <source>Tabloid / ANSI B</source>
        <translation>ཚགས་པར་ / ANSI B</translation>
    </message>
    <message>
        <source>Ledger / ANSI B</source>
        <translation>རྩིས་ཐོ། / ANSI B</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>གོམས་སྲོལ།</translation>
    </message>
    <message>
        <source>A3 Extra</source>
        <translation>A3འཕར་སྣོན་བྱུང་བ།</translation>
    </message>
    <message>
        <source>A4 Extra</source>
        <translation>A4འཕར་སྣོན་བྱུང་བ།</translation>
    </message>
    <message>
        <source>A4 Plus</source>
        <translation>A4 Plus</translation>
    </message>
    <message>
        <source>A4 Small</source>
        <translation>A4 ཆུང་བ།</translation>
    </message>
    <message>
        <source>A5 Extra</source>
        <translation>A5འཕར་སྣོན་བྱུང་བ།</translation>
    </message>
    <message>
        <source>B5 Extra</source>
        <translation>B5འཕར་སྣོན་བྱུང་བ།</translation>
    </message>
    <message>
        <source>JIS B0</source>
        <translation>JIS B0</translation>
    </message>
    <message>
        <source>JIS B1</source>
        <translation>JIS B1</translation>
    </message>
    <message>
        <source>JIS B2</source>
        <translation>JIS B2</translation>
    </message>
    <message>
        <source>JIS B3</source>
        <translation>JIS B3</translation>
    </message>
    <message>
        <source>JIS B4</source>
        <translation>JIS B4</translation>
    </message>
    <message>
        <source>JIS B5</source>
        <translation>JIS B5</translation>
    </message>
    <message>
        <source>JIS B6</source>
        <translation>JIS B6</translation>
    </message>
    <message>
        <source>JIS B7</source>
        <translation>JIS B7</translation>
    </message>
    <message>
        <source>JIS B8</source>
        <translation>JIS B8</translation>
    </message>
    <message>
        <source>JIS B9</source>
        <translation>JIS B9</translation>
    </message>
    <message>
        <source>JIS B10</source>
        <translation>JIS B10</translation>
    </message>
    <message>
        <source>ANSI C</source>
        <translation>ཨན་ཞི་C</translation>
    </message>
    <message>
        <source>ANSI D</source>
        <translation>ཨན་ཞི་ཏི།</translation>
    </message>
    <message>
        <source>ANSI E</source>
        <translation>ཨན་ཞི་E</translation>
    </message>
    <message>
        <source>Legal Extra</source>
        <translation>བཅའ་ཁྲིམས་ཀྱི་ཚད་བརྒལ་</translation>
    </message>
    <message>
        <source>Letter Extra</source>
        <translation>འཕྲིན་ཡིག་གི་ཁ་སྣོན</translation>
    </message>
    <message>
        <source>Letter Plus</source>
        <translation>འཕྲིན་ཡིག་ཁ་སྣོན་བྱས་</translation>
    </message>
    <message>
        <source>Letter Small</source>
        <translation>འཕྲིན་ཡིག་ཆུང་ཆུང་།</translation>
    </message>
    <message>
        <source>Tabloid Extra</source>
        <translation>ཚགས་པར་གྱི་ཚད་ལས་བརྒལ་བ།</translation>
    </message>
    <message>
        <source>Architect A</source>
        <translation>ཨར་ལས་པ་A</translation>
    </message>
    <message>
        <source>Architect B</source>
        <translation>ཨར་ལས་པ་B</translation>
    </message>
    <message>
        <source>Architect C</source>
        <translation>ཨར་ལས་པ་C</translation>
    </message>
    <message>
        <source>Architect D</source>
        <translation>འཛུགས་སྐྲུན་པ་D</translation>
    </message>
    <message>
        <source>Architect E</source>
        <translation>ཨར་ལས་པ་E</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>མཆན་འགྲེལ།</translation>
    </message>
    <message>
        <source>Quarto</source>
        <translation>ཁྭ་ཐའོ་ཡིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <source>Statement</source>
        <translation>གསལ་བསྒྲགས།</translation>
    </message>
    <message>
        <source>Super A</source>
        <translation>རིམ་འདས་A</translation>
    </message>
    <message>
        <source>Super B</source>
        <translation>རིམ་འདས་B</translation>
    </message>
    <message>
        <source>Postcard</source>
        <translation>ཡིག་སྐོགས་མེད་པའི་འཕྲིན</translation>
    </message>
    <message>
        <source>Double Postcard</source>
        <translation>ཡིག་སྐོགས་མེད་པའི་འཕྲིན་ཡིག</translation>
    </message>
    <message>
        <source>PRC 16K</source>
        <translation>PRC 16K</translation>
    </message>
    <message>
        <source>PRC 32K</source>
        <translation>PRC 32K</translation>
    </message>
    <message>
        <source>PRC 32K Big</source>
        <translation>PRC 32K Big</translation>
    </message>
    <message>
        <source>Fan-fold US (14.875 x 11 in)</source>
        <translation>རླུང་གཡབ་ཨ་མེ་རི་ཁ། (14.875 x 11 in)</translation>
    </message>
    <message>
        <source>Fan-fold German (8.5 x 12 in)</source>
        <translation>རླུང་གཡབ་ཀྱི་འཇར་མན་གྱི་སྐད།(8.5 x 12 in)</translation>
    </message>
    <message>
        <source>Fan-fold German Legal (8.5 x 13 in)</source>
        <translation>ཧྥན་ཧྥུ་ཏེ་འཇར་མན་གྱི་བཅའ་ཁྲིམས་(8.5 x 13 in)</translation>
    </message>
    <message>
        <source>Envelope B4</source>
        <translation>ཡིག་ཤུབས་B4</translation>
    </message>
    <message>
        <source>Envelope B5</source>
        <translation>ཡིག་ཤུབས་B5</translation>
    </message>
    <message>
        <source>Envelope B6</source>
        <translation>ཡིག་ཤུབས་B6</translation>
    </message>
    <message>
        <source>Envelope C0</source>
        <translation>ཡིག་ཤུབས་C0</translation>
    </message>
    <message>
        <source>Envelope C1</source>
        <translation>ཡིག་ཤུབས་C1</translation>
    </message>
    <message>
        <source>Envelope C2</source>
        <translation>ཡིག་ཤུབས་C2</translation>
    </message>
    <message>
        <source>Envelope C3</source>
        <translation>ཡིག་ཤུབས་C3</translation>
    </message>
    <message>
        <source>Envelope C4</source>
        <translation>ཡིག་ཤུབས་C4</translation>
    </message>
    <message>
        <source>Envelope C5</source>
        <translation>ཡིག་ཤུབས་C5</translation>
    </message>
    <message>
        <source>Envelope C6</source>
        <translation>ཡིག་ཤུབས་C6</translation>
    </message>
    <message>
        <source>Envelope C65</source>
        <translation>ཡིག་ཤུབས་C65</translation>
    </message>
    <message>
        <source>Envelope C7</source>
        <translation>ཡིག་ཤུབས་C7</translation>
    </message>
    <message>
        <source>Envelope DL</source>
        <translation>ཡིག་ཤུབས་DL</translation>
    </message>
    <message>
        <source>Envelope US 9</source>
        <translation>ཡིག་ཤུབས་ཨ་མེ་རི་ཁ་9</translation>
    </message>
    <message>
        <source>Envelope US 10</source>
        <translation>ཡིག་ཤུབས་ཨ་རིའི་ཨང་10</translation>
    </message>
    <message>
        <source>Envelope US 11</source>
        <translation>ཡིག་ཤུབས་ཨ་རིའི་ཨང་11</translation>
    </message>
    <message>
        <source>Envelope US 12</source>
        <translation>ཡིག་ཤུབས་ཨ་རིའི་ཨང་12</translation>
    </message>
    <message>
        <source>Envelope US 14</source>
        <translation>ཡིག་ཤུབས་ཨ་རིའི་ཨང་14པ།</translation>
    </message>
    <message>
        <source>Envelope Monarch</source>
        <translation>ཡིག་ཤུབས་རྒྱལ་པོ།</translation>
    </message>
    <message>
        <source>Envelope Personal</source>
        <translation>ཡིག་ཤུབས་མི་སྒེར་གྱི་ཡིག་ཤུབས་</translation>
    </message>
    <message>
        <source>Envelope Chou 3</source>
        <translation>ཡིག་ཤུབས་ཀྲོའུ་3</translation>
    </message>
    <message>
        <source>Envelope Chou 4</source>
        <translation>ཡིག་ཤུབས་ཀྲོའུ་4</translation>
    </message>
    <message>
        <source>Envelope Invite</source>
        <translation>ཡིག་ཤུབས་གདན་འདྲེན་ཞུ་ཡིག</translation>
    </message>
    <message>
        <source>Envelope Italian</source>
        <translation>ཡིག་ཤུབས་དབྱི་ཐ་ལིའི་ཡིག་ཤུབས།</translation>
    </message>
    <message>
        <source>Envelope Kaku 2</source>
        <translation>ཡིག་ཤུབས་ཁ་ཁུའུ་2</translation>
    </message>
    <message>
        <source>Envelope Kaku 3</source>
        <translation>ཡིག་ཤུབས་ཁ་ཁུའུ་3</translation>
    </message>
    <message>
        <source>Envelope PRC 1</source>
        <translation>ཡིག་ཤུབས་PRC 1</translation>
    </message>
    <message>
        <source>Envelope PRC 2</source>
        <translation>ཡིག་ཤུབས་PRC 2</translation>
    </message>
    <message>
        <source>Envelope PRC 3</source>
        <translation>ཡིག་ཤུབས་PRC 3</translation>
    </message>
    <message>
        <source>Envelope PRC 4</source>
        <translation>ཡིག་ཤུབས་PRC4</translation>
    </message>
    <message>
        <source>Envelope PRC 5</source>
        <translation>ཡིག་ཤུབས་PRC 5</translation>
    </message>
    <message>
        <source>Envelope PRC 6</source>
        <translation>ཡིག་ཤུབས་PRC6</translation>
    </message>
    <message>
        <source>Envelope PRC 7</source>
        <translation>ཡིག་ཤུབས་PRC7</translation>
    </message>
    <message>
        <source>Envelope PRC 8</source>
        <translation>ཡིག་ཤུབས་PRC 8</translation>
    </message>
    <message>
        <source>Envelope PRC 9</source>
        <translation>ཡིག་ཤུབས་PRC 9</translation>
    </message>
    <message>
        <source>Envelope PRC 10</source>
        <translation>ཡིག་ཤུབས་PRC 10</translation>
    </message>
    <message>
        <source>Envelope You 4</source>
        <translation>ཡིག་ཤུབས་ཁྱེད་4</translation>
    </message>
</context>
</TS>
