<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>KAlertDialog</name>
    <message>
        <source>Cleanable file not selected!!</source>
        <translation type="vanished">未选择可清理的文件！</translation>
    </message>
    <message>
        <source>sure</source>
        <translation type="vanished">确定</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::CleandetailVeiw</name>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="111"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="116"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="287"/>
        <source>Computer scan in progress...</source>
        <translation>Компьютердик сканерлеу жүрүшүндө...</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="119"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="125"/>
        <source>Cleanup</source>
        <translation>Тазалоо</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="134"/>
        <source>Return</source>
        <translation>Кайтып келүү</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="141"/>
        <source>Finish</source>
        <translation>Бүтүрүү</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="183"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="284"/>
        <source>System cache</source>
        <translation>Системалык кэш</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="191"/>
        <source>Clear package、thumbnails and browser cache</source>
        <translation>Так пакети миниатюралар жана браузер кэш</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="202"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="235"/>
        <source>Details</source>
        <translation>Майда-чүйдөсүнө</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="219"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="286"/>
        <source>Cookies</source>
        <translation>Кукилер</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="227"/>
        <source>Clear internet、games、shopping history, etc.</source>
        <translation>Ачык интернет оюндар соода тарыхы ж.б.</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="258"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="285"/>
        <source>Historical trace</source>
        <translation>Тарыхый изи</translation>
    </message>
    <message>
        <source>Clear browser and system usage traces</source>
        <translation type="vanished">Так браузер жана системаны колдонуу изи</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="346"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="841"/>
        <source>Computer cleanup in progress...</source>
        <translation>Компьютердик тазалоо жүрүшүндө...</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="479"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="481"/>
        <source>Cleanable cache </source>
        <translation>Таза кэш </translation>
    </message>
    <message>
        <source>Cleanable cookie </source>
        <translation type="vanished">可清理cookie</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="485"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="688"/>
        <source> items</source>
        <translation> элементтер</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="545"/>
        <source> historical use traces</source>
        <translation> тарыхый колдонуу изи</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="604"/>
        <source>There&apos;s nothing to clean up.</source>
        <translation>Тазалоо үчүн эч нерсе жок.</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="615"/>
        <source>Complete!</source>
        <translation>Толук!</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="657"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="658"/>
        <source>Cleanable Cache</source>
        <translation>Таза кэш</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="670"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="672"/>
        <source>Cleanable Cookie</source>
        <translation>Таза куки</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="713"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="715"/>
        <source>Clear cache </source>
        <translation>Кэшти тазалоо </translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="688"/>
        <source>Clear cookie </source>
        <translation>Ачык-айкын куки </translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="266"/>
        <source>Clear system usage traces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="485"/>
        <source>Cleanable browser </source>
        <translation>Тазалоочу браузер </translation>
    </message>
    <message>
        <source> items,</source>
        <translation type="vanished">个</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="597"/>
        <source>system cache</source>
        <translation>система кэш</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="598"/>
        <source>cookie record</source>
        <translation>куки рекорду</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="599"/>
        <source>history trace</source>
        <translation>тарых изи</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="610"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="626"/>
        <source> item,</source>
        <translation> элемент,</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="611"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="627"/>
        <source> item</source>
        <translation> элемент</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="754"/>
        <source>Clear </source>
        <translation>Тазалоо </translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="754"/>
        <source> historical traces</source>
        <translation> тарыхый изи</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="854"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="857"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="860"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="987"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="988"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="989"/>
        <source>Cleaning up......</source>
        <translation>Тазалоо ......</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="876"/>
        <source>Clearance completed!</source>
        <translation>Тазалоо аяктады!</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="877"/>
        <source>Computer is very energetic, please keep cleaning habits!</source>
        <translation>Компьютер абдан энергиялуу, сураныч, тазалоо адаттарын сактоо!</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="972"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="973"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="974"/>
        <source>Cleaning up</source>
        <translation>Тазалоо</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="977"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="978"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="979"/>
        <source>Cleaning up..</source>
        <translation>Тазалоо..</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="982"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="983"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="984"/>
        <source>Cleaning up....</source>
        <translation>Тазалоо....</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::ClearMainWidget</name>
    <message>
        <source>Often clean, safe and strong!</source>
        <translation type="vanished">常清理，安全又强劲</translation>
    </message>
    <message>
        <source>Garbage 2.1 has been cleaned up</source>
        <translation type="vanished">已累计清理2.1GB</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="71"/>
        <source>SystemCache</source>
        <translation>SystemCache</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="72"/>
        <source>Clean up packages, thumbnails, and recycle bin.</source>
        <translation>Пакеттерди, миниатюраларды тазалап, бинтти кайра иштетүү.</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="82"/>
        <source>Clean up Internet history,cookies.</source>
        <translation>Интернет тарыхын тазалоо,кукилер.</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="91"/>
        <source>HistoryTrace</source>
        <translation>HistoryTrace</translation>
    </message>
    <message>
        <source>Clean up browser and system usage traces.</source>
        <translation type="vanished">Браузерди жана системаны колдонуу изи тазалоо.</translation>
    </message>
    <message>
        <source>ClearMaster</source>
        <translation type="vanished">一键清理</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="48"/>
        <source>Computer garbage, one key to clean up!</source>
        <translation>Компьютердик таштандылар, тазалоо үчүн бир ачкыч!</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="54"/>
        <source>Clean regularly to keep your computer light and safe</source>
        <translation>Компьютердин жарыгын жана коопсуздугун сактоо үчүн дайыма тазалагыла</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="92"/>
        <source>Clean up system usage traces.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="122"/>
        <source>StartClear</source>
        <translation>StartClear</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <source>Cleanup Package Cache</source>
        <translation>Тазалоо пакети Кэш</translation>
    </message>
    <message>
        <source>Cleanup Software Center Cache</source>
        <translation type="vanished">清理软件商店缓存</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <source>Cleanup Thumbnails Cache</source>
        <translation>Тазалоо Миниатюра Кэш</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="317"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="340"/>
        <source>Cleanup FireFox Cache</source>
        <translation>Тазалоо FireFox Кэш</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="269"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="292"/>
        <source>Cleanup Chromium Cache</source>
        <translation>Тазалоо Хромий кэш</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="365"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="388"/>
        <source>Cleanup Qaxbrowser Cache</source>
        <translation>Тазалоо Каксброузер Кэш</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <source>cleanup trash box</source>
        <translation>тазалоо таштанды кутуча</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="195"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="324"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="347"/>
        <source>Cleanup the Cookies saving in Firefox</source>
        <translation>Отко үнөмдөлгөн кукилер тазалоо</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="195"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="276"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="299"/>
        <source>Cleanup the Cookies saving in Chromium</source>
        <translation>Хромда сакталган кукилер тазалоо</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="195"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="372"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="395"/>
        <source>Cleanup the Cookies saving in Qaxbrowser</source>
        <translation>Касброузердеги кукилерди тазалоо</translation>
    </message>
    <message>
        <source>Clean up the Firefox Internet records</source>
        <translation type="vanished">Firefox Интернет жазууларын тазалоо</translation>
    </message>
    <message>
        <source>Clean up the Chromium Internet records</source>
        <translation type="vanished">Хром интернет-жазууларын тазалоо</translation>
    </message>
    <message>
        <source>Clean up the Qaxbrowser Internet records</source>
        <translation type="vanished">Qaxbrowser Интернет жазууларын тазалоо</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="197"/>
        <source>Clean up the recently opened documents records</source>
        <translation>Жакында ачылган документтердин жазууларын тазалоо</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="197"/>
        <source>Delete the command history</source>
        <translation>Команда тарыхын жоготуу</translation>
    </message>
    <message>
        <source>Delete the debug logs</source>
        <translation type="vanished">Дебюттик журналдарды жоготуу</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="223"/>
        <source>Cache Items</source>
        <translation>Кэш заттары</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="236"/>
        <source>Cookies Items</source>
        <translation>Кукилер элементтери</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="249"/>
        <source>Trace Items</source>
        <translation>Изи элементтер</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::KAlertDialog</name>
    <message>
        <source>Cleanable file not selected!!</source>
        <translation type="obsolete">未选择可清理的文件！</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/kalertdialog.cpp" line="29"/>
        <source>Cleanable items not selected!</source>
        <translation>Тазалоочу заттар тандалган жок!</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/kalertdialog.cpp" line="32"/>
        <source>sure</source>
        <translation>албетте</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::SelectListWidget</name>
    <message>
        <location filename="../../plugins/rubbish-clear/selectlistwidget.cpp" line="55"/>
        <location filename="../../plugins/rubbish-clear/selectlistwidget.cpp" line="83"/>
        <location filename="../../plugins/rubbish-clear/selectlistwidget.cpp" line="176"/>
        <source>Clean Items:</source>
        <translation>Таза заттар:</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/selectlistwidget.cpp" line="97"/>
        <source>No items to clean</source>
        <translation>Тазалоо үчүн эч кандай буюмдар</translation>
    </message>
</context>
<context>
    <name>ListWidgetItem</name>
    <message>
        <location filename="../../plugins/tool-box/listwidgetitem.cpp" line="73"/>
        <source>coming soon</source>
        <translation>Жакында</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/listwidgetitem.cpp" line="74"/>
        <source>More tools are coming soon</source>
        <translation>Жакында дагы көп инструменттер келе жатат</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="33"/>
        <location filename="../mainwindow.cpp" line="34"/>
        <source>kylin-os-manager</source>
        <translation>килин-ос-менеджери</translation>
    </message>
    <message>
        <source>Account not logged in</source>
        <translation type="vanished">账户未登录</translation>
    </message>
    <message>
        <source>Log in to protect the device</source>
        <translation type="vanished">登录账号，保护设备</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="221"/>
        <source>PC check</source>
        <translation>ДК текшерүү</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="125"/>
        <location filename="../mainwindow.cpp" line="223"/>
        <source>IssueRepair</source>
        <translation>IssueRepair</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="133"/>
        <location filename="../mainwindow.cpp" line="225"/>
        <source>RubbishClear</source>
        <translation>RubbishClear</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="141"/>
        <location filename="../mainwindow.cpp" line="227"/>
        <source>MultiTerminal</source>
        <translation>Мультитерминалдык</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="149"/>
        <location filename="../mainwindow.cpp" line="229"/>
        <source>ToolBox</source>
        <translation>ИнструментТер</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="161"/>
        <source>This function is missing</source>
        <translation>Бул функция жок</translation>
    </message>
</context>
<context>
    <name>ToolBoxWidget</name>
    <message>
        <source>SoftwareCenter</source>
        <translation type="vanished">软件商店</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/toolboxwidget.cpp" line="26"/>
        <source>My Tool</source>
        <translation>Менин куралым</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/toolboxwidget.cpp" line="31"/>
        <source>All kinds of system tools to help you better use the computer</source>
        <translation>Компьютерди жакшыраак колдонууга жардам берүү үчүн системалык инструменттердин бардык түрлөрү</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/toolboxwidget.cpp" line="167"/>
        <source>FileCrush</source>
        <translation>ФайлКруш</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/toolboxwidget.cpp" line="168"/>
        <source>Shred files that cannot be deleted</source>
        <translation>Жоготууга мүмкүн эмес майдаланган файлдар</translation>
    </message>
</context>
</TS>
