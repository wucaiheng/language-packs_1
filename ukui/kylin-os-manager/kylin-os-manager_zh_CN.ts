<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>KAlertDialog</name>
    <message>
        <source>Cleanable file not selected!!</source>
        <translation type="vanished">未选择可清理的文件！</translation>
    </message>
    <message>
        <source>sure</source>
        <translation type="vanished">确定</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::CleandetailVeiw</name>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="111"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="116"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="287"/>
        <source>Computer scan in progress...</source>
        <translation>正在扫描中...</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="119"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="125"/>
        <source>Cleanup</source>
        <translation>一键清理</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="134"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="141"/>
        <source>Finish</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="183"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="284"/>
        <source>System cache</source>
        <translation>系统缓存</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="191"/>
        <source>Clear package、thumbnails and browser cache</source>
        <translation>清理包、缩略图和浏览器缓存等</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="202"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="235"/>
        <source>Details</source>
        <translation>详情</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="219"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="286"/>
        <source>Cookies</source>
        <translation>Cookies</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="227"/>
        <source>Clear internet、games、shopping history, etc.</source>
        <translation>清理上网、游戏、购物等历史</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="258"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="285"/>
        <source>Historical trace</source>
        <translation>历史痕迹</translation>
    </message>
    <message>
        <source>Clear browser and system usage traces</source>
        <translation type="vanished">清理浏览器和系统使用痕迹</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="346"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="841"/>
        <source>Computer cleanup in progress...</source>
        <translation>正在清理中...</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="479"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="481"/>
        <source>Cleanable cache </source>
        <translation>可清理缓存</translation>
    </message>
    <message>
        <source>Cleanable cookie </source>
        <translation type="vanished">可清理cookie</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="485"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="688"/>
        <source> items</source>
        <translation>个</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="545"/>
        <source> historical use traces</source>
        <translation>历史使用痕迹</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="604"/>
        <source>There&apos;s nothing to clean up.</source>
        <translation>无可清理内容</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="615"/>
        <source>Complete!</source>
        <translation>扫描完成！</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="657"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="658"/>
        <source>Cleanable Cache</source>
        <translation>可清理缓存</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="670"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="672"/>
        <source>Cleanable Cookie</source>
        <translation>可清理Cookie</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="713"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="715"/>
        <source>Clear cache </source>
        <translation>清理缓存</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="688"/>
        <source>Clear cookie </source>
        <translation>清理cookie</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="266"/>
        <source>Clear system usage traces</source>
        <translation>清理系统使用痕迹</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="485"/>
        <source>Cleanable browser </source>
        <translation>可清理浏览器</translation>
    </message>
    <message>
        <source> items,</source>
        <translation type="vanished">个</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="597"/>
        <source>system cache</source>
        <translation>系统缓存</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="598"/>
        <source>cookie record</source>
        <translation>Cookie记录</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="599"/>
        <source>history trace</source>
        <translation>历史痕迹</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="610"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="626"/>
        <source> item,</source>
        <translation>项,</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="611"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="627"/>
        <source> item</source>
        <translation>条</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="754"/>
        <source>Clear </source>
        <translation>清理</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="754"/>
        <source> historical traces</source>
        <translation>历史痕迹</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="854"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="857"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="860"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="987"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="988"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="989"/>
        <source>Cleaning up......</source>
        <translation>清理中...</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="876"/>
        <source>Clearance completed!</source>
        <translation>清理完成!</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="877"/>
        <source>Computer is very energetic, please keep cleaning habits!</source>
        <translation>电脑越来越干净啦，请保持常清理习惯</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="972"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="973"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="974"/>
        <source>Cleaning up</source>
        <translation>清理</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="977"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="978"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="979"/>
        <source>Cleaning up..</source>
        <translation>清理中...</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="982"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="983"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="984"/>
        <source>Cleaning up....</source>
        <translation>清理中...</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::ClearMainWidget</name>
    <message>
        <source>Often clean, safe and strong!</source>
        <translation type="vanished">常清理，安全又强劲</translation>
    </message>
    <message>
        <source>Garbage 2.1 has been cleaned up</source>
        <translation type="vanished">已累计清理2.1GB</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="71"/>
        <source>SystemCache</source>
        <translation>系统缓存</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="72"/>
        <source>Clean up packages, thumbnails, and recycle bin.</source>
        <translation>清理包，缩略图及回收站等。</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="82"/>
        <source>Clean up Internet history,cookies.</source>
        <translation>清理上网历史，cookies等。</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="91"/>
        <source>HistoryTrace</source>
        <translation>历史痕迹</translation>
    </message>
    <message>
        <source>Clean up browser and system usage traces.</source>
        <translation type="vanished">清理浏览器和系统使用痕迹</translation>
    </message>
    <message>
        <source>ClearMaster</source>
        <translation type="vanished">一键清理</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="48"/>
        <source>Computer garbage, one key to clean up!</source>
        <translation>电脑垃圾，一键清理</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="54"/>
        <source>Clean regularly to keep your computer light and safe</source>
        <translation>经常清理，让您的电脑又轻快又安全</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="92"/>
        <source>Clean up system usage traces.</source>
        <translation>清理系统使用痕迹</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="122"/>
        <source>StartClear</source>
        <translation>开始扫描</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <source>Cleanup Package Cache</source>
        <translation>清理安装包缓存</translation>
    </message>
    <message>
        <source>Cleanup Software Center Cache</source>
        <translation type="vanished">清理软件商店缓存</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <source>Cleanup Thumbnails Cache</source>
        <translation>清理缩略图</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="317"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="340"/>
        <source>Cleanup FireFox Cache</source>
        <translation>清理FireFox浏览器</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="269"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="292"/>
        <source>Cleanup Chromium Cache</source>
        <translation>清理Chromium浏览器</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="365"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="388"/>
        <source>Cleanup Qaxbrowser Cache</source>
        <translation>清理奇安信浏览器</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <source>cleanup trash box</source>
        <translation>清理回收站</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="195"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="324"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="347"/>
        <source>Cleanup the Cookies saving in Firefox</source>
        <translation>清理FireFox浏览器 Cookies</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="195"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="276"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="299"/>
        <source>Cleanup the Cookies saving in Chromium</source>
        <translation>清理Chromium浏览器Cookies</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="195"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="372"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="395"/>
        <source>Cleanup the Cookies saving in Qaxbrowser</source>
        <translation>清理奇安信浏览器Cookies</translation>
    </message>
    <message>
        <source>Clean up the Firefox Internet records</source>
        <translation type="vanished">清理Firefox浏览器上网记录</translation>
    </message>
    <message>
        <source>Clean up the Chromium Internet records</source>
        <translation type="vanished">清理Chromium浏览器上网记录</translation>
    </message>
    <message>
        <source>Clean up the Qaxbrowser Internet records</source>
        <translation type="vanished">清理奇安信浏览器上网记录</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="197"/>
        <source>Clean up the recently opened documents records</source>
        <translation>清理最近打开的文档记录</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="197"/>
        <source>Delete the command history</source>
        <translation>清理命令行历史记录</translation>
    </message>
    <message>
        <source>Delete the debug logs</source>
        <translation type="vanished">清理调试日志</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="223"/>
        <source>Cache Items</source>
        <translation>缓存项目</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="236"/>
        <source>Cookies Items</source>
        <translation>Cookies项目</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="249"/>
        <source>Trace Items</source>
        <translation>使用记录</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::KAlertDialog</name>
    <message>
        <source>Cleanable file not selected!!</source>
        <translation type="obsolete">未选择可清理的文件！</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/kalertdialog.cpp" line="29"/>
        <source>Cleanable items not selected!</source>
        <translation>未选择可清理的项目！</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/kalertdialog.cpp" line="32"/>
        <source>sure</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::SelectListWidget</name>
    <message>
        <location filename="../../plugins/rubbish-clear/selectlistwidget.cpp" line="55"/>
        <location filename="../../plugins/rubbish-clear/selectlistwidget.cpp" line="83"/>
        <location filename="../../plugins/rubbish-clear/selectlistwidget.cpp" line="176"/>
        <source>Clean Items:</source>
        <translation>清理项目：</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/selectlistwidget.cpp" line="97"/>
        <source>No items to clean</source>
        <translation>没有清理项</translation>
    </message>
</context>
<context>
    <name>ListWidgetItem</name>
    <message>
        <location filename="../../plugins/tool-box/listwidgetitem.cpp" line="73"/>
        <source>coming soon</source>
        <translation>敬请期待</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/listwidgetitem.cpp" line="74"/>
        <source>More tools are coming soon</source>
        <translation>更多系统小工具即将上线</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="33"/>
        <location filename="../mainwindow.cpp" line="34"/>
        <source>kylin-os-manager</source>
        <translation>麒麟管家</translation>
    </message>
    <message>
        <source>Account not logged in</source>
        <translation type="vanished">账户未登录</translation>
    </message>
    <message>
        <source>Log in to protect the device</source>
        <translation type="vanished">登录账号，保护设备</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="221"/>
        <source>PC check</source>
        <translation>电脑体检</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="125"/>
        <location filename="../mainwindow.cpp" line="223"/>
        <source>IssueRepair</source>
        <translation>故障修复</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="133"/>
        <location filename="../mainwindow.cpp" line="225"/>
        <source>RubbishClear</source>
        <translation>垃圾清理</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="141"/>
        <location filename="../mainwindow.cpp" line="227"/>
        <source>MultiTerminal</source>
        <translation>多端协同</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="149"/>
        <location filename="../mainwindow.cpp" line="229"/>
        <source>ToolBox</source>
        <translation>工具箱</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="161"/>
        <source>This function is missing</source>
        <translation>功能暂未开放</translation>
    </message>
</context>
<context>
    <name>ToolBoxWidget</name>
    <message>
        <source>SoftwareCenter</source>
        <translation type="vanished">软件商店</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/toolboxwidget.cpp" line="26"/>
        <source>My Tool</source>
        <translation>我的工具</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/toolboxwidget.cpp" line="31"/>
        <source>All kinds of system tools to help you better use the computer</source>
        <translation>各类系统小工具，帮您更好使用电脑</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/toolboxwidget.cpp" line="167"/>
        <source>FileCrush</source>
        <translation>文件粉碎</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/toolboxwidget.cpp" line="168"/>
        <source>Shred files that cannot be deleted</source>
        <translation>彻底粉碎无法删除的文件</translation>
    </message>
</context>
</TS>
