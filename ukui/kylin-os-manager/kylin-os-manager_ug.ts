<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>KAlertDialog</name>
    <message>
        <source>Cleanable file not selected!!</source>
        <translation type="vanished">未选择可清理的文件！</translation>
    </message>
    <message>
        <source>sure</source>
        <translation type="vanished">确定</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::CleandetailVeiw</name>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="111"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="116"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="287"/>
        <source>Computer scan in progress...</source>
        <translation>كومپيۇتېرنى سىكاننېرلاش داۋامى...</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="119"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="125"/>
        <source>Cleanup</source>
        <translation>تازىلاش</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="134"/>
        <source>Return</source>
        <translation>قايتىش</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="141"/>
        <source>Finish</source>
        <translation>تاماملاش</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="183"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="284"/>
        <source>System cache</source>
        <translation>سىستېما cache</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="191"/>
        <source>Clear package、thumbnails and browser cache</source>
        <translation>بوغچا،كىچىك نادىر ۋە تور كۆرگۈچنىڭ كاچاتلىرىنى تازىلاش</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="202"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="235"/>
        <source>Details</source>
        <translation>تەپسىلاتى</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="219"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="286"/>
        <source>Cookies</source>
        <translation>پېچىنە-پىرەنىكلەر</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="227"/>
        <source>Clear internet、games、shopping history, etc.</source>
        <translation>تور ئويۇنى،مال سېتىۋېلىش تارىخى قاتارلىقلارنى ئېنىقلاش</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="258"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="285"/>
        <source>Historical trace</source>
        <translation>تارىخىي ئىزلار</translation>
    </message>
    <message>
        <source>Clear browser and system usage traces</source>
        <translation type="vanished">تور كۆرگۈچ ۋە سىستېما ئىشلىتىش ئىزلىرىنى تازىلاش</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="346"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="841"/>
        <source>Computer cleanup in progress...</source>
        <translation>كومپىيۇتېر تازىلاش خىزمىتى داۋاملىشىۋاتىدۇ...</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="479"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="481"/>
        <source>Cleanable cache </source>
        <translation>تازىلىغىلى بولىدىغان كاچكا </translation>
    </message>
    <message>
        <source>Cleanable cookie </source>
        <translation type="vanished">可清理cookie</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="485"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="688"/>
        <source> items</source>
        <translation> تۈر</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="545"/>
        <source> historical use traces</source>
        <translation> تارىخىي ئىشلىتىلىش ئىزلىرى</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="604"/>
        <source>There&apos;s nothing to clean up.</source>
        <translation>تازىلىغۇدەك ھېچنېمە يوق.</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="615"/>
        <source>Complete!</source>
        <translation>تامام!</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="657"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="658"/>
        <source>Cleanable Cache</source>
        <translation>تازىلاشقا بولىدىغان كاچكا</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="670"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="672"/>
        <source>Cleanable Cookie</source>
        <translation>پاكىزە ئاشپەزلەر</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="713"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="715"/>
        <source>Clear cache </source>
        <translation>Cache نى تازىلاش </translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="688"/>
        <source>Clear cookie </source>
        <translation>پىچىنە-پىرەنىكنى تازىلاش </translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="266"/>
        <source>Clear system usage traces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="485"/>
        <source>Cleanable browser </source>
        <translation>پاكىزە تور كۆرگۈچ </translation>
    </message>
    <message>
        <source> items,</source>
        <translation type="vanished">个</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="597"/>
        <source>system cache</source>
        <translation>سىستېما cache</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="598"/>
        <source>cookie record</source>
        <translation>cookie پىلاستىنكىسى</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="599"/>
        <source>history trace</source>
        <translation>تارىخ ئىزنالىرى</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="610"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="626"/>
        <source> item,</source>
        <translation> ئەزا ،</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="611"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="627"/>
        <source> item</source>
        <translation> تۈر</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="754"/>
        <source>Clear </source>
        <translation>تازىلاش </translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="754"/>
        <source> historical traces</source>
        <translation> تارىخىي ئىزلار</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="854"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="857"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="860"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="987"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="988"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="989"/>
        <source>Cleaning up......</source>
        <translation>تازىلى...</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="876"/>
        <source>Clearance completed!</source>
        <translation>تازىلاش تاماملاندى!</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="877"/>
        <source>Computer is very energetic, please keep cleaning habits!</source>
        <translation>كومپىيۇتېرنىڭ تېتىكلىكى ئىنتايىن كۈچلۈك، تازىلىق ئادىتىنى داۋاملاشتۇرۇڭ!</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="972"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="973"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="974"/>
        <source>Cleaning up</source>
        <translation>تازىلاش</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="977"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="978"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="979"/>
        <source>Cleaning up..</source>
        <translation>تازىلاش..</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="982"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="983"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="984"/>
        <source>Cleaning up....</source>
        <translation>تازلاش....</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::ClearMainWidget</name>
    <message>
        <source>Often clean, safe and strong!</source>
        <translation type="vanished">常清理，安全又强劲</translation>
    </message>
    <message>
        <source>Garbage 2.1 has been cleaned up</source>
        <translation type="vanished">已累计清理2.1GB</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="71"/>
        <source>SystemCache</source>
        <translation>SystemCache</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="72"/>
        <source>Clean up packages, thumbnails, and recycle bin.</source>
        <translation>بوغچا، نادىرلارنى تازىلاپ، قايتا يىغىۋىتىش ساندۇقى.</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="82"/>
        <source>Clean up Internet history,cookies.</source>
        <translation>تور تارىخىنى تازىلاپ، پېچىنە-پىرەنىكلەرنى...</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="91"/>
        <source>HistoryTrace</source>
        <translation>HistoryTrace</translation>
    </message>
    <message>
        <source>Clean up browser and system usage traces.</source>
        <translation type="vanished">تور كۆرگۈچ ۋە سىستېما ئىشلىتىش ئىزلىرىنى تازىلايدۇ.</translation>
    </message>
    <message>
        <source>ClearMaster</source>
        <translation type="vanished">一键清理</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="48"/>
        <source>Computer garbage, one key to clean up!</source>
        <translation>كومپىيۇتېر ئەخلەتلىرى، بىر ئاچقۇچنى تازىلاڭلار!</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="54"/>
        <source>Clean regularly to keep your computer light and safe</source>
        <translation>كومپيۇتېرنىڭ يورۇقلۇقى ۋە بىخەتەرلىكىنى ساقلاش ئۈچۈن قەرەللىك تازىلاڭ</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="92"/>
        <source>Clean up system usage traces.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="122"/>
        <source>StartClear</source>
        <translation>StartClear</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <source>Cleanup Package Cache</source>
        <translation>قاچىلاش بوغچىسىنى تازىلاش</translation>
    </message>
    <message>
        <source>Cleanup Software Center Cache</source>
        <translation type="vanished">清理软件商店缓存</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <source>Cleanup Thumbnails Cache</source>
        <translation>كىچىك نادىرلار Cache نى تازىلاش</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="317"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="340"/>
        <source>Cleanup FireFox Cache</source>
        <translation>FireFox Cache نى تازىلاش</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="269"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="292"/>
        <source>Cleanup Chromium Cache</source>
        <translation>Chromium Cache نى تازىلاش</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="365"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="388"/>
        <source>Cleanup Qaxbrowser Cache</source>
        <translation>Qaxbrowser Cache نى تازىلاش</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <source>cleanup trash box</source>
        <translation>ئەخلەت ساندۇقىنى تازىلاش</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="195"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="324"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="347"/>
        <source>Cleanup the Cookies saving in Firefox</source>
        <translation>Firefox دا ساقلىغان Cookie لارنى تازىلاش</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="195"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="276"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="299"/>
        <source>Cleanup the Cookies saving in Chromium</source>
        <translation>Chromium دا ساقلىغان Cookies نى تازىلاش</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="195"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="372"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="395"/>
        <source>Cleanup the Cookies saving in Qaxbrowser</source>
        <translation>Qaxbrowser دا ساقلىغان Cookies لارنى تازىلاش</translation>
    </message>
    <message>
        <source>Clean up the Firefox Internet records</source>
        <translation type="vanished">Firefox ئىنتېرنېت خاتىرىسىنى تازىلاش</translation>
    </message>
    <message>
        <source>Clean up the Chromium Internet records</source>
        <translation type="vanished">Chromium ئىنتېرنېت خاتىرىسىنى تازىلاش</translation>
    </message>
    <message>
        <source>Clean up the Qaxbrowser Internet records</source>
        <translation type="vanished">Qaxbrowser ئىنتېرنېت خاتىرىسىنى تازىلاش</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="197"/>
        <source>Clean up the recently opened documents records</source>
        <translation>يېقىندا ئېچىلغان ھۆججەت خاتىرىسىنى تازىلاش</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="197"/>
        <source>Delete the command history</source>
        <translation>بۇيرۇق تارىخىنى ئۆچۈرۈش</translation>
    </message>
    <message>
        <source>Delete the debug logs</source>
        <translation type="vanished">دەبدەبىلىك خاتىرىلەرنى ئۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="223"/>
        <source>Cache Items</source>
        <translation>Cache تۈرلىرى</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="236"/>
        <source>Cookies Items</source>
        <translation>Cookies تۈرلىرى</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="249"/>
        <source>Trace Items</source>
        <translation>ئىز تۈرلىرى</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::KAlertDialog</name>
    <message>
        <source>Cleanable file not selected!!</source>
        <translation type="obsolete">未选择可清理的文件！</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/kalertdialog.cpp" line="29"/>
        <source>Cleanable items not selected!</source>
        <translation>تازىلىغىلى بولىدىغان بۇيۇملار تاللاپ چىقىلمىغان!</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/kalertdialog.cpp" line="32"/>
        <source>sure</source>
        <translation>ئەلۋەتتە</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::SelectListWidget</name>
    <message>
        <location filename="../../plugins/rubbish-clear/selectlistwidget.cpp" line="55"/>
        <location filename="../../plugins/rubbish-clear/selectlistwidget.cpp" line="83"/>
        <location filename="../../plugins/rubbish-clear/selectlistwidget.cpp" line="176"/>
        <source>Clean Items:</source>
        <translation>پاكىز بۇيۇملار:</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/selectlistwidget.cpp" line="97"/>
        <source>No items to clean</source>
        <translation>تازىلىماقچى بولغان بۇيۇملار يوق</translation>
    </message>
</context>
<context>
    <name>ListWidgetItem</name>
    <message>
        <location filename="../../plugins/tool-box/listwidgetitem.cpp" line="73"/>
        <source>coming soon</source>
        <translation>ئۇزاققا قالماي كىلىدۇ</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/listwidgetitem.cpp" line="74"/>
        <source>More tools are coming soon</source>
        <translation>تېخىمۇ كۆپ قوراللار تېزلا كېلىدۇ</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="33"/>
        <location filename="../mainwindow.cpp" line="34"/>
        <source>kylin-os-manager</source>
        <translation>kylin-os-باشقۇرغۇچى</translation>
    </message>
    <message>
        <source>Account not logged in</source>
        <translation type="vanished">账户未登录</translation>
    </message>
    <message>
        <source>Log in to protect the device</source>
        <translation type="vanished">登录账号，保护设备</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="221"/>
        <source>PC check</source>
        <translation>PC تەكشۈرۈش</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="125"/>
        <location filename="../mainwindow.cpp" line="223"/>
        <source>IssueRepair</source>
        <translation>IssueRepair</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="133"/>
        <location filename="../mainwindow.cpp" line="225"/>
        <source>RubbishClear</source>
        <translation>ئەخلەتClear</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="141"/>
        <location filename="../mainwindow.cpp" line="227"/>
        <source>MultiTerminal</source>
        <translation>MultiTerminal</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="149"/>
        <location filename="../mainwindow.cpp" line="229"/>
        <source>ToolBox</source>
        <translation>قورال ساندۇقى</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="161"/>
        <source>This function is missing</source>
        <translation>بۇ فۇنكسىيە كەم</translation>
    </message>
</context>
<context>
    <name>ToolBoxWidget</name>
    <message>
        <source>SoftwareCenter</source>
        <translation type="vanished">软件商店</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/toolboxwidget.cpp" line="26"/>
        <source>My Tool</source>
        <translation>قورالىم</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/toolboxwidget.cpp" line="31"/>
        <source>All kinds of system tools to help you better use the computer</source>
        <translation>ھەر خىل سىستېما قوراللىرى كومپىيۇتېرنى تېخىمۇ ياخشى ئىشلىتىشىڭىزگە ياردەم بېرىدۇ</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/toolboxwidget.cpp" line="167"/>
        <source>FileCrush</source>
        <translation>FileCrush</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/toolboxwidget.cpp" line="168"/>
        <source>Shred files that cannot be deleted</source>
        <translation>ئۆچۈرگىلى بولمايدىغان ھۆججەتلەرنى shred</translation>
    </message>
</context>
</TS>
