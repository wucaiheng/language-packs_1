<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>KAlertDialog</name>
    <message>
        <source>Cleanable file not selected!!</source>
        <translation type="vanished">未选择可清理的文件！</translation>
    </message>
    <message>
        <source>sure</source>
        <translation type="vanished">确定</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::CleandetailVeiw</name>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="111"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="116"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="287"/>
        <source>Computer scan in progress...</source>
        <translation>ཞིབ་བཤེར་བྱེད་བཞིན་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="119"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="125"/>
        <source>Cleanup</source>
        <translation>གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="134"/>
        <source>Return</source>
        <translation>ཕྱིར་སློག་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="141"/>
        <source>Finish</source>
        <translation>མཇུག་སྒྲིལ།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="183"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="284"/>
        <source>System cache</source>
        <translation>མ་ལག་གི་ལྷོད་གསོག</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="191"/>
        <source>Clear package、thumbnails and browser cache</source>
        <translation>ཁ་གསལ་བའི་ཐུམ་སྒྲིལ་དང་། མཐེབ་བཀྱག་དང་བལྟ་ཀློག་ཆས་ཀྱི་ལྷོད་གསོག</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="202"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="235"/>
        <source>Details</source>
        <translation>ཞིབ་ཕྲའི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="219"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="286"/>
        <source>Cookies</source>
        <translation>Cookies</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="227"/>
        <source>Clear internet、games、shopping history, etc.</source>
        <translation>དྲ་འཇུག་།་རོལ་རྩེད་།དངོས་ཟོག་ཉོ་སྒྲུབ་སོགས་ཀྱི་ལོ་རྒྱུས་གཙང་བཤེར་བྱས་།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="258"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="285"/>
        <source>Historical trace</source>
        <translation>ལོ་རྒྱུས་ཀྱི་རྗེས་ཤུལ།</translation>
    </message>
    <message>
        <source>Clear browser and system usage traces</source>
        <translation type="vanished">清理浏览器和系统使用痕迹</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="346"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="841"/>
        <source>Computer cleanup in progress...</source>
        <translation>གཙང་བཤེར་བྱེད་བཞིན་པའི་སྒང་རེད།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="479"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="481"/>
        <source>Cleanable cache </source>
        <translation>གསོག་འཇོག་གཙང་སེལ་བྱས་ཆོག </translation>
    </message>
    <message>
        <source>Cleanable cookie </source>
        <translation type="vanished">可清理cookie</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="485"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="688"/>
        <source> items</source>
        <translation> གཅིག་པུ།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="545"/>
        <source> historical use traces</source>
        <translation> ལོ་རྒྱུས་ཀྱི་བེད་སྤྱོད་རྗེས་ཤུལ།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="604"/>
        <source>There&apos;s nothing to clean up.</source>
        <translation>གཙང་སྦྲ་བྱེད་དགོས་དོན་གང་ཡང་མེད།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="615"/>
        <source>Complete!</source>
        <translation>བཤེར་འབེབས་བྱས་ཚར།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="657"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="658"/>
        <source>Cleanable Cache</source>
        <translation>གསོག་འཇོག་གཙང་སེལ་བྱས་ཆོག</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="670"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="672"/>
        <source>Cleanable Cookie</source>
        <translation>གཙང་སེལ་བྱེད་པCookie</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="713"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="715"/>
        <source>Clear cache </source>
        <translation>གཙང་སེལ་ལྷོད་གསོག </translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="688"/>
        <source>Clear cookie </source>
        <translation>གཙང་སེལ་cookie </translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="266"/>
        <source>Clear system usage traces</source>
        <translation>གཙང་བཤེར་མ་ལག་བེད་སྤྱོད་རྗེས་ཤུལ།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="485"/>
        <source>Cleanable browser </source>
        <translation>བཤར་ཆས་གཙང་བཤེར་བྱེད་ཆོག </translation>
    </message>
    <message>
        <source> items,</source>
        <translation type="vanished">个</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="597"/>
        <source>system cache</source>
        <translation>མ་ལག་གི་ལྷོད་གསོག</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="598"/>
        <source>cookie record</source>
        <translation>Cookieཟིན་ཐོ།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="599"/>
        <source>history trace</source>
        <translation>ལོ་རྒྱུས་ཀྱི་རྗེས་ཤུལ།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="610"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="626"/>
        <source> item,</source>
        <translation> རྣམ་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="611"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="627"/>
        <source> item</source>
        <translation> དོན་ཚན།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="754"/>
        <source>Clear </source>
        <translation>གཙང་སེལ། </translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="754"/>
        <source> historical traces</source>
        <translation> ལོ་རྒྱུས་ཀྱི་རྗེས་ཤུལ།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="854"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="857"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="860"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="987"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="988"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="989"/>
        <source>Cleaning up......</source>
        <translation>གཙང་སེལ་བྱེད་པ......</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="876"/>
        <source>Clearance completed!</source>
        <translation>གཙང་བཤེར་བྱས་ཚར་སོང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="877"/>
        <source>Computer is very energetic, please keep cleaning habits!</source>
        <translation>རྩིས་འཁོར་ལ་གསོན་ཤུགས་ཧ་ཅང་ཆེན་པོ་ཡོད་པས་གཙང་སྦྲའི་གོམས་གཤིས་རྒྱུན་འཁྱོངས་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="972"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="973"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="974"/>
        <source>Cleaning up</source>
        <translation>གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="977"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="978"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="979"/>
        <source>Cleaning up..</source>
        <translation>གཙང་སེལ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="982"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="983"/>
        <location filename="../../plugins/rubbish-clear/cleandetailveiw.cpp" line="984"/>
        <source>Cleaning up....</source>
        <translation>གཙང་སེལ་བྱེད་དགོས།</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::ClearMainWidget</name>
    <message>
        <source>Often clean, safe and strong!</source>
        <translation type="vanished">常清理，安全又强劲</translation>
    </message>
    <message>
        <source>Garbage 2.1 has been cleaned up</source>
        <translation type="vanished">已累计清理2.1GB</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="71"/>
        <source>SystemCache</source>
        <translation>མ་ལག་གི་ལྷོད་གསོག</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="72"/>
        <source>Clean up packages, thumbnails, and recycle bin.</source>
        <translation>གཙང་བཤེར་ཁུག་མ།་བསྡུས་རིས་དང་ཕྱིར་བསྡུ་ས་ཚིགས་སོགས།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="82"/>
        <source>Clean up Internet history,cookies.</source>
        <translation>དྲ་རྒྱའི་ལོ་རྒྱུས་དང་cookiesསོགས་གཙང་སེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="91"/>
        <source>HistoryTrace</source>
        <translation>ལོ་རྒྱུས་ཀྱི་བཤུལ་ལམ།</translation>
    </message>
    <message>
        <source>Clean up browser and system usage traces.</source>
        <translation type="vanished">清理浏览器和系统使用痕迹</translation>
    </message>
    <message>
        <source>ClearMaster</source>
        <translation type="vanished">一键清理</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="48"/>
        <source>Computer garbage, one key to clean up!</source>
        <translation>རྩིས་འཁོར་གྱི་གད་སྙིགས་དང་ལྡེ་མིག་གཅིག་གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="54"/>
        <source>Clean regularly to keep your computer light and safe</source>
        <translation>རྒྱུན་དུ་གཙང་སྦྲ་བྱས་ནས་ཁྱེད་ཀྱི་གློག་ཀླད་ཡང་ཞིང་བདེ་འཇགས་ཡིན་པར་བྱས།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="92"/>
        <source>Clean up system usage traces.</source>
        <translation>མ་ལག་གི་བཀོལ་སྤྱོད་རྗེས་ཤུལ་གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="122"/>
        <source>StartClear</source>
        <translation>འགོ་རྩོམ་པའི་བཤར་འབེབས།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <source>Cleanup Package Cache</source>
        <translation>གཙང་བཤེར་བྱེད་པའི་ཁུག་མའི་ནང་དུ་གཙང་སེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Cleanup Software Center Cache</source>
        <translation type="vanished">清理软件商店缓存</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <source>Cleanup Thumbnails Cache</source>
        <translation>གཙང་བཤེར་བྱས་པའི་བསྡུས་རིས།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="317"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="340"/>
        <source>Cleanup FireFox Cache</source>
        <translation>གཙང་བཤེར་བྱས་པའི་FireFox Cacheལྟ་ཀློག་ཆས།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="269"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="292"/>
        <source>Cleanup Chromium Cache</source>
        <translation>Chromiumལྟ་ཀློག་ཆས་གཙང་སེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="365"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="388"/>
        <source>Cleanup Qaxbrowser Cache</source>
        <translation>ཆི་ཨན་ཞིན་བཤར་ཆས་གཙང་བཤེར་བྱས།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="193"/>
        <source>cleanup trash box</source>
        <translation>སྙིགས་སྣོད་གཙང་སེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="195"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="324"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="347"/>
        <source>Cleanup the Cookies saving in Firefox</source>
        <translation>FireFoxབཤར་ཆས Cookiesགཙང་སེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="195"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="276"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="299"/>
        <source>Cleanup the Cookies saving in Chromium</source>
        <translation>FireFoxབཤར་ཆས Cookiesགཙང་སེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="195"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="372"/>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="395"/>
        <source>Cleanup the Cookies saving in Qaxbrowser</source>
        <translation>ཆི་ཨན་ཞིན་བཤར་ཆས་གཙང་བཤེར་བྱས།Cookies</translation>
    </message>
    <message>
        <source>Clean up the Firefox Internet records</source>
        <translation type="vanished">清理Firefox浏览器上网记录</translation>
    </message>
    <message>
        <source>Clean up the Chromium Internet records</source>
        <translation type="vanished">清理Chromium浏览器上网记录</translation>
    </message>
    <message>
        <source>Clean up the Qaxbrowser Internet records</source>
        <translation type="vanished">清理奇安信浏览器上网记录</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="197"/>
        <source>Clean up the recently opened documents records</source>
        <translation>ཉེ་ཆར་ཁ་ཕྱེས་པའི་ཡིག་ཆའི་ཟིན་ཐོ་གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="197"/>
        <source>Delete the command history</source>
        <translation>བཀའ་རྒྱའི་ལོ་རྒྱུས་ཟིན་ཐོ་གཙང་བཤེར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Delete the debug logs</source>
        <translation type="vanished">清理调试日志</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="223"/>
        <source>Cache Items</source>
        <translation>གསོག་འཇོག་རྣམ་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="236"/>
        <source>Cookies Items</source>
        <translation>Cookieརྣམ་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/clearmainwidget.cpp" line="249"/>
        <source>Trace Items</source>
        <translation>ཟིན་ཐོ་བེད་སྤྱོད།</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::KAlertDialog</name>
    <message>
        <source>Cleanable file not selected!!</source>
        <translation type="obsolete">未选择可清理的文件！</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/kalertdialog.cpp" line="29"/>
        <source>Cleanable items not selected!</source>
        <translation>གཙང་བཤེར་བྱེད་ཆོག་པའི་རྣམ་གྲངས་བདམས་མེད།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/kalertdialog.cpp" line="32"/>
        <source>sure</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::SelectListWidget</name>
    <message>
        <location filename="../../plugins/rubbish-clear/selectlistwidget.cpp" line="55"/>
        <location filename="../../plugins/rubbish-clear/selectlistwidget.cpp" line="83"/>
        <location filename="../../plugins/rubbish-clear/selectlistwidget.cpp" line="176"/>
        <source>Clean Items:</source>
        <translation>གཙང་སེལ་རྣམ་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/rubbish-clear/selectlistwidget.cpp" line="97"/>
        <source>No items to clean</source>
        <translation>གཙང་སེལ་བྱེད་དགོས་པའི་དངོས་རྫས་མེད།</translation>
    </message>
</context>
<context>
    <name>ListWidgetItem</name>
    <message>
        <location filename="../../plugins/tool-box/listwidgetitem.cpp" line="73"/>
        <source>coming soon</source>
        <translation>རེ་སྒུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/listwidgetitem.cpp" line="74"/>
        <source>More tools are coming soon</source>
        <translation>མ་ལག་གི་ལག་ཆ་ཆུང་ཆུང་མང་པོ་ཞིག་མི་འགྱངས་བར་དྲ་ལམ་དུ་ཞུགས་རྒྱུ་རེད།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="33"/>
        <location filename="../mainwindow.cpp" line="34"/>
        <source>kylin-os-manager</source>
        <translation>ཅིན་ལིན་གྱི་སྤྱི་གཉེར་བ་ཡིན།</translation>
    </message>
    <message>
        <source>Account not logged in</source>
        <translation type="vanished">账户未登录</translation>
    </message>
    <message>
        <source>Log in to protect the device</source>
        <translation type="vanished">登录账号，保护设备</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="221"/>
        <source>PC check</source>
        <translation>གློག་ཀླད་ཞིབ་བཤེར།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="125"/>
        <location filename="../mainwindow.cpp" line="223"/>
        <source>IssueRepair</source>
        <translation>ཆག་སྐྱོན་ཞིག་གསོ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="133"/>
        <location filename="../mainwindow.cpp" line="225"/>
        <source>RubbishClear</source>
        <translation>གད་སྙིགས་གཙང་སེལ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="141"/>
        <location filename="../mainwindow.cpp" line="227"/>
        <source>MultiTerminal</source>
        <translation>སྣེ་མང་མཉམ་སྒྲུབ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="149"/>
        <location filename="../mainwindow.cpp" line="229"/>
        <source>ToolBox</source>
        <translation>ཡོ་བྱད་སྒམ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="161"/>
        <source>This function is missing</source>
        <translation>བྱེད་ནུས་གནས་སྐབས་སུ་སྒོ་འབྱེད་བྱས་མེད།</translation>
    </message>
</context>
<context>
    <name>ToolBoxWidget</name>
    <message>
        <source>SoftwareCenter</source>
        <translation type="vanished">软件商店</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/toolboxwidget.cpp" line="26"/>
        <source>My Tool</source>
        <translation>ངའི་ལག་ཆ།</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/toolboxwidget.cpp" line="31"/>
        <source>All kinds of system tools to help you better use the computer</source>
        <translation>རིགས་མི་འདྲ་བའི་མ་ལག་གི་ལག་ཆ་ཆུང་ཆུང་།ཁྱེད་ལ་གློག་ཀླད་བཀོལ་སྤྱོད་བྱེད་རོགས་བྱེད།</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/toolboxwidget.cpp" line="167"/>
        <source>FileCrush</source>
        <translation>ཡིག་ཆ་རྡུལ་དུ་བརླག</translation>
    </message>
    <message>
        <location filename="../../plugins/tool-box/toolboxwidget.cpp" line="168"/>
        <source>Shred files that cannot be deleted</source>
        <translation>བསུབ་ཐབས་མེད་པའི་ཡིག་ཆ་རྩ་བ་ནས་རྡུལ་དུ་བརླག</translation>
    </message>
    <message>
        <source>StartGuide</source>
        <translation type="vanished">འགོ་ཚུགས་པའི་ཀུའེ་ཏེར་</translation>
    </message>
    <message>
        <source>Faster startup setup</source>
        <translation type="vanished">སྔར་ལས་མགྱོགས་པའི་སྒོ་ནས་བྱ་གཞག་གསར་གཏོད་</translation>
    </message>
</context>
</TS>
