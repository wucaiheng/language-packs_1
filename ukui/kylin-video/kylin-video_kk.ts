<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>AboutDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;kylin video&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;麒麟影音&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;version: 0.0.1&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;版本: 0.0.1&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    Kylin video is a front player with beautiful interface and good interaction. It is developed with Qt5 and uses MPV as the playback engine. Kylin video supports almost all audio and video formats and has powerful decoding ability.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;         麒麟影音是一款界面美观、互动性好的前置播放器。它是用Qt5开发的，使用MPV作为播放引擎。麒麟视频支持几乎所有的音频和视频格式，具有强大的解码能力。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>service and support: support@kylinos.cn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>service and support: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video Player About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans Syloti Nagri&apos;; font-size:13pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt;&quot;&gt;    Video Player is a front player with beautiful interface and good interaction. It is developed with Qt5 and uses MPV as the playback engine. Kylin video supports almost all audio and video formats and has powerful decoding ability.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;         影音是一款界面美观、互动性好的前置播放器。它是用Qt5开发的，使用MPV作为播放引擎。麒麟视频支持几乎所有的音频和视频格式，具有强大的解码能力。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;Video Player&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt;&quot;&gt;    Video Player is a front player with beautiful interface and good interaction. It is developed with Qt5 and uses MPV as the playback engine. Kylin video supports almost all audio and video formats and has powerful decoding ability.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt;&quot;&gt;        影音是一款界面美观、互动性好的前置播放器。它是用Qt5开发的，使用MPV作为播放引擎。麒麟视频支持几乎所有的音频和视频格式，具有强大的解码能力。&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt;&quot;&gt;Video Player is a front player with beautiful interface and good interaction. It is developed with Qt5 and uses MPV as the playback engine. Kylin video supports almost all audio and video formats and has powerful decoding ability.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt;&quot;&gt;影音是一款界面美观、互动性好的前置播放器。它是用Qt5开发的，使用MPV作为播放引擎。麒麟视频支持几乎所有的音频和视频格式，具有强大的解码能力。&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Video Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt;&quot;&gt;Video Player is a front player with beautiful interface and good interaction. It is developed with Qt5 and uses MPV as the playback engine. Video Player supports almost all audio and video formats and has powerful decoding ability.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt;&quot;&gt;影音是一款界面美观、互动性好的前置播放器。它是用Qt5开发的，使用MPV作为播放引擎。影音支持几乎所有的音频和视频格式，具有强大的解码能力。&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Video Player is a front player with beautiful interface and good interaction. It is developed with Qt5 and uses MPV as the playback engine. Video Player supports almost all audio and video formats and has powerful decoding ability.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;影音是一款界面美观、互动性好的前置播放器。它是用Qt5开发的，使用MPV作为播放引擎。影音支持几乎所有的音频和视频格式，具有强大的解码能力。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;version: 3.1.1&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans Mono CJK SC&apos;; font-size:12pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt;&quot;&gt;Video Player is a front player with beautiful interface and good interaction. It is developed with Qt5 and uses MPV as the playback engine. Video Player supports almost all audio and video formats and has powerful decoding ability.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContralBar</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>2.0X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>1.5X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>1.25X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>1.0X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>0.75X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>0.5X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Screen shot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Previous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Full screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exit full screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>--:--:--/--:--:--</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GlobalSetup</name>
    <message>
        <source>Pictures</source>
        <translation type="vanished">图片</translation>
    </message>
    <message>
        <source>no</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>default</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HomePage</name>
    <message>
        <source>open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>open dir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video Player</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KRightClickMenu</name>
    <message>
        <source>Open</source>
        <translation type="vanished">打开</translation>
    </message>
    <message>
        <source>Open &amp;File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;URL...</source>
        <translation type="vanished">&amp;打开URL...</translation>
    </message>
    <message>
        <source>ToTop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>One Loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>List loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Default frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Full frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>forward rotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Along rotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>backward rotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Inverse rotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>horizontal flip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Horizontally flip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>vertical flip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Vertically flip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>AudioTrack</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>AudioChannel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stereo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Left channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Right channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Subtitle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose a file</source>
        <translation type="vanished">选择一个文件</translation>
    </message>
    <message>
        <source>Subtitles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Load subtitle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Subtitle select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No subtitle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Subtitle set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>volume up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>volume down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>backward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>More</source>
        <translation type="vanished">更多</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="vanished">主题</translation>
    </message>
    <message>
        <source>Follow system</source>
        <translation type="vanished">跟随系统</translation>
    </message>
    <message>
        <source>Light theme</source>
        <translation type="vanished">浅色主题</translation>
    </message>
    <message>
        <source>Black theme</source>
        <translation type="vanished">深色主题</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Privacy</source>
        <translation type="vanished">隐私</translation>
    </message>
    <message>
        <source>Clear mark</source>
        <translation type="vanished">清理痕迹</translation>
    </message>
    <message>
        <source>No mark</source>
        <translation type="vanished">无痕播放</translation>
    </message>
    <message>
        <source>Match subtitle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open &amp;Directory...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>open dir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>to top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search subtitle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Play/Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Volume up</source>
        <translation type="vanished">音量+</translation>
    </message>
    <message>
        <source>Volume down</source>
        <translation type="vanished">音量-</translation>
    </message>
    <message>
        <source>Play forward</source>
        <translation type="vanished">快进</translation>
    </message>
    <message>
        <source>Play backward</source>
        <translation type="vanished">快退</translation>
    </message>
    <message>
        <source>Player set</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Media info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>4:3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>16:9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>restore frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video Player Choose a file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sub load</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KylinUI::MessageBox</name>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListLoopMenu</name>
    <message>
        <source>One Loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>List loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sequence</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source>Video Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>open file</source>
        <translation type="vanished">打开文件</translation>
    </message>
    <message>
        <source>open dir</source>
        <translation type="vanished">打开文件夹</translation>
    </message>
    <message>
        <source>Choose a file</source>
        <translation type="vanished">选择一个文件</translation>
    </message>
    <message>
        <source>Multimedia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Playlists</source>
        <translation type="vanished">播放列表</translation>
    </message>
    <message>
        <source>All files</source>
        <translation type="vanished">所有文件</translation>
    </message>
    <message>
        <source>Choose a directory</source>
        <translation type="vanished">选择一个文件夹</translation>
    </message>
    <message>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Video Player </source>
        <translation type="vanished">影音 </translation>
    </message>
    <message>
        <source>Video Player Choose a file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video Player Choose a directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>video player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>/Video</source>
        <translation type="vanished">/视频</translation>
    </message>
</context>
<context>
    <name>MarkListItem</name>
    <message>
        <source>Video Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File not exist!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MediaInfoDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>media info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>media name:</source>
        <translation type="vanished">媒体文件:</translation>
    </message>
    <message>
        <source>file type:</source>
        <translation type="vanished">文件类型:</translation>
    </message>
    <message>
        <source>file size:</source>
        <translation type="vanished">文件大小:</translation>
    </message>
    <message>
        <source>file duration:</source>
        <translation type="vanished">文件时长:</translation>
    </message>
    <message>
        <source>file path:</source>
        <translation type="vanished">文件路径:</translation>
    </message>
    <message>
        <source>ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kylin video</source>
        <translation type="obsolete">影音</translation>
    </message>
    <message>
        <source>title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>duration:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>video player</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MiniModeShade</name>
    <message>
        <source>pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>normal mode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MpvCore</name>
    <message>
        <source>File size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Media length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>brightness : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>subtitle delay : %1s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Horizontal Flip: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Vertical Flip: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Resolution:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Estimated FPS:</source>
        <translation type="vanished">估算 FPS:</translation>
    </message>
    <message>
        <source>Bitrate:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sample Rate:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Channels:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio/video synchronization:</source>
        <translation type="vanished">音视频同步差:</translation>
    </message>
    <message>
        <source>volume : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>speed : %1x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel Mute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Playing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Paused</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>subtitle : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video (x%0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>FPS</source>
        <translation></translation>
    </message>
    <message>
        <source>A/V Sync</source>
        <translation type="vanished">音视频同步</translation>
    </message>
    <message>
        <source>Bitrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%0 kbps</source>
        <translation></translation>
    </message>
    <message>
        <source>Audio (x%0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sample Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chapters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Metadata</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fps:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>restore frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ScreenShot OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ScreenShot Failed</source>
        <translation type="vanished">截图失败，文件夹无写权限</translation>
    </message>
    <message>
        <source>ScreenShot Failed, folder has no write permission.</source>
        <translation type="vanished">截图失败，文件夹无写入权限。</translation>
    </message>
    <message>
        <source>Stereo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Left Channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Right Channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ScreenShot Failed, folder has no write permission or folder not exit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add mark ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add mark error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Buffering...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlayListItem</name>
    <message>
        <source>File not exist!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlayListItemMenu</name>
    <message>
        <source>Remove selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clear list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sort by name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sort by type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlayListWidget</name>
    <message>
        <source>Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tips</source>
        <translation type="vanished">提示</translation>
    </message>
    <message>
        <source>File not exist!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Are you sure you want to clear the list?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Play order</source>
        <translation type="vanished">播放顺序</translation>
    </message>
    <message>
        <source>Clear list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kylin-video-990</source>
        <translation type="vanished">影音</translation>
    </message>
    <message>
        <source>Switch views</source>
        <translation type="vanished">切换视图</translation>
    </message>
    <message>
        <source>Are you sure you want to clear the list?
The file being played will be stopped.</source>
        <translation type="vanished">确定清空列表</translation>
    </message>
    <message>
        <source>The file being played will be stopped.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Marks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Load file error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preview view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>List view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>One loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sequential</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>List loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please add file to list ~</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PreviewWidget</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Model name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetUpDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set up</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ScreenShot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Subtitle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Volume</source>
        <translation type="vanished">声音设置</translation>
    </message>
    <message>
        <source>Codec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video Player Set up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetupCodec</name>
    <message>
        <source>Demux</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video decoder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio decoder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Decode threads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>no</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>auto</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetupPlay</name>
    <message>
        <source>Set fullscreen when open video.</source>
        <translation type="vanished">打开视频时自动全屏</translation>
    </message>
    <message>
        <source>Clear play list on exit.</source>
        <translation type="vanished">退出时清空播放列表</translation>
    </message>
    <message>
        <source>Automatically plays from where the file was last stopped.</source>
        <translation type="vanished">自动从文件上次停止的位置播放</translation>
    </message>
    <message>
        <source>Automatically find associated files to play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set fullscreen when open video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clear play list on exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Automatically plays from where the file was last stopped</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetupScreenshot</name>
    <message>
        <source>Only save to clipboard.</source>
        <translation type="vanished">仅保存到剪切板</translation>
    </message>
    <message>
        <source>Save to file.</source>
        <translation type="vanished">保存为文件</translation>
    </message>
    <message>
        <source>save path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>browse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>save type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Screenshot according to the current screen size.</source>
        <translation type="vanished">按当前显示的画面尺寸截图</translation>
    </message>
    <message>
        <source>Choose a directory</source>
        <translation type="vanished">选择一个文件夹</translation>
    </message>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Only save to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save to file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Screenshot according to the current screen size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video Player Choose a directory</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetupShortcut</name>
    <message>
        <source>file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>volume</source>
        <translation type="vanished">声音</translation>
    </message>
    <message>
        <source>subtitle</source>
        <translation type="vanished">字幕</translation>
    </message>
    <message>
        <source>other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>open dir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>prev file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>next file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>play/pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>speed up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>speed down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>speed normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>forword</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>backword</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>forward 30s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>backword 30s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>insert bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ib notes</source>
        <translation type="vanished">插入与注释书签</translation>
    </message>
    <message>
        <source>fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>mini mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>to top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>screenshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>cut</source>
        <translation type="vanished">截取</translation>
    </message>
    <message>
        <source>light up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>light down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>forward rotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>backward rotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>horizontal flip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>vertical flip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>image boost</source>
        <translation type="vanished">画质增强</translation>
    </message>
    <message>
        <source>volume up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>volume down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>mute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>audio next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>default channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>left channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>right channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sub load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sub earlier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sub later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sub up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sub down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sub next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>play list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sub</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetupSubtitle</name>
    <message>
        <source>Sub loading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto loading subtitles with the same name.</source>
        <translation type="vanished">自动载入同名字幕</translation>
    </message>
    <message>
        <source>Auto loading other subtitles in the same folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sub Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>browse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Font Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Family</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose a directory</source>
        <translation type="vanished">选择一个文件夹</translation>
    </message>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto loading subtitles with the same name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video Player Choose a directory</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetupSystem</name>
    <message>
        <source>Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Minimize to system tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pause video playback when minimized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Multiple run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Allow multiple Video Player to run simultaneously</source>
        <translation type="vanished">允许同时运行多个影音</translation>
    </message>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>After sleep/sleep/lock screen and wake up, keep playing state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Allow multiple Kylin Video to run simultaneously</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetupVolume</name>
    <message>
        <source>Sound card selection</source>
        <translation type="vanished">声卡选择</translation>
    </message>
    <message>
        <source>Volume contral</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Global volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Default volume standardization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Output driver</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShortCutItem</name>
    <message>
        <source>Hotkey conflict</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShortCutSetting</name>
    <message>
        <source>open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>open dir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>prev file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>next file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>play/pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>speed up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>speed down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>speed normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>forword</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>backword</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>forward 30s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>backword 30s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>insert bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ib notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>mini mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>to top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>screenshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>light up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>light down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>forward rotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>backward rotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>horizontal flip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>vertical flip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>image boost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>volume up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>volume down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>mute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>audio next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>default channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>left channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>right channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sub load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sub earlier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sub later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sub up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sub down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sub next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>play list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>help documentation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SystemTrayIcon</name>
    <message>
        <source>Video Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TitleMenu</name>
    <message>
        <source>Upload to cloud</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Follow system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Light theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Black theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Privacy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clear mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Advice and feedback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Official website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>System setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Play setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Screenshot setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Subtitle setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Decoder setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Shortcut setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TitleWidget</name>
    <message>
        <source>Video Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mini mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TopWindow</name>
    <message>
        <source>Video Player</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WaylandDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
