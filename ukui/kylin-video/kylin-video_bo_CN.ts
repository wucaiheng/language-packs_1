<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../aboutdialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>ཁ་བརྡའི་སྒྲོམ་གཞི་འཛུགས་པ།</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="189"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;Video Player&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt; &lt;head/&gt; &lt;body&gt; &lt;p align=&quot;center&quot;&gt; བརྙན་ཕབ་འཕྲུལ་འཁོར&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="199"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;version: 3.1.1&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt; &lt;head/&gt; &lt;body&gt; &lt;p align=&quot;center&quot;&gt;པར་གཞི། 3.1.1&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="230"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans Mono CJK SC&apos;; font-size:12pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt;&quot;&gt;Video Player is a front player with beautiful interface and good interaction. It is developed with Qt5 and uses MPV as the playback engine. Video Player supports almost all audio and video formats and has powerful decoding ability.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt; &lt;head&gt; &lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt; &lt;style type=&quot;text/css&quot;&gt;
p, li {དཀར་པོའི་བར་སྟོང་། སྔོན་ཚུད་ནས་ཐུམ་སྒྲིལ།}
&lt;/style&gt; &lt;/head&gt; &lt;body style=&quot; font-family:&apos;Noto Sans Mono CJK SC&apos;; font-size:12pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; &lt;span style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt;&quot;&gt; བརྙན་ཕབ་འཕྲུལ་ཆས་ནི་མཛེས་སྡུག་ལྡན་པའི་འབྲེལ་མཐུད་དང་ཕན་ཚུན་ནུས་པ་འདོན་སྤེལ་བྱེད་པའི་མདུན་ དེ་ནི་Qt5དང་མཉམ་དུ་གསར་སྤེལ་བྱས་པ་མ་ཟད་MPVདེ་ཕྱིར་ལྡོག་བྱེད་པའི་སྒུལ་བྱེད་འཕྲུལ་འཁོར་དུ་བཀོལ་བ་རེད། བརྙན་ཕབ་འཕྲུལ་ཆས་ཀྱིས་ཧ་ལམ་སྒྲ་དང་བརྙན་ཕབ་ཀྱི་རྣམ་པ་ཡོད་ཚད་ལ་རྒྱབ་སྐྱོར་བྱས་ནས་ནུས་ལྡན་གྱི་ཨང་སྒྲིག་ནུས་པ་ལྡན་པ་རེད། &lt;/span&gt; &lt;/p&gt; &lt;/body&gt; &lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../aboutdialog.ui" line="254"/>
        <source>service and support: support@kylinos.cn</source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར། support@kylinos.cn</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="38"/>
        <source>version: </source>
        <translation>པར་གཞི། </translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="39"/>
        <source>service and support: </source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར་ཚོགས་པ། </translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="62"/>
        <location filename="../aboutdialog.cpp" line="76"/>
        <source>Service &amp; Support: </source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར། </translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="102"/>
        <source>Video Player About</source>
        <translation>བརྙན་ཕབ་འཕྲུལ་འཁོར་གྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cpp" line="119"/>
        <source>Video Player</source>
        <translation>བརྙན་ཕབ་འཕྲུལ་འཁོར།</translation>
    </message>
</context>
<context>
    <name>ContralBar</name>
    <message>
        <location filename="../contralbar.ui" line="26"/>
        <source>Form</source>
        <translation>རིགས།</translation>
    </message>
    <message>
        <location filename="../contralbar.ui" line="101"/>
        <source>--:--:--/--:--:--</source>
        <translation>--:--:--/--:--:--</translation>
    </message>
    <message>
        <location filename="../contralbar.cpp" line="425"/>
        <source>Next</source>
        <translation>རྗེས་མ།</translation>
    </message>
    <message>
        <location filename="../contralbar.cpp" line="426"/>
        <source>Previous</source>
        <translation>སྔོན་མ།</translation>
    </message>
    <message>
        <location filename="../contralbar.cpp" line="427"/>
        <location filename="../contralbar.cpp" line="691"/>
        <source>Play</source>
        <translation>གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../contralbar.cpp" line="428"/>
        <source>Volume</source>
        <translation>སྒྲ་ཚད།</translation>
    </message>
    <message>
        <location filename="../contralbar.cpp" line="429"/>
        <source>Speed</source>
        <translation>མྱུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../contralbar.cpp" line="430"/>
        <source>Tools</source>
        <translation>ཡོ་བྱད་སྒམ།</translation>
    </message>
    <message>
        <location filename="../contralbar.cpp" line="431"/>
        <location filename="../contralbar.cpp" line="727"/>
        <source>Full screen</source>
        <translation>བརྙན་ཤེལ་ཧྲིལ་པོ།</translation>
    </message>
    <message>
        <location filename="../contralbar.cpp" line="502"/>
        <location filename="../contralbar.cpp" line="670"/>
        <source>Screen shot</source>
        <translation>རིས་འདྲ།</translation>
    </message>
    <message>
        <location filename="../contralbar.cpp" line="634"/>
        <source>2.0X</source>
        <translation>ལྡབ་གཉིས་ཀྱི་མྱུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../contralbar.cpp" line="635"/>
        <source>1.5X</source>
        <translation>ལྡབ1.5མྱུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../contralbar.cpp" line="636"/>
        <source>1.25X</source>
        <translation>ལྡབ1.25མྱུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../contralbar.cpp" line="637"/>
        <source>1.0X</source>
        <translation>དང་ཐོག་གི་མྱུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../contralbar.cpp" line="638"/>
        <source>0.75X</source>
        <translation>ལྡབ0.75མྱུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../contralbar.cpp" line="639"/>
        <source>0.5X</source>
        <translation>ལྡབ0.5མྱུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../contralbar.cpp" line="670"/>
        <source>Add mark</source>
        <translation>རྟགས་ཁ་སྣོན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../contralbar.cpp" line="687"/>
        <source>Pause</source>
        <translation>གནས་སྐབས་མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../contralbar.cpp" line="724"/>
        <source>Exit full screen</source>
        <translation>བརྙན་ཤེལ་ཧྲིལ་བོ་ཕྱིར་འབུད་པ།</translation>
    </message>
</context>
<context>
    <name>GlobalSetup</name>
    <message>
        <location filename="../global/globalsetup.cpp" line="428"/>
        <location filename="../global/globalsetup.cpp" line="440"/>
        <location filename="../global/globalsetup.cpp" line="445"/>
        <location filename="../global/globalsetup.cpp" line="462"/>
        <location filename="../global/globalsetup.cpp" line="470"/>
        <source>auto</source>
        <translation>རང་འགུལ།</translation>
    </message>
    <message>
        <location filename="../global/globalsetup.cpp" line="467"/>
        <location filename="../global/globalsetup.cpp" line="471"/>
        <source>default</source>
        <translation>སོར་བཞག</translation>
    </message>
</context>
<context>
    <name>HomePage</name>
    <message>
        <location filename="../homepage.cpp" line="25"/>
        <source>open file</source>
        <translation>ཁ་ཕྱེ་བའི་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../homepage.cpp" line="35"/>
        <source>open dir</source>
        <translation>ཁ་ཕྱེས་པའི་ཡིག་སྣོད།</translation>
    </message>
    <message>
        <location filename="../homepage.cpp" line="52"/>
        <source>Video Player</source>
        <translation>བརྙན་ཕབ་འཕྲུལ་འཁོར།</translation>
    </message>
</context>
<context>
    <name>KRightClickMenu</name>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="191"/>
        <source>Open &amp;File...</source>
        <translation>ཁ་ཕྱེ་བའི་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="192"/>
        <source>open file</source>
        <translation>ཁ་ཕྱེ་བའི་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="196"/>
        <source>Open &amp;Directory...</source>
        <translation>སྒོ་འབྱེད་པའི་ཡིག་སྣོད།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="197"/>
        <source>open dir</source>
        <translation>ཁ་ཕྱེས་པའི་ཡིག་སྣོད།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="215"/>
        <source>to top</source>
        <translation>རྩེ་མོར་བཞག</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="225"/>
        <source>ToTop</source>
        <translation>རྩེ་མོར་བཞག</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="234"/>
        <source>Order</source>
        <translation>མངགས་ཐོའི་གོ་རིམ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="237"/>
        <source>One Loop</source>
        <translation>འཁོར་སྐྱོད་རྐྱང་པ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="241"/>
        <source>Sequence</source>
        <translation>གཏོང་བའི་གོ་རིམ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="245"/>
        <source>List loop</source>
        <translation>རེའུ་མིག་འཁོར་སྐྱོད་བྱེད་བཞིན་པ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="249"/>
        <source>Random</source>
        <translation>སྐབས་བསྟུན་ཁྱབ་གཏོང་།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="298"/>
        <source>Frame</source>
        <translation>པར་ངོས།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="304"/>
        <source>Default frame</source>
        <translation>སོར་བཞག</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="308"/>
        <source>4:3</source>
        <translation>4:3</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="312"/>
        <source>16:9</source>
        <translation>16:9</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="316"/>
        <source>Full frame</source>
        <translation>སྒྲོམ་གཞི་ཆ་ཚང་།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="340"/>
        <location filename="../kwidget/kmenu.cpp" line="346"/>
        <source>restore frame</source>
        <translation>སླར་གསོ་བྱེད་པའི་སྒྲོམ་གཞི།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="348"/>
        <location filename="../kwidget/kmenu.cpp" line="354"/>
        <source>forward rotate</source>
        <translation>དུས་ཚོད་ཀྱི་འཁོར་ཕྱོགས་དང་བསྟུན་ནས་ཏུའུ་༩༠འཁོར།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="353"/>
        <source>Along rotate</source>
        <translation>དུས་ཚོད་ཀྱི་འཁོར་ཕྱོགས་དང་བསྟུན་ནས་ཏུའུ་༩༠འཁོར།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="356"/>
        <location filename="../kwidget/kmenu.cpp" line="362"/>
        <source>backward rotate</source>
        <translation>དུས་ཚོད་ཀྱི་འཁོར་ཕྱོགས་དང་བསྟུན་ནས་ཏུའུ་༩༠འཁོར།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="361"/>
        <source>Inverse rotate</source>
        <translation>དུས་ཚོད་ཀྱི་འཁོར་ཕྱོགས་དང་བསྟུན་ནས་ཏུའུ་༩༠འཁོར།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="364"/>
        <location filename="../kwidget/kmenu.cpp" line="367"/>
        <source>horizontal flip</source>
        <translation>ཆུ་ཚད་ཕྱིར་འཁོར།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="366"/>
        <source>Horizontally flip</source>
        <translation>ཆུ་ཚད་ཕྱིར་འཁོར།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="369"/>
        <location filename="../kwidget/kmenu.cpp" line="372"/>
        <source>vertical flip</source>
        <translation>དྲང་འཕྱང་འཁོར་བ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="371"/>
        <source>Vertically flip</source>
        <translation>གཞུང་ཕྱོགས་ནས་ཕར་འགྲོ་ཚུར་འོང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="378"/>
        <source>Show profile</source>
        <translation>ངོ་སྤྲོད་མདོར་བསྡུས།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="400"/>
        <source>Audio</source>
        <translation>སྒྲ་ཕབ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="403"/>
        <source>AudioTrack</source>
        <translation>སྒྲ་ཕབ་འཕྲུལ་ཆས།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="413"/>
        <source>AudioChannel</source>
        <translation>སྒྲ་ལམ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="420"/>
        <source>Default</source>
        <translation>སོར་བཞག</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="427"/>
        <source>Stereo</source>
        <translation>ལངས་གཟུགས་སྒྲ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="434"/>
        <source>Left channel</source>
        <translation>གཡོན་ཕྱོགས་ཀྱི་སྒྲ་ལམ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="441"/>
        <source>Right channel</source>
        <translation>གཡས་ཕྱོགས་ཀྱི་སྒྲ་ལམ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="476"/>
        <source>Audio set</source>
        <translation>སྒྲའི་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="490"/>
        <source>Subtitle</source>
        <translation>ཡིག་བརྙན།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="493"/>
        <source>sub load</source>
        <translation>ལག་པ་འགུལ་ནས་ཡི་གེ་སྣོན་པ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="541"/>
        <location filename="../kwidget/kmenu.cpp" line="564"/>
        <source>Video Player Choose a file</source>
        <translation>བརྙན་ཕབ་འཕྲུལ་འཁོར་གྱིས་ཡིག་ཆ་ཞིག་བདམས་པ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="543"/>
        <location filename="../kwidget/kmenu.cpp" line="566"/>
        <source>Subtitles</source>
        <translation>ཡིག་བརྙན།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="567"/>
        <source>All files</source>
        <translation>ཡིག་ཆ་ཡོད་ཚད།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="573"/>
        <source>Load subtitle</source>
        <translation>ཡིག་བརྙན་སྣོན་པ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="578"/>
        <source>Subtitle select</source>
        <translation>ཡིག་བརྙན་གདམ་གསེས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="583"/>
        <source>No subtitle</source>
        <translation>ཡིག་བརྙན་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="597"/>
        <source>Match subtitle</source>
        <translation>ཆ་འགྲིག་ཡིག་བརྙན།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="602"/>
        <source>Search subtitle</source>
        <translation>ཡིག་བརྙན་འཚོལ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="607"/>
        <source>Subtitle set</source>
        <translation>ཡིག་བརྙན་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="617"/>
        <source>Play</source>
        <translation>གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="621"/>
        <source>Play/Pause</source>
        <translation>གཏོང་བ་དང་མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="627"/>
        <source>volume up</source>
        <translation>སྒྲ་ཇེ་མཐོར་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="633"/>
        <source>volume down</source>
        <translation>སྒྲ་ཇེ་དམའ་རུ་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="639"/>
        <source>forward</source>
        <translation>མྱུར་འཛུལ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="645"/>
        <source>backward</source>
        <translation>མྱུར་དུ་ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="657"/>
        <source>setup</source>
        <translation>སྒྲིག་འཛུགས་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="668"/>
        <source>Media info</source>
        <translation>སྨྱན་སྦྱོར་གྱི་ཆ་འཕྲིན།</translation>
    </message>
</context>
<context>
    <name>KylinUI::MessageBox</name>
    <message>
        <location filename="../messagebox.cpp" line="92"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../messagebox.cpp" line="96"/>
        <source>Ok</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
</context>
<context>
    <name>ListLoopMenu</name>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="886"/>
        <source>One Loop</source>
        <translation>འཁོར་སྐྱོད་རྐྱང་པ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="891"/>
        <source>List loop</source>
        <translation>རེའུ་མིག་འཁོར་སྐྱོད་བྱེད་བཞིན་པ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="896"/>
        <source>Random</source>
        <translation>སྐབས་བསྟུན་ཁྱབ་གཏོང་།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="901"/>
        <source>Sequence</source>
        <translation>གོ་རིམ་ལྟར་གཏོང་བ།</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <location filename="../mainwidget.ui" line="20"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="115"/>
        <source>Video Player</source>
        <translation>གློག་བརྙན་གྱི་སྒྲ།</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="741"/>
        <source>Video Player Choose a file</source>
        <translation>བརྙན་ཕབ་འཕྲུལ་འཁོར་གྱིས་ཡིག་ཆ་ཞིག་བདམས་པ།</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="743"/>
        <source>Multimedia</source>
        <translation>སྨྱན་སྦྱོར།</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="746"/>
        <source>Video</source>
        <translation>བརྙན་ཕབ།</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="749"/>
        <source>Audio</source>
        <translation>སྒྲ་ཕབ།</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="824"/>
        <source>Video Player Choose a directory</source>
        <translation>བརྙན་ཕབ་འཕྲུལ་འཁོར་གྱིས་ཡིག་ཆ་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="917"/>
        <source>video player</source>
        <translation>གློག་བརྙན་གྱི་སྒྲ།</translation>
    </message>
</context>
<context>
    <name>MarkListItem</name>
    <message>
        <location filename="../playlistwidget.cpp" line="1683"/>
        <source>Video Player</source>
        <translation>གློག་བརྙན་གྱི་སྒྲ།</translation>
    </message>
    <message>
        <location filename="../playlistwidget.cpp" line="1683"/>
        <location filename="../playlistwidget.cpp" line="1687"/>
        <source>File not exist!</source>
        <translation>ཡིག་ཆ་མེད་པ་རེད།</translation>
    </message>
</context>
<context>
    <name>MediaInfoDialog</name>
    <message>
        <location filename="../mediainfodialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>ཁ་བརྡའི་སྒྲོམ་གཞི་འཛུགས་པ།</translation>
    </message>
    <message>
        <location filename="../mediainfodialog.ui" line="74"/>
        <source>video player</source>
        <translation>བརྙན་ཕབ་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../mediainfodialog.ui" line="127"/>
        <source>ok</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../mediainfodialog.cpp" line="52"/>
        <source>media info</source>
        <translation>སྨྱན་སྦྱོར་གྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../mediainfodialog.cpp" line="53"/>
        <source>title:</source>
        <translation>ཡིག་ཆའི་ཁ་བྱང་ནི།</translation>
    </message>
    <message>
        <location filename="../mediainfodialog.cpp" line="54"/>
        <source>type:</source>
        <translation>ཡིག་ཆའི་རིགས་དབྱིབས་ནི།</translation>
    </message>
    <message>
        <location filename="../mediainfodialog.cpp" line="55"/>
        <source>size:</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་ནི།</translation>
    </message>
    <message>
        <location filename="../mediainfodialog.cpp" line="56"/>
        <source>duration:</source>
        <translation>ཡིག་ཆའི་དུས་ཡུན་ནི།</translation>
    </message>
    <message>
        <location filename="../mediainfodialog.cpp" line="57"/>
        <source>path:</source>
        <translation>ཡིག་ཆའི་གནས།</translation>
    </message>
</context>
<context>
    <name>MiniModeShade</name>
    <message>
        <location filename="../minimodeshade.cpp" line="50"/>
        <source>pause</source>
        <translation>མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../minimodeshade.cpp" line="54"/>
        <source>play</source>
        <translation>གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../minimodeshade.cpp" line="93"/>
        <source>close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../minimodeshade.cpp" line="102"/>
        <source>normal mode</source>
        <translation>རྒྱུན་ལྡན་གྱི་རྣམ་པ།</translation>
    </message>
</context>
<context>
    <name>MpvCore</name>
    <message>
        <location filename="../core/mpvcore.cpp" line="66"/>
        <source>Pictures</source>
        <translation>པར་རིས།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="93"/>
        <source>default</source>
        <translation>སོར་བཞག</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="102"/>
        <source>auto</source>
        <translation>རང་འགུལ།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="337"/>
        <source>File</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="338"/>
        <source>Title</source>
        <translation>ཁ་བྱང་།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="339"/>
        <source>File size</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="340"/>
        <source>Date created</source>
        <translation>གསར་སྐྲུན་བྱས་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="341"/>
        <source>Media length</source>
        <translation>དུས་ཡུན།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="343"/>
        <source>Video (x%0)</source>
        <translation>བརྙན་ཕབ(x%0)</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="344"/>
        <source>Video Output</source>
        <translation>བརྙན་ཕབ་ཀྱི་ཕྱིར་འདོན།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="345"/>
        <source>Resolution</source>
        <translation>འབྱེད་ཕྱོད།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="348"/>
        <source>FPS</source>
        <translation>FPS</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="349"/>
        <location filename="../core/mpvcore.cpp" line="355"/>
        <source>Bitrate</source>
        <translation>ཨང་ཕྱོད།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="349"/>
        <location filename="../core/mpvcore.cpp" line="355"/>
        <source>%0 kbps</source>
        <translation>%0 kbps</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="351"/>
        <source>Audio (x%0)</source>
        <translation>སྒྲ་གདངས་(x%0)</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="352"/>
        <source>Audio Output</source>
        <translation>སྒྲ་ཕབ་ཕྱིར་གཏོང་།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="353"/>
        <source>Sample Rate</source>
        <translation>དཔེ་མཚོན་འབྱེད་ཕྱོད།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="354"/>
        <source>Channels</source>
        <translation>སྒྲ་ལམ།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="359"/>
        <source>Chapters</source>
        <translation>ལེའུ་སོ་སོ།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="368"/>
        <source>Metadata</source>
        <translation>རྒྱུའི་གཞི་གྲངས།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="633"/>
        <source>Stereo</source>
        <translation>ལངས་གཟུགས་སྒྲ།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="639"/>
        <source>Left Channel</source>
        <translation>གཡོན་ཕྱོགས་ཀྱི་སྒྲ་ལམ།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="648"/>
        <source>Right Channel</source>
        <translation>གཡས་ཕྱོགས་ཀྱི་སྒྲ་ལམ།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="763"/>
        <location filename="../core/mpvcore.cpp" line="774"/>
        <source>brightness : %1</source>
        <translation>གསལ་ཆ། %1</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="906"/>
        <location filename="../core/mpvcore.cpp" line="913"/>
        <source>subtitle delay : %1s</source>
        <translation>ཡིག་བརྙན་གྱི་འགོར་འགྱངས་ནི་%1</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="932"/>
        <source>Add mark error</source>
        <translation>དཔེ་རྟགས་ཁ་སྣོན་བྱས་པ་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="962"/>
        <source>Add mark ok</source>
        <translation>དཔེ་རྟགས་ཁ་སྣོན་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1010"/>
        <source>restore frame</source>
        <translation>སླར་གསོ་བྱེད་པའི་སྒྲོམ་གཞི།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1052"/>
        <source>Horizontal Flip: </source>
        <translation>འཕྲེད་ཕྱོགས་ནས་ཕར་འགྲོ་ཚུར་འོང་བྱེད། </translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1052"/>
        <location filename="../core/mpvcore.cpp" line="1069"/>
        <source>close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1052"/>
        <location filename="../core/mpvcore.cpp" line="1069"/>
        <source>open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1069"/>
        <source>Vertical Flip: </source>
        <translation>དྲང་འཕྱང་གི་ལྡོག་ཕྱོགས་ནི། </translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1080"/>
        <source>ScreenShot OK</source>
        <translation>པར་བཅད་ལེགས་འགྲུབ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1082"/>
        <source>ScreenShot Failed, folder has no write permission or folder not exit.</source>
        <translation>པར་དྲས་པ་ཕམ་སོང་།ཡིག་ཁུག་ནང་དུ་འབྲི་བའི་དབང་ཆ་མེད་པའམ་ཡང་ན་ཡིག་ཁུག་མེད་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1151"/>
        <source>File:</source>
        <translation>ཡིག་ཆ་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1153"/>
        <source>Video:</source>
        <translation>བརྙན་ཕབ།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1154"/>
        <source>Resolution:</source>
        <translation>འབྱེད་ཕྱོད།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1155"/>
        <source>fps:</source>
        <translation>ལྷེབ་གྲངས།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1156"/>
        <location filename="../core/mpvcore.cpp" line="1162"/>
        <source>Bitrate:</source>
        <translation>པི་ཐི་ཕྱོད།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1159"/>
        <source>Audio:</source>
        <translation>སྒྲ་ཕབ།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1160"/>
        <source>Sample Rate:</source>
        <translation>མ་དཔེ་འཚོལ་ཚད།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1161"/>
        <source>Channels:</source>
        <translation>སྒྲ་ལམ།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1277"/>
        <location filename="../core/mpvcore.cpp" line="1508"/>
        <source>volume : %1</source>
        <translation>སྒྲ་ཚད།%1</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1284"/>
        <location filename="../core/mpvcore.cpp" line="1540"/>
        <source>Mute</source>
        <translation>སྒྲ་མེད།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1284"/>
        <location filename="../core/mpvcore.cpp" line="1540"/>
        <source>Cancel Mute</source>
        <translation>སྒྲ་མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1343"/>
        <source>Buffering...</source>
        <translation>ལྷོད་གཏོང་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1516"/>
        <source>speed : %1x</source>
        <translation>མྱུར་ཚད: %1x</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1604"/>
        <source>Playing</source>
        <translation>གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1614"/>
        <source>Paused</source>
        <translation>མཚམས་བཞག་པ།</translation>
    </message>
    <message>
        <location filename="../core/mpvcore.cpp" line="1660"/>
        <location filename="../core/mpvcore.cpp" line="1665"/>
        <source>subtitle : </source>
        <translation>ཡིག་བརྙན། </translation>
    </message>
</context>
<context>
    <name>PlayListItem</name>
    <message>
        <location filename="../playlistwidget.cpp" line="1409"/>
        <source>File not exist!</source>
        <translation>ཡིག་ཆ་མེད་པ་རེད།</translation>
    </message>
</context>
<context>
    <name>PlayListItemMenu</name>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1235"/>
        <source>Remove selected</source>
        <translation>བདམས་ཟིན་པའི་གནས་དབྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1239"/>
        <source>Remove invalid</source>
        <translation>གོ་མི་ཆོད་པའི་ཡིག་ཆ་མེད་པར་བཏང་།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1243"/>
        <source>Clear list</source>
        <translation>གཏོང་བའི་རེའུ་མིག་གཙང་སེལ་བྱས།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1247"/>
        <source>Open folder</source>
        <translation>ཡིག་སྣོད་ཚང་མ་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1251"/>
        <source>Sort</source>
        <translation>རིམ་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1254"/>
        <source>Sort by name</source>
        <translation>མིང་ལྟར་གོ་རིམ་སྒྲིག་པ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1257"/>
        <source>Sort by type</source>
        <translation>རིགས་ལྟར་གོ་རིམ་སྒྲིག་པ།</translation>
    </message>
</context>
<context>
    <name>PlayListWidget</name>
    <message>
        <location filename="../playlistwidget.ui" line="14"/>
        <source>Form</source>
        <translation>རིགས།</translation>
    </message>
    <message>
        <location filename="../playlistwidget.cpp" line="174"/>
        <location filename="../playlistwidget.cpp" line="178"/>
        <location filename="../playlistwidget.cpp" line="922"/>
        <location filename="../playlistwidget.cpp" line="934"/>
        <source>Video Player</source>
        <translation>བརྙན་ཕབ།</translation>
    </message>
    <message>
        <location filename="../playlistwidget.cpp" line="174"/>
        <source>Are you sure you want to clear the list?</source>
        <translation>ཁྱོད་ཀྱིས་ངེས་པར་དུ་མིང་ཐོ་གཙང་བཤེར་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../playlistwidget.cpp" line="178"/>
        <source>The file being played will be stopped.</source>
        <translation>འདོན་སྤེལ་བྱེད་བཞིན་པའི་ཡིག་ཆ་དེ་བཀག་འགོག་བྱེད་རྒྱུ་རེད།</translation>
    </message>
    <message>
        <location filename="../playlistwidget.cpp" line="402"/>
        <source>Preview view</source>
        <translation>བསྡུས་རིས་རི་མོ།</translation>
    </message>
    <message>
        <location filename="../playlistwidget.cpp" line="415"/>
        <source>List view</source>
        <translation>རེའུ་མིག་མཐོང་རིས།</translation>
    </message>
    <message>
        <location filename="../playlistwidget.cpp" line="616"/>
        <source>Video</source>
        <translation>བརྙན་ཕབ།</translation>
    </message>
    <message>
        <location filename="../playlistwidget.cpp" line="616"/>
        <source>Marks</source>
        <translation>མཚོན་རྟགས།</translation>
    </message>
    <message>
        <location filename="../playlistwidget.cpp" line="650"/>
        <source>Add file</source>
        <translation>ཡིག་ཆ་ཁ་སྣོན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../playlistwidget.cpp" line="651"/>
        <source>Clear list</source>
        <translation>རེའུ་མིག་གཙང་སེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../playlistwidget.cpp" line="741"/>
        <location filename="../playlistwidget.cpp" line="743"/>
        <source>Load file error!</source>
        <translation>ཡིག་ཚགས་སྣོན་པ་ནོར་འཁྲུལ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../playlistwidget.cpp" line="823"/>
        <source>Please add file to list ~</source>
        <translation>མིང་ཐོའི་ནང་དུ་ཡིག་ཆ་ཁ་སྣོན་བྱེད་རོགས། ~</translation>
    </message>
    <message>
        <location filename="../playlistwidget.cpp" line="841"/>
        <source>One loop</source>
        <translation>འཁོར་སྐྱོད་རྐྱང་པ།</translation>
    </message>
    <message>
        <location filename="../playlistwidget.cpp" line="845"/>
        <source>Sequential</source>
        <translation>གོ་རིམ་ལྟར་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../playlistwidget.cpp" line="849"/>
        <source>List loop</source>
        <translation>རེའུ་མིག་འཁོར་སྐྱོད་བྱེད་བཞིན་པ།</translation>
    </message>
    <message>
        <location filename="../playlistwidget.cpp" line="853"/>
        <source>Random</source>
        <translation>སྐབས་བསྟུན་ཁྱབ་གཏོང་།</translation>
    </message>
    <message>
        <location filename="../playlistwidget.cpp" line="922"/>
        <location filename="../playlistwidget.cpp" line="926"/>
        <location filename="../playlistwidget.cpp" line="934"/>
        <location filename="../playlistwidget.cpp" line="938"/>
        <source>File not exist!</source>
        <translation>ཡིག་ཆ་མེད་པ་རེད།</translation>
    </message>
</context>
<context>
    <name>PreviewWidget</name>
    <message>
        <location filename="../previewwidget.ui" line="14"/>
        <source>Form</source>
        <translation>རིགས།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../global/functions.cpp" line="109"/>
        <source>Model name</source>
        <translation>དཔེ་དབྱིབས་ཀྱི་མིང་།</translation>
    </message>
</context>
<context>
    <name>SetUpDialog</name>
    <message>
        <location filename="../setupdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>སྒྲིག་བཀོད་གླེང་མོལ་སྒྲོམ།</translation>
    </message>
    <message>
        <location filename="../setupdialog.ui" line="233"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../setupdialog.ui" line="240"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../setupdialog.cpp" line="175"/>
        <source>Video Player Set up</source>
        <translation>བརྙན་ཕབ་འཕྲུལ་ཆས་བཙུགས་པ།</translation>
    </message>
    <message>
        <location filename="../setupdialog.cpp" line="197"/>
        <source>Setup</source>
        <translation>སྒྲིག་འཛུགས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../setupdialog.cpp" line="225"/>
        <source>System</source>
        <translation>མ་ལག་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../setupdialog.cpp" line="226"/>
        <source>Play</source>
        <translation>སྒྲིག་བཀོད་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../setupdialog.cpp" line="227"/>
        <source>ScreenShot</source>
        <translation>དྲས་པའི་པར་རིས་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../setupdialog.cpp" line="228"/>
        <source>Subtitle</source>
        <translation>ཡིག་བརྙན་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../setupdialog.cpp" line="229"/>
        <source>Audio</source>
        <translation>སྒྲ་ཕབ་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../setupdialog.cpp" line="230"/>
        <source>Codec</source>
        <translation>ཨང་གྲངས་འབྱེད་ཆས་ཀྱི་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../setupdialog.cpp" line="231"/>
        <source>Shortcut</source>
        <translation>མགྱོགས་མཐེབ་སྒྲིག་འགོད།</translation>
    </message>
</context>
<context>
    <name>SetupCodec</name>
    <message>
        <location filename="../setupwidget/setupcodec.ui" line="14"/>
        <source>Form</source>
        <translation>རིགས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupcodec.ui" line="32"/>
        <source>Demux</source>
        <translation>བསྐྱར་འབྱེད་བཀོལ་ཆས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupcodec.ui" line="49"/>
        <source>Video decoder</source>
        <translation>བརྙན་གྱི་ཨང་གྲངས་འབྱེད་ཆས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupcodec.ui" line="66"/>
        <source>Audio decoder</source>
        <translation>སྒྲ་ཟློས་ཨང་གྲངས་འབྱེད་ཆས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupcodec.cpp" line="36"/>
        <source>default</source>
        <translation>སོར་བཞག</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupcodec.cpp" line="36"/>
        <source>no</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupcodec.cpp" line="40"/>
        <source>Video output</source>
        <translation>བརྙན་འཕྲིན་ཕྱིར་གཏོང་སྒུལ་ཤུགས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupcodec.cpp" line="41"/>
        <source>auto</source>
        <translation>རང་འགུལ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupcodec.cpp" line="49"/>
        <source>Decode threads</source>
        <translation>ཨང་སྒྲིག་སྐུད་པ།</translation>
    </message>
</context>
<context>
    <name>SetupPlay</name>
    <message>
        <location filename="../setupwidget/setupplay.ui" line="14"/>
        <source>Form</source>
        <translation>རིགས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupplay.ui" line="35"/>
        <source>Set fullscreen when open video</source>
        <translation>བརྙན་ཕབ་བྱེད་སྐབས་བརྙན་ཤེལ་ཆ་ཚང་ཞིག་གཏན་འཁེལ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupplay.ui" line="48"/>
        <source>Clear play list on exit</source>
        <translation>ཕྱིར་འབུད་སྐབས་གཏོང་བའི་རེའུ་མིག་གཙང་སེལ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupplay.ui" line="61"/>
        <source>Automatically plays from where the file was last stopped</source>
        <translation>རང་འགུལ་གྱིས་ཡིག་ཆ་དེ་ཆེས་མཐའ་མཇུག་གི་བཀག་འགོག་བྱེད་ས་ནས་འདོན་སྤེལ་བྱས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupplay.ui" line="74"/>
        <source>Automatically find associated files to play</source>
        <translation>རང་འགུལ་གྱིས་འབྲེལ་ཡོད་ཡིག་ཆ་བཙལ་ནས་རྩེད་མོ་རྩེ་བཞིན་ཡོད།</translation>
    </message>
</context>
<context>
    <name>SetupScreenshot</name>
    <message>
        <location filename="../setupwidget/setupscreenshot.ui" line="14"/>
        <source>Form</source>
        <translation>རིགས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupscreenshot.ui" line="35"/>
        <source>Only save to clipboard</source>
        <translation>དྲས་གཏུབ་པང་ལེབ་ཁོ་ནར་ཉར་ཚགས་བྱས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupscreenshot.ui" line="48"/>
        <source>Save to file</source>
        <translation>ཡིག་ཚགས་སུ་ཉར་ཚགས་བྱས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupscreenshot.ui" line="63"/>
        <source>save path</source>
        <translation>ཉར་ཚགས་ལམ་བུ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupscreenshot.ui" line="86"/>
        <source>browse</source>
        <translation>བཤར་ཆས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupscreenshot.ui" line="100"/>
        <source>save type</source>
        <translation>ཉར་ཚགས་བྱས་པའི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupscreenshot.ui" line="144"/>
        <source>Screenshot according to the current screen size</source>
        <translation>མིག་སྔར་མངོན་པའི་རི་མོའི་ཆེ་ཆུང་ལྟར་པར་དྲས་པ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupscreenshot.cpp" line="55"/>
        <source>Pictures</source>
        <translation>པར་རིས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupscreenshot.cpp" line="160"/>
        <source>Video Player Choose a directory</source>
        <translation>བརྙན་ཕབ་འཕྲུལ་འཁོར་གྱིས་ཡིག་སྣོད་གཅིག་འདེམས་པ།</translation>
    </message>
</context>
<context>
    <name>SetupShortcut</name>
    <message>
        <location filename="../setupwidget/setupshortcut.ui" line="14"/>
        <source>Form</source>
        <translation>རིགས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="181"/>
        <source>file</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="182"/>
        <source>play</source>
        <translation>གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="183"/>
        <source>image</source>
        <translation>པར་རིས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="184"/>
        <source>audio</source>
        <translation>སྒྲ</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="185"/>
        <source>sub</source>
        <translation>ཡིག་གཟུགས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="186"/>
        <source>other</source>
        <translation>གཞན་དག</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="242"/>
        <source>open file</source>
        <translation>ཁ་ཕྱེ་བའི་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="243"/>
        <source>open dir</source>
        <translation>ཡིག་སྣོད་ཁ་ཕྱེས་པ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="244"/>
        <source>prev file</source>
        <translation>ཡིག་ཆ་སྔོན་མ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="245"/>
        <source>next file</source>
        <translation>རྗེས་མའི་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="268"/>
        <source>play/pause</source>
        <translation>གཏོང་བ་དང་མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="269"/>
        <source>speed up</source>
        <translation>མྱུར་ཚད་ཇེ་མགྱོགས་སུ་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="270"/>
        <source>speed down</source>
        <translation>མྱུར་ཚད་ཇེ་དལ་དུ་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="271"/>
        <source>speed normal</source>
        <translation>མྱུར་ཚད་རྒྱུན་ལྡན་ལྟར་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="272"/>
        <source>forword</source>
        <translation>མྱུར་འཛུལ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="273"/>
        <source>backword</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="274"/>
        <source>forward 30s</source>
        <translation>སྐར་ཆ་30མྱུར་འཛུལ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="275"/>
        <source>backword 30s</source>
        <translation>སྐར་ཆ་30ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="276"/>
        <source>insert bookmark</source>
        <translation>དཔེ་རྟགས་ནང་དུ་བཅུག</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="300"/>
        <source>fullscreen</source>
        <translation>བརྙན་ཤེལ་ཧྲིལ་བོ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="301"/>
        <source>mini mode</source>
        <translation>miniརྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="302"/>
        <source>to top</source>
        <translation>རྩེ་གོང་བཞག</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="303"/>
        <source>screenshot</source>
        <translation>བརྙན་ཤེལ་གྱི་པར་རིས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="305"/>
        <source>light up</source>
        <translation>འོད་ཆེམ་ཆེམ་དུ་</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="306"/>
        <source>light down</source>
        <translation>འོད་སྣང་འཕྲོ་བ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="307"/>
        <source>forward rotate</source>
        <translation>མདུན་སྐྱོད་ཀྱི་འཁོར་སྐྱོད།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="308"/>
        <source>backward rotate</source>
        <translation>དུས་ཚོད་ཀྱི་འཁོར་ཕྱོགས་དང་ལྡོག་ནས་ཏུའུ་༩༠འཁོར།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="309"/>
        <source>horizontal flip</source>
        <translation>ཆུ་ཚད་ཕྱིར་འཁོར།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="310"/>
        <source>vertical flip</source>
        <translation>དྲང་འཕྱང་འཁོར་བ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="335"/>
        <source>volume up</source>
        <translation>སྒྲ་ཇེ་མཐོར་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="336"/>
        <source>volume down</source>
        <translation>བོངས་ཚད་མར་ཆག་པ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="337"/>
        <source>mute</source>
        <translation>སྒྲ་མེད།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="338"/>
        <source>audio next</source>
        <translation>སྒྲ་ལམ་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="339"/>
        <source>default channel</source>
        <translation>ཁས་ལེན་པའི་སྒྲ་ལམ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="340"/>
        <source>left channel</source>
        <translation>གཡོན་ཕྱོགས་ཀྱི་སྒྲ་ལམ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="341"/>
        <source>right channel</source>
        <translation>གཡས་ཕྱོགས་ཀྱི་སྒྲ་ལམ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="364"/>
        <source>sub load</source>
        <translation>ལག་པ་འགུལ་ནས་ཡི་གེ་སྣོན་པ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="365"/>
        <source>sub earlier</source>
        <translation>ཡིག་བརྙན་སྐར་ཆ་0.5་ཡི་སྔོན་ལ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="366"/>
        <source>sub later</source>
        <translation>ཡིག་བརྙན་སྐར་ཆ་0.5་ཡི་རྗེས་ལ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="367"/>
        <source>sub up</source>
        <translation>ཡི་གེའི་སྟེང་སྤོ་བ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="368"/>
        <source>sub down</source>
        <translation>ཡི་གེ་མར་ཕབ་པ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="369"/>
        <source>sub next</source>
        <translation>ཡིག་བརྙན་བརྗེ་སྤོར།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="392"/>
        <source>play list</source>
        <translation>གཏོང་བའི་རེའུ་མིག</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="393"/>
        <source>setup</source>
        <translation>གཏོང་ཆས་སྒྲིག་བཀོད།</translation>
    </message>
</context>
<context>
    <name>SetupSubtitle</name>
    <message>
        <location filename="../setupwidget/setupsubtitle.ui" line="14"/>
        <source>Form</source>
        <translation>རིགས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupsubtitle.ui" line="32"/>
        <source>Sub loading</source>
        <translation>ཡིག་བརྙན་ནང་དུ་བཀོད།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupsubtitle.ui" line="45"/>
        <source>Auto loading subtitles with the same name</source>
        <translation>རང་འགུལ་གྱིས་མིང་གཅིག་པའི་ཡི་གེའི་ནང་དུ་བཀོད།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupsubtitle.ui" line="58"/>
        <source>Auto loading other subtitles in the same folder</source>
        <translation>ཡིག་སྣོད་གཅིག་གི་ནང་དུ་ཡིག་བརྙན་གཞན་པ་རང་འགུལ་གྱིས་ནང་འཇུག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupsubtitle.ui" line="85"/>
        <source>Sub Path</source>
        <translation>ལམ་བུ་གཏན་འཁེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupsubtitle.ui" line="108"/>
        <source>browse</source>
        <translation>བཤར་ཆས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupsubtitle.ui" line="117"/>
        <source>Font Style</source>
        <translation>ཡིག་གཟུགས་ཀྱི་ཉམས་འགྱུར།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupsubtitle.ui" line="141"/>
        <source>Family</source>
        <translation>ཡིག་གཟུགས་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupsubtitle.ui" line="190"/>
        <source>Size</source>
        <translation>ཡིག་གཟུགས་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupsubtitle.cpp" line="168"/>
        <source>Video Player Choose a directory</source>
        <translation>བརྙན་ཕབ་འཕྲུལ་འཁོར་གྱིས་དཀར་ཆག་འདེམས་པ།</translation>
    </message>
</context>
<context>
    <name>SetupSystem</name>
    <message>
        <location filename="../setupwidget/setupsystem.ui" line="14"/>
        <source>Form</source>
        <translation>རིགས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupsystem.ui" line="38"/>
        <source>Window</source>
        <translation>སྒེའུ་ཁུང་།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupsystem.ui" line="54"/>
        <source>Minimize to system tray icon</source>
        <translation>ཆེས་ཆུང་བ་དེ་མ་ལག་གི་སྡེར་མའི་དཔེ་རིས་སུ་བསྒྱུར།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupsystem.ui" line="67"/>
        <source>Pause video playback when minimized</source>
        <translation>ཆེས་ཆུང་བའི་དུས་སུ་བརྙན་ཕབ་ཕྱིར་ལྡོག་བྱེད་མཚམས་འཇོག་དགོས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupsystem.ui" line="80"/>
        <source>After sleep/sleep/lock screen and wake up, keep playing state</source>
        <translation>གཉིད་ཉལ་བ་།་གཉིད་ཉལ་བ་།ཟྭ་བརྒྱབ་ནས་སད་རྗེས་གཏོང་བའི་རྣམ་པ་རྒྱུན་འཁྱོངས་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupsystem.ui" line="99"/>
        <source>Multiple run</source>
        <translation>ཐེངས་མང་པོར་འཁོར་སྐྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupsystem.ui" line="115"/>
        <source>Allow multiple Kylin Video to run simultaneously</source>
        <translation>དུས་མཚུངས་སུ་གློག་བརྙན་མང་པོ་འཁོར་སྐྱོད་བྱས་ཆོག</translation>
    </message>
</context>
<context>
    <name>SetupVolume</name>
    <message>
        <location filename="../setupwidget/setupvolume.ui" line="14"/>
        <source>Form</source>
        <translation>རིགས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupvolume.ui" line="29"/>
        <source>Output driver</source>
        <translation>ཕྱིར་གཏོང་སྒུལ་ཤུགས།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupvolume.ui" line="69"/>
        <source>Volume contral</source>
        <translation>སྒྲ་གདངས་སྙོམས་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupvolume.ui" line="82"/>
        <source>Global volume</source>
        <translation>ཁྱོན་ཡོངས་ཀྱི་སྒྲ་ཚད།</translation>
    </message>
    <message>
        <location filename="../setupwidget/setupvolume.ui" line="95"/>
        <source>Default volume standardization</source>
        <translation>སྒྲ་གདངས་ཚད་ལྡན་ཅན་དུ་ཁས་བླངས།</translation>
    </message>
</context>
<context>
    <name>ShortCutItem</name>
    <message>
        <location filename="../setupwidget/setupshortcut.cpp" line="459"/>
        <source>Hotkey conflict</source>
        <translation>ཚ་མཐེབ་གདོང་གཏུག</translation>
    </message>
</context>
<context>
    <name>ShortCutSetting</name>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="43"/>
        <source>help documentation</source>
        <translation>རོགས་རམ་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="44"/>
        <source>exit</source>
        <translation>ཕྱིར་འཐེན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="46"/>
        <source>open file</source>
        <translation>ཁ་ཕྱེ་བའི་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="47"/>
        <source>open dir</source>
        <translation>ཡིག་སྣོད་ཁ་ཕྱེས་པ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="48"/>
        <source>prev file</source>
        <translation>ཡིག་ཆ་སྔོན་མ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="49"/>
        <source>next file</source>
        <translation>རྗེས་མའི་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="52"/>
        <source>play/pause</source>
        <translation>གཏོང་བ་དང་མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="53"/>
        <source>speed up</source>
        <translation>མྱུར་ཚད་ཇེ་མགྱོགས་སུ་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="54"/>
        <source>speed down</source>
        <translation>དལ་མོར་གཏོང་།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="55"/>
        <source>speed normal</source>
        <translation>རྒྱུན་ལྡན་གྱི་མྱུར་ཚད་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="56"/>
        <source>forword</source>
        <translation>མྱུར་འཛུལ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="57"/>
        <source>backword</source>
        <translation>དལ་མོར་ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="58"/>
        <source>forward 30s</source>
        <translation>30སྐར་ཆ་མདུན་སྐྱོད།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="59"/>
        <source>backword 30s</source>
        <translation>སྐར་ཆ་30་མགྱོགས་པོར་ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="60"/>
        <source>insert bookmark</source>
        <translation>དཔེ་རྟགས་ནང་དུ་བཅུག</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="61"/>
        <source>ib notes</source>
        <translation>ནང་འཇུག་དང་མཆན་འགྲེལ་དཔེ་རྟགས།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="64"/>
        <source>fullscreen</source>
        <translation>བརྙན་ཤེལ་ཧྲིལ་བོ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="65"/>
        <source>mini mode</source>
        <translation>miniབྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="66"/>
        <source>to top</source>
        <translation>གོང་བཞག</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="67"/>
        <source>screenshot</source>
        <translation>པར་རིས་འདྲ་བ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="68"/>
        <source>cut</source>
        <translation>དྲས་གཏུབ་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="69"/>
        <source>light up</source>
        <translation>གསལ་ཚད་ཇེ་ཆེར་བཏང་།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="70"/>
        <source>light down</source>
        <translation>གསལ་ཚད་ཇེ་ཆུང་དུ་བཏང་།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="71"/>
        <source>forward rotate</source>
        <translation>དུས་ཚོད་ཀྱི་འཁོར་ཕྱོགས་དང་བསྟུན་ནས་ཏུའུ་༩༠འཁོར།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="72"/>
        <source>backward rotate</source>
        <translation>དུས་ཚོད་ཀྱི་འཁོར་ཕྱོགས་དང་ལྡོག་ནས་ཏུའུ་༩༠འཁོར།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="73"/>
        <source>horizontal flip</source>
        <translation>ཆུ་ཚད་ཕྱིར་འཁོར།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="74"/>
        <source>vertical flip</source>
        <translation>གཞུང་ཕྱོགས་ནས་ཕར་འགྲོ་ཚུར་འོང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="75"/>
        <source>image boost</source>
        <translation>པར་རིས་ལ་སྐུལ་འདེད།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="78"/>
        <source>volume up</source>
        <translation>སྒྲ་ཇེ་མཐོར་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="79"/>
        <source>volume down</source>
        <translation>སྒྲ་ཇེ་དམའ་རུ་གཏོང་།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="80"/>
        <source>mute</source>
        <translation>སྒྲ་མེད།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="81"/>
        <source>audio next</source>
        <translation>སྒྲ་ལམ་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="82"/>
        <source>default channel</source>
        <translation>དང་ཐོག་གི་སྒྲ་ལམ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="83"/>
        <source>left channel</source>
        <translation>གཡོན་ཕྱོགས་ཀྱི་སྒྲ་ལམ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="84"/>
        <source>right channel</source>
        <translation>གཡས་ཕྱོགས་ཀྱི་སྒྲ་ལམ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="87"/>
        <source>sub load</source>
        <translation>ལག་པ་འགུལ་ནས་ཡི་གེ་སྣོན་པ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="90"/>
        <location filename="../global/shortcutsetting.cpp" line="95"/>
        <source>sub earlier</source>
        <translation>ཡིག་བརྙན་སྐར་ཆ་0.5་ཡི་སྔོན་ལ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="91"/>
        <location filename="../global/shortcutsetting.cpp" line="96"/>
        <source>sub later</source>
        <translation>ཡིག་བརྙན་སྐར་ཆ་0.5་ཡི་རྗེས་ལ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="98"/>
        <source>sub up</source>
        <translation>ཡི་གེ་གོང་དུ་སྤོ་བ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="99"/>
        <source>sub down</source>
        <translation>ཡི་གེ་འོག་ཏུ་སྤོ་བ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="100"/>
        <source>sub next</source>
        <translation>ཡི་གེ་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="103"/>
        <source>play list</source>
        <translation>རེའུ་མིག་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../global/shortcutsetting.cpp" line="104"/>
        <source>setup</source>
        <translation>སྒྲིག་བཀོད་གཏོང་ཆས།</translation>
    </message>
</context>
<context>
    <name>SystemTrayIcon</name>
    <message>
        <location filename="../systemtrayicon.cpp" line="11"/>
        <source>Video Player</source>
        <translation>བརྙན་ཕབ་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../systemtrayicon.cpp" line="27"/>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན་བྱ་རྒྱུ།</translation>
    </message>
</context>
<context>
    <name>TitleMenu</name>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1029"/>
        <source>Upload to cloud</source>
        <translation>སྤྲིན་སྣེ་ལ་བསྐུར།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1033"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1037"/>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན་བྱ་རྒྱུ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1047"/>
        <source>Theme</source>
        <translation>བརྗོད་བྱ་གཙོ་བོ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1055"/>
        <source>Follow system</source>
        <translation>རྗེས་འདེད་མ་ལག</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1063"/>
        <source>Light theme</source>
        <translation>མདོག་སྐྱ་བོའི་ཁ་བྱང་།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1071"/>
        <source>Black theme</source>
        <translation>མདོག་ཟབ་པའི་བརྗོད་གཞི།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1100"/>
        <source>Privacy</source>
        <translation>སྒེར་གྱི་གསང་དོན།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1106"/>
        <source>Clear mark</source>
        <translation>གསལ་པོར་རྟགས་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1116"/>
        <source>No mark</source>
        <translation>རྟགས་མེད་པར་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1130"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1134"/>
        <source>Check update</source>
        <translation>ཞིབ་བཤེར་གསར་སྒྱུར།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1147"/>
        <source>Manual</source>
        <translation>རོགས་རམ་ལག་དེབ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1155"/>
        <source>Advice and feedback</source>
        <translation>བསམ་འཆར་དང་ལྡོག་འདྲེན།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1161"/>
        <source>Official website</source>
        <translation>གཞུང་ཕྱོགས་ཀྱི་དྲ་ཚིགས།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1176"/>
        <source>Setup</source>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1180"/>
        <source>System setup</source>
        <translation>མ་ལག་སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1184"/>
        <source>Play setup</source>
        <translation>སྒྲིག་བཀོད་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1188"/>
        <source>Screenshot setup</source>
        <translation>པར་རིས་སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1192"/>
        <source>Subtitle setup</source>
        <translation>ཡིག་བརྙན་གྱི་སྒྲིག་གཞི།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1196"/>
        <source>Audio setup</source>
        <translation>སྒྲ་ཕབ་སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1200"/>
        <source>Decoder setup</source>
        <translation>སྒྲིག་ཆས་སྒྲིག་འཛུགས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../kwidget/kmenu.cpp" line="1204"/>
        <source>Shortcut setup</source>
        <translation>མགྱོགས་མཐེབ་སྒྲིག་འགོད།</translation>
    </message>
</context>
<context>
    <name>TitleWidget</name>
    <message>
        <location filename="../titlewidget.cpp" line="54"/>
        <source>Restore</source>
        <translation>སླར་གསོ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../titlewidget.cpp" line="59"/>
        <location filename="../titlewidget.cpp" line="166"/>
        <source>Maximize</source>
        <translation>ཆེས་ཆེར་བསྒྱུར།</translation>
    </message>
    <message>
        <location filename="../titlewidget.cpp" line="122"/>
        <source>Video Player</source>
        <translation>བརྙན་ཕབ་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../titlewidget.cpp" line="134"/>
        <source>Menu</source>
        <translation>འདེམས་པང་།</translation>
    </message>
    <message>
        <location filename="../titlewidget.cpp" line="146"/>
        <source>Mini mode</source>
        <translation>ཆུང་གྲས་ཀྱི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../titlewidget.cpp" line="156"/>
        <source>Minimize</source>
        <translation>ཆེས་ཆུང་བསྒྱུར།</translation>
    </message>
    <message>
        <location filename="../titlewidget.cpp" line="176"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>TopWindow</name>
    <message>
        <location filename="../topwindow.cpp" line="232"/>
        <location filename="../topwindow.cpp" line="355"/>
        <source>Video Player</source>
        <translation>བརྙན་ཕབ་འཕྲུལ་འཁོར།</translation>
    </message>
</context>
<context>
    <name>WaylandDialog</name>
    <message>
        <location filename="../ukui-wayland/waylanddialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>ཁ་བརྡའི་སྒྲོམ་གཞི་འཛུགས་པ།</translation>
    </message>
</context>
</TS>
