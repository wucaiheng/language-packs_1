<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AboutWidget</name>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>VERSION</source>
        <translation type="vanished">版本号</translation>
    </message>
</context>
<context>
    <name>BatteryInfo</name>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="234"/>
        <source>Model</source>
        <translation>ᠳ᠋ᠢᠶᠠᠨ ᡂᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <source>soc</source>
        <translation type="vanished">剩余电量</translation>
    </message>
    <message>
        <source>Estimated Service Time</source>
        <translation type="vanished">预计使用时间</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="147"/>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="173"/>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="222"/>
        <source>Battery</source>
        <translation>ᠳ᠋ᠢᠶᠠᠨ ᡂᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="226"/>
        <source>Serail Number</source>
        <translation>ᠴᠤᠪᠤᠷᠠᠯ᠎ᠤᠨ ᠨᠤᠮᠸᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="222"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="230"/>
        <source>Manufacturer</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="238"/>
        <source>State</source>
        <translation>ᠳᠦᠯᠦᠪ ᠪᠠᠢᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="242"/>
        <source>Percentage</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ᠎ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="246"/>
        <source>Energy</source>
        <translation>ᠪᠠᠭ᠍ᠳᠠᠭᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="250"/>
        <source>Energy Full</source>
        <translation>ᠪᠠᠭ᠍ᠳᠠᠭᠠᠮᠵᠢ᠎ᠶ᠋ᠢ ᠪᠦᠷᠢᠨ ᠳᠦᠬᠦᠷᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="254"/>
        <source>Time To Empty</source>
        <translation>ᠤᠷᠢᠳᠴᠢᠯᠠᠭ᠍ᠰᠠᠨ ᠬᠡᠷᠡᠭ᠌ᠯᠡᠬᠦ᠌ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="258"/>
        <source>Used Times</source>
        <translation>ᠬᠡᠷᠡᠭ᠌ᠯᠡᠬᠦ᠌ ᠤᠳᠠᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>BluetoothInfo</name>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="145"/>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="171"/>
        <source>Bluetooth</source>
        <translation>ᠬᠦᠬᠡ ᠱᠢᠳᠦ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="220"/>
        <source>Bus Address</source>
        <translation>ᠪᠠᠰ᠎ᠤᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="224"/>
        <source>Function</source>
        <translation>ᠴᠢᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="228"/>
        <source>Frequency</source>
        <translation>ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="232"/>
        <source>Configuration</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="236"/>
        <source>Type</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠳᠦᠷᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="240"/>
        <source>ID</source>
        <translation>IDᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="244"/>
        <source>Model</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="248"/>
        <source>Resource</source>
        <translation>ᠡᠬᠢ ᠪᠠᠶᠠᠯᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="252"/>
        <source>Manufacturer</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="256"/>
        <source>Version</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="260"/>
        <source>Data Width</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ᠎ᠶ᠋ᠢᠨ ᠦᠷᠬᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="264"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="268"/>
        <source>Driver</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="272"/>
        <source>Speed</source>
        <translation>ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="276"/>
        <source>Serial Number</source>
        <translation type="unfinished">ᠴᠤᠪᠤᠷᠠᠯ᠎ᠤᠨ ᠨᠤᠮᠸᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="280"/>
        <source>Address</source>
        <translation>ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="284"/>
        <source>Link Mode</source>
        <translation>ᠵᠠᠯᠭᠠᠬᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="288"/>
        <source>Link Policy</source>
        <translation>ᠵᠠᠯᠭᠠᠬᠤ ᠠᠷᠭ᠎ᠠ ᠪᠤᠳᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="292"/>
        <source>Capabilities</source>
        <translation>ᠴᠢᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="296"/>
        <source>Bus</source>
        <translation>ᠪᠠᠰ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="300"/>
        <source>SCO MTU</source>
        <translation>SCO ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠶᠡᠬᠡ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠨᠢᠬᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="304"/>
        <source>ACL MTU</source>
        <translation>ACL ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠶᠡᠬᠡ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠨᠢᠬᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="308"/>
        <source>Packet Type</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ᠎ᠶ᠋ᠢᠨ ᠪᠠᠭ᠍ᠯᠠᠭ᠎ᠠ ᠎ᠶ᠋ᠢᠨ ᠳᠦᠷᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="312"/>
        <source>Features</source>
        <translation>ᠤᠨᠴᠠᠯᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="333"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation type="unfinished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠤᠷᠤᠱᠢᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠤᠯᠵᠤ ᠠᠪᠤᠭ᠍ᠰᠠᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠵᠠᠩᠬᠢ ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
</context>
<context>
    <name>CDRomInfo</name>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="148"/>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="174"/>
        <source>CD-ROM</source>
        <translation>ᠬᠡᠷᠡᠯ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="223"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="227"/>
        <source>Manufacturer</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="231"/>
        <source>Version</source>
        <translation>ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="235"/>
        <source>Model</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ᠎ᠶ᠋ᠢᠨ ᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="239"/>
        <source>Serail Number</source>
        <translation type="unfinished">ᠴᠤᠪᠤᠷᠠᠯ᠎ᠤᠨ ᠨᠤᠮᠸᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="243"/>
        <source>Bus Info</source>
        <translation>ᠪᠠᠰ᠎ᠤᠨ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="247"/>
        <source>Driver</source>
        <translation type="unfinished">ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="251"/>
        <source>Speed</source>
        <translation type="unfinished">ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="255"/>
        <source>Device Number</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠳ᠋ᠤᠭᠠᠷᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="276"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠤᠷᠤᠱᠢᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠤᠯᠵᠤ ᠠᠪᠤᠭ᠍ᠰᠠᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠵᠠᠩᠬᠢ ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
</context>
<context>
    <name>CameraInfo</name>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation type="unfinished">ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="144"/>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="170"/>
        <source>Camera</source>
        <translation>ᠺᠡᠮᠸᠷᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="219"/>
        <source>Name</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="223"/>
        <source>Resolution</source>
        <translation>ᠢᠯᠭᠠᠮᠵᠢ ᠵᠢᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="227"/>
        <source>Manufacturer</source>
        <translation type="unfinished">ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="231"/>
        <source>Model</source>
        <translation type="unfinished">ᠬᠡᠯᠪᠡᠷᠢ᠎ᠶ᠋ᠢᠨ ᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="235"/>
        <source>Interface</source>
        <translation>ᠵᠠᠯᠭᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="239"/>
        <source>Driver</source>
        <translation type="unfinished">ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="243"/>
        <source>Type</source>
        <translation>ᠳᠦᠷᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="247"/>
        <source>Version</source>
        <translation type="unfinished">ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="251"/>
        <source>Bus Info</source>
        <translation type="unfinished">ᠪᠠᠰ᠎ᠤᠨ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="255"/>
        <source>Speed</source>
        <translation type="unfinished">ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
</context>
<context>
    <name>CpuFMPage</name>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmpage.cpp" line="79"/>
        <source>Current CPU frequency</source>
        <translation>CPU ᠤᠳᠤᠬᠠᠨ᠎ᠤ ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmpage.cpp" line="101"/>
        <source>Current average CPU core frequency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmpage.cpp" line="111"/>
        <source>CPU FM Note: The CPU FM function has some risks, please use it carefully! After FM is completed, restarting will restore the default configuration!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmpage.cpp" line="151"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation type="unfinished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠤᠷᠤᠱᠢᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠤᠯᠵᠤ ᠠᠪᠤᠭ᠍ᠰᠠᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠵᠠᠩᠬᠢ ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
</context>
<context>
    <name>CpuFMSetWidget</name>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="87"/>
        <source>CPU Management Strategy</source>
        <translation>CPU ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠳᠠ᠎ᠶ᠋ᠢᠨ ᠠᠷᠭ᠎ᠠ ᠪᠤᠳᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="95"/>
        <source>performance</source>
        <translation>ᠴᠢᠳᠠᠪᠬᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠪ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="102"/>
        <source>powersave</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠠᠷᠪᠢᠯᠠᠬᠤ ᠬᠡᠪ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="109"/>
        <source>userspace</source>
        <translation>ᠳᠤᠷ᠎ᠠ᠎ᠪᠠᠷ ᠳᠤᠭ᠍ᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="116"/>
        <source>schedutil</source>
        <translation>ᠵᠢᠭ᠌ᠳᠡ ᠬᠡᠪ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="123"/>
        <source>ondemand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="130"/>
        <source>conservative</source>
        <translation>ᠬᠡᠪᠱᠢᠮᠡᠯ ᠬᠡᠪ</translation>
    </message>
</context>
<context>
    <name>DeviceMonitorPage</name>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="49"/>
        <source>The equipment is normal and the heat dissipation is good</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠬᠡᠪ᠎ᠦᠨ᠂ ᠢᠯᠴᠢ ᠳᠠᠷᠬᠠᠭᠠᠬᠤ ᠪᠠᠢᠳᠠᠯ ᠰᠠᠢᠨ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="50"/>
        <source>Equipment temperature is high, please pay attention to heat dissipation</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠳᠤᠯᠠᠭᠠᠨ᠎ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠨᠡᠯᠢᠶᠡᠳ ᠦᠨᠳᠦᠷ᠂ ᠢᠯᠴᠢ ᠳᠠᠷᠬᠠᠭᠠᠬᠤ᠎ᠶ᠋ᠢ ᠠᠩᠬᠠᠷᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="51"/>
        <source>Equipment temperature is abnormal, please pay attention to heat dissipation</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠳᠤᠯᠠᠭᠠᠨ᠎ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠬᠡᠪ᠎ᠦᠨ ᠪᠤᠰᠤ᠂ ᠢᠯᠴᠢ ᠳᠠᠷᠬᠠᠭᠠᠬᠤ᠎ᠶ᠋ᠢ ᠠᠩᠬᠠᠷᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="176"/>
        <source>CPU Temp</source>
        <translation>CPU ᠳᠤᠯᠠᠭᠠᠨ᠎ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="182"/>
        <source>HARDDISK Temp</source>
        <translation>ᠬᠠᠳᠠᠭᠤ ᠳ᠋ᠢᠰᠺ ᠳᠤᠯᠠᠭᠠᠨ᠎ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="188"/>
        <source>GPU Temp</source>
        <translation type="unfinished">CPU ᠳᠤᠯᠠᠭᠠᠨ᠎ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="194"/>
        <source>DEV Temp</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠳᠤᠯᠠᠭᠠᠨ᠎ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="223"/>
        <source>DEV Usage</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠷᠡᠭ᠌ᠯᠡᠬᠡᠨ᠎ᠦ ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <source>BaseBoard Temp</source>
        <translation type="vanished">主板温度</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="211"/>
        <source>CPU Usage</source>
        <translation>CPU ᠬᠡᠷᠡᠭ᠌ᠯᠡᠭᠡᠨ᠎ᠦ ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="217"/>
        <source>Memory Usage</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠷᠡᠭ᠌ᠯᠡᠬᠡᠨ᠎ᠦ ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
</context>
<context>
    <name>DriveInfoPage</name>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="69"/>
        <source>CopyAll</source>
        <translation type="unfinished">ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="124"/>
        <source>Motherboard</source>
        <translation>ᠭᠤᠤᠯ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="128"/>
        <source>Graphics Card</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ᠎ᠶ᠋ᠢᠨ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="132"/>
        <source>Wired Network Card</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="136"/>
        <source>Sound Card</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="140"/>
        <source>Wireless Network Card</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="146"/>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="150"/>
        <source>Other</source>
        <translation>ᠪᠤᠰᠤᠳ</translation>
    </message>
</context>
<context>
    <name>DriveManage</name>
    <message>
        <location filename="../../plugins/drivemanage/drivemanage.cpp" line="43"/>
        <source>DriveManager</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ᠎ᠦᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠳᠠ</translation>
    </message>
</context>
<context>
    <name>FanInfo</name>
    <message>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation type="unfinished">ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="148"/>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="174"/>
        <source>Fan</source>
        <translation>ᠰᠡᠩᠰᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="223"/>
        <source>Speed</source>
        <translation>ᠡᠷᠭᠢᠯᠳᠡ᠎ᠶ᠋ᠢᠨ ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="244"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation type="unfinished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠤᠷᠤᠱᠢᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠤᠯᠵᠤ ᠠᠪᠤᠭ᠍ᠰᠠᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠵᠠᠩᠬᠢ ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
</context>
<context>
    <name>GraphicsCardInfo</name>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="71"/>
        <source>CopyAll</source>
        <translation type="unfinished">ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="156"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="182"/>
        <source>Graphics Card</source>
        <translation type="unfinished">ᠳᠡᠯᠭᠡᠴᠡ᠎ᠶ᠋ᠢᠨ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="231"/>
        <source>Manufacturer</source>
        <translation type="unfinished">ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="235"/>
        <source>SubSystem</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="239"/>
        <source>Name</source>
        <translation type="unfinished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="243"/>
        <source>IsDiscrete</source>
        <translation>ᠳᠤᠰᠠᠭᠠᠷ ᠳᠡᠯᠭᠡᠴᠡᠨ᠎ᠦ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="247"/>
        <source>Memory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="260"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="263"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="267"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="271"/>
        <source>Video Memory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="276"/>
        <source>Memory Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="280"/>
        <source>Model</source>
        <translation type="unfinished">ᠬᠡᠯᠪᠡᠷᠢ᠎ᠶ᠋ᠢᠨ ᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="284"/>
        <source>Version</source>
        <translation type="unfinished">ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="288"/>
        <source>Bit Width</source>
        <translation type="unfinished">ᠳ᠋ᠠᠢᠲ᠋ᠠ᠎ᠶ᠋ᠢᠨ ᠤᠷᠤᠨ᠎ᠤ ᠦᠷᠬᠡᠨ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="292"/>
        <source>Funtion</source>
        <translation>ᠴᠢᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="296"/>
        <source>Clock</source>
        <translation>ᠴᠠᠭ᠎ᠤᠨ ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="300"/>
        <source>Driver</source>
        <translation type="unfinished">ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="304"/>
        <source>Dbus Info</source>
        <translation type="unfinished">ᠪᠠᠰ᠎ᠤᠨ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="308"/>
        <source>ID</source>
        <translation>ID ᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="312"/>
        <source>Device</source>
        <translation type="unfinished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="316"/>
        <source>GDDR Capacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="320"/>
        <source>EGL Version</source>
        <translation>EGL ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="324"/>
        <source>EGL Client APIs</source>
        <translation>EGL ᠵᠠᠯᠭᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="328"/>
        <source>GL Version</source>
        <translation>GL ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="332"/>
        <source>GLSL Version</source>
        <translation>GLSL ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="353"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation type="unfinished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠤᠷᠤᠱᠢᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠤᠯᠵᠤ ᠠᠪᠤᠭ᠍ᠰᠠᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠵᠠᠩᠬᠢ ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
</context>
<context>
    <name>HWMonitorInfo</name>
    <message>
        <location filename="../../plugins/hwmonitor/hwmonitor.cpp" line="32"/>
        <source>HardwareMonitor</source>
        <translation>ᠬᠠᠷᠳ᠋ᠸᠡᠢᠷ᠎ᠢ ᠬᠢᠨᠠᠨ ᠱᠢᠯᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>HWMonitorWidget</name>
    <message>
        <source>Device Monitor</source>
        <translation type="vanished">设备监测</translation>
    </message>
    <message>
        <source>CPU FM</source>
        <translation type="vanished">CPU调频</translation>
    </message>
</context>
<context>
    <name>HWParam</name>
    <message>
        <location filename="../../plugins/hwparam/hwparam.cpp" line="34"/>
        <source>HardwareParam</source>
        <translation>ᠬᠠᠷᠳ᠋ᠸᠡᠢᠷ᠎ᠤᠨ ᠫᠠᠷᠠᠮᠸᠲᠷ</translation>
    </message>
</context>
<context>
    <name>HardDiskInfo</name>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation type="unfinished">ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="147"/>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="173"/>
        <source>Hard Disk Info</source>
        <translation>ᠬᠠᠳᠠᠭᠤ ᠳ᠋ᠢᠰᠺ᠎ᠦᠨ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="222"/>
        <source>Manufacturer</source>
        <translation type="unfinished">ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="226"/>
        <source>Name</source>
        <translation>ᠬᠠᠳᠠᠭᠤ ᠳ᠋ᠢᠰᠺ᠎ᠦᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="230"/>
        <source>Capacity</source>
        <translation>ᠪᠠᠭ᠍ᠳᠠᠭᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="234"/>
        <source>Used Times</source>
        <translation>ᠬᠡᠷᠡᠭ᠌ᠯᠡᠭ᠌ᠰᠡᠨ ᠤᠳᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="238"/>
        <source>Interface</source>
        <translation type="unfinished">ᠵᠠᠯᠭᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="244"/>
        <source>Yes</source>
        <translation>ᠮᠦᠨ（ᠳᠡᠢᠮᠤ）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="246"/>
        <source>No</source>
        <translation>ᠪᠢᠰᠢ（ᠦᠬᠡᠢ）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="248"/>
        <source>Main Disk</source>
        <translation>ᠭᠤᠤᠯ ᠬᠠᠳᠠᠭᠤ ᠳ᠋ᠢᠰᠺ（ᠮᠦᠨ/ ᠪᠢᠰᠢ）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="254"/>
        <source>SSD</source>
        <translation>ᠬᠠᠳᠠᠭᠤ ᠳᠦᠯᠦᠪ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="256"/>
        <source>HDD</source>
        <translation>ᠮᠸᠬᠠᠨᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="258"/>
        <source>Type</source>
        <translation>ᠳᠦᠷᠦᠯ（ᠬᠠᠳᠠᠭᠤ ᠳᠦᠯᠦᠪ/ ᠮᠸᠬᠠᠨᠢᠭ）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="262"/>
        <source>Serial Num</source>
        <translation>ᠴᠤᠪᠤᠷᠠᠯ᠎ᠤᠨ ᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="266"/>
        <source>Model</source>
        <translation type="unfinished">ᠬᠡᠯᠪᠡᠷᠢ᠎ᠶ᠋ᠢᠨ ᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="270"/>
        <source>Transfer Rate</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="274"/>
        <source>Read Speed</source>
        <translation>ᠳ᠋ᠢᠰᠺ ᠤᠩᠱᠢᠬᠤ ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="278"/>
        <source>Write Speed</source>
        <translation>ᠳ᠋ᠢᠰᠺ᠎ᠲᠦ ᠪᠢᠴᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="282"/>
        <source>Firmware Version</source>
        <translation>ᠬᠠᠳᠠᠭᠤ ᠳᠤᠨᠤᠭ᠍ᠯᠠᠯ᠎ᠤᠨ ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="303"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation type="unfinished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠤᠷᠤᠱᠢᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠤᠯᠵᠤ ᠠᠪᠤᠭ᠍ᠰᠠᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠵᠠᠩᠬᠢ ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
</context>
<context>
    <name>InfoPage</name>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="183"/>
        <source>Manufacturer</source>
        <translation>ᠪᠦᠬᠦᠯᠢ ᠮᠠᠱᠢᠨ᠎ᠤ ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <source>MachineModel</source>
        <translation type="obsolete">整机型号</translation>
    </message>
    <message>
        <source>SerialNum</source>
        <translation type="obsolete">序列号</translation>
    </message>
    <message>
        <source>SystemBits</source>
        <translation type="obsolete">系统位数</translation>
    </message>
    <message>
        <source>KernelArch</source>
        <translation type="obsolete">内核架构</translation>
    </message>
    <message>
        <source>HostName</source>
        <translation type="obsolete">主机名</translation>
    </message>
    <message>
        <source>OSVersion</source>
        <translation type="obsolete">操作系统版本</translation>
    </message>
    <message>
        <source>KernelVersion</source>
        <translation type="obsolete">内核版本</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="69"/>
        <source>CopyAll</source>
        <translation type="unfinished">ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="189"/>
        <source>Machine Model</source>
        <translation>ᠪᠦᠬᠦᠯᠢ ᠮᠠᠱᠢᠨ᠎ᠤ ᠬᠡᠯᠪᠡᠷᠢ᠎ᠶ᠋ᠢᠨ ᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="195"/>
        <source>Serial Number</source>
        <translation type="unfinished">ᠴᠤᠪᠤᠷᠠᠯ᠎ᠤᠨ ᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="201"/>
        <source>System Bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="207"/>
        <source>Kernel Arch</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ ᠴᠦᠮ᠎ᠡ᠎ᠶ᠋ᠢᠨ ᠪᠦᠷᠢᠯᠳᠦᠴᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="213"/>
        <source>Host Name</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠱᠢᠨ᠎ᠤ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="219"/>
        <source>OS Version</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠬᠤ ᠱᠢᠰᠲᠸᠮ᠎ᠦᠨ ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="225"/>
        <source>Kernel Version</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ ᠴᠦᠮ᠎ᠡ᠎ᠶ᠋ᠢᠨ ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="231"/>
        <source>Processor</source>
        <translation>ᠱᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="237"/>
        <source>Memory</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="243"/>
        <source>Mother Board</source>
        <translation>ᠭᠤᠤᠯ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="249"/>
        <source>Hard Disk</source>
        <translation>ᠬᠠᠳᠠᠭᠤ ᠳ᠋ᠢᠰᠺ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="255"/>
        <source>Graphics Card</source>
        <translation type="unfinished">ᠳᠡᠯᠭᠡᠴᠡ᠎ᠶ᠋ᠢᠨ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="267"/>
        <source>Network Card</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="273"/>
        <source>Sound Card</source>
        <translation type="unfinished">ᠳᠠᠭᠤᠨ᠎ᠤ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <source>MainBoard</source>
        <translation type="obsolete">主板</translation>
    </message>
    <message>
        <source>HardDisk</source>
        <translation type="obsolete">硬盘</translation>
    </message>
    <message>
        <source>GraphicsCard</source>
        <translation type="obsolete">显卡</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="261"/>
        <source>Monitor</source>
        <translation>ᠳᠡᠯᠬᠡᠴᠡ</translation>
    </message>
    <message>
        <source>NetworkCard</source>
        <translation type="obsolete">网卡</translation>
    </message>
    <message>
        <source>SoundCard</source>
        <translation type="obsolete">声卡</translation>
    </message>
</context>
<context>
    <name>KAAboutDialog</name>
    <message>
        <location filename="../kaaboutdialog.cpp" line="117"/>
        <location filename="../kaaboutdialog.cpp" line="143"/>
        <source>ToolKit</source>
        <translation>ᠪᠠᠭᠠᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../kaaboutdialog.cpp" line="148"/>
        <source>VERSION:  </source>
        <translation>ᠬᠡᠪᠯᠡᠯ᠎ᠦᠨ ᠳ᠋ᠤᠭᠠᠷ:  </translation>
    </message>
    <message>
        <location filename="../kaaboutdialog.cpp" line="153"/>
        <source>ToolKit provides some extended functions and users can query the hardware details of the current computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ToolKit is a powerful system supporting software which is developed by Kylin team. Mainly for the naive user, it can help users manage the system. At present, It provides system junk scanning and cleaning, viewing the system hardware and software information, task manager, and some other functions.</source>
        <translation type="obsolete">工具箱是由麒麟团队开发的一款功能强大的系统辅助软件。主要面向初级用户，能够帮助用户对系统进行管理。目前已具备了系统垃圾扫描与清理、系统软硬件信息查看、系统全方位定制、任务管理器等功能。</translation>
    </message>
    <message>
        <location filename="../kaaboutdialog.cpp" line="177"/>
        <source>Service &amp; Support :</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠬᠢᠬᠡᠳ ᠳᠡᠮᠵᠢᠯᠬᠡ:</translation>
    </message>
</context>
<context>
    <name>KALabel</name>
    <message>
        <source>Copy</source>
        <translation type="vanished">复制</translation>
    </message>
</context>
<context>
    <name>KAUsageItem</name>
    <message>
        <location filename="../../plugins/hwmonitor/kausageitem.cpp" line="64"/>
        <source>Used</source>
        <translation>ᠨᠢᠬᠡᠨᠳᠡ ᠬᠡᠷᠡᠭ᠌ᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/kausageitem.cpp" line="66"/>
        <source>Left</source>
        <translation>ᠬᠡᠷᠡᠭ᠌ᠯᠡᠵᠦ᠍ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>KDriveInfoItem</name>
    <message>
        <source>Copy</source>
        <translation type="vanished">复制</translation>
    </message>
</context>
<context>
    <name>KInfoListItem</name>
    <message>
        <source>Copy</source>
        <translation type="obsolete">复制</translation>
    </message>
</context>
<context>
    <name>KLeftWidget</name>
    <message>
        <location filename="../kleftwidget.cpp" line="64"/>
        <source>ToolKit</source>
        <translation type="unfinished">ᠪᠠᠭᠠᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ</translation>
    </message>
</context>
<context>
    <name>KRightWidget</name>
    <message>
        <location filename="../krightwidget.cpp" line="52"/>
        <source>menu</source>
        <translation type="unfinished">ᠲᠤᠪᠶᠤᠭ</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="58"/>
        <source>minimize</source>
        <translation type="unfinished">ᠬᠠᠮᠤᠭ ᠎ᠤᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="64"/>
        <source>close</source>
        <translation type="unfinished">ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="75"/>
        <location filename="../krightwidget.cpp" line="89"/>
        <source>Help</source>
        <translation>ᠬᠠᠪᠰᠤᠷᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="76"/>
        <location filename="../krightwidget.cpp" line="83"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="77"/>
        <location filename="../krightwidget.cpp" line="96"/>
        <source>Exit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <source>ToolKit</source>
        <translation type="vanished">工具箱</translation>
    </message>
    <message>
        <source>&lt;p&gt;ToolKit is a powerful system supporting software which is developed by Kylin team. Mainly for the naive user, it can help users manage the system. At present, It provides system junk scanning and cleaning, viewing the system hardware and software information, task manager, and some other functions.&lt;/p&gt;</source>
        <translation type="vanished"> &lt;p&gt;工具箱是由麒麟团队开发的一款功能强大的系统辅助软件。主要面向初级用户，能够帮助用户对系统进行管理。目前已具备了系统垃圾扫描与清理、系统软硬件信息查看、系统全方位定制、任务管理器等功能。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Service &amp; Support : &lt;a style=&apos;color: black;&apos; href=&apos;mailto:support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;服务和支持: &lt;a style=&apos;color: black;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>KeyboardInfo</name>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="148"/>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="174"/>
        <source>Keyboard</source>
        <translation>ᠳᠠᠷᠤᠭᠤᠯ</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">设备类型</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation type="unfinished">ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="223"/>
        <source>Name</source>
        <translation type="unfinished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="231"/>
        <source>Model</source>
        <translation type="unfinished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="235"/>
        <source>Manufacurer</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="239"/>
        <source>Interface</source>
        <translation type="unfinished">ᠵᠠᠯᠭᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="243"/>
        <source>Driver</source>
        <translation type="unfinished">ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="247"/>
        <source>Device Address</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="268"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation type="unfinished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠤᠷᠤᠱᠢᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠤᠯᠵᠤ ᠠᠪᠤᠭ᠍ᠰᠠᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠵᠠᠩᠬᠢ ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
</context>
<context>
    <name>LoadingWidget</name>
    <message>
        <location filename="../../CommonControl/loadingwidget.cpp" line="51"/>
        <source>Scanning, please wait</source>
        <translation>ᠱᠢᠷᠪᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠳᠦᠷ ᠬᠦᠯᠢᠶᠡᠬᠡᠷᠡᠢ</translation>
    </message>
</context>
<context>
    <name>MachineInfo</name>
    <message>
        <location filename="../../plugins/machineinfo/machineinfo.cpp" line="36"/>
        <source>MachineInfo</source>
        <translation>ᠪᠦᠬᠦᠯᠢ ᠮᠠᠱᠢᠨ᠎ᠤ ᠵᠠᠩᠬᠢ</translation>
    </message>
</context>
<context>
    <name>MainInfoPage</name>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="205"/>
        <source>Processor</source>
        <translation type="unfinished">ᠱᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="225"/>
        <source>Memory</source>
        <translation type="unfinished">ᠳᠤᠳᠤᠭᠠᠳᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="245"/>
        <source>Graphics Card</source>
        <translation type="unfinished">ᠳᠡᠯᠭᠡᠴᠡ᠎ᠶ᠋ᠢᠨ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="265"/>
        <source>Motherboard</source>
        <translation type="unfinished">ᠭᠤᠤᠯ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="285"/>
        <source>Network Card</source>
        <translation type="unfinished">ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="305"/>
        <source>Hard Disk</source>
        <translation type="unfinished">ᠬᠠᠳᠠᠭᠤ ᠳ᠋ᠢᠰᠺ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="326"/>
        <source>Monitor</source>
        <translation type="unfinished">ᠳᠡᠯᠬᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="346"/>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="363"/>
        <source>Sound Card</source>
        <translation type="unfinished">ᠳᠠᠭᠤᠨ᠎ᠤ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <source>Voice Card</source>
        <translation type="obsolete">声卡</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="384"/>
        <source>Keyboard</source>
        <translation type="unfinished">ᠳᠠᠷᠤᠭᠤᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="404"/>
        <source>Mouse</source>
        <translation>ᠮᠠᠦ᠋ᠰ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="424"/>
        <source>Battery</source>
        <translation>ᠳ᠋ᠢᠶᠠᠨ ᡂᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="444"/>
        <source>CD-ROM</source>
        <translation type="unfinished">ᠬᠡᠷᠡᠯ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="464"/>
        <source>Camera</source>
        <translation type="unfinished">ᠺᠡᠮᠸᠷᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="484"/>
        <source>Bluetooth</source>
        <translation type="unfinished">ᠬᠦᠬᠡ ᠱᠢᠳᠦ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="504"/>
        <source>Fan</source>
        <translation type="unfinished">ᠰᠡᠩᠰᠡ</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="45"/>
        <source>ToolKit</source>
        <translation type="unfinished">ᠪᠠᠭᠠᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <source>MachineInfo</source>
        <translation type="obsolete">整机信息</translation>
    </message>
    <message>
        <source>HardwareMonitor</source>
        <translation type="obsolete">硬件监测</translation>
    </message>
</context>
<context>
    <name>MemoryInfo</name>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation type="unfinished">ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="145"/>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="171"/>
        <source>Memory Info</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="220"/>
        <source>Slot</source>
        <translation>ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ ᠬᠤᠪᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="224"/>
        <source>Name</source>
        <translation type="unfinished">ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="228"/>
        <source>Freq</source>
        <translation>ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="232"/>
        <source>Bus Width</source>
        <translation>ᠶᠡᠷᠦᠩᠬᠡᠢ ᠤᠷᠤᠨ᠎ᠤ ᠦᠷᠬᠡᠨ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="236"/>
        <source>Total Capacity</source>
        <translation>ᠶᠡᠷᠦᠩᠬᠡᠢ ᠪᠠᠭ᠍ᠳᠠᠭᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="240"/>
        <source>Used Capacity</source>
        <translation>ᠨᠢᠬᠡᠨᠳᠡ ᠬᠡᠷᠡᠭ᠌ᠯᠡᠭ᠌ᠰᠡᠨ ᠪᠠᠭ᠍ᠳᠠᠭᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="244"/>
        <source>Serail Num</source>
        <translation type="unfinished">ᠴᠤᠪᠤᠷᠠᠯ᠎ᠤᠨ ᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="248"/>
        <source>Manufacturer</source>
        <translation type="unfinished">ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="252"/>
        <source>Data Width</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠤᠷᠤᠨ᠎ᠤ ᠦᠷᠬᠡᠨ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="256"/>
        <source>Type</source>
        <translation>ᠳᠦᠷᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="260"/>
        <source>Speed</source>
        <translation>ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="264"/>
        <source>Config Speed</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯ᠎ᠤᠨ ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="268"/>
        <source>Channel</source>
        <translation>ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ ᠬᠤᠪᠢᠯ᠎ᠤᠨ ᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="272"/>
        <source>Array Handle</source>
        <translation>ᠲᠤᠭᠠᠨ ᠪᠦᠯᠦᠭ᠎ᠦᠨ ᠫᠷᠤᠭ᠌ᠷᠡᠮ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="276"/>
        <source>Part Number</source>
        <translation>ᠪᠦᠳᠦᠬᠡᠭ᠌ᠳᠡᠬᠦᠨ᠎ᠦ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="280"/>
        <source>Physical ID</source>
        <translation>ᠹᠢᠽᠢᠺ᠎ᠦᠨID</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="284"/>
        <source>Model</source>
        <translation type="unfinished">ᠬᠡᠯᠪᠡᠷᠢ᠎ᠶ᠋ᠢᠨ ᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="305"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation type="unfinished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠤᠷᠤᠱᠢᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠤᠯᠵᠤ ᠠᠪᠤᠭ᠍ᠰᠠᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠵᠠᠩᠬᠢ ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
</context>
<context>
    <name>MonitorInfo</name>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="65"/>
        <source>CopyAll</source>
        <translation type="unfinished">ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="197"/>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="223"/>
        <source>Monitor</source>
        <translation>ᠳᠤᠳᠤᠳᠭᠠᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="272"/>
        <source>Manufacturer</source>
        <translation>ᠳᠤᠳᠤᠳᠭᠠᠭᠤᠷ ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="276"/>
        <source>Name</source>
        <translation>ᠳᠤᠳᠤᠳᠭᠠᠭᠤᠷ᠎ᠤᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="280"/>
        <source>Size</source>
        <translation>ᠳᠡᠯᠬᠡᠴᠡᠨ᠎ᠦ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="284"/>
        <source>Ratio</source>
        <translation>ᠢᠮᠡᠭᠧ ᠵᠢᠷᠤᠭ᠎ᠤᠨ ᠦᠨᠳᠦᠷ ᠪᠤᠯᠤᠨ ᠦᠷᠬᠡᠨ᠎ᠦ ᠬᠠᠷᠢᠴᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="288"/>
        <source>Resolution</source>
        <translation type="unfinished">ᠢᠯᠭᠠᠮᠵᠢ ᠵᠢᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="292"/>
        <source>MAX Resolution</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠶᠡᠬᠡ ᠬᠡᠷᠡᠭ᠌ᠯᠡᠵᠦ᠍ ᠪᠤᠯᠬᠤᠢᠴᠠ ᠢᠯᠭᠠᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="298"/>
        <source>Yes</source>
        <translation>ᠮᠦᠨ（ᠳᠡᠢᠮᠤ）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="300"/>
        <source>No</source>
        <translation>ᠪᠢᠰᠢ（ ᠡᠰᠡᠬᠦ᠌᠂ ᠦᠬᠡᠢ）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="302"/>
        <source>Main Screen</source>
        <translation>ᠭᠤᠤᠯ ᠳᠤᠳᠤᠳᠭᠠᠭᠤᠷ（ᠮᠦᠨ/ ᠪᠢᠰᠢ）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="306"/>
        <source>Interface</source>
        <translation type="unfinished">ᠵᠠᠯᠭᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="310"/>
        <source>Visible Area</source>
        <translation>ᠬᠠᠷᠠᠭᠠᠨ᠎ᠳᠤ ᠬᠦᠷᠳᠡᠬᠦ᠌ ᠳᠠᠯᠠᠪᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="314"/>
        <source>Product Week</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠰᠡᠨ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ/ ᠭᠠᠷᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="318"/>
        <source>Product Year</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠰᠡᠨ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ/ ᠤᠨ/</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="322"/>
        <source>Gamma</source>
        <translation>ᠭᠠᠮᠮᠠ ᠬᠡᠮᠵᠢᠭ᠌ᠳᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="343"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation type="unfinished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠤᠷᠤᠱᠢᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠤᠯᠵᠤ ᠠᠪᠤᠭ᠍ᠰᠠᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠵᠠᠩᠬᠢ ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
</context>
<context>
    <name>MotherBoardInfo</name>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="155"/>
        <source>Name</source>
        <translation>ᠭᠤᠤᠯ ᠬᠠᠪᠳᠠᠰᠤᠨ᠎ᠤ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="159"/>
        <source>Manufacturer</source>
        <translation type="unfinished">ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="167"/>
        <source>Chipset</source>
        <translation>ᠴᠢᠫ᠎ᠦᠨ ᠪᠦᠯᠦᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="171"/>
        <source>Serial Num</source>
        <translation type="unfinished">ᠴᠤᠪᠤᠷᠠᠯ᠎ᠤᠨ ᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="196"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation type="unfinished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠤᠷᠤᠱᠢᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠤᠯᠵᠤ ᠠᠪᠤᠭ᠍ᠰᠠᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠵᠠᠩᠬᠢ ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="163"/>
        <source>Version</source>
        <translation>ᠭᠤᠤᠯ ᠬᠠᠪᠳᠠᠰᠤᠨ᠎ᠤ ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="61"/>
        <source>CopyAll</source>
        <translation type="unfinished">ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="175"/>
        <source>Publish Date</source>
        <translation>ᠨᠡᠢᠳᠡᠯᠡᠭ᠌ᠰᠡᠨ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="179"/>
        <source>BIOS Manufacturer</source>
        <translation>BIOS ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="183"/>
        <source>BIOS Version</source>
        <translation>BIOS ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
</context>
<context>
    <name>MouseInfo</name>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="148"/>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="174"/>
        <source>Mouse</source>
        <translation>ᠮᠠᠦ᠋ᠰ</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">设备类型</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation type="unfinished">ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="223"/>
        <source>Name</source>
        <translation type="unfinished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="231"/>
        <source>Model</source>
        <translation type="unfinished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="235"/>
        <source>Manufacurer</source>
        <translation type="unfinished">ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="239"/>
        <source>Interface</source>
        <translation type="unfinished">ᠵᠠᠯᠭᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="243"/>
        <source>Driver</source>
        <translation type="unfinished">ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="247"/>
        <source>Device Address</source>
        <translation type="unfinished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="268"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation type="unfinished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠤᠷᠤᠱᠢᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠤᠯᠵᠤ ᠠᠪᠤᠭ᠍ᠰᠠᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠵᠠᠩᠬᠢ ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
</context>
<context>
    <name>NetCardInfo</name>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation type="unfinished">ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="145"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="171"/>
        <source>Network Card</source>
        <translation type="unfinished">ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="220"/>
        <source>Name</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠳᠤᠨᠤᠭ᠎ᠤᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="226"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="228"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="230"/>
        <source>Type</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠳᠤᠨᠤᠭ᠎ᠦᠨ ᠳᠦᠷᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="226"/>
        <source>Wired</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="228"/>
        <source>Wireless</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="235"/>
        <source>Manufacturer</source>
        <translation type="unfinished">ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="239"/>
        <source>Bus Address</source>
        <translation type="unfinished">ᠪᠠᠰ᠎ᠤᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="243"/>
        <source>MAC</source>
        <translation>MAC ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="247"/>
        <source>Driver</source>
        <translation type="unfinished">ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="251"/>
        <source>Link Speed</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠦ᠌ ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="255"/>
        <source>MTU</source>
        <translation>MTU</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="259"/>
        <source>IP</source>
        <translation>IP ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="263"/>
        <source>Model</source>
        <translation type="unfinished">ᠬᠡᠯᠪᠡᠷᠢ᠎ᠶ᠋ᠢᠨ ᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="267"/>
        <source>Subnet Mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="271"/>
        <source>Gateway</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠪᠤᠭᠤᠮᠳᠠ᠎ᠶ᠋ᠢᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="275"/>
        <source>DNS Server</source>
        <translation>DNS ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="279"/>
        <source>Bytes Received</source>
        <translation>ᠦᠰᠦᠭ᠎ᠦᠨ ᠦᠶ᠎ᠡ᠎ᠶ᠋ᠢ ᠨᠢᠬᠡᠨᠳᠡ ᠳᠤᠰᠪᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="283"/>
        <source>Bytes Sent</source>
        <translation>ᠦᠰᠦᠭ᠎ᠦᠨ ᠦᠶ᠎ᠡ᠎ᠶ᠋ᠢ ᠨᠢᠬᠡᠨᠳᠡ ᠢᠯᠡᠬᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="304"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation type="unfinished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠤᠷᠤᠱᠢᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠤᠯᠵᠤ ᠠᠪᠤᠭ᠍ᠰᠠᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠵᠠᠩᠬᠢ ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
</context>
<context>
    <name>ProcessorInfo</name>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="154"/>
        <source>Processor</source>
        <translation type="unfinished">ᠱᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <source>Number Of Cores</source>
        <translation type="vanished">核心数目</translation>
    </message>
    <message>
        <source>Number Of Threads</source>
        <translation type="vanished">线程数</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="178"/>
        <source>Slot</source>
        <translation type="unfinished">ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ ᠬᠤᠪᠢᠯ</translation>
    </message>
    <message>
        <source>Fref</source>
        <translation type="vanished">基准频率</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="61"/>
        <source>CopyAll</source>
        <translation type="unfinished">ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="158"/>
        <source>Manufacturer</source>
        <translation type="unfinished">ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="162"/>
        <source>Architecture</source>
        <translation>ᠪᠦᠷᠢᠯᠳᠦᠴᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="166"/>
        <source>Core Num</source>
        <translation>ᠴᠦᠮ᠎ᠡ ᠲᠤᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="170"/>
        <source>Core Online Num</source>
        <translation>ᠶᠠᠪᠤᠭ᠍ᠳᠠᠵᠤ ᠪᠤᠢ ᠴᠦᠮ᠎ᠡ ᠲᠤᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="174"/>
        <source>Thread</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="182"/>
        <source>Max Frequency</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠶᠡᠬᠡ ᠭᠤᠤᠯ ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="186"/>
        <source>Frequency</source>
        <translation type="unfinished">ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="190"/>
        <source>L1 Cache</source>
        <translation>ᠨᠢᠬᠡᠳᠦᠬᠡᠷ ᠳᠡᠰ᠎ᠦᠨ ᠨᠠᠮᠬᠠᠳᠭᠠᠯ ᠬᠠᠳᠠᠭᠠᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="194"/>
        <source>L1d Cache</source>
        <translation>ᠨᠢᠬᠡᠳᠦᠬᠡᠷ ᠳᠡᠰ᠎ᠦᠨ ᠨᠠᠮᠬᠠᠳᠭᠠᠯ ᠬᠠᠳᠠᠭᠠᠯᠠᠯᠳᠠ（ᠳ᠋ᠠᠢᠲ᠋ᠠ）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="198"/>
        <source>L1i Cache</source>
        <translation>ᠨᠢᠬᠡᠳᠦᠬᠡᠷ ᠳᠡᠰ᠎ᠦᠨ ᠨᠠᠮᠬᠠᠳᠭᠠᠯ ᠬᠠᠳᠠᠭᠠᠯᠠᠯᠳᠠ（ᠵᠠᠷᠯᠢᠭ）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="202"/>
        <source>L2 Cache</source>
        <translation>ᠬᠤᠶᠠᠳᠤᠭᠠᠷ ᠳᠡᠰ᠎ᠦᠨ ᠨᠠᠮᠬᠠᠳᠭᠠᠯ ᠬᠠᠳᠠᠭᠠᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="206"/>
        <source>L3 Cache</source>
        <translation>ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠳᠡᠰ᠎ᠦᠨ ᠨᠠᠮᠬᠠᠳᠭᠠᠯ ᠬᠠᠳᠠᠭᠠᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="210"/>
        <source>Instruction Set</source>
        <translation>ᠵᠠᠷᠯᠢᠭ᠎ᠤᠨ ᠴᠤᠭ᠍ᠯᠠᠷᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="214"/>
        <source>EXT Instruction Set</source>
        <translation>ᠵᠠᠷᠯᠢᠭ᠎ᠤᠨ ᠴᠤᠭ᠍ᠯᠠᠷᠠᠯ᠎ᠢ ᠦᠷᠬᠡᠳᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="218"/>
        <source>Used</source>
        <translation>ᠭᠡᠷᠡᠭ᠌ᠯᠡᠬᠡᠨ᠎ᠦ ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="63"/>
        <source>app is already running!</source>
        <translation>ᠫᠷᠤᠭ᠌ᠷᠡᠮ ᠨᠢᠬᠡᠨᠳᠡ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
</context>
<context>
    <name>VoiceCardInfo</name>
    <message>
        <source>Voice Card</source>
        <translation type="obsolete">声卡</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation type="unfinished">ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="161"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="187"/>
        <source>Sound Card</source>
        <translation type="unfinished">ᠳᠠᠭᠤᠨ᠎ᠤ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="236"/>
        <source>Bus Address</source>
        <translation type="unfinished">ᠪᠠᠰ᠎ᠤᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="240"/>
        <source>Name</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ ᠳᠤᠨᠤᠭ᠎ᠦᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="244"/>
        <source>Drive</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ ᠳᠤᠨᠤᠭ᠎ᠦᠨ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="248"/>
        <source>Model</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ ᠳᠤᠨᠤᠭ᠎ᠦᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="252"/>
        <source>Manufacurer</source>
        <translation type="unfinished">ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="256"/>
        <source>Clock</source>
        <translation type="unfinished">ᠴᠠᠭ᠎ᠤᠨ ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="260"/>
        <source>Bit Width</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ᠎ᠶ᠋ᠢᠨ ᠤᠷᠤᠨ᠎ᠤ ᠦᠷᠬᠡᠨ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="281"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation type="unfinished">ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠤᠷᠤᠱᠢᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠤᠯᠵᠤ ᠠᠪᠤᠭ᠍ᠰᠠᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠵᠠᠩᠬᠢ ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
</context>
<context>
    <name>processorinfo</name>
    <message>
        <source>Processor</source>
        <translation type="obsolete">处理器</translation>
    </message>
    <message>
        <source>Number Of Cores</source>
        <translation type="obsolete">核心数目</translation>
    </message>
    <message>
        <source>Number Of Threads</source>
        <translation type="obsolete">线程数</translation>
    </message>
    <message>
        <source>Slot</source>
        <translation type="obsolete">插槽</translation>
    </message>
    <message>
        <source>Fref</source>
        <translation type="obsolete">基准频率</translation>
    </message>
    <message>
        <source>L1 Cache</source>
        <translation type="obsolete">一级缓存</translation>
    </message>
    <message>
        <source>L2 Cache</source>
        <translation type="obsolete">二级缓存</translation>
    </message>
    <message>
        <source>L3 Cache</source>
        <translation type="obsolete">三级缓存</translation>
    </message>
    <message>
        <source>Instruction Set</source>
        <translation type="obsolete">指令集</translation>
    </message>
    <message>
        <source>Used</source>
        <translation type="obsolete">使用率</translation>
    </message>
</context>
</TS>
