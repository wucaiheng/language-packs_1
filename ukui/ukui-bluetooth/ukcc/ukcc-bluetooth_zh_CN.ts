<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en">
<context>
    <name>BlueToothMain</name>
    <message>
        <location filename="../bluetoothmain.cpp" line="1520"/>
        <source>Bluetooth</source>
        <translation>蓝牙</translation>
    </message>
    <message>
        <location filename="../bluetoothmain.cpp" line="1556"/>
        <source>Turn on</source>
        <oldsource>Turn on :</oldsource>
        <translation>开启</translation>
        <extra-contents_path>/bluetooth/Turn on Bluetooth</extra-contents_path>
    </message>
    <message>
        <location filename="../bluetoothmain.cpp" line="1605"/>
        <source>Bluetooth adapter</source>
        <translation>蓝牙适配器</translation>
        <extra-contents_path>/Bluetooth/Bluetooth adapter</extra-contents_path>
    </message>
    <message>
        <location filename="../bluetoothmain.cpp" line="1642"/>
        <source>Show icon on taskbar</source>
        <translation>在任务栏显示蓝牙图标</translation>
        <extra-contents_path>/bluetooth/Show icon on taskbar</extra-contents_path>
    </message>
    <message>
        <location filename="../bluetoothmain.cpp" line="1682"/>
        <source>Discoverable by nearby Bluetooth devices</source>
        <translation>可被附近的蓝牙设备发现</translation>
        <extra-contents_path>/bluetooth/Discoverable</extra-contents_path>
    </message>
    <message>
        <location filename="../bluetoothmain.cpp" line="1710"/>
        <source>Auto discover Bluetooth audio devices</source>
        <translation>自动发现蓝牙音频设备</translation>
        <extra-contents_path>/bluetooth/Automatically discover Bluetooth audio devices</extra-contents_path>
    </message>
    <message>
        <location filename="../bluetoothmain.cpp" line="1742"/>
        <source>My Devices</source>
        <translation>我的设备</translation>
    </message>
    <message>
        <location filename="../bluetoothmain.cpp" line="1770"/>
        <source>Other Devices</source>
        <translation>其他设备</translation>
        <extra-contents_path>/bluetooth/Other Devices</extra-contents_path>
    </message>
    <message>
        <location filename="../bluetoothmain.cpp" line="1880"/>
        <source>Bluetooth driver abnormal</source>
        <translation>蓝牙驱动异常</translation>
    </message>
    <message>
        <source>No Bluetooth adapter detected!</source>
        <translation type="vanished">未检测到蓝牙适配器！</translation>
    </message>
    <message>
        <location filename="../bluetoothmain.cpp" line="1811"/>
        <source>All</source>
        <translation>所有</translation>
    </message>
    <message>
        <location filename="../bluetoothmain.cpp" line="1812"/>
        <source>Audio</source>
        <translation>音频设备</translation>
    </message>
    <message>
        <location filename="../bluetoothmain.cpp" line="1813"/>
        <source>Peripherals</source>
        <translation>键鼠设备</translation>
    </message>
    <message>
        <location filename="../bluetoothmain.cpp" line="1814"/>
        <source>PC</source>
        <translation>电脑</translation>
    </message>
    <message>
        <location filename="../bluetoothmain.cpp" line="1815"/>
        <source>Phone</source>
        <translation>手机</translation>
    </message>
    <message>
        <location filename="../bluetoothmain.cpp" line="1816"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
    <message>
        <source>Bluetooth adapter is not detected!</source>
        <translation type="vanished">未检测到蓝牙适配器！</translation>
    </message>
</context>
<context>
    <name>BlueToothMainWindow</name>
    <message>
        <source>Bluetooth adapter is abnormal !</source>
        <translation type="vanished">蓝牙适配器异常！</translation>
    </message>
    <message>
        <source>Bluetooth adapter not detected !</source>
        <translation type="vanished">未检测到蓝牙适配器！</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation type="vanished">蓝牙</translation>
    </message>
    <message>
        <source>Turn on :</source>
        <oldsource>Turn on</oldsource>
        <translation type="vanished">开启</translation>
    </message>
    <message>
        <source>Adapter List</source>
        <translation type="vanished">蓝牙适配器</translation>
    </message>
    <message>
        <source>Show icon on taskbar</source>
        <translation type="vanished">在任务栏显示蓝牙图标</translation>
    </message>
    <message>
        <source>Discoverable by nearby Bluetooth devices</source>
        <translation type="vanished">可被附近的蓝牙设备发现</translation>
    </message>
    <message>
        <source>My Devices</source>
        <translation type="vanished">我的设备</translation>
    </message>
    <message>
        <source>Other Devices</source>
        <translation type="vanished">蓝牙设备</translation>
    </message>
    <message>
        <source>All</source>
        <translation type="vanished">所有</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="vanished">音频设备</translation>
    </message>
    <message>
        <source>Peripherals</source>
        <translation type="vanished">键鼠设备</translation>
    </message>
    <message>
        <source>PC</source>
        <translation type="vanished">电脑</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="vanished">手机</translation>
    </message>
    <message>
        <source>Other</source>
        <translation type="vanished">其他</translation>
    </message>
</context>
<context>
    <name>Bluetooth</name>
    <message>
        <location filename="../bluetooth.cpp" line="15"/>
        <source>Bluetooth</source>
        <translation>蓝牙</translation>
    </message>
</context>
<context>
    <name>BluetoothNameLabel</name>
    <message>
        <location filename="../bluetoothnamelabel.cpp" line="24"/>
        <source>Click to change the device name</source>
        <oldsource>Double-click to change the device name</oldsource>
        <translation>点击修改设备名称</translation>
    </message>
    <message>
        <location filename="../bluetoothnamelabel.cpp" line="81"/>
        <location filename="../bluetoothnamelabel.cpp" line="204"/>
        <source>Can now be found as &quot;%1&quot;</source>
        <translation>现在可被发现为&quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../bluetoothnamelabel.cpp" line="92"/>
        <source>Tip</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../bluetoothnamelabel.cpp" line="93"/>
        <source>The length of the device name does not exceed %1 characters !</source>
        <translation>设备名称的长度不超过 %1 个字符！</translation>
    </message>
</context>
<context>
    <name>DevRemoveDialog</name>
    <message>
        <location filename="../devremovedialog.cpp" line="27"/>
        <source>Bluetooth Connections</source>
        <translation>蓝牙连接</translation>
    </message>
    <message>
        <location filename="../devremovedialog.cpp" line="48"/>
        <location filename="../devremovedialog.cpp" line="55"/>
        <source>After it is removed, the PIN code must be matched for the next connection.</source>
        <translation>移除后，下一次连接可能需匹配PIN码。</translation>
    </message>
    <message>
        <location filename="../devremovedialog.cpp" line="74"/>
        <source>Remove</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="../devremovedialog.cpp" line="82"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../devremovedialog.cpp" line="107"/>
        <source>Connection failed! Please remove it before connecting.</source>
        <translation>连接失败，请先移除后再次连接！</translation>
    </message>
    <message>
        <location filename="../devremovedialog.cpp" line="109"/>
        <location filename="../devremovedialog.cpp" line="118"/>
        <source>Are you sure to remove %1 ?</source>
        <translation>确定移除&quot;%1&quot;?</translation>
    </message>
</context>
<context>
    <name>DevRenameDialog</name>
    <message>
        <location filename="../devrenamedialog.cpp" line="79"/>
        <source>Rename</source>
        <translation>修改名称</translation>
    </message>
    <message>
        <location filename="../devrenamedialog.cpp" line="95"/>
        <source>Name</source>
        <translation>设备名称</translation>
    </message>
    <message>
        <location filename="../devrenamedialog.cpp" line="109"/>
        <source>The value contains 1 to 32 characters</source>
        <translation>长度必须为 1-32 个字符</translation>
    </message>
    <message>
        <source>The input character length exceeds the limit</source>
        <translation type="vanished">输入字符长度超过限制！</translation>
    </message>
    <message>
        <location filename="../devrenamedialog.cpp" line="114"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../devrenamedialog.cpp" line="127"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>DeviceInfoItem</name>
    <message>
        <location filename="../deviceinfoitem.cpp" line="13"/>
        <source>Connecting</source>
        <translation>正在连接</translation>
    </message>
    <message>
        <location filename="../deviceinfoitem.cpp" line="14"/>
        <source>Disconnecting</source>
        <translation>正在断连</translation>
    </message>
    <message>
        <location filename="../deviceinfoitem.cpp" line="15"/>
        <source>Connected</source>
        <translation>已连接</translation>
    </message>
    <message>
        <location filename="../deviceinfoitem.cpp" line="16"/>
        <source>Not Connected</source>
        <translation>未连接</translation>
    </message>
    <message>
        <location filename="../deviceinfoitem.cpp" line="150"/>
        <location filename="../deviceinfoitem.cpp" line="509"/>
        <source>disconnect</source>
        <translation>断开连接</translation>
    </message>
    <message>
        <source>Paired</source>
        <translation type="vanished">已匹配</translation>
    </message>
    <message>
        <location filename="../deviceinfoitem.cpp" line="17"/>
        <source>Connect fail</source>
        <translation>连接失败</translation>
    </message>
    <message>
        <location filename="../deviceinfoitem.cpp" line="18"/>
        <source>Disconnect fail</source>
        <translation>断连失败</translation>
    </message>
    <message>
        <source>Send files</source>
        <translation type="vanished">发送文件</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="vanished">移除</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <location filename="../deviceinfoitem.cpp" line="139"/>
        <location filename="../deviceinfoitem.cpp" line="539"/>
        <source>remove</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="../deviceinfoitem.cpp" line="133"/>
        <location filename="../deviceinfoitem.cpp" line="532"/>
        <source>send file</source>
        <translation>发送文件</translation>
    </message>
    <message>
        <source>Sure to remove,</source>
        <translation type="vanished">确定删除，</translation>
    </message>
    <message>
        <source>After removal, the next connection requires matching PIN code!</source>
        <translation type="vanished">移除后，下一次连接需要匹配的PIN码！</translation>
    </message>
</context>
</TS>
