<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>ActiveConnectionWidget</name>
    <message>
        <location filename="../activeConn/activeconnectionwidget.cpp" line="30"/>
        <source>Bluetooth Connection</source>
        <translation>蓝牙连接</translation>
    </message>
    <message>
        <location filename="../activeConn/activeconnectionwidget.cpp" line="40"/>
        <source>Bluetooth Connections</source>
        <translation>蓝牙连接</translation>
    </message>
    <message>
        <location filename="../activeConn/activeconnectionwidget.cpp" line="53"/>
        <location filename="../activeConn/activeconnectionwidget.cpp" line="121"/>
        <source>Found audio device &quot;</source>
        <translation>发现音频设备 &quot;</translation>
    </message>
    <message>
        <location filename="../activeConn/activeconnectionwidget.cpp" line="53"/>
        <location filename="../activeConn/activeconnectionwidget.cpp" line="121"/>
        <source>&quot;, connect it or not?</source>
        <translation>&quot; ，是否连接？</translation>
    </message>
    <message>
        <location filename="../activeConn/activeconnectionwidget.cpp" line="61"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <location filename="../activeConn/activeconnectionwidget.cpp" line="63"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>BluetoothFileTransferWidget</name>
    <message>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="32"/>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="143"/>
        <source>Bluetooth file transfer</source>
        <translation>蓝牙文件</translation>
    </message>
    <message>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="47"/>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="308"/>
        <source>Transferring to &quot;</source>
        <translation>发送文件至 &quot;</translation>
    </message>
    <message>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="72"/>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="420"/>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="461"/>
        <source> and </source>
        <translation> 等 </translation>
    </message>
    <message>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="72"/>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="420"/>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="461"/>
        <source> files more</source>
        <translation> 个文件</translation>
    </message>
    <message>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="87"/>
        <source>Select Device</source>
        <translation>选择设备</translation>
    </message>
    <message>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="92"/>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="501"/>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="562"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="105"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="121"/>
        <source>File Transmission Failed !</source>
        <translation>文件发送失败！</translation>
    </message>
    <message>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="250"/>
        <source>Warning</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="250"/>
        <source>The selected file is empty, please select the file again !</source>
        <translation>所选文件为空，请重新选择！</translation>
    </message>
    <message>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="504"/>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="513"/>
        <source>File Transmition Succeed!</source>
        <translation>文件发送成功！</translation>
    </message>
    <message>
        <location filename="../fileSend/bluetoothfiletransferwidget.cpp" line="521"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>BluetoothSettingLabel</name>
    <message>
        <location filename="../component/bluetoothsettinglabel.cpp" line="114"/>
        <source>Bluetooth Settings</source>
        <translation>蓝牙设置</translation>
    </message>
</context>
<context>
    <name>Config</name>
    <message>
        <source>ukui-bluetooth</source>
        <translation type="vanished">蓝牙</translation>
    </message>
    <message>
        <location filename="../config/config.cpp" line="24"/>
        <source>Bluetooth</source>
        <translation>蓝牙</translation>
    </message>
    <message>
        <location filename="../config/config.cpp" line="27"/>
        <source>Bluetooth Message</source>
        <translation>蓝牙消息</translation>
    </message>
    <message>
        <source>Bluetooth message</source>
        <translation type="vanished">蓝牙消息</translation>
    </message>
</context>
<context>
    <name>DeviceSeleterWidget</name>
    <message>
        <location filename="../fileSend/deviceseleterwidget.cpp" line="114"/>
        <source>No device currently available 
 Please go to pair the device</source>
        <translation>当前没有可用的设备
请去配对设备</translation>
    </message>
</context>
<context>
    <name>FileReceivingPopupWidget</name>
    <message>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="30"/>
        <source>Bluetooth file transfer</source>
        <translation>蓝牙文件传输</translation>
    </message>
    <message>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="176"/>
        <source>Bluetooth File</source>
        <translation>蓝牙文件</translation>
    </message>
    <message>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="80"/>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="82"/>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="204"/>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="206"/>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="281"/>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="413"/>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="493"/>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="549"/>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="553"/>
        <source>File from &quot;</source>
        <translation>文件来自 &quot;</translation>
    </message>
    <message>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="80"/>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="204"/>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="553"/>
        <source>&quot;, waiting for receive...</source>
        <translation>&quot;，等待接收…</translation>
    </message>
    <message>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="82"/>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="206"/>
        <source>&quot;, waiting for receive.</source>
        <translation>&quot;，等待接收。</translation>
    </message>
    <message>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="120"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="126"/>
        <source>Accept</source>
        <translation>接收</translation>
    </message>
    <message>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="137"/>
        <source>View</source>
        <translation>查看</translation>
    </message>
    <message>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="281"/>
        <source>&quot;, is receiving... (has recieved </source>
        <translation>&quot;，正在接收… (已接收 </translation>
    </message>
    <message>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="281"/>
        <source> files)</source>
        <translation> 个文件)</translation>
    </message>
    <message>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="413"/>
        <source>&quot;, is receiving...</source>
        <translation>&quot;，正在接收…</translation>
    </message>
    <message>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="474"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="493"/>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="549"/>
        <source>&quot;, received failed !</source>
        <translation>&quot;，接收失败！</translation>
    </message>
    <message>
        <location filename="../fileReceive/filereceivingpopupwidget.cpp" line="501"/>
        <source>File Transmission Failed !</source>
        <translation>文件传输失败！</translation>
    </message>
</context>
<context>
    <name>KyFileDialog</name>
    <message>
        <location filename="../component/kyfiledialog.cpp" line="13"/>
        <source>Bluetooth File</source>
        <translation>蓝牙文件</translation>
    </message>
</context>
<context>
    <name>MainProgram</name>
    <message>
        <location filename="../main/mainprogram.cpp" line="179"/>
        <source>Warning</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../main/mainprogram.cpp" line="179"/>
        <source>The selected file is empty, please select the file again !</source>
        <translation>所选文件为空，请重新选择！</translation>
    </message>
</context>
<context>
    <name>PinCodeWidget</name>
    <message>
        <location filename="../pin/pincodewidget.cpp" line="40"/>
        <source>Bluetooth pairing</source>
        <translation>蓝牙设备配对</translation>
    </message>
    <message>
        <location filename="../pin/pincodewidget.cpp" line="55"/>
        <location filename="../pin/pincodewidget.cpp" line="196"/>
        <source>&apos; is the same as this PIN. Please press &apos;Connect&apos;</source>
        <translation>”上的数字与下面相同，请点击“连接”</translation>
    </message>
    <message>
        <location filename="../pin/pincodewidget.cpp" line="57"/>
        <location filename="../pin/pincodewidget.cpp" line="198"/>
        <source>If &apos;</source>
        <translation>如果 &quot;</translation>
    </message>
    <message>
        <location filename="../pin/pincodewidget.cpp" line="85"/>
        <source>Refuse</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../pin/pincodewidget.cpp" line="113"/>
        <source>Bluetooth Connections</source>
        <translation>蓝牙连接</translation>
    </message>
    <message>
        <location filename="../pin/pincodewidget.cpp" line="153"/>
        <source>Bluetooth Connect Failed</source>
        <translation>蓝牙连接失败</translation>
    </message>
    <message>
        <location filename="../pin/pincodewidget.cpp" line="163"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../pin/pincodewidget.cpp" line="171"/>
        <source>Connect Failed!</source>
        <translation>连接失败！</translation>
    </message>
    <message>
        <location filename="../pin/pincodewidget.cpp" line="57"/>
        <location filename="../pin/pincodewidget.cpp" line="198"/>
        <source>&apos; the PIN on is the same as this PIN. Please press &apos;Connect&apos;.</source>
        <translation>&quot; 上的PIN码与此PIN码相同，请按 &quot;连接&quot;。</translation>
    </message>
    <message>
        <location filename="../pin/pincodewidget.cpp" line="38"/>
        <source>Bluetooth Connection</source>
        <translation>蓝牙连接</translation>
    </message>
    <message>
        <location filename="../pin/pincodewidget.cpp" line="55"/>
        <location filename="../pin/pincodewidget.cpp" line="196"/>
        <source>If the PIN on &apos;</source>
        <translation>如“</translation>
    </message>
    <message>
        <location filename="../pin/pincodewidget.cpp" line="59"/>
        <location filename="../pin/pincodewidget.cpp" line="200"/>
        <source>Please enter the following PIN code on the bluetooth device &apos;%1&apos; and press enter to pair:</source>
        <translation>请在蓝牙设备 &quot;%1&quot; 上输入相同PIN，并按&quot;Enter&quot;确认:</translation>
    </message>
    <message>
        <location filename="../pin/pincodewidget.cpp" line="92"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../pin/pincodewidget.cpp" line="83"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <location filename="../pin/pincodewidget.cpp" line="170"/>
        <source>Pair</source>
        <translation>配对</translation>
    </message>
</context>
<context>
    <name>QDevItem</name>
    <message>
        <location filename="../component/qdevitem.cpp" line="57"/>
        <source>The connection with the Bluetooth device “%1” is successful!</source>
        <translation>与蓝牙设备“%1”连接成功！</translation>
    </message>
    <message>
        <location filename="../component/qdevitem.cpp" line="60"/>
        <source>Bluetooth device “%1” disconnected!</source>
        <translation>蓝牙设备“%1”失去连接！</translation>
    </message>
</context>
<context>
    <name>SwitchAction</name>
    <message>
        <source>Bluetooth</source>
        <translation type="vanished">蓝牙</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <location filename="../mainwidget/trayicon.cpp" line="13"/>
        <source>Set Bluetooth Item</source>
        <translation>设置蓝牙项</translation>
    </message>
    <message>
        <location filename="../mainwidget/trayicon.cpp" line="24"/>
        <source>Bluetooth</source>
        <translation>蓝牙</translation>
    </message>
</context>
<context>
    <name>TrayWidget</name>
    <message>
        <location filename="../mainwidget/traywidget.cpp" line="89"/>
        <source>bluetooth</source>
        <translation>蓝牙</translation>
    </message>
    <message>
        <location filename="../mainwidget/traywidget.cpp" line="291"/>
        <source>Bluetooth</source>
        <translation>蓝牙</translation>
    </message>
    <message>
        <location filename="../mainwidget/traywidget.cpp" line="343"/>
        <source>My Device</source>
        <translation>我的设备</translation>
    </message>
    <message>
        <location filename="../mainwidget/traywidget.cpp" line="433"/>
        <source>The connection with the Bluetooth device “%1” is successful!</source>
        <translation>与蓝牙设备“%1”连接成功！</translation>
    </message>
    <message>
        <location filename="../mainwidget/traywidget.cpp" line="436"/>
        <source>Bluetooth device “%1” disconnected!</source>
        <translation>蓝牙设备“%1”失去连接！</translation>
    </message>
</context>
</TS>
