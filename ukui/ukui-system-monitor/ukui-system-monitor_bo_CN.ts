<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>CpuHistoryWidget</name>
    <message>
        <location filename="../sysresource/cpuhistorywidget.cpp" line="64"/>
        <source>CPU</source>
        <translation>སྒྲིག་གཅོད་ཆས།</translation>
    </message>
</context>
<context>
    <name>CustomServiceNameDlg</name>
    <message>
        <location filename="../dialog/customservicenamedlg.cpp" line="18"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../dialog/customservicenamedlg.cpp" line="19"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
</context>
<context>
    <name>FileSystemInfoItem</name>
    <message>
        <location filename="../filesystem/filesysteminfoitem.cpp" line="94"/>
        <source>%1 Used</source>
        <translation>%1བཀོལ་སྤྱོད་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../filesystem/filesysteminfoitem.cpp" line="96"/>
        <source>(%1 Available/%2 Free)  Total %3  %4  Directory:%5</source>
        <translation>(%1 ཉེར་སྤྱོད/%2རིན་མེད) སྤྱིའི་བོངས་ཚད %3%4 ལམ་བུ%5</translation>
    </message>
</context>
<context>
    <name>FileSystemWidget</name>
    <message>
        <location filename="../filesystem/filesystemwidget.cpp" line="47"/>
        <source>No search result</source>
        <translation>འཚོལ་ཞིབ་བྱས་འབྲས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../filesystem/filesystemwidget.cpp" line="71"/>
        <source>Refresh</source>
        <translation>གསར་བསྒྱུར།</translation>
    </message>
</context>
<context>
    <name>KLeftWidget</name>
    <message>
        <location filename="../kleftwidget.cpp" line="173"/>
        <source>Kylin System Monitor</source>
        <translation>མ་ལག་ལྟ་ཞིབ་འཕྲུལ་ཆས།</translation>
    </message>
</context>
<context>
    <name>KRightWidget</name>
    <message>
        <location filename="../krightwidget.cpp" line="73"/>
        <source>menu</source>
        <translation>འདེམས་པང་།</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="79"/>
        <source>minimize</source>
        <translation>ཆེས་ཆུང་བསྒྱུར།</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="85"/>
        <source>maximize/restore</source>
        <translation>ཆེས་ཆེ་བསྒྱུར།</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="91"/>
        <source>close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="103"/>
        <location filename="../krightwidget.cpp" line="117"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="104"/>
        <location filename="../krightwidget.cpp" line="111"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="105"/>
        <location filename="../krightwidget.cpp" line="120"/>
        <source>Exit</source>
        <translation>ཕྱིར་འཐེན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="132"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="21"/>
        <source>Kylin System Monitor</source>
        <translation>མ་ལག་ལྟ་ཞིབ་འཕྲུལ་ཆས།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="137"/>
        <location filename="../mainwindow.cpp" line="158"/>
        <source>Processes</source>
        <translation>མདུན་སྐྱོད།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="141"/>
        <location filename="../mainwindow.cpp" line="163"/>
        <source>Services</source>
        <translation>ཞབས་ཞུ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="145"/>
        <location filename="../mainwindow.cpp" line="167"/>
        <source>Disks</source>
        <translation>ཁབ་ལེན་འཁོར་གཞོང་།</translation>
    </message>
</context>
<context>
    <name>MemHistoryWidget</name>
    <message>
        <location filename="../sysresource/memhistorywidget.cpp" line="111"/>
        <source>memory</source>
        <translation>ནང་སྙིང་།</translation>
    </message>
    <message>
        <location filename="../sysresource/memhistorywidget.cpp" line="121"/>
        <source>swap</source>
        <translation>བརྗེ་རེས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../sysresource/memhistorywidget.cpp" line="126"/>
        <location filename="../sysresource/memhistorywidget.cpp" line="274"/>
        <source>Not enabled</source>
        <translation>ནུས་པ་ཐོན་མི་ཐུབ་པ།</translation>
    </message>
</context>
<context>
    <name>NetHistoryWidget</name>
    <message>
        <location filename="../sysresource/nethistorywidget.cpp" line="117"/>
        <source>Network History</source>
        <translation>དྲ་རྒྱའི་ལོ་རྒྱུས།</translation>
    </message>
    <message>
        <location filename="../sysresource/nethistorywidget.cpp" line="126"/>
        <source>send</source>
        <translation>སྐུར་སྐྱེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../sysresource/nethistorywidget.cpp" line="139"/>
        <source>receive</source>
        <translation>བསྡུ་ལེན་བྱས་པ།</translation>
    </message>
</context>
<context>
    <name>ProcPropertiesDlg</name>
    <message>
        <location filename="../dialog/procpropertiesdlg.cpp" line="137"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
</context>
<context>
    <name>ProcessTableModel</name>
    <message>
        <location filename="../process/processtablemodel.cpp" line="185"/>
        <source>Process Name</source>
        <translation>འཕེལ་རིམ་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="189"/>
        <source>User</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།།</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="193"/>
        <source>Disk</source>
        <translation>སྡུད་སྡེར།</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="196"/>
        <source>CPU</source>
        <translation>སྒྲིག་གཅོད་ཆས།</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="199"/>
        <source>ID</source>
        <translation>མདུན་སྐྱོད་ཀྱི་ཨང་།</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="202"/>
        <source>Flownet Persec</source>
        <translation>དྲ་རྒྱ།</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="205"/>
        <source>Memory</source>
        <translation>ནང་སྙིང་།</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="208"/>
        <source>Priority</source>
        <translation>སྔོན་ཐོབ་ཀྱི་དོན་དག</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="242"/>
        <source>Suspend</source>
        <translation>གནས་སྐབས་མཚམས་འཇོག</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="245"/>
        <source>No response</source>
        <translation>ལན་མི་འདེབས།</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="248"/>
        <source>Uninterruptible</source>
        <translation>བར་ཆད་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>ProcessTableView</name>
    <message>
        <location filename="../process/processtableview.cpp" line="182"/>
        <source>No search result</source>
        <translation>འཚོལ་ཞིབ་བྱས་འབྲས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="269"/>
        <location filename="../process/processtableview.cpp" line="573"/>
        <location filename="../process/processtableview.cpp" line="676"/>
        <location filename="../process/processtableview.cpp" line="686"/>
        <source>End process</source>
        <translation>མཇུག་སྒྲིལ་བའི་གོ་རིམ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="271"/>
        <location filename="../process/processtableview.cpp" line="611"/>
        <location filename="../process/processtableview.cpp" line="705"/>
        <location filename="../process/processtableview.cpp" line="715"/>
        <source>Kill process</source>
        <translation>བཙན་ཤེད་ཀྱིས་འཕེལ་རིམ་མཇུག་སྒྲིལ་དུ་བཅུག</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="285"/>
        <source>Very High</source>
        <translation>ཧ་ཅང་མཐོ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="286"/>
        <source>High</source>
        <translation>མཐོ་ཚད།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="287"/>
        <source>Normal</source>
        <translation>རྒྱུན་ལྡན།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="288"/>
        <source>Low</source>
        <translation>དམའ་བ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="289"/>
        <source>Very Low</source>
        <translation>ཧ་ཅང་དམའ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="290"/>
        <source>Custom</source>
        <translation>མཚན་ཉིད་རང་འཇོག</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="295"/>
        <source>Change Priority</source>
        <translation>སྔོན་ལ་བསྒྱུར་བཅོས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="297"/>
        <source>Properties</source>
        <translation>ངོ་བོ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="315"/>
        <source>User</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="321"/>
        <source>Disk</source>
        <translation>སྡུད་སྡེར།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="327"/>
        <source>CPU</source>
        <translation>སྒྲིག་གཅོད་ཆས།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="333"/>
        <source>ID</source>
        <translation>མདུན་སྐྱོད་ཨང་།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="339"/>
        <source>Flownet Persec</source>
        <translation>དྲ་བ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="345"/>
        <source>Memory</source>
        <translation>ནང་སྙིང་།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="351"/>
        <source>Priority</source>
        <translation>སྔོན་ཐོབ་ཀྱི་དོན་དག</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="536"/>
        <location filename="../process/processtableview.cpp" line="544"/>
        <source>Stop process</source>
        <translation>མདུན་སྐྱོད་མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="681"/>
        <source>End the selected process &quot;%1&quot;(PID:%2)?</source>
        <translation>འདེམས་བསྐོ་བྱས་པའི་འཕེལ་རིམ་གྱི་%1་(་འཕེལ་རིམ་གྱི་ཨང་གྲངས་ནི%2)་མཇུག་བསྒྲིལ་རྒྱུ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="682"/>
        <source>Ending a process may destroy data, break the session or introduce a security risk. Only unresponsive processes should be ended.
Are you sure to continue?</source>
        <translation>མཇུག་སྒྲིལ་བའི་བརྒྱུད་རིམ་གྱིས་གཞི་གྲངས་ལ་ཆག་སྐྱོན་བཟོ་སྲིད།སྐད་ཆ་བཤད་མཚམས་འཇོག་པའམ་ཡང་ན་བདེ་འཇགས་ཀྱི་ཉེན་ཁ་བཟོ་སྲིད།ཁྱེད་ཀྱིས་དང་ལེན་མ་བྱས་པའི་འཕེལ་རིམ་ཁོ་ན་མཇུག་བསྒྲིལ་དགོས།ཁྱེད་ཀྱིས་མུ་མཐུད་དུ་བཀོལ་སྤྱོད་བྱེད་རྒྱུ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="685"/>
        <location filename="../process/processtableview.cpp" line="714"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="710"/>
        <source>Kill the selected process &quot;%1&quot;(PID:%2)?</source>
        <translation>བདམས་ཟིན་པའི་གོ་རིམ་&quot;%1&quot;(PID:%2)བསད་པ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="711"/>
        <source>Killing a process may destroy data, break the session or introduce a security risk. Only unresponsive processes should be killed.
Are you sure to continue?</source>
        <translation>བཙན་ཤེད་ཀྱིས་མཇུག་སྒྲིལ་བའི་བརྒྱུད་རིམ་གྱིས་གཞི་གྲངས་ལ་གནོད་སྐྱོན་བཟོ་སྲིད་པ་དང་།སྐད་ཆ་བཤད་མཚམས་འཇོག་པའམ་ཡང་ན་བདེ་འཇགས་ཀྱི་ཉེན་ཁ་བཟོ་སྲིད།ཁྱེད་ཀྱིས་དང་ལེན་མི་བྱེད་པའི་འཕེལ་རིམ་ཁོ་ན་གསོད་དགོས་།ཁྱེད་ཀྱིས་མུ་མཐུད་དུ་བཀོལ་སྤྱོད་བྱེད་རྒྱུ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="724"/>
        <source>Not allowed operation!</source>
        <translation>བཀོལ་སྤྱོད་བྱེད་མི་ཆོག!</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="725"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
</context>
<context>
    <name>ProcessWidget</name>
    <message>
        <location filename="../process/processwidget.cpp" line="223"/>
        <source>Active Processes</source>
        <translation>བྱེད་སྒོའི་འཕེལ་རིམ།</translation>
    </message>
    <message>
        <location filename="../process/processwidget.cpp" line="235"/>
        <source>Applications</source>
        <translation>ཉེར་སྤྱོད་གོ་རིམ།</translation>
    </message>
    <message>
        <location filename="../process/processwidget.cpp" line="247"/>
        <source>My Processes</source>
        <translation>ངའི་བརྒྱུད་རིམ།</translation>
    </message>
    <message>
        <location filename="../process/processwidget.cpp" line="259"/>
        <source>All Process</source>
        <translation>བརྒྱུད་རིམ་ཡོད་ཚད།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../dialog/procpropertiesdlg.cpp" line="148"/>
        <source>User name:</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location filename="../dialog/procpropertiesdlg.cpp" line="148"/>
        <source>Process name:</source>
        <translation>བཟོ་རྩལ་གྱི་མིང་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location filename="../dialog/procpropertiesdlg.cpp" line="148"/>
        <source>Command line:</source>
        <translation>བཀོད་འདོམས་ལམ་ཐིག་ནི།</translation>
    </message>
    <message>
        <location filename="../dialog/procpropertiesdlg.cpp" line="148"/>
        <source>Started Time:</source>
        <translation>འགོ་ཚུགས་པའི་དུས་ཚོད་ནི།</translation>
    </message>
    <message>
        <location filename="../dialog/procpropertiesdlg.cpp" line="148"/>
        <source>CPU Time:</source>
        <translation>སྒྲིག་གཅོད་ཆས་ཀྱི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="124"/>
        <source>ukui-system-monitor is already running!</source>
        <translation>ukui-system-monitor འཁོར་སྐྱོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="50"/>
        <location filename="../sysresource/memhistorywidget.cpp" line="33"/>
        <location filename="../sysresource/nethistorywidget.cpp" line="35"/>
        <source>MiB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="241"/>
        <location filename="../process/processtablemodel.cpp" line="329"/>
        <location filename="../util.cpp" line="396"/>
        <source>Stopped</source>
        <translation>བཀག་འགོག་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="244"/>
        <location filename="../process/processtablemodel.cpp" line="332"/>
        <location filename="../util.cpp" line="400"/>
        <source>Zombie</source>
        <translation>རེངས་པོར་གྱུར།</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="247"/>
        <location filename="../process/processtablemodel.cpp" line="335"/>
        <location filename="../util.cpp" line="404"/>
        <source>Uninterruptible</source>
        <translation>བར་ཆད་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../sysresource/memhistorywidget.cpp" line="28"/>
        <location filename="../sysresource/nethistorywidget.cpp" line="29"/>
        <source>KiB</source>
        <translation>KB</translation>
    </message>
    <message>
        <location filename="../sysresource/memhistorywidget.cpp" line="38"/>
        <location filename="../sysresource/nethistorywidget.cpp" line="41"/>
        <source>GiB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location filename="../sysresource/memhistorywidget.cpp" line="43"/>
        <location filename="../sysresource/nethistorywidget.cpp" line="47"/>
        <source>TiB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../sysresource/nethistorywidget.cpp" line="31"/>
        <source>KiB/s</source>
        <translation>KBསྐར་ཆ་རེ།</translation>
    </message>
    <message>
        <location filename="../sysresource/nethistorywidget.cpp" line="37"/>
        <source>MiB/s</source>
        <translation>MBསྐར་ཆ་རེ།</translation>
    </message>
    <message>
        <location filename="../sysresource/nethistorywidget.cpp" line="43"/>
        <source>GiB/s</source>
        <translation>GBསྐར་ཆ།</translation>
    </message>
    <message>
        <location filename="../sysresource/nethistorywidget.cpp" line="49"/>
        <source>TiB/s</source>
        <translation>TBསྐར་ཆ།</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="392"/>
        <source>Running</source>
        <translation>འཁོར་སྐྱོད་བྱེད་བཞིན།</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="408"/>
        <source>Sleeping</source>
        <translation>གཉིད་དུ་ཡུར་བ།</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="418"/>
        <source>Very High</source>
        <translation>ཧ་ཅང་མཐོ།</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="420"/>
        <source>High</source>
        <translation>མཐོ་ཚད།</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="422"/>
        <source>Normal</source>
        <translation>རྒྱུན་ལྡན།</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="424"/>
        <source>Low</source>
        <translation>དམའ་བ།</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="426"/>
        <source>Very Low</source>
        <translation>ཧ་ཅང་དམའ།</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="432"/>
        <source>Very High Priority</source>
        <translation>ཧ་ཅང་མཐོ་བའི་སྔོན་ཐོན་རིམ་པ།</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="434"/>
        <source>High Priority</source>
        <translation>ཚད་མཐོའི་མཐོང་ཆེན།</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="436"/>
        <source>Normal Priority</source>
        <translation>རྒྱུན་ལྡན་གྱི་སྔོན་ཐོབ་དབང་ཆ།</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="438"/>
        <source>Low Priority</source>
        <translation>སྔོན་ཐོན་རིམ་པ་དམའ་བ།</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="440"/>
        <source>Very Low Priority</source>
        <translation>ཧ་ཅང་དམའ་བའི་སྔོན་ཐོན་རིམ་པ།</translation>
    </message>
</context>
<context>
    <name>ReniceDialog</name>
    <message>
        <location filename="../dialog/renicedialog.cpp" line="65"/>
        <location filename="../dialog/renicedialog.cpp" line="300"/>
        <source>Change Priority of Process %1 (PID: %2)</source>
        <translation>བཟོ་རྩལ་གྱི་སྔོན་ཐོབ་དབང་ཆ་བསྒྱུར་བཅོས་བྱ་དགོས། %1 (PID: %2)</translation>
    </message>
    <message>
        <location filename="../dialog/renicedialog.cpp" line="83"/>
        <location filename="../dialog/renicedialog.cpp" line="330"/>
        <source>Nice value:</source>
        <translation>སྔོན་ཐོབ་གྲངས།</translation>
    </message>
    <message>
        <location filename="../dialog/renicedialog.cpp" line="103"/>
        <source>Note:</source>
        <translation>མཆན་འགྲེལ།</translation>
    </message>
    <message>
        <location filename="../dialog/renicedialog.cpp" line="107"/>
        <source>The priority of a process is given by its nice value. A lower nice value corresponds to a higher priority.</source>
        <translation>འཕེལ་རིམ་གྱི་སྔོན་ཐོན་རིམ་པ་ནི་དེའི་སྔོན་ཐོན་རིན་ཐང་གིས་གཏན་འཁེལ་བྱས་པ་རེད།ཅུང་དམའ་བའི་སྔོན་ཐོན་རིན་ཐང་ལྟོས་བཅས་ཀྱིས་ཅུང་མཐོ་བའི་སྔོན་ཐོན་རིམ་པ།</translation>
    </message>
    <message>
        <location filename="../dialog/renicedialog.cpp" line="119"/>
        <location filename="../dialog/renicedialog.cpp" line="310"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../dialog/renicedialog.cpp" line="124"/>
        <location filename="../dialog/renicedialog.cpp" line="320"/>
        <source>Change Priority</source>
        <translation>སྔོན་ལ་བསྒྱུར་བཅོས་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>ServiceManager</name>
    <message>
        <location filename="../service/servicemanager.cpp" line="330"/>
        <source>Failed to set service startup type</source>
        <translation>ཞབས་ཞུའི་ལས་འགོ་འཛུགས་པའི་རིགས་དབྱིབས་གཏན་འཁེལ་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicemanager.cpp" line="372"/>
        <source>Error: Failed to set service startup type due to the crashed sub process.</source>
        <translation>ནོར་འཁྲུལ། ཐོར་ཞིག་ཏུ་སོང་བའི་ཡན་ལག་བརྒྱུད་རིམ་གྱི་རྐྱེན་གྱིས་ཞབས་ཞུའི་ལས་རིགས་ཀྱི་གསར་གཏོད་རིགས་གཏན་འཁེལ་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
</context>
<context>
    <name>ServiceTableModel</name>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="40"/>
        <source>stub</source>
        <translation>སི་ཐུའུ་ལུའུ་པུའུ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="40"/>
        <source>loaded</source>
        <translation>དངོས་ཟོག་བླུགས་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="40"/>
        <source>not-found</source>
        <translation>མ་རྙེད་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="40"/>
        <source>bad-setting</source>
        <translation>བཀོད་སྒྲིག་ཡག་པོ་མ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="40"/>
        <source>error</source>
        <translation>ནོར་འཁྲུལ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="40"/>
        <source>merged</source>
        <translation>ཟླ་སྒྲིལ་བཏང་བ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="40"/>
        <location filename="../service/servicetablemodel.cpp" line="44"/>
        <source>masked</source>
        <translation>སྒྲིབ་ཡོལ་བྱས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="41"/>
        <source>active</source>
        <translation>འགོ་ཚུགས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="41"/>
        <source>reloading</source>
        <translation>ཡང་བསྐྱར་དངོས་ཟོག་བླུགས་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="41"/>
        <source>inactive</source>
        <translation>འགུལ་སྐྱོད་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="41"/>
        <source>failed</source>
        <translation>ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="41"/>
        <source>activating</source>
        <translation>སྐུལ་སློང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="41"/>
        <source>deactivating</source>
        <translation>འགུལ་སྐྱོད་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <source>dead</source>
        <translation>བཀལ་ཟིན།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <source>start-pre</source>
        <translation>སྔོན་ལ་འགོ་ཚུགས།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <source>start</source>
        <translation>འགོ་ཚུགས་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <source>start-post</source>
        <translation>འགོ་ཚུགས་པའི་ལས་གནས།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <source>running</source>
        <translation>འཁོར་སྐྱོད་བྱེད་བཞིན།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <source>exited</source>
        <translation>ཕྱིར་འཐེན་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <source>reload</source>
        <translation>བསྐྱར་དུ་ཕབ་ལེན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <source>stop</source>
        <translation>མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <source>stop-watchdog</source>
        <translation>stop-watchdog</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <location filename="../service/servicetablemodel.cpp" line="43"/>
        <source>stop-sigterm</source>
        <translation>stop-sigterm</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="43"/>
        <source>stop-sigkill</source>
        <translation>མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="43"/>
        <source>stop-post</source>
        <translation>ལས་གནས་མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="43"/>
        <source>final-sigterm</source>
        <translation>མཐའ་མའི་འགྲན་བསྡུར་གྱི་འགྲན་བསྡུར།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="43"/>
        <source>final-sigkill</source>
        <translation>final-sigkill</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="43"/>
        <source>auto-restart</source>
        <translation>རང་འགུལ་གྱིས་བསྐྱར་དུ་འགོ་ཚུགས་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="44"/>
        <source>enabled</source>
        <translation>སྤྱོད་འགོ་ཚུགས་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="44"/>
        <source>disabled</source>
        <translation>བཀག་སྡོམ་བྱས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="44"/>
        <source>static</source>
        <translation>ལྷིང་འཇགས་སུ་གནས་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="44"/>
        <source>transient</source>
        <translation>གནས་སྐབས་རིང་།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="44"/>
        <source>indirect</source>
        <translation>གཞན་བརྒྱུད་ཀྱི་སྒོ་ནས།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="44"/>
        <source>enabled-runtime</source>
        <translation>འཁོར་སྐྱོད་བྱེད་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="44"/>
        <source>generated</source>
        <translation>ཐོན་སྐྱེད་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="45"/>
        <source>Auto</source>
        <translation>རང་འགུལ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="45"/>
        <source>Manual</source>
        <translation>ལག་འགུལ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="45"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="149"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="151"/>
        <source>Load</source>
        <translation>སྣོན་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="153"/>
        <source>Active</source>
        <translation>འགོ་ཚུགས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="155"/>
        <source>Sub</source>
        <translation>ཡན་ལག་རུ་ཁག</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="157"/>
        <source>State</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="159"/>
        <source>Description</source>
        <translation>གསལ་བཤད།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="161"/>
        <source>PID</source>
        <translation>མདུན་སྐྱོད་ཨང་།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="163"/>
        <source>StartupMode</source>
        <translation>གསར་གཏོད་མ་ལག</translation>
    </message>
</context>
<context>
    <name>ServiceTableView</name>
    <message>
        <location filename="../service/servicetableview.cpp" line="102"/>
        <source>No search result</source>
        <translation>འཚོལ་ཞིབ་བྱས་འབྲས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="127"/>
        <source>Start</source>
        <translation>འགོ་རྩོམ་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="128"/>
        <source>Stop</source>
        <translation>མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="129"/>
        <source>Restart</source>
        <translation>ཡང་བསྐྱར་འགོ་འཛུགས།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="130"/>
        <source>Startup type</source>
        <translation>འགོ་ཚུགས་པའི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="131"/>
        <source>Auto</source>
        <translation>རང་འགུལ།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="132"/>
        <source>Manual</source>
        <translation>ལག་འགུལ།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="133"/>
        <source>Refresh</source>
        <translation>གསར་བསྒྱུར།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="138"/>
        <source>Load</source>
        <translation>སྣོན་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="140"/>
        <source>Active</source>
        <translation>བྱ་འགུལ།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="142"/>
        <source>Sub</source>
        <translation>ལག་བསྟར་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="144"/>
        <source>State</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="146"/>
        <source>Startup Type</source>
        <translation>འགོ་ཚུགས་པའི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="148"/>
        <source>Description</source>
        <translation>གསལ་བཤད།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="150"/>
        <source>PID</source>
        <translation>མདུན་སྐྱོད་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="307"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="412"/>
        <location filename="../service/servicetableview.cpp" line="469"/>
        <location filename="../service/servicetableview.cpp" line="524"/>
        <source>Service instance name</source>
        <translation>ཞབས་ཞུའི་དངོས་པོའི་མིང་།</translation>
    </message>
</context>
<context>
    <name>USMAboutDialog</name>
    <message>
        <location filename="../dialog/usmaboutdialog.cpp" line="123"/>
        <location filename="../dialog/usmaboutdialog.cpp" line="149"/>
        <source>Kylin System Monitor</source>
        <translation>ཆི་ལིན་མ་ལག་ལྟ་ཞིབ་འཕྲུལ་ཆས།</translation>
    </message>
    <message>
        <location filename="../dialog/usmaboutdialog.cpp" line="154"/>
        <source>version: </source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ། </translation>
    </message>
    <message>
        <location filename="../dialog/usmaboutdialog.cpp" line="159"/>
        <source>System monitor is a desktop application that face desktop users of Kylin operating system,It meets the needs of users to monitor the system process, system resources and file system</source>
        <translation>མ་ལག་སོ་ལྟ་ཆས་ནི་ཆི་ལིན་བཀོལ་སྤྱོད་མ་ལག་སྤྱོད་མཁན་ལ་ཁ་ཕྱོགས་པའི་ཉེར་སྤྱོད་ཞིག་ཡིན་པ་དང་།དེས་སྤྱོད་མཁན་གྱི་མ་ལག་སོ་ལྟ་ཆས་དང་།འཕེལ་རིམ་ཐོན་ཁུངས་དང་ཡིག་ཆའི་མ་ལག་ལྟ་སྐུལ་ཚོད་འཛིན་གྱི་དགོས་མཁོ་སྐོང་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../dialog/usmaboutdialog.cpp" line="171"/>
        <source>Service and support team:</source>
        <translation>ཞབས་ཞུ་དང་རོགས་སྐྱོར་ཚོགས་པ་གཤམ་གསལ།</translation>
    </message>
</context>
</TS>
