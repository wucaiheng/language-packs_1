<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>AboutDialog</name>
    <message>
        <source>Scanner is an interface-friendly scanner, which could be also used as one-click beautification, intelligent correction and text recognition tools.</source>
        <translation type="vanished">扫描是一款可用于普通扫描、一键美化、智能纠偏和文字识别的界面友好扫描仪软件。</translation>
    </message>
    <message>
        <source>Service &amp; Support : </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
</context>
<context>
    <name>DefaultConnectFailedPageWidget</name>
    <message>
        <source>Not detect scanners, please connect scanners firstly!</source>
        <translation type="vanished">未检测到可用扫描设备，请先连接扫描设备</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接扫描仪</translation>
    </message>
</context>
<context>
    <name>DefaultConnectSuccessPageLeftWidget</name>
    <message>
        <source>Connect scanners, please click scan button to start scanning.</source>
        <translation type="vanished">已连接扫描设备，点击按钮开始扫描</translation>
    </message>
</context>
<context>
    <name>DefaultConnectSuccessPageRightWidget</name>
    <message>
        <source>Begin Scan</source>
        <translation type="vanished">开始扫描</translation>
    </message>
    <message>
        <source>Device</source>
        <translation type="vanished">设备</translation>
    </message>
    <message>
        <source>Scanner device</source>
        <translation type="vanished">扫描仪设备</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">延时</translation>
    </message>
    <message>
        <source>File settings</source>
        <translation type="vanished">文件预设</translation>
    </message>
    <message>
        <source>Pages</source>
        <translation type="vanished">页数</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">类型</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">色彩</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation type="vanished">分辨率</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">尺寸</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="vanished">格式</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">名称</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">扫描至</translation>
    </message>
    <message>
        <source>Send email</source>
        <translation type="vanished">发送至邮箱</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="vanished">另存为</translation>
    </message>
</context>
<context>
    <name>DetectPageWidget</name>
    <message>
        <source>Detect scanners, please waiting ...</source>
        <translation type="vanished">检测扫描设备中，请稍后 ……</translation>
    </message>
    <message>
        <location filename="../src/detectpagewidget.cpp" line="44"/>
        <source>Detect scanners, please waiting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FailedPageWidget</name>
    <message>
        <location filename="../src/failedpagewidget.cpp" line="42"/>
        <source>Not detect scanners, please connect scanners firstly!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/failedpagewidget.cpp" line="45"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageOperationOCR</name>
    <message>
        <location filename="../src/imageOp/imageoperationocr.cpp" line="24"/>
        <location filename="../src/imageOp/imageoperationocr.cpp" line="32"/>
        <source>Unable to read text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KYCAboutDialog</name>
    <message>
        <location filename="../src/about/about.ui" line="26"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scanner is an interface-friendly scanner, which could be also used as one-click beautification, intelligent correction and text recognition tools.</source>
        <translation type="vanished">扫描是一款可用于普通扫描、一键美化、智能纠偏和文字识别的界面友好扫描仪软件。</translation>
    </message>
    <message>
        <source>Service &amp; Support : </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
</context>
<context>
    <name>LeftSuccessPageWidget</name>
    <message>
        <location filename="../src/leftsuccesspagewidget.cpp" line="58"/>
        <source>Connect scanners, please click scan button to start scanning.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <location filename="../src/mainwidget.cpp" line="76"/>
        <source>kylin-scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="207"/>
        <source>Scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="210"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is a new scanner connect, please restart this application manually. </source>
        <translation type="vanished">存在新设备连接，请手动重启应用使用该新设备。</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="462"/>
        <location filename="../src/mainwidget.cpp" line="482"/>
        <location filename="../src/mainwidget.cpp" line="520"/>
        <source>There is a new scanner connect, redetect all scanners, please wait a moment. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="493"/>
        <location filename="../src/mainwidget.cpp" line="564"/>
        <source>No available device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="577"/>
        <location filename="../src/mainwidget.cpp" line="595"/>
        <source>device </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="577"/>
        <location filename="../src/mainwidget.cpp" line="595"/>
        <source> has been disconnect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="674"/>
        <source>Single</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="716"/>
        <source>Invalid argument, please change arguments or switch other scanners.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="716"/>
        <location filename="../src/mainwidget.cpp" line="737"/>
        <location filename="../src/mainwidget.cpp" line="739"/>
        <location filename="../src/mainwidget.cpp" line="741"/>
        <location filename="../src/mainwidget.cpp" line="744"/>
        <source>error code:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>error code: </source>
        <translation type="vanished">错误码：</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="737"/>
        <source>Device busy, please wait or switch other scanners.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="739"/>
        <source>Document feeder out of documents, please place papers and scan again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="741"/>
        <source>Scan operation has been cancelled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="744"/>
        <source>Scan failed, please check your scanner or switch other scanners.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="791"/>
        <source>Running beauty ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="813"/>
        <source>Running rectify ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NoMailDialog</name>
    <message>
        <location filename="../src/sendmail.cpp" line="52"/>
        <location filename="../src/sendmail.cpp" line="84"/>
        <source>No email client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="58"/>
        <source>Scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="61"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="98"/>
        <source>Not find email client in the system, please install email client firstly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="113"/>
        <source>Install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="110"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OcrObject</name>
    <message>
        <location filename="../src/ocrobject.cpp" line="16"/>
        <location filename="../src/ocrobject.cpp" line="26"/>
        <source>Unable to read text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OcrThread</name>
    <message>
        <location filename="../src/mainwidget.cpp" line="1034"/>
        <location filename="../src/mainwidget.cpp" line="1047"/>
        <source>Unable to read text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>User </source>
        <translation type="vanished">用户</translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="287"/>
        <source>Question</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="284"/>
        <source>Current </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="284"/>
        <source> User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="285"/>
        <source> has already opened kylin-scanner, open will close </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="286"/>
        <source>&apos;s operations. Are you continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="737"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="744"/>
        <source>Gray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="750"/>
        <source>Lineart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="799"/>
        <source>Default Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="878"/>
        <source>Flatbed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="888"/>
        <source>ADF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="897"/>
        <source>ADF Front</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="906"/>
        <source>ADF Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="915"/>
        <source>ADF Duplex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="965"/>
        <location filename="../src/saneobject.cpp" line="1029"/>
        <source>4800 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="968"/>
        <location filename="../src/saneobject.cpp" line="1033"/>
        <source>2400 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="971"/>
        <location filename="../src/saneobject.cpp" line="1037"/>
        <source>1200 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="974"/>
        <location filename="../src/saneobject.cpp" line="1041"/>
        <source>600 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="977"/>
        <location filename="../src/saneobject.cpp" line="1045"/>
        <source>300 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="980"/>
        <location filename="../src/saneobject.cpp" line="1049"/>
        <source>200 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="983"/>
        <location filename="../src/saneobject.cpp" line="1053"/>
        <source>150 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="986"/>
        <location filename="../src/saneobject.cpp" line="1057"/>
        <source>100 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="989"/>
        <location filename="../src/saneobject.cpp" line="1061"/>
        <source>75 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1007"/>
        <location filename="../src/saneobject.cpp" line="1077"/>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RunningDialog</name>
    <message>
        <location filename="../src/runningdialog.cpp" line="54"/>
        <location filename="../src/runningdialog.cpp" line="141"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/runningdialog.cpp" line="84"/>
        <location filename="../src/runningdialog.cpp" line="171"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaneObject</name>
    <message>
        <location filename="../src/saneobject.cpp" line="2032"/>
        <source>Default Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2024"/>
        <source>Flatbed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2026"/>
        <source>ADF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2028"/>
        <source>ADF Front</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2030"/>
        <source>ADF Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lineart</source>
        <translation type="vanished">黑白</translation>
    </message>
    <message>
        <source>Gray</source>
        <translation type="vanished">灰度</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">彩色</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2034"/>
        <source>ADF Duplex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2106"/>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2110"/>
        <source>75 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2112"/>
        <source>150 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2114"/>
        <source>200 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2116"/>
        <source>300 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2118"/>
        <source>600 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2120"/>
        <source>1200 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2122"/>
        <source>2400 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2124"/>
        <source>4800 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2209"/>
        <source>Multiple</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScanDialog</name>
    <message>
        <location filename="../src/scandialog.cpp" line="28"/>
        <location filename="../src/scandialog.cpp" line="31"/>
        <source>Scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="34"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="52"/>
        <location filename="../src/scandialog.cpp" line="154"/>
        <source>Number of pages scanning: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of pages being scanned: </source>
        <translation type="vanished">正在扫描页数：</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="61"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="123"/>
        <source>Canceling scan，please wait a moment!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="152"/>
        <source>Multiple</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScanSettingsWidget</name>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="511"/>
        <source>Begin Scan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="534"/>
        <source>Scanner device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="465"/>
        <location filename="../src/scansettingswidget.cpp" line="664"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="549"/>
        <source>File settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="463"/>
        <source>Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="112"/>
        <source>Select a directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="134"/>
        <source>Currently user has no permission to modify directory </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="194"/>
        <location filename="../src/scansettingswidget.cpp" line="735"/>
        <source>Multiple</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="464"/>
        <source>Pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="466"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="238"/>
        <location filename="../src/scansettingswidget.cpp" line="795"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="468"/>
        <source>Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="469"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="470"/>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="471"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="556"/>
        <source>scanner01</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="472"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="477"/>
        <source>Mail to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send email</source>
        <translation type="vanished">发送至邮箱</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="575"/>
        <location filename="../src/scansettingswidget.cpp" line="927"/>
        <source>Save as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No available scanners</source>
        <translation type="vanished">无可用设备</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="205"/>
        <location filename="../src/scansettingswidget.cpp" line="735"/>
        <source>Single</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="251"/>
        <source>4800 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="252"/>
        <source>2400 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="253"/>
        <source>1200 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="255"/>
        <source>This resolution will take a loog time to scan, please choose carelly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="280"/>
        <source>cannot contain &apos;/&apos; character.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="286"/>
        <location filename="../src/scansettingswidget.cpp" line="379"/>
        <source>cannot save as hidden file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="368"/>
        <source>Save As</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="392"/>
        <source>Path without access rights: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="396"/>
        <source>File path that does not exist: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="919"/>
        <source>Store text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save as dialog</source>
        <translation type="vanished">另存为</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="415"/>
        <location filename="../src/scansettingswidget.cpp" line="436"/>
        <source>The file </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="416"/>
        <location filename="../src/scansettingswidget.cpp" line="437"/>
        <source> already exists, do you want to overwrite it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="418"/>
        <location filename="../src/scansettingswidget.cpp" line="439"/>
        <source>tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="467"/>
        <source>Colour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="749"/>
        <source>3s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="749"/>
        <source>5s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="749"/>
        <source>7s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="749"/>
        <source>10s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="749"/>
        <source>15s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="766"/>
        <source>Flatbed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="766"/>
        <source>ADF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="795"/>
        <source>Gray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="240"/>
        <location filename="../src/scansettingswidget.cpp" line="795"/>
        <source>Lineart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="818"/>
        <source>75 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="818"/>
        <source>100 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="818"/>
        <source>150 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="835"/>
        <source>Resolution is empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="850"/>
        <source>A4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="850"/>
        <source>A5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scanning images&apos;s length cannot be large than 252</source>
        <translation type="vanished">扫描文档名称的长度不能超过252。</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1153"/>
        <source>Scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1156"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScanThread</name>
    <message>
        <location filename="../src/mainwidget.cpp" line="881"/>
        <source>Multiple</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="929"/>
        <source>3s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="932"/>
        <source>5s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="935"/>
        <source>7s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="938"/>
        <source>10s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="941"/>
        <source>15s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SendMailDialog</name>
    <message>
        <location filename="../src/sendmail.cpp" line="216"/>
        <location filename="../src/sendmail.cpp" line="240"/>
        <source>Select email client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="222"/>
        <source>Scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="225"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="254"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="257"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowImageWidget</name>
    <message>
        <location filename="../src/showimagewidget.cpp" line="822"/>
        <source>Running beauty ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="858"/>
        <source>Running rectify ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="29"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="109"/>
        <location filename="../src/titlebar/titlebar.h" line="83"/>
        <source>kylin-scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="112"/>
        <location filename="../src/titlebar/titlebar.cpp" line="170"/>
        <source>Scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="175"/>
        <source>Option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="178"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="219"/>
        <source>Minimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="260"/>
        <location filename="../src/titlebar/titlebar.cpp" line="114"/>
        <source>Maximize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="307"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maxmize</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="37"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="41"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="44"/>
        <source>Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="45"/>
        <source>Message provides text chat and file transfer functions in the LAN. There is no need to build a server. It supports multiple people to interact at the same time and send and receive in parallel.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="56"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="111"/>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="168"/>
        <source>The current document is not saved. Do you want to save it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="174"/>
        <source>Straight &amp;Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="176"/>
        <source>&amp;Save Exit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBarWidget</name>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="36"/>
        <source>Beauty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="41"/>
        <source>Rectify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="46"/>
        <source>OCR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="53"/>
        <source>Crop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="58"/>
        <source>Rotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="63"/>
        <source>Mirror</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="68"/>
        <source>Watermark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="85"/>
        <source>ZoomIn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="80"/>
        <source>ZoomOut</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UsbHotplugThread</name>
    <message>
        <source>device </source>
        <translation type="vanished">设备 </translation>
    </message>
    <message>
        <source> has been disconnect.</source>
        <translation type="vanished"> 已经断开连接！</translation>
    </message>
    <message>
        <source>device has been disconnect.</source>
        <translation type="vanished">设备已断开！</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="71"/>
        <source>Scanner has been disconnect.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatermarkDialog</name>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="27"/>
        <location filename="../src/watermarkdialog.cpp" line="33"/>
        <source>Scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="36"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="50"/>
        <source>Add watermark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="66"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="69"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/main.cpp" line="202"/>
        <source>Open file &lt;filename&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="203"/>
        <source>Filename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="207"/>
        <source>Hide scan settings widget</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>showOcrWidget</name>
    <message>
        <location filename="../src/showocrwidget.cpp" line="30"/>
        <location filename="../src/showocrwidget.cpp" line="146"/>
        <location filename="../src/showocrwidget.cpp" line="154"/>
        <source>The document is in character recognition ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
