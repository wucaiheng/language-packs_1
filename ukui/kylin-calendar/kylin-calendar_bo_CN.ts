<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../UI/aboutdialog.cpp" line="26"/>
        <source>Service &amp; Support: </source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར་ཚོགས་པ། </translation>
    </message>
    <message>
        <location filename="../UI/aboutdialog.cpp" line="30"/>
        <location filename="../UI/aboutdialog.cpp" line="97"/>
        <source>Version: </source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ། </translation>
    </message>
    <message>
        <location filename="../UI/aboutdialog.cpp" line="58"/>
        <location filename="../UI/aboutdialog.cpp" line="72"/>
        <source>Calendar</source>
        <translation>ལོ་ཐོ།</translation>
    </message>
    <message>
        <location filename="../UI/aboutdialog.cpp" line="63"/>
        <source>close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>CSchceduleDlg</name>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="140"/>
        <location filename="../UI/schedulewidget.cpp" line="148"/>
        <source>New Event</source>
        <translation>བྱེད་སྒོ་གསར་པ།</translation>
    </message>
    <message>
        <source>Edit Event</source>
        <translation type="vanished">编辑日程</translation>
    </message>
    <message>
        <source>New Schedule</source>
        <translation type="vanished">新建日程</translation>
    </message>
    <message>
        <source>Type:</source>
        <translation type="vanished">类型:</translation>
    </message>
    <message>
        <source>Work</source>
        <translation type="vanished">工作</translation>
    </message>
    <message>
        <source>Life</source>
        <translation type="vanished">生活</translation>
    </message>
    <message>
        <source>Other</source>
        <translation type="vanished">其他</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation type="vanished">描述:</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="158"/>
        <source>All Day:</source>
        <translation>ཉིན་གང་བོར།</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="169"/>
        <source>Starts:</source>
        <translation>འགོ་འཛུགས་རྒྱུ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="200"/>
        <source>Ends:</source>
        <translation>མཇུག་བསྒྲིལ་རྒྱུ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="237"/>
        <source>Theme:</source>
        <translation>བརྗོད་བྱ་གཙོ་བོ་ནི།</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="247"/>
        <source>Remind Me:</source>
        <translation>ང་ལ་དྲན་སྐུལ་བྱས་དོན།</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="258"/>
        <source>Repeat:</source>
        <translation>ཡང་བསྐྱར་ཐེངས་གཅིག་ལ།</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="275"/>
        <source>Frequency:</source>
        <translation>འགུལ་ཚད།</translation>
    </message>
    <message>
        <source>Never</source>
        <translation type="vanished">从不</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="271"/>
        <source>Daily</source>
        <translation>ཉིན་རེའི།</translation>
    </message>
    <message>
        <source>Weekdays</source>
        <translation type="vanished">周末</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="272"/>
        <source>Weekly</source>
        <translation>གཟའ་འཁོར་རེར།</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="273"/>
        <source>Monthly</source>
        <translation>ཟླ་རེར།</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="274"/>
        <source>Yearly</source>
        <translation>ལོ་རེ་བཞིན།</translation>
    </message>
    <message>
        <source>End Repeat:</source>
        <translation type="vanished">最后重复:</translation>
    </message>
    <message>
        <source>After</source>
        <translation type="vanished">之后</translation>
    </message>
</context>
<context>
    <name>DayItem</name>
    <message>
        <location filename="../UI/dayitem.cpp" line="36"/>
        <source>解析json文件错误！</source>
        <translation>jsonཡི་ཡིག་ཆའི་ནོར་འཁྲུལ་ལ་དབྱེ་ཞིབ་བྱས།</translation>
    </message>
</context>
<context>
    <name>DayView</name>
    <message>
        <location filename="../UI/dayview.cpp" line="101"/>
        <source>Monday</source>
        <translation>གཟའ་ཟླ་བའི་ཉིན།</translation>
    </message>
    <message>
        <location filename="../UI/dayview.cpp" line="103"/>
        <source>Tuesday</source>
        <translation>གཟའ་མིག་དམར་ཉིན།</translation>
    </message>
    <message>
        <location filename="../UI/dayview.cpp" line="105"/>
        <source>Wednesday</source>
        <translation>གཟའ་ལྷག་པ།</translation>
    </message>
    <message>
        <location filename="../UI/dayview.cpp" line="107"/>
        <source>Thursday</source>
        <translation>གཟའ་ཕུར་བུ།</translation>
    </message>
    <message>
        <location filename="../UI/dayview.cpp" line="109"/>
        <source>Friday</source>
        <translation>གཟའ་པ་སངས་ཉིན།</translation>
    </message>
    <message>
        <location filename="../UI/dayview.cpp" line="111"/>
        <source>Saturday</source>
        <translation>གཟའ་སྤེན་པའི་ཉིན།</translation>
    </message>
    <message>
        <location filename="../UI/dayview.cpp" line="113"/>
        <source>Sunday</source>
        <translation>གཟའ་ཉི་མ།</translation>
    </message>
</context>
<context>
    <name>HeaderWidget</name>
    <message>
        <location filename="../UI/headerwidget.cpp" line="10"/>
        <source>腊月</source>
        <translation>རྒྱལ་ཟླ།</translation>
    </message>
    <message>
        <location filename="../UI/headerwidget.cpp" line="16"/>
        <source>AllDay</source>
        <translation>ཉིན་གང་བོར།</translation>
    </message>
    <message>
        <location filename="../UI/headerwidget.cpp" line="22"/>
        <source>00</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LeftHeaderWidget</name>
    <message>
        <source>AllDay</source>
        <translation type="obsolete">全天</translation>
    </message>
</context>
<context>
    <name>LeftWidget</name>
    <message>
        <location filename="../UI/leftwidget.cpp" line="22"/>
        <location filename="../UI/leftwidget.cpp" line="82"/>
        <source>Create Schedule</source>
        <translation>གསར་དུ་བཙུགས་པའི་ཉིན་རེའི།</translation>
    </message>
    <message>
        <location filename="../UI/leftwidget.cpp" line="37"/>
        <source>Yi Ji</source>
        <translation>འཛེམ་བྱ།</translation>
    </message>
    <message>
        <location filename="../UI/leftwidget.cpp" line="50"/>
        <source>Prev</source>
        <translation>ཟླ་བ་སྔོན་མ།</translation>
    </message>
    <message>
        <location filename="../UI/leftwidget.cpp" line="53"/>
        <source>Next</source>
        <translation>ཟླ་བ་རྗེས་མར།</translation>
    </message>
    <message>
        <source>Mon</source>
        <translation type="obsolete">周一</translation>
    </message>
    <message>
        <source>Tue</source>
        <translation type="obsolete">周二</translation>
    </message>
    <message>
        <source>Wed</source>
        <translation type="obsolete">周三</translation>
    </message>
    <message>
        <source>Thu</source>
        <translation type="obsolete">周四</translation>
    </message>
    <message>
        <source>Fri</source>
        <translation type="obsolete">周五</translation>
    </message>
    <message>
        <source>Sat</source>
        <translation type="obsolete">周六</translation>
    </message>
    <message>
        <source>Sun</source>
        <translation type="obsolete">周日</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <location filename="../UI/mainwidget.cpp" line="81"/>
        <source>Calendar</source>
        <translation>ལོ་ཐོ།</translation>
    </message>
</context>
<context>
    <name>MonthItemWidget</name>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="63"/>
        <source>Jan</source>
        <translation>ཟླ་བ་དང་པོ།</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="65"/>
        <source>Feb</source>
        <translation>ཟླ་2པར།</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="67"/>
        <source>Mar</source>
        <translation>ཟླ་གསུམ་པ།</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="69"/>
        <source>Apr</source>
        <translation>ཟླ་བ་4</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="71"/>
        <source>May</source>
        <translation>ཟླ་བ་ལྔ་པ།</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="73"/>
        <source>Jun</source>
        <translation>ཟླ་བ་དྲུག་པ།</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="75"/>
        <source>Jul</source>
        <translation>ཟླ་བ་བདུན་པ།</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="77"/>
        <source>Aug</source>
        <translation>ཟླ་བརྒྱད་པ།</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="79"/>
        <source>Sep</source>
        <translation>ཟླ་བ་དགུ་བ།</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="81"/>
        <source>Oct</source>
        <translation>ཟླ་བཅུ་བ།</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="83"/>
        <source>Nov</source>
        <translation>ཟླ་བ་བཅུ་གཅིག</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="85"/>
        <source>Dec</source>
        <translation>ཟླ་བ་བཅུ་གཉིས་པ།</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="95"/>
        <source>M</source>
        <translation>གཅིག</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="95"/>
        <source>Tu</source>
        <translation>གཉིས།</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="95"/>
        <source>Th</source>
        <translation>བཞི།</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="95"/>
        <source>Sa</source>
        <translation>དྲུག</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="95"/>
        <source>Su</source>
        <translation>ཉིན།</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="95"/>
        <source>W</source>
        <translation>གསུམ།</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="95"/>
        <source>F</source>
        <translation>ལྔ།</translation>
    </message>
</context>
<context>
    <name>MonthView</name>
    <message>
        <location filename="../UI/monthview.cpp" line="154"/>
        <source>Mon</source>
        <translation>གཟའ་ཟླ་བ།</translation>
    </message>
    <message>
        <location filename="../UI/monthview.cpp" line="154"/>
        <source>Tue</source>
        <translation>གཟའ་མིག་དམར།</translation>
    </message>
    <message>
        <location filename="../UI/monthview.cpp" line="154"/>
        <source>Wed</source>
        <translation>གཟའ་ལྷག་པ།</translation>
    </message>
    <message>
        <location filename="../UI/monthview.cpp" line="154"/>
        <source>Thu</source>
        <translation>གཟའ་ཕུར་བུ།</translation>
    </message>
    <message>
        <location filename="../UI/monthview.cpp" line="154"/>
        <source>Fri</source>
        <translation>གཟའ་པ་སངས།</translation>
    </message>
    <message>
        <location filename="../UI/monthview.cpp" line="154"/>
        <source>Sat</source>
        <translation>གཟའ་སྤེན་པ།</translation>
    </message>
    <message>
        <location filename="../UI/monthview.cpp" line="154"/>
        <source>Sun</source>
        <translation>གཟའ་ཉི་མ།</translation>
    </message>
</context>
<context>
    <name>RightWidget</name>
    <message>
        <location filename="../UI/rightwidget.cpp" line="22"/>
        <location filename="../UI/rightwidget.cpp" line="156"/>
        <source>Day</source>
        <translation>ཉིན་མོ།</translation>
    </message>
    <message>
        <location filename="../UI/rightwidget.cpp" line="23"/>
        <source>Week</source>
        <translation>གཟའ་འཁོར་གཅིག</translation>
    </message>
    <message>
        <location filename="../UI/rightwidget.cpp" line="27"/>
        <location filename="../UI/rightwidget.cpp" line="148"/>
        <location filename="../UI/rightwidget.cpp" line="152"/>
        <location filename="../UI/rightwidget.cpp" line="156"/>
        <source>Month</source>
        <translation>ཟླ་བ་གཅིག</translation>
    </message>
    <message>
        <location filename="../UI/rightwidget.cpp" line="28"/>
        <location filename="../UI/rightwidget.cpp" line="143"/>
        <location filename="../UI/rightwidget.cpp" line="147"/>
        <location filename="../UI/rightwidget.cpp" line="151"/>
        <location filename="../UI/rightwidget.cpp" line="155"/>
        <location filename="../UI/rightwidget.cpp" line="223"/>
        <location filename="../UI/rightwidget.cpp" line="226"/>
        <source>Year</source>
        <translation>ལོ་གཅིག</translation>
    </message>
    <message>
        <location filename="../UI/rightwidget.cpp" line="29"/>
        <source>Today</source>
        <translation>དེ་རིང་།</translation>
    </message>
</context>
<context>
    <name>ScheduleMark</name>
    <message>
        <location filename="../UI/schedulemark.cpp" line="75"/>
        <source>Edit</source>
        <translation>རྩོམ་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../UI/schedulemark.cpp" line="76"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
</context>
<context>
    <name>TitleWidget</name>
    <message>
        <location filename="../UI/titlewidget.cpp" line="21"/>
        <location filename="../UI/titlewidget.cpp" line="51"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../UI/titlewidget.cpp" line="22"/>
        <location filename="../UI/titlewidget.cpp" line="54"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../UI/titlewidget.cpp" line="23"/>
        <location filename="../UI/titlewidget.cpp" line="57"/>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན་བྱ་རྒྱུ།</translation>
    </message>
    <message>
        <location filename="../UI/titlewidget.cpp" line="88"/>
        <source>Calendar</source>
        <translation>ལོ་ཐོ།</translation>
    </message>
    <message>
        <location filename="../UI/titlewidget.cpp" line="91"/>
        <source>Menu</source>
        <translation>འདེམས་པང་།</translation>
    </message>
    <message>
        <location filename="../UI/titlewidget.cpp" line="100"/>
        <source>Minimize</source>
        <translation>ཆེས་ཆུང་དུ་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../UI/titlewidget.cpp" line="107"/>
        <source>Max</source>
        <translation>ཆེས་ཆེར་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../UI/titlewidget.cpp" line="117"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>WeekView</name>
    <message>
        <source>AllDay</source>
        <translation type="vanished">全天</translation>
    </message>
    <message>
        <location filename="../UI/weekview.cpp" line="130"/>
        <source> </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../UI/weekview.cpp" line="130"/>
        <source>Mon</source>
        <translation>གཟའ་ཟླ་བ།</translation>
    </message>
    <message>
        <location filename="../UI/weekview.cpp" line="130"/>
        <source>Tue</source>
        <translation>གཟའ་མིག་དམར།</translation>
    </message>
    <message>
        <location filename="../UI/weekview.cpp" line="130"/>
        <source>Wed</source>
        <translation>གཟའ་ལྷག་པ།</translation>
    </message>
    <message>
        <location filename="../UI/weekview.cpp" line="130"/>
        <source>Thu</source>
        <translation>གཟའ་ཕུར་བུ།</translation>
    </message>
    <message>
        <location filename="../UI/weekview.cpp" line="130"/>
        <source>Fri</source>
        <translation>གཟའ་པ་སངས།</translation>
    </message>
    <message>
        <location filename="../UI/weekview.cpp" line="130"/>
        <source>Sat</source>
        <translation>གཟའ་སྤེན་པ།</translation>
    </message>
    <message>
        <location filename="../UI/weekview.cpp" line="130"/>
        <source>Sun</source>
        <translation>གཟའ་ཉི་མ།</translation>
    </message>
</context>
</TS>
