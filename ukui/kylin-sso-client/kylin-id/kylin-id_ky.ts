<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>KYComboBox</name>
    <message>
        <location filename="../../common-ui/kycombobox.cpp" line="48"/>
        <source>Your Email/Name/Phone</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Kylin ID Center</source>
        <translation type="vanished">麒麟ID登录中心</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="130"/>
        <source>Forget?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kylin ID</source>
        <translation type="vanished">麒麟ID</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="129"/>
        <source>Kylin ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="135"/>
        <source>Remember it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="305"/>
        <source>Your password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your Email/Name/Phone</source>
        <translation type="vanished">邮箱/用户名/手机号码</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="309"/>
        <source>Pass login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="310"/>
        <source>Phone login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="311"/>
        <location filename="../mainwindow.cpp" line="683"/>
        <location filename="../mainwindow.cpp" line="770"/>
        <location filename="../mainwindow.cpp" line="838"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="312"/>
        <location filename="../mainwindow.cpp" line="789"/>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="306"/>
        <source>Please wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your Email/Name/Phone here</source>
        <translation type="vanished">请输入邮箱/用户名/手机号码</translation>
    </message>
    <message>
        <source>Your password here</source>
        <translation type="vanished">请输入密码</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="136"/>
        <source>Register</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="307"/>
        <source>Your code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="308"/>
        <source>Your phone number here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="339"/>
        <location filename="../mainwindow.cpp" line="592"/>
        <source>Please move slider to right place</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="795"/>
        <source>%1s left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1086"/>
        <source>User stop verify Captcha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1087"/>
        <source>Parsing data failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1088"/>
        <location filename="../mainwindow.cpp" line="1095"/>
        <source>No response data!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1089"/>
        <source>Timeout!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1090"/>
        <source>Server internal error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1091"/>
        <location filename="../mainwindow.cpp" line="1099"/>
        <location filename="../mainwindow.cpp" line="1101"/>
        <source>Phone number error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1093"/>
        <location filename="../mainwindow.cpp" line="1103"/>
        <source>Pictrure has expired!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1094"/>
        <source>User deleted!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation type="vanished">手机号码已经在使用！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1098"/>
        <source>Your are reach the limit!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please check your phone number!</source>
        <translation type="vanished">请检查您的手机号码！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1100"/>
        <source>Please check your code!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1104"/>
        <source>Pictrure blocked!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1105"/>
        <source>Illegal code!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1106"/>
        <source>Phone code is expired!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1107"/>
        <source>Failed attemps limit reached!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1109"/>
        <source>Slider validate error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1110"/>
        <source>Phone code error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1111"/>
        <source>Code can not be empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1112"/>
        <source>MCode can not be empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1113"/>
        <source>Please check account status!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account doesn&apos;t exists!</source>
        <translation type="vanished">用户未注册！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1092"/>
        <source>No network!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1096"/>
        <source>Phone number exsists!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1097"/>
        <source>Wrong phone number format!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1102"/>
        <source>Send sms Limited!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Too many attemps,you will lock in 5 min!</source>
        <translation type="vanished">尝试过多，锁定5分钟!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1108"/>
        <source>Wrong account or password!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wrong phone code!</source>
        <translation type="vanished">手机验证码错误!</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exist!</source>
        <translation type="vanished">用户未注册！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1114"/>
        <source>Unsupported operation!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1115"/>
        <source>Unsupported Client Type!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1116"/>
        <source>Please check your input!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1118"/>
        <source>Process failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WechatLogin</name>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="114"/>
        <source>Register</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="123"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="137"/>
        <location filename="../wechat/wechatlogin.cpp" line="468"/>
        <source>Scan Code for Quick Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="299"/>
        <location filename="../wechat/wechatlogin.cpp" line="347"/>
        <source>Fail to Get QRcode,Please Retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="313"/>
        <source>QRcode Invaild,Please Click to Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="327"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="424"/>
        <source>Not register yet, click to register</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="445"/>
        <source>Click login button after register successful</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
