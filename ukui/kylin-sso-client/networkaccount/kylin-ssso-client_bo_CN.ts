<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>CloudSyncUI::GlobalVariant</name>
    <message>
        <source>Your account:%1</source>
        <translation type="vanished">您的帐户：%1</translation>
    </message>
    <message>
        <source>Auto sync</source>
        <translation type="vanished">自动同步</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="vanished">未连接</translation>
    </message>
    <message>
        <source>Sync your settings</source>
        <translation type="vanished">同步您的设置</translation>
    </message>
    <message>
        <source>Sync failed!</source>
        <translation type="vanished">同步存在失败项！</translation>
    </message>
    <message>
        <source>CloudSync service may crashed!</source>
        <translation type="vanished">云帐户功能可能没有开启！</translation>
    </message>
    <message>
        <source>Please ensure CloudSync installed!</source>
        <translation type="vanished">确保云帐户服务已经安装！</translation>
    </message>
    <message>
        <source>Sync processing!</source>
        <translation type="vanished">同步中！</translation>
    </message>
    <message>
        <source>Kylin Cloud Account</source>
        <translation type="vanished">云帐户</translation>
    </message>
    <message>
        <source>Cloud ID desktop message</source>
        <translation type="vanished">消息</translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <location filename="../firstpage.cpp" line="20"/>
        <source>Service is not valid for private server</source>
        <translation>སྒེར་གྱི་ཞབས་ཞུའི་ཡོ་བྱད་ལ་ཞབས་ཞུ་ནུས་པ་མེད།</translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="100"/>
        <source>Sync your settings across the other devices!</source>
        <translation>དུས་མཉམ་དུ་ཁྱེད་ཀྱི་ཐོ་ཁོངས་གཞི་གྲངས་དང་རང་གཤིས་ཅན་གྱི་སྒྲིག་བཀོད་སྒྲིག་ཆས་གཞན་དག</translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="110"/>
        <source>Network is not accessible</source>
        <translation>དྲ་རྒྱ་ཤར་གཏོང་ཐུབ་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="111"/>
        <source>Ensure internet accessibility</source>
        <translation>དྲ་རྒྱ་སྦྲེལ་རྗེས་ད་གཟོད་སྤྲིན་གྱི་རྩིས་ཐོའི་ནུས་པ་བཀོལ་ཆོག</translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="134"/>
        <source>Sign In</source>
        <translation>ནང་འཛུལ།</translation>
    </message>
</context>
<context>
    <name>FrameItem</name>
    <message>
        <source>Auto-sync</source>
        <translation type="vanished">自动同步</translation>
    </message>
    <message>
        <location filename="../frameitem.cpp" line="77"/>
        <source>Some items sync failed!</source>
        <translation>རྣམ་གྲངས་ཁ་ཤས་དུས་གཅིག་ཏུ་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../frameitem.cpp" line="78"/>
        <source>Watting for sync!</source>
        <translation>མཉམ་དུ་སྒུག་ནས་བསྡད་ཡོད།</translation>
    </message>
    <message>
        <location filename="../frameitem.cpp" line="128"/>
        <source>Auto-Sync</source>
        <translation>རང་འགུལ་གྱིས་དུས་མཉམ་དུ།</translation>
    </message>
</context>
<context>
    <name>FrameList</name>
    <message>
        <location filename="../framelist.cpp" line="22"/>
        <source>Wallpaper</source>
        <translation>ཅོག་ངོས་རྒྱབ་ལྗོངས།</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="25"/>
        <source>ScreenSaver</source>
        <translation>བརྙན་ཡོལ་སྲུང་སྐྱོབ་བྱ་རིམ།</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="28"/>
        <source>Peony</source>
        <translation>ཡིག་ཆའི་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="31"/>
        <source>Power</source>
        <translation>གློག་ཁུངས་དོ་དམ།</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="34"/>
        <source>Themes</source>
        <translation>བརྗོད་བྱ་གཙོ་བོ།</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="37"/>
        <source>Touchpad</source>
        <translation>རེག་ཐུག་པང་ལེབ།</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="40"/>
        <source>Quick Launch</source>
        <translation>མགྱོགས་མྱུར་ངང་སྒོ་འབྱེད་ར་བ།</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="43"/>
        <source>Panel</source>
        <translation>ལས་འགན་སྡེར།</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="46"/>
        <source>Mouse</source>
        <translation>ཙིག་རྟགས།</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="49"/>
        <source>Menu</source>
        <translation>འདེམས་པང་།</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="52"/>
        <source>Font</source>
        <translation>ཡིག་གཟུགས།</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="55"/>
        <source>Keyboard</source>
        <translation>མཐེབ་གཞོང་།</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="58"/>
        <source>Datetime</source>
        <translation>དུས་ཚོད་ཀྱི་རྣམ་བཞག</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="61"/>
        <source>Avatar</source>
        <translation>མགོ་པར།</translation>
    </message>
</context>
<context>
    <name>HeaderModel</name>
    <message>
        <location filename="../headermodel.cpp" line="25"/>
        <source>Change Password</source>
        <translation>གསང་གྲངས་བཟོ་བཅོས།</translation>
    </message>
    <message>
        <location filename="../headermodel.cpp" line="26"/>
        <source>Sign Out</source>
        <translation>མིང་རྟགས་འགོད་པ།</translation>
    </message>
</context>
<context>
    <name>KYComboBox</name>
    <message>
        <location filename="../../common-ui/kycombobox.cpp" line="48"/>
        <source>Your Email/Name/Phone</source>
        <translation>ཡིག་སྒམ།སྤྱོད་མཁན་གྱི་མིང་།ཁ་པར་ཨང་གྲངས།</translation>
    </message>
</context>
<context>
    <name>MCodeWidget</name>
    <message>
        <location filename="../mcodewidget.cpp" line="33"/>
        <source>SongTi</source>
        <translation>སུང་ཐའེ།</translation>
    </message>
</context>
<context>
    <name>MainDialog</name>
    <message>
        <source>Kylin ID Center</source>
        <translation type="vanished">麒麟ID登录中心</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="98"/>
        <source>Forget?</source>
        <translation>གསང་གྲངས་བརྗེད་པ།</translation>
    </message>
    <message>
        <source>kylin ID</source>
        <translation type="vanished">麒麟ID</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="97"/>
        <source>Kylin ID</source>
        <translation>ཅིན་ལིན་NM</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="103"/>
        <source>Remember it</source>
        <translation>གསང་གྲངས་ཡིད་ལ་འཛིན་དགོས།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="244"/>
        <source>Your password</source>
        <translation>ཁྱེད་ཀྱི་གསང་གྲངས་ནང་འཇུག</translation>
    </message>
    <message>
        <source>Your Email/Name/Phone</source>
        <translation type="vanished">邮箱/用户名/手机号码</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="248"/>
        <source>Pass login</source>
        <translation>ཐོ་འགོད་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="249"/>
        <source>Phone login</source>
        <translation>ཁ་པར་གྱི་ཐོ་འགོད།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="250"/>
        <location filename="../maindialog.cpp" line="632"/>
        <location filename="../maindialog.cpp" line="713"/>
        <location filename="../maindialog.cpp" line="747"/>
        <source>Login</source>
        <translation>ཐོ་འགོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="251"/>
        <location filename="../maindialog.cpp" line="732"/>
        <source>Send</source>
        <translation>སྐུར་སྐྱེལ།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="245"/>
        <source>Please wait</source>
        <translation>སྐུ་མཁྱེན་སྒུག་རོགས།</translation>
    </message>
    <message>
        <source>Your Email/Name/Phone here</source>
        <translation type="vanished">请输入邮箱/用户名/手机号码</translation>
    </message>
    <message>
        <source>Your password here</source>
        <translation type="vanished">请输入密码</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="104"/>
        <source>Register</source>
        <translation>ཐོ་འགོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="246"/>
        <source>Your code</source>
        <translation>ཁྱེད་ཀྱི་ཚབ་རྟགས་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="247"/>
        <source>Your phone number here</source>
        <translation>ཁྱེད་ཀྱི་འདི་གའི་ཁ་པར་ཨང་གྲངས་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="278"/>
        <location filename="../maindialog.cpp" line="548"/>
        <source>Please move slider to right place</source>
        <translation>ཁྱེད་ཀྱིས་ཤུད་ཆས་དེ་གཡས་ཕྱོགས་སུ་སྤོར་རོགས།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="738"/>
        <source>%1s left</source>
        <translation>%1སྐར་ཆ་ལྷག་ཡོད།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="931"/>
        <source>User stop verify Captcha</source>
        <translation>སྤྱོད་མཁན་གྱིས་ཁ་ཐེ་ཁྲ་ལ་ཞིབ་བཤེར་བྱེད་མཚམས་བཞག</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="932"/>
        <source>Parsing data failed!</source>
        <translation>གཞི་གྲངས་ལ་དབྱེ་ཞིབ་བྱས་ཀྱང་ཕམ་ཉེས་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="933"/>
        <location filename="../maindialog.cpp" line="940"/>
        <source>No response data!</source>
        <translation>དང་ལེན་མེད་པའི་གཞི་གྲངས།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="934"/>
        <source>Timeout!</source>
        <translation>དུས་ཚོད་ཕྱིར་འགོར་སོང་། ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱེད་རོགས།།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="935"/>
        <source>Server internal error!</source>
        <translation>ཞབས་ཞུའི་ཡོ་བྱད་ནང་ཁུལ་གྱི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="936"/>
        <location filename="../maindialog.cpp" line="944"/>
        <location filename="../maindialog.cpp" line="946"/>
        <source>Phone number error!</source>
        <translation>ཁྱོད་ཀྱི་ཁ་པར་ཨང་གྲངས་ལ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="938"/>
        <location filename="../maindialog.cpp" line="948"/>
        <source>Pictrure has expired!</source>
        <translation>འདྲ་པར་དུས་ལས་ཡོལ་འདུག</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="939"/>
        <source>User deleted!</source>
        <translation>སྤྱོད་མཁན་གྱིས་བསུབ་སོང་།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="958"/>
        <source>Please check account status!</source>
        <translation>ཁྱེད་ཀྱིས་རྩིས་ཐོའི་གནས་ཚུལ་ལ་ཞིབ་བཤེར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation type="vanished">手机号码已经在使用！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="943"/>
        <source>Your are reach the limit!</source>
        <translation>ཁ་པར་འདིས་ཉིན་དེར་འཕྲིན་ཐུང་བསྡུ་ལེན་བྱེད་པའི་ཐེངས་གྲངས་ཚད་བཀག་ལ་སླེབས་ཡོད།</translation>
    </message>
    <message>
        <source>Please check your phone number!</source>
        <translation type="vanished">手机号码其他错误！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="945"/>
        <source>Please check your code!</source>
        <translation>ཁ་པར་ཨང་གྲངས་གཞན་པ་ནོར་འདུག</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="949"/>
        <source>Pictrure blocked!</source>
        <translation>འདྲ་པར་གྱི་རྣམ་པ་ཆག་སོང་།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="950"/>
        <source>Illegal code!</source>
        <translation>ཁྲིམས་དང་མི་མཐུན་པའི་ཚབ་རྟགས།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="951"/>
        <source>Phone code is expired!</source>
        <translation>ཁ་པར་གྱི་ཨང་གྲངས་དུས་ལས་ཡོལ་ཟིན།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="952"/>
        <source>Failed attemps limit reached!</source>
        <translation>ཕམ་ཁ་བྱུང་བའི་ཚོད་འཛིན་གྱི་ཚད་ལ་སླེབས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="954"/>
        <source>Slider validate error</source>
        <translation>འདྲེད་བརྡར་གྱིས་ནོར་འཁྲུལ་ལ་ཚོད་དཔག་བྱས།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="955"/>
        <source>Phone code error!</source>
        <translation>ཁ་པར་ཨང་གྲངས་ནོར་སོང་།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="956"/>
        <source>Code can not be empty!</source>
        <translation>ཚབ་རྟགས་ནི་སྟོང་པ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="957"/>
        <source>MCode can not be empty!</source>
        <translation>ཨང་ཀི་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exists!</source>
        <translation type="vanished">用户未注册！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="937"/>
        <source>No network!</source>
        <translation>དྲ་རྒྱ་མེད།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="941"/>
        <source>Phone number exsists!</source>
        <translation>ཁ་པར་ཨང་གྲངས་ཡོད་པ།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="942"/>
        <source>Wrong phone number format!</source>
        <translation>ཁ་པར་ཨང་གྲངས་ཀྱི་རྣམ་གཞག་ནོར་སོང་།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="947"/>
        <source>Send sms Limited!</source>
        <translation>འཕྲིན་ཐུང་བསྐུར་ན་ཚད་བཀག་ལ་ཐོན་ཡོད།</translation>
    </message>
    <message>
        <source>Too many attemps,you will lock in 5 min!</source>
        <translation type="vanished">尝试过多，锁定5分钟!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="953"/>
        <source>Wrong account or password!</source>
        <translation>ནོར་འཁྲུལ་གྱི་རྩིས་ཐོ་དང་ཡང་ན་གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Wrong phone code!</source>
        <translation type="vanished">手机验证码错误!</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exist!</source>
        <translation type="vanished">用户未注册！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="959"/>
        <source>Unsupported operation!</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་བྱ་སྤྱོད་ཅིག་རེད།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="960"/>
        <source>Unsupported Client Type!</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་ཚོང་འགྲུལ་གྱི་རིགས་དབྱིབས་ཤིག་རེད།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="961"/>
        <source>Please check your input!</source>
        <translation>ཁྱེད་ཀྱི་ནང་དོན་ལ་ཞིབ་བཤེར་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="963"/>
        <source>Process failed</source>
        <translation>བརྒྱུད་རིམ་ལ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source>Sync your settings</source>
        <translation type="vanished">同步您的设置</translation>
    </message>
    <message>
        <source>Sync failed!</source>
        <translation type="vanished">同步失败！</translation>
    </message>
    <message>
        <source>CloudSync service may crashed!</source>
        <translation type="vanished">云帐户功能可能没有开启！</translation>
    </message>
    <message>
        <source>Please ensure CloudSync installed!</source>
        <translation type="vanished">确保云帐户服务已经安装！</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="95"/>
        <location filename="../mainwidget.cpp" line="522"/>
        <location filename="../mainwidget.h" line="105"/>
        <source>Disconnected</source>
        <translation>འབྲེལ་ཐག་ཆད་པ།</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="144"/>
        <source>Sync processing!</source>
        <translation>དུས་མཉམ་དུ་ཐག་གཅོད་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Waiting for service start...</source>
        <translation type="vanished">等待服务准备完毕...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="275"/>
        <source>Auto-sync</source>
        <translation>རང་འགུལ་གྱིས་དུས་གཅིག་ཏུ་བཀོད་པ།</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="280"/>
        <source>Cloud Account</source>
        <translation>སྤྲིན་གྱི་རྩིས་ཐོ།</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="339"/>
        <location filename="../mainwidget.cpp" line="589"/>
        <source>Failed to sync!</source>
        <translation>དུས་མཉམ་དུ་བརྒྱུད་རིམ་ལ་ནོར་འཁྲུལ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="357"/>
        <source>We get some trouble when service start</source>
        <translation>ཞབས་ཞུའི་སྒོ་འབྱེད་ལ་གནད་དོན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="386"/>
        <source>Make sure installed cloud sync!</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པའི་སྤྲིན་གྱི་དུས་མཉམ་ལ་ཁག་ཐེག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="537"/>
        <source>Waitting for sync complete!</source>
        <translation>དུས་མཉམ་དུ་མཇུག་རྫོགས་རྗེས་རང་འགུལ་གྱིས་ཕྱིར་འབུད་པར་སྒུག</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="567"/>
        <source>Cloud Sync</source>
        <translation>སྤྲིན་གྱི་རྩིས་ཐོ།</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="570"/>
        <source>Kylin Account</source>
        <translation>སྤྲིན་གྱི་རྩིས་ཐོའི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <source>Sync!</source>
        <translation type="vanished">同步中！</translation>
    </message>
    <message>
        <source>Kylin Cloud Account</source>
        <translation type="vanished">云帐户</translation>
    </message>
    <message>
        <source>Cloud ID desktop message</source>
        <translation type="vanished">消息</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Your account:%1</source>
        <translation type="vanished">您的帐户：%1</translation>
    </message>
    <message>
        <source>Auto sync</source>
        <translation type="vanished">自动同步</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="vanished">未连接</translation>
    </message>
    <message>
        <source>Sync your settings</source>
        <translation type="vanished">同步您的设置</translation>
    </message>
    <message>
        <source>Sync failed!</source>
        <translation type="vanished">同步存在失败！</translation>
    </message>
    <message>
        <source>CloudSync service may crashed!</source>
        <translation type="vanished">云帐户功能可能没有开启！</translation>
    </message>
    <message>
        <source>Please ensure CloudSync installed!</source>
        <translation type="vanished">确保云帐户服务已经安装！</translation>
    </message>
    <message>
        <source>Sync processing!</source>
        <translation type="vanished">同步中！</translation>
    </message>
    <message>
        <source>Kylin Cloud Account</source>
        <translation type="vanished">云帐户</translation>
    </message>
    <message>
        <source>Cloud ID desktop message</source>
        <translation type="vanished">消息</translation>
    </message>
</context>
<context>
    <name>networkaccount</name>
    <message>
        <location filename="../networkaccount.cpp" line="30"/>
        <source>Cloud Account</source>
        <translation>སྤྲིན་གྱི་རྩིས་ཐོ།</translation>
    </message>
</context>
</TS>
