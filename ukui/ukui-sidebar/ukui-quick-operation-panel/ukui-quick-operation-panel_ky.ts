<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>ModifybluetoothWidget</name>
    <message>
        <source>Not connected</source>
        <translation type="obsolete">未连接</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/accountinformation.cpp" line="17"/>
        <source>administrators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/accountinformation.cpp" line="18"/>
        <source>standard users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Standard users</source>
        <translation type="vanished">普通用户</translation>
    </message>
    <message>
        <source>Auto rotate</source>
        <translation type="vanished">自动旋转</translation>
    </message>
    <message>
        <source>eyeshield</source>
        <translation type="vanished">护眼模式</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="350"/>
        <source>NotiToggle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>pad</source>
        <translation type="vanished">平板模式</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="336"/>
        <source>Energy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>setting</source>
        <translation type="vanished">系统设置</translation>
    </message>
    <message>
        <source>Eyeshield</source>
        <translation type="vanished">护眼模式</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="354"/>
        <source>Pad</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="334"/>
        <source>Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/powershow.cpp" line="56"/>
        <source>Electricity surplus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/powershow.cpp" line="57"/>
        <source>Charging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/wifiwidget.h" line="78"/>
        <source>Wired connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/bluetoothwidgetModify.cpp" line="26"/>
        <location filename="../../src/plugins/ukui-quick-operation-panel/wifiwidget.h" line="79"/>
        <source>Not connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/bluetoothwidgetModify.cpp" line="27"/>
        <location filename="../../src/plugins/ukui-quick-operation-panel/wifiwidget.h" line="80"/>
        <source>Closed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>AutoRotate</source>
        <translation type="obsolete">自动旋转</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="352"/>
        <source>Autorotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="346"/>
        <source>screenshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="348"/>
        <source>clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Eye care mode</source>
        <translation type="vanished">护眼模式</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="344"/>
        <source>Night mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="340"/>
        <source>flight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="342"/>
        <source>projectscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="338"/>
        <source>Bluetooth</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
