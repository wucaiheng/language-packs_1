<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/main.cpp" line="47"/>
        <source>ukui-sidebar is already running!</source>
        <translation>ukui-sidebar ་འཁོར་སྐྱོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="52"/>
        <source>show or hide sidebar widget</source>
        <translation>མངོན་པའམ་ཡང་ན་གཞོགས་ངོས་ཀྱི་ལྷུ་ལག་ཆུང་ཆུང་སྦས་པ།</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="53"/>
        <source>show sidebar widget</source>
        <translation>གཞོགས་ངོས་ཀྱི་གྲུ་ཁ་ཆུང་ཆུང་མངོན་པར་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="54"/>
        <source>hide sidebar widget</source>
        <translation>གཞོགས་ངོས་ཀྱི་ལྷུ་ལག་ཆུང་ཆུང་སྦས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/widget.cpp" line="161"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../src/widget.cpp" line="164"/>
        <source>Set up notification center</source>
        <translation>བརྡ་ཁྱབ་ལྟེ་གནས་བཙུགས་པ།</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../src/widget.cpp" line="192"/>
        <source>ukui-sidebar</source>
        <translation>གཞོགས་མཐའ་ར་བ།</translation>
    </message>
</context>
</TS>
