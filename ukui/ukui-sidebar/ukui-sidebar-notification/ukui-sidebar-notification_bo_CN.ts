<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>QObject</name>
    <message>
        <source>Notification center</source>
        <translation type="obsolete">Bildirim Merkezi</translation>
    </message>
    <message>
        <source>Important notice</source>
        <translation type="obsolete">Önemli Uyarı</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/notificationPlugin.cpp" line="161"/>
        <source>Clean up</source>
        <translation>གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/notificationPlugin.cpp" line="156"/>
        <source>Recent news</source>
        <translation>ཉེ་ལམ་གྱི་གསར་འགྱུར།</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/notificationPlugin.cpp" line="189"/>
        <source>Set up</source>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/notificationPlugin.cpp" line="528"/>
        <source>No new notifications</source>
        <translation>བརྡ་ཐོ་གསར་པ་བཏང་མེད་པ།</translation>
    </message>
    <message>
        <source>No unimportant notice</source>
        <translation type="obsolete">Önemsiz bildirim yok</translation>
    </message>
    <message>
        <source>Unimportant notice</source>
        <translation type="obsolete">Önemsiz Bildirim</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/appmsg.cpp" line="592"/>
        <source> fold</source>
        <translation> ལྟེབ་རྩེག</translation>
    </message>
</context>
<context>
    <name>SingleMsg</name>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/singlemsg.cpp" line="153"/>
        <source>now</source>
        <translation>ད་ལྟ།</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/singlemsg.cpp" line="355"/>
        <source>Yesterday </source>
        <translation>ཁ་སང་། </translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/singlemsg.cpp" line="431"/>
        <source>In addition </source>
        <translation>དེའི་ཐོག་བསྣན་ན། </translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/singlemsg.cpp" line="431"/>
        <source> notification</source>
        <translation> བརྡ་ཐོ་གཏོང་དགོས།</translation>
    </message>
    <message>
        <source>In addition</source>
        <translation type="obsolete">Tür</translation>
    </message>
</context>
<context>
    <name>TakeInBoxToolButton</name>
    <message>
        <source>Enter unimportant news</source>
        <translation type="obsolete">Önemsiz haberlere gir</translation>
    </message>
    <message>
        <source>Quit unimportant news</source>
        <translation type="obsolete">Önemsiz haberlerden çık</translation>
    </message>
</context>
</TS>
