<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AuthDialog</name>
    <message>
        <source>More Devices</source>
        <translation type="obsolete">选择其他设备</translation>
    </message>
    <message>
        <source>Biometric</source>
        <translation type="obsolete">使用生物识别认证</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">使用密码认证</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="691"/>
        <source>Retry</source>
        <translation>བསྐྱར་དུ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>UnLock</source>
        <translation type="obsolete">解锁</translation>
    </message>
    <message>
        <source>Slide to unlock</source>
        <translation type="vanished">向上滑动解锁</translation>
    </message>
    <message>
        <source>You have %1 unread message</source>
        <translation type="vanished">您有%1条未读消息</translation>
    </message>
    <message>
        <source>LoggedIn</source>
        <translation type="obsolete">已登录</translation>
    </message>
    <message>
        <source>Password: </source>
        <translation type="vanished">密码：</translation>
    </message>
    <message>
        <source>Account locked %1 minutes due to %2 fail attempts</source>
        <translation type="vanished">账户锁定%1分钟由于%2次错误尝试</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="257"/>
        <location filename="../src/authdialog.cpp" line="258"/>
        <location filename="../src/authdialog.cpp" line="325"/>
        <location filename="../src/authdialog.cpp" line="326"/>
        <source>Please try again in %1 minutes.</source>
        <translation>%1རྗེས་སུ་ཁྱོད་ཀྱིས་ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་ཚོད་ལྟ་ཞིག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="267"/>
        <location filename="../src/authdialog.cpp" line="268"/>
        <location filename="../src/authdialog.cpp" line="334"/>
        <location filename="../src/authdialog.cpp" line="335"/>
        <source>Please try again in %1 seconds.</source>
        <translation>%1རྗེས་སུ་ཁྱོད་ཀྱིས་དུས་ཚོད་སྐར་ཆ་གཅིག་གི་ནང་དུ་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="277"/>
        <location filename="../src/authdialog.cpp" line="278"/>
        <location filename="../src/authdialog.cpp" line="343"/>
        <location filename="../src/authdialog.cpp" line="344"/>
        <source>Account locked permanently.</source>
        <translation>དུས་གཏན་དུ་ཟྭ་བརྒྱབ་པའི་རྩིས་ཐོ།</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="459"/>
        <source>Verify face recognition or enter password to unlock</source>
        <translation>དཔང་མིའི་གདོང་ལ་ཞིབ་བཤེར་བྱེད་པའམ་ཡང་ན་གསང་གྲངས་བསྣན་ནས་ཟྭ་འབྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="464"/>
        <source>Press fingerprint or enter password to unlock</source>
        <translation>མཛུབ་རིས་མནན་པའམ་ཡང་ན་གསང་གྲངས་བསྣན་ནས་ཟྭ་འབྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="469"/>
        <source>Verify voiceprint or enter password to unlock</source>
        <translation>སྒྲ་པར་ཞིབ་བཤེར་བྱེད་པའམ་ཡང་ན་གསང་གྲངས་ནང་འཇུག་བྱས་ནས་ཟྭ་རྒྱག</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="474"/>
        <source>Verify finger vein or enter password to unlock</source>
        <translation>ཚོད་ལྟས་ར་སྤྲོད་བྱེད་པ་ནི་སྡོད་རྩ་དང་གསང་གྲངས་བསྣན་ནས་ཟྭ་འབྱེད་པ་དེ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="479"/>
        <source>Verify iris or enter password to unlock</source>
        <translation>འཇའ་སྐྱི་ར་སྤྲོད་བྱེད་པའམ་ཡང་ན་གསང་གྲངས་བསྣན་ནས་ཟྭ་འབྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="623"/>
        <source>Input Password</source>
        <translation>ནང་འཇུག་གི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="919"/>
        <source>Failed to verify %1, please enter password to unlock</source>
        <translation>%1ལ་ཞིབ་བཤེར་བྱེད་མ་ཐུབ་ན། གསང་གྲངས་ནང་འཇུག་བྱས་ནས་ཟྭ་རྒྱག་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="923"/>
        <source>Unable to verify %1, please enter password to unlock</source>
        <translation>%1ལ་ཞིབ་བཤེར་བྱེད་ཐབས་བྲལ་བ་དང་། གསང་གྲངས་ནང་འཇུག་བྱས་ནས་ཟྭ་རྒྱག་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="940"/>
        <source>Abnormal network</source>
        <translation>རྒྱུན་ལྡན་མིན་པའི་དྲ་བ།</translation>
    </message>
    <message>
        <source>Use the bound wechat scanning code or enter the password to log in</source>
        <translation type="vanished">使用绑定的微信扫码或输入密码登录</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="537"/>
        <location filename="../src/authdialog.cpp" line="538"/>
        <source>Password cannot be empty</source>
        <translation>གསང་གྲངས་སྟོང་པ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <source>Failed to verify %1, please enter password.</source>
        <translation type="vanished">验证%1失败，请输入密码.</translation>
    </message>
    <message>
        <source>Unable to verify %1, please enter password.</source>
        <translation type="vanished">无法验证%1，请输入密码.</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="929"/>
        <source>Failed to verify %1, you still have %2 verification opportunities</source>
        <translation>%1ལ་ཞིབ་བཤེར་བྱེད་མ་ཐུབ་ན། ཁྱེད་ཚོར་ད་དུང་%2ལ་ཞིབ་བཤེར་བྱེད་པའི་གོ་སྐབས་ཡོད།</translation>
    </message>
    <message>
        <source>Biometric/code scan authentication failed too many times, please enter the password.</source>
        <translation type="vanished">生物/扫码验证失败达最大次数，请使用密码解锁</translation>
    </message>
    <message>
        <source>Bioauth/code scan authentication failed, you still have %1 verification opportunities</source>
        <translation type="vanished">生物/扫码验证失败，您还有%1次尝试机会</translation>
    </message>
    <message>
        <source>NET Exception</source>
        <translation type="vanished">网络异常</translation>
    </message>
    <message>
        <source>Password Incorrect, Please try again</source>
        <translation type="vanished">密码错误，请重试</translation>
    </message>
    <message>
        <source>Authentication failure,there are still %1 remaining opportunities</source>
        <translation type="vanished">认证失败，还剩%1次尝试机会</translation>
    </message>
    <message>
        <source>Please enter your password or enroll your fingerprint </source>
        <translation type="vanished">请输入密码或者录入指纹</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="660"/>
        <source>Authentication failure, Please try again</source>
        <translation>བདེན་དཔང་ར་སྤྲོད་བྱེད་མ་ཐུབ་ན། ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="484"/>
        <source>Use the bound wechat scanning code or enter the password to unlock</source>
        <translation>འབྲེལ་མཐུད་ཀྱི་འཕྲིན་ཕྲན་ཞིབ་བཤེར་ཨང་གྲངས་སམ་ཡང་ན་གསང་གྲངས་ནང་འཇུག་བྱས་ནས་ཟྭ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="621"/>
        <source>Password </source>
        <translation>གསང་གྲངས། </translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="685"/>
        <source>Login</source>
        <translation>ཐོ་འགོད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Biometric Authentication</source>
        <translation type="vanished">生物识别认证</translation>
    </message>
    <message>
        <source>Password Authentication</source>
        <translation type="vanished">密码认证</translation>
    </message>
    <message>
        <source>Other Devices</source>
        <translation type="vanished">其他设备</translation>
    </message>
    <message>
        <source>Too many unsuccessful attempts,please enter password.</source>
        <translation type="vanished">指纹验证失败达最大次数，请使用密码登录</translation>
    </message>
    <message>
        <source>Fingerprint authentication failed, you still have %1 verification opportunities</source>
        <translation type="vanished">指纹验证失败，您还有%1次尝试机会</translation>
    </message>
</context>
<context>
    <name>BioDevices</name>
    <message>
        <source>FingerPrint</source>
        <translation type="obsolete">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="obsolete">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="obsolete">虹膜</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="obsolete">人脸</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="obsolete">声纹</translation>
    </message>
</context>
<context>
    <name>BioDevicesWidget</name>
    <message>
        <source>Please select other biometric devices</source>
        <translation type="obsolete">请选择其他生物识别设备</translation>
    </message>
    <message>
        <source>Device Type:</source>
        <translation type="obsolete">设备类型：</translation>
    </message>
    <message>
        <source>Device Name:</source>
        <translation type="obsolete">设备名称：</translation>
    </message>
</context>
<context>
    <name>BiometricAuthWidget</name>
    <message>
        <location filename="../BiometricAuth/biometricauthwidget.cpp" line="119"/>
        <source>Current device: </source>
        <translation>མིག་སྔའི་སྒྲིག་ཆས་ནི། </translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricauthwidget.cpp" line="185"/>
        <source>Identify failed, Please retry.</source>
        <translation>ཕམ་ཉེས་བྱུང་བར་ངོས་འཛིན་གནང་རོགས། ཁྱེད་ཀྱིས་ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་ཚོད་ལྟ་བྱོས།</translation>
    </message>
</context>
<context>
    <name>BiometricDevicesWidget</name>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="48"/>
        <source>Please select the biometric device</source>
        <translation>སྐྱེ་དངོས་དབྱེ་འབྱེད་སྒྲིག་ཆས་འདེམས་རོགས།</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="53"/>
        <source>Device type:</source>
        <translation>སྒྲིག་ཆས་ཀྱི་རིགས་དབྱིབས་ནི།</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="69"/>
        <source>Device name:</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="79"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
</context>
<context>
    <name>ConfForm</name>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="38"/>
        <source>edit network</source>
        <translation>རྩོམ་སྒྲིག་དྲ་བ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="116"/>
        <source>LAN name: </source>
        <translation>དྲ་བའི་མིང་ནི། </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="117"/>
        <source>Method: </source>
        <translation>རྩོམ་སྒྲིག་IPསྒྲིག་བཀོད། </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="118"/>
        <source>Address: </source>
        <translation>IPསྡོད་གནས་ནི། </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="119"/>
        <source>Netmask: </source>
        <translation>དྲ་རྒྱའི་འགེབས་སྲུང་ཨང་གྲངས། </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="120"/>
        <source>Gateway: </source>
        <translation>དྲ་སྒོ་ཁས་ལེན་པ། </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="121"/>
        <source>DNS 1: </source>
        <translation>སྔོན་འདེམསDNS </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="122"/>
        <source>DNS 2: </source>
        <translation>གྲབས་འདེམས་DNS </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="124"/>
        <source>Edit Conn</source>
        <translation>དྲ་བ་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="125"/>
        <location filename="../KylinNM/src/confform.cpp" line="127"/>
        <source>Auto(DHCP)</source>
        <translation>རང་འགུལ་(DHCP)</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="126"/>
        <location filename="../KylinNM/src/confform.cpp" line="128"/>
        <source>Manual</source>
        <translation>ལག་འགུལ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="158"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="159"/>
        <source>Save</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="160"/>
        <source>Ok</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="301"/>
        <source>Can not create new wired network for without wired card</source>
        <translation>སྐུད་ཡོད་བྱང་བུ་མེད་ན་སྐུད་ཡོད་དྲ་རྒྱ་གསར་པ་གཏོད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="318"/>
        <source>New network already created</source>
        <translation>དྲ་རྒྱ་གསར་པ་གསར་སྐྲུན་བྱས་ཟིན་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="362"/>
        <source>New network settings already finished</source>
        <translation>དྲ་རྒྱའི་སྒྲིག་བཀོད་གསར་པ་ལེགས་འགྲུབ་བྱུང་ཡོད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="395"/>
        <source>Edit Network</source>
        <translation>རྩོམ་སྒྲིག་དྲ་བ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="431"/>
        <source>Add Wired Network</source>
        <translation>སྐུད་ཡོད་བརྙན་འཕྲིན་དྲ་བ་ཁ་སྣོན་</translation>
    </message>
    <message>
        <source>create wired network successfully</source>
        <translation type="obsolete">已创建新的有线网络</translation>
    </message>
    <message>
        <source>change configuration of wired network successfully</source>
        <translation type="obsolete">新的设置已经生效</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="371"/>
        <source>New settings already effective</source>
        <translation type="unfinished">སྒྲིག་གཞི་གསར་པར་ཕན་ནུས་ཐོན་ཡོད།</translation>
    </message>
    <message>
        <source>There is a same named LAN exsits.</source>
        <translation type="obsolete">已有同名连接存在</translation>
    </message>
</context>
<context>
    <name>DeviceType</name>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="42"/>
        <source>FingerPrint</source>
        <translation>མཛུབ་རིས།</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="44"/>
        <source>FingerVein</source>
        <translation>སྡཽད་རྩ།</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="46"/>
        <source>Iris</source>
        <translation>འཇའ་སྐྱི།</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="48"/>
        <source>Face</source>
        <translation>ངོ་གདོང་དབྱེ་འབྱེད།</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="50"/>
        <source>VoicePrint</source>
        <translation>སྒྲ་གདངས་ཀྱི་པར་གཞི།</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="52"/>
        <source>QRCode</source>
        <translation>རྩ་གཉིས་ཨང་གྲངས།</translation>
    </message>
</context>
<context>
    <name>DigitalAuthDialog</name>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="57"/>
        <location filename="../src/digitalauthdialog.cpp" line="759"/>
        <source>LoginByUEdu</source>
        <translation>འཆར་ངོས་ཟྭ་ཡི་གསང་གྲངས་སྣོན་རོགས།</translation>
    </message>
    <message>
        <source>now is authing, wait a moment</source>
        <translation type="vanished">认证中，请稍后</translation>
    </message>
    <message>
        <source>Password Incorrect, Please try again</source>
        <translation type="obsolete">密码错误，请重试</translation>
    </message>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="61"/>
        <source>ResetPWD?</source>
        <translation>ཡང་བསྐྱར་བཀོད་སྒྲིག་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="87"/>
        <location filename="../src/digitalauthdialog.cpp" line="776"/>
        <source>SetNewUEduPWD</source>
        <translation>འཆར་ངོས་གསར་བའི་གསང་གྲངས་འཇོག་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="599"/>
        <source>ConfirmNewUEduPWD</source>
        <translation>འཆར་ངོས་གསར་བའི་གསང་གྲངས་གཏན་འཁེལ་བྱས།</translation>
    </message>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="611"/>
        <source>The two password entries are inconsistent, please reset</source>
        <translation>གསང་བའི་འཇུག་སྒོ་གཉིས་གཅིག་མཐུན་མིན་པས་བསྐྱར་དུ་བཀོད་སྒྲིག་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="700"/>
        <source>Password entered incorrectly, please try again</source>
        <translation>གསང་གྲངས་ནང་འཇུག་བྱས་པ་ནོར་འདུག་པས་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="224"/>
        <source>clear</source>
        <translation>གསལ་པོར་བཤད་ན།</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifi</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="76"/>
        <source>Add Hidden Wi-Fi</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="77"/>
        <source>Connection</source>
        <translation>འབྲེལ་མཐུད་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="78"/>
        <source>Wi-Fi name</source>
        <translation>Wi-Fiཡི་མིང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="79"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="80"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="81"/>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="83"/>
        <source>C_reate…</source>
        <translation>C_reate…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="104"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="105"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA དངWPA2 མི་སྒེར།</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128 位密钥(十六进制或ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128 位密码句</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">动态 WEP (802.1x)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA 及 WPA2 企业</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiLeap</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="68"/>
        <source>Add hidden Wi-Fi</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="69"/>
        <source>Connection</source>
        <translation>འབྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="70"/>
        <source>Network name</source>
        <translation>དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="71"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="72"/>
        <source>Username</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="73"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="74"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="75"/>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="77"/>
        <source>C_reate…</source>
        <translation>C_reate…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="95"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="96"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPAདང WPA2 མི་སྒེར།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="97"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128-bit Key (Hex or ASCII)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="98"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128-bit Passprase</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="100"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>འགུལ་རྣམ་གྱི་WEP (802.1X)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="101"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 ཁེ་ལས།</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecFast</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="83"/>
        <source>Add hidden Wi-Fi</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="84"/>
        <source>Connection</source>
        <translation>འབྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="85"/>
        <source>Network name</source>
        <translation>དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="86"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="87"/>
        <source>Authentication</source>
        <translation>བདེན་དཔང་ར་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="88"/>
        <source>Anonymous identity</source>
        <translation>མིང་མ་བཀོད་པའི་ཐོབ་ཐང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="89"/>
        <source>Allow automatic PAC pro_visioning</source>
        <translation>རང་འགུལ་གྱིས་PACའགོ་pro_visioning་བྱས་ཆོག།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="90"/>
        <source>PAC file</source>
        <translation>PACཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="91"/>
        <source>Inner authentication</source>
        <translation>ནང་ཁུལ་གྱི་བདེན་དཔང་ར་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="92"/>
        <source>Username</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="93"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="94"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="95"/>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="100"/>
        <source>C_reate…</source>
        <translation>C_reate…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="118"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="148"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="119"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPAདང WPA2 མི་སྒེར།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="120"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128-bit Key (Hex or ASCII)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="121"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128-bit Passprase</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="123"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>འགུལ་རྣམ་གྱི་WEP (802.1X)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="124"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 ཁེ་ལས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="136"/>
        <source>Tunneled TLS</source>
        <translation>ཕུག་ལམ་གྱི་TLS</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="137"/>
        <source>Protected EAP (PEAP)</source>
        <translation>སྲུང་སྐྱོབ་ཐོབ་པའི་EAP(PEAP)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="143"/>
        <source>Anonymous</source>
        <translation>མིང་མ་བཀོད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="144"/>
        <source>Authenticated</source>
        <translation>བདེན་དཔང་ར་སྤྲོད་བྱས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="145"/>
        <source>Both</source>
        <translation>གཉིས་ཀ་མཉམ་སྤྱོད།</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecLeap</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="73"/>
        <source>Add hidden Wi-Fi</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="74"/>
        <source>Connection</source>
        <translation>འབྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="75"/>
        <source>Network name</source>
        <translation>དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="76"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="77"/>
        <source>Authentication</source>
        <translation>བདེན་དཔང་ར་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="78"/>
        <source>Username</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="79"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="80"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="81"/>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="83"/>
        <source>C_reate…</source>
        <translation>C_reate…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="101"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="102"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA &amp; WPA2 མི་སྒེར།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="103"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128-bit Key (Hex or ASCII)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="104"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128-bit Passprase</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="106"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>འགུལ་རྣམ་གྱི་WEP (802.1X)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="107"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 ཁེ་ལས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="119"/>
        <source>Tunneled TLS</source>
        <translation>ཕུག་ལམ་གྱི་TLS</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="120"/>
        <source>Protected EAP (PEAP)</source>
        <translation>སྲུང་སྐྱོབ་ཐོབ་པའི་EAP(PEAP)</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPeap</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="91"/>
        <source>Add hidden Wi-Fi</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="92"/>
        <source>Connection</source>
        <translation>འབྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="93"/>
        <source>Network name</source>
        <translation>དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="94"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="95"/>
        <source>Authentication</source>
        <translation>བདེན་དཔང་ར་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="96"/>
        <source>Anonymous identity</source>
        <translation>མིང་མ་བཀོད་པའི་ཐོབ་ཐང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="97"/>
        <source>Domain</source>
        <translation>ཁྱབ་ཁོངས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="98"/>
        <source>CA certificate</source>
        <translation>CA དཔང་ཡིག</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="99"/>
        <source>CA certificate password</source>
        <translation>CA དཔང་ཡིག་གི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="100"/>
        <source>No CA certificate is required</source>
        <translation>CAལག་ཁྱེར་མི་དགོས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="101"/>
        <source>PEAP version</source>
        <translation>PEAPཔར་གཞི།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="102"/>
        <source>Inner authentication</source>
        <translation>ནང་ཁུལ་གྱི་བདེན་དཔང་ར་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="103"/>
        <source>Username</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="104"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="105"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="106"/>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="126"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="149"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="127"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA &amp; WPA2 མི་སྒེར།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="128"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128-bit Key (Hex or ASCII)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="129"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128-bit Passprase</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="131"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>འགུལ་རྣམ་གྱི་WEP (802.1X)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="132"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 ཁེ་ལས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="144"/>
        <source>Tunneled TLS</source>
        <translation>ཕུག་ལམ་གྱི་TLS</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="145"/>
        <source>Protected EAP (PEAP)</source>
        <translation>སྲུང་སྐྱོབ་ཐོབ་པའི་EAP(PEAP)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="150"/>
        <source>Choose from file</source>
        <translation>ཡིག་ཆའི་ནང་ནས་གདམ་གསེས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="153"/>
        <source>Automatic</source>
        <translation>རང་འགུལ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="154"/>
        <source>Version 0</source>
        <translation>པར་གཞི་0</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="155"/>
        <source>Version 1</source>
        <translation>པར་གཞི་དང་པོ།</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPwd</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="74"/>
        <source>Add hidden Wi-Fi</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="75"/>
        <source>Connection</source>
        <translation>འབྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="76"/>
        <source>Network name</source>
        <translation>དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="77"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="78"/>
        <source>Authentication</source>
        <translation>བདེན་དཔང་ར་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="79"/>
        <source>Username</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="80"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="81"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="82"/>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="84"/>
        <source>C_reate…</source>
        <translation>C_reate…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="102"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="103"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA &amp; WPA2 མི་སྒེར།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="104"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128-bit Key (Hex or ASCII)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="105"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128-bit Passprase</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="107"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>འགུལ་རྣམ་གྱི་WEP (802.1X)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="108"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 ཁེ་ལས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="120"/>
        <source>Tunneled TLS</source>
        <translation>ཕུག་ལམ་གྱི་TLS</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="121"/>
        <source>Protected EAP (PEAP)</source>
        <translation>སྲུང་སྐྱོབ་ཐོབ་པའི་EAP(PEAP)</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecTls</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="90"/>
        <source>Add hidden Wi-Fi</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="91"/>
        <source>Connection</source>
        <translation>འབྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="92"/>
        <source>Network name</source>
        <translation>དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="93"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="94"/>
        <source>Authentication</source>
        <translation>བདེན་དཔང་ར་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="95"/>
        <source>Identity</source>
        <translation>ཐོབ་ཐང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="96"/>
        <source>Domain</source>
        <translation>ཁྱབ་ཁོངས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="97"/>
        <source>CA certificate</source>
        <translation>CA དཔང་ཡིག</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="98"/>
        <source>CA certificate password</source>
        <translation>CA དཔང་ཡིག་གི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="99"/>
        <source>No CA certificate is required</source>
        <translation>CAལག་ཁྱེར་མི་དགོས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="100"/>
        <source>User certificate</source>
        <translation>སྤྱོད་མཁན་གྱི་ལག་ཁྱེར།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="101"/>
        <source>User certificate password</source>
        <translation>སྤྱོད་མཁན་གྱི་ལག་ཁྱེར་གྱི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="102"/>
        <source>User private key</source>
        <translation>སྤྱོད་མཁན་གྱི་སྒེར་གྱི་ལྡེ་མིག</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="103"/>
        <source>User key password</source>
        <translation>སྤྱོད་མཁན་གྱི་ལྡེ་མིག་གི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="104"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="105"/>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="107"/>
        <source>C_reate…</source>
        <translation>C_reate…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="125"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="148"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="152"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="156"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="126"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA &amp; WPA2 མི་སྒེར།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="127"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128-bit Key (Hex or ASCII)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="128"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128-bit Passprase</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="130"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>འགུལ་རྣམ་གྱི་WEP (802.1X)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="131"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 ཁེ་ལས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="143"/>
        <source>Tunneled TLS</source>
        <translation>ཕུག་ལམ་གྱི་TLS</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="144"/>
        <source>Protected EAP (PEAP)</source>
        <translation>སྲུང་སྐྱོབ་ཐོབ་པའི་EAP(PEAP)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="149"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="153"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="157"/>
        <source>Choose from file</source>
        <translation>ཡིག་ཆའི་ནང་ནས་གདམ་གསེས་གྲངས།</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecTunnelTLS</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="89"/>
        <source>Add hidden Wi-Fi</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="90"/>
        <source>Connection</source>
        <translation>འབྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="91"/>
        <source>Network name</source>
        <translation>དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="92"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="93"/>
        <source>Authentication</source>
        <translation>བདེན་དཔང་ར་སྤྲོད་</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="94"/>
        <source>Anonymous identity</source>
        <translation>མིང་མ་བཀོད་པའི་ཐོབ་ཐང</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="95"/>
        <source>Domain</source>
        <translation>ཁྱབ་ཁོངས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="96"/>
        <source>CA certificate</source>
        <translation>CA དཔང་ཡིག</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="97"/>
        <source>CA certificate password</source>
        <translation>CA དཔང་ཡིག་གི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="98"/>
        <source>No CA certificate is required</source>
        <translation>CAལག་ཁྱེར་མི་དགོས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="99"/>
        <source>Inner authentication</source>
        <translation>ནང་ཁུལ་གྱི་བདེན་དཔང་ར་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="100"/>
        <source>Username</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="101"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="102"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="103"/>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="105"/>
        <source>C_reate…</source>
        <translation>C_reate…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="123"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="146"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="124"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA &amp; WPA2 མི་སྒེར།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="125"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128-bit Key (Hex or ASCII)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="126"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128-bit Passprase</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="128"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>འགུལ་རྣམ་གྱི་WEP (802.1X)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="129"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 ཁེ་ལས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="141"/>
        <source>Tunneled TLS</source>
        <translation>ཕུག་ལམ་གྱི་TLS</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="142"/>
        <source>Protected EAP (PEAP)</source>
        <translation>སྲུང་སྐྱོབ་ཐོབ་པའི་EAP(PEAP)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="147"/>
        <source>Choose from file</source>
        <translation>ཡིག་ཆའི་ནང་ནས་གདམ་གསེས།</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiWep</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="73"/>
        <source>Add hidden Wi-Fi</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="74"/>
        <source>Connection</source>
        <translation>འབྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="75"/>
        <source>Network name</source>
        <translation>དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="76"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="77"/>
        <source>Key</source>
        <translation>ལྡེ་མིག</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="78"/>
        <source>WEP index</source>
        <translation>WEPསྟོན་གྲངས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="79"/>
        <source>Authentication</source>
        <translation>བདེན་དཔང་ར་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="80"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="81"/>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="83"/>
        <source>C_reate…</source>
        <translation>C_reate…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="101"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="102"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA &amp; WPA2 མི་སྒེར།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="103"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128-bit Key (Hex or ASCII)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="104"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128-bit Passprase</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="106"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>འགུལ་རྣམ་གྱི་WEP (802.1X)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="107"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 ཁེ་ལས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="115"/>
        <source>1(default)</source>
        <translation>1(སོར་བཞག)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="121"/>
        <source>Open System</source>
        <translation>སྒོ་འབྱེད་མ་ལག</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="122"/>
        <source>Shared Key</source>
        <translation>མཉམ་སྤྱོད་ཀྱི་ལྡེ་མིག</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiWpa</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="82"/>
        <source>Add Hidden Wi-Fi</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiབསྣན་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="83"/>
        <source>Connection</source>
        <translation>འབྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="84"/>
        <source>Wi-Fi name</source>
        <translation>Wi-Fiཡི་མིང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="85"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="86"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="87"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="88"/>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="90"/>
        <source>C_reate…</source>
        <translation>C_reate…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="113"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="114"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA &amp; WPA2 མི་སྒེར།</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="obsolete">WEP 40/128 位密钥(十六进制或ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="obsolete">WEP 128 位密码句</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="obsolete">动态 WEP (802.1x)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="obsolete">WPA 及 WPA2 企业</translation>
    </message>
</context>
<context>
    <name>DlgHotspotCreate</name>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.ui" line="14"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="46"/>
        <source>Create Hotspot</source>
        <translation>ཚ་བ་ཆེ་བའི་ས་ཆ་གསར་སྐྲུན།</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="47"/>
        <source>Network name</source>
        <translation>དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="48"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fiབདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="49"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="50"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="51"/>
        <source>Ok</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="54"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="55"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA &amp; WPA2 མི་སྒེར།</translation>
    </message>
</context>
<context>
    <name>InputInfos</name>
    <message>
        <location filename="../src/verificationwidget.cpp" line="338"/>
        <source>Service exception...</source>
        <translation>ཞབས་ཞུ་རྒྱུན་ལྡན་མིན་པ། ཡང་བསྐྱར་ཚོད་ལྟ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="341"/>
        <source>Invaild parameters...</source>
        <translation>གཞི་གྲངས་རྒྱུན་ལྡན་མིན་པ། ཡང་བསྐྱར་ཚོད་ལྟ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="344"/>
        <source>Unknown fault:%1</source>
        <translation>ཤེས་མེད་པའི་ནོར་འཛོལ། %1</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="288"/>
        <source>Recapture(60s)</source>
        <translation>ཕྱིར་ལེན་པ།(60s)</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="312"/>
        <source>Recapture(%1s)</source>
        <translation>ཕྱིར་ལེན་པ།(%1s)</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="141"/>
        <location filename="../src/verificationwidget.cpp" line="318"/>
        <source>Get code</source>
        <translation>ཚབ་རྟགས་ཐོབ་པ།</translation>
    </message>
</context>
<context>
    <name>KeyboardWidget</name>
    <message>
        <location filename="../VirtualKeyboard/src/keyboardwidget.ui" line="29"/>
        <source>KeyboardWidget</source>
        <translation>མཐེབ་གཞོང་Widget</translation>
    </message>
</context>
<context>
    <name>KylinDBus</name>
    <message>
        <source>kylin network applet desktop message</source>
        <translation type="obsolete">麒麟网络工具信息提示</translation>
    </message>
</context>
<context>
    <name>KylinNM</name>
    <message>
        <location filename="../KylinNM/src/kylinnm.ui" line="14"/>
        <source>kylin-nm</source>
        <translation>དྲ་རྒྱའི་ཡོ་བྱད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="413"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="417"/>
        <source>LAN</source>
        <translation>སྐུད་ཡོད་དྲ་རྒྱ།</translation>
    </message>
    <message>
        <source>Enabel LAN List</source>
        <translation type="obsolete">其他有线网络</translation>
    </message>
    <message>
        <source>WiFi</source>
        <translation type="obsolete">无线网络</translation>
    </message>
    <message>
        <source>Enabel WiFi List</source>
        <translation type="obsolete">其他无线网络</translation>
    </message>
    <message>
        <source>New WiFi</source>
        <translation type="obsolete">加入其他网络</translation>
    </message>
    <message>
        <source>Network</source>
        <translation type="vanished">网络</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="456"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="611"/>
        <source>Advanced</source>
        <translation>དྲ་རྒྱ་སྒྲིག་བཀོལ།</translation>
    </message>
    <message>
        <source>Ethernet</source>
        <translation type="vanished">有线网络</translation>
    </message>
    <message>
        <source>Connect Hide Network</source>
        <translation type="vanished">加入网络</translation>
    </message>
    <message>
        <source>Wifi</source>
        <translation type="vanished">无线网络</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="vanished">已开启</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="vanished">已关闭</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="438"/>
        <source>HotSpot</source>
        <translation>མི་སྒེར་གྱི་དྲ་བ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="447"/>
        <source>FlyMode</source>
        <translation>འཕུར་སྐྱོད་རྣམ་པ།</translation>
    </message>
    <message>
        <source>Show MainWindow</source>
        <translation type="vanished">显示网络连接界面</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="293"/>
        <source>Inactivated LAN</source>
        <translation>གྲུང་སྐུལ་བྱས་མེད།</translation>
    </message>
    <message>
        <source>Inactivated WLAN</source>
        <translation type="vanished">未激活</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="317"/>
        <source>Other WLAN</source>
        <translation>གཞན་དག</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="423"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="427"/>
        <source>WLAN</source>
        <translation>སྐུད་མེད་དྲ་རྒྱ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="610"/>
        <source>Show KylinNM</source>
        <translation>ཅིན་ལིན་ནའེ་མུའུ་ལ་བསྟན་དོན།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1289"/>
        <source>No wireless card detected</source>
        <translation>སྐུད་མེད་བྱང་བུ་མ་རྙེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1326"/>
        <source>Activated LAN</source>
        <translation>གྲུང་སྐུལ་བྱས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1393"/>
        <source>Activated WLAN</source>
        <translation>གྲུང་སྐུལ་བྱས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1437"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1535"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1701"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2426"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2517"/>
        <source>Not connected</source>
        <translation>དྲ་རྒྱ་གཅིག་ཀྱང་འབྲེལ་མཐུད་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1440"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1537"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1607"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1608"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1704"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1828"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1995"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2428"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2519"/>
        <source>Disconnected</source>
        <translation>འབྲེལ་ཐག་ཆད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1633"/>
        <source>No Other Wired Network Scheme</source>
        <translation>སྐུད་ཡོད་བརྙན་འཕྲིན་དྲ་བའི་འཆར་གཞི་གཞན་དག་མེད</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="obsolete">编辑</translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="obsolete">完成</translation>
    </message>
    <message>
        <source>No wifi connected.</source>
        <translation type="obsolete">未连接任何网络</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1849"/>
        <source>No Other Wireless Network Scheme</source>
        <translation>སྐུད་མེད་དྲ་རྒྱའི་འཆར་གཞི་གཞན་དག་མེད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2335"/>
        <source>Wired net is disconnected</source>
        <translation>སྐུད་ཡོད་དྲ་རྒྱ་ཆད་པ།</translation>
    </message>
    <message>
        <source>Wi-Fi is disconnected</source>
        <translation type="obsolete">断开无线网络</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2751"/>
        <source>Confirm your Wi-Fi password or usable of wireless card</source>
        <translation>ཁྱོད་ཀྱི་Wi-Fiཡི་གསང་གྲངས་སམ་ཡང་ན་སྐུད་མེད་བྱང་བུ་བཀོལ་སྤྱོད་བྱེད་ཆོག་པ་ར་སྤྲོད</translation>
    </message>
    <message>
        <source>Ethernet Networks</source>
        <translation type="vanished">其他有线网络</translation>
    </message>
    <message>
        <source>New LAN</source>
        <translation type="obsolete">新建有线网络</translation>
    </message>
    <message>
        <source>Hide WiFi</source>
        <translation type="vanished">加入网络</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="359"/>
        <source>No usable network in the list</source>
        <translation>མིང་ཐོའི་ནང་དུ་སྤྱོད་གོ་ཆོད་པའི་དྲ་རྒྱ་མེད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1588"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1798"/>
        <source>NetOn,</source>
        <translation>དྲ་རྒྱ་སྦྲེལ་བ།</translation>
    </message>
    <message>
        <source>Wifi Networks</source>
        <translation type="vanished">其他无线网络</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>keep wired network switch is on before turning on wireless switch</source>
        <translation type="vanished">打开无线网开关前保持有线网开关打开</translation>
    </message>
    <message>
        <source>please insert the wireless network adapter</source>
        <translation type="vanished">请先插入无线网卡</translation>
    </message>
    <message>
        <source>Abnormal connection exist, program will delete it</source>
        <translation type="vanished">正在断开异常连接的网络</translation>
    </message>
    <message>
        <source>update Wi-Fi list now, click again</source>
        <translation type="vanished">正在更新 Wi-Fi列表 请再次点击</translation>
    </message>
    <message>
        <source>update Wi-Fi list now</source>
        <translation type="vanished">正在更新 Wi-Fi列表</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2705"/>
        <source>Conn Ethernet Success</source>
        <translation>སྐུད་ཡོད་དྲ་རྒྱ་སྦྲེལ་བ་ལེགས་འགྲུབ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2717"/>
        <source>Conn Ethernet Fail</source>
        <translation>སྐུད་ཡོད་དྲ་རྒྱ་སྦྲེལ་བར་ཕམ་ཁ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2742"/>
        <source>Conn Wifi Success</source>
        <translation>སྐུད་མེད་དྲ་སྦྲེལ་ལེགས་འགྲུབ་བྱུང་།</translation>
    </message>
</context>
<context>
    <name>LockWidget</name>
    <message>
        <location filename="../src/lockwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/lockwidget.ui" line="55"/>
        <source>Date</source>
        <translation>ཟླ་ཚེས།</translation>
    </message>
    <message>
        <location filename="../src/lockwidget.ui" line="48"/>
        <source>Time</source>
        <translation>དུས་ཚོད།</translation>
    </message>
    <message>
        <source>Guest</source>
        <translation type="vanished">游客</translation>
    </message>
    <message>
        <source>SwitchUser</source>
        <translation type="vanished">切换用户</translation>
    </message>
    <message>
        <location filename="../src/lockwidget.cpp" line="487"/>
        <source>Multiple users are logged in at the same time.Are you sure you want to reboot this system?</source>
        <translation>སྤྱོད་མཁན་མང་པོ་ཞིག་དུས་གཅིག་ཏུ་ཐོ་འགོད་བྱས་པ་རེད། ཁྱོད་ཀྱིས་མ་ལག་འདི་བསྐྱར་དུ་སྒྲིག་རྒྱུ་ཡིན་ནམ།</translation>
    </message>
</context>
<context>
    <name>LoginOptionsWidget</name>
    <message>
        <location filename="../src/loginoptionswidget.cpp" line="49"/>
        <source>Login Options</source>
        <translation>ཐོ་འགོད་ཀྱི་བསལ་འདེམས་ཀྱི་དབང་ཆ།</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">密码</translation>
    </message>
    <message>
        <source>Wechat</source>
        <translation type="vanished">微信</translation>
    </message>
    <message>
        <location filename="../src/loginoptionswidget.cpp" line="504"/>
        <source>Identify device removed!</source>
        <translation>དབྱེ་འབྱེད་སྒྲིག་ཆས་མེད་པར་བཟོ་དགོས།</translation>
    </message>
</context>
<context>
    <name>MyLineEdit</name>
    <message>
        <location filename="../src/verificationwidget.cpp" line="599"/>
        <location filename="../src/verificationwidget.cpp" line="605"/>
        <location filename="../src/verificationwidget.cpp" line="622"/>
        <source>Verification code</source>
        <translation>ཞིབ་བཤེར་གྱི་ཚབ་རྟགས།</translation>
    </message>
</context>
<context>
    <name>OneConnForm</name>
    <message>
        <location filename="../KylinNM/src/oneconnform.ui" line="14"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="158"/>
        <source>Automatically join the network</source>
        <translation>རང་འགུལ་གྱིས་དྲ་རྒྱའི་ནང་དུ་ཞུགས་པ།</translation>
    </message>
    <message>
        <source>Input password</source>
        <translation type="vanished">输入密码</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="42"/>
        <location filename="../KylinNM/src/oneconnform.cpp" line="43"/>
        <location filename="../KylinNM/src/oneconnform.cpp" line="44"/>
        <location filename="../KylinNM/src/oneconnform.cpp" line="46"/>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="45"/>
        <source>Disconnect</source>
        <translation>འབྲེལ་ཐག་ཆད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="47"/>
        <source>Input Password...</source>
        <translation>ནང་འཇུག་གི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="419"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>སྦས་སྐུང་བྱས་པའི་Wi-Fiདྲ་རྒྱ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="555"/>
        <source>Signal：</source>
        <translation>བརྡ་རྟགས་ནི།</translation>
    </message>
    <message>
        <source>Public</source>
        <translation type="vanished">开放</translation>
    </message>
    <message>
        <source>Safe</source>
        <translation type="vanished">安全</translation>
    </message>
    <message>
        <source>Rate</source>
        <translation type="vanished">速率</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="552"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="554"/>
        <source>WiFi Security：</source>
        <translation>WiFiབཀོལ་སྤྱོད་བདེ་འཇགས་ནི།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="556"/>
        <source>MAC：</source>
        <translation>MAC:</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="833"/>
        <source>Conn Wifi Failed</source>
        <translation>ཁུང་ནེ་ཝེ་ཧྥེ་ཕམ་སོང་།</translation>
    </message>
</context>
<context>
    <name>OneLancForm</name>
    <message>
        <location filename="../KylinNM/src/onelancform.ui" line="14"/>
        <source>Form</source>
        <translation>--</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="31"/>
        <location filename="../KylinNM/src/onelancform.cpp" line="32"/>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="34"/>
        <source>Disconnect</source>
        <translation>འབྲེལ་ཐག་ཆད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="289"/>
        <location filename="../KylinNM/src/onelancform.cpp" line="293"/>
        <source>No Configuration</source>
        <translation>བཀོད་སྒྲིག་བྱས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="296"/>
        <source>IPv4：</source>
        <translation>IPv4:</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="297"/>
        <source>IPv6：</source>
        <translation>IPv6:</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="298"/>
        <source>BandWidth：</source>
        <translation>ཐགས་ཞེང་།</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="299"/>
        <source>MAC：</source>
        <translation>MAC:</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="obsolete">自动</translation>
    </message>
</context>
<context>
    <name>PhoneAuthWidget</name>
    <message>
        <location filename="../src/permissioncheck.cpp" line="236"/>
        <location filename="../src/verificationwidget.cpp" line="375"/>
        <source>Verification by phoneNum</source>
        <translation>ཁ་པར་ཨང་གྲངས་ཚོད་ལྟས་ར་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../src/permissioncheck.cpp" line="241"/>
        <source>「 Use bound Phone number to verification 」</source>
        <translation>「ས་མཚམས་ཀྱི་ཁ་པར་ཨང་གྲངས་ལ་བརྟེན་ནས་ཞིབ་བཤེར་བྱ་དགོས།」</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="381"/>
        <source>「 Use SMS to verification 」</source>
        <translation>「SMSབཀོལ་ནས་ཞིབ་བཤེར་བྱ་དགོས།」</translation>
    </message>
    <message>
        <location filename="../src/permissioncheck.cpp" line="259"/>
        <location filename="../src/verificationwidget.cpp" line="399"/>
        <source>commit</source>
        <translation>གོང་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="484"/>
        <location filename="../src/verificationwidget.cpp" line="524"/>
        <source>Network not connected~</source>
        <translation>མ་ལག་དྲ་སྦྲེལ་བྱས་མེད་པས་དྲ་སྦྲེལ་ལ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="530"/>
        <source>Network unavailable~</source>
        <translation>དྲ་རྒྱའི་རྣམ་པ་ཞན་པས་དྲ་རྒྱའི་སྦྲེལ་མཐུད་ལ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="487"/>
        <source>Verification Code invalid!</source>
        <translation>ཞིབ་བཤེར་ཚད་གཞི་གོ་མི་ཆོད་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="490"/>
        <source>Verification Code incorrect.Please retry!</source>
        <translation>ར་སྤྲོད་ཨང་གྲངས་ནོར་འདུགཡང་དག་པའི་ར་སྤྲོད་ཨང་གྲངས་ཕྲིས་ཤིག</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="493"/>
        <source>Failed time over limit!Retry after 1 hour!</source>
        <translation>ཚད་ལས་བརྒལ་ནས་ཕམ་ཁ་བྱུང་བའི་དུས་ཚོད་ ཆུ་ཚོད་1འགོར་རྗེས་ཡང་བསྐྱར་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="497"/>
        <source>verifaction failed!</source>
        <translation>ངོ་རྒོལ་བྱེད་པའི་བྱ་སྤྱོད་ལ་ཕམ་ཉེས་བྱུང་བ་རེད།</translation>
    </message>
</context>
<context>
    <name>PowerManager</name>
    <message>
        <location filename="../src/powermanager.cpp" line="289"/>
        <source>lock</source>
        <translation>ཟྭ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>SwitchUser</source>
        <translation type="vanished">切换用户</translation>
    </message>
    <message>
        <source>logout</source>
        <translation type="vanished">注销</translation>
    </message>
    <message>
        <source>reboot</source>
        <translation type="vanished">重启</translation>
    </message>
    <message>
        <source>shutdown</source>
        <translation type="vanished">关机</translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation type="vanished">锁屏</translation>
    </message>
    <message>
        <source>Switch User</source>
        <translation type="vanished">切换用户</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="306"/>
        <source>Log Out</source>
        <translation>ཕྱིར་འཐེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="324"/>
        <location filename="../src/powermanager.cpp" line="648"/>
        <source>Restart</source>
        <translation>ཡང་བསྐྱར་འགོ་འཛུགས་པ།</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="344"/>
        <source>Power Off</source>
        <translation>གློག་ཆད་པ།</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="666"/>
        <source>Shut Down</source>
        <translation>སྒོ་རྒྱབ་པ།</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="692"/>
        <source>Hibernate</source>
        <translation>མལ་གསོ།</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="719"/>
        <source>Suspend</source>
        <translation>གཉིད་པ།</translation>
    </message>
    <message>
        <source>Sleep</source>
        <translation type="vanished">休眠</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="78"/>
        <source>The screensaver is active.</source>
        <translation>འཆར་ངོས་སྲུང་སྐྱོབ་བརྒྱུད་རིམ་སྒོ་འབྱེད་བྱས་སོང་།</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="80"/>
        <source>The screensaver is inactive.</source>
        <translation>འཆར་ངོས་སྲུང་སྐྱོབ་བརྒྱུད་རིམ་སྒོ་འབྱེད་བྱས་མེད།</translation>
    </message>
</context>
<context>
    <name>Screensaver</name>
    <message>
        <source>exit(Esc)</source>
        <translation type="vanished">退出(Esc)</translation>
    </message>
    <message>
        <source>exit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <location filename="../screensaver/screensaver.cpp" line="142"/>
        <source>Picture does not exist</source>
        <translation>རི་མོ་མི་གནས་པ།</translation>
    </message>
    <message>
        <source>Set as desktop wallpaper</source>
        <translation type="vanished">设置为桌面壁纸</translation>
    </message>
    <message>
        <source>Automatic switching</source>
        <translation type="vanished">自动切换</translation>
    </message>
    <message>
        <source>You have %1 unread message</source>
        <translation type="obsolete">您有%1条未读消息</translation>
    </message>
    <message>
        <location filename="../screensaver/screensaver.cpp" line="1339"/>
        <source>You have new notification</source>
        <translation>ཁྱེད་ཚོར་བརྡ་ཐོ་གསར་པ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../screensaver/screensaver.cpp" line="1183"/>
        <source>View</source>
        <translation>ལྟ་ཚུལ།</translation>
    </message>
</context>
<context>
    <name>SleepTime</name>
    <message>
        <location filename="../screensaver/sleeptime.cpp" line="70"/>
        <source>You have rested:</source>
        <translation>ཁྱེད་ཚོས་ངལ་གསོ་བྱས་ཟིན་པ་གཤམ་གསལ།</translation>
    </message>
</context>
<context>
    <name>SureWindow</name>
    <message>
        <location filename="../src/surewindow.ui" line="14"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../src/surewindow.ui" line="56"/>
        <source>TextLabel</source>
        <translation>ཡི་གེ་ལ་པེར་གྱིས་བཤད་རྒྱུར</translation>
    </message>
    <message>
        <location filename="../src/surewindow.ui" line="157"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../src/surewindow.ui" line="176"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Multiple users are logged in at the same time.Are you sure you want to reboot this system?</source>
        <translation type="vanished">同时有多个用户登录系统，您确定要退出系统吗？</translation>
    </message>
    <message>
        <location filename="../src/surewindow.cpp" line="46"/>
        <source>The following program is running to prevent the system from suspend!</source>
        <translation>གཤམ་གྱི་གོ་རིམ་ནི་མ་ལག་གནས་སྐབས་མཚམས་འཇོག་པར་བཀག་འགོག་བྱེད་པའི་ཆེད་དུ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../src/surewindow.cpp" line="49"/>
        <source>The following program is running to prevent the system from hibernate!</source>
        <translation>གཤམ་གྱི་གོ་རིམ་ནི་མ་ལག་ལ་བཀག་འགོག་བྱེད་པའི་ཆེད་དུ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../src/surewindow.cpp" line="43"/>
        <source>The following program is running to prevent the system from shutting down!</source>
        <translation>གཤམ་གྱི་གོ་རིམ་ནི་མ་ལག་གི་སྒོ་རྒྱག་པར་བཀག་འགོག་བྱེད་པའི་ཆེད་དུ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../src/surewindow.cpp" line="40"/>
        <source>The following program is running to prevent the system from reboot!</source>
        <translation>གཤམ་གྱི་གོ་རིམ་ནི་མ་ལག་བསྐྱར་དུ་འབྱུང་བར་སྔོན་འགོག་བྱེད་ཆེད་ཡིན།</translation>
    </message>
</context>
<context>
    <name>SwitchButton</name>
    <message>
        <source>login by password</source>
        <translation type="vanished">密码登录</translation>
    </message>
    <message>
        <source>login by qr code</source>
        <translation type="vanished">微信登录</translation>
    </message>
</context>
<context>
    <name>SwitchButtonGroup</name>
    <message>
        <location filename="../src/switchbuttongroup.cpp" line="35"/>
        <source>uEduPWD</source>
        <translation>གསང་གྲངས་ཐོ་འགོད།</translation>
    </message>
    <message>
        <location filename="../src/switchbuttongroup.cpp" line="36"/>
        <source>Wechat</source>
        <translation>འཕྲིན་ཕྲན་ཐོ་འགོད།</translation>
    </message>
</context>
<context>
    <name>TabletLockWidget</name>
    <message>
        <source>You have %1 unread message</source>
        <translation type="vanished">您有%1条未读消息</translation>
    </message>
    <message>
        <source>Slide to unlock</source>
        <translation type="vanished">向上滑动解锁</translation>
    </message>
    <message>
        <location filename="../src/tabletlockwidget.cpp" line="266"/>
        <source>New password is the same as old</source>
        <translation>གསང་གྲངས་གསར་པ་དང་རྙིང་པ་གཅིག་མཚུངས་ཡིན།</translation>
    </message>
    <message>
        <location filename="../src/tabletlockwidget.cpp" line="277"/>
        <source>Reset password error:%1</source>
        <translation>གསང་གྲངས་ཀྱི་ནོར་འཁྲུལ་བསྐྱར་དུ་བཀོད་སྒྲིག་བྱེད་པ།%1</translation>
    </message>
    <message>
        <location filename="../src/tabletlockwidget.cpp" line="289"/>
        <source>Please scan by correct WeChat</source>
        <translation>ཡང་དག་པའི་འཕྲིན་ཕྲན་ཐོག་ནས་ཞིབ་བཤེར་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/tabletlockwidget.cpp" line="180"/>
        <location filename="../src/tabletlockwidget.cpp" line="200"/>
        <location filename="../src/tabletlockwidget.cpp" line="219"/>
        <location filename="../src/tabletlockwidget.cpp" line="234"/>
        <location filename="../src/tabletlockwidget.cpp" line="373"/>
        <location filename="../src/tabletlockwidget.cpp" line="388"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../src/tabletlockwidget.cpp" line="186"/>
        <location filename="../src/tabletlockwidget.cpp" line="398"/>
        <source>Back</source>
        <translation>ཕྱིར་ལོག་པ།</translation>
    </message>
    <message>
        <location filename="../src/tabletlockwidget.cpp" line="239"/>
        <source>Skip</source>
        <translation>མཆོང་བ།</translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <location filename="../KylinNM/src/utils.cpp" line="87"/>
        <source>kylin network applet desktop message</source>
        <translation>kylinདྲ་རྒྱའི་ཀུ་ཤུའི་ཅོག་ཙེའི་ཆ་འཕྲིན།</translation>
    </message>
</context>
<context>
    <name>VerificationWidget</name>
    <message>
        <location filename="../src/verificationwidget.cpp" line="55"/>
        <source>Please scan by bound WeChat</source>
        <translation>སྦྲེལ་ཟིན་པའི་སྐད་འཕྲིན་ཨང་གྲངས་བཀོལ་རོགས།</translation>
    </message>
</context>
<context>
    <name>VerticalVerificationWidget</name>
    <message>
        <location filename="../src/verticalVerificationwidget.cpp" line="52"/>
        <source>Please scan by bound WeChat</source>
        <translation>འཕྲིན་ཕྲན་གྱིས་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
</context>
<context>
    <name>WeChatAuthDialog</name>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="74"/>
        <location filename="../src/wechatauthdialog.cpp" line="136"/>
        <source>Login by wechat</source>
        <translation>སྐད་འཕྲིན་ཐོ་འགོད་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="78"/>
        <location filename="../src/wechatauthdialog.cpp" line="140"/>
        <source>Verification by wechat</source>
        <translation>སྐད་འཕྲིན་ཞིབ་བཤེར་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="75"/>
        <location filename="../src/wechatauthdialog.cpp" line="137"/>
        <source>「 Use registered WeChat account to login 」</source>
        <translation>「ཐོ་འགོད་བྱས་ཟིན་པའི་སྐད་འཕྲིན་གྱི་ཐོ་ཁོངས་ལ་བརྟེན་ནས་ཐོ་འགོད་བྱེད་པ།」</translation>
    </message>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="79"/>
        <location filename="../src/wechatauthdialog.cpp" line="141"/>
        <source>「 Use bound WeChat account to verification 」</source>
        <translation>「ཚོད་འཛིན་རང་བཞིན་གྱི་སྐད་འཕྲིན་ཨང་གྲངས་ལ་བརྟེན་ནས་ཞིབ་བཤེར་བྱ་དགོས།」</translation>
    </message>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="128"/>
        <location filename="../src/wechatauthdialog.cpp" line="183"/>
        <source>Network not connected~</source>
        <translation>མ་ལག་དྲ་སྦྲེལ་བྱས་མེད་པས་དྲ་སྦྲེལ་ལ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="227"/>
        <source>Scan code successfully</source>
        <translation>ཞིབ་བཤེར་ཨང་གྲངས་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="252"/>
        <source>Timeout!Try again!</source>
        <translation>དུས་ཚོད་ཕྱིར་འགོར་སོང་། ཡང་བསྐྱར་ཚོད་ལྟ།!</translation>
    </message>
    <message>
        <source>Login failed</source>
        <translation type="vanished">登录失败</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="42"/>
        <source>Start command for the ukui ScreenSaver.</source>
        <translation>སྒོ་ལྕགས་རྒྱག་པ་ནས་བཀའ་རྒྱའི་བར།</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="47"/>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="164"/>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="166"/>
        <source>lock the screen immediately</source>
        <translation>འཕྲལ་དུ་བརྙན་ཤེལ་ལ་ཟྭ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="50"/>
        <source>query the status of the screen saver</source>
        <translation>བརྙན་ཤེལ་གསོག་འཇོག་བྱེད་མཁན་གྱི་གནས་ཚུལ་ལ་འདྲི་རྩད་བྱས།</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="53"/>
        <source>unlock the screen saver</source>
        <translation>བརྙན་ཤེལ་གསོག་འཇོག་བྱེད་མཁན་ལ་སྒོ་འབྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="55"/>
        <source>show the screensaver</source>
        <translation>བརྙན་ཤེལ་གྱི་བརྙན་ཤེལ་ལ་བལྟ་རུ་འཇུག་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="159"/>
        <source>Dialog for the ukui ScreenSaver.</source>
        <translation>དབྱིན་ཇིའི་བརྙན་ཤེལ་གྱི་བརྙན་ཤེལ་ལ་ཁ་པར་རྒྱག་པའི་གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="168"/>
        <source>activated by session idle signal</source>
        <translation>སྐབས་འདིའི་ཁོམ་པའི་བརྡ་རྟགས་ཀྱིས་སྐུལ་སློང་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="170"/>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="174"/>
        <source>lock the screen and show screensaver immediately</source>
        <translation>བརྙན་ཤེལ་ལ་ཟྭ་བརྒྱབ་ནས་འཕྲལ་མར་བརྙན་ཤེལ་གྱི་བརྙན་ཤེལ་སྟོན་པ།</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="172"/>
        <source>show screensaver immediately</source>
        <translation>འཕྲལ་མར་བརྙན་ཤེལ་གྱི་བརྙན་ཤེལ་སྟོན་པ།</translation>
    </message>
    <message>
        <location filename="../screensaver/main.cpp" line="65"/>
        <source>Screensaver for ukui-screensaver</source>
        <translation>དབྱིན་ཇིའི་བརྙན་ཤེལ་གྱི་བརྙན་ཤེལ་ལ་ལྟ་ཞིབ་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location filename="../screensaver/main.cpp" line="69"/>
        <source>show on root window</source>
        <translation>རྩ་བའི་སྒེའུ་ཁུང་ནས་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../screensaver/main.cpp" line="71"/>
        <source>show on window.</source>
        <translation>སྒེའུ་ཁུང་ནས་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../screensaver/main.cpp" line="72"/>
        <source>window id</source>
        <translation>སྒེའུ་ཁུང་གི་ཐོབ་ཐང་</translation>
    </message>
</context>
</TS>
