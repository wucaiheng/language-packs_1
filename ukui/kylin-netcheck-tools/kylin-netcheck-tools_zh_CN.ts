<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>ConfigWin</name>
    <message>
        <location filename="../src/config_win.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>是</source>
        <translation type="vanished">是</translation>
    </message>
    <message>
        <location filename="../src/config_win.ui" line="274"/>
        <source>SetInner</source>
        <translation>开启内网检测</translation>
    </message>
    <message>
        <location filename="../src/config_win.ui" line="309"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../src/config_win.ui" line="328"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../src/config_win.ui" line="474"/>
        <location filename="../src/config_win.cpp" line="32"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/config_win.ui" line="509"/>
        <location filename="../src/config_win.cpp" line="33"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../src/config_win.cpp" line="15"/>
        <source>Config</source>
        <translation>设置</translation>
    </message>
</context>
<context>
    <name>DHCPCheck</name>
    <message>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="12"/>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="14"/>
        <source>DHCP Config</source>
        <translation>DHCP 服务</translation>
    </message>
    <message>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="15"/>
        <source>Are DHCP config right?</source>
        <translation>检测 DHCP 服务是否正常工作</translation>
    </message>
    <message>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="44"/>
        <source>Checking DHCP config</source>
        <translation>检测 DHCP 服务是否正常工作</translation>
    </message>
    <message>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="44"/>
        <source>Checking</source>
        <translation>检测中...</translation>
    </message>
    <message>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="52"/>
        <source>DHCP RUNNING RIGHT</source>
        <translation>DHCP 服务工作正常</translation>
    </message>
    <message>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="52"/>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="65"/>
        <source>OK</source>
        <translation>正常</translation>
    </message>
    <message>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="58"/>
        <source>DHCP DISTRIBUTED WRONG IP</source>
        <translation>DHCP分配了错误的IP</translation>
    </message>
    <message>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="58"/>
        <source>ERR</source>
        <translation>异常</translation>
    </message>
    <message>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="65"/>
        <source>DHCP IS OFF, NO CHECK</source>
        <translation>DHCP未开启，不检测</translation>
    </message>
</context>
<context>
    <name>DNSCheck</name>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="13"/>
        <location filename="../../DNSCheck/dnscheck.cpp" line="16"/>
        <source>DNS Config</source>
        <translation>DNS 服务</translation>
    </message>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="14"/>
        <source>Are DNS config right?</source>
        <translation>检测 DNS 是否配置，地址解析服务是否正常</translation>
    </message>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="207"/>
        <source>Checking DNS config</source>
        <translation>检测 DNS 是否配置，地址解析服务是否正常</translation>
    </message>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="207"/>
        <source>Checking</source>
        <translation>检测中...</translation>
    </message>
    <message>
        <source>Checking DHCP config</source>
        <translation type="vanished">检测 DHCP 服务是否正常工作</translation>
    </message>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="256"/>
        <source>NO DNS</source>
        <translation>DNS配置不正确</translation>
    </message>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="256"/>
        <location filename="../../DNSCheck/dnscheck.cpp" line="263"/>
        <location filename="../../DNSCheck/dnscheck.cpp" line="268"/>
        <source>ERR</source>
        <translation>异常</translation>
    </message>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="263"/>
        <source>INNER NET RESOLVE TIME OUT</source>
        <translation>内网网址DNS解析超时</translation>
    </message>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="268"/>
        <source>HAS DNS,TIME OUT</source>
        <translation>有配置DNS，但解析超时</translation>
    </message>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="274"/>
        <source>HAS DNS,CONNECTED</source>
        <translation>DNS 服务工作正常</translation>
    </message>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="274"/>
        <source>OK</source>
        <translation>正常</translation>
    </message>
</context>
<context>
    <name>DetailButton</name>
    <message>
        <location filename="../../customWidget/detailbutton.cpp" line="12"/>
        <source>detail</source>
        <translation>查看详情</translation>
    </message>
</context>
<context>
    <name>HWCheck</name>
    <message>
        <source>NetWork HardWare</source>
        <translation type="vanished">网络硬件配置</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="12"/>
        <location filename="../../HWCheck/hwcheck.cpp" line="14"/>
        <source>HardWare</source>
        <translation>网络硬件配置</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="13"/>
        <source>Are network card OK and cable connected?</source>
        <translation>检测网线是否插好，网卡及驱动是否正常工作</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="51"/>
        <source>Checking NetWork HardWares</source>
        <translation>检测网线是否插好，网卡及驱动是否正常工作</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="51"/>
        <source>Checking</source>
        <translation>检测中...</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="58"/>
        <source>NetWork HardWares are OK,Primary Wired.</source>
        <translation>网络硬件配置正常，首选有线</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="58"/>
        <location filename="../../HWCheck/hwcheck.cpp" line="63"/>
        <source>OK</source>
        <translation>正常</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="63"/>
        <source>NetWork HardWares are OK,Primary Wireless.</source>
        <translation>网络硬件配置正常，首选无线</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="70"/>
        <source>NetWork HardWares are OK, but no connection</source>
        <translation>未连接可用网络</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="70"/>
        <location filename="../../HWCheck/hwcheck.cpp" line="75"/>
        <source>ERR</source>
        <translation>异常</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="75"/>
        <source>No valid net card</source>
        <translation>无可用硬件设备</translation>
    </message>
</context>
<context>
    <name>HelpManual</name>
    <message>
        <location filename="../src/help_manual.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/help_manual.ui" line="120"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../src/help_manual.ui" line="289"/>
        <source>概述</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/help_manual.ui" line="365"/>
        <source>网络诊断工具是一款网络故障检测工具，可帮助您一键全面诊断网络故障，解决无法上网
的问题。同时支持定制化检测内网IP是否可达、网站是否能正常服务。</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/help_manual.ui" line="429"/>
        <source>打开方式</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/help_manual.ui" line="496"/>
        <source>“开始菜单”&gt;“网络诊断工具”或“任务栏”&gt;“搜索”&gt;“网络诊断工具”</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/help_manual.ui" line="550"/>
        <source>基本操作</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/help_manual.ui" line="617"/>
        <source>1.外网检测：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/help_manual.ui" line="684"/>
        <source>打开网络诊断工具，点击“网络检测”一键全面诊断网络故障，解决无法上网的问题（如
图1所示）。</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/help_manual.ui" line="789"/>
        <source>图1 网络诊断工具主界面</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>HostCheck</name>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="14"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="16"/>
        <source>Host File</source>
        <translation>host文件</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="17"/>
        <source>Are Host File config right?</source>
        <translation>检测 host 文件格式是否正常</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="34"/>
        <source>No host file!</source>
        <translation>没有host文件</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="57"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="59"/>
        <source>Has no sperated line.</source>
        <translation>host文件中有未加空格的行</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="69"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="71"/>
        <source>Ipv4 localhost error.</source>
        <translation>IPv4 localhost错误</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="85"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="87"/>
        <source>Ipv4 localPChost error.</source>
        <translation>IPv4 localhost错误</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="95"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="97"/>
        <source>Ipv6 localhost error.</source>
        <translation>IPv6 localhost错误</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="105"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="107"/>
        <source>Ipv6 localnet error.</source>
        <translation>IPv6 localnet错误</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="115"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="117"/>
        <source>Ipv6 mcastsprefix error.</source>
        <translation>IPv6 mcastsprefix错误</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="125"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="127"/>
        <source>Ipv6 nodes error.</source>
        <translation>IPv6 nodes错误</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="135"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="137"/>
        <source>Ipv6 routers error.</source>
        <translation>IPv6 routers错误</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="153"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="155"/>
        <source>User add illegal hosts.</source>
        <translation>用户添加的hosts映射不符合规范</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="204"/>
        <source>Checking Host Files</source>
        <translation>检测 host 文件格式是否正常</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="204"/>
        <source>Checking</source>
        <translation>检测中...</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="209"/>
        <source>Hosts Files are OK</source>
        <translation>host 文件配置正常</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="209"/>
        <source>OK</source>
        <translation>正常</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="214"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="219"/>
        <source>ERR</source>
        <translation>异常</translation>
    </message>
</context>
<context>
    <name>IPCheck</name>
    <message>
        <location filename="../../IPCheck/ipcheck.cpp" line="15"/>
        <location filename="../../IPCheck/ipcheck.cpp" line="17"/>
        <source>IP Config</source>
        <translation>网络连接配置</translation>
    </message>
    <message>
        <location filename="../../IPCheck/ipcheck.cpp" line="16"/>
        <source>Are IP config right?</source>
        <translation>检测网卡相关设置，是否可获取到IP 地址</translation>
    </message>
    <message>
        <location filename="../../IPCheck/ipcheck.cpp" line="52"/>
        <source>Checking IP config</source>
        <translation>检测网卡相关设置，是否可获取到IP 地址</translation>
    </message>
    <message>
        <location filename="../../IPCheck/ipcheck.cpp" line="52"/>
        <source>Checking</source>
        <translation>检测中...</translation>
    </message>
    <message>
        <location filename="../../IPCheck/ipcheck.cpp" line="57"/>
        <source>DHCP ON</source>
        <translation>DHCP开启，此项不检查</translation>
    </message>
    <message>
        <location filename="../../IPCheck/ipcheck.cpp" line="57"/>
        <location filename="../../IPCheck/ipcheck.cpp" line="67"/>
        <source>OK</source>
        <translation>正常</translation>
    </message>
    <message>
        <location filename="../../IPCheck/ipcheck.cpp" line="62"/>
        <source>IP CONFIG FALSE</source>
        <translation>网关和 IP 地址不在同一网段</translation>
    </message>
    <message>
        <location filename="../../IPCheck/ipcheck.cpp" line="62"/>
        <source>ERR</source>
        <translation>异常</translation>
    </message>
    <message>
        <location filename="../../IPCheck/ipcheck.cpp" line="67"/>
        <source>IP CONFIG RIGHT</source>
        <translation>网络连接配置正常</translation>
    </message>
</context>
<context>
    <name>IPWebWidget</name>
    <message>
        <location filename="../src/ipweb_widget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ipweb_widget.ui" line="103"/>
        <source>Addr</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../src/ipweb_widget.cpp" line="32"/>
        <source>IP</source>
        <translation>IP 地址</translation>
    </message>
    <message>
        <location filename="../src/ipweb_widget.cpp" line="36"/>
        <source>Website</source>
        <translation>网站地址</translation>
    </message>
    <message>
        <location filename="../src/ipweb_widget.cpp" line="81"/>
        <source>Format error,IP is invalid</source>
        <translation>错误格式，IP地址无效</translation>
    </message>
    <message>
        <location filename="../src/ipweb_widget.cpp" line="94"/>
        <source>Format error,web is invalid</source>
        <translation>错误格式，web地址无效</translation>
    </message>
</context>
<context>
    <name>IncreaseWidget</name>
    <message>
        <location filename="../src/increase_widget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ItemWidget</name>
    <message>
        <location filename="../../customWidget/item_widget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../customWidget/item_widget.cpp" line="86"/>
        <source>Checking</source>
        <translation>检测中...</translation>
    </message>
    <message>
        <location filename="../../customWidget/item_widget.cpp" line="90"/>
        <location filename="../../customWidget/item_widget.cpp" line="102"/>
        <location filename="../../customWidget/item_widget.cpp" line="110"/>
        <source>OK</source>
        <translation>正常</translation>
    </message>
    <message>
        <location filename="../../customWidget/item_widget.cpp" line="94"/>
        <location filename="../../customWidget/item_widget.cpp" line="106"/>
        <location filename="../../customWidget/item_widget.cpp" line="114"/>
        <source>ERR</source>
        <translation>异常</translation>
    </message>
    <message>
        <location filename="../../customWidget/item_widget.cpp" line="98"/>
        <location filename="../../customWidget/item_widget.cpp" line="118"/>
        <source>WARNING</source>
        <translation>警告</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.ui" line="32"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <source>Start Check</source>
        <translation type="vanished">开始检测</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="358"/>
        <source>PushButton</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="426"/>
        <location filename="../src/mainwindow.cpp" line="38"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="451"/>
        <location filename="../src/mainwindow.cpp" line="41"/>
        <source>Restart</source>
        <translation>重新检测</translation>
    </message>
    <message>
        <source>Check Again</source>
        <translation type="vanished">重新检测</translation>
    </message>
    <message>
        <source>NetWork Check Tools</source>
        <translation type="vanished">网络检测工具</translation>
    </message>
    <message>
        <source>检测中...</source>
        <translation type="vanished">检测中...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="472"/>
        <source>Checking...</source>
        <translation>检测中...</translation>
    </message>
    <message>
        <source>No Error</source>
        <translation type="vanished">没有问题</translation>
    </message>
    <message>
        <source>开始检测</source>
        <translation type="vanished">开始检测</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="496"/>
        <location filename="../src/mainwindow.cpp" line="524"/>
        <location filename="../src/mainwindow.cpp" line="608"/>
        <source>Check Result</source>
        <translation>检测结果</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="498"/>
        <source>Find %1 issues,%2 is error,repair and start</source>
        <translation>发现 %1 项问题，%2 项提示问题，请修复后重新检测</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="510"/>
        <source>Stopping, please wait!</source>
        <translation>停止中，请等待！</translation>
    </message>
    <message>
        <source>Extranet Web</source>
        <translation type="vanished">外网地址</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="730"/>
        <source>Intranet IP</source>
        <translation>内网IP地址</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="733"/>
        <source>Intranet Web</source>
        <translation>内网网站地址</translation>
    </message>
    <message>
        <source>Find %1 errs,net err,repair and restart</source>
        <translation type="vanished">发现 %1 项问题，网络存在异常，请修复后重新检测</translation>
    </message>
    <message>
        <source>Checked %1 items,no err,extranet ok</source>
        <translation type="vanished">已检测 %1 项，未发现问题，外网通畅</translation>
    </message>
    <message>
        <source>Find %1 errs,extranet ok</source>
        <translation type="vanished">发现 %1 项问题，外网通畅</translation>
    </message>
    <message>
        <source>Checked %1 items,no err,intranet ok</source>
        <translation type="vanished">已检测 %1 项，未发现问题，内网通畅</translation>
    </message>
    <message>
        <source>Find %1 errs,intranet ok</source>
        <translation type="vanished">发现 %1 项问题，内网通畅</translation>
    </message>
    <message>
        <source>Checked %1 items,no issue</source>
        <translation type="vanished">已检测 %1 项，未发现问题</translation>
    </message>
    <message>
        <source>Checked %1 items,find %2 errs,%3 issues</source>
        <translation type="vanished">已检测 %1 项，发现 %2 项错误，%3项提示错误</translation>
    </message>
    <message>
        <source>You can check again now!</source>
        <translation type="vanished">您现在可以重新检测了！</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="401"/>
        <location filename="../src/mainwindow.cpp" line="473"/>
        <source>Start</source>
        <translation>开始检测</translation>
    </message>
    <message>
        <source>Find %1 issues，%2 is error，repair and start</source>
        <translation type="vanished">发现 %1 项问题，%2 项提示问题，请修复后重新检测</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="527"/>
        <source>Pause，checked %1 item，no issue</source>
        <translation>扫描中断，已检测 %1 项，未发现问题</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="531"/>
        <source>Pause，find %1 issues，repair and start</source>
        <translation>扫描中断，发现 %1 项问题，请修复后重新检测</translation>
    </message>
    <message>
        <source>finished，no issue</source>
        <translation type="vanished">扫描结束，未发现问题</translation>
    </message>
    <message>
        <source>finished，have issue</source>
        <translation type="vanished">扫描结束，发现问题</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="622"/>
        <source>find %1 errs,%2 issues,repair and start</source>
        <translation>发现 %1 项问题，%2 项提示问题，请修复后重新检测</translation>
    </message>
    <message>
        <source>IP</source>
        <translation type="vanished">IP 地址</translation>
    </message>
    <message>
        <source>Website</source>
        <translation type="vanished">网站地址</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="55"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="106"/>
        <source>NetCheck</source>
        <translation>网络检测</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="112"/>
        <source>total 7 items</source>
        <translation>共 7 项</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="823"/>
        <source>InnerNet Check</source>
        <translation>内网检测</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="824"/>
        <source>Can user browse inner net?</source>
        <translation>检测您的电脑是否可以正常、流畅的访问内网</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="827"/>
        <source>OutNet Check</source>
        <translation>外网检测</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="828"/>
        <source>Can user browse out net?</source>
        <translation>检测您的电脑是否可以正常、流畅的访问外网</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="854"/>
        <source>checked %1 items,no issue</source>
        <translation>已检测 %1 项，未发现问题</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="856"/>
        <source>checked %1 items,find %2 errs,%3 issues</source>
        <translation>已检测 %1 项，发现 %2 项问题，%3项提示问题</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="618"/>
        <source>checked %1 items，no issue</source>
        <translation>已检测 %1 项，未发现问题</translation>
    </message>
    <message>
        <source>checked %1 items，find %2 issues</source>
        <translation type="vanished">已检测 %1 项，发现 %2 项问题</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="290"/>
        <location filename="../src/mainwindow.cpp" line="27"/>
        <location filename="../src/mainwindow.cpp" line="904"/>
        <source>Detect and resolve Network Faults</source>
        <translation>全面诊断网络故障，解决无法上网问题</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="249"/>
        <location filename="../src/mainwindow.cpp" line="24"/>
        <location filename="../src/mainwindow.cpp" line="905"/>
        <source>Detect Network Faults</source>
        <translation>网络故障  一键检测</translation>
    </message>
    <message>
        <source>已检测 %1 项</source>
        <translation type="vanished">已检测 %1 项</translation>
    </message>
</context>
<context>
    <name>MenuModule</name>
    <message>
        <location filename="../src/menumodule.cpp" line="67"/>
        <location filename="../src/menumodule.cpp" line="108"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="69"/>
        <location filename="../src/menumodule.cpp" line="105"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="71"/>
        <location filename="../src/menumodule.cpp" line="112"/>
        <source>Configure</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="73"/>
        <location filename="../src/menumodule.cpp" line="101"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="141"/>
        <source>Network-check-tool is a software that can quickly detect,diagnose,and optimize networks.</source>
        <translation>网络检测工具是一款快速检测、诊断、优化网络的软件，操作流程简单便捷。</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="140"/>
        <location filename="../src/menumodule.cpp" line="262"/>
        <source>Version: </source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="266"/>
        <source>Network-check-tool is a software that can quickly detect, diagnose, and optimize networks. </source>
        <translation>网络检测工具是一款快速检测、诊断、优化网络的软件，操作流程简单便捷。</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="278"/>
        <location filename="../src/menumodule.cpp" line="361"/>
        <location filename="../src/menumodule.cpp" line="369"/>
        <source>Service &amp; Support: </source>
        <translation>服务与支持：</translation>
    </message>
    <message>
        <location filename="../include/menumodule.h" line="61"/>
        <location filename="../include/menumodule.h" line="62"/>
        <location filename="../include/menumodule.h" line="64"/>
        <source>NetWork Check Tools</source>
        <translation>网络检测工具</translation>
    </message>
</context>
<context>
    <name>NetCheck</name>
    <message>
        <source>O/INetCheck</source>
        <translation type="vanished">外/内网检测</translation>
    </message>
    <message>
        <source>Can use browse net?</source>
        <translation type="vanished">检测您的电脑是否可以正常、流畅的访问网络</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="25"/>
        <location filename="../../NetCheck/netcheck.cpp" line="26"/>
        <location filename="../../NetCheck/netcheck.cpp" line="257"/>
        <location filename="../../NetCheck/netcheck.cpp" line="258"/>
        <source>InnerNet Check</source>
        <translation>内网检测</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="27"/>
        <source>Can user browse inner net?</source>
        <translation>检测您的电脑是否可以正常、流畅的访问内网</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="31"/>
        <location filename="../../NetCheck/netcheck.cpp" line="32"/>
        <location filename="../../NetCheck/netcheck.cpp" line="260"/>
        <location filename="../../NetCheck/netcheck.cpp" line="261"/>
        <source>OutNet Check</source>
        <translation>外网检测</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="33"/>
        <source>Can user browse out net?</source>
        <translation>检测您的电脑是否可以正常、流畅的访问外网</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="295"/>
        <location filename="../../NetCheck/netcheck.cpp" line="298"/>
        <location filename="../../NetCheck/netcheck.cpp" line="307"/>
        <location filename="../../NetCheck/netcheck.cpp" line="312"/>
        <location filename="../../NetCheck/netcheck.cpp" line="315"/>
        <location filename="../../NetCheck/netcheck.cpp" line="318"/>
        <location filename="../../NetCheck/netcheck.cpp" line="328"/>
        <location filename="../../NetCheck/netcheck.cpp" line="336"/>
        <source>OK</source>
        <translation>正常</translation>
    </message>
    <message>
        <source>Extranet err</source>
        <translation type="vanished">您的电脑网络存在异常</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="307"/>
        <location filename="../../NetCheck/netcheck.cpp" line="312"/>
        <source>Intranet normal</source>
        <translation>内网检测通畅</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="315"/>
        <location filename="../../NetCheck/netcheck.cpp" line="325"/>
        <source>Url cannot be accessed</source>
        <translation>内网指定站点无法正常访问</translation>
    </message>
    <message>
        <source>Network is error</source>
        <translation type="vanished">您的电脑网络存在异常</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="318"/>
        <source>IP is reachable</source>
        <translation>内网 IP 地址可达</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="328"/>
        <source>IP is reachable，url cannot be accessed</source>
        <translation>内网 IP 地址可达，内网指定站点无法正常访问</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="333"/>
        <source>IP is unreachable</source>
        <translation>内网 IP 地址不可达</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="336"/>
        <source>IP is unreachable，url can be accessed</source>
        <translation>内网 IP 地址不可达，内网指定站点能正常访问</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="343"/>
        <source>IP is unreachable，url cannot be accessed</source>
        <translation>内网 IP 地址不可达，内网指定站点无法正常访问</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="295"/>
        <location filename="../../NetCheck/netcheck.cpp" line="298"/>
        <source>Extranet normal</source>
        <translation>外网检测通畅</translation>
    </message>
    <message>
        <source>Extranet accessed</source>
        <translation type="vanished">外网可连通</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="301"/>
        <source>Extranet abnormal</source>
        <translation>外网检测失败</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="301"/>
        <location filename="../../NetCheck/netcheck.cpp" line="325"/>
        <location filename="../../NetCheck/netcheck.cpp" line="333"/>
        <location filename="../../NetCheck/netcheck.cpp" line="343"/>
        <source>ERR</source>
        <translation>异常</translation>
    </message>
    <message>
        <source>not perfect connected!</source>
        <translation type="vanished">可以连接，但存在问题！</translation>
    </message>
    <message>
        <source>can not connected!</source>
        <translation type="vanished">无法联网！</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="254"/>
        <source>Checking NetWorks</source>
        <translation>检测 IP 是否可达，网站能否访问</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="254"/>
        <source>Checking</source>
        <translation>检测中...</translation>
    </message>
</context>
<context>
    <name>NetCheckHomePage</name>
    <message>
        <location filename="../src/netcheckhomepage.cpp" line="10"/>
        <source>Check and Repair</source>
        <translation>电脑故障 一键排查</translation>
    </message>
    <message>
        <location filename="../src/netcheckhomepage.cpp" line="17"/>
        <source>Detection and repair of computer problems！</source>
        <translation>快速检测修复故障问题，为您的电脑健康保驾护航</translation>
    </message>
    <message>
        <location filename="../src/netcheckhomepage.cpp" line="30"/>
        <source>NetCheck</source>
        <translation>网络检测</translation>
    </message>
    <message>
        <location filename="../src/netcheckhomepage.cpp" line="46"/>
        <source>Start</source>
        <translation>开始检测</translation>
    </message>
    <message>
        <location filename="../src/netcheckhomepage.cpp" line="76"/>
        <source>IntraNetSet</source>
        <translation>内网检测设置</translation>
    </message>
</context>
<context>
    <name>ProxyCheck</name>
    <message>
        <location filename="../../ProxyCheck/proxycheck.cpp" line="19"/>
        <source>NetWork Proxy</source>
        <translation>浏览器配置</translation>
    </message>
    <message>
        <source>Checking NetWork HardWares</source>
        <translation type="vanished">检测系统代理配置问题</translation>
    </message>
    <message>
        <location filename="../../ProxyCheck/proxycheck.cpp" line="47"/>
        <source>Checking</source>
        <translation>检测中...</translation>
    </message>
    <message>
        <location filename="../../ProxyCheck/proxycheck.cpp" line="21"/>
        <source>Proxy</source>
        <translation>浏览器配置</translation>
    </message>
    <message>
        <location filename="../../ProxyCheck/proxycheck.cpp" line="22"/>
        <location filename="../../ProxyCheck/proxycheck.cpp" line="47"/>
        <source>Check whether the proxy is working?</source>
        <translation>检测系统代理配置问题</translation>
    </message>
    <message>
        <location filename="../../ProxyCheck/proxycheck.cpp" line="52"/>
        <source>proxy disable</source>
        <translation>代理未开启</translation>
    </message>
    <message>
        <location filename="../../ProxyCheck/proxycheck.cpp" line="57"/>
        <source>auto proxy normal</source>
        <translation>自动代理正常</translation>
    </message>
    <message>
        <location filename="../../ProxyCheck/proxycheck.cpp" line="62"/>
        <source>auto proxy abnormal</source>
        <translation>自动代理异常</translation>
    </message>
    <message>
        <location filename="../../ProxyCheck/proxycheck.cpp" line="66"/>
        <source>manual proxy normal</source>
        <translation>手动代理正常</translation>
    </message>
</context>
<context>
    <name>QuadBtnsTitleBar</name>
    <message>
        <location filename="../src/quad_btns_title_bar.cpp" line="16"/>
        <source>menu</source>
        <translation>菜单</translation>
    </message>
    <message>
        <location filename="../src/quad_btns_title_bar.cpp" line="25"/>
        <source>minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../src/quad_btns_title_bar.cpp" line="39"/>
        <source>full screen</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../src/quad_btns_title_bar.cpp" line="53"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
</context>
</TS>
