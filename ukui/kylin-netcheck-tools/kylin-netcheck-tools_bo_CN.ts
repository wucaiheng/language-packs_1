<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>ConfigWin</name>
    <message>
        <location filename="../src/config_win.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>是</source>
        <translation type="vanished">是</translation>
    </message>
    <message>
        <location filename="../src/config_win.ui" line="274"/>
        <source>SetInner</source>
        <translation>ནང་དྲ་ཞིབ་དཔྱད་ཚད་ལེན་གྱི་སྒོ་ཕྱེ།</translation>
    </message>
    <message>
        <location filename="../src/config_win.ui" line="309"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location filename="../src/config_win.ui" line="328"/>
        <source>No</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../src/config_win.ui" line="474"/>
        <location filename="../src/config_win.cpp" line="32"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../src/config_win.ui" line="509"/>
        <location filename="../src/config_win.cpp" line="33"/>
        <source>Save</source>
        <translation>གསོག་ཉར།</translation>
    </message>
    <message>
        <location filename="../src/config_win.cpp" line="15"/>
        <source>Config</source>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
</context>
<context>
    <name>DHCPCheck</name>
    <message>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="12"/>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="14"/>
        <source>DHCP Config</source>
        <translation>DHCP ཞབས་ཞུ།</translation>
    </message>
    <message>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="15"/>
        <source>Are DHCP config right?</source>
        <translation>DHCPཞབས་ཞུ་རྒྱུན་ལྡན་ཡིན་མིན་ལ་བརྟག་དཔྱད་བྱས།</translation>
    </message>
    <message>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="44"/>
        <source>Checking DHCP config</source>
        <translation>DHCPཞབས་ཞུ་རྒྱུན་ལྡན་ཡིན་མིན་ལ་བརྟག་དཔྱད་བྱས།</translation>
    </message>
    <message>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="44"/>
        <source>Checking</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="52"/>
        <source>DHCP RUNNING RIGHT</source>
        <translation>DHCPཞབས་ཞུའི་བྱ་བ་རྒྱུན་ལྡན་རེད།</translation>
    </message>
    <message>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="52"/>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="65"/>
        <source>OK</source>
        <translation>རྒྱུན་ལྡན།</translation>
    </message>
    <message>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="58"/>
        <source>DHCP DISTRIBUTED WRONG IP</source>
        <translation>DHCPཡིས་ནོར་འཁྲུལ་གྱི་IPབགོ་བཤའ་བྱས།</translation>
    </message>
    <message>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="58"/>
        <source>ERR</source>
        <translation>རྒྱན་ལྡན་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../../DHCPCheck/dhcpcheck.cpp" line="65"/>
        <source>DHCP IS OFF, NO CHECK</source>
        <translation>DHCPསྒོ་ཕྱེ་མེད།ཞིབ་བཤེར་མི་བྱེད།</translation>
    </message>
</context>
<context>
    <name>DNSCheck</name>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="13"/>
        <location filename="../../DNSCheck/dnscheck.cpp" line="16"/>
        <source>DNS Config</source>
        <translation>DNSཞབས་ཞུ།</translation>
    </message>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="14"/>
        <source>Are DNS config right?</source>
        <translation>DNS བཀོད་སྒྲིག་བྱས་ཡོད་མེད་དང་།ས་གནས་དབྱེ་ཞིབ་ཞབས་ཞུ་རྒྱུན་ལྡན་ཡིན་མིན་ཞིབ་དཔྱད་བྱས།</translation>
    </message>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="207"/>
        <source>Checking DNS config</source>
        <translation>DNSབཀོད་སྒྲིག་བྱས་ཡོད་མེད་དང་།ས་གནས་དབྱེ་ཞིབ་ཞབས་ཞུ་རྒྱུན་ལྡན་ཡིན་མིན་ཞིབ་དཔྱད་བྱས།</translation>
    </message>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="207"/>
        <source>Checking</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Checking DHCP config</source>
        <translation type="vanished">DHCPཡི་བག་ལེབ་ཀོར་མོ་ལ་ཞིབ་བཤེར་བྱས།</translation>
    </message>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="256"/>
        <source>NO DNS</source>
        <translation>DNS བཀོད་སྒྲིག་ཡང་དག་མ་རེད།</translation>
    </message>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="256"/>
        <location filename="../../DNSCheck/dnscheck.cpp" line="263"/>
        <location filename="../../DNSCheck/dnscheck.cpp" line="268"/>
        <source>ERR</source>
        <translation>རྒྱུན་ལྡན་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="263"/>
        <source>INNER NET RESOLVE TIME OUT</source>
        <translation>DNSནང་དྲ་དྲ་ཚིགས་དབྱེ་ཞིབ་དུས་ལས་བརྒལ་འདུག</translation>
    </message>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="268"/>
        <source>HAS DNS,TIME OUT</source>
        <translation>DNS,བཀོད་སྒྲིག་བྱས་ཡོད་མོད།འོན་ཀྱང་དབྱེ་ཞིབ་དུས་ལས་བརྒལ་འདུག</translation>
    </message>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="274"/>
        <source>HAS DNS,CONNECTED</source>
        <translation>DNSཞབས་ཞུའི་བྱ་བ་རྒྱུན་ལྡན་རེད།</translation>
    </message>
    <message>
        <location filename="../../DNSCheck/dnscheck.cpp" line="274"/>
        <source>OK</source>
        <translation>རྒྱུན་ལྡན།</translation>
    </message>
</context>
<context>
    <name>DetailButton</name>
    <message>
        <location filename="../../customWidget/detailbutton.cpp" line="12"/>
        <source>detail</source>
        <translation>ཞིབ་ཕྲའི་གནས་ཚུལ།</translation>
    </message>
</context>
<context>
    <name>HWCheck</name>
    <message>
        <source>NetWork HardWare</source>
        <translation type="vanished">དྲ་རྒྱའི་ལས་ཀ་མཁྲེགས་ཆས།</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="12"/>
        <location filename="../../HWCheck/hwcheck.cpp" line="14"/>
        <source>HardWare</source>
        <translation>དྲ་རྒྱའི་སྲ་མཁྲེགས་ཅན་གྱི་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="13"/>
        <source>Are network card OK and cable connected?</source>
        <translation>དྲ་སྐུད་བཙུགས་ཡོད་མེད་དང་དྲ་བྱང་དང་སྐུལ་འདེད་རྒྱུན་ལྡན་ཡིན་མིན་ཞིབ་བཤེར་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="51"/>
        <source>Checking NetWork HardWares</source>
        <translation>དྲ་སྐུད་བཙུགས་ཡོད་མེད་དང་དྲ་བྱང་དང་སྐུལ་འདེད་རྒྱུན་ལྡན་ཡིན་མིན་ཞིབ་བཤེར་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="51"/>
        <source>Checking</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="58"/>
        <source>NetWork HardWares are OK,Primary Wired.</source>
        <translation>དྲ་རྒྱའི་མཁྲེགས་ཆས་ལ་སྐྱོན་མེད། དམའ་རིམ་གློག་སྐུད་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="58"/>
        <location filename="../../HWCheck/hwcheck.cpp" line="63"/>
        <source>OK</source>
        <translation>རྒྱུན་ལྡན།</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="63"/>
        <source>NetWork HardWares are OK,Primary Wireless.</source>
        <translation>དྲ་རྒྱའི་མཁྲེགས་ཆས་ལ་སྐྱོན་མེད། དམའ་རིམ་སྐུད་མེད་ཡིན།</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="70"/>
        <source>NetWork HardWares are OK, but no connection</source>
        <translation>དྲ་རྒྱའི་ལས་ཀ་མཁྲེགས་ཆས་ཡག་པོ་ཡོད་ཀྱང་འབྲེལ་བ་མེད།</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="70"/>
        <location filename="../../HWCheck/hwcheck.cpp" line="75"/>
        <source>ERR</source>
        <translation>རྒྱུན་ལྡན་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../../HWCheck/hwcheck.cpp" line="75"/>
        <source>No valid net card</source>
        <translation>བཀོལ་སྤྱོད་བྱེད་མི་ཐུབ་པའི་མཁྲེགས་ཆས་སྒྲིག་ཆས།</translation>
    </message>
</context>
<context>
    <name>HelpManual</name>
    <message>
        <location filename="../src/help_manual.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/help_manual.ui" line="120"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/help_manual.ui" line="289"/>
        <source>概述</source>
        <translation>རགས་བཤད།</translation>
    </message>
    <message>
        <location filename="../src/help_manual.ui" line="365"/>
        <source>网络诊断工具是一款网络故障检测工具，可帮助您一键全面诊断网络故障，解决无法上网
的问题。同时支持定制化检测内网IP是否可达、网站是否能正常服务。</source>
        <translation>དྲ་རྒྱའི་བརྟག་དཔྱད་ཡོ་བྱད་ནི་དྲ་རྒྱའི་ཆག་སྐྱོན་བརྟག་དཔྱད་ཡོ་བྱད་ཅིག་ཡིན་པས་ཁྱེད་ལ་ཕྱོགས་ཡོངས་ནས་དྲ་རྒྱའི་ཆག་སྐྱོན་བརྟག་དཔྱད་བྱེད་པར་རོགས་རམ་བྱེད་ཐུབ།་ཐག་གཅོད་བྱེད་ཐབས་བྲལ་བ་རེད།གནད་དོན་།དུས་མཚུངས་ལམ་སྲོལ་ཅན་གྱི་ཞིབ་དཔྱད་ཚད་ལེན་ནང་དྲ་སླེབས་ཐུབ་མིན་དང་དྲ་ཚིགས་རྒྱུན་གཏན་ལྟར་ཞབས་འདེགས་ཞུ་ཐུབ་མིན་ལ་རྒྱབ་སྐྱོར་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/help_manual.ui" line="429"/>
        <source>打开方式</source>
        <translation>ཁ་འབྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../src/help_manual.ui" line="496"/>
        <source>“开始菜单”&gt;“网络诊断工具”或“任务栏”&gt;“搜索”&gt;“网络诊断工具”</source>
        <translation>“འགོ་ཚུགས་འདེམས་པང་།”&gt;“དྲ་རྒྱའི་བརྟག་དཔྱད་ཡོ་བྱད”ཡང་ན“ལས་འགན་རེའུ་མིག”&gt;“འཚོལ་བཤེར།”&gt;“དྲ་རྒྱའི་བརྟག་དཔྱད།”</translation>
    </message>
    <message>
        <location filename="../src/help_manual.ui" line="550"/>
        <source>基本操作</source>
        <translation>གཞི་རྩའི་བཀོལ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../src/help_manual.ui" line="617"/>
        <source>1.外网检测：</source>
        <translation>1.ཕྱི་དྲ་ཞིབ་དཔྱད་ཚད་ལེན།</translation>
    </message>
    <message>
        <location filename="../src/help_manual.ui" line="684"/>
        <source>打开网络诊断工具，点击“网络检测”一键全面诊断网络故障，解决无法上网的问题（如
图1所示）。</source>
        <translation>དྲ་རྒྱའི་བརྟག་དཔྱད་ཡོ་བྱད་ཁ་ཕྱེ་ནས་&quot;་དྲ་རྒྱའི་ཞིབ་དཔྱད་ཚད་ལེན་&quot;་གྱི་མཐེབ་བཀྱག་གཅིག་མནན་ནས་ཕྱོགས་ཡོངས་ནས་དྲ་རྒྱའི་སྐྱོན་ཆ་བརྟག་དཔྱད་བྱས་ཏེ་དྲ་བར་ཞུགས་ཐབས་བྲལ་བའི་གནད་དོན་ཐག་གཅོད་བྱ་དགོས།པར་རིས་1བསྟན་པ།་)</translation>
    </message>
    <message>
        <location filename="../src/help_manual.ui" line="789"/>
        <source>图1 网络诊断工具主界面</source>
        <translation>པར་རིས་1་དྲ་རྒྱའི་བརྟག་དཔྱད་ཡོ་བྱད་གཙོ་ངོས།</translation>
    </message>
</context>
<context>
    <name>HostCheck</name>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="14"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="16"/>
        <source>Host File</source>
        <translation>hostཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="17"/>
        <source>Are Host File config right?</source>
        <translation>host ཡིག་ཆའི་རྣམ་གཞག་རྒྱུན་ལྡན་ཡིན་མིན་ཞིབ་དཔྱད་ཚད་ལེན་བྱས།</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="34"/>
        <source>No host file!</source>
        <translation>hostཡིག་ཆ་མེད།</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="57"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="59"/>
        <source>Has no sperated line.</source>
        <translation>hostཡིག་ཆའི་ཁྲོད་དུ་སྟོང་ཆ་བསྣན་མེད་པའི་ཆོག</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="69"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="71"/>
        <source>Ipv4 localhost error.</source>
        <translation>IPv4 localhostནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="85"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="87"/>
        <source>Ipv4 localPChost error.</source>
        <translation>IPv4 localhostནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="95"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="97"/>
        <source>Ipv6 localhost error.</source>
        <translation>IPv4 localhost་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="105"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="107"/>
        <source>Ipv6 localnet error.</source>
        <translation>IPv4 localhostནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="115"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="117"/>
        <source>Ipv6 mcastsprefix error.</source>
        <translation>Ipv6 mcastprefixནོར་འཁྲུལ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="125"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="127"/>
        <source>Ipv6 nodes error.</source>
        <translation>IPv6 nodesནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="135"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="137"/>
        <source>Ipv6 routers error.</source>
        <translation>IPv6 routers་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="153"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="155"/>
        <source>User add illegal hosts.</source>
        <translation>hostsསྤྱོད་མཁན་གྱིས་བསྣན་པའི་་རྟེན་འཕྲོ་ཚད་ལྡན་དང་མི་མཐུན།</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="204"/>
        <source>Checking Host Files</source>
        <translation>host ཡིག་ཆའི་རྣམ་གཞག་རྒྱུན་ལྡན་ཡིན་མིན་ཞིབ་དཔྱད་ཚད་ལེན་བྱས།</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="204"/>
        <source>Checking</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="209"/>
        <source>Hosts Files are OK</source>
        <translation>host ཡིག་ཆ་བཀོད་སྒྲིག་རྒྱུན་ལྡན་རེད།</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="209"/>
        <source>OK</source>
        <translation>རྒྱུན་ལྡན།</translation>
    </message>
    <message>
        <location filename="../../HostCheck/hostcheck.cpp" line="214"/>
        <location filename="../../HostCheck/hostcheck.cpp" line="219"/>
        <source>ERR</source>
        <translation>རྒྱུན་ལྡན་མིན་པ།</translation>
    </message>
</context>
<context>
    <name>IPCheck</name>
    <message>
        <location filename="../../IPCheck/ipcheck.cpp" line="15"/>
        <location filename="../../IPCheck/ipcheck.cpp" line="17"/>
        <source>IP Config</source>
        <translation>དྲ་རྒྱའི་འབྲེལ་མཐུད་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../../IPCheck/ipcheck.cpp" line="16"/>
        <source>Are IP config right?</source>
        <translation>དྲ་བྱང་གི་འབྲེལ་ཡོད་སྒྲིག་བཀོད་ལ་ཞིབ་བཤེར་བྱས་ནIP་ས་གནས་ཐོབ་ཐུབ་བམ།</translation>
    </message>
    <message>
        <location filename="../../IPCheck/ipcheck.cpp" line="52"/>
        <source>Checking IP config</source>
        <translation>དྲ་བྱང་གི་འབྲེལ་ཡོད་སྒྲིག་བཀོད་ལ་ཞིབ་བཤེར་བྱས་ན་IP ས་གནས་ཐོབ་ཐུབ་བམ།</translation>
    </message>
    <message>
        <location filename="../../IPCheck/ipcheck.cpp" line="52"/>
        <source>Checking</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../IPCheck/ipcheck.cpp" line="57"/>
        <source>DHCP ON</source>
        <translation>DHCP སྒོ་ཕྱེ་སོང་།རྣམ་གྲངས་འདི་ལ་ཞིབ་བཤེར་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="../../IPCheck/ipcheck.cpp" line="57"/>
        <location filename="../../IPCheck/ipcheck.cpp" line="67"/>
        <source>OK</source>
        <translation>རྒྱུན་ལྡན།</translation>
    </message>
    <message>
        <location filename="../../IPCheck/ipcheck.cpp" line="62"/>
        <source>IP CONFIG FALSE</source>
        <translation>དྲ་སྒོ་དང་IPས་གནས་དྲ་ཚིགས་གཅིག་ན་མེད།</translation>
    </message>
    <message>
        <location filename="../../IPCheck/ipcheck.cpp" line="62"/>
        <source>ERR</source>
        <translation>རྒྱུན་ལྡན་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../../IPCheck/ipcheck.cpp" line="67"/>
        <source>IP CONFIG RIGHT</source>
        <translation>དྲ་རྒྱའི་འབྲེལ་མཐུད་བཀོད་སྒྲིག་རྒྱུན་ལྡན་རེད།</translation>
    </message>
</context>
<context>
    <name>IPWebWidget</name>
    <message>
        <location filename="../src/ipweb_widget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ipweb_widget.ui" line="103"/>
        <source>Addr</source>
        <translation>ས་གནས།</translation>
    </message>
    <message>
        <location filename="../src/ipweb_widget.cpp" line="32"/>
        <source>IP</source>
        <translation>IPས་གནས།</translation>
    </message>
    <message>
        <location filename="../src/ipweb_widget.cpp" line="36"/>
        <source>Website</source>
        <translation>དྲ་ཚིགས་ས་གནས།</translation>
    </message>
    <message>
        <location filename="../src/ipweb_widget.cpp" line="81"/>
        <source>Format error,IP is invalid</source>
        <translation>རྣམ་གཞག་ནོར་འཁྲུལ་བྱུང་ན་IPགནས་ལ་ནུས་པ་མེད།</translation>
    </message>
    <message>
        <location filename="../src/ipweb_widget.cpp" line="94"/>
        <source>Format error,web is invalid</source>
        <translation>རྣམ་གཞག་གི་ནོར་འཁྲུལ་དང་།web དྲ་རྒྱ་ནི་གོ་མི་ཆོད་པ།</translation>
    </message>
</context>
<context>
    <name>IncreaseWidget</name>
    <message>
        <location filename="../src/increase_widget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ItemWidget</name>
    <message>
        <location filename="../../customWidget/item_widget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../customWidget/item_widget.cpp" line="86"/>
        <source>Checking</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../customWidget/item_widget.cpp" line="90"/>
        <location filename="../../customWidget/item_widget.cpp" line="102"/>
        <location filename="../../customWidget/item_widget.cpp" line="110"/>
        <source>OK</source>
        <translation>རྒྱུན་ལྡན།</translation>
    </message>
    <message>
        <location filename="../../customWidget/item_widget.cpp" line="94"/>
        <location filename="../../customWidget/item_widget.cpp" line="106"/>
        <location filename="../../customWidget/item_widget.cpp" line="114"/>
        <source>ERR</source>
        <translation>རྒྱུན་ལྡན་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../../customWidget/item_widget.cpp" line="98"/>
        <location filename="../../customWidget/item_widget.cpp" line="118"/>
        <source>WARNING</source>
        <translation>ཉེན་བརྡ་གཏོང་བ།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.ui" line="32"/>
        <source>MainWindow</source>
        <translation>MainWindow</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="249"/>
        <location filename="../src/mainwindow.cpp" line="24"/>
        <location filename="../src/mainwindow.cpp" line="905"/>
        <source>Detect Network Faults</source>
        <translation>དྲ་རྒྱའི་ཆག་སྐྱོན་མཐེབ་གཞོང་གཅིག་གིས་ཞིབ་བཤེར་བྱས།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="290"/>
        <location filename="../src/mainwindow.cpp" line="27"/>
        <location filename="../src/mainwindow.cpp" line="904"/>
        <source>Detect and resolve Network Faults</source>
        <translation>ཕྱོགས་ཡོངས་ནས་དྲ་རྒྱའི་ཆག་སྐྱོན་ལ་བརྟག་དཔྱད་བྱས་ནས་དྲ་ཞུགས་ཀྱི་གནད་དོན་ཐག་གཅོད་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="358"/>
        <source>PushButton</source>
        <translation>མཐེབ་གནོན་</translation>
    </message>
    <message>
        <source>Start Check</source>
        <translation type="vanished">ཞིབ་བཤེར་བྱེད་འགོ་ཚུགས།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="426"/>
        <location filename="../src/mainwindow.cpp" line="38"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="451"/>
        <location filename="../src/mainwindow.cpp" line="41"/>
        <source>Restart</source>
        <translation>ཡང་བསྐྱར་ཞིབ་བཤེར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Check Again</source>
        <translation type="vanished">ཡང་བསྐྱར་ཞིབ་བཤེར་ཐེངས་</translation>
    </message>
    <message>
        <source>NetWork Check Tools</source>
        <translation type="vanished">དྲ་རྒྱའི་ལས་དོན་ཞིབ་བཤེར་ཡོ་བྱད།</translation>
    </message>
    <message>
        <source>检测中...</source>
        <translation type="vanished">检测中...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="472"/>
        <source>Checking...</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>No Error</source>
        <translation type="vanished">没有问题</translation>
    </message>
    <message>
        <source>开始检测</source>
        <translation type="vanished">开始检测</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="496"/>
        <location filename="../src/mainwindow.cpp" line="524"/>
        <location filename="../src/mainwindow.cpp" line="608"/>
        <source>Check Result</source>
        <translation>ཞིབ་བཤེར་བྱས་འབྲས།</translation>
    </message>
    <message>
        <source>已检测 %1 项，未发现问题</source>
        <translation type="vanished">已检测 %1 项，未发现问题</translation>
    </message>
    <message>
        <source>发现 %1 项问题，%2 项提示问题，请修复后重新检测</source>
        <translation type="vanished">发现 %1 项问题，%2 项提示问题，请修复后重新检测</translation>
    </message>
    <message>
        <source>扫描中断，已检测 %1 项，未发现问题</source>
        <translation type="vanished">扫描中断，已检测 %1 项，未发现问题</translation>
    </message>
    <message>
        <source>扫描中断，发现 %1 项问题，请修复后重新检测</source>
        <translation type="vanished">扫描中断，发现 %1 项问题，请修复后重新检测</translation>
    </message>
    <message>
        <source>已检测 %1 项，发现 %2 项问题</source>
        <translation type="vanished">已检测 %1 项，发现 %2 项问题</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="510"/>
        <source>Stopping, please wait!</source>
        <translation>མཚམས་བཞག་ནས་ཅུང་ཙམ་སྒུགས་དང་།</translation>
    </message>
    <message>
        <source>You can check again now!</source>
        <translation type="vanished">您现在可以重新检测了！</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="401"/>
        <location filename="../src/mainwindow.cpp" line="473"/>
        <source>Start</source>
        <translation>ཞིབ་བཤེར་འགོ་རྩོམ་པ།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="370"/>
        <source>Find %1 issues，%2 is error，repair and start</source>
        <translation>%1གནམ་དོན་འཚོལ་བ་དང་། %2ནི་ནོར་འཁྲུལ་ཡིན་པ། ཞིག་གསོ་བྱེད་པ། འགོ་རྩོམ་པ་བཅས་ཡིན།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="398"/>
        <source>Pause，checked %1 item，no issue</source>
        <translation>མཚམས་བཞག་ནས་ཞིབ་བཤེར་བྱས་པའི་%1རིམ་གྲངས་ལ་ཞིབ་བཤེར་བྱས་ཀྱང་གནད་དོན་མ་རྙེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="402"/>
        <source>Pause，find %1 issues，repair and start</source>
        <translation>མཚམས་བཞག་ནས་%1གནད་དོན་བཙལ་ནས་ཞིག་གསོ་བྱེད་འགོ་ཚུགས།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="446"/>
        <source>finished，no issue</source>
        <translation>བསྒྲུབས་ཚར་རྗེས་གནད་དོན་གང་ཡང་མེད།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="451"/>
        <source>finished，have issue</source>
        <translation>བསྒྲུབས་ཚར་རྗེས་གནད་དོན་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="55"/>
        <source>Return</source>
        <translation>ཕྱིར་སློག་པ།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="106"/>
        <source>NetCheck</source>
        <translation>དྲ་རྒྱའི་ཞིབ་བཤེར།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="112"/>
        <source>total 7 items</source>
        <translation>ཁྱོན་བསྡོམས་རྣམ་གྲངས་7ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="622"/>
        <source>find %1 errs,%2 issues,repair and start</source>
        <translation>%1་གནད་དོན་དང་%2 ་ཀྱི་གསལ་འདེབས་གནད་དོན་ཤེས་སོང་།་ཞིག་གསོ་བྱས་རྗེས་ཡང་བསྐྱར་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="823"/>
        <source>InnerNet Check</source>
        <translation>ནང་ལོགས་ཀྱི་དྲ་རྒྱའི་ཞིབ་བཤེར།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="824"/>
        <source>Can user browse inner net?</source>
        <translation>ཁྱེད་ཀྱི་གློག་ཀླད་རྒྱུན་ལྡན་དང་བྱང་ཆ་ལྡན་པའི་སྒོ་ནས་ནང་གི་དྲ་བར་བཅར་འདྲི་བྱེད་ཐུབ་མིན་ལ་བརྟག་དཔྱད་བྱོས།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="827"/>
        <source>OutNet Check</source>
        <translation>ཕྱི་རོལ་གྱི་དྲ་རྒྱའི་ཞིབ་བཤེར།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="828"/>
        <source>Can user browse out net?</source>
        <translation>ཁྱེད་ཀྱི་གློག་ཀླད་རྒྱུན་ལྡན་དང་བྱང་ཆ་ལྡན་པའི་སྒོ་ནས་ཕྱི་དྲ་ལ་བཅར་འདྲི་བྱེད་ཐུབ་མིན་ལ་བརྟག་དཔྱད་བྱོས།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="629"/>
        <source>checked %1 items，no issue</source>
        <translation>%1ལ་ཞིབ་བཤེར་བྱས་ཟིན།་གནད་དོན་མ་རྙེད།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="631"/>
        <source>checked %1 items，find %2 issues</source>
        <translation>%1 རྣམ་གྲངས་ལ་ཞིབ་བཤེར་བྱས་ནས་%2གནམ་དོན་རྙེད་པ།</translation>
    </message>
    <message>
        <source>已检测 %1 项</source>
        <translation type="vanished">已检测 %1 项</translation>
    </message>
</context>
<context>
    <name>MenuModule</name>
    <message>
        <location filename="../src/menumodule.cpp" line="67"/>
        <location filename="../src/menumodule.cpp" line="108"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="69"/>
        <location filename="../src/menumodule.cpp" line="105"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="71"/>
        <location filename="../src/menumodule.cpp" line="112"/>
        <source>Configure</source>
        <translation>བཀོད་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="73"/>
        <location filename="../src/menumodule.cpp" line="101"/>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན་བྱ་རྒྱུ།</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="141"/>
        <source>Network-check-tool is a software that can quickly detect,diagnose,and optimize networks.</source>
        <translation>དྲ་རྒྱའི་ཞིབ་བཤེར་ཡོ་བྱད་ནི་མགྱོགས་མྱུར་ངང་དྲ་རྒྱར་ཞིབ་དཔྱད་ཚད་ལེན་དང་། ནད་གཞི་བརྟག་དཔྱད། ལེགས་སྒྱུར་བཅས་བྱེད་ཐུབ་པའི་མཉེན་ཆས་ཤིག་ཡིན།</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="140"/>
        <location filename="../src/menumodule.cpp" line="262"/>
        <source>Version: </source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ། </translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="266"/>
        <source>Network-check-tool is a software that can quickly detect, diagnose, and optimize networks. </source>
        <translation>དྲ་རྒྱའི་ཞིབ་བཤེར་ཡོ་བྱད་ནི་མགྱོགས་མྱུར་ངང་དྲ་རྒྱར་ཞིབ་དཔྱད་ཚད་ལེན་དང་། ནད་གཞི་བརྟག་དཔྱད། ལེགས་སྒྱུར་བཅས་བྱེད་ཐུབ་པའི་མཉེན་ཆས་ཤིག་ཡིན། </translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="278"/>
        <location filename="../src/menumodule.cpp" line="361"/>
        <location filename="../src/menumodule.cpp" line="369"/>
        <source>Service &amp; Support: </source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར། </translation>
    </message>
    <message>
        <location filename="../include/menumodule.h" line="61"/>
        <location filename="../include/menumodule.h" line="62"/>
        <location filename="../include/menumodule.h" line="64"/>
        <source>NetWork Check Tools</source>
        <translation>དྲ་རྒྱའི་ལས་དོན་ཞིབ་བཤེར་ཡོ་བྱད།</translation>
    </message>
</context>
<context>
    <name>NetCheck</name>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="25"/>
        <location filename="../../NetCheck/netcheck.cpp" line="26"/>
        <location filename="../../NetCheck/netcheck.cpp" line="257"/>
        <location filename="../../NetCheck/netcheck.cpp" line="258"/>
        <source>InnerNet Check</source>
        <translation>ནང་ལོགས་ཀྱི་དྲ་རྒྱའི་ཞིབ་བཤེར།</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="27"/>
        <source>Can user browse inner net?</source>
        <translation>སྤྱོད་མཁན་གྱིས་ནང་ཁུལ་གྱི་དྲ་རྒྱ་ལ་ལྟ་ཞིབ་བྱེད་ཐུབ་བམ།</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="31"/>
        <location filename="../../NetCheck/netcheck.cpp" line="32"/>
        <location filename="../../NetCheck/netcheck.cpp" line="260"/>
        <location filename="../../NetCheck/netcheck.cpp" line="261"/>
        <source>OutNet Check</source>
        <translation>ཕྱི་རོལ་གྱི་དྲ་རྒྱའི་ཞིབ་བཤེར།</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="33"/>
        <source>Can user browse out net?</source>
        <translation>ཁྱེད་ཀྱི་གློག་ཀླད་རྒྱུན་ལྡན་དང་བྱང་ཆ་ལྡན་པའི་སྒོ་ནས་ཕྱི་དྲ་ལ་བཅར་འདྲི་བྱེད་ཐུབ་མིན་ལ་བརྟག་དཔྱད་བྱོས།</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="295"/>
        <location filename="../../NetCheck/netcheck.cpp" line="298"/>
        <location filename="../../NetCheck/netcheck.cpp" line="307"/>
        <location filename="../../NetCheck/netcheck.cpp" line="312"/>
        <location filename="../../NetCheck/netcheck.cpp" line="315"/>
        <location filename="../../NetCheck/netcheck.cpp" line="318"/>
        <location filename="../../NetCheck/netcheck.cpp" line="328"/>
        <location filename="../../NetCheck/netcheck.cpp" line="336"/>
        <source>OK</source>
        <translation>རྒྱུན་ལྡན།</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="301"/>
        <source>Extranet abnormal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="343"/>
        <source>IP is unreachable，url cannot be accessed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="307"/>
        <location filename="../../NetCheck/netcheck.cpp" line="312"/>
        <source>Intranet normal</source>
        <translation>ནང་དྲ་ཞིབ་དཔྱད་ཚད་ལེན་ཤར་གཏོང་།</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="287"/>
        <source>IP is reachable，url cannot be accessed</source>
        <translation>ནང་གི་དྲ་བའི་ IPས་གནས་ལ་ཐོན་ཐུབ།་ནང་གི་དྲ་བས་གཏན་འཁེལ་བྱས་པའི་ས་ཚིགས་ལ་རྒྱུན་ལྡན་གྱི་བཅར་འདྲི་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="292"/>
        <source>IP is unreachable，url can be accessed</source>
        <translation>ནང་དྲ IP ་ས་གནས་ལ་ཐོན་མི་ཐུབ།་ནང་དྲ་ཡིས་གཏན་འཁེལ་བྱས་པའི་ས་ཚིགས་རྒྱུན་ལྡན་ལྟར་བཅར་འདྲི་བྱེད་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="297"/>
        <location filename="../../NetCheck/netcheck.cpp" line="303"/>
        <source>IP is unreachable，url cannot be accessed</source>
        <translation>IPནི་འབྲེལ་གཏུག་བྱེད་ཐབས་བྲལ་བ་དང་། དྲ་ཚིགས་ལ་རྒྱུན་ལྡན་ལྟར་ལྟ་ཞིབ་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="308"/>
        <source>Extranet normal</source>
        <translation>ཕྱི་དྲ་ཞིབ་དཔྱད་ཤར་གཏོང་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="313"/>
        <source>Extranet accessed</source>
        <translation>དྲ་སྦྲེལ་ལ་ལྟ་ཞིབ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="318"/>
        <source>Extranet abnormal</source>
        <translation>ཕྱི་དྲ་ཞིབ་དཔྱད་ཚད་ལེན་ཕམ་སོང་།</translation>
    </message>
    <message>
        <source>内网检测通畅</source>
        <translation type="vanished">内网检测通畅</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="301"/>
        <location filename="../../NetCheck/netcheck.cpp" line="325"/>
        <location filename="../../NetCheck/netcheck.cpp" line="333"/>
        <location filename="../../NetCheck/netcheck.cpp" line="343"/>
        <source>ERR</source>
        <translation>རྒྱུན་ལྡན་མིན་པ།</translation>
    </message>
    <message>
        <source>外网检测通畅</source>
        <translation type="vanished">外网检测通畅</translation>
    </message>
    <message>
        <source>外网可连通</source>
        <translation type="vanished">外网可连通</translation>
    </message>
    <message>
        <source>not perfect connected!</source>
        <translation type="vanished">可以连接，但存在问题！</translation>
    </message>
    <message>
        <source>can not connected!</source>
        <translation type="vanished">无法联网！</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="254"/>
        <source>Checking NetWorks</source>
        <translation>ཐོན་ཐུབ་མིན་ལ་ IPབརྟག་དཔྱད་བྱེད་པ།དྲ་ཚིགས་ལ་བཅར་འདྲི་བྱེད་ཐུབ་བམ།</translation>
    </message>
    <message>
        <location filename="../../NetCheck/netcheck.cpp" line="254"/>
        <source>Checking</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>NetCheckHomePage</name>
    <message>
        <location filename="../src/netcheckhomepage.cpp" line="10"/>
        <source>Check and Repair</source>
        <translation>གློག་ཀླད་ཀྱི་ཆག་སྐྱོན་མཐེབ་གཞོང་གཅིག་གིས་ཞིབ་བཤེར་བྱས།</translation>
    </message>
    <message>
        <location filename="../src/netcheckhomepage.cpp" line="17"/>
        <source>Detection and repair of computer problems！</source>
        <translation>མྱུར་དུ་ཆག་སྐྱོན་གྱི་གནད་དོན་ཞིག་གསོ་བྱས་ནས་ཁྱོད་ཀྱི་གློག་ཀླད་བདེ་ཐང་ལ་སྲུང་སྐྱོབ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/netcheckhomepage.cpp" line="30"/>
        <source>NetCheck</source>
        <translation>དྲ་རྒྱའི་ཞིབ་བཤེར།</translation>
    </message>
    <message>
        <location filename="../src/netcheckhomepage.cpp" line="46"/>
        <source>Start</source>
        <translation>ཞིབ་བཤེར་འགོ་རྩོམ་པ།</translation>
    </message>
    <message>
        <location filename="../src/netcheckhomepage.cpp" line="76"/>
        <source>IntraNetSet</source>
        <translation>ནང་ཁུལ་གྱི་དྲ་རྒྱའི་ནང་དུ་ཞིབ་བཤེར་བཀོལ་སྒྲིག</translation>
    </message>
</context>
<context>
    <name>ProxyCheck</name>
    <message>
        <location filename="../../ProxyCheck/proxycheck.cpp" line="19"/>
        <source>NetWork Proxy</source>
        <translation>བཤར་ཆས་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <source>Checking NetWork HardWares</source>
        <translation type="vanished">检测系统代理配置问题</translation>
    </message>
    <message>
        <location filename="../../ProxyCheck/proxycheck.cpp" line="47"/>
        <source>Checking</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../ProxyCheck/proxycheck.cpp" line="21"/>
        <source>Proxy</source>
        <translation>བཤར་ཆས་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../../ProxyCheck/proxycheck.cpp" line="22"/>
        <location filename="../../ProxyCheck/proxycheck.cpp" line="47"/>
        <source>Check whether the proxy is working?</source>
        <translation>ཞིབ་དཔྱད་མ་ལག་གི་ལས་ཚབ་བཀོད་སྒྲིག་གི་གནད་དོན།</translation>
    </message>
    <message>
        <location filename="../../ProxyCheck/proxycheck.cpp" line="52"/>
        <source>proxy disable</source>
        <translation>ལས་ཚབ་ཁ་ཕྱེ་མེད།</translation>
    </message>
    <message>
        <location filename="../../ProxyCheck/proxycheck.cpp" line="57"/>
        <source>auto proxy normal</source>
        <translation>རང་འགུལ་གྱིས་ཚབ་བྱེད་རྒྱུན་ལྡན།</translation>
    </message>
    <message>
        <location filename="../../ProxyCheck/proxycheck.cpp" line="62"/>
        <source>auto proxy abnormal</source>
        <translation>རང་འགུལ་ངོ་ཚབ་རྒྱུན་ལྡན་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../../ProxyCheck/proxycheck.cpp" line="66"/>
        <source>manual proxy normal</source>
        <translation>ལག་སྒུལ་ལས་ཚབ་རྒྱུན་ལྡན་རེད།</translation>
    </message>
</context>
<context>
    <name>QuadBtnsTitleBar</name>
    <message>
        <location filename="../src/quad_btns_title_bar.cpp" line="16"/>
        <source>menu</source>
        <translation>འདེམས་པང་།</translation>
    </message>
    <message>
        <location filename="../src/quad_btns_title_bar.cpp" line="25"/>
        <source>minimize</source>
        <translation>ཆེས་ཆུང་དུ་བཏང་བ།</translation>
    </message>
    <message>
        <location filename="../src/quad_btns_title_bar.cpp" line="39"/>
        <source>full screen</source>
        <translation>ཆེས་ཆེར་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../src/quad_btns_title_bar.cpp" line="53"/>
        <source>close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
</TS>
