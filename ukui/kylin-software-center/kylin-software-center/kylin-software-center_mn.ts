<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AdImageWidget</name>
    <message>
        <source>Download</source>
        <translation type="vanished">下载</translation>
    </message>
    <message>
        <source>After installation, you can experience the Android environment after restarting</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠳᠠᠭᠤᠰᠪᠠ᠂ ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ Android ᠤᠷᠴᠢᠨ ᠢ᠋ ᠪᠡᠶᠡᠴᠢᠯᠡᠨ ᠳᠤᠷᠠᠰᠢᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠤᠭᠰᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>AppCardWidget</name>
    <message>
        <source>install</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <source>New features</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠴᠢᠳᠠᠪᠬᠢ</translation>
    </message>
    <message>
        <source>Uninstall</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠤᠭᠰᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>AppDetailWidget</name>
    <message>
        <source>Platform introduction</source>
        <translation>ᠳᠠᠪᠴᠠᠩ ᠤ᠋ᠨ ᠳᠠᠨᠢᠯᠴᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>Technical Features</source>
        <translation>ᠮᠡᠷᠬᠡᠵᠢᠯ ᠤ᠋ᠨ ᠤᠨᠴᠠᠯᠢᠭ</translation>
    </message>
    <message>
        <source>Number of Downloads</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ ᠲᠤᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>times</source>
        <translation>ᠤᠳᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source> scores</source>
        <translation> ᠤᠳᠠᠭᠠᠨ ᠤ᠋ ᠬᠤᠪᠢ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Full Score 5 points</source>
        <translation>ᠪᠦᠳᠦᠨ ᠬᠤᠪᠢ ᠨᠢ5 ᠬᠤᠪᠢ</translation>
    </message>
    <message>
        <source>size</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>version</source>
        <translation>ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <source>update</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠤᠭᠰᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Brief Introduction</source>
        <translation>ᠳᠠᠨᠢᠯᠴᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>ᠠᠯᠪᠠᠨ ᠤ᠋ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <source>Update Content</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>Other information</source>
        <translation>ᠪᠤᠰᠤᠳ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <source>Package name</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <source>Payment type</source>
        <translation>ᠳᠦᠯᠦᠪᠦᠷᠢ ᠳᠦᠯᠦᠬᠦ ᠳᠦᠷᠦᠯ</translation>
    </message>
    <message>
        <source>Safety inspection</source>
        <translation>ᠠᠮᠤᠷ ᠳᠦᠪᠰᠢᠨ ᠢ᠋ ᠬᠢᠨᠠᠨ ᠰᠢᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Developer</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠨ ᠬᠦᠭᠵᠢᠬᠦᠯᠦᠭᠴᠢ</translation>
    </message>
    <message>
        <source>Additional terms</source>
        <translation>ᠨᠡᠮᠡᠯᠳᠡ ᠵᠤᠷᠪᠤᠰ</translation>
    </message>
    <message>
        <source>In software charge</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠳᠤᠳᠤᠭᠠᠳᠤ ᠳ᠋ᠤ᠌ ᠳᠦᠯᠦᠪᠦᠷᠢ ᠳᠦᠯᠦᠬᠦ</translation>
    </message>
    <message>
        <source>free</source>
        <translation>ᠳᠦᠯᠦᠪᠦᠷᠢ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Software signature</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠭᠠᠷ ᠤ᠋ᠨ ᠦᠰᠦᠭ</translation>
    </message>
    <message>
        <source>Signature unit:</source>
        <translation>ᠭᠠᠷ ᠦᠰᠦᠭ ᠵᠢᠷᠤᠭᠰᠠᠨ ᠨᠢᠭᠡᠴᠢ:</translation>
    </message>
    <message>
        <source>Manual recheck</source>
        <translation>ᠬᠦᠮᠦᠨ ᠤ᠋ ᠬᠦᠴᠦ ᠪᠡᠷ ᠳᠠᠬᠢᠨ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Intellectual property</source>
        <translation>ᠮᠡᠳᠡᠯᠭᠡ ᠵᠢᠨ ᠦᠮᠴᠢ ᠵᠢᠨ ᠡᠷᠬᠡ ᠵᠢᠨ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠯ</translation>
    </message>
    <message>
        <source>Similar Software Recommendation</source>
        <translation>ᠠᠳᠠᠯᠢ ᠳᠦᠷᠦᠯ ᠤ᠋ᠨ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠳᠠᠨᠢᠯᠴᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Score</source>
        <translation>ᠬᠤᠪᠢ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>User comments</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠬᠤᠪᠢ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>comments</source>
        <translation>ᠵᠤᠷᠪᠤᠰ</translation>
    </message>
    <message>
        <source>Your feedback is the driving force for us to do a good job in domestic system software ecology!</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯ ᠪᠤᠯ ᠪᠢᠳᠡᠨ ᠤ᠋ ᠤᠯᠤᠰ ᠡᠴᠡ ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭᠰᠡᠨ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠠᠮᠢ ᠠᠬᠤᠢ ᠵᠢᠨ ᠤᠷᠴᠢᠨ ᠢ᠋ ᠰᠠᠢᠨ ᠪᠠᠢᠯᠭᠠᠬᠤ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠬᠦᠴᠦᠨ!</translation>
    </message>
    <message>
        <source>Enter up to 200 characters</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠤᠯᠠᠨ ᠳ᠋ᠤ᠌ ᠪᠡᠨ 200 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>submit</source>
        <translation>ᠳᠤᠰᠢᠶᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Please rate before commenting!</source>
        <translation>ᠰᠡᠳᠬᠢᠭᠳᠡᠯ ᠦᠯᠡᠳᠡᠬᠡᠬᠦ ᠡᠴᠡ ᠪᠡᠨ ᠡᠮᠦᠨ᠎ᠡ ᠤᠷᠢᠳᠠᠪᠠᠷ ᠬᠤᠪᠢ ᠳᠤᠭᠳᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Please install the software before commenting!</source>
        <translation>ᠰᠡᠳᠬᠢᠭᠳᠡᠯ ᠦᠯᠡᠳᠡᠬᠡᠬᠦ ᠡᠴᠡ ᠪᠡᠨ ᠡᠮᠦᠨ᠎ᠡ ᠤᠷᠢᠳᠠᠪᠠᠷ ᠬᠡᠷᠡᠭᠯᠡᠬᠡ ᠵᠢ ᠤᠭᠰᠠᠷᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <source>You have read all the comments</source>
        <translation>ᠲᠠ ᠨᠢᠭᠡᠨᠳᠡ ᠪᠦᠬᠦ ᠰᠡᠳᠬᠢᠭᠳᠡᠯ ᠢ᠋ ᠦᠵᠡᠵᠤ ᠳᠠᠭᠤᠰᠤᠭᠰᠠᠨ</translation>
    </message>
    <message>
        <source> comments</source>
        <translation> ᠵᠤᠷᠪᠤᠰ</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>W+</source>
        <translation>ᠳᠦᠮᠡ+</translation>
    </message>
    <message>
        <source>MB</source>
        <translation></translation>
    </message>
    <message>
        <source>KB</source>
        <translation></translation>
    </message>
    <message>
        <source>B</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AppendCommentPopupWindow</name>
    <message>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Additional comments</source>
        <translation>ᠰᠡᠳᠬᠢᠭᠳᠡᠯ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Your feedback is the driving force for us to do a good job in domestic system software ecology!</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯ ᠪᠤᠯ ᠪᠢᠳᠡᠨ ᠤ᠋ ᠤᠯᠤᠰ ᠡᠴᠡ ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭᠰᠡᠨ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠠᠮᠢ ᠠᠬᠤᠢ ᠵᠢᠨ ᠤᠷᠴᠢᠨ ᠢ᠋ ᠰᠠᠢᠨ ᠪᠠᠢᠯᠭᠠᠬᠤ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠬᠦᠴᠦᠨ!</translation>
    </message>
    <message>
        <source>Enter up to 200 characters</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠤᠯᠠᠨ ᠳ᠋ᠤ᠌ ᠪᠡᠨ 200 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>submit</source>
        <translation>ᠳᠤᠰᠢᠶᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>CategoryShowWidget</name>
    <message>
        <source>Default ranking</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <source>Download ranking</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Sort ranking</source>
        <translation>ᠬᠤᠪᠢ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <source>UpdateTime sorting</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>Comment ranking</source>
        <translation>ᠰᠡᠳᠬᠢᠭᠳᠡᠯ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ</translation>
    </message>
</context>
<context>
    <name>CategoryWidget</name>
    <message>
        <source>Default ranking</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <source>Download ranking</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Sort ranking</source>
        <translation>ᠬᠤᠪᠢ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <source>Comment ranking</source>
        <translation>ᠰᠡᠳᠬᠢᠭᠳᠡᠯ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <source>UpdateTime sorting</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>The environment is initialized, the interface is temporarily locked, please wait...</source>
        <translation>ᠤᠷᠴᠢᠨ ᠳᠤᠭᠤᠷᠢᠨ ᠢ᠋ ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠪᠠ᠂ ᠵᠠᠭᠠᠭ ᠭᠠᠳᠠᠷᠭᠤ ᠵᠢ ᠳᠦᠷ ᠤᠨᠢᠰᠤᠯᠠᠪᠠ᠂ ᠲᠦᠷ ᠬᠦᠯᠢᠶᠡᠬᠡᠷᠡᠢ...</translation>
    </message>
    <message>
        <source>retry</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠠᠰᠢᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Comment</name>
    <message>
        <source>just</source>
        <translation>ᠰᠠᠶᠢᠬᠠᠨ</translation>
    </message>
    <message>
        <source>delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Additional comments</source>
        <translation>ᠰᠡᠳᠬᠢᠭᠳᠡᠯ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>CommentWidget</name>
    <message>
        <source>just</source>
        <translation type="vanished">刚刚</translation>
    </message>
    <message>
        <source>delete</source>
        <translation type="vanished">删除</translation>
    </message>
</context>
<context>
    <name>DownloadRankWidget</name>
    <message>
        <source>Download Ranking</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>View all</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>DownloadRankingItem</name>
    <message>
        <source>Uninstall</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠤᠭᠰᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>Downloading</name>
    <message>
        <source>Downloading</source>
        <translation>ᠶᠠᠭ ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>all Pause</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠳᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <source>Open download directory</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Empty downloaded</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠪᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠤᠭᠰᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <source>No download task</source>
        <translation>ᠳᠦᠷ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠡᠬᠦᠷᠭᠡ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>ImproveWorkEfficiencyTemplate</name>
    <message>
        <source>Selected Applications</source>
        <translation>ᠨᠠᠷᠢᠯᠢᠭ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠡ</translation>
    </message>
    <message>
        <source>View all</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>ImproveWorkEfficiencyTemplateCard</name>
    <message>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠤᠭᠰᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>KYComboBox</name>
    <message>
        <source>Your Email/Name/Phone</source>
        <translation>ᠢᠮᠸᠯ/ ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ/ ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ</translation>
    </message>
</context>
<context>
    <name>LandingWidget</name>
    <message>
        <source>No download task</source>
        <translation type="vanished">没有找到你要的软件</translation>
    </message>
    <message>
        <source>No software found</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠠᠪᠬᠤ ᠬᠡᠰᠡᠨᠡᠭ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠢ᠋ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>go to</source>
        <translation>ᠤᠴᠢᠬᠤ</translation>
    </message>
    <message>
        <source> Full library </source>
        <translation> ᠪᠦᠬᠦᠯᠢ ᠬᠦᠮᠦᠷᠭᠡ </translation>
    </message>
    <message>
        <source>search and try</source>
        <translation>ᠬᠠᠢᠵᠤ ᠳᠤᠷᠰᠢᠬᠤ</translation>
    </message>
</context>
<context>
    <name>MainDialog</name>
    <message>
        <source>Kylin ID</source>
        <translation>ᠴᠢ ᠯᠢᠨ ID</translation>
    </message>
    <message>
        <source>Forget?</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠵᠢᠡᠨ ᠮᠠᠷᠳᠠᠭᠰᠠᠨ ᠤᠤ?</translation>
    </message>
    <message>
        <source>Remember it</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠵᠢᠨᠨ ᠴᠡᠬᠡᠵᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Register</source>
        <translation>ᠪᠦᠷᠢᠳᠬᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>Your password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠵᠢᠨᠨ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Your code</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Your phone number here</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠵᠢᠨᠨ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Pass login</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠵᠢᠡᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Phone login</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤ ᠪᠡᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>ᠢᠯᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Please move slider to right place</source>
        <translation>ᠭᠤᠯᠭᠤᠬᠤ ᠬᠡᠰᠡᠭ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠮᠵᠢᠳᠠᠢ ᠪᠠᠢᠷᠢᠨ ᠳ᠋ᠤ᠌ ᠭᠤᠯᠭᠤᠭᠤᠯᠵᠤ ᠠᠪᠠᠴᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>%1s left</source>
        <translation>%1 ᠰᠸᠺᠦᠨ᠋ᠲ ᠦᠯᠡᠳᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>User stop verify Captcha</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠵᠢ ᠵᠤᠭᠰᠤᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <source>No response data!</source>
        <translation>ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ ᠳ᠋ᠠᠢᠲ᠋ᠠ!</translation>
    </message>
    <message>
        <source>Timeout!</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠴᠠᠭ ᠬᠡᠳᠦᠷᠡᠪᠡ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <source>Wrong account or password!</source>
        <translation>ᠳᠠᠩᠰᠠ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠷᠤᠭᠤ!</translation>
    </message>
    <message>
        <source>No network!</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <source>Pictrure has expired!</source>
        <translation>ᠵᠢᠷᠤᠭ ᠨᠢᠭᠡᠨᠳᠡ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠨᠢ ᠦᠩᠬᠡᠷᠡᠪᠡ!</translation>
    </message>
    <message>
        <source>User deleted!</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠳᠠᠩᠰᠠ ᠪᠡᠨ ᠬᠠᠰᠤᠪᠠ!</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠢ᠋ ᠨᠢᠭᠡᠨᠳᠡ ᠬᠡᠷᠡᠭᠯᠡᠪᠡ!</translation>
    </message>
    <message>
        <source>Wrong phone number format!</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ ᠪᠤᠷᠤᠭᠤ!</translation>
    </message>
    <message>
        <source>Your are reach the limit!</source>
        <translation>ᠳᠤᠰ ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠳᠤᠰ ᠡᠳᠦᠷ ᠲᠤ᠌ ᠬᠤᠷᠢᠶᠠᠵᠤ ᠠᠪᠬᠤ ᠤᠬᠤᠷᠢ ᠮᠡᠳᠡᠬᠡᠨ ᠤ᠋ ᠲᠤᠭ᠎ᠠ ᠨᠢᠭᠡᠨᠳᠡ ᠬᠦᠢᠴᠡᠪᠡ!</translation>
    </message>
    <message>
        <source>Please check your phone number!</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠵᠢᠨᠨ ᠪᠠᠢᠴᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <source>Please check your code!</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠪᠤᠰᠤᠳ ᠪᠤᠷᠤᠭᠤ!</translation>
    </message>
    <message>
        <source>Send sms Limited!</source>
        <translation>ᠢᠯᠡᠬᠡᠬᠦ ᠤᠬᠤᠷᠢ ᠮᠡᠳᠡᠭᠡᠨ ᠤ᠋ ᠲᠤᠭ᠎ᠠ ᠬᠦᠢᠴᠡᠪᠡ!</translation>
    </message>
    <message>
        <source>Pictrure blocked!</source>
        <translation>ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ ᠡᠪᠳᠡᠷᠡᠪᠡ!</translation>
    </message>
    <message>
        <source>Illegal code!</source>
        <translation>ᠬᠠᠤᠯᠢ ᠪᠤᠰᠤ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ!</translation>
    </message>
    <message>
        <source>Phone code is expired!</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠪᠠ!</translation>
    </message>
    <message>
        <source>Failed attemps limit reached!</source>
        <translation>ᠳᠤᠷᠰᠢᠭᠰᠠᠨ ᠤᠳᠠᠭ᠎ᠠ ᠬᠡᠳᠦ ᠤᠯᠠᠨ ᠪᠤᠯᠪᠠ!</translation>
    </message>
    <message>
        <source>Please wait</source>
        <translation>ᠶᠠᠭ ᠨᠡᠪᠳᠡᠷᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Parsing data failed!</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠵᠠᠳᠠᠯᠤᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ!</translation>
    </message>
    <message>
        <source>Server internal error!</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠳᠤᠳᠤᠭᠠᠳᠤ ᠵᠢᠨ ᠪᠤᠷᠤᠭᠤ!</translation>
    </message>
    <message>
        <source>Slider validate error</source>
        <translation>ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠵᠢᠡᠷ ᠪᠠᠳᠤᠯᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Phone code error!</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠪᠤᠷᠤᠭᠤ!</translation>
    </message>
    <message>
        <source>Code can not be empty!</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>MCode can not be empty!</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>Unsupported operation!</source>
        <translation>ᠳᠤᠰ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠳᠦᠷ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>Unsupported Client Type!</source>
        <translation>ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠵᠦᠬᠦᠷ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ!</translation>
    </message>
    <message>
        <source>Please check your input!</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠵᠢᠨᠨ ᠪᠠᠢᠴᠠᠭᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <source>Process failed</source>
        <translation>ᠢᠯᠡᠬᠡᠭᠰᠡᠨ ᠭᠤᠶᠤᠴᠢᠯᠠᠯ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>software store</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <source>homePage</source>
        <translation>ᠭᠤᠤᠯ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <source>Office</source>
        <translation type="vanished">办公</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="obsolete">图像</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="vanished">影音</translation>
    </message>
    <message>
        <source>Drive</source>
        <translation>ᠬᠦᠯᠳᠡᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <source>Mobile apps</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠬᠡᠷᠡᠭᠯᠡᠬᠡ</translation>
    </message>
    <message>
        <source>All classification</source>
        <translation>ᠪᠦᠬᠦ ᠠᠩᠬᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <source>Software management</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠬᠠᠮᠢᠶᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <source>Not logged in!</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠭᠰᠡᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>Log in now</source>
        <translation>ᠳᠠᠷᠤᠢ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <source>User settings</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>Log out</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠡᠴᠡ ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <source>Sign in</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <source>NetWork error</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <source>Please check the network or try again later</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠪᠡᠨ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ ᠪᠤᠶᠤ ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠬᠤ</translation>
    </message>
    <message>
        <source>Comment failed</source>
        <translation>ᠰᠡᠳᠬᠢᠭᠳᠡᠯ ᠦᠯᠡᠳᠡᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Contains sensitive words</source>
        <translation>ᠡᠮᠵᠡᠭ ᠦᠭᠡ ᠤᠷᠤᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>ok</source>
        <translation>ok</translation>
    </message>
    <message>
        <source>Failed to get mobile app list.</source>
        <translation type="vanished">获取移动应用列表失败</translation>
    </message>
    <message>
        <source>The obtained application list is empty.</source>
        <translation type="vanished">获取的应用程序列表为空</translation>
    </message>
    <message>
        <source>Get mobile app list...</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠤ᠋ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠵᠢ ᠤᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <source>Failed to get mobile app list, please try again later.</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠤ᠋ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠵᠢ ᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠤᠭᠰᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>It is not suitable for the hardware combination of this machine. Please look forward to it.</source>
        <translation>ᠳᠤᠰ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠬᠠᠳᠠᠭᠤᠨ ᠳᠤᠨᠤᠭ ᠤ᠋ᠨ ᠬᠠᠮᠰᠠᠯ ᠳ᠋ᠤ᠌ ᠳᠦᠷ ᠳᠤᠬᠢᠷᠠᠬᠤ ᠦᠬᠡᠢ᠂ ᠬᠦᠯᠢᠶᠡᠵᠤ ᠪᠠᠢᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <source>The obtained mobile app list is empty.</source>
        <translation>ᠤᠯᠵᠠᠯᠠᠭᠰᠠᠨ ᠰᠢᠯᠵᠢᠮᠡᠯ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠤ᠋ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠬᠤᠭᠤᠰᠤᠨ.</translation>
    </message>
    <message>
        <source>Please install mobile operating environment!</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠠᠵᠢᠯᠯᠠᠭᠠᠨ ᠤ᠋ ᠤᠷᠴᠢᠨ ᠢ᠋ ᠤᠭᠰᠠᠷᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <source>Select all</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <source>709 graphics card optimization, please look forward to!</source>
        <translation>709 ᠢᠯᠡ ᠺᠠᠷᠲ ᠢ᠋ ᠰᠢᠯᠢᠭᠳᠡᠵᠢᠬᠦᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠬᠦᠯᠢᠶᠡᠵᠤ ᠪᠠᠢᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <source>Frequent operation</source>
        <translation>ᠪᠠᠢᠨ᠎ᠠ ᠳᠠᠬᠢᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ</translation>
    </message>
    <message>
        <source>Please try again later</source>
        <translation>ᠤᠳᠠᠰᠭᠢᠭᠠᠳ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠱᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Multiple likes or cancellations are not allowed</source>
        <translation>ᠤᠯᠠᠨ ᠤᠳᠠᠭ᠎ᠠ ᠲᠤᠭᠤᠷ ᠦᠭᠬᠦ ᠪᠤᠶᠤ ᠦᠭᠬᠦᠭᠰᠡᠨ ᠲᠤᠭᠤᠷ ᠵᠢᠨᠨ ᠪᠤᠴᠠᠭᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Comment like failed</source>
        <translation>ᠰᠡᠳᠬᠢᠭᠳᠡᠯ ᠦᠯᠡᠳᠡᠬᠡᠵᠤ ᠲᠤᠭᠤᠷ ᠦᠭᠴᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Failed to delete user comments</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠯᠡᠳᠡᠭᠡᠭᠰᠡᠨ ᠰᠡᠳᠬᠢᠭᠳᠡᠯ ᠢ᠋ ᠬᠠᠰᠤᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Update now</source>
        <translation>ᠳᠠᠷᠤᠢ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Deselect all</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠢ᠋ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>No software to update</source>
        <translation>ᠳᠦᠷ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Update software</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>No uninstallable software</source>
        <translation>ᠳᠦᠷ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Uninstall software</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Local historical installation</source>
        <translation>ᠳᠤᠰ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠤᠭᠰᠠᠷᠠᠭᠰᠠᠨ ᠲᠡᠤᠬᠡ</translation>
    </message>
    <message>
        <source>No historical installation</source>
        <translation>ᠳᠦᠷ ᠤᠭᠰᠠᠷᠠᠭᠰᠠᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Historical installation</source>
        <translation>ᠲᠡᠤᠬᠡᠨ ᠤᠭᠰᠠᠷᠠᠯ</translation>
    </message>
    <message>
        <source>Cloud history installation</source>
        <translation>ᠡᠬᠦᠯᠡᠨ ᠲᠡᠤᠬᠡᠨ ᠤᠭᠰᠠᠷᠠᠯ</translation>
    </message>
    <message>
        <source>Network problem, please try again later</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠲᠠᠢ᠂ ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Uninstall</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>all Pause</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠳᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <source>all Continue</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>Downloading</source>
        <translation>ᠶᠠᠭ ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Download path does not have read/write permission</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠵᠠᠮ ᠱᠤᠭᠤᠮ ᠡᠷᠬᠡ ᠵᠢ ᠤᠩᠰᠢᠵᠤ ᠪᠢᠴᠢᠭᠰᠡᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Please use a different download path</source>
        <translation>ᠪᠤᠰᠤᠳ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠵᠠᠮ ᠱᠤᠭᠤᠮ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <source>set up path</source>
        <translation>ᠵᠠᠮ ᠱᠤᠭᠤᠮ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source> is installed</source>
        <translation> ᠨᠢᠭᠡᠨᠳᠡ ᠤᠭᠰᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <source>Failed to install app!</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ ᠵᠢ ᠤᠭᠰᠠᠷᠴᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>Dependency resolution failed</source>
        <translation>ᠳᠦᠰᠢᠭᠯᠡᠯ ᠢ᠋ ᠵᠠᠳᠠᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Install Error</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Insufficient space</source>
        <translation>ᠤᠷᠤᠨ ᠵᠠᠢ ᠬᠦᠷᠦᠯᠴᠡᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Please check the network</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠪᠡᠨ ᠪᠠᠢᠴᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Successfully installed the new version</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠬᠡᠪᠯᠡᠯ ᠢ᠋ ᠤᠭᠰᠠᠷᠴᠤ ᠴᠢᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <source>Open the new software store</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠬᠦᠷ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Install error</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Please check apt</source>
        <translation type="vanished">请检查apt</translation>
    </message>
    <message>
        <source>Confirm to cancel the upgrade?</source>
        <translation>ᠳᠡᠰ ᠳᠡᠪᠰᠢᠬᠦ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>ok</translation>
    </message>
    <message>
        <source>Uninstall error</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>New version detected</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠬᠡᠪᠯᠡᠯ ᠢ᠋ ᠬᠢᠨᠠᠨ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠮᠡᠳᠡᠪᠡ</translation>
    </message>
    <message>
        <source>Downloading installation package...</source>
        <translation>ᠤᠭᠰᠠᠷᠬᠤ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠵᠢ ᠶᠠᠭ ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <source>Confirm to cancel the upgrade</source>
        <translation type="vanished">确定取消升级？</translation>
    </message>
    <message>
        <source>Cancel upgrade</source>
        <translation>ᠳᠡᠰ ᠳᠡᠪᠰᠢᠬᠦ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Continue upgrading</source>
        <translation>ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠨ ᠳᠡᠰ ᠳᠡᠪᠰᠢᠬᠦ</translation>
    </message>
    <message>
        <source>It is currently the latest version</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠳ᠋ᠤ᠌ ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠰᠢᠨ᠎ᠡ ᠬᠡᠪᠯᠡᠯ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>All results </source>
        <translation>ᠪᠦᠬᠦᠳᠡ ᠳᠠᠭᠤᠰᠪᠠ </translation>
    </message>
    <message>
        <source>Default ranking</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <source>Download ranking</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Sort ranking</source>
        <translation>ᠬᠤᠪᠢ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <source>Comment ranking</source>
        <translation>ᠰᠡᠳᠬᠢᠭᠳᠡᠯ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <source>UpdateTime sorting</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>There are still tasks in progress. Are you sure you want to exit?</source>
        <translation>ᠡᠬᠦᠷᠬᠡ ᠬᠦᠢᠴᠡᠳᠬᠡᠭᠳᠡᠭᠰᠡᠬᠡᠷ᠂ ᠨᠡᠬᠡᠷᠡᠨ ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <source>Exit now, you will not be able to check more applications.</source>
        <translation>ᠤᠳᠤ ᠪᠡᠷ ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠪᠠᠯ ᠲᠠ ᠨᠡᠩ ᠤᠯᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ ᠵᠢᠨ ᠫᠷᠦᠭᠷᠡᠮ ᠢ᠋ ᠪᠡᠶᠡᠴᠢᠯᠡᠨ ᠮᠡᠳᠡᠷᠡᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <source>maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠎ᠤᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <source>MB</source>
        <translation></translation>
    </message>
    <message>
        <source>KB</source>
        <translation></translation>
    </message>
    <message>
        <source>B</source>
        <translation></translation>
    </message>
    <message>
        <source>apk Security verification failed</source>
        <translation>apk ᠪᠠᠭᠯᠠᠭᠠᠨ ᠤ᠋ ᠠᠮᠤᠷ ᠳᠦᠪᠰᠢᠨ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>%1</source>
        <translation></translation>
    </message>
    <message>
        <source>About to update automatically</source>
        <translation>ᠤᠳᠠᠬᠤ ᠦᠬᠡᠢ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠰᠢᠨᠡᠴᠢᠯᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <source>Error Code</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <source>Environment startup failed</source>
        <translation>ᠤᠷᠴᠢᠨ ᠳᠤᠭᠤᠷᠢᠨ ᠢ᠋ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>The environment is initialized, the interface is temporarily locked, please wait...</source>
        <translation>ᠤᠷᠴᠢᠨ ᠳᠤᠭᠤᠷᠢᠨ ᠢ᠋ ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠵᠠᠭᠠᠭ ᠭᠠᠳᠠᠷᠭᠤ ᠵᠢ ᠳᠦᠷ ᠤᠨᠢᠰᠤᠯᠠᠪᠠ᠂ ᠬᠦᠯᠢᠶᠡᠵᠤ ᠪᠠᠢᠭᠠᠷᠠᠢ...</translation>
    </message>
</context>
<context>
    <name>NewFeaturesWidget</name>
    <message>
        <source>New Features</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠴᠢᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <source>Release time:</source>
        <translation>ᠨᠡᠢᠳᠡᠯᠡᠭᠰᠡᠨ ᠴᠠᠭ:</translation>
    </message>
    <message>
        <source>Installed version:</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠤᠭᠰᠠᠷᠰᠠᠨ ᠬᠡᠪᠯᠡᠯ:</translation>
    </message>
    <message>
        <source>Software size:</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ:</translation>
    </message>
    <message>
        <source>Latest version:</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠰᠢᠨ᠎ᠡ ᠬᠡᠪᠯᠡᠯ:</translation>
    </message>
</context>
<context>
    <name>NewProductsWidget</name>
    <message>
        <source>The latest %1 software on the shelf today</source>
        <translation>ᠦᠨᠦᠳᠦᠷ%1 ᠰᠢᠨᠡᠴᠢᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <source>View all</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>OtherInfomationModule</name>
    <message>
        <source>Privacy policy</source>
        <translation>ᠬᠤᠪᠢ ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠵᠢᠨ ᠪᠤᠳᠤᠯᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>kylin-software-center is already running!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RatingWidget</name>
    <message>
        <source>scores</source>
        <translation>ᠬᠤᠪᠢ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>full Score 5 points</source>
        <translation>ᠪᠦᠳᠦᠨ ᠬᠤᠪᠢ ᠨᠢ5 ᠬᠤᠪᠢ</translation>
    </message>
    <message>
        <source>My score</source>
        <translation>ᠮᠢᠨᠤ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠬᠤᠪᠢ</translation>
    </message>
    <message>
        <source>Scoring failed</source>
        <translation>ᠬᠤᠪᠢ ᠳᠤᠭᠳᠠᠭᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Please install the app before scoring</source>
        <translation>ᠬᠤᠪᠢ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ ᠡᠴᠡ ᠡᠮᠦᠨ᠎ᠡ ᠤᠷᠢᠳᠠᠪᠠᠷ ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ ᠵᠢ ᠤᠭᠰᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>No score yet</source>
        <translation>ᠳᠦᠷ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠬᠤᠪᠢ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>RemoterDaemon</name>
    <message>
        <source>Several new software products in the software store have been launched. Please check more details now!</source>
        <translation type="vanished">软件商店推出了几种新的软件产品,请立即查看更多详细信息！</translation>
    </message>
    <message>
        <source>Software Store</source>
        <translation type="vanished">软件商店</translation>
    </message>
    <message>
        <source>New Arrivals</source>
        <translation type="vanished">新品上架</translation>
    </message>
    <message>
        <source>View now</source>
        <translation type="vanished">立即查看</translation>
    </message>
</context>
<context>
    <name>SearchTipWidget</name>
    <message>
        <source>Daily recommendations</source>
        <translation>ᠡᠳᠦᠷ ᠪᠦᠷᠢ ᠳᠠᠨᠢᠯᠴᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>ᠨᠢᠬᠡ ᠪᠦᠯᠦᠭ ᠢ᠋ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <source>Search all software...</source>
        <translation>ᠪᠦᠬᠦ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠡᠴᠡ ᠬᠠᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <source>Software</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ</translation>
    </message>
    <message>
        <source>Mobile Apps</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ</translation>
    </message>
    <message>
        <source>Drive</source>
        <translation>ᠬᠦᠯᠳᠡᠬᠡᠬᠦᠷ</translation>
    </message>
</context>
<context>
    <name>SelectedApplicationsCard</name>
    <message>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠤᠭᠰᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>SelectedApplicationsWidget</name>
    <message>
        <source>View all</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Selected Applications</source>
        <translation>ᠨᠠᠷᠢᠯᠢᠭ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ</translation>
    </message>
</context>
<context>
    <name>SetWidget</name>
    <message>
        <source>software store</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <source>Download directory settings</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Save the downloaded software in the following directory:</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠢ᠋ ᠳᠤᠤᠷᠠᠬᠢ ᠭᠠᠷᠴᠠᠭ ᠲᠤ᠌ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ:</translation>
    </message>
    <message>
        <source>Restore default directory</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Installation package cleaning</source>
        <translation>ᠤᠭᠰᠠᠷᠬᠤ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>After installation, the downloaded installation package will be automatically deleted</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠳᠠᠭᠤᠰᠤᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ᠂ ᠪᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ ᠤᠭᠰᠠᠷᠬᠤ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠵᠢ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠬᠠᠰᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>After installation, keep the downloaded installation package</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠳᠠᠭᠤᠰᠤᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ᠂ ᠪᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ ᠤᠭᠰᠠᠷᠬᠤ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠵᠢ ᠦᠯᠡᠳᠡᠬᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <source>One week after the download, the installation package will be deleted automatically</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠭᠠᠳ ᠨᠢᠭᠡ ᠭᠠᠷᠠᠭ ᠪᠤᠯᠤᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ᠂ ᠤᠭᠰᠠᠷᠬᠤ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠵᠢ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠬᠠᠰᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Allow the installed software to generate desktop shortcuts</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠳᠦᠬᠦᠮ ᠳᠦᠳᠡ ᠠᠷᠭ᠎ᠠ ᠪᠤᠯᠬᠤ ᠵᠢ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <source>Update settings</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Automatic software update</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠢ᠋ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Automatically download and install software updates</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠪᠠ ᠤᠭᠰᠠᠷᠴᠤ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>When the software is updated, a notification is displayed on the right side of the screen</source>
        <translation type="vanished">软件有更新时，在屏幕右侧显示通知</translation>
    </message>
    <message>
        <source>When the software is updated, you will be notified</source>
        <translation type="vanished">软件有更新时，会以通知形式告诉您</translation>
    </message>
    <message>
        <source>Auto update software store</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠬᠦᠷ ᠢ᠋ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Automatically download the new version of software store and install it after you close software store</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠪᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ ᠰᠢᠨ᠎ᠡ ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠬᠦᠷ ᠨᠢ ᠲᠠ ᠬᠠᠭᠤᠴᠢᠨ ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠬᠦᠷ ᠢ᠋ ᠬᠠᠭᠠᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠤᠭᠰᠠᠷᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Start the software store automatically</source>
        <translation>ᠮᠠᠰᠢᠨ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠬᠦᠷ ᠢ᠋ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>After startup, the software store will start in the background</source>
        <translation>ᠮᠠᠰᠢᠨ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ᠂ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠬᠦᠷ ᠠᠷᠤ ᠳᠠᠪᠴᠠᠩ ᠳ᠋ᠤ᠌ ᠡᠬᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Server address settings</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠬᠠᠶᠢᠭ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>If there are internal services, you can change the server address to obtain all services of Kirin software store and Kirin ID.</source>
        <translation>ᠬᠡᠷᠪᠡ ᠳᠤᠳᠤᠭᠠᠳᠤ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠲᠠᠢ ᠪᠤᠯ᠂ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠬᠠᠶᠢᠭ ᠢ᠋ ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠪᠡᠷ ᠳᠠᠮᠵᠢᠭᠤᠯᠤᠨ ᠴᠢ ᠯᠢᠨ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠬᠦᠷ ᠪᠤᠯᠤᠨ ᠴᠢ ᠯᠢᠨ ID ᠤ᠋ᠨ/ ᠵᠢᠨ ᠪᠦᠬᠦ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠵᠢ ᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <source>Software store server address:</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠬᠦᠷ ᠤ᠋ᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠬᠠᠶᠢᠭ:</translation>
    </message>
    <message>
        <source>Kylin ID server address:</source>
        <translation>ᠴᠢ ᠯᠢᠨ ID ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠬᠠᠶᠢᠭ:</translation>
    </message>
    <message>
        <source>Restore default settings</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Download path cannot be empty</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ ᠵᠠᠮ ᠱᠤᠭᠤᠮ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Download path does not exist</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ ᠵᠠᠮ ᠱᠤᠭᠤᠮ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>SoftwareManagement</name>
    <message>
        <source>Update</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠢ᠋ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Uninstall</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠢ᠋ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Historical</source>
        <translation>ᠲᠡᠤᠬᠡᠨ ᠤᠭᠰᠠᠷᠠᠯ</translation>
    </message>
    <message>
        <source>Update software</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Select all</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Update all</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Uninstall all</source>
        <translation>ᠨᠢᠬᠡ ᠳᠤᠪᠴᠢᠳᠠᠯ ᠵᠢᠡᠷ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Install all</source>
        <translation>ᠨᠢᠭᠡ ᠳᠤᠪᠴᠢᠳᠠᠯ ᠵᠢᠡᠷ ᠤᠭᠰᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <source>Deselect all</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠢ᠋ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Local historical installation</source>
        <translation>ᠳᠤᠰ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠤᠭᠰᠠᠷᠠᠭᠰᠠᠨ ᠲᠡᠤᠬᠡ</translation>
    </message>
    <message>
        <source>Cloud history installation</source>
        <translation>ᠡᠬᠦᠯᠡᠨ ᠲᠡᠤᠬᠡᠨ ᠤᠭᠰᠠᠷᠠᠯ</translation>
    </message>
    <message>
        <source>About to update automatically</source>
        <translation>ᠤᠳᠠᠬᠤ ᠦᠬᠡᠢ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠰᠢᠨᠡᠴᠢᠯᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠤᠭᠰᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>SortButton</name>
    <message>
        <source>Default ranking</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ</translation>
    </message>
</context>
<context>
    <name>SortCardWidget</name>
    <message>
        <source>Down</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Run</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>SpecialModelCard</name>
    <message>
        <source>more</source>
        <translation>ᠨᠡᠩ ᠤᠯᠠᠨ</translation>
    </message>
</context>
<context>
    <name>SpecialModelWidget</name>
    <message>
        <source>View all</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Search software/mobile app/driver</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠬᠠᠢᠬᠤ/ ᠰᠢᠯᠵᠢᠮᠡᠯ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ/ ᠬᠦᠯᠳᠡᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <source>minimize</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <source>maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠎ᠤᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>homePageWidget</name>
    <message>
        <source>The latest %1 software on the shelf today</source>
        <translation>ᠦᠨᠦᠳᠦᠷ%1 ᠰᠢᠨᠡᠴᠢᠯᠡᠪᠡ</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Theme</source>
        <translation type="vanished">主题</translation>
    </message>
    <message>
        <source>Set</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Check for updates</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation>ᠴᠢᠯᠦᠬᠡᠳᠡᠢ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>ᠭᠦᠶᠦᠬᠡᠨ ᠦᠨᠭᠭᠡ ᠵᠢᠨ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>ᠭᠦᠨ ᠦᠨᠭᠭᠡ ᠵᠢᠨ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <source>kyliin-software-center</source>
        <translation type="vanished">软件商店</translation>
    </message>
    <message>
        <source>Software store is a graphical software management tool with rich content. It supports the recommendation of new products and popular applications, and provides one-stop software services such as software search, download, installation, update and uninstall.</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠬᠦᠷ ᠪᠤᠯ ᠨᠢᠭᠡ ᠵᠦᠢᠯ ᠤ᠋ᠨ ᠠᠭᠤᠯᠭ᠎ᠠ ᠪᠡᠷ ᠪᠠᠶᠠᠯᠢᠭ ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦᠳᠦ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠬᠠᠮᠢᠶᠠᠷᠬᠤ ᠪᠠᠭᠠᠵᠢ ᠪᠤᠯᠤᠨ᠎ᠠ᠂ ᠰᠢᠨ᠎ᠡ ᠪᠦᠳᠦᠬᠡᠭᠳᠡᠬᠦᠨ ᠢ᠋ ᠳᠡᠯᠬᠡᠬᠦᠷ ᠲᠤ᠌ ᠤᠷᠤᠭᠤᠯᠬᠤ᠂ ᠬᠠᠯᠠᠮᠰᠢᠯ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠢ᠋ ᠳᠠᠨᠢᠯᠴᠠᠭᠤᠯᠬᠤ ᠵᠡᠷᠭᠡ ᠴᠢᠳᠠᠮᠵᠢ ᠵᠢ ᠳᠡᠮᠵᠢᠵᠤ᠂ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠬᠠᠢᠬᠤ᠂ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ᠂ ᠤᠭᠰᠠᠷᠬᠤ᠂ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ᠂ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠵᠡᠷᠭᠡ ᠦᠷᠳᠡᠬᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠬᠠᠩᠭᠠᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Software store</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本:</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠤᠯᠤᠨ ᠳᠡᠮᠵᠢᠬᠦ ᠪᠦᠯᠬᠦᠮ: </translation>
    </message>
    <message>
        <source>kylin-software-center</source>
        <translation type="vanished">软件商店</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <source>Disclaimers:</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠴᠠᠯᠭ᠎ᠠ ᠡᠴᠡ ᠬᠡᠯᠲᠦᠷᠢᠭᠦᠯᠬᠦ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ:</translation>
    </message>
    <message>
        <source>Kylin Software Store Disclaimers</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠬᠦᠷ ᠤ᠋ᠨ ᠬᠠᠷᠢᠭᠤᠴᠠᠯᠭ᠎ᠠ ᠡᠴᠡ ᠬᠡᠯᠲᠦᠷᠢᠭᠦᠯᠬᠦ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ</translation>
    </message>
</context>
<context>
    <name>newproductcard</name>
    <message>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠤᠭᠰᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>N</source>
        <translation>ᠰᠢᠨ᠎ᠡ</translation>
    </message>
</context>
<context>
    <name>sideBarWidget</name>
    <message>
        <source>Software Store</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <source>Sign in</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
</context>
</TS>
