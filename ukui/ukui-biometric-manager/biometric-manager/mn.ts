<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn" sourcelanguage="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.ui" line="26"/>
        <location filename="../ui_aboutdialog.h" line="191"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.ui" line="283"/>
        <location filename="../ui_aboutdialog.h" line="195"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC DemiLight&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Noto Sans CJK SC&apos;;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC DemiLight&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Noto Sans CJK SC&apos;;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="31"/>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>Biometric Manager</source>
        <translation>ᠠᠮᠢᠳᠤ ᠪᠤᠳᠠᠰ ᠤ᠋ᠨ ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠳᠠ ᠵᠢᠨ ᠪᠠᠭᠠᠵᠢ</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="34"/>
        <source>Version number: </source>
        <translation>ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠳ᠋ᠤᠭᠠᠷ: </translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="35"/>
        <location filename="../src/aboutdialog.cpp" line="48"/>
        <source>         Biometric Manager is a supporting software for managing biometric identification which is developed by Kylin team.  It mainly contains biometirc verification management, biometirc service management, biometric device&apos;s driver management and biometirc features management, etc.All functions of the software are still being perfected. Please look forward to it. </source>
        <translation>         ᠠᠮᠢᠳᠤ ᠪᠤᠳᠠᠰ ᠤ᠋ᠨ ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠳᠠ ᠵᠢᠨ ᠪᠠᠭᠠᠵᠢ ᠪᠤᠯᠤ ᠴᠢ ᠯᠢᠨ ᠪᠦᠯᠬᠦᠮ ᠤ᠋ᠨ ᠨᠡᠬᠡᠬᠡᠭᠰᠡᠨ ᠠᠮᠢᠳᠤ ᠪᠤᠳᠠᠰ ᠤ᠋ᠨ ᠤᠨᠴᠠᠯᠢᠭ ᠢ᠋ ᠬᠠᠮᠢᠶᠠᠷᠬᠤ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠳᠤᠰᠠᠯᠠᠬᠤ ᠵᠦᠬᠡᠯᠡᠨ ᠳᠤᠨᠤᠭ ᠪᠤᠯᠤᠨ᠎ᠠ᠃ ᠭᠤᠤᠯ ᠴᠢᠳᠠᠮᠵᠢ ᠳ᠋ᠤ᠌ ᠠᠮᠢᠳᠤ ᠪᠤᠳᠠᠰ ᠤ᠋ᠨ ᠤᠨᠴᠠᠯᠢᠭ ᠢ᠋ ᠬᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠳᠠ᠂ ᠠᠮᠢᠳᠤ ᠪᠤᠳᠠᠰ ᠤ᠋ᠨ ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡᠨ ᠤ᠋ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠳᠠ᠂ ᠠᠮᠢᠳᠤ ᠪᠤᠳᠠᠰ ᠤ᠋ᠨ ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠬᠦᠳᠡᠯᠬᠡᠬᠦ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠳᠠ ᠵᠢᠴᠢ ᠠᠮᠢᠳᠤ ᠪᠤᠳᠠᠰ ᠤ᠋ᠨ ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠳᠠ ᠵᠡᠷᠬᠡ ᠴᠢᠳᠠᠮᠵᠢ ᠪᠠᠭᠳᠠᠨ᠎ᠠ᠃ ᠡᠯ᠎ᠡ ᠵᠦᠢᠯ ᠤ᠋ᠨ ᠴᠢᠳᠠᠮᠵᠢ ᠤᠳᠤᠬᠠᠨ ᠳ᠋ᠤ᠌ ᠪᠡᠨ ᠪᠠᠰᠠ ᠳᠠᠰᠤᠷᠠᠯᠳᠠ ᠦᠬᠡᠢ ᠳᠡᠬᠦᠯᠳᠡᠷᠵᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠬᠦᠰᠡᠨ ᠬᠦᠯᠢᠶᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠤᠤ. </translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="39"/>
        <source>Service &amp; Support: </source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠠ ᠳᠡᠮᠵᠢᠬᠦ ᠪᠦᠯᠬᠦᠮ: </translation>
    </message>
    <message>
        <source>developers：</source>
        <translation type="vanished">开发者邮箱</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Contributor</source>
        <translation type="vanished">贡献者</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;         Biometric Manager is a supporting software for managing biometric identification which is developed by Kylin team.  It mainly contains biometirc verification management, biometirc service management, biometric device&apos;s driver management and biometirc features management, etc.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;       &lt;/span&gt;&lt;a name=&quot;textarea-bg-text&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;A&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;ll functions of the software are still being perfected. Please look forward to it. &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;         生物特征管理工具是由麒麟团队开发的一款用于管理生物特征的辅助软件。主要功能包括生物特征认证管理、生物特征服务管理、生物特征设备驱动管理以及生物特征的管理等功能。&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;        各项功能目前还在不断完善中，敬请期待。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
</context>
<context>
    <name>AttributeWindow</name>
    <message>
        <location filename="../src/attributewindow.ui" line="14"/>
        <location filename="../ui_attributewindow.h" line="96"/>
        <source>Dialog</source>
        <translation>ᠬᠠᠷᠢᠯᠴᠠᠬᠤ ᠴᠤᠨᠭᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.ui" line="48"/>
        <location filename="../ui_attributewindow.h" line="99"/>
        <source>Bus Type:</source>
        <translation>ᠶᠡᠷᠦᠨᠭᠬᠡᠢ ᠬᠡᠯᠬᠢᠶ᠎ᠡ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ:</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.ui" line="62"/>
        <location filename="../ui_attributewindow.h" line="101"/>
        <source>Storage Type:</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠮᠵᠢ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ:</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.ui" line="69"/>
        <location filename="../ui_attributewindow.h" line="102"/>
        <source>Identification Type:</source>
        <translation>ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠬᠤ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ:</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.ui" line="83"/>
        <location filename="../ui_attributewindow.h" line="104"/>
        <source>Verify Type:</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ:</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="42"/>
        <source>FingerPrint</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ ᠤ᠋ ᠤᠷᠤᠮ</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="44"/>
        <source>Fingervein</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠨᠠᠮᠵᠢᠭᠤᠨ ᠰᠤᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="46"/>
        <source>Iris</source>
        <translation>ᠰᠤᠯᠤᠨᠭᠭ᠎ᠠ ᠪᠦᠷᠬᠦᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="48"/>
        <source>Face</source>
        <translation>ᠨᠢᠭᠤᠷ ᠴᠢᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="50"/>
        <source>VoicePrint</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ ᠢᠷᠠᠯᠵᠢ</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="59"/>
        <source>Hardware Verification</source>
        <translation>ᠬᠠᠳᠠᠭᠤ ᠳᠤᠨᠤᠭ ᠤ᠋ᠨ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="61"/>
        <source>Software Verification</source>
        <translation>ᠵᠦᠬᠡᠯᠡᠨ ᠲᠤᠨᠤᠭ ᠤ᠋ᠨ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="63"/>
        <source>Mix Verification</source>
        <translation>ᠬᠤᠯᠢᠮᠠᠭ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="65"/>
        <source>Other Verification</source>
        <translation>ᠪᠤᠰᠤᠳ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="73"/>
        <source>Device Storage</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="75"/>
        <source>OS Storage</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="77"/>
        <source>Mix Storage</source>
        <translation>ᠬᠤᠯᠢᠮᠠᠭ ᠬᠠᠳᠠᠭᠠᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="85"/>
        <source>Serial</source>
        <translation>ᠬᠤᠯᠪᠤᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="87"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="89"/>
        <source>PCIE</source>
        <translation>PCIE</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="91"/>
        <source>Any</source>
        <translation>ᠳᠤᠷ᠎ᠠ ᠵᠢᠨ ᠨᠢᠭᠡ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="93"/>
        <source>Other</source>
        <translation>ᠪᠤᠰᠤᠳ</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="101"/>
        <source>Hardware Identification</source>
        <translation>ᠬᠠᠳᠠᠭᠤ ᠲᠤᠨᠤᠭ ᠤ᠋ᠨ ᠢᠯᠭᠠᠨ ᠳᠨᠠᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="103"/>
        <source>Software Identification</source>
        <translation>ᠵᠦᠬᠡᠯᠡᠨ ᠳᠤᠨᠤᠭ ᠤ᠋ᠨ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="105"/>
        <source>Mix Identification</source>
        <translation>ᠬᠤᠯᠢᠮᠠᠭ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="107"/>
        <source>Other Identification</source>
        <translation>ᠪᠤᠰᠤᠳ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ</translation>
    </message>
</context>
<context>
    <name>ContentPane</name>
    <message>
        <location filename="../src/contentpane.ui" line="14"/>
        <location filename="../ui_contentpane.h" line="119"/>
        <source>Form</source>
        <translation>ᠹᠣᠣᠮ</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <source>Driver Status:</source>
        <translation type="vanished">驱动状态：</translation>
    </message>
    <message>
        <source>Device Status:</source>
        <translation type="vanished">设备状态：</translation>
    </message>
    <message>
        <source>Default Device:</source>
        <translation type="vanished">默认设备：</translation>
    </message>
    <message>
        <source>Verify Type:</source>
        <translation type="vanished">验证类型：</translation>
    </message>
    <message>
        <source>Clean</source>
        <translation type="vanished">清空</translation>
    </message>
    <message>
        <source>Biometric Type:</source>
        <translation type="obsolete">生物特征类型：</translation>
    </message>
    <message>
        <source>Device Short Name:</source>
        <translation type="vanished">设备简称：</translation>
    </message>
    <message>
        <source>Device Full Name:</source>
        <translation type="vanished">设备全称：</translation>
    </message>
    <message>
        <source>Bus Type:</source>
        <translation type="vanished">总线类型：</translation>
    </message>
    <message>
        <source>Identification Type:</source>
        <translation type="vanished">识别类型：</translation>
    </message>
    <message>
        <source>Storage Type:</source>
        <translation type="vanished">存储类型：</translation>
    </message>
    <message>
        <source>Status:</source>
        <translation type="obsolete">状态：</translation>
    </message>
    <message>
        <source>Biometric Feature List</source>
        <translation type="obsolete">特征列表</translation>
    </message>
    <message>
        <source>Enroll</source>
        <translation type="vanished">录入</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <source>Verify</source>
        <translation type="vanished">验证</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation type="obsolete">清空所有</translation>
    </message>
    <message>
        <source>Feature name</source>
        <translation type="obsolete">特征名称</translation>
    </message>
    <message>
        <source>index</source>
        <translation type="obsolete">索引</translation>
    </message>
    <message>
        <source>Device is not connected</source>
        <translation type="obsolete">设备未连接</translation>
    </message>
    <message>
        <source>Device is available</source>
        <translation type="obsolete">设备可用</translation>
    </message>
    <message>
        <source>Opened</source>
        <translation type="vanished">开</translation>
    </message>
    <message>
        <source>Closed</source>
        <translation type="vanished">关</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="174"/>
        <source>Connected</source>
        <translation>ᠨᠢᠬᠡᠨᠳᠡ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="174"/>
        <source>Unconnected</source>
        <translation>ᠳᠠᠰᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="285"/>
        <source>New Feature</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠤᠨᠴᠠᠯᠢᠭ</translation>
    </message>
    <message>
        <source>Please input a name for the feature:</source>
        <translation type="vanished">特征名称：</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="286"/>
        <source>name:</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ:</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="288"/>
        <source>Rename Feature</source>
        <translation>ᠣᠨᠴᠠᠯᠢᠭ ᠢ ᠳᠠᠬᠢᠨ ᠨᠡᠡᠷᠡᠢᠳᠴᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="294"/>
        <source>Duplicate feature name</source>
        <translation>ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠳᠠᠪᠬᠤᠴᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="296"/>
        <source>Empty feature name</source>
        <translation>ᠬᠤᠭᠤᠰᠤᠨ ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="298"/>
        <source>feature name is too long</source>
        <translation>ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠬᠡᠳᠦ ᠤᠷᠳᠤ</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="498"/>
        <source>Fail to delete! </source>
        <translation>ᠤᠰᠠᠳᠬᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ! </translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="502"/>
        <source>Delete successfully!</source>
        <translation>ᠤᠰᠠᠳᠬᠠᠵᠤ ᠴᠢᠳᠠᠪᠠ!</translation>
    </message>
    <message>
        <source>Please do not use &apos;_&apos; as the beginning of the feature name </source>
        <translation type="vanished">请不要以空格作为特征名称的开头</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="300"/>
        <source>Please do not use spaces as the beginning or end of the feature name</source>
        <translation>ᠪᠢᠳᠡᠬᠡᠢ ᠬᠤᠭᠤᠰᠤᠨ ᠵᠠᠢ ᠵᠢᠡᠷ ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠤ᠋ᠨ ᠡᠬᠢᠯᠡᠯᠳᠡ ᠪᠤᠶᠤ ᠳᠡᠬᠦᠰᠬᠡᠯ ᠪᠤᠯᠭᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="293"/>
        <source>Please input a new</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠵᠢ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="396"/>
        <source>Confirm whether clean all the features?</source>
        <translation>ᠪᠦᠬᠦ ᠤᠨᠴᠠᠯᠢᠭ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠡᠰᠡᠬᠦ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="405"/>
        <source>Confirm Clean</source>
        <translation>ᠬᠤᠭᠤᠰᠤᠯᠠᠭᠰᠠᠨ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="408"/>
        <location filename="../src/contentpane.cpp" line="411"/>
        <source>Confirm whether delete the features selected?</source>
        <translation>ᠰᠤᠨᠭᠭᠤᠭᠰᠠᠨ ᠤᠨᠴᠠᠯᠢᠭ ᠢ᠋ ᠤᠰᠠᠳᠬᠠᠬᠤ ᠡᠰᠡᠬᠦ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="409"/>
        <source>Confirm Delete</source>
        <translation>ᠤᠰᠠᠳᠬᠠᠬᠤ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Feature Delete</source>
        <translation type="vanished">特征删除</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="458"/>
        <source>Please select the feature you want to delete.</source>
        <translation>ᠤᠰᠠᠳᠬᠠᠬᠤ ᠬᠡᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠴᠢᠳᠠᠮᠵᠢ ᠪᠡᠨ ᠰᠤᠨᠭᠭᠤᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <source>Delete successfully</source>
        <translation type="vanished">删除成功</translation>
    </message>
    <message>
        <source>The result of delete:</source>
        <translation type="vanished">删除结果：</translation>
    </message>
    <message>
        <source>Clean Failed: </source>
        <translation type="vanished">清空失败：</translation>
    </message>
    <message>
        <source>Clean successfully</source>
        <translation type="vanished">清空成功</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="704"/>
        <source>DBus calling error</source>
        <translation>DBus ᠵᠢ/ ᠢ᠋ ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠳ᠋ᠤ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <source>Delete all selected features successfully</source>
        <translation type="obsolete">所有选择的特征删除成功</translation>
    </message>
    <message>
        <source>Delete Result</source>
        <translation type="obsolete">删除结果</translation>
    </message>
    <message>
        <source>Clean Successfully</source>
        <translation type="obsolete">清空成功</translation>
    </message>
    <message>
        <source>Clean Result</source>
        <translation type="vanished">清空结果</translation>
    </message>
    <message>
        <source>Feature Verify</source>
        <translation type="vanished">特征验证</translation>
    </message>
    <message>
        <source>Please select the feature you want to verify.</source>
        <translation type="vanished">请选择您想要验证的特征</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="673"/>
        <source>Rename Successfully</source>
        <translation>ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠵᠤ ᠴᠢᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="689"/>
        <source>Rename Result</source>
        <translation>ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠭᠰᠡᠨ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠨᠭ</translation>
    </message>
    <message>
        <source>Delete Failed</source>
        <translation type="obsolete">删除失败</translation>
    </message>
    <message>
        <source>Clean Failed</source>
        <translation type="obsolete">清空失败</translation>
    </message>
    <message>
        <source>Rename Failed</source>
        <translation type="obsolete">重命名失败</translation>
    </message>
    <message>
        <source>Fingerprint</source>
        <translation type="obsolete">指纹</translation>
    </message>
    <message>
        <source>Fingervein</source>
        <translation type="obsolete">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="obsolete">虹膜</translation>
    </message>
    <message>
        <source>FingerPrint</source>
        <translation type="obsolete">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="obsolete">指静脉</translation>
    </message>
    <message>
        <source>Hardware Verification</source>
        <translation type="obsolete">硬件验证</translation>
    </message>
    <message>
        <source>Software Verification</source>
        <translation type="obsolete">软件验证</translation>
    </message>
    <message>
        <source>Mix Verification</source>
        <translation type="obsolete">混合验证</translation>
    </message>
    <message>
        <source>Other Verification</source>
        <translation type="obsolete">其他验证</translation>
    </message>
    <message>
        <source>Serial</source>
        <translation type="obsolete">串口</translation>
    </message>
    <message>
        <source>USB</source>
        <translation type="obsolete">USB</translation>
    </message>
    <message>
        <source>PCIE</source>
        <translation type="obsolete">PCIE</translation>
    </message>
    <message>
        <source>Device Storage</source>
        <translation type="obsolete">设备存储</translation>
    </message>
    <message>
        <source>OS Storage</source>
        <translation type="obsolete">系统存储</translation>
    </message>
    <message>
        <source>Mix Storage</source>
        <translation type="obsolete">混合存储</translation>
    </message>
    <message>
        <source>Hardware Identification</source>
        <translation type="obsolete">硬件识别</translation>
    </message>
    <message>
        <source>Software Identification</source>
        <translation type="obsolete">软件识别</translation>
    </message>
    <message>
        <source>Mix Identification</source>
        <translation type="obsolete">混合识别</translation>
    </message>
    <message>
        <source>Other Identification</source>
        <translation type="obsolete">其他识别</translation>
    </message>
    <message>
        <source> list</source>
        <translation type="obsolete">列表</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="173"/>
        <source>List</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="87"/>
        <location filename="../src/contentpane.cpp" line="411"/>
        <location filename="../src/contentpane.cpp" line="458"/>
        <location filename="../src/contentpane.cpp" line="502"/>
        <location filename="../src/contentpane.cpp" line="680"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Please input a feature name</source>
        <translation type="obsolete">请输入特征名称</translation>
    </message>
    <message>
        <source>Feature Rename</source>
        <translation type="obsolete">特征重命名</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="289"/>
        <source>Please input a new name for the feature:</source>
        <translation>ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠰᠢᠨ᠎ᠡ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ:</translation>
    </message>
    <message>
        <source>Permission is required. Please authenticate yourself to continue</source>
        <translation type="obsolete">需要授权！请先进行认证以继续操作</translation>
    </message>
    <message>
        <source>Enroll successfully</source>
        <translation type="obsolete">录入成功</translation>
    </message>
    <message>
        <source>D-Bus calling error</source>
        <translation type="obsolete">D-Bus 调用错误</translation>
    </message>
    <message>
        <source>Failed to enroll</source>
        <translation type="obsolete">录入失败</translation>
    </message>
    <message>
        <source>Device encounters an error</source>
        <translation type="obsolete">设备遇到错误</translation>
    </message>
    <message>
        <source>Operation timeout</source>
        <translation type="obsolete">操作超时</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="712"/>
        <source>Device is busy</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠠᠪ ᠦᠬᠡᠢ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="716"/>
        <source>No such device</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="720"/>
        <source>Permission denied</source>
        <translation>ᠡᠷᠬᠡ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Search Result</source>
        <translation type="obsolete">搜索结果</translation>
    </message>
    <message>
        <source>Failed to get notify message</source>
        <translation type="obsolete">读取操作信息失败</translation>
    </message>
    <message>
        <source>In progress, please wait...</source>
        <translation type="obsolete">操作中，请稍后...</translation>
    </message>
    <message>
        <source>Match successfully</source>
        <translation type="obsolete">匹配成功</translation>
    </message>
    <message>
        <source>Not Match</source>
        <translation type="obsolete">不匹配</translation>
    </message>
    <message>
        <source>Failed to match</source>
        <translation type="obsolete">匹配失败</translation>
    </message>
    <message>
        <source>Found the matching features:</source>
        <translation type="obsolete">搜索到匹配的特征:</translation>
    </message>
    <message>
        <source>Found the matching features:
</source>
        <translation type="obsolete">搜索到匹配的特征:</translation>
    </message>
    <message>
        <source>Found the matching features: 
</source>
        <translation type="obsolete">查找到匹配的特征:</translation>
    </message>
    <message>
        <source>Found the matching features name: </source>
        <translation type="obsolete">搜索到匹配的特征名称：</translation>
    </message>
    <message>
        <source>No matching features Found</source>
        <translation type="obsolete">未搜索到匹配的特征</translation>
    </message>
    <message>
        <source>Found! Username: %1, Feature name: %2</source>
        <translation type="obsolete">搜索成功！用户名：%1，特征名称：%2</translation>
    </message>
    <message>
        <source>Not Found</source>
        <translation type="obsolete">未搜索到</translation>
    </message>
</context>
<context>
    <name>EnumToString</name>
    <message>
        <location filename="../src/customtype.cpp" line="101"/>
        <source>FingerPrint</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ ᠤ᠋ ᠤᠷᠤᠮ</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="103"/>
        <source>Fingervein</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠨᠠᠮᠵᠢᠭᠤᠨ ᠰᠤᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="105"/>
        <source>Iris</source>
        <translation>ᠰᠤᠯᠤᠨᠭᠭ᠎ᠠ ᠪᠦᠷᠬᠦᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="107"/>
        <source>Face</source>
        <translation>ᠨᠢᠭᠤᠷ ᠴᠢᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="109"/>
        <source>VoicePrint</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ ᠢᠷᠠᠯᠵᠢ</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="117"/>
        <source>Hardware Verification</source>
        <translation>ᠬᠠᠳᠠᠭᠤ ᠳᠤᠨᠤᠭ ᠤ᠋ᠨ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="119"/>
        <source>Software Verification</source>
        <translation>ᠵᠦᠬᠡᠯᠡᠨ ᠲᠤᠨᠤᠭ ᠤ᠋ᠨ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="121"/>
        <source>Mix Verification</source>
        <translation>ᠬᠤᠯᠢᠮᠠᠭ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="123"/>
        <source>Other Verification</source>
        <translation>ᠪᠤᠰᠤᠳ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="131"/>
        <source>Device Storage</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="133"/>
        <source>OS Storage</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="135"/>
        <source>Mix Storage</source>
        <translation>ᠬᠤᠯᠢᠮᠠᠭ ᠬᠠᠳᠠᠭᠠᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="143"/>
        <source>Serial</source>
        <translation>ᠬᠤᠯᠪᠤᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="145"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="147"/>
        <source>PCIE</source>
        <translation>PCIE</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="149"/>
        <source>Any</source>
        <translation>ᠳᠤᠷ᠎ᠠ ᠵᠢᠨ ᠨᠢᠭᠡ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="151"/>
        <source>Other</source>
        <translation>ᠪᠤᠰᠤᠳ</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="159"/>
        <source>Hardware Identification</source>
        <translation>ᠬᠠᠳᠠᠭᠤ ᠲᠤᠨᠤᠭ ᠤ᠋ᠨ ᠢᠯᠭᠠᠨ ᠳᠨᠠᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="161"/>
        <source>Software Identification</source>
        <translation>ᠵᠦᠬᠡᠯᠡᠨ ᠳᠤᠨᠤᠭ ᠤ᠋ᠨ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="163"/>
        <source>Mix Identification</source>
        <translation>ᠬᠤᠯᠢᠮᠠᠭ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="165"/>
        <source>Other Identification</source>
        <translation>ᠪᠤᠰᠤᠳ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ</translation>
    </message>
</context>
<context>
    <name>InputDialog</name>
    <message>
        <location filename="../src/inputdialog.ui" line="14"/>
        <location filename="../ui_inputdialog.h" line="142"/>
        <source>Dialog</source>
        <translation>ᠬᠠᠷᠢᠯᠴᠠᠬᠤ ᠴᠤᠨᠭᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/inputdialog.ui" line="202"/>
        <location filename="../ui_inputdialog.h" line="146"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../src/inputdialog.ui" line="215"/>
        <location filename="../ui_inputdialog.h" line="147"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/inputdialog.cpp" line="49"/>
        <source>feature name is too long</source>
        <translation>ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠬᠡᠳᠦ ᠤᠷᠳᠤ</translation>
    </message>
    <message>
        <location filename="../src/inputdialog.cpp" line="51"/>
        <source>Duplicate feature name</source>
        <translation>ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠳᠠᠪᠬᠤᠴᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/inputdialog.cpp" line="53"/>
        <source>Please do not use spaces as the beginning or end of the feature name</source>
        <translation>ᠪᠢᠳᠡᠬᠡᠢ ᠬᠤᠭᠤᠰᠤᠨ ᠵᠠᠢ ᠵᠢᠡᠷ ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠤ᠋ᠨ ᠡᠬᠢᠯᠡᠯᠳᠡ ᠪᠤᠶᠤ ᠳᠡᠬᠦᠰᠬᠡᠯ ᠪᠤᠯᠭᠠᠭᠠᠷᠠᠢ</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="261"/>
        <source>Biometric Manager</source>
        <translation>ᠠᠮᠢᠳᠤ ᠪᠤᠳᠠᠰ ᠤ᠋ᠨ ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠳᠠ ᠵᠢᠨ ᠪᠠᠭᠠᠵᠢ</translation>
    </message>
    <message>
        <source>Icon</source>
        <translation type="obsolete">图标</translation>
    </message>
    <message>
        <source>Dashboard</source>
        <translation type="obsolete">主面板</translation>
    </message>
    <message>
        <source>Authentication Management</source>
        <translation type="obsolete">认证管理</translation>
    </message>
    <message>
        <source>Biometric Authentication Status:</source>
        <translation type="obsolete">生物特征认证状态：</translation>
    </message>
    <message>
        <source>Biometric Authentication can take over system authentication processes which include Login, Screensaver, sudo/su and Polkit.</source>
        <translation type="obsolete">生物特征可接管系统认证过程，包括登录、锁屏、sudo/su 授权和 Polkit 提权。</translation>
    </message>
    <message>
        <source>Biometric authentication is enabled only when the biometric identification is opened, existing available devices are turned on and the user has enrolled the feature.</source>
        <translation type="obsolete">只用当开启了生物特征、存在可用设备而且该用户已经录入了特征才会启用生物特征认证</translation>
    </message>
    <message>
        <source>Device Driver Management</source>
        <translation type="obsolete">设备驱动管理</translation>
    </message>
    <message>
        <source>Fingerprint Devices Driver</source>
        <translation type="obsolete">指纹设备驱动</translation>
    </message>
    <message>
        <source>Fingervein Devices Driver</source>
        <translation type="obsolete">指静脉设备驱动</translation>
    </message>
    <message>
        <source>Iris Devices Driver</source>
        <translation type="obsolete">虹膜设备驱动</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1016"/>
        <location filename="../src/mainwindow.ui" line="1349"/>
        <location filename="../src/mainwindow.ui" line="1679"/>
        <location filename="../src/mainwindow.ui" line="2009"/>
        <location filename="../src/mainwindow.ui" line="2339"/>
        <location filename="../ui_mainwindow.h" line="1513"/>
        <location filename="../ui_mainwindow.h" line="1518"/>
        <location filename="../ui_mainwindow.h" line="1523"/>
        <location filename="../ui_mainwindow.h" line="1528"/>
        <location filename="../ui_mainwindow.h" line="1533"/>
        <source>Device Management</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠳᠠ</translation>
    </message>
    <message>
        <source>Fingerprint Devices</source>
        <translation type="vanished">指纹设备</translation>
    </message>
    <message>
        <source>Fingervein Devices</source>
        <translation type="vanished">指静脉设备</translation>
    </message>
    <message>
        <source>Iris Devices</source>
        <translation type="vanished">虹膜设备</translation>
    </message>
    <message>
        <source>Fingerprint</source>
        <translation type="vanished">指纹</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="26"/>
        <location filename="../ui_mainwindow.h" line="1495"/>
        <source>BiometricManager</source>
        <translation>ᠠᠮᠢᠳᠤ ᠪᠤᠳᠠᠰ ᠤ᠋ᠨ ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="147"/>
        <location filename="../src/mainwindow.ui" line="177"/>
        <location filename="../ui_mainwindow.h" line="1496"/>
        <location filename="../ui_mainwindow.h" line="1497"/>
        <source>TextLabel</source>
        <translation>ᠲᠸᠺ ᠤ᠋ᠨ ᠱᠤᠰᠢᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2509"/>
        <location filename="../ui_mainwindow.h" line="1535"/>
        <source>Biometirc Manager</source>
        <translation>ᠠᠮᠢᠳᠤ ᠪᠤᠳᠠᠰ ᠤ᠋ᠨ ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠳᠠ</translation>
    </message>
    <message>
        <source>UserName</source>
        <translation type="vanished">用户名</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="406"/>
        <location filename="../src/mainwindow.ui" line="2611"/>
        <location filename="../src/mainwindow.cpp" line="262"/>
        <location filename="../ui_mainwindow.h" line="1499"/>
        <location filename="../ui_mainwindow.h" line="1538"/>
        <source>Biometric</source>
        <translation>ᠠᠮᠢᠳᠤ ᠪᠤᠳᠠᠰ ᠤ᠋ᠨ ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="776"/>
        <location filename="../src/mainwindow.ui" line="2671"/>
        <location filename="../src/mainwindow.cpp" line="1109"/>
        <location filename="../ui_mainwindow.h" line="1509"/>
        <location filename="../ui_mainwindow.h" line="1540"/>
        <source>FingerPrint</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ ᠤ᠋ ᠤᠷᠤᠮ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2721"/>
        <location filename="../src/mainwindow.cpp" line="1109"/>
        <location filename="../ui_mainwindow.h" line="1542"/>
        <source>FingerVein</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠨᠠᠮᠵᠢᠭᠤᠨ ᠰᠤᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2696"/>
        <location filename="../src/mainwindow.cpp" line="1109"/>
        <location filename="../ui_mainwindow.h" line="1541"/>
        <source>Face</source>
        <translation>ᠨᠢᠭᠤᠷ ᠴᠢᠷᠠᠢ ᠵᠢᠨ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="326"/>
        <location filename="../ui_mainwindow.h" line="1498"/>
        <source>Permisions</source>
        <translation>ᠡᠷᠬᠡ ᠵᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="425"/>
        <location filename="../ui_mainwindow.h" line="1500"/>
        <source>After this function is enabled, it can be used for system authentication, including login, screen lock, system authorization, and Polkit.</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ᠂ ᠡᠬᠦᠨ ᠳ᠋ᠤ᠌ ᠰᠢᠰᠲ᠋ᠧᠮ ᠳ᠋ᠤ᠌ ᠤᠷᠤᠬᠤ᠂ ᠳᠡᠯᠭᠡᠴᠡ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ ᠪᠠ Polkit ᠵᠡᠷᠬᠡ ᠪᠠᠭᠳᠠᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="490"/>
        <location filename="../ui_mainwindow.h" line="1502"/>
        <source>Biometrics will be used to:</source>
        <translation>ᠠᠮᠢᠳᠤ ᠪᠤᠳᠠᠰ ᠤ᠋ᠨ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="535"/>
        <location filename="../ui_mainwindow.h" line="1503"/>
        <source>Login system</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠳ᠋ᠤ᠌ ᠤᠷᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="608"/>
        <location filename="../ui_mainwindow.h" line="1505"/>
        <source>Unlock the lock screen</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠵᠢᠨ ᠤᠨᠢᠰᠤ ᠵᠢ ᠳᠠᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="681"/>
        <location filename="../ui_mainwindow.h" line="1507"/>
        <source>System authorization</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="818"/>
        <location filename="../ui_mainwindow.h" line="1510"/>
        <source>Seletct Device</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠰᠤᠨᠭᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="961"/>
        <location filename="../ui_mainwindow.h" line="1512"/>
        <source>Fingerprint recognition is not available. No device supporting this feature was detected.</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ ᠤᠷᠤᠮ ᠢ᠋ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠬᠤ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂ ᠳᠤᠰ ᠴᠢᠳᠠᠮᠵᠢ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠤᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1115"/>
        <location filename="../ui_mainwindow.h" line="1514"/>
        <source>Face Recognition </source>
        <translation>ᠴᠢᠷᠠᠢ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1157"/>
        <location filename="../src/mainwindow.ui" line="1490"/>
        <location filename="../src/mainwindow.ui" line="1820"/>
        <location filename="../src/mainwindow.ui" line="2150"/>
        <location filename="../ui_mainwindow.h" line="1515"/>
        <location filename="../ui_mainwindow.h" line="1520"/>
        <location filename="../ui_mainwindow.h" line="1525"/>
        <location filename="../ui_mainwindow.h" line="1530"/>
        <source>Select Device</source>
        <translation>ᠰᠤᠨᠭᠭᠤᠭᠰᠠᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1297"/>
        <location filename="../ui_mainwindow.h" line="1517"/>
        <source>Face recognition is not available. No device supporting this feature was detected.</source>
        <translation>ᠴᠢᠷᠠᠢ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂ ᠳᠤᠰ ᠴᠢᠳᠠᠮᠵᠢ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠤᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1627"/>
        <location filename="../ui_mainwindow.h" line="1522"/>
        <source>Fingervein recognition is not available. No device supporting this feature was detected.</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ ᠤᠷᠤᠮ ᠤ᠋ᠨ ᠨᠠᠮᠵᠢᠭᠤᠨ ᠰᠤᠳᠠᠯ ᠤ᠋ᠨ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂ ᠳᠤᠰ ᠴᠢᠳᠠᠮᠵᠢ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠤᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1778"/>
        <location filename="../ui_mainwindow.h" line="1524"/>
        <source>Iris Recognition</source>
        <translation>ᠰᠤᠯᠤᠨᠭᠭ᠎ᠠ ᠪᠦᠷᠬᠦᠪᠴᠢ ᠵᠢᠨ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1957"/>
        <location filename="../ui_mainwindow.h" line="1527"/>
        <source>Iric recognition is not available. No device supporting this feature was detected.</source>
        <translation>ᠰᠤᠯᠤᠨᠭᠭ᠎ᠠ ᠪᠦᠷᠬᠦᠪᠴᠢ ᠵᠢᠨ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ. ᠳᠤᠰ ᠴᠢᠳᠠᠮᠵᠢ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠤᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2108"/>
        <location filename="../ui_mainwindow.h" line="1529"/>
        <source>Voiceprint</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ ᠢᠷᠠᠯᠵᠢ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2287"/>
        <location filename="../ui_mainwindow.h" line="1532"/>
        <source>Voiceprint recognition is not available. No device supporting this feature was detected.</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ᠋ ᠢᠷᠠᠯᠵᠢ ᠵᠢᠨ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂ ᠳᠤᠰ ᠴᠢᠳᠠᠮᠵᠢ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠤᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2547"/>
        <location filename="../ui_mainwindow.h" line="1536"/>
        <source>Device&amp;Permission</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠬᠢᠬᠡᠳ ᠡᠷᠬᠡ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2580"/>
        <location filename="../ui_mainwindow.h" line="1537"/>
        <source>Permissions</source>
        <translation>ᠡᠷᠬᠡ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2641"/>
        <location filename="../src/mainwindow.cpp" line="260"/>
        <location filename="../ui_mainwindow.h" line="1539"/>
        <source>Password Setting</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2771"/>
        <location filename="../src/mainwindow.cpp" line="1110"/>
        <location filename="../ui_mainwindow.h" line="1544"/>
        <source>VoicePrint</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ ᠢᠷᠠᠯᠵᠢ</translation>
    </message>
    <message>
        <source>System components are certified using biometrics</source>
        <translation type="vanished">系统组件使用生物特征进行认证</translation>
    </message>
    <message>
        <source>Biometric Verification Status:</source>
        <translation type="obsolete">生物特征状态：</translation>
    </message>
    <message>
        <source>Closed</source>
        <translation type="vanished">关</translation>
    </message>
    <message>
        <source>     Devices Type</source>
        <translation type="obsolete">    设备类型</translation>
    </message>
    <message>
        <source>     All Devices</source>
        <translation type="obsolete">所有设备</translation>
    </message>
    <message>
        <source>Driver Not Found</source>
        <translation type="vanished">驱动未找到</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1448"/>
        <location filename="../ui_mainwindow.h" line="1519"/>
        <source>Fingervein</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠨᠠᠮᠵᠢᠭᠤᠨ ᠰᠤᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2746"/>
        <location filename="../src/mainwindow.cpp" line="1110"/>
        <location filename="../ui_mainwindow.h" line="1543"/>
        <source>Iris</source>
        <translation>ᠰᠤᠯᠤᠨᠭᠭ᠎ᠠ ᠪᠦᠷᠬᠦᠪᠴᠢ</translation>
    </message>
    <message>
        <source>Fatal Error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>the biometric-authentication service was not started</source>
        <translation type="obsolete">&apos;biometric-authentication&apos;服务没有启动</translation>
    </message>
    <message>
        <source>API version is not compatible</source>
        <translation type="obsolete">API 版本不兼容</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="459"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <source>exit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <source>help</source>
        <translation type="vanished">帮助</translation>
    </message>
    <message>
        <source>Opened</source>
        <translation type="vanished">开</translation>
    </message>
    <message>
        <source>Biometric Authentication can take over system authentication processes which include Login, LockScreen, sudo/su and Polkit</source>
        <translation type="vanished">生物特征可进行系统认证，包括登录、锁屏、sudo/su 授权和 Polkit 提权。</translation>
    </message>
    <message>
        <source>There is no any available biometric device or no features enrolled currently.</source>
        <translation type="obsolete">当前没有可用的生物特征设备，或者当前用户没有录入任何特征。</translation>
    </message>
    <message>
        <source>Process of using biometrics 1.Confirm that the device is connected 2.Set the connected device as the default 3. The biometric status is to be turned on. 4.Finally enter the fingerprint</source>
        <translation type="vanished"> 使用生物特征的流程 1.确认设备已连接 2.将已连接的设备设为默认 3.打开生物特征状态开关 4.最后录入指纹 </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="310"/>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠎ᠤᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="329"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="321"/>
        <source>Main menu</source>
        <translation>ᠲᠤᠪᠶᠤᠭ</translation>
    </message>
    <message>
        <source>Devices and Permissions</source>
        <translation type="vanished">设备与权限</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="471"/>
        <source>Exit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="476"/>
        <source>Help</source>
        <translation>ᠬᠠᠪᠰᠤᠷᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>Process of using biometrics 1.Confirm that the device is connected 2.Set the connected device as the default 3. The biometric status is to be turned on. 4.Finally enter the feature</source>
        <translation type="vanished">使用生物特征的流程 1.确认设备已连接 2.将已连接的设备设为默认 3.打开生物特征状态开关 4.最后录入生物特征</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="820"/>
        <location filename="../src/mainwindow.cpp" line="1367"/>
        <source>(default) </source>
        <translation>( ᠠᠶᠠᠳᠠᠯ) </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1267"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1267"/>
        <source>Status</source>
        <translation>ᠪᠠᠢᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1267"/>
        <source>Driver</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1267"/>
        <source>Default</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1462"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1743"/>
        <source>Connected</source>
        <translation>ᠨᠢᠬᠡᠨᠳᠡ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1748"/>
        <source>Unconnected</source>
        <translation>ᠳᠠᠰᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1753"/>
        <source>disable</source>
        <translation>ᠴᠠᠭᠠᠵᠠᠯᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1770"/>
        <source>The Service is stopped</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠵᠢ ᠨᠢᠬᠡᠨᠳᠡ ᠬᠠᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1461"/>
        <source>Fail to change device status</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠪᠠᠢᠳᠠᠯ ᠢ᠋ ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="264"/>
        <source>Select Device Type</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠢ᠋ ᠰᠤᠨᠭᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="452"/>
        <source>Restart Service</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠵᠢ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>  Restart immediately  </source>
        <translation type="obsolete">立即重启</translation>
    </message>
    <message>
        <source>  Restart later  </source>
        <translation type="obsolete">稍后重启</translation>
    </message>
    <message>
        <source>The configuration has been modified. Restart the service immediately to make it effecitve?</source>
        <translation type="obsolete">配置修改成功，是否立即重启服务使其生效？</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">错误</translation>
    </message>
    <message>
        <source>Device is not connected</source>
        <translation type="obsolete">设备未连接</translation>
    </message>
    <message>
        <source>Warnning</source>
        <translation type="obsolete">警告</translation>
    </message>
    <message>
        <source>There is no available device or no features enrolled</source>
        <translation type="obsolete">没有可用设备或者没有录入特征</translation>
    </message>
</context>
<context>
    <name>MessageDialog</name>
    <message>
        <location filename="../src/messagedialog.ui" line="26"/>
        <location filename="../ui_messagedialog.h" line="146"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/messagedialog.ui" line="215"/>
        <location filename="../ui_messagedialog.h" line="149"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/messagedialog.ui" line="231"/>
        <location filename="../ui_messagedialog.h" line="150"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
</context>
<context>
    <name>PromptDialog</name>
    <message>
        <location filename="../src/promptdialog.ui" line="26"/>
        <location filename="../ui_promptdialog.h" line="192"/>
        <source>Current Progress</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠠᠬᠢᠴᠠ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.ui" line="287"/>
        <location filename="../ui_promptdialog.h" line="198"/>
        <source>Continue</source>
        <translation>ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠨ ᠪᠢᠴᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.ui" line="300"/>
        <location filename="../ui_promptdialog.h" line="199"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="obsolete">确认</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Operations are in progress. Please wait...</source>
        <translation type="obsolete">操作中，请稍后...</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="168"/>
        <location filename="../src/promptdialog.cpp" line="169"/>
        <source>Enroll</source>
        <translation>ᠪᠢᠴᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="168"/>
        <source>feature</source>
        <translation>ᠤᠨᠴᠠᠯᠢᠭ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="172"/>
        <source>Verify</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="175"/>
        <source>Search</source>
        <translation>ᠬᠠᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="229"/>
        <location filename="../src/promptdialog.cpp" line="239"/>
        <source>In progress, please wait...</source>
        <translation>ᠬᠦᠢᠴᠡᠳᠬᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠳᠦᠷ ᠬᠦᠯᠢᠶᠡᠬᠡᠷᠡᠢ...</translation>
    </message>
    <message>
        <source>Index</source>
        <translation type="vanished">序列号</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="319"/>
        <source>UserName</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="319"/>
        <location filename="../src/promptdialog.cpp" line="321"/>
        <source>FeatureName</source>
        <translation>ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="319"/>
        <location filename="../src/promptdialog.cpp" line="321"/>
        <source>Serial number</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="385"/>
        <source>Permission is required.
Please authenticate yourself to continue</source>
        <translation>ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ ᠴᠢᠬᠤᠯᠠᠳᠠᠢ.
ᠤᠷᠢᠳᠠᠪᠠᠷ ᠪᠠᠳᠤᠯᠠᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠬᠦᠯᠦᠬᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="417"/>
        <source>Enroll successfully</source>
        <translation>ᠪᠢᠴᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="458"/>
        <source>Verify successfully</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="461"/>
        <source>Not Match</source>
        <translation>ᠠᠪᠤᠴᠠᠯᠳᠤᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="500"/>
        <source>Search Result</source>
        <translation>ᠬᠠᠢᠯᠳᠠ ᠵᠢᠨ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠨᠭ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="516"/>
        <source>No matching features Found</source>
        <translation>ᠠᠪᠤᠴᠠᠯᠳᠤᠬᠤ ᠤᠨᠴᠠᠯᠢᠭ ᠢ᠋ ᠬᠠᠢᠵᠤ ᠤᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="683"/>
        <source>Keep looking straight at the camera.</source>
        <translation>ᠳᠤᠷᠠᠨᠳᠠᠭᠤᠷ ᠢ᠋ ᠡᠭᠴᠡ ᠰᠢᠷᠳᠡᠬᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="701"/>
        <source>D-Bus calling error</source>
        <translation>D-Bus ᠵᠢ/ ᠢ᠋ ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <source>the window will be closed after two second</source>
        <translation type="vanished">窗口将在两秒后关闭</translation>
    </message>
    <message>
        <source>Device encounters an error</source>
        <translation type="obsolete">设备遇到错误</translation>
    </message>
    <message>
        <source>Operation timeout</source>
        <translation type="obsolete">操作超时</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="711"/>
        <source>Device is busy</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠠᠪ ᠦᠬᠡᠢ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="717"/>
        <source>No such device</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="723"/>
        <source>Permission denied</source>
        <translation>ᠡᠷᠬᠡ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="740"/>
        <source>Failed to enroll</source>
        <translation>ᠪᠢᠴᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="744"/>
        <source>Failed to match</source>
        <translation>ᠠᠪᠤᠴᠠᠯᠳᠤᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="747"/>
        <source>Not Found</source>
        <translation>ᠬᠠᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;2&apos;&gt;the window will be closed after two second&lt;/font&gt;</source>
        <translation type="vanished">&lt;font size=&apos;2&apos;&gt;窗口将在两秒后关闭&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/main.cpp" line="49"/>
        <source>Username</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="155"/>
        <location filename="../src/main.cpp" line="164"/>
        <source>Fatal Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="156"/>
        <source>the biometric-authentication service was not started</source>
        <translation>&apos;biometric-authentication&apos; ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠭᠰᠡᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="165"/>
        <source>API version is not compatible</source>
        <translation>API ᠬᠡᠪᠯᠡᠯ ᠵᠤᠬᠢᠴᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>FingerPrint</source>
        <translation type="obsolete">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="obsolete">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="obsolete">虹膜</translation>
    </message>
</context>
<context>
    <name>TreeModel</name>
    <message>
        <source>FingerPrint</source>
        <translation type="obsolete">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="obsolete">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="obsolete">虹膜</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="obsolete">人脸特征</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="obsolete">声纹</translation>
    </message>
    <message>
        <location filename="../src/treemodel.cpp" line="30"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../src/treemodel.cpp" line="32"/>
        <location filename="../src/treemodel.cpp" line="34"/>
        <source>index</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤ᠋ᠨ ᠨᠤᠮᠸᠷ</translation>
    </message>
    <message>
        <location filename="../src/treemodel.cpp" line="32"/>
        <source>username</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/treemodel.cpp" line="181"/>
        <source>Empty feature name</source>
        <translation>ᠬᠤᠭᠤᠰᠤᠨ ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../src/treemodel.cpp" line="186"/>
        <source>feature name is too long</source>
        <translation>ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠬᠡᠳᠦ ᠤᠷᠳᠤ</translation>
    </message>
    <message>
        <location filename="../src/treemodel.cpp" line="191"/>
        <source>Duplicate feature name</source>
        <translation>ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠳᠠᠪᠬᠤᠴᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/treemodel.cpp" line="196"/>
        <source>Please do not use spaces as the beginning or end of the feature name</source>
        <translation>ᠪᠢᠳᠡᠬᠡᠢ ᠬᠤᠭᠤᠰᠤᠨ ᠵᠠᠢ ᠵᠢᠡᠷ ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠤ᠋ᠨ ᠡᠬᠢᠯᠡᠯᠳᠡ ᠪᠤᠶᠤ ᠳᠡᠬᠦᠰᠬᠡᠯ ᠪᠤᠯᠭᠠᠭᠠᠷᠠᠢ</translation>
    </message>
</context>
</TS>
