<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>BiometricEnrollDialog</name>
    <message>
        <location filename="../biometricenroll.ui" line="26"/>
        <location filename="../ui_biometricenroll.h" line="220"/>
        <source>Dialog</source>
        <translation>ᠬᠠᠷᠢᠯᠴᠠᠬᠤ ᠴᠤᠨᠭᠬᠤ</translation>
    </message>
    <message>
        <location filename="../biometricenroll.ui" line="340"/>
        <location filename="../ui_biometricenroll.h" line="225"/>
        <source>Continue adding</source>
        <translation>ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠨ ᠪᠢᠴᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../biometricenroll.ui" line="359"/>
        <location filename="../ui_biometricenroll.h" line="226"/>
        <source>Finish</source>
        <translation>ᠳᠠᠭᠤᠰᠪᠠ</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="187"/>
        <source>FingerPrint</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ ᠤ᠋ ᠤᠷᠤᠮ</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="189"/>
        <source>Fingervein</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠨᠠᠮᠵᠢᠭᠤᠨ ᠰᠤᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="191"/>
        <source>Iris</source>
        <translation>ᠰᠤᠯᠤᠨᠭᠭ᠎ᠠ ᠪᠦᠷᠬᠦᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="193"/>
        <source>Face</source>
        <translation>ᠨᠢᠭᠤᠷ ᠴᠢᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="195"/>
        <source>VoicePrint</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ ᠢᠷᠠᠯᠵᠢ</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="205"/>
        <source>Enroll</source>
        <translation>ᠪᠢᠴᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="208"/>
        <source>Verify</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="211"/>
        <source>Search</source>
        <translation>ᠬᠠᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="240"/>
        <source>Permission is required.
Please authenticate yourself to continue</source>
        <translation>ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ ᠴᠢᠬᠤᠯᠠᠳᠠᠢ.
ᠤᠷᠢᠳᠠᠪᠠᠷ ᠪᠠᠳᠤᠯᠠᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠬᠦᠯᠦᠬᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="266"/>
        <location filename="../biometricenroll.cpp" line="451"/>
        <source>Enroll successfully</source>
        <translation>ᠪᠢᠴᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="334"/>
        <location filename="../biometricenroll.cpp" line="453"/>
        <source>Verify successfully</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="337"/>
        <source>Not Match</source>
        <translation>ᠠᠪᠤᠴᠠᠯᠳᠤᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="609"/>
        <source>Keep looking straight at the camera.</source>
        <translation>ᠳᠤᠷᠠᠨᠳᠠᠭᠤᠷ ᠢ᠋ ᠡᠭᠴᠡ ᠰᠢᠷᠳᠡᠬᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <source>Place your finger on the device button and remove. Repeat</source>
        <translation type="vanished">将手指放在设备按钮上再移开，重复此步骤</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="626"/>
        <source>D-Bus calling error</source>
        <translation>D-Bus ᠵᠢ/ ᠢ᠋ᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="635"/>
        <source>Device is busy</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠠᠪ ᠦᠬᠡᠢ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="640"/>
        <source>No such device</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="645"/>
        <source>Permission denied</source>
        <translation>ᠡᠷᠬᠡ ᠬᠦᠷᠦᠯᠴᠡᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>BiometricMoreInfoDialog</name>
    <message>
        <location filename="../biometricmoreinfo.ui" line="26"/>
        <location filename="../ui_biometricmoreinfo.h" line="205"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="76"/>
        <location filename="../ui_biometricmoreinfo.h" line="206"/>
        <source>Biometrics </source>
        <translation>ᠠᠮᠢᠳᠤ ᠪᠤᠳᠠᠰ ᠤ᠋ᠨ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ </translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="166"/>
        <location filename="../ui_biometricmoreinfo.h" line="208"/>
        <source>Default device </source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ </translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="208"/>
        <location filename="../ui_biometricmoreinfo.h" line="209"/>
        <source>Verify Type:</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ:</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="215"/>
        <location filename="../ui_biometricmoreinfo.h" line="210"/>
        <source>Bus Type:</source>
        <translation>ᠶᠡᠷᠦᠨᠭᠬᠡᠢ ᠬᠡᠯᠬᠢᠶ᠎ᠡ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ:</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="222"/>
        <location filename="../ui_biometricmoreinfo.h" line="211"/>
        <source>Device Status:</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠪᠠᠢᠳᠠᠯ:</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="243"/>
        <location filename="../ui_biometricmoreinfo.h" line="214"/>
        <source>Storage Type:</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠮᠵᠢ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ:</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="250"/>
        <location filename="../ui_biometricmoreinfo.h" line="215"/>
        <source>Identification Type:</source>
        <translation>ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠬᠤ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ:</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="77"/>
        <source>Connected</source>
        <translation>ᠨᠢᠬᠡᠨᠳᠡ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="77"/>
        <source>Unconnected</source>
        <translation>ᠳᠠᠰᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="126"/>
        <source>FingerPrint</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ ᠤ᠋ ᠤᠷᠤᠮ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="128"/>
        <source>Fingervein</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠨᠠᠮᠵᠢᠭᠤᠨ ᠰᠤᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="130"/>
        <source>Iris</source>
        <translation>ᠰᠤᠯᠤᠨᠭᠭ᠎ᠠ ᠪᠦᠷᠬᠦᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="132"/>
        <source>Face</source>
        <translation>ᠨᠢᠭᠤᠷ ᠴᠢᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="134"/>
        <source>VoicePrint</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ ᠢᠷᠠᠯᠵᠢ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="143"/>
        <source>Hardware Verification</source>
        <translation>ᠬᠠᠳᠠᠭᠤ ᠳᠤᠨᠤᠭ ᠤ᠋ᠨ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="145"/>
        <source>Software Verification</source>
        <translation>ᠵᠦᠬᠡᠯᠡᠨ ᠲᠤᠨᠤᠭ ᠤ᠋ᠨ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="147"/>
        <source>Mix Verification</source>
        <translation>ᠬᠤᠯᠢᠮᠠᠭ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="149"/>
        <source>Other Verification</source>
        <translation>ᠪᠤᠰᠤᠳ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="157"/>
        <source>Device Storage</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="159"/>
        <source>OS Storage</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="161"/>
        <source>Mix Storage</source>
        <translation>ᠬᠤᠯᠢᠮᠠᠭ ᠬᠠᠳᠠᠭᠠᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="169"/>
        <source>Serial</source>
        <translation>ᠬᠤᠯᠪᠤᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="171"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="173"/>
        <source>PCIE</source>
        <translation>PCIE</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="175"/>
        <source>Any</source>
        <translation>ᠳᠤᠷ᠎ᠠ ᠵᠢᠨ ᠨᠢᠭᠡ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="177"/>
        <source>Other</source>
        <translation>ᠪᠤᠰᠤᠳ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="185"/>
        <source>Hardware Identification</source>
        <translation>ᠬᠠᠳᠠᠭᠤ ᠲᠤᠨᠤᠭ ᠤ᠋ᠨ ᠢᠯᠭᠠᠨ ᠳᠨᠠᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="187"/>
        <source>Software Identification</source>
        <translation>ᠵᠦᠬᠡᠯᠡᠨ ᠳᠤᠨᠤᠭ ᠤ᠋ᠨ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="189"/>
        <source>Mix Identification</source>
        <translation>ᠬᠤᠯᠢᠮᠠᠭ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="191"/>
        <source>Other Identification</source>
        <translation>ᠪᠤᠰᠤᠳ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ</translation>
    </message>
</context>
<context>
    <name>Biometrics</name>
    <message>
        <source>Biometrics</source>
        <translation type="vanished">登录选项</translation>
    </message>
    <message>
        <location filename="../biometrics.cpp" line="36"/>
        <source>Login Options</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦ᠌ ᠰᠤᠩᠭᠤᠯᠳᠠ</translation>
    </message>
</context>
<context>
    <name>BiometricsWidget</name>
    <message>
        <source>Biometric password</source>
        <translation type="vanished">登录选项</translation>
    </message>
    <message>
        <source>Account password</source>
        <translation type="vanished">帐户密码</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="139"/>
        <location filename="../ui_biometricswidget.h" line="380"/>
        <source>Change password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="334"/>
        <location filename="../ui_biometricswidget.h" line="387"/>
        <source>(Fingerprint, face recognition, etc)</source>
        <translation>( ᠬᠤᠷᠤᠭᠤᠨ ᠤ᠋ ᠤᠷᠤᠮ᠂ ᠴᠢᠷᠠᠢ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ ᠵᠡᠷᠬᠡ)</translation>
    </message>
    <message>
        <source>Enable biometrics </source>
        <translation type="vanished">生物特征</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="208"/>
        <location filename="../biometricswidget.cpp" line="878"/>
        <location filename="../biometricswidget.cpp" line="944"/>
        <location filename="../ui_biometricswidget.h" line="382"/>
        <source>(Can be used to log in, unlock the system, and authorize authentication)</source>
        <translation>( ᠨᠡᠪᠳᠡᠷᠡᠬᠦ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠤᠨᠢᠰᠤ ᠵᠢᠨ ᠳᠠᠢᠯᠬᠤ ᠵᠢᠴᠢ ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠤ᠋ᠨ ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ)</translation>
    </message>
    <message>
        <source>Device Type</source>
        <translation type="vanished">设备类型</translation>
    </message>
    <message>
        <source>Device Name</source>
        <translation type="vanished">设备名</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="201"/>
        <location filename="../ui_biometricswidget.h" line="381"/>
        <source>Scan code login</source>
        <translation>ᠺᠤᠳ᠋ ᠰᠢᠷᠪᠢᠵᠤ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="59"/>
        <location filename="../ui_biometricswidget.h" line="378"/>
        <source>Login options</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠰᠤᠨᠭᠭᠤᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="113"/>
        <location filename="../ui_biometricswidget.h" line="379"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="228"/>
        <location filename="../ui_biometricswidget.h" line="383"/>
        <source>Bound wechat:</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠤᠶᠠᠭᠰᠠᠨ ᠸᠢᠴᠠᠲ:</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="257"/>
        <location filename="../ui_biometricswidget.h" line="385"/>
        <source>Bind</source>
        <translation>ᠤᠶᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="327"/>
        <location filename="../ui_biometricswidget.h" line="386"/>
        <source>Biometric</source>
        <translation>ᠠᠮᠢᠳᠤ ᠪᠤᠳᠠᠰ ᠤ᠋ᠨ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="456"/>
        <location filename="../ui_biometricswidget.h" line="389"/>
        <source>Type</source>
        <translation>ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="574"/>
        <location filename="../ui_biometricswidget.h" line="390"/>
        <source>Device</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <source>Add biometric feature</source>
        <translation type="vanished">添加生物密码</translation>
    </message>
    <message>
        <source>Disable this function</source>
        <translation type="vanished">禁用该功能</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="99"/>
        <source>Advanced Settings</source>
        <translation>ᠦᠨᠳᠦᠷ ᠵᠡᠷᠬᠡ ᠵᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="267"/>
        <source>Standard</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠳᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="269"/>
        <source>Admin</source>
        <translation>ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="271"/>
        <source>root</source>
        <translation>root</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="495"/>
        <source>(default)</source>
        <translation>( ᠠᠶᠠᠳᠠᠯ)</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="502"/>
        <source>Add </source>
        <translation>ᠨᠡᠮᠡᠬᠦ </translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="506"/>
        <location filename="../biometricswidget.cpp" line="511"/>
        <source>No available device was detected</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠤᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="929"/>
        <source>Binding WeChat</source>
        <translation>ᠸᠢᠴᠠᠲ ᠤᠶᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">删除</translation>
    </message>
    <message>
        <source>Unbound</source>
        <translation type="vanished">未绑定</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="933"/>
        <source>Unbind</source>
        <translation>ᠤᠶᠠᠭᠠᠰᠤ ᠵᠢᠨ ᠳᠠᠢᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>ChangeFeatureName</name>
    <message>
        <location filename="../changefeaturename.ui" line="26"/>
        <location filename="../ui_changefeaturename.h" line="171"/>
        <source>Change Username</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢᠨ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../changefeaturename.ui" line="65"/>
        <location filename="../ui_changefeaturename.h" line="173"/>
        <source>Change featurename</source>
        <translation>ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢᠨ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../changefeaturename.ui" line="261"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Feature name</source>
        <translation type="vanished">特征名称</translation>
    </message>
    <message>
        <location filename="../changefeaturename.ui" line="242"/>
        <location filename="../ui_changefeaturename.h" line="177"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../ui_changefeaturename.h" line="178"/>
        <source>Save</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="23"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="35"/>
        <source>Name already in use, change another one.</source>
        <translation>ᠳᠤᠰ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠵᠠᠰᠠᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="37"/>
        <source>Duplicate feature name</source>
        <translation>ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠳᠠᠪᠬᠤᠴᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="39"/>
        <source>Please do not use spaces as the beginning or end of the feature name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ ᠤ᠋ᠨ ᠡᠬᠢᠯᠡᠯᠳᠡ ᠪᠤᠶᠤ ᠳᠡᠬᠦᠰᠬᠡᠯ ᠳ᠋ᠤ᠌ ᠪᠢᠳᠡᠬᠡᠢ ᠬᠤᠭᠤᠰᠤᠨ ᠵᠠᠢ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source> rename</source>
        <translation type="vanished">重命名</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="70"/>
        <source> Rename</source>
        <translation> ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠬᠦ</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="67"/>
        <source> name</source>
        <translation> ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">重命名</translation>
    </message>
    <message>
        <source>name</source>
        <translation type="vanished">名称</translation>
    </message>
</context>
<context>
    <name>ChangePwdDialog</name>
    <message>
        <location filename="../changepwddialog.ui" line="130"/>
        <location filename="../ui_changepwddialog.h" line="356"/>
        <source>Change Pwd</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="317"/>
        <location filename="../ui_changepwddialog.h" line="360"/>
        <source>Pwd type</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="395"/>
        <location filename="../ui_changepwddialog.h" line="362"/>
        <source>Cur pwd</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="440"/>
        <location filename="../ui_changepwddialog.h" line="363"/>
        <source>New pwd</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="485"/>
        <location filename="../ui_changepwddialog.h" line="364"/>
        <source>New pwd sure</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="605"/>
        <location filename="../ui_changepwddialog.h" line="366"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="627"/>
        <location filename="../ui_changepwddialog.h" line="367"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="64"/>
        <source>Change pwd</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <source>Cur pwd checking!</source>
        <translation type="vanished">当前密码检查!</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="172"/>
        <source>General Pwd</source>
        <translation>ᠨᠡᠢᠳᠡᠮ ᠤ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="178"/>
        <location filename="../changepwddialog.cpp" line="389"/>
        <source>Current Password</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="179"/>
        <location filename="../changepwddialog.cpp" line="390"/>
        <location filename="../changepwddialog.cpp" line="398"/>
        <source>New Password</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="180"/>
        <location filename="../changepwddialog.cpp" line="391"/>
        <location filename="../changepwddialog.cpp" line="399"/>
        <source>New Password Identify</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Authentication failed, input authtok again!</source>
        <translation type="vanished">密码输入错误,重新输入!</translation>
    </message>
    <message>
        <source>Pwd input error, re-enter!</source>
        <translation type="vanished">密码输入错误,重新输入!</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="327"/>
        <source>Contains illegal characters!</source>
        <translation>ᠳᠦᠷᠢᠮ ᠳ᠋ᠤ᠌ ᠨᠡᠢᠴᠡᠬᠦ ᠦᠬᠡᠢ ᠳᠡᠮᠳᠡᠭᠡᠳ ᠠᠭᠤᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="329"/>
        <source>Same with old pwd</source>
        <translation>ᠬᠠᠭᠤᠴᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠠᠢ ᠢᠵᠢᠯ</translation>
    </message>
    <message>
        <source>Password length needs to more than %1 character!</source>
        <translation type="vanished">密码长度至少大于%1个字符！</translation>
    </message>
    <message>
        <source>Password length needs to less than %1 character!</source>
        <translation type="vanished">密码长度需要小于%1个字符！</translation>
    </message>
    <message>
        <source>Password length needs to more than 5 character!</source>
        <translation type="vanished">密码长度需要大于5个字符！</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="245"/>
        <location filename="../changepwddialog.cpp" line="365"/>
        <source>Inconsistency with pwd</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠠᠢ ᠢᠵᠢᠯ ᠪᠤᠰᠤ</translation>
    </message>
</context>
<context>
    <name>ChangeUserPwd</name>
    <message>
        <source>Change Pwd</source>
        <translation type="vanished">修改密码</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="116"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="123"/>
        <source>Change password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="136"/>
        <location filename="../changeuserpwd.cpp" line="149"/>
        <location filename="../changeuserpwd.cpp" line="555"/>
        <source>Current Pwd</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="178"/>
        <location filename="../changeuserpwd.cpp" line="189"/>
        <location filename="../changeuserpwd.cpp" line="556"/>
        <location filename="../changeuserpwd.cpp" line="569"/>
        <source>New Pwd</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="218"/>
        <location filename="../changeuserpwd.cpp" line="222"/>
        <location filename="../changeuserpwd.cpp" line="557"/>
        <location filename="../changeuserpwd.cpp" line="570"/>
        <source>Sure Pwd</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="280"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="284"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="450"/>
        <source>Authentication failed, input authtok again!</source>
        <translation>ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ᠂ ᠳᠠᠬᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="749"/>
        <source>current pwd cannot be empty!</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="754"/>
        <source>new pwd cannot be empty!</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="759"/>
        <source>sure pwd cannot be empty!</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠭᠰᠠᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="373"/>
        <location filename="../changeuserpwd.cpp" line="638"/>
        <source>Inconsistency with pwd</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠠᠢ ᠢᠵᠢᠯ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="600"/>
        <source>Contains illegal characters!</source>
        <translation>ᠳᠦᠷᠢᠮ ᠳ᠋ᠤ᠌ ᠨᠡᠢᠴᠡᠬᠦ ᠦᠬᠡᠢ ᠳᠡᠮᠳᠡᠭᠡᠳ ᠠᠭᠤᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
</context>
<context>
    <name>DeviceType</name>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="40"/>
        <source>FingerPrint</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ ᠤ᠋ ᠤᠷᠤᠮ</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="42"/>
        <source>FingerVein</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠨᠠᠮᠵᠢᠭᠤᠨ ᠰᠤᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="44"/>
        <source>Iris</source>
        <translation>ᠰᠤᠯᠤᠨᠭᠭ᠎ᠠ ᠪᠦᠷᠬᠦᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="46"/>
        <source>Face</source>
        <translation>ᠨᠢᠭᠤᠷ ᠴᠢᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="48"/>
        <source>VoicePrint</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ ᠢᠷᠠᠯᠵᠢ</translation>
    </message>
</context>
<context>
    <name>PasswdCheckUtil</name>
    <message>
        <location filename="../passwdcheckutil.cpp" line="159"/>
        <source>The password is shorter than %1 characters</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠨᠢ %1 ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ ᠳᠡᠮᠳᠡᠭ</translation>
    </message>
    <message>
        <source>The password contains less than %ld character classes</source>
        <translation type="obsolete">密码包含的字符类型少于 %1 种</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="162"/>
        <source>The password contains less than %1 character classes</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ ᠠᠭᠤᠯᠠᠭᠰᠠᠨ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠨᠢ %1 ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="165"/>
        <source>The password is the same as the old one</source>
        <translation>ᠬᠠᠭᠤᠴᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠠᠢ ᠢᠵᠢᠯ</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="168"/>
        <source>The password contains the user name in some form</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠨᠢ ᠶᠠᠮᠠᠷ ᠨᠢᠭᠡᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠵᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢᠨ ᠪᠠᠭᠳᠠᠭᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="171"/>
        <source>The password differs with case changes only</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ ᠵᠦᠪᠬᠡᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠪᠢᠴᠢᠯᠬᠡ ᠵᠢᠨ ᠬᠤᠪᠢᠷᠠᠯᠳᠠ ᠵᠢᠨ ᠪᠠᠭᠳᠠᠭᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="174"/>
        <source>The password is too similar to the old one</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠤᠯ ᠤ᠋ᠨ ᠬᠢ ᠲᠠᠢ ᠪᠡᠨ ᠳᠡᠨᠭᠳᠡᠬᠦᠦ ᠢᠵᠢᠯ</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="177"/>
        <source>The password is a palindrome</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠨᠢ ᠨᠢᠬᠡᠨ ᠳᠡᠭᠰᠢ ᠬᠡᠮᠳᠦ</translation>
    </message>
</context>
<context>
    <name>QRCodeEnrollDialog</name>
    <message>
        <location filename="../qrcodeenroll.ui" line="26"/>
        <location filename="../ui_qrcodeenroll.h" line="139"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.ui" line="102"/>
        <location filename="../qrcodeenroll.cpp" line="140"/>
        <location filename="../ui_qrcodeenroll.h" line="141"/>
        <source>Bind Wechat Account</source>
        <translation>ᠸᠢᠴᠠᠲ ᠤ᠋ᠨ ᠳᠠᠨᠭᠰᠠᠨ ᠳ᠋ᠤ᠌ ᠤᠶᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.ui" line="112"/>
        <location filename="../qrcodeenroll.cpp" line="190"/>
        <location filename="../qrcodeenroll.cpp" line="613"/>
        <location filename="../ui_qrcodeenroll.h" line="142"/>
        <source>Please use wechat scanning code for binding.</source>
        <translation>ᠸᠢᠴᠠᠲ ᠵᠢᠡᠷ ᠰᠢᠷᠪᠢᠵᠤ ᠤᠶᠠᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.ui" line="199"/>
        <location filename="../ui_qrcodeenroll.h" line="144"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="143"/>
        <source>Verify Wechat Account</source>
        <translation>ᠸᠢᠴᠠᠲ ᠤ᠋ᠨ ᠳᠠᠨᠭᠰᠠ ᠵᠢ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="146"/>
        <source>Search Wechat Account</source>
        <translation>ᠸᠢᠴᠠᠲ ᠤ᠋ᠨ ᠳᠠᠨᠭᠰᠠ ᠵᠢ ᠬᠠᠢᠬᠤ</translation>
    </message>
    <message>
        <source>Permission is required.
Please authenticate yourself to continue</source>
        <translation type="vanished">需要授权，请先进行认证以继续操作</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="212"/>
        <source>Bind Successfully</source>
        <translation>ᠤᠶᠠᠵᠤ ᠴᠢᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="247"/>
        <location filename="../qrcodeenroll.cpp" line="325"/>
        <source>Verify successfully</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="254"/>
        <source>Not Match</source>
        <translation>ᠠᠪᠤᠴᠠᠯᠳᠤᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="322"/>
        <source>The wechat account is bound successfully!</source>
        <translation>ᠸᠢᠴᠠᠲ ᠤ᠋ᠨ ᠳᠠᠨᠭᠰᠠ ᠵᠢ ᠤᠶᠠᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="477"/>
        <source>Abnormal network</source>
        <translation>ᠲᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <source>Network anomalies</source>
        <translation type="vanished">网络异常</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="490"/>
        <location filename="../qrcodeenroll.cpp" line="495"/>
        <location filename="../qrcodeenroll.cpp" line="500"/>
        <source>Binding failure</source>
        <translation>ᠤᠶᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Enroll successfully</source>
        <translation type="vanished">录入成功</translation>
    </message>
    <message>
        <source>D-Bus calling error</source>
        <translation type="obsolete">D-Bus获取错误</translation>
    </message>
    <message>
        <source>Device is busy</source>
        <translation type="vanished">设备忙</translation>
    </message>
    <message>
        <source>No such device</source>
        <translation type="vanished">设备不存在</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">权限不够</translation>
    </message>
</context>
</TS>
