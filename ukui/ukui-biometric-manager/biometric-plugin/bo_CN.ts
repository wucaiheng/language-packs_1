<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>BiometricEnrollDialog</name>
    <message>
        <location filename="../biometricenroll.ui" line="26"/>
        <location filename="../ui_biometricenroll.h" line="220"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../biometricenroll.ui" line="340"/>
        <location filename="../ui_biometricenroll.h" line="225"/>
        <source>Continue adding</source>
        <translation>མུ་མཐུད་དུ་ཁ་སྣོན།</translation>
    </message>
    <message>
        <location filename="../biometricenroll.ui" line="359"/>
        <location filename="../ui_biometricenroll.h" line="226"/>
        <source>Finish</source>
        <translation>མཇུག་སྒྲིལ།</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="187"/>
        <source>FingerPrint</source>
        <translation>མཛུབ་རིས།</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="189"/>
        <source>Fingervein</source>
        <translation>སྡོད་རྩ།</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="191"/>
        <source>Iris</source>
        <translation>འཇའ་སྐྱི།</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="193"/>
        <source>Face</source>
        <translation>ངོ་གདོང་།</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="195"/>
        <source>VoicePrint</source>
        <translation>སྒྲ་གདངས་ཀྱི་པར་གཞི།</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="205"/>
        <source>Enroll</source>
        <translation>ནང་འཇུག</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="208"/>
        <source>Verify</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="211"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="240"/>
        <source>Permission is required.
Please authenticate yourself to continue</source>
        <translation>ཆོག་མཆན་འཐོབ་དགོས།
མུ་མཐུད་དུ་རང་ཉིད་ལ་བདེན་དཔང་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="266"/>
        <location filename="../biometricenroll.cpp" line="451"/>
        <source>Enroll successfully</source>
        <translation>ནང་འཇུག་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="334"/>
        <location filename="../biometricenroll.cpp" line="453"/>
        <source>Verify successfully</source>
        <translation>ཞིབ་བཤེར་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="337"/>
        <source>Not Match</source>
        <translation>ཆ་མི་ཚང་བ།</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="609"/>
        <source>Keep looking straight at the camera.</source>
        <translation>མུ་མཐུད་དུ་ཐད་ཀར་པར་ཆས་ལ་ལྟ་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>Place your finger on the device button and remove. Repeat</source>
        <translation type="vanished">将手指放在设备按钮上再移开，重复此步骤</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="626"/>
        <source>D-Bus calling error</source>
        <translation>D-Bus ཐོབ་པ་ནོར་སོང་།</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="635"/>
        <source>Device is busy</source>
        <translation>སྒྲིག་ཆས་བྲེལ་བ་ཆེ།</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="640"/>
        <source>No such device</source>
        <translation>སྒྲིག་ཆས་དེ་རིགས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="645"/>
        <source>Permission denied</source>
        <translation>ཆོག་མཆན་མ་ཐོབ་པ།</translation>
    </message>
</context>
<context>
    <name>BiometricMoreInfoDialog</name>
    <message>
        <location filename="../biometricmoreinfo.ui" line="26"/>
        <location filename="../ui_biometricmoreinfo.h" line="205"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="76"/>
        <location filename="../ui_biometricmoreinfo.h" line="206"/>
        <source>Biometrics </source>
        <translation>སྐྱེ་དངོས་དབྱེ་འབྱེད་རིག་པ། </translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="166"/>
        <location filename="../ui_biometricmoreinfo.h" line="208"/>
        <source>Default device </source>
        <translation>དང་ཐོག་གི་དབྱེ་འབྱེད། </translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="208"/>
        <location filename="../ui_biometricmoreinfo.h" line="209"/>
        <source>Verify Type:</source>
        <translation>རིགས་དབྱིབས་ལ་ཞིབ་བཤེར་བྱ་རྒྱུ།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="215"/>
        <location filename="../ui_biometricmoreinfo.h" line="210"/>
        <source>Bus Type:</source>
        <translation>སྤྱིའི་གློག་སྐུལ་ཀྱི་རིགས།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="222"/>
        <location filename="../ui_biometricmoreinfo.h" line="211"/>
        <source>Device Status:</source>
        <translation>སྒྲིག་ཆས་ཀྱི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="243"/>
        <location filename="../ui_biometricmoreinfo.h" line="214"/>
        <source>Storage Type:</source>
        <translation>གསོག་ཉར་གྱི་རིགས་དབྱིབས་ནི།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="250"/>
        <location filename="../ui_biometricmoreinfo.h" line="215"/>
        <source>Identification Type:</source>
        <translation>ཞིབ་བཤེར་གྱི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="77"/>
        <source>Connected</source>
        <translation>འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="77"/>
        <source>Unconnected</source>
        <translation>འབྲེལ་བ་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="126"/>
        <source>FingerPrint</source>
        <translation>མཛུབ་རིས།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="128"/>
        <source>Fingervein</source>
        <translation>སྡོད་རྩ།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="130"/>
        <source>Iris</source>
        <translation>འཇའ་སྐྱི།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="132"/>
        <source>Face</source>
        <translation>ངོ་གདོང་།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="134"/>
        <source>VoicePrint</source>
        <translation>སྒྲ་གདངས་ཀྱི་པར་གཞི།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="143"/>
        <source>Hardware Verification</source>
        <translation>མཁྲེགས་ཆས་ཞིབ་བཤེར།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="145"/>
        <source>Software Verification</source>
        <translation>མཉེན་ཆས་ཞིབ་བཤེར།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="147"/>
        <source>Mix Verification</source>
        <translation>མཉམ་བསྲེས་ཞིབ་བཤེར།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="149"/>
        <source>Other Verification</source>
        <translation>ཞིབ་བཤེར་གཞན་དག</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="157"/>
        <source>Device Storage</source>
        <translation>སྒྲིག་ཆས་གསོག་ཉར།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="159"/>
        <source>OS Storage</source>
        <translation>མ་ལག་གསོག་ཉར།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="161"/>
        <source>Mix Storage</source>
        <translation>མཉམ་བསྲེས་གསོག་ཉར།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="169"/>
        <source>Serial</source>
        <translation>གོ་རིམ་ལྟར་ན།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="171"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="173"/>
        <source>PCIE</source>
        <translation>PCIE</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="175"/>
        <source>Any</source>
        <translation>གང་ཞིག་ཡིན་རུང་གི་རིགས།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="177"/>
        <source>Other</source>
        <translation>དེ་མིན།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="185"/>
        <source>Hardware Identification</source>
        <translation>མཁྲེགས་ཆས་ཀྱི་དབྱེ་འབྱེད།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="187"/>
        <source>Software Identification</source>
        <translation>མཉེན་ཆས་ཀྱི་དབྱེ་འབྱེད།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="189"/>
        <source>Mix Identification</source>
        <translation>མཉམ་བསྲེས་གསལ་འབྱེད།</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="191"/>
        <source>Other Identification</source>
        <translation>ཐོབ་ཐང་ལག་ཁྱེར་གཞན་དག</translation>
    </message>
</context>
<context>
    <name>Biometrics</name>
    <message>
        <source>Biometrics</source>
        <translation type="vanished">登录选项</translation>
    </message>
    <message>
        <location filename="../biometrics.cpp" line="36"/>
        <source>Login Options</source>
        <translation>ཐོ་འགོད་ཀྱི་བསལ་འདེམས་ཀྱི་དབང་ཆ།</translation>
    </message>
</context>
<context>
    <name>BiometricsWidget</name>
    <message>
        <source>Biometric password</source>
        <translation type="vanished">登录选项</translation>
    </message>
    <message>
        <source>Account password</source>
        <translation type="vanished">帐户密码</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="139"/>
        <location filename="../ui_biometricswidget.h" line="380"/>
        <source>Change password</source>
        <translation>གསང་གྲངས་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="334"/>
        <location filename="../ui_biometricswidget.h" line="387"/>
        <source>(Fingerprint, face recognition, etc)</source>
        <translation>(མཛུབ་རིས་དང་ངོ་གདོང་ངོས་འཛིན་སོགས་ཚུད་ཡོད། )</translation>
    </message>
    <message>
        <source>Enable biometrics </source>
        <translation type="vanished">生物特征</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="208"/>
        <location filename="../biometricswidget.cpp" line="878"/>
        <location filename="../biometricswidget.cpp" line="944"/>
        <location filename="../ui_biometricswidget.h" line="382"/>
        <source>(Can be used to log in, unlock the system, and authorize authentication)</source>
        <translation>(ཐོ་འགོད་དང་། མ་ལག་གི་སྒོ་འབྱེད། བདེན་དཔང་ར་སྤྲོད་བཅས་བྱེད་པར་སྤྱད་ཆོག)</translation>
    </message>
    <message>
        <source>Device Type</source>
        <translation type="vanished">设备类型</translation>
    </message>
    <message>
        <source>Device Name</source>
        <translation type="vanished">设备名</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="201"/>
        <location filename="../ui_biometricswidget.h" line="381"/>
        <source>Scan code login</source>
        <translation>ཞིབ་བཤེར་ཨང་གྲངས་ཐོ་འགོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="59"/>
        <location filename="../ui_biometricswidget.h" line="378"/>
        <source>Login options</source>
        <translation>ཐོ་འགོད་ཀྱི་བསལ་འདེམས་ཀྱི་དབང་ཆ།</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="113"/>
        <location filename="../ui_biometricswidget.h" line="379"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="228"/>
        <location filename="../ui_biometricswidget.h" line="383"/>
        <source>Bound wechat:</source>
        <translation>སྦྲེལ་ཟིན་པའི་སྐད་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="257"/>
        <location filename="../ui_biometricswidget.h" line="385"/>
        <source>Bind</source>
        <translation>སྦྲེལ་བ།</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="327"/>
        <location filename="../ui_biometricswidget.h" line="386"/>
        <source>Biometric</source>
        <translation>སྐྱེ་དངོས་རིག་པ་དབྱེ་འབྱེད།</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="456"/>
        <location filename="../ui_biometricswidget.h" line="389"/>
        <source>Type</source>
        <translation>རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="574"/>
        <location filename="../ui_biometricswidget.h" line="390"/>
        <source>Device</source>
        <translation>སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <source>Add biometric feature</source>
        <translation type="vanished">添加生物密码</translation>
    </message>
    <message>
        <source>Disable this function</source>
        <translation type="vanished">禁用该功能</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="99"/>
        <source>Advanced Settings</source>
        <translation>སྔོན་ཐོན་གྱི་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="267"/>
        <source>Standard</source>
        <translation>ཚད་ལྡན་གྱི་སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="269"/>
        <source>Admin</source>
        <translation>སྲིད་འཛིན་དོ་དམ།</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="271"/>
        <source>root</source>
        <translation>རྩ་བ།</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="495"/>
        <source>(default)</source>
        <translation>(སོར་བཞག)</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="502"/>
        <source>Add </source>
        <translation>ཁ་སྣོན་བརྒྱབ་པ། </translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="506"/>
        <location filename="../biometricswidget.cpp" line="511"/>
        <source>No available device was detected</source>
        <translation>ད་ཡོད་ཀྱི་སྒྲིག་ཆས་ལ་ཞིབ་བཤེར་བྱས་མེད།</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="929"/>
        <source>Binding WeChat</source>
        <translation>འཕྲིན་ཕྲན་དང་སྦྲེལ་ཟིན་པ།</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">删除</translation>
    </message>
    <message>
        <source>Unbound</source>
        <translation type="vanished">未绑定</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="933"/>
        <source>Unbind</source>
        <translation>བཀག་འགོག་མི་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>ChangeFeatureName</name>
    <message>
        <location filename="../changefeaturename.ui" line="26"/>
        <location filename="../ui_changefeaturename.h" line="171"/>
        <source>Change Username</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་བསྒྱུར་དགོས།</translation>
    </message>
    <message>
        <location filename="../changefeaturename.ui" line="65"/>
        <location filename="../ui_changefeaturename.h" line="173"/>
        <source>Change featurename</source>
        <translation>ཁྱད་ཆོས་ཀྱི་མིང་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../changefeaturename.ui" line="261"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Feature name</source>
        <translation type="vanished">特征名称</translation>
    </message>
    <message>
        <location filename="../changefeaturename.ui" line="242"/>
        <location filename="../ui_changefeaturename.h" line="177"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui_changefeaturename.h" line="178"/>
        <source>Save</source>
        <translation>གསོག་ཉར།</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="23"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="35"/>
        <source>Name already in use, change another one.</source>
        <translation>བཀོལ་སྤྱོད་བྱེད་བཞིན་པའི་མིང་ལ་མིང་གཞན་ཞིག་བརྗེ་དགོས།</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="37"/>
        <source>Duplicate feature name</source>
        <translation>བསྐྱར་ཟློས་ཀྱི་ཁྱད་ཆོས་ལྡན་པའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="39"/>
        <source>Please do not use spaces as the beginning or end of the feature name</source>
        <translation>ཁྱད་ཆོས་ལྡན་པའི་མིང་གི་འགོ་རྩོམ་པའམ་ཡང་ན་མཇུག་རྫོགས་པའི་བར་སྟོང་བེད་སྤྱོད་མ་བྱེད།</translation>
    </message>
    <message>
        <source> rename</source>
        <translation type="vanished">重命名</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="70"/>
        <source> Rename</source>
        <translation> མིང་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="67"/>
        <source> name</source>
        <translation> མིང་།</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">重命名</translation>
    </message>
    <message>
        <source>name</source>
        <translation type="vanished">名称</translation>
    </message>
</context>
<context>
    <name>ChangePwdDialog</name>
    <message>
        <location filename="../changepwddialog.ui" line="130"/>
        <location filename="../ui_changepwddialog.h" line="356"/>
        <source>Change Pwd</source>
        <translation>གསང་གྲངས་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="317"/>
        <location filename="../ui_changepwddialog.h" line="360"/>
        <source>Pwd type</source>
        <translation>གསང་གྲངས་རིགས།</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="395"/>
        <location filename="../ui_changepwddialog.h" line="362"/>
        <source>Cur pwd</source>
        <translation>མིག་སྡའི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="440"/>
        <location filename="../ui_changepwddialog.h" line="363"/>
        <source>New pwd</source>
        <translation>གསང་གྲངས་གསར་པ།</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="485"/>
        <location filename="../ui_changepwddialog.h" line="364"/>
        <source>New pwd sure</source>
        <translation>གསང་གྲངས་གསར་བ་གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="605"/>
        <location filename="../ui_changepwddialog.h" line="366"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="627"/>
        <location filename="../ui_changepwddialog.h" line="367"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="64"/>
        <source>Change pwd</source>
        <translation>གསང་གྲངས་བརྗེ་བ།</translation>
    </message>
    <message>
        <source>Cur pwd checking!</source>
        <translation type="vanished">当前密码检查!</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="172"/>
        <source>General Pwd</source>
        <translation>སྤྱི་སྤྱོད་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="178"/>
        <location filename="../changepwddialog.cpp" line="389"/>
        <source>Current Password</source>
        <translation>མིག་སྔའི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="179"/>
        <location filename="../changepwddialog.cpp" line="390"/>
        <location filename="../changepwddialog.cpp" line="398"/>
        <source>New Password</source>
        <translation>གསང་གྲངས་གསར་པ།</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="180"/>
        <location filename="../changepwddialog.cpp" line="391"/>
        <location filename="../changepwddialog.cpp" line="399"/>
        <source>New Password Identify</source>
        <translation>གསང་གྲངས་གསར་པ་གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>Authentication failed, input authtok again!</source>
        <translation type="vanished">密码输入错误,重新输入!</translation>
    </message>
    <message>
        <source>Pwd input error, re-enter!</source>
        <translation type="vanished">密码输入错误,重新输入!</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="327"/>
        <source>Contains illegal characters!</source>
        <translation>དེའི་ནང་དུ་ཁྲིམས་འགལ་གྱི་ཡིག་རྟགས་འདུས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="329"/>
        <source>Same with old pwd</source>
        <translation>གསང་གྲངས་རྙིང་བ་དང་འདྲ།</translation>
    </message>
    <message>
        <source>Password length needs to more than %1 character!</source>
        <translation type="vanished">密码长度至少大于%1个字符！</translation>
    </message>
    <message>
        <source>Password length needs to less than %1 character!</source>
        <translation type="vanished">密码长度需要小于%1个字符！</translation>
    </message>
    <message>
        <source>Password length needs to more than 5 character!</source>
        <translation type="vanished">密码长度需要大于5个字符！</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="245"/>
        <location filename="../changepwddialog.cpp" line="365"/>
        <source>Inconsistency with pwd</source>
        <translation>གསང་གྲངས་གསར་བ་དང་གཅིག་མཐུན་མིན་པ།</translation>
    </message>
</context>
<context>
    <name>ChangeUserPwd</name>
    <message>
        <source>Change Pwd</source>
        <translation type="vanished">修改密码</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="116"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="123"/>
        <source>Change password</source>
        <translation>གསང་གྲངས་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="136"/>
        <location filename="../changeuserpwd.cpp" line="149"/>
        <location filename="../changeuserpwd.cpp" line="555"/>
        <source>Current Pwd</source>
        <translation>མིག་སྔའི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="178"/>
        <location filename="../changeuserpwd.cpp" line="189"/>
        <location filename="../changeuserpwd.cpp" line="556"/>
        <location filename="../changeuserpwd.cpp" line="569"/>
        <source>New Pwd</source>
        <translation>གསང་གྲངས་གསར་པ།</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="218"/>
        <location filename="../changeuserpwd.cpp" line="222"/>
        <location filename="../changeuserpwd.cpp" line="557"/>
        <location filename="../changeuserpwd.cpp" line="570"/>
        <source>Sure Pwd</source>
        <translation>གཏན་འཁེལ་བྱས་པའི་གསང་གྲངས་གསར་བ།</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="280"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="284"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="450"/>
        <source>Authentication failed, input authtok again!</source>
        <translation>བདེན་དཔང་ར་སྤྲོད་བྱེད་མ་ཐུབ་པར་ཡང་བསྐྱར་ནང་འཇུག་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="749"/>
        <source>current pwd cannot be empty!</source>
        <translation>ད་ལྟའི་གསང་གྲངས་ནི་སྟོང་པ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="754"/>
        <source>new pwd cannot be empty!</source>
        <translation>གསང་གྲངས་གསར་བ་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="759"/>
        <source>sure pwd cannot be empty!</source>
        <translation>གཏན་འཁེལ་བྱས་པའི་གསང་གྲངས་ནི་སྟོང་བ་ཡིན་མི་སྲིད་པ།</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="373"/>
        <location filename="../changeuserpwd.cpp" line="638"/>
        <source>Inconsistency with pwd</source>
        <translation>གསང་གྲངས་གསར་བ་དང་གཅིག་མཐུན་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="600"/>
        <source>Contains illegal characters!</source>
        <translation>དེའི་ནང་དུ་ཁྲིམས་འགལ་གྱི་ཡིག་རྟགས་འདུས་ཡོད།</translation>
    </message>
</context>
<context>
    <name>DeviceType</name>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="40"/>
        <source>FingerPrint</source>
        <translation>མཛུབ་རིས།</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="42"/>
        <source>FingerVein</source>
        <translation>སྡོད་རྩ།</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="44"/>
        <source>Iris</source>
        <translation>འཇའ་སྐྱི།</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="46"/>
        <source>Face</source>
        <translation>ངོ་གདོང་།</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="48"/>
        <source>VoicePrint</source>
        <translation>སྒྲ་གདངས་ཀྱི་པར་གཞི།</translation>
    </message>
</context>
<context>
    <name>PasswdCheckUtil</name>
    <message>
        <location filename="../passwdcheckutil.cpp" line="159"/>
        <source>The password is shorter than %1 characters</source>
        <translation>གསང་གྲངས་ནི་%1 ཡི་གེ་ལས་ཐུང་བ་རེད།</translation>
    </message>
    <message>
        <source>The password contains less than %ld character classes</source>
        <translation type="obsolete">密码包含的字符类型少于 %1 种</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="162"/>
        <source>The password contains less than %1 character classes</source>
        <translation>གསང་གྲངས་འདིའི་ནང་དུ་%1མན་གྱི་ཡི་གེའི་འཛིན་གྲྭ་འདུས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="165"/>
        <source>The password is the same as the old one</source>
        <translation>གསང་གྲངས་དང་གསང་གྲངས་རྙིང་བ་གཉིས་གཅིག་མཚུངས་ཡིན།</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="168"/>
        <source>The password contains the user name in some form</source>
        <translation>གསང་གྲངས་ནང་དུ་རྣམ་པ་ག་གེ་མོ་ཞིག་གི་ཐོག་ནས་སྤྱོད་མཁན་གྱི་མིང་འདུས་པ།</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="171"/>
        <source>The password differs with case changes only</source>
        <translation>གསང་གྲངས་ལ་ཆེ་ཆུང་གི་འགྱུར་བ་མ་གཏོགས་མེད།</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="174"/>
        <source>The password is too similar to the old one</source>
        <translation>གསང་གྲངས་དང་གསང་གྲངས་རྙིང་བ་གཉིས་ཧ་ཅང་འདྲ་མཚུངས་ཡིན།</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="177"/>
        <source>The password is a palindrome</source>
        <translation>གསང་གྲངས་ནི་ཡིག་ལན་ཞིག་རེད།</translation>
    </message>
</context>
<context>
    <name>QRCodeEnrollDialog</name>
    <message>
        <location filename="../qrcodeenroll.ui" line="26"/>
        <location filename="../ui_qrcodeenroll.h" line="139"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.ui" line="102"/>
        <location filename="../qrcodeenroll.cpp" line="140"/>
        <location filename="../ui_qrcodeenroll.h" line="141"/>
        <source>Bind Wechat Account</source>
        <translation>སྐད་འཕྲིན་ཨང་གྲངས་དང་སྦྲེལ།</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.ui" line="112"/>
        <location filename="../qrcodeenroll.cpp" line="190"/>
        <location filename="../qrcodeenroll.cpp" line="613"/>
        <location filename="../ui_qrcodeenroll.h" line="142"/>
        <source>Please use wechat scanning code for binding.</source>
        <translation>ཁྱེད་ཀྱིས་འཕྲིན་ཕྲན་གྱི་ཞིབ་བཤེར་ཨང་གྲངས་བཀོལ་ནས་ཚོད་འཛིན་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.ui" line="199"/>
        <location filename="../ui_qrcodeenroll.h" line="144"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="143"/>
        <source>Verify Wechat Account</source>
        <translation>འཕྲིན་ཕྲན་གྱི་རྩིས་ཐོར་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="146"/>
        <source>Search Wechat Account</source>
        <translation>འཕྲིན་ཕྲན་གྱི་རྩིས་ཐོའི་ཨང་གྲངས་འཚོལ་ཞིབ་བྱེད།</translation>
    </message>
    <message>
        <source>Permission is required.
Please authenticate yourself to continue</source>
        <translation type="vanished">需要授权，请先进行认证以继续操作</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="212"/>
        <source>Bind Successfully</source>
        <translation>ཚོད་འཛིན་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="247"/>
        <location filename="../qrcodeenroll.cpp" line="325"/>
        <source>Verify successfully</source>
        <translation>ཞིབ་བཤེར་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="254"/>
        <source>Not Match</source>
        <translation>ཆ་མི་ཚང་བ།</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="322"/>
        <source>The wechat account is bound successfully!</source>
        <translation>འཕྲིན་ཕྲན་གྱི་རྩིས་ཐོའི་ཨང་གྲངས་བདེ་བླག་ངང་བཀག་སྡོམ་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="477"/>
        <source>Abnormal network</source>
        <translation>རྒྱུན་ལྡན་མིན་པའི་དྲ་རྒྱ།</translation>
    </message>
    <message>
        <source>Network anomalies</source>
        <translation type="vanished">网络异常</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="490"/>
        <location filename="../qrcodeenroll.cpp" line="495"/>
        <location filename="../qrcodeenroll.cpp" line="500"/>
        <source>Binding failure</source>
        <translation>ཚོད་འཛིན་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <source>Enroll successfully</source>
        <translation type="vanished">录入成功</translation>
    </message>
    <message>
        <source>D-Bus calling error</source>
        <translation type="obsolete">D-Bus获取错误</translation>
    </message>
    <message>
        <source>Device is busy</source>
        <translation type="vanished">设备忙</translation>
    </message>
    <message>
        <source>No such device</source>
        <translation type="vanished">设备不存在</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">权限不够</translation>
    </message>
</context>
</TS>
