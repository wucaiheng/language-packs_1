<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>NetUsershareHelper</name>
    <message>
        <source>Peony-Qt-Share-Extension</source>
        <translation type="vanished">共享</translation>
    </message>
</context>
<context>
    <name>Peony::SharePropertiesPagePlugin</name>
    <message>
        <location filename="../share-properties-page-plugin.h" line="44"/>
        <source>Peony Qt Share Extension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../share-properties-page-plugin.h" line="45"/>
        <source>Allow user share folders</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SharePage</name>
    <message>
        <location filename="../share-page.cpp" line="169"/>
        <source>Share folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Share:</source>
        <translation type="vanished">共享:</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="140"/>
        <source>usershare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="144"/>
        <location filename="../share-page.cpp" line="187"/>
        <source>share this folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="144"/>
        <location filename="../share-page.cpp" line="194"/>
        <source>don`t share this folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="220"/>
        <source>Share name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="225"/>
        <source>Read Only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="228"/>
        <source>Allow Anonymous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="221"/>
        <source>Comment:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
