<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>NetUsershareHelper</name>
    <message>
        <source>Peony-Qt-Share-Extension</source>
        <translation type="vanished">共享</translation>
    </message>
</context>
<context>
    <name>Peony::SharePropertiesPagePlugin</name>
    <message>
        <location filename="../share-properties-page-plugin.h" line="44"/>
        <source>Peony Qt Share Extension</source>
        <translation>མཉམ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../share-properties-page-plugin.h" line="45"/>
        <source>Allow user share folders</source>
        <translation>ཡིག་ཁུག་མཉམ་སྤྱོད་བྱེད་ཆོག</translation>
    </message>
</context>
<context>
    <name>SharePage</name>
    <message>
        <location filename="../share-page.cpp" line="169"/>
        <source>Share folder</source>
        <translation>ཡིག་ཁུག་མཉམ་སྤྱོད།</translation>
    </message>
    <message>
        <source>Share:</source>
        <translation type="vanished">共享:</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="140"/>
        <source>usershare</source>
        <translation>གློག་ཀླད་འདིའི་མཉམ་སྤྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="144"/>
        <location filename="../share-page.cpp" line="187"/>
        <source>share this folder</source>
        <translation>ཡིག་ཁུག་འདི་མཉམ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="144"/>
        <location filename="../share-page.cpp" line="194"/>
        <source>don`t share this folder</source>
        <translation>ཡིག་ཁུག་འདི་མཉམ་སྤྱོད་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="220"/>
        <source>Share name:</source>
        <translation>མཉམ་སྤྱོད་ཀྱི་མིང་།:</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="225"/>
        <source>Read Only</source>
        <translation>ཀློག་ཙམ།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="228"/>
        <source>Allow Anonymous</source>
        <translation>ཡུལ་སྐོར་བར་ལྟ་སྤྱོད་བྱས་ཆོག</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="221"/>
        <source>Comment:</source>
        <translation>མཆན་འགྲེལ།:</translation>
    </message>
</context>
</TS>
