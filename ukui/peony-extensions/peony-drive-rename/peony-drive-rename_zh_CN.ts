<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>Peony::DriveRename</name>
    <message>
        <source>drive rename</source>
        <translation type="vanished">设备重命名</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="165"/>
        <location filename="../drive-rename.cpp" line="177"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="177"/>
        <source>Device name:</source>
        <translation>设备名称：</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="181"/>
        <location filename="../drive-rename.cpp" line="189"/>
        <location filename="../drive-rename.cpp" line="195"/>
        <location filename="../drive-rename.cpp" line="208"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="181"/>
        <source>Renaming cannot start with a decimal point, Please re-enter!</source>
        <translation>重命名不能以小数点开始，请重新输入!</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="189"/>
        <source>The device name exceeds the character limit, rename failed!</source>
        <translation>设备名称超过了字符限制，重命名失败!</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="195"/>
        <source>Renaming will unmount the device. Do you want to continue?</source>
        <translation>重命名将会卸载设备，是否继续？</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="208"/>
        <source>The device may not support the rename operation, rename failed!</source>
        <translation>可能设备不支持重命名操作，重命名失败！</translation>
    </message>
    <message>
        <source>Failed to rename!</source>
        <translation type="vanished">重命名失败！</translation>
    </message>
</context>
<context>
    <name>Peony::DriveRenamePlugin</name>
    <message>
        <location filename="../driverenameplugin.h" line="21"/>
        <source>drive rename</source>
        <translation>设备重命名</translation>
    </message>
</context>
<context>
    <name>Peony::DriverAction</name>
    <message>
        <source>Send to a removable device</source>
        <translation type="vanished">发送到移动设备</translation>
    </message>
</context>
<context>
    <name>Peony::SendToPlugin</name>
    <message>
        <source>Send to a removable device</source>
        <translation type="vanished">发送到移动设备</translation>
    </message>
</context>
</TS>
