<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Peony::MateTerminalMenuPlugin</name>
    <message>
        <location filename="../mate-terminal-menu-plugin.cpp" line="133"/>
        <location filename="../mate-terminal-menu-plugin.cpp" line="152"/>
        <source>Open Directory in Terminal</source>
        <translation>མཐའ་སྣེ་ཁ་འབྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../mate-terminal-menu-plugin.h" line="43"/>
        <source>Peony-Qt Mate Terminal Menu Extension</source>
        <translation>UKUIཡིག་ཆ་དོ་དམ་ཆས་མཐའ་སྣེ་རྒྱ་བསྐྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../mate-terminal-menu-plugin.h" line="44"/>
        <source>Open Terminal with menu.</source>
        <translation>གཡས་མཐེབ་ཀྱི་འདེམས་བྱང་བཀོལ་སྤྱོད་བྱས་ནས་མཐའ་སྣེའི་ཁ་འབྱེད་པ།</translation>
    </message>
    <message>
        <source>Open Directory in T&amp;erminal</source>
        <translation type="vanished">打开终端(&amp;E)</translation>
    </message>
</context>
</TS>
