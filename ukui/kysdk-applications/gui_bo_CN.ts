<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>kdk::KAboutDialog</name>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="164"/>
        <location filename="../src/kaboutdialog.cpp" line="176"/>
        <source>Service &amp; Support: </source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར་ཚོགས་པ།</translation>
    </message>
</context>
<context>
    <name>kdk::KInputDialog</name>
    <message>
        <location filename="../src/kinputdialog.cpp" line="206"/>
        <source>Enter a value:</source>
        <translation>ཁྱོད་ཀྱིས་གྲངས་ཀ་ནང་འཇུག་གནང་རོགས།</translation>
    </message>
</context>
<context>
    <name>kdk::KInputDialogPrivate</name>
    <message>
        <location filename="../src/kinputdialog.cpp" line="212"/>
        <source>ok</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../src/kinputdialog.cpp" line="214"/>
        <source>cancel</source>
        <translation>མེད་པར་བཟོ་དགོས།</translation>
    </message>
</context>
<context>
    <name>kdk::KMenuButton</name>
    <message>
        <location filename="../src/kmenubutton.cpp" line="39"/>
        <source>more</source>
        <translation>དེ་བས་ཀྱང་མང་།</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="40"/>
        <source>Setting</source>
        <translation>བཀོད་སྒྲིག་བཅས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="41"/>
        <source>Theme</source>
        <translation>བརྗོད་བྱ་གཙོ་བོ།</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="42"/>
        <source>Assist</source>
        <translation>རོགས་རམ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="43"/>
        <source>About</source>
        <translation>དེའི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="44"/>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="51"/>
        <source>Auto</source>
        <translation>བརྗོད་བྱ་གཙོ་བོའི་རྗེས་སུ་འབྲང་བ།</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="53"/>
        <source>Light</source>
        <translation>བརྗོད་བྱ་གཙོ་བོ་གཏིང་ཟབ་པོ།</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="55"/>
        <source>Dark</source>
        <translation>བརྗོད་བྱ་གཙོ་བོ་གཏིང་ཟབ་པོ།</translation>
    </message>
</context>
<context>
    <name>kdk::KProgressDialog</name>
    <message>
        <location filename="../src/kprogressdialog.cpp" line="47"/>
        <source>cancel</source>
        <translation>མེད་པར་བཟོ་དགོས།</translation>
    </message>
</context>
<context>
    <name>kdk::KSearchLineEditPrivate</name>
    <message>
        <location filename="../src/ksearchlineedit.cpp" line="285"/>
        <source>search</source>
        <translation>འཚོལ་ཞིབ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/ksearchlineedit.cpp" line="374"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>kdk::KSecurityLevelBar</name>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="38"/>
        <source>Low</source>
        <translation>དམའ་བ།</translation>
    </message>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="41"/>
        <source>Medium</source>
        <translation>འབྲིང་བ།</translation>
    </message>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="44"/>
        <source>High</source>
        <translation>མཐོན་པོ།</translation>
    </message>
</context>
<context>
    <name>kdk::KSecurityLevelBarPrivate</name>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="112"/>
        <source>Low</source>
        <translation>དམའ་བ།</translation>
    </message>
</context>
<context>
    <name>kdk::KUninstallDialog</name>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="102"/>
        <location filename="../src/kuninstalldialog.cpp" line="194"/>
        <source>uninstall</source>
        <translation>འཕྲལ་མར་ལེན་པ།</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="163"/>
        <source>deb name:</source>
        <translation>འགན་གཙང་ལེན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="168"/>
        <source>deb version:</source>
        <translation>པར་གཞི།</translation>
    </message>
</context>
<context>
    <name>kdk::KUninstallDialogPrivate</name>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="284"/>
        <source>deb name:</source>
        <translation>འགན་གཙང་ལེན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="288"/>
        <source>deb version:</source>
        <translation>པར་གཞི།</translation>
    </message>
</context>
<context>
    <name>kdk::KWindowButtonBarPrivate</name>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="162"/>
        <source>minimize</source>
        <translation>ཆེས་ཆུང་བ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="168"/>
        <source>maximize</source>
        <translation>ཆེས་ཆེ་བ་འགྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="173"/>
        <source>close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
</TS>
