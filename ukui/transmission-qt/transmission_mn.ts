<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../AboutDialog.ui" line="+14"/>
        <source>About Transmission</source>
        <translation>BT ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠪᠠᠭᠠᠵᠢ ᠵᠢᠨ ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../AboutDialog.cc" line="+29"/>
        <source>&lt;b style=&apos;font-size:x-large&apos;&gt;Transmission %1&lt;/b&gt;</source>
        <translation>&lt;b style=&apos;font-size:x-large&apos;&gt;BT ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠪᠠᠭᠠᠵᠢ %1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../AboutDialog.ui" line="+26"/>
        <source>A fast and easy BitTorrent client</source>
        <translation>ᠳᠦᠷᠬᠡᠨ ᠪᠦᠭᠡᠳ ᠳᠦᠬᠦᠮ BitTorrent ᠦᠵᠦᠬᠦᠷᠯᠢᠭ</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Copyright (c) The Transmission Project</source>
        <translation>Copyright (c) The BT ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠪᠠᠭᠠᠵᠢ Project</translation>
    </message>
    <message>
        <location filename="../AboutDialog.cc" line="+4"/>
        <source>C&amp;redits</source>
        <translation>ᠳᠠᠯᠠᠷᠬᠠᠯ(&amp;R)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;License</source>
        <translation>ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠬᠡᠯᠡᠯᠴᠡᠬᠡᠷ(&amp;L)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Credits</source>
        <translation>ᠳᠠᠯᠠᠷᠬᠠᠯ</translation>
    </message>
</context>
<context>
    <name>Application</name>
    <message>
        <location filename="../Application.cc" line="+281"/>
        <source>&lt;b&gt;Transmission is a file sharing program.&lt;/b&gt;</source>
        <translation>&lt;b&gt;BT ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠪᠠᠭᠠᠵᠢ ᠪᠤᠯᠤ ᠨᠢᠭᠡᠨ ᠹᠠᠢᠯ ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠫᠡᠷᠦᠭᠷᠡᠮ.&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>When you run a torrent, its data will be made available to others by means of upload. Any content you share is your sole responsibility.</source>
        <translation>ᠲᠠ Torrent， ᠵᠢ/ ᠢ᠋ ᠠᠵᠢᠯᠯᠠᠭᠤᠯᠬᠤ ᠦᠶᠡᠰ ᠳᠡᠬᠦᠨ ᠤ᠋ ᠲᠤᠭ᠎ᠠ ᠪᠠᠷᠢᠮᠳᠠ ᠢ᠋ ᠪᠤᠰᠤᠳ ᠬᠦᠮᠦᠨ ᠳ᠋ᠤ᠌ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠠᠷᠭ᠎ᠠ ᠵᠢ ᠬᠠᠨᠭᠭᠠᠨ᠎ᠠ᠃ ᠲᠠ ᠦᠪᠡᠷ ᠤ᠋ᠨ ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠠᠭᠤᠯᠭ᠎ᠠ ᠳ᠋ᠤ᠌ ᠴᠤᠷᠢ ᠵᠢᠨ ᠭᠠᠭᠴᠠ ᠬᠠᠷᠢᠭᠤᠴᠠᠯᠭ᠎ᠠ ᠡᠬᠦᠷᠬᠡᠯᠡᠨ᠎ᠡ.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>I &amp;Agree</source>
        <translation>ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠨ᠎ᠡ(&amp;A)</translation>
    </message>
    <message>
        <location line="+85"/>
        <source>Torrent Completed</source>
        <translation>Torrent ᠵᠢ/ ᠢ᠋ ᠨᠢᠭᠡᠨᠳᠡ ᠳᠠᠭᠤᠰᠬᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Torrent Added</source>
        <translation>Torrent ᠵᠢ/ ᠢ᠋ ᠨᠢᠭᠡᠨᠳᠡ ᠨᠡᠮᠡᠪᠡ</translation>
    </message>
</context>
<context>
    <name>DetailsDialog</name>
    <message>
        <location filename="../DetailsDialog.cc" line="+364"/>
        <source>None</source>
        <translation>ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mixed</source>
        <translation>ᠬᠤᠯᠢᠮᠠᠭ</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+218"/>
        <source>Unknown</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="-179"/>
        <source>Finished</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠳᠠᠭᠤᠰᠬᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Paused</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠵᠤᠭᠰᠤᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+204"/>
        <source>Active now</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠬᠦᠳᠡᠯᠬᠡᠬᠡᠨ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 ago</source>
        <translation>%1 ᠡᠴᠡ ᠡᠮᠦᠨ᠎ᠡ</translation>
    </message>
    <message numerus="yes">
        <location line="+51"/>
        <source>%1 (%Ln pieces @ %2)</source>
        <translation>
            <numerusform>%1 (%Ln pieces @ %2)</numerusform>
            <numerusform>%1 (%Ln ᠬᠡᠰᠡᠭ @ %2)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%1 (%Ln pieces)</source>
        <translation>
            <numerusform>%1 (%Ln ᠬᠡᠰᠡᠭ)</numerusform>
            <numerusform>%1 (%Ln ᠬᠡᠰᠡᠭ)</numerusform>
        </translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Private to this tracker -- DHT and PEX disabled</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠳ᠋ᠤ᠌ Tracker ᠵᠢᠨ/ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ ᠨᠢ ᠬᠤᠪᠢ ᠵᠢᠨ-- DHT 和 PEX ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠵᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Public torrent</source>
        <translation>ᠢᠯᠡ ᠪᠤᠯᠭᠠᠭᠰᠠᠨ Torrent</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Created by %1</source>
        <translation>%1 ᠵᠢᠡᠷ/ ᠪᠡᠷ ᠦᠢᠯᠡᠳᠬᠦ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Created on %1</source>
        <translation>%1 ᠳ᠋ᠤ᠌/ ᠲᠤ᠌ ᠦᠢᠯᠡᠳᠬᠦ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Created by %1 on %2</source>
        <translation>%1 ᠡᠴᠡ%2 ᠳ᠋ᠤ᠌/ ᠲᠤ᠌ ᠦᠢᠯᠡᠳᠬᠦ</translation>
    </message>
    <message>
        <location line="+123"/>
        <location line="+23"/>
        <source>Encrypted connection</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠨᠡᠮᠡᠵᠤ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Optimistic unchoke</source>
        <translation>ᠤᠨᠢᠰᠤ ᠳᠠᠢᠯᠬᠤ ᠳ᠋ᠤ᠌ ᠬᠢᠯᠪᠠᠷ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Downloading from this peer</source>
        <translation>ᠶᠠᠭ ᠳᠤᠰ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠡᠴᠡ ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>We would download from this peer if they would let us</source>
        <translation>ᠳᠡᠳᠡ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠪᠡᠯ᠂ ᠪᠢᠳᠡ ᠳᠤᠰ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠡᠴᠡ ᠪᠠᠭᠤᠯᠭᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Uploading to peer</source>
        <translation>ᠶᠠᠭ ᠳᠤᠰ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠳ᠋ᠤ᠌ ᠳᠠᠮᠵᠢᠭᠤᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>We would upload to this peer if they asked</source>
        <translation>ᠳᠡᠳᠡ ᠭᠤᠶᠤᠴᠢᠯᠠᠪᠠᠯ᠂ ᠪᠢᠳᠡ ᠳᠤᠰ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠳ᠋ᠤ᠌ ᠳᠠᠮᠵᠢᠭᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Peer has unchoked us, but we&apos;re not interested</source>
        <translation>ᠳᠤᠰ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠤᠨᠢᠰᠤ ᠪᠡᠨ ᠳᠠᠢᠯᠤᠭᠰᠠᠨ᠂ ᠬᠡᠪᠡᠴᠤ ᠪᠢᠳᠡ ᠰᠤᠨᠢᠷᠬᠠᠯ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>We unchoked this peer, but they&apos;re not interested</source>
        <translation>ᠪᠢᠳᠡ ᠳᠤᠰ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠤᠨᠢᠰᠤ ᠵᠢ ᠨᠢᠬᠡᠨᠳᠡ ᠳᠠᠢᠯᠤᠭᠰᠠᠨ᠂ ᠬᠡᠪᠡᠴᠤ ᠳᠡᠳᠡ ᠰᠤᠨᠢᠷᠬᠠᠯ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Peer was discovered through DHT</source>
        <translation>DHT ᠪᠡᠷ/ ᠵᠢᠡᠷ ᠳᠠᠮᠵᠢᠨ ᠢᠯᠡᠷᠡᠬᠦᠯᠦᠭᠰᠡᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Peer was discovered through Peer Exchange (PEX)</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠰᠤᠯᠢᠯᠴᠠᠭ᠎ᠠ (PEX) ᠪᠡᠷ ᠳᠠᠮᠵᠢᠨ ᠢᠯᠡᠷᠡᠬᠦᠯᠦᠭᠰᠡᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Peer is an incoming connection</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠪᠤᠯ ᠦᠷᠳᠡᠭᠡ ᠳ᠋ᠤ᠌ ᠤᠷᠤᠬᠤ ᠬᠤᠯᠪᠤᠭᠠᠰᠤ</translation>
    </message>
    <message numerus="yes">
        <location line="+120"/>
        <source> minute(s)</source>
        <extracomment>Spin box suffix, &quot;Stop seeding if idle for: [ 5 minutes ]&quot; (includes leading space after the number, if needed)</extracomment>
        <translation>
            <numerusform> ᠮᠢᠨᠦ᠋ᠲ</numerusform>
            <numerusform> ᠮᠢᠨᠦ᠋ᠲ</numerusform>
        </translation>
    </message>
    <message>
        <location line="+45"/>
        <location line="+12"/>
        <location line="+34"/>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location line="-34"/>
        <source>Tracker already exists.</source>
        <translation>Tracker ᠨᠢᠭᠡᠨᠳᠡ ᠤᠷᠤᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="-628"/>
        <source>%1 (100%)</source>
        <extracomment>Text following the &quot;Have:&quot; label in torrent properties dialog; %1 is amount of downloaded and verified data</extracomment>
        <translation>%1 (100%)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1 of %2 (%3%)</source>
        <extracomment>Text following the &quot;Have:&quot; label in torrent properties dialog; %1 is amount of downloaded and verified data, %2 is overall size of torrent data, %3 is percentage (%1/%2*100)</extracomment>
        <translation>%1 of %2 (%3%)</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1 of %2 (%3%), %4 Unverified</source>
        <extracomment>Text following the &quot;Have:&quot; label in torrent properties dialog; %1 is amount of downloaded data (both verified and unverified), %2 is overall size of torrent data, %3 is percentage (%1/%2*100), %4 is amount of downloaded but not yet verified data</extracomment>
        <translation>%1 of %2 (%3%), %4 ᠪᠠᠢᠴᠠᠭᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>%1 (%2 corrupt)</source>
        <translation>%1 (%2 ᠡᠪᠳᠡᠷᠡᠭᠰᠡᠨ)</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>%1 (Ratio: %2)</source>
        <translation>%1 (ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠤᠷᠮ᠎ᠠ: %2)</translation>
    </message>
    <message>
        <location line="+220"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location line="+156"/>
        <source>Peer is connected over uTP</source>
        <translation>uTP ᠬᠤᠯᠪᠤᠭᠠᠰᠤ ᠪᠡᠷ ᠳᠠᠮᠵᠢᠭᠰᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location line="+155"/>
        <source>Add URL </source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠨᠡᠮᠡᠬᠦ </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add tracker announce URL:</source>
        <translation>Tracker ᠵᠠᠷᠯᠠᠯ ᠤ᠋ᠨ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠨᠡᠮᠡᠬᠦ:</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+46"/>
        <source>Invalid URL &quot;%1&quot;</source>
        <translation>ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠦᠬᠡᠢ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ&quot;%1&quot;</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Edit URL </source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢ ᠨᠠᠢᠷᠠᠭᠤᠯᠬᠤ </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit tracker announce URL:</source>
        <translation>Tracker ᠵᠠᠷᠯᠠᠯ ᠤ᠋ᠨ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢ ᠨᠠᠢᠷᠠᠭᠤᠯᠬᠤ:</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>High</source>
        <translation>ᠦᠨᠳᠦᠷ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Normal</source>
        <translation>ᠡᠨᠭ ᠤ᠋ᠨ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Low</source>
        <translation>ᠳᠤᠤᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+4"/>
        <source>Use Global Settings</source>
        <translation>ᠪᠦᠬᠦ ᠳᠠᠯ᠎ᠠ ᠵᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>Seed regardless of ratio</source>
        <translation>ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠤᠷᠮ᠎ᠠ ᠬᠡᠳᠦᠢ ᠪᠠᠢᠬᠤ ᠵᠢ ᠦᠯᠦ ᠬᠠᠢᠬᠤᠷᠤᠨ ᠪᠦᠷ ᠦᠷ᠎ᠡ ᠪᠤᠯᠭᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop seeding at ratio:</source>
        <translation>ᠦᠷ᠎ᠡ ᠪᠤᠯᠬᠤ ᠵᠢ ᠵᠤᠭᠰᠤᠭᠠᠵᠤ ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠤᠷᠮ᠎ᠠ ᠵᠢ ᠬᠦᠷᠬᠡᠬᠦ ᠨᠢ:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Seed regardless of activity</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠡᠨ ᠤ᠋ ᠪᠠᠢᠳᠠᠯ ᠶᠠᠮᠠᠷ ᠪᠠᠢᠬᠤ ᠵᠢ ᠦᠯᠦ ᠬᠠᠢᠬᠤᠷᠤᠨ ᠪᠦᠷ ᠦᠷ᠎ᠡ ᠪᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop seeding if idle for:</source>
        <translation>ᠦᠷ᠎ᠡ ᠪᠤᠯᠬᠤ ᠵᠢ ᠵᠤᠭᠰᠤᠭᠠᠵᠤ ᠴᠢᠯᠦᠬᠡ ᠵᠠᠢ ᠵᠢ ᠬᠦᠷᠬᠡᠬᠦ ᠨᠢ:</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Up</source>
        <translation>ᠠᠴᠢᠶᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Down</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Status</source>
        <translation>ᠪᠠᠢᠳᠠᠯ ᠳᠦᠯᠦᠪ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address</source>
        <translation>ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Client</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠵᠦᠬᠦᠷᠯᠢᠭ</translation>
    </message>
    <message>
        <location filename="../DetailsDialog.ui" line="+14"/>
        <source>Torrent Properties</source>
        <translation>Torrent ᠵᠢᠨ/ ᠤ᠋ᠨ ᠰᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Information</source>
        <translation>ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Activity</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠡᠨ</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Have:</source>
        <translation>ᠨᠢᠬᠡᠨᠳᠡ ᠪᠠᠢᠬᠤ:</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Availability:</source>
        <translation>ᠡᠷᠡᠬᠦᠯ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ:</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Uploaded:</source>
        <translation>ᠨᠢᠬᠡᠨᠳᠡ ᠠᠴᠢᠶᠠᠯᠠᠭᠰᠠᠨ:</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Downloaded:</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠪᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ:</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>State:</source>
        <translation>ᠳᠦᠯᠦᠪ ᠪᠠᠢᠳᠠᠯ:</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Running time:</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠭᠠᠨ ᠤ᠋ ᠴᠠᠭ:</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Remaining time:</source>
        <translation>ᠦᠯᠡᠳᠡᠭᠰᠡᠨ ᠴᠠᠭ:</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Last activity:</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠤᠳᠠᠭᠠᠨ ᠤ᠋ ᠬᠦᠳᠡᠯᠬᠡᠬᠡᠨ:</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Error:</source>
        <translation>ᠪᠤᠷᠤᠭᠤ:</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>Details</source>
        <translation>ᠳᠡᠯᠭᠡᠷᠡᠩᠭᠦᠢ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Size:</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Location:</source>
        <translation>ᠪᠠᠢᠷᠢ:</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>Hash:</source>
        <translation>ᠳᠠᠷᠠᠬᠠᠢ:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Privacy:</source>
        <translation>ᠨᠢᠭᠤᠴᠠ:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Origin:</source>
        <translation>ᠢᠷᠡᠯᠳᠡ:</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Comment:</source>
        <translation>ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ:</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Peers</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Tracker</source>
        <translation>Tracker</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Add Tracker</source>
        <translation>Tracker ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Edit Tracker</source>
        <translation>Tracker ᠨᠠᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Remove Trackers</source>
        <translation>Tracker ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Show &amp;more details</source>
        <translation>ᠨᠡᠨᠭ ᠤᠯᠠᠨ ᠳᠡᠯᠭᠡᠷᠡᠨᠭᠬᠦᠢ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show &amp;backup trackers</source>
        <translation>ᠨᠦᠬᠡᠴᠡ Tracker ᠵᠢ/ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Files</source>
        <translation>ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Options</source>
        <translation>ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Speed</source>
        <translation>ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Honor global &amp;limits</source>
        <translation>ᠪᠦᠬᠦ ᠳᠠᠯ᠎ᠠ ᠵᠢᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ ᠢ᠋ ᠵᠢᠷᠤᠮᠯᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Limit &amp;download speed:</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠬᠤᠷᠳᠤᠴᠠ ᠵᠢ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠬᠤ:</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Limit &amp;upload speed:</source>
        <translation>ᠠᠴᠢᠶᠠᠯᠠᠬᠤ ᠬᠤᠷᠳᠤᠴᠠ ᠵᠢ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠬᠤ:</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Torrent &amp;priority:</source>
        <translation>Torrent ᠳᠡᠷᠢᠬᠦᠯᠡᠬᠦ ᠳᠡᠰ:</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Seeding Limits</source>
        <translation>ᠦᠷ᠎ᠡ ᠪᠤᠯᠬᠤ ᠵᠢ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Ratio:</source>
        <translation>ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠤᠷᠮ᠎ᠠ(&amp;R):</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Idle:</source>
        <translation>ᠰᠤᠯᠠ ᠰᠡᠯᠡᠬᠦᠦ(&amp;R):</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Peer Connections</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Maximum peers:</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠲᠤᠭ᠎ᠠ(&amp;M):</translation>
    </message>
</context>
<context>
    <name>FileAdded</name>
    <message>
        <location filename="../Session.cc" line="+94"/>
        <source>Add Torrent</source>
        <translation>Torrent ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;p&gt;&lt;b&gt;Unable to add &quot;%1&quot;.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;It is a duplicate of &quot;%2&quot; which is already added.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt; ᠨᠡᠮᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ &quot;%1&quot;。&lt;/b&gt;&lt;/p&gt;&lt;p&gt; ᠳᠡᠷᠡ ᠨᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠨᠡᠮᠡᠭᠰᠡᠨ &quot;%2&quot; ᠲᠠᠢ ᠳᠠᠪᠬᠤᠴᠠᠪᠠ。&lt;/p&gt;</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+12"/>
        <source>Error Adding Torrent</source>
        <translation>Torrent ᠵᠢ/ ᠢ᠋ ᠨᠡᠮᠡᠭᠰᠡᠨ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Invalid Or Corrupt Torrent File</source>
        <translatorcomment>无效或损坏的种子文件</translatorcomment>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠡᠪᠳᠡᠷᠡᠭᠰᠡᠨ ᠦᠷ᠎ᠡ ᠵᠢᠨ ᠹᠠᠢᠯ</translation>
    </message>
</context>
<context>
    <name>FileTreeItem</name>
    <message>
        <location filename="../FileTreeItem.cc" line="+271"/>
        <location filename="../FileTreeView.cc" line="+105"/>
        <location line="+257"/>
        <source>Low</source>
        <translation>ᠳᠤᠤᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../FileTreeView.cc" line="-256"/>
        <location line="+254"/>
        <source>High</source>
        <translation>ᠦᠨᠳᠦᠷ</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../FileTreeView.cc" line="-255"/>
        <location line="+256"/>
        <source>Normal</source>
        <translation>ᠡᠨᠭ ᠤ᠋ᠨ</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../FileTreeView.cc" line="-255"/>
        <source>Mixed</source>
        <translation>ᠬᠤᠯᠢᠮᠠᠭ</translation>
    </message>
</context>
<context>
    <name>FileTreeModel</name>
    <message>
        <location filename="../FileTreeModel.cc" line="+196"/>
        <source>File</source>
        <translation>ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Size</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Progress</source>
        <translation>ᠠᠬᠢᠴᠠ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Download</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Priority</source>
        <translation>ᠳᠡᠷᠢᠬᠦᠯᠡᠬᠦ ᠳᠡᠰ</translation>
    </message>
</context>
<context>
    <name>FileTreeView</name>
    <message>
        <location filename="../FileTreeView.cc" line="+247"/>
        <source>Check Selected</source>
        <translation>ᠭᠤᠬᠠᠳᠠᠵᠤ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Uncheck Selected</source>
        <translation>ᠭᠤᠬᠠᠳᠠᠭᠰᠠᠨ ᠰᠤᠩᠭᠤᠯᠳᠠ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Only Check Selected</source>
        <translation>ᠵᠦᠪᠬᠡᠨ ᠭᠤᠬᠠᠳᠠᠵᠤ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Priority</source>
        <translation>ᠳᠡᠷᠢᠬᠦᠯᠡᠬᠦ ᠳᠡᠰ</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rename...</source>
        <translation>ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠬᠦ...</translation>
    </message>
</context>
<context>
    <name>FilterBar</name>
    <message>
        <location filename="../FilterBar.cc" line="+61"/>
        <location line="+143"/>
        <source>All</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>Active</source>
        <translation>ᠬᠦᠳᠡᠯᠭᠡᠭᠡᠨ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Downloading</source>
        <translation>ᠶᠠᠭ ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Seeding</source>
        <translation>ᠶᠠᠭ ᠦᠷ᠎ᠡ ᠪᠤᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Paused</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠲᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Finished</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠳᠠᠭᠤᠰᠪᠠ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Verifying</source>
        <translation>ᠶᠠᠭ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location line="+141"/>
        <source>Show:</source>
        <translation>ᠢᠯᠡᠷᠡᠬᠦ:</translation>
    </message>
</context>
<context>
    <name>FilterBarLineEdit</name>
    <message>
        <location filename="../FilterBarLineEdit.cc" line="+48"/>
        <source>Search...</source>
        <translation>ᠬᠠᠢᠯᠳᠠ...</translation>
    </message>
</context>
<context>
    <name>Formatter</name>
    <message>
        <location filename="../Formatter.cc" line="+33"/>
        <source>B/s</source>
        <translation>B/s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>kB/s</source>
        <translation>kB/s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>MB/s</source>
        <translation>MB/s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>GB/s</source>
        <translation>GB/s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>TB/s</source>
        <translation>TB/s</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+12"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>kB</source>
        <translation>kB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>MiB</source>
        <translation>MiB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>GiB</source>
        <translation>GiB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>TiB</source>
        <translation>TiB</translation>
    </message>
    <message>
        <location line="+32"/>
        <location line="+14"/>
        <source>Unknown</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="-11"/>
        <location line="+14"/>
        <source>None</source>
        <translation>ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+20"/>
        <location line="+8"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message numerus="yes">
        <location line="+32"/>
        <source>%Ln day(s)</source>
        <translation>
            <numerusform>%Ln ᠡᠳᠦᠷ</numerusform>
            <numerusform>%Ln ᠡᠳᠦᠷ</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%Ln hour(s)</source>
        <translation>
            <numerusform>%Ln ᠴᠠᠭ</numerusform>
            <numerusform>%Ln ᠴᠠᠭ ᠤ᠋ᠳ</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%Ln minute(s)</source>
        <translation>
            <numerusform>%Ln ᠮᠢᠨᠦ᠋ᠲ</numerusform>
            <numerusform>%Ln ᠮᠢᠨᠦ᠋ᠲ ᠤ᠋ᠳ</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%Ln second(s)</source>
        <translation>
            <numerusform>%Ln ᠰᠸᠺᠦᠨ᠋ᠲ</numerusform>
            <numerusform>%Ln ᠰᠸᠺᠦᠨ᠋ᠲ ᠤ᠋ᠳ</numerusform>
        </translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+7"/>
        <location line="+7"/>
        <source>%1, %2</source>
        <translation>%1, %2</translation>
    </message>
</context>
<context>
    <name>FreeSpaceLabel</name>
    <message>
        <location filename="../FreeSpaceLabel.cc" line="+58"/>
        <source>&lt;i&gt;Calculating Free Space...&lt;/i&gt;</source>
        <translation>&lt;i&gt; ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠤᠷᠤᠨ ᠵᠠᠢ ᠵᠢ ᠶᠠᠭ ᠪᠤᠳᠤᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...&lt;/i&gt;</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>%1 free</source>
        <translation>%1 ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>LicenseDialog</name>
    <message>
        <location filename="../LicenseDialog.ui" line="+14"/>
        <source>License</source>
        <translation>ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠬᠡᠯᠡᠯᠴᠡᠬᠡᠷ</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="+14"/>
        <location filename="../MainWindow.cc" line="+645"/>
        <source>Transmission</source>
        <translation>BT ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠪᠠᠭᠠᠵᠢ</translation>
    </message>
    <message>
        <location line="+175"/>
        <source>&amp;Torrent</source>
        <translation>Torrent(&amp;)</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Edit</source>
        <translation>ᠨᠠᠢᠷᠠᠭᠤᠯᠬᠤ(&amp;E)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Help</source>
        <translation>ᠬᠠᠪᠰᠤᠷᠤᠯᠭ᠎ᠠ（&amp;H）</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;View</source>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠨ ᠦᠵᠡᠬᠦ(&amp;V)</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>&amp;File</source>
        <translation>ᠹᠠᠢᠯ(&amp;F)</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>&amp;New...</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ(&amp;N)...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Create a new torrent</source>
        <translation>ᠰᠢᠨ᠎ᠡ Torrent ᠦᠢᠯᠡᠳᠬᠦ</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Properties</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠳᠤ ᠰᠢᠨᠵᠢ(&amp;P)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show torrent properties</source>
        <translation>Torrent ᠵᠢᠨ/ ᠤ᠋ᠨ ᠬᠠᠷᠢᠶᠠᠳᠤ ᠰᠢᠨᠵᠢ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Open the torrent&apos;s folder</source>
        <translation>Torrent ᠤ᠋ᠨ/ ᠵᠢᠨ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠢᠳᠠᠰᠤ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="-161"/>
        <source>Queue</source>
        <translation>ᠡᠩᠨᠡᠭᠡ</translation>
    </message>
    <message>
        <location line="+122"/>
        <source>&amp;Open...</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ(&amp;O)...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open a torrent</source>
        <translation>ᠨᠢᠭᠡTorrent ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Open Fold&amp;er</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠢᠳᠠᠰᠤ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ(&amp;E)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Start</source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦ(&amp;S)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Start torrent</source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦ Torrent</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Ask Tracker for &amp;More Peers</source>
        <translation>Torrent ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠬᠠᠨᠳᠤᠨ ᠨᠡᠩ ᠤᠯᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠭᠤᠶᠤᠴᠢᠯᠠᠬᠤ(&amp;M)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ask tracker for more peers</source>
        <translation>Torrent ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠬᠠᠨᠳᠤᠨ ᠨᠡᠩ ᠤᠯᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠭᠤᠶᠤᠴᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Pause</source>
        <translation>ᠳᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ(&amp;P)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Pause torrent</source>
        <translation>Torrent ᠵᠢ/ ᠢ᠋ ᠳᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Verify Local Data</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤ᠋ᠨ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ(&amp;V)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Verify local data</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤ᠋ᠨ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Remove</source>
        <translation>ᠤᠰᠠᠳᠬᠠᠬᠤ(&amp;R)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove torrent</source>
        <translation>Torrent ᠵᠢ/ ᠢ᠋ ᠤᠰᠠᠳᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Delete Files and Remove</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠰᠤᠵᠤ(&amp;D) ᠵᠢ/ ᠢ᠋ ᠤᠰᠠᠳᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove torrent and delete its files</source>
        <translation>Torrent ᠵᠢ/ ᠢ᠋ ᠤᠰᠠᠳᠬᠠᠵᠤ ᠳᠡᠬᠦᠨ ᠤ᠋ ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Start All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ(&amp;S)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Pause All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠳᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ(&amp;P)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Quit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ(&amp;Q)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Select All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ(&amp;S)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Deselect All</source>
        <translation>ᠪᠦᠬᠦ ᠰᠤᠩᠭᠤᠯᠳᠠ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ(&amp;D)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Preferences</source>
        <translation>ᠳᠡᠷᠢᠬᠦᠨ ᠤ᠋ ᠰᠤᠩᠭᠤᠯᠳᠠ(&amp;P)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Compact View</source>
        <translation>ᠠᠪᠤᠴᠠᠯᠳᠤᠩᠭᠤᠢ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ(&amp;C)</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+3"/>
        <source>Compact View</source>
        <translation>ᠠᠪᠤᠴᠠᠯᠳᠤᠩᠭᠤᠢ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Toolbar</source>
        <translation>ᠪᠠᠭᠠᠵᠢ ᠵᠢᠨ ᠪᠠᠭᠠᠷ(&amp;T)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Filterbar</source>
        <translation>ᠱᠦᠬᠦᠯᠳᠡ ᠵᠢᠨ ᠪᠠᠭᠠᠷ(&amp;F)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Statusbar</source>
        <translation>ᠪᠠᠢᠳᠠᠯ ᠤ᠋ᠨ ᠪᠠᠭᠠᠷ(&amp;S)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by &amp;Activity</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠡᠨ ᠵᠢᠡᠷ ᠵᠢᠭᠰᠠᠭᠠᠬᠤ(&amp;A)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by A&amp;ge</source>
        <translation>ᠤᠷᠤᠰᠢᠬᠤ ᠴᠠᠭ ᠵᠢᠡᠷ ᠵᠢᠭᠰᠠᠭᠠᠬᠤ(&amp;G)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by Time &amp;Left</source>
        <translation>ᠦᠯᠡᠳᠡᠵᠤ ᠪᠤᠢ ᠴᠠᠭ ᠵᠢᠡᠷ ᠵᠢᠭᠰᠠᠭᠠᠬᠤ(&amp;L)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by &amp;Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ ᠵᠢᠡᠷ ᠵᠢᠭᠰᠠᠭᠠᠬᠤ(&amp;N)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by &amp;Progress</source>
        <translation>ᠠᠬᠢᠴᠠ ᠪᠡᠷ ᠵᠢᠭᠰᠠᠭᠠᠬᠤ(&amp;P)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by Rati&amp;o</source>
        <translation>ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠤᠷᠮ᠎ᠠ ᠪᠡᠷ ᠵᠢᠭᠰᠠᠭᠠᠬᠤ(&amp;O)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by Si&amp;ze</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠪᠡᠷ ᠵᠢᠭᠰᠠᠭᠠᠬᠤ(&amp;Z)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by Stat&amp;e</source>
        <translation>ᠪᠠᠢᠳᠠᠯ ᠳᠦᠯᠦᠪ ᠵᠢᠡᠷ ᠵᠢᠭᠰᠠᠭᠠᠬᠤ(&amp;E)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by T&amp;racker</source>
        <translation>Tracker ᠵᠢᠡᠷ/ ᠪᠡᠷ ᠵᠢᠭᠰᠠᠭᠠᠬᠤ(&amp;R)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Message &amp;Log</source>
        <translation>ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠵᠢᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ(&amp;L)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Statistics</source>
        <translation>ᠲᠤᠭ᠎ᠠ ᠪᠦᠷᠢᠳᠬᠡᠯ(&amp;S)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Contents</source>
        <translation>ᠠᠭᠤᠯᠭ᠎ᠠ(&amp;C)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;About</source>
        <translation>ᠲᠤᠬᠠᠢ(&amp;A)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Re&amp;verse Sort Order</source>
        <translation>ᠳᠤᠩᠭᠤᠷᠤᠤ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ(&amp;V)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ(&amp;N)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Files</source>
        <translation>ᠹᠠᠢᠯ(&amp;F)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Tracker</source>
        <translation>Tracker(&amp;T)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Total Ratio</source>
        <translation>ᠶᠡᠷᠦᠩᠬᠡᠢ ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠤᠷᠮ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Session Ratio</source>
        <translation>ᠶᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠵᠢ ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠤᠷᠮ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Total Transfer</source>
        <translation>ᠶᠡᠷᠦᠩᠬᠡᠢ ᠳᠠᠮᠵᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Session Transfer</source>
        <translation>ᠶᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠵᠢ ᠳᠠᠮᠵᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Main Window</source>
        <translation>ᠭᠤᠤᠯ ᠴᠤᠩᠬᠤ(&amp;M)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Tray &amp;Icon</source>
        <translation>ᠲᠠᠪᠠᠭ ᠤ᠋ᠨ ᠢᠺᠦᠨ ᠵᠢᠷᠤᠭ(&amp;I)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Change Session...</source>
        <translation>ᠶᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠵᠢ ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠬᠦ(&amp;C)...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose Session</source>
        <extracomment>Start a local session or connect to a running session</extracomment>
        <translation>ᠶᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠵᠢ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Set &amp;Location...</source>
        <translation>ᠪᠠᠢᠷᠢ ᠵᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ(&amp;L)...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Copy Magnet Link to Clipboard</source>
        <translation>ᠰᠤᠷᠢᠨᠵᠢᠨ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢ ᠨᠠᠭᠠᠳᠠᠭ ᠲᠤ᠌ ᠺᠤᠫᠢᠳᠠᠪᠠ(&amp;C)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open &amp;URL...</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ(&amp;U)...</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Donate</source>
        <translation>ᠬᠠᠨᠳᠢᠪᠯᠠᠬᠤ(&amp;D)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Start &amp;Now</source>
        <translation>ᠳᠤᠤᠷ᠎ᠠ ᠨᠢ ᠡᠬᠢᠯᠡᠬᠦ(&amp;N)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Bypass the queue and start now</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠵᠢ ᠳᠤᠭᠤᠷᠢᠵᠤ ᠭᠠᠷᠤᠭᠠᠳ ᠳᠠᠷᠤᠢᠬᠠᠨ ᠡᠬᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Move to &amp;Top</source>
        <translation>ᠤᠷᠤᠢ ᠳ᠋ᠤ᠌ ᠰᠢᠯᠵᠢᠬᠦᠯᠬᠦ(&amp;T)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Move &amp;Up</source>
        <translation>ᠳᠡᠭᠡᠭᠰᠢ ᠰᠢᠯᠵᠢᠬᠦᠯᠬᠦ(&amp;U)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Move &amp;Down</source>
        <translation>ᠳᠤᠷᠤᠭᠰᠢ ᠰᠢᠯᠵᠢᠬᠦᠯᠬᠦ(&amp;D)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Move to &amp;Bottom</source>
        <translation>ᠤᠯᠠᠨ ᠳ᠋ᠤ᠌ ᠰᠢᠯᠵᠢᠬᠦᠯᠬᠦ(&amp;B)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by &amp;Queue</source>
        <translation>ᠡᠩᠨᠡᠬᠡ ᠪᠡᠷ ᠵᠢᠭᠰᠠᠭᠠᠬᠤ(&amp;Q)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cc" line="-211"/>
        <source>Limit Download Speed</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠬᠤᠷᠳᠤᠴᠠ ᠵᠢ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="-54"/>
        <source>Unlimited</source>
        <translation>ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+641"/>
        <location line="+8"/>
        <source>Limited at %1</source>
        <translation>%1 ᠳ᠋ᠤ᠌/ ᠲᠤ᠌ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="-599"/>
        <source>Limit Upload Speed</source>
        <translation>ᠠᠴᠢᠶᠠᠯᠠᠬᠤ ᠬᠤᠷᠳᠤᠴᠠ ᠵᠢ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Stop Seeding at Ratio</source>
        <translation>ᠦᠷ᠎ᠡ ᠪᠤᠯᠬᠤ ᠵᠢ ᠵᠤᠭᠰᠤᠭᠠᠵᠤ ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠤᠷᠮ᠎ᠠ ᠵᠢ ᠬᠦᠷᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Seed Forever</source>
        <translation>ᠦᠷᠬᠦᠯᠵᠢ ᠦᠷ᠎ᠡ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+627"/>
        <source>Stop at Ratio (%1)</source>
        <translation>ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠤᠷᠮ᠎ᠠ ᠵᠢ ᠵᠤᠭᠰᠤᠭᠠᠭᠠᠳ (%1) ᠬᠦᠷᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="-393"/>
        <source> - %1:%2</source>
        <extracomment>Second (optional) part of main window title &quot;Transmission - host:port&quot; (added when connected to remote session); notice that leading space (before the dash) is included here</extracomment>
        <translation> - %1:%2</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Idle</source>
        <translation>ᠰᠤᠯᠠ ᠰᠡᠯᠡᠬᠦᠦ</translation>
    </message>
    <message>
        <location line="+35"/>
        <location line="+17"/>
        <source>Ratio: %1</source>
        <translation>ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠤᠷᠮ᠎ᠠ: %1</translation>
    </message>
    <message>
        <location line="-12"/>
        <location line="+6"/>
        <source>Down: %1, Up: %2</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ: %1, ᠠᠴᠢᠶᠠᠯᠠᠬᠤ: %2</translation>
    </message>
    <message>
        <location line="+421"/>
        <source>Torrent Files (*.torrent);;All Files (*.*)</source>
        <translation>Torrent ᠹᠠᠢᠯ (*.torrent);; ᠪᠦᠬᠦ ᠹᠠᠢᠯ (*.*)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show &amp;options dialog</source>
        <translation>ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ ᠤ᠋ ᠬᠠᠷᠢᠯᠴᠠᠬᠤ ᠴᠤᠩᠬᠤ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ(&amp;O)</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>Open Torrent</source>
        <translation>Torrent ᠵᠢ/ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="-918"/>
        <source>Speed Limits</source>
        <translation>ᠬᠤᠷᠳᠤᠴᠠ ᠵᠢᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ</translation>
    </message>
    <message>
        <location line="+449"/>
        <source>Network Error</source>
        <translation>ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location line="+433"/>
        <source>Click to disable Temporary Speed Limits
 (%1 down, %2 up)</source>
        <translation>ᠳᠦᠷ ᠴᠠᠭ ᠤ᠋ᠨ ᠬᠤᠷᠳᠤᠴᠠ ᠵᠢᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ ᠢ᠋ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠵᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠬᠤ
(%1 ᠪᠠᠭᠤᠯᠭᠠᠬᠤ, %2 ᠠᠴᠢᠶᠠᠯᠠᠬᠤ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Click to enable Temporary Speed Limits
 (%1 down, %2 up)</source>
        <translation>ᠳᠦᠷ ᠴᠠᠭ ᠤ᠋ᠨ ᠬᠤᠷᠳᠤᠴᠠ ᠵᠢᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ ᠢ᠋ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠵᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠬᠤ
(%1 ᠪᠠᠭᠤᠯᠭᠠᠬᠤ, %2 ᠠᠴᠢᠶᠠᠯᠠᠬᠤ)</translation>
    </message>
    <message>
        <location line="+133"/>
        <source>Remove torrent?</source>
        <translation>Torrent ᠵᠢ/ ᠢ᠋ ᠤᠰᠠᠳᠬᠠᠬᠤᠤᠤ?</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Delete this torrent&apos;s downloaded files?</source>
        <translation>ᠡᠨᠡᠬᠦ Torren ᠤ᠋ᠨ/ ᠵᠢᠨ ᠨᠢᠭᠡᠨᠳᠡ ᠪᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
    <message numerus="yes">
        <location line="-5"/>
        <source>Remove %Ln torrent(s)?</source>
        <translation>
            <numerusform>%Ln Torrent ᠢ᠋/ ᠵᠢ ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</numerusform>
            <numerusform>%Ln Torrent ᠤ᠋ᠳ ᠢ᠋ ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="-497"/>
        <source>Showing %L1 of %Ln torrent(s)</source>
        <translation>
            <numerusform>L1 / %Ln Torrent ᠢ᠋/ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</numerusform>
            <numerusform>L1 / %Ln Torrent ᠤ᠋ᠳ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+503"/>
        <source>Delete these %Ln torrent(s)&apos; downloaded files?</source>
        <translation>
            <numerusform>%Ln Torrent ᠤ᠋ᠨ/ ᠵᠢᠨ ᠪᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</numerusform>
            <numerusform>%Ln Torrent ᠤ᠋ᠳ ᠤ᠋ᠨ ᠪᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</numerusform>
        </translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Once removed, continuing the transfer will require the torrent file or magnet link.</source>
        <translation>ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠬᠠᠰᠤᠭᠰᠠᠭᠠᠷ ᠳᠤᠰ ᠳᠠᠮᠵᠢᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠬᠦᠯᠬᠦ ᠳ᠋ᠤ᠌ Torrent ᠹᠠᠢᠯ ᠪᠤᠶᠤ ᠰᠤᠷᠢᠨᠵᠢᠨ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢ ᠱᠠᠭᠠᠷᠳᠠᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Once removed, continuing the transfers will require the torrent files or magnet links.</source>
        <translation>ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠬᠠᠰᠤᠭᠰᠠᠭᠠᠷ ᠳᠤᠰ ᠳᠠᠮᠵᠢᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠬᠦᠯᠬᠦ ᠳ᠋ᠤ᠌ Torrent ᠹᠠᠢᠯ ᠪᠤᠶᠤ ᠰᠤᠷᠢᠨᠵᠢᠨ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢ ᠱᠠᠭᠠᠷᠳᠠᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This torrent has not finished downloading.</source>
        <translation>ᠲᠤᠰ Torrent ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠳᠠᠭᠤᠰᠤᠭ᠎ᠠ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>These torrents have not finished downloading.</source>
        <translation>ᠡᠳᠡᠭᠡᠷ Torrent ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠳᠠᠭᠤᠰᠤᠭ᠎ᠠ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This torrent is connected to peers.</source>
        <translation>ᠲᠤᠰ Torrent ᠨᠢᠬᠡᠨᠳᠡ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠲᠠᠢ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>These torrents are connected to peers.</source>
        <translation>ᠡᠳᠡᠭᠡᠷ Torrent ᠨᠢᠬᠡᠨᠳᠡ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠲᠠᠢ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>One of these torrents is connected to peers.</source>
        <translation>ᠡᠬᠦᠨ ᠤ᠋ ᠳᠤᠳᠤᠷᠠᠬᠢ ᠨᠢᠭᠡ Torrent ᠨᠢᠬᠡᠨᠳᠡ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠲᠠᠢ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Some of these torrents are connected to peers.</source>
        <translation>ᠡᠬᠦᠨ ᠤ᠋ ᠳᠤᠳᠤᠷᠠᠬᠢ ᠵᠠᠷᠢᠮ Torrent ᠨᠢᠬᠡᠨᠳᠡ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠲᠠᠢ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>One of these torrents has not finished downloading.</source>
        <translation>ᠡᠬᠦᠨ ᠤ᠋ ᠳᠤᠳᠤᠷᠠᠬᠢ ᠨᠢᠭᠡ Torrent ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠳᠠᠭᠤᠰᠤᠭ᠎ᠠ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Some of these torrents have not finished downloading.</source>
        <translation>ᠡᠬᠦᠨ ᠤ᠋ ᠳᠤᠳᠤᠷᠠᠬᠢ ᠵᠠᠷᠢᠮ Torrent ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠳᠠᠭᠤᠰᠤᠭ᠎ᠠ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>%1 has not responded yet</source>
        <translation>%1 ᠬᠠᠷᠠᠬᠠᠨ ᠠᠶᠠᠳᠠᠯ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1 is responding</source>
        <translation>%1 ᠶᠠᠭ ᠠᠶᠠᠳᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 last responded %2 ago</source>
        <translation>%1 ᠵᠢᠨ ᠳᠡᠭᠡᠷ᠎ᠡ ᠤᠳᠠᠭᠠᠨ ᠤ᠋ ᠠᠶᠠᠳᠠᠯ%2 ᠡᠴᠡ ᠡᠮᠦᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 is not responding</source>
        <translation>%1 ᠠᠶᠠᠳᠠᠯ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>MakeDialog</name>
    <message>
        <location filename="../MakeDialog.ui" line="+17"/>
        <source>New Torrent</source>
        <translation>ᠰᠢᠨ᠎ᠡ Torrent</translation>
    </message>
    <message>
        <location filename="../MakeDialog.cc" line="+201"/>
        <source>&lt;i&gt;No source selected&lt;i&gt;</source>
        <translation>&lt;i&gt; ᠢᠷᠡᠯᠳᠡ ᠵᠢ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ &lt;i&gt;</translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%Ln File(s)</source>
        <translation>
            <numerusform>%Ln ᠹᠠᠢᠯ</numerusform>
            <numerusform>%Ln ᠹᠠᠢᠯ ᠤ᠋ᠳ</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%Ln Piece(s)</source>
        <translation>
            <numerusform>%Ln ᠲᠤᠭᠤᠷᠢᠭ ᠤ᠋ᠨ ᠬᠡᠰᠡᠭ</numerusform>
            <numerusform>%Ln ᠲᠤᠭᠤᠷᠢᠭ ᠤ᠋ᠨ ᠬᠡᠰᠡᠭ ᠤ᠋ᠳ</numerusform>
        </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 in %2; %3 @ %4</source>
        <translation>%1 in %2; %3 @ %4</translation>
    </message>
    <message>
        <location filename="../MakeDialog.ui" line="+9"/>
        <source>Files</source>
        <translation>ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Sa&amp;ve to:</source>
        <translation>(&amp;V) ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Source f&amp;older:</source>
        <translation>ᠢᠷᠡᠯᠳᠡ ᠵᠢᠨ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠢᠳᠠᠰᠤ(&amp;O):</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Source &amp;file:</source>
        <translation>ᠢᠷᠡᠯᠳᠡ ᠵᠢᠨ ᠹᠠᠢᠯ(&amp;F):</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Properties</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠳᠤ ᠴᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Trackers:</source>
        <translation>Tracker(&amp;T):</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>To add a backup URL, add it on the line after the primary URL.
To add another primary URL, add it after a blank line.</source>
        <translation>ᠨᠢᠭᠡ ᠨᠦᠭᠡᠴᠡ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠨᠡᠮᠡᠬᠦ ᠳ᠋ᠤ᠌᠂ ᠳᠡᠬᠦᠨ ᠢ᠋ ᠭᠤᠤᠯ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠵᠢᠨ ᠮᠦᠷ ᠲᠤ᠌ ᠨᠡᠮᠡᠬᠡᠷᠡᠢ.
 ᠨᠦᠬᠦᠭᠡ ᠨᠢᠭᠡ ᠭᠤᠤᠯ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢ ᠨᠡᠮᠡᠬᠦ ᠳ᠋ᠤ᠌᠂ ᠳᠡᠬᠦᠨ ᠢ᠋ ᠨᠢᠭᠡ ᠬᠤᠭᠤᠰᠤᠨ ᠮᠦᠷ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠮᠦᠷ ᠲᠤ᠌ ᠨᠡᠮᠡᠬᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Co&amp;mment:</source>
        <translation>ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ(&amp;M):</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>&amp;Private torrent</source>
        <translation>ᠬᠤᠪᠢ ᠵᠢᠨ Torrent(&amp;P)</translation>
    </message>
</context>
<context>
    <name>MakeProgressDialog</name>
    <message>
        <location filename="../MakeProgressDialog.ui" line="+14"/>
        <source>New Torrent</source>
        <translation>ᠰᠢᠨ᠎ᠡ Torrent</translation>
    </message>
    <message>
        <location filename="../MakeDialog.cc" line="-108"/>
        <source>Creating &quot;%1&quot;</source>
        <translation>ᠶᠠᠭ ᠦᠢᠯᠡᠳᠴᠤ ᠪᠤᠢ &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Created &quot;%1&quot;!</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠦᠢᠯᠡᠳᠦᠭᠰᠡᠨ&quot;%1&quot;!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error: invalid announce URL &quot;%1&quot;</source>
        <translation>ᠪᠤᠷᠤᠭᠤ: ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠵᠠᠷᠯᠠᠯ ᠤ᠋ᠨ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancelled</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠦᠬᠡᠢᠰᠬᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error reading &quot;%1&quot;: %2</source>
        <translation>&quot;%1&quot; ᠵᠢ ᠤᠩᠰᠢᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ: %2</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error writing &quot;%1&quot;: %2</source>
        <translation>&quot;%1&quot; ᠵᠢ ᠪᠢᠴᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ: %2</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../OptionsDialog.cc" line="+56"/>
        <source>Open Torrent</source>
        <translation>Torrent ᠵᠢ/ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Open Torrent from File</source>
        <translation>ᠹᠠᠢᠯ ᠡᠴᠡ Torrent ᠢ᠋/ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open Torrent from URL or Magnet Link</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠪᠤᠶᠤ ᠰᠤᠷᠢᠨᠵᠢᠨ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠡᠴᠡ Torrent ᠢ᠋/ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../OptionsDialog.ui" line="+17"/>
        <source>&amp;Source:</source>
        <translation>ᠢᠷᠡᠯᠳᠡ(&amp;S):</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>&amp;Destination folder:</source>
        <translation>ᠬᠠᠷᠠᠯᠳᠠᠳᠤ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠢᠳᠠᠰᠤ(&amp;D):</translation>
    </message>
    <message>
        <location filename="../OptionsDialog.cc" line="+60"/>
        <source>High</source>
        <translation>ᠦᠨᠳᠦᠷ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Normal</source>
        <translation>ᠡᠨᠭ ᠤ᠋ᠨ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Low</source>
        <translation>ᠳᠤᠤᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../OptionsDialog.ui" line="+35"/>
        <source>&amp;Priority:</source>
        <translation>ᠳᠡᠷᠢᠬᠦᠯᠡᠬᠦ ᠳᠡᠰ(&amp;P):</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>S&amp;tart when added</source>
        <translation>ᠨᠡᠮᠡᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠡᠬᠢᠯᠡᠬᠦ(&amp;T)</translation>
    </message>
    <message>
        <location filename="../OptionsDialog.cc" line="+5"/>
        <source>&amp;Verify Local Data</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤ᠋ᠨ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ(&amp;V)</translation>
    </message>
    <message>
        <location filename="../OptionsDialog.ui" line="+7"/>
        <source>Mo&amp;ve .torrent file to the trash</source>
        <translation>.torrent ᠹᠠᠢᠯ ᠢ᠋ ᠬᠤᠭᠯᠠᠭᠤᠷ ᠲᠤ᠌ ᠰᠢᠯᠵᠢᠬᠦᠯᠬᠦ(&amp;V)</translation>
    </message>
    <message>
        <location filename="../OptionsDialog.cc" line="-55"/>
        <source>Torrent Files (*.torrent);;All Files (*.*)</source>
        <translation>Torrent ᠹᠠᠢᠯ (*.torrent);; ᠪᠦᠬᠦ ᠹᠠᠢᠯ (*.*)</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Select Destination</source>
        <translation>ᠬᠠᠷᠠᠯᠳᠠ ᠵᠢ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>PathButton</name>
    <message>
        <location filename="../PathButton.cc" line="+31"/>
        <location line="+72"/>
        <source>(None)</source>
        <translation>( ᠦᠬᠡᠢ)</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Select Folder</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠢᠳᠠᠰᠤ ᠵᠢ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Select File</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>PrefsDialog</name>
    <message>
        <location filename="../PrefsDialog.ui" line="+1139"/>
        <source>Use &amp;authentication</source>
        <translation>ᠪᠡᠶ᠎ᠡ ᠵᠢᠨ ᠭᠠᠷᠤᠯ ᠤ᠋ᠨ ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ(&amp;A)</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Username:</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ(&amp;U):</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Pass&amp;word:</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋(&amp;W):</translation>
    </message>
    <message>
        <location line="-53"/>
        <source>&amp;Open web client</source>
        <translation>WEB ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠵᠦᠬᠦᠷᠯᠢᠭ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ (&amp;O)</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Addresses:</source>
        <translation>ᠬᠠᠶᠢᠭ:</translation>
    </message>
    <message>
        <location line="-1150"/>
        <source>Speed Limits</source>
        <translation>ᠬᠤᠷᠳᠤᠴᠠ ᠵᠢᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>&lt;small&gt;Override normal speed limits manually or at scheduled times&lt;/small&gt;</source>
        <translation>&lt;small&gt; ᠭᠠᠷ ᠵᠢᠡᠷ ᠬᠦᠳᠡᠯᠬᠡᠬᠦ ᠪᠤᠶᠤ ᠳᠤᠭᠤᠢᠳᠠᠭᠰᠠᠨ ᠴᠠᠭ ᠲᠤ᠌ ᠡᠩ ᠤ᠋ᠨ ᠬᠤᠷᠳᠤᠴᠠ ᠵᠢᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ ᠢ᠋ ᠪᠦᠷᠬᠦᠬᠦᠯᠬᠦ &lt;/small&gt;</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>&amp;Scheduled times:</source>
        <translation>ᠳᠤᠭᠤᠢᠳᠠᠭᠰᠠᠨ ᠴᠠᠭ(&amp;S):</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>&amp;On days:</source>
        <translation>ᠬᠤᠨᠤᠭ ᠤ᠋ᠨ ᠲᠤᠭ᠎ᠠ(&amp;O):</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.cc" line="+261"/>
        <source>Every Day</source>
        <translation>ᠡᠳᠦᠷ ᠪᠦᠷᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Weekdays</source>
        <translation>ᠠᠵᠢᠯ ᠤ᠋ᠨ ᠡᠳᠦᠷ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Weekends</source>
        <translation>ᠭᠠᠷᠠᠭ ᠤ᠋ᠨ ᠰᠡᠬᠦᠯᠴᠢ</translation>
    </message>
    <message>
        <location line="-152"/>
        <source>Sunday</source>
        <translation>ᠭᠠᠷᠠᠭ ᠤ᠋ᠨ ᠡᠳᠦᠷ</translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Monday</source>
        <translation>ᠭᠠᠷᠠᠭ ᠦ᠋ᠨ ᠨᠢᠭᠡᠨ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Tuesday</source>
        <translation>ᠭᠠᠷᠠᠭ ᠦ᠋ᠨ ᠬᠣᠶᠠᠷ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Wednesday</source>
        <translation>ᠭᠠᠷᠠᠭ ᠦ᠋ᠨ ᠭᠤᠷᠪᠠᠨ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Thursday</source>
        <translation>ᠭᠠᠷᠠᠭ ᠦ᠋ᠨ ᠲᠦᠷᠪᠡᠨ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Friday</source>
        <translation>ᠭᠠᠷᠠᠭ ᠦ᠋ᠨ ᠲᠠᠪᠤᠨ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Saturday</source>
        <translation>ᠭᠠᠷᠠᠭ ᠦ᠋ᠨ ᠵᠢᠷᠭᠤᠭᠠᠨ</translation>
    </message>
    <message>
        <location line="+210"/>
        <source>Port is &lt;b&gt;open&lt;/b&gt;</source>
        <translation>ᠦᠵᠦᠬᠦᠷᠯᠢᠭ ᠨᠢ &lt;b&gt; ᠨᠡᠬᠡᠬᠡᠭᠰᠡᠨ&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Port is &lt;b&gt;closed&lt;/b&gt;</source>
        <translation>ᠦᠵᠦᠬᠦᠷᠯᠢᠭ ᠨᠢ &lt;b&gt; ᠬᠠᠭᠠᠭᠰᠠᠨ&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="+513"/>
        <source>Incoming Peers</source>
        <translation>ᠦᠷᠳᠡᠬᠡᠨ ᠳ᠋ᠤ᠌ ᠤᠷᠤᠭᠰᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location line="+32"/>
        <location filename="../PrefsDialog.cc" line="+340"/>
        <source>Status unknown</source>
        <translation>ᠪᠠᠢᠳᠠᠯ ᠢ᠋ ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>&amp;Port for incoming connections:</source>
        <translation>ᠦᠷᠳᠡᠬᠡᠨ ᠳ᠋ᠤ᠌ ᠤᠷᠤᠬᠤ ᠴᠦᠷᠬᠡᠯᠡᠬᠡᠨ ᠤ᠋ ᠦᠵᠦᠬᠦᠷᠯᠢᠭ(&amp;P):</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Use UPnP or NAT-PMP port &amp;forwarding from my router</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Options</source>
        <translation>ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Enable &amp;uTP for peer connections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-3"/>
        <source>uTP is a tool for reducing network congestion.</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../PrefsDialog.cc" line="-219"/>
        <source> minute(s)</source>
        <extracomment>Spin box suffix, &quot;Stop seeding if idle for: [ 5 minutes ]&quot; (includes leading space after the number, if needed)</extracomment>
        <translation>
            <numerusform> ᠮᠢᠨᠦ᠋ᠲ</numerusform>
            <numerusform> ᠮᠢᠨᠦ᠋ᠲ ᠤ᠋ᠳ</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+22"/>
        <source> minute(s) ago</source>
        <extracomment>Spin box suffix, &quot;Download is inactive if data sharing stopped: [ 5 minutes ago ]&quot; (includes leading space after the number, if needed)</extracomment>
        <translation>
            <numerusform> ᠮᠢᠨᠦ᠋ᠲ ᠤ᠋ᠨ ᠡᠮᠦᠨ᠎ᠡ</numerusform>
            <numerusform> ᠮᠢᠨᠦ᠋ᠲ ᠤ᠋ᠳ ᠤ᠋ᠨ ᠡᠮᠦᠨ᠎ᠡ</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="-638"/>
        <source>Automatically add .torrent files &amp;from:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Show the Torrent Options &amp;dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Start added torrents</source>
        <translation>Torrent ᠢ᠋/ ᠵᠢ ᠨᠡᠮᠡᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠡᠬᠢᠯᠡᠬᠦ(&amp;S)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Mo&amp;ve the .torrent file to the trash</source>
        <translation>.torrent ᠹᠠᠢᠯ ᠢ᠋ ᠬᠤᠭᠯᠠᠭᠤᠷ ᠲᠤ᠌ ᠰᠢᠯᠵᠢᠬᠦᠯᠬᠦ(&amp;V)</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Download Queue</source>
        <translation>ᠡᠩᠨᠡᠭᠡ ᠵᠢ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Ma&amp;ximum active downloads:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Incomplete</source>
        <translation>ᠳᠠᠭᠤᠰᠤᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Seeding</source>
        <translation>ᠦᠷ᠎ᠡ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+548"/>
        <source>Remote</source>
        <translation>ᠠᠯᠤᠰ</translation>
    </message>
    <message numerus="yes">
        <location filename="../PrefsDialog.cc" line="+145"/>
        <source>&lt;i&gt;Blocklist contains %Ln rule(s)&lt;/i&gt;</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="-287"/>
        <source>Pick a &amp;random port every time Transmission is started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-252"/>
        <source>Limits</source>
        <translation>ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ</translation>
    </message>
    <message>
        <location line="+299"/>
        <source>Maximum peers per &amp;torrent:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Maximum peers &amp;overall:</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../PrefsDialog.cc" line="-229"/>
        <source>&lt;b&gt;Update succeeded!&lt;/b&gt;&lt;p&gt;Blocklist now has %Ln rule(s).</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&lt;b&gt;Update Blocklist&lt;/b&gt;&lt;p&gt;Getting new blocklist...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="-193"/>
        <source>Blocklist</source>
        <translation>ᠬᠠᠷ᠎ᠠ ᠳᠠᠩᠰᠠ</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Enable &amp;automatic updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PrefsDialog.cc" line="+19"/>
        <source>Allow encryption</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠨᠡᠮᠡᠬᠦ ᠵᠢ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Prefer encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Require encryption</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠨᠡᠮᠡᠬᠦ ᠪᠡᠷ ᠱᠠᠭᠠᠷᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="-97"/>
        <source>Privacy</source>
        <translation>ᠨᠢᠭᠤᠴᠠ</translation>
    </message>
    <message>
        <location line="-403"/>
        <source>&amp;to</source>
        <translation>ᠬᠦᠷᠬᠦ(&amp;T)</translation>
    </message>
    <message>
        <location line="+763"/>
        <location line="+9"/>
        <source>Desktop</source>
        <translation>ᠰᠢᠷᠡᠬᠡᠨ ᠨᠢᠭᠤᠷ</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Show Transmission icon in the &amp;notification area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-213"/>
        <source>Te&amp;st Port</source>
        <translation>ᠦᠵᠦᠬᠦᠷᠯᠢᠭ ᠢ᠋ ᠳᠤᠷᠰᠢᠨ ᠰᠢᠯᠭᠠᠬᠤ(&amp;S)</translation>
    </message>
    <message>
        <location line="-101"/>
        <source>Enable &amp;blocklist:</source>
        <translation>ᠬᠠᠷ᠎ᠠ ᠳᠠᠩᠰᠠ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ(&amp;B):</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&amp;Update</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ(&amp;U)</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>&amp;Encryption mode:</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠨᠡᠮᠡᠬᠦ ᠵᠠᠭᠪᠤᠷ(&amp;E):</translation>
    </message>
    <message>
        <location line="+453"/>
        <source>Remote Control</source>
        <translation>ᠠᠯᠤᠰ ᠡᠴᠡ ᠡᠵᠡᠮᠰᠢᠬᠦ</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Allow &amp;remote access</source>
        <translation>ᠠᠯᠤᠰ ᠡᠴᠡ ᠠᠢᠯᠴᠢᠯᠠᠬᠤ ᠵᠢ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ(&amp;R)</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>HTTP &amp;port:</source>
        <translation>HTTP ᠦᠵᠦᠬᠦᠷᠯᠢᠭ(&amp;P):</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Only allow these IP a&amp;ddresses:</source>
        <translation>ᠵᠦᠪᠬᠡᠨ ᠡᠳᠡᠬᠡᠷ IP ᠬᠠᠶᠢᠭ ᠢ᠋ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ(&amp;D):</translation>
    </message>
    <message>
        <location line="-1128"/>
        <source>&amp;Upload:</source>
        <translation>ᠠᠴᠢᠶᠠᠯᠠᠬᠤ(&amp;U):</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&amp;Download:</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ(&amp;D):</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Alternative Speed Limits</source>
        <translation>ᠨᠦᠬᠡᠴᠡ ᠬᠤᠷᠳᠤᠴᠠ ᠵᠢ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>U&amp;pload:</source>
        <translation>ᠠᠴᠢᠶᠠᠯᠠᠬᠤ(&amp;P):</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Do&amp;wnload:</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ(&amp;W):</translation>
    </message>
    <message>
        <location line="+836"/>
        <source>Start &amp;minimized in notification area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Notification</source>
        <translation>ᠮᠡᠳᠡᠭᠳᠡᠯ</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Show a notification when torrents are a&amp;dded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show a notification when torrents &amp;finish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Play a &amp;sound when torrents finish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PrefsDialog.cc" line="-83"/>
        <source>Testing TCP Port...</source>
        <translation>TCP ᠦᠵᠦᠬᠦᠷᠯᠢᠭ ᠢ᠋ ᠶᠠᠭ ᠳᠤᠷᠰᠢᠨ ᠰᠢᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="-229"/>
        <source>Peer Limits</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ</translation>
    </message>
    <message>
        <location line="+104"/>
        <source>Use PE&amp;X to find more peers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-3"/>
        <source>PEX is a tool for exchanging peer lists with the peers you&apos;re connected to.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Use &amp;DHT to find more peers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-3"/>
        <source>DHT is a tool for finding peers without a tracker.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Use &amp;Local Peer Discovery to find more peers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-3"/>
        <source>LPD is a tool for finding peers on your local network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-325"/>
        <source>Encryption</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.cc" line="+160"/>
        <source>Select &quot;Torrent Done&quot; Script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-1"/>
        <source>Select Incomplete Directory</source>
        <translation>ᠳᠠᠭᠤᠰᠤᠭ᠎ᠠ ᠦᠬᠡᠢ ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="-2"/>
        <source>Select Watch Directory</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select Destination</source>
        <translation>ᠬᠠᠷᠠᠯᠳᠠ ᠵᠢ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="-355"/>
        <source>Adding</source>
        <translation>ᠶᠠᠭ ᠨᠡᠮᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Download is i&amp;nactive if data sharing stopped:</source>
        <extracomment>Please keep this phrase as short as possible, it&apos;s curently the longest and influences dialog width</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-146"/>
        <source>Downloading</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+205"/>
        <source>Append &quot;.&amp;part&quot; to incomplete files&apos; names</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Keep &amp;incomplete files in:</source>
        <translation>ᠳᠠᠭᠤᠰᠤᠭ᠎ᠠ ᠦᠬᠡᠢ ᠹᠠᠢᠯ ᠢ᠋(&amp;I) ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ:</translation>
    </message>
    <message>
        <location line="-148"/>
        <source>Save to &amp;Location:</source>
        <translation>(&amp;L) ᠪᠠᠢᠷᠢᠨ ᠳ᠋ᠤ᠌ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ:</translation>
    </message>
    <message>
        <location line="+170"/>
        <source>Call scrip&amp;t when torrent is completed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Stop seeding at &amp;ratio:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Stop seedi&amp;ng if idle for:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-557"/>
        <source>Transmission Preferences</source>
        <translation>BT ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠪᠠᠭᠠᠵᠢ ᠳᠡᠷᠢᠬᠦᠨ ᠤ᠋ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Speed</source>
        <translation>ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location line="+703"/>
        <source>Network</source>
        <translation>ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.cc" line="+91"/>
        <source>Not supported by remote sessions</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../Application.cc" line="-241"/>
        <source>Invalid option</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
</context>
<context>
    <name>RelocateDialog</name>
    <message>
        <location filename="../RelocateDialog.cc" line="+65"/>
        <source>Select Location</source>
        <translation>ᠪᠠᠢᠷᠢᠯᠠᠯ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../RelocateDialog.ui" line="+14"/>
        <source>Set Torrent Location</source>
        <translation>Torrent ᠪᠠᠢᠷᠢᠯᠠᠯ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Set Location</source>
        <translation>ᠪᠠᠢᠷᠢᠯᠠᠯ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>New &amp;location:</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠠᠢᠷᠢᠯᠠᠯ(&amp;L):</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Move from the current folder</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤᠨ ᠡᠴᠡ ᠨᠡᠬᠦᠯᠬᠡᠬᠦ(&amp;M)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Local data is &amp;already there</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤ᠋ᠨ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠨᠢᠭᠡᠨᠳᠡ ᠳᠡᠨᠳᠡ ᠪᠠᠢᠨ᠎ᠠ(&amp;A)</translation>
    </message>
</context>
<context>
    <name>Session</name>
    <message>
        <location filename="../Session.cc" line="+559"/>
        <source>Error Renaming Path</source>
        <translation>ᠵᠠᠮ ᠱᠤᠭᠤᠮ ᠢ᠋ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠭᠰᠡᠨ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;p&gt;&lt;b&gt;Unable to rename &quot;%1&quot; as &quot;%2&quot;: %3.&lt;/b&gt;&lt;/p&gt; &lt;p&gt;Please correct the errors and try again.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt; ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠬᠦ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ &quot;%1&quot; 为 &quot;%2&quot;: %3.&lt;/b&gt;&lt;/p&gt; &lt;p&gt; ᠠᠯᠳᠠᠭ᠎ᠠ ᠵᠢ ᠵᠠᠯᠠᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠭᠠᠳ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ.&lt;/p&gt;</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Add Torrent</source>
        <translation>Torrent ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>SessionDialog</name>
    <message>
        <location filename="../SessionDialog.ui" line="+14"/>
        <source>Change Session</source>
        <translation>ᠶᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠵᠢ ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Source</source>
        <translation>ᠢᠷᠡᠯᠳᠡ</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Start &amp;Local Session</source>
        <translation>ᠳᠤᠰ ᠭᠠᠵᠠᠷ ᠲᠡᠬᠢ ᠶᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ(&amp;L)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Connect to &amp;Remote Session</source>
        <translation>ᠠᠯᠤᠰ ᠤ᠋ᠨ ᠶᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠵᠢ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ(&amp;R)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Host:</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ(&amp;H):</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Port:</source>
        <translation>ᠦᠵᠦᠬᠦᠷᠯᠢᠭ(&amp;P):</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&amp;Authentication required</source>
        <translation>ᠪᠡᠶ᠎ᠡ ᠵᠢᠨ ᠭᠠᠷᠤᠯ ᠤ᠋ᠨ ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠱᠠᠭᠠᠷᠳᠠᠬᠤ(&amp;A)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Username:</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ(&amp;U):</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Pass&amp;word:</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋(&amp;W):</translation>
    </message>
</context>
<context>
    <name>StatsDialog</name>
    <message>
        <location filename="../StatsDialog.ui" line="+14"/>
        <source>Statistics</source>
        <translation>ᠲᠤᠭ᠎ᠠ ᠪᠦᠷᠢᠳᠬᠡᠯ</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Current Session</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠶᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+12"/>
        <location line="+96"/>
        <source>Uploaded:</source>
        <translation>ᠨᠢᠬᠡᠨᠳᠡ ᠠᠴᠢᠶᠠᠯᠠᠭᠰᠠᠨ:</translation>
    </message>
    <message>
        <location line="-82"/>
        <location line="+96"/>
        <source>Downloaded:</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠪᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ:</translation>
    </message>
    <message>
        <location line="-82"/>
        <location line="+96"/>
        <source>Ratio:</source>
        <translation>ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠤᠷᠮ᠎ᠠ:</translation>
    </message>
    <message>
        <location line="-82"/>
        <location line="+96"/>
        <source>Duration:</source>
        <translation>ᠴᠠᠭ ᠤ᠋ᠨ ᠤᠷᠳᠤ:</translation>
    </message>
    <message>
        <location line="-61"/>
        <source>Total</source>
        <translation>ᠨᠡᠢᠳᠡ</translation>
    </message>
    <message numerus="yes">
        <location filename="../StatsDialog.cc" line="+71"/>
        <source>Started %Ln time(s)</source>
        <translation>
            <numerusform>ᠨᠢᠭᠡᠨᠳᠡ %Ln ᠤᠳᠠᠭ᠎ᠠ ᠡᠬᠢᠯᠡᠬᠦᠯᠪᠡ</numerusform>
            <numerusform>ᠨᠢᠭᠡᠨᠳᠡ %Ln ᠤᠳᠠᠭ᠎ᠠ ᠡᠬᠢᠯᠡᠬᠦᠯᠪᠡ</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Torrent</name>
    <message>
        <location filename="../Torrent.cc" line="+774"/>
        <source>Verifying local data</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤ᠋ᠨ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠶᠠᠭ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Downloading</source>
        <translation>ᠶᠠᠭ ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seeding</source>
        <translation>ᠶᠠᠭ ᠦᠷ᠎ᠡ ᠪᠤᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>Finished</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠳᠠᠭᠤᠰᠪᠠ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Paused</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠲᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Queued for verification</source>
        <translation>ᠮᠦᠷᠯᠡᠵᠤ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Queued for download</source>
        <translation>ᠮᠦᠷᠯᠡᠵᠤ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Queued for seeding</source>
        <translation>ᠮᠦᠷᠯᠡᠵᠤ ᠦᠷ᠎ᠡ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Tracker gave a warning: %1</source>
        <translation>Tracker ᠨᠢᠭᠡ ᠤᠳᠠᠭ᠎ᠠ ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠭᠰᠡᠨ: %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tracker gave an error: %1</source>
        <translation>Tracker ᠨᠢᠭᠡ ᠤᠳᠠᠭ᠎ᠠ ᠪᠤᠷᠤᠭᠤ ᠭᠠᠷᠭᠠᠭᠰᠠᠨ: %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error: %1</source>
        <translation>ᠪᠤᠷᠤᠭᠤ: %1</translation>
    </message>
</context>
<context>
    <name>TorrentDelegate</name>
    <message>
        <location filename="../TorrentDelegate.cc" line="+171"/>
        <source>Magnetized transfer - retrieving metadata (%1%)</source>
        <extracomment>First part of torrent progress string; %1 is the percentage of torrent metadata downloaded</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1 of %2 (%3%)</source>
        <extracomment>First part of torrent progress string; %1 is how much we&apos;ve got, %2 is how much we&apos;ll have when done, %3 is a percentage of the two</extracomment>
        <translation>%1 /%2 (%3%)</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>%1 of %2 (%3%), uploaded %4 (Ratio: %5 Goal: %6)</source>
        <extracomment>First part of torrent progress string; %1 is how much we&apos;ve got, %2 is the torrent&apos;s total size, %3 is a percentage of the two, %4 is how much we&apos;ve uploaded, %5 is our upload-to-download ratio, %6 is the ratio we want to reach before we stop uploading</extracomment>
        <translation>%1 / %2 (%3%), ᠨᠢᠭᠡᠨᠳᠡ ᠠᠴᠢᠶᠠᠯᠠᠭᠰᠠᠨ %4 (ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠤᠷᠮ᠎ᠠ: %5 ᠬᠠᠷᠠᠯᠳᠠ: %6)</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>%1 of %2 (%3%), uploaded %4 (Ratio: %5)</source>
        <extracomment>First part of torrent progress string; %1 is how much we&apos;ve got, %2 is the torrent&apos;s total size, %3 is a percentage of the two, %4 is how much we&apos;ve uploaded, %5 is our upload-to-download ratio</extracomment>
        <translation>%1 / %2 (%3%), ᠨᠢᠭᠡᠨᠳᠡ ᠠᠴᠢᠶᠠᠯᠠᠭᠰᠠᠨ %4 (ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠤᠷᠮ᠎ᠠ: %5)</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>%1, uploaded %2 (Ratio: %3 Goal: %4)</source>
        <extracomment>First part of torrent progress string; %1 is the torrent&apos;s total size, %2 is how much we&apos;ve uploaded, %3 is our upload-to-download ratio, %4 is the ratio we want to reach before we stop uploading</extracomment>
        <translation>%1, ᠨᠢᠭᠡᠨᠳᠡ ᠠᠴᠢᠶᠠᠯᠠᠭᠰᠠᠨ %2 ( ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠤᠷᠮ᠎ᠠ: %3 ᠬᠠᠷᠠᠯᠳᠠ: %4)</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1, uploaded %2 (Ratio: %3)</source>
        <extracomment>First part of torrent progress string; %1 is the torrent&apos;s total size, %2 is how much we&apos;ve uploaded, %3 is our upload-to-download ratio</extracomment>
        <translation>%1, ᠨᠢᠭᠡᠨᠳᠡ ᠠᠴᠢᠶᠠᠯᠠᠭᠰᠠᠨ %2 ( ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠤᠷᠮ᠎ᠠ: %3)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source> - %1 left</source>
        <extracomment>Second (optional) part of torrent progress string; %1 is duration; notice that leading space (before the dash) is included here</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source> - Remaining time unknown</source>
        <extracomment>Second (optional) part of torrent progress string; notice that leading space (before the dash) is included here</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Ratio: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location line="+41"/>
        <source>Downloading from %Ln peer(s)</source>
        <extracomment>First part of phrase &quot;Downloading from ... peer(s) and ... web seed(s)&quot;</extracomment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+15"/>
        <source>Seeding to %Ln peer(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+15"/>
        <source> - </source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location line="-38"/>
        <source>Downloading metadata from %Ln peer(s) (%1% done)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>Downloading from %1 of %Ln connected peer(s)</source>
        <extracomment>First part of phrase &quot;Downloading from ... of ... connected peer(s) and ... web seed(s)&quot;</extracomment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+6"/>
        <source> and %Ln web seed(s)</source>
        <extracomment>Second (optional) part of phrase &quot;Downloading from ... of ... connected peer(s) and ... web seed(s)&quot;; notice that leading space (before &quot;and&quot;) is included here</extracomment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+8"/>
        <source>Seeding to %1 of %Ln connected peer(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-70"/>
        <source>Verifying local data (%1% tested)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrackerDelegate</name>
    <message numerus="yes">
        <location filename="../TrackerDelegate.cc" line="+212"/>
        <source>Got a list of%1 %Ln peer(s)%2 %3 ago</source>
        <extracomment>%1 and %2 are replaced with HTML markup, %3 is duration</extracomment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Peer list request %1timed out%2 %3 ago; will retry</source>
        <extracomment>%1 and %2 are replaced with HTML markup, %3 is duration</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Got an error %1&quot;%2&quot;%3 %4 ago</source>
        <extracomment>%1 and %3 are replaced with HTML markup, %2 is error message, %4 is duration</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>No updates scheduled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Asking for more peers in %1</source>
        <extracomment>%1 is duration</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Queued to ask for more peers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Asking for more peers now... &lt;small&gt;%1&lt;/small&gt;</source>
        <extracomment>%1 is duration</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location line="+17"/>
        <source>Tracker had%1 %Ln seeder(s)%2</source>
        <extracomment>First part of phrase &quot;Tracker had ... seeder(s) and ... leecher(s) ... ago&quot;; %1 and %2 are replaced with HTML markup</extracomment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+6"/>
        <source> and%1 %Ln leecher(s)%2 %3 ago</source>
        <extracomment>Second part of phrase &quot;Tracker had ... seeder(s) and ... leecher(s) ... ago&quot;; %1 and %2 are replaced with HTML markup, %3 is duration; notice that leading space (before &quot;and&quot;) is included here</extracomment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Tracker had %1no information%2 on peer counts %3 ago</source>
        <extracomment>%1 and %2 are replaced with HTML markup, %3 is duration</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Got a scrape error %1&quot;%2&quot;%3 %4 ago</source>
        <extracomment>%1 and %3 are replaced with HTML markup, %2 is error message, %4 is duration</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Asking for peer counts in %1</source>
        <extracomment>%1 is duration</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Queued to ask for peer counts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Asking for peer counts now... &lt;small&gt;%1&lt;/small&gt;</source>
        <extracomment>%1 is duration</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
