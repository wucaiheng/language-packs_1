<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AboutWidget</name>
    <message>
        <source>Kylin-camera</source>
        <translation type="vanished">麒麟摄像头</translation>
    </message>
    <message>
        <location filename="../src/aboutwidget.cpp" line="134"/>
        <source>kylin-camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Camera</source>
        <translation type="vanished">摄像头</translation>
    </message>
    <message>
        <location filename="../src/aboutwidget.cpp" line="138"/>
        <source>Version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/aboutwidget.cpp" line="148"/>
        <source>Camera is a very strong camera software. It has the characteristics of easy to use. You can realize the function of taking a photo recording and displaying the album</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/aboutwidget.cpp" line="183"/>
        <source>privacy statement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Kylin camera is a very strong camera software. It has the characteristics of easy to use. You can realize the function of taking a photo recording and displaying the album</source>
        <translation type="vanished">麒麟摄像头是一款很专业的摄像头软件。有着简单易用的特点。可以实现拍照录像以及相册显示的功能</translation>
    </message>
    <message>
        <location filename="../src/aboutwidget.cpp" line="165"/>
        <location filename="../src/aboutwidget.cpp" line="171"/>
        <location filename="../src/aboutwidget.cpp" line="308"/>
        <location filename="../src/aboutwidget.cpp" line="316"/>
        <source>Service &amp; Support: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>support</source>
        <translation type="vanished">支持</translation>
    </message>
</context>
<context>
    <name>Button</name>
    <message>
        <location filename="../src/button.cpp" line="203"/>
        <source>capture mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="204"/>
        <source>video mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="205"/>
        <location filename="../src/button.cpp" line="206"/>
        <source>cheese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="207"/>
        <source>video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="208"/>
        <source>stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="209"/>
        <source>album</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="210"/>
        <source>delay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="211"/>
        <source>mirror</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="212"/>
        <source>grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>filter</source>
        <translation type="vanished">滤镜</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="214"/>
        <source>seiral shoot</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CameraPage</name>
    <message>
        <source>No devices were found</source>
        <translation type="vanished">未发现设备</translation>
    </message>
    <message>
        <source>No device were found</source>
        <translation type="obsolete">无摄像头可用</translation>
    </message>
    <message>
        <source>Please connect the camera first</source>
        <translation type="obsolete">若要使用此功能，请先连接摄像头。</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>3s</source>
        <translation type="vanished">3秒</translation>
    </message>
    <message>
        <source>6s</source>
        <translation type="vanished">6秒</translation>
    </message>
    <message>
        <source>9s</source>
        <translation type="vanished">9秒</translation>
    </message>
    <message>
        <source>waring</source>
        <translation type="obsolete">警告</translation>
    </message>
    <message>
        <source>save path can&apos;t write</source>
        <translation type="obsolete">储存路径没有写如权限</translation>
    </message>
    <message>
        <source>File generation in progress, please try again later</source>
        <translation type="obsolete">文件正在保存，请稍后操作</translation>
    </message>
    <message>
        <source>path does not exist, save to default path</source>
        <translation type="obsolete">路径不存在，文件保存到默认路径</translation>
    </message>
</context>
<context>
    <name>DelayWdiget</name>
    <message>
        <location filename="../src/delaywdiget.cpp" line="69"/>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/delaywdiget.cpp" line="70"/>
        <source>3s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/delaywdiget.cpp" line="71"/>
        <source>6s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/delaywdiget.cpp" line="72"/>
        <source>9s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DevicePull</name>
    <message>
        <location filename="../src/devicepull.cpp" line="95"/>
        <source>Device is pulled out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/devicepull.cpp" line="106"/>
        <source>Device is pulled in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/devicepull.cpp" line="150"/>
        <source>kylin-camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/devicepull.cpp" line="153"/>
        <source>kylin camera message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HasDevicePage</name>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="427"/>
        <location filename="../src/hasdevicepage.cpp" line="479"/>
        <location filename="../src/hasdevicepage.cpp" line="1043"/>
        <source>camera is being used by kylin-camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="433"/>
        <location filename="../src/hasdevicepage.cpp" line="470"/>
        <location filename="../src/hasdevicepage.cpp" line="497"/>
        <location filename="../src/hasdevicepage.cpp" line="602"/>
        <location filename="../src/hasdevicepage.cpp" line="775"/>
        <location filename="../src/hasdevicepage.cpp" line="786"/>
        <location filename="../src/hasdevicepage.cpp" line="1037"/>
        <source>waring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="433"/>
        <location filename="../src/hasdevicepage.cpp" line="1037"/>
        <source>The camera is occupied, please check the use of the device!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="470"/>
        <source>The camera is occupied or there is an exception in the target switching device, please check the device!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="497"/>
        <location filename="../src/hasdevicepage.cpp" line="775"/>
        <source>save path can&apos;t write</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="602"/>
        <source>path does not exist, save to default path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="786"/>
        <source>File generation in progress, please try again later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="944"/>
        <source>3p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="948"/>
        <source>5p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="952"/>
        <source>10p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="956"/>
        <source>20p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="1489"/>
        <source>kylin-camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="1492"/>
        <source>kylin camera message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>3pic</source>
        <translation type="vanished">3张</translation>
    </message>
    <message>
        <source>5pic</source>
        <translation type="vanished">5张</translation>
    </message>
    <message>
        <source>10pic</source>
        <translation type="vanished">10张</translation>
    </message>
    <message>
        <source>20pic</source>
        <translation type="vanished">20张</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Camera</source>
        <translation type="obsolete">摄像头</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="145"/>
        <source>kylin-camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="203"/>
        <location filename="../src/mainwindow.cpp" line="531"/>
        <source>waring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="403"/>
        <source>maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="408"/>
        <source>normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>other user already open device!</source>
        <translation type="obsolete">其他用户正在使用摄像头</translation>
    </message>
    <message>
        <source>path does not exist, save to default path</source>
        <translation type="obsolete">路径不存在，文件保存到默认路径</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="203"/>
        <source>path does not exist, please set storage path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>save path can&apos;t write</source>
        <translation type="vanished">储存路径没有写如权限</translation>
    </message>
    <message>
        <source>File generation in progress, please try again later</source>
        <translation type="obsolete">文件正在保存，请稍后操作</translation>
    </message>
    <message>
        <source>delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <source>Are you sure?!</source>
        <translation type="vanished">你确定要这样做？</translation>
    </message>
    <message>
        <source>no</source>
        <translation type="vanished">否</translation>
    </message>
    <message>
        <source>yes</source>
        <translation type="vanished">是</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="531"/>
        <source>The camera is turned on and cannot be started again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>help</source>
        <translation type="vanished">帮助</translation>
    </message>
    <message>
        <source>about</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>theme</source>
        <translation type="vanished">主题</translation>
    </message>
    <message>
        <source>set</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>quit</source>
        <translation type="vanished">退出</translation>
    </message>
</context>
<context>
    <name>NPixCapWidget</name>
    <message>
        <location filename="../src/npixcapwidget.cpp" line="77"/>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/npixcapwidget.cpp" line="78"/>
        <source>3p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/npixcapwidget.cpp" line="79"/>
        <source>5p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/npixcapwidget.cpp" line="80"/>
        <source>10p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/npixcapwidget.cpp" line="81"/>
        <source>20p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>3pic</source>
        <translation type="vanished">3张</translation>
    </message>
    <message>
        <source>5pic</source>
        <translation type="vanished">5张</translation>
    </message>
    <message>
        <source>10pic</source>
        <translation type="vanished">10张</translation>
    </message>
    <message>
        <source>20pic</source>
        <translation type="vanished">20张</translation>
    </message>
    <message>
        <source>3sheets</source>
        <translation type="vanished">3张</translation>
    </message>
    <message>
        <source>5sheets</source>
        <translation type="vanished">5张</translation>
    </message>
    <message>
        <source>10sheets</source>
        <translation type="vanished">10张</translation>
    </message>
    <message>
        <source>20sheets</source>
        <translation type="vanished">20张</translation>
    </message>
</context>
<context>
    <name>NoDevicePage</name>
    <message>
        <location filename="../src/nodevicepage.cpp" line="47"/>
        <source>No device were found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/nodevicepage.cpp" line="48"/>
        <source>Please connect the camera first and app will continuously retrieve avaliable devices for you</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please connect the camera first</source>
        <translation type="vanished">若要使用此功能，请先连接摄像头。</translation>
    </message>
</context>
<context>
    <name>PictureViewPage</name>
    <message>
        <location filename="../src/pictureviewpage.cpp" line="20"/>
        <source>Album</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordModule</name>
    <message>
        <location filename="../src/recordmodule.cpp" line="11"/>
        <source>stop record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/recordmodule.cpp" line="17"/>
        <source>countinue record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/recordmodule.cpp" line="23"/>
        <source>pause record</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Setting</name>
    <message>
        <source>Delayed shooting</source>
        <translation type="vanished">延时</translation>
    </message>
    <message>
        <source>Image mirroring</source>
        <translation type="vanished">镜像</translation>
    </message>
    <message>
        <source>setting</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="33"/>
        <source>theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="36"/>
        <location filename="../src/setting.cpp" line="135"/>
        <source>quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="34"/>
        <location filename="../src/setting.cpp" line="111"/>
        <source>help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="51"/>
        <source>Delayed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>mirroring</source>
        <translation type="vanished">镜像</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="32"/>
        <location filename="../src/setting.cpp" line="124"/>
        <source>set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="35"/>
        <location filename="../src/setting.cpp" line="114"/>
        <source>about</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="46"/>
        <source>kylin-camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="67"/>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="71"/>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="75"/>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The image path</source>
        <translation type="vanished">图像路径</translation>
    </message>
    <message>
        <source>The image scale</source>
        <translation type="vanished">图像尺寸</translation>
    </message>
    <message>
        <source>Camera device</source>
        <translation type="vanished">摄像头设备</translation>
    </message>
    <message>
        <source>Sound device</source>
        <translation type="vanished">声音设备</translation>
    </message>
    <message>
        <source>Select the directory</source>
        <translation type="vanished">选择目录</translation>
    </message>
    <message>
        <source>error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>have no permissions !?</source>
        <translation type="vanished">没有权限！？</translation>
    </message>
</context>
<context>
    <name>SettingPage</name>
    <message>
        <source>setting</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Delayed shooting</source>
        <translation type="obsolete">延迟拍照</translation>
    </message>
    <message>
        <source>Image mirroring</source>
        <translation type="obsolete">图像镜像</translation>
    </message>
    <message>
        <source>The image path</source>
        <translation type="vanished">图像路径</translation>
    </message>
    <message>
        <source>The image scale</source>
        <translation type="vanished">图像分辨率</translation>
    </message>
    <message>
        <source>Camera device</source>
        <translation type="vanished">摄像头设备</translation>
    </message>
    <message>
        <source>Sound device</source>
        <translation type="vanished">声音设备</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="73"/>
        <source>confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="74"/>
        <source>cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>photo save format</source>
        <translation type="vanished">图片保存格式</translation>
    </message>
    <message>
        <source>video save format</source>
        <translation type="vanished">录像保存格式</translation>
    </message>
    <message>
        <source>camera setting</source>
        <translation type="vanished">摄像头设置</translation>
    </message>
    <message>
        <source>The image path:</source>
        <translation type="vanished">默认存储位置:</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="31"/>
        <source>browse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Camera device:</source>
        <translation type="vanished">默认摄像头:</translation>
    </message>
    <message>
        <source>save photo format:</source>
        <translation type="vanished">照片格式:</translation>
    </message>
    <message>
        <source>save video format:</source>
        <translation type="vanished">视频格式:</translation>
    </message>
    <message>
        <source>The image scale:</source>
        <translation type="vanished">拍摄分辨率:</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="33"/>
        <source>image path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="41"/>
        <source>device:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="52"/>
        <source>photo format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="61"/>
        <source>video format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="68"/>
        <source>scale:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="245"/>
        <source>Select the directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="257"/>
        <source>error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="257"/>
        <source>The directory does not have write permissions, select the user directory to store the files .</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="276"/>
        <location filename="../src/settingpage.cpp" line="278"/>
        <source>waring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="276"/>
        <source>The settings cannot be changed during recording</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="278"/>
        <source>The settings cannot be changed,please try again later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>have no permissions !?</source>
        <translation type="vanished">没有权限！？</translation>
    </message>
</context>
<context>
    <name>SettingPageTitle</name>
    <message>
        <location filename="../src/settingpagetitle.cpp" line="35"/>
        <source>Camera</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../src/titlebar.cpp" line="78"/>
        <source>Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="85"/>
        <source>Minimise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="91"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="97"/>
        <source>Maximize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="106"/>
        <source>kylin-camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="183"/>
        <source>maximize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="189"/>
        <source>normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>camera</source>
        <translation type="vanished">摄像头</translation>
    </message>
    <message>
        <source>Camera</source>
        <translation type="vanished">摄像头</translation>
    </message>
</context>
<context>
    <name>cameraFilterWidget</name>
    <message>
        <location filename="../src/camerafilterwidget.cpp" line="23"/>
        <source>original</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/camerafilterwidget.cpp" line="33"/>
        <source>warm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/camerafilterwidget.cpp" line="44"/>
        <source>cool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/camerafilterwidget.cpp" line="53"/>
        <source>black-and-white</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
