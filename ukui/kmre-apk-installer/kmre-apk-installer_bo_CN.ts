<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>ImportWidget</name>
    <message>
        <location filename="../importwidget.cpp" line="54"/>
        <source>Drag and drop APK file here</source>
        <translation>APK ་ཡིག་ཆ་འདི་ལ་འདྲུད་འཐེན་བྱས།</translation>
    </message>
    <message>
        <location filename="../importwidget.cpp" line="64"/>
        <source>Note: installing the third party APK may run abnormally</source>
        <translation>མཆན་འགྲེལ། ཕུང་གསུམ་པའི་APKསྒྲིག་སྦྱོར་བྱས་ན་རྒྱུན་ལྡན་མིན་པར་འཁོར་སྐྱོད་བྱེད་སྲིད།</translation>
    </message>
    <message>
        <source>Note: Installing Android apps that are not available in the kylin-software-center may not work properly</source>
        <translation type="vanished">注意：安装未在软件商店上架的安卓应用，可能会运行异常</translation>
    </message>
    <message>
        <location filename="../importwidget.cpp" line="81"/>
        <source>Select APK File</source>
        <translation>APKཡི་ཡིག་ཆ་གདམ་གསེས།</translation>
    </message>
</context>
<context>
    <name>InfoPage</name>
    <message>
        <location filename="../infopage.cpp" line="43"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../infopage.cpp" line="46"/>
        <source>Run</source>
        <translation>འཁོར་སྐྱོད་སྒོ་ཕྱེ་བ།</translation>
    </message>
</context>
<context>
    <name>InstallWidget</name>
    <message>
        <location filename="../installwidget.cpp" line="84"/>
        <source>Install</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="85"/>
        <source>Back</source>
        <translation>ཕྱིར་ལོག་པ།</translation>
    </message>
    <message>
        <source>Name: </source>
        <translation type="vanished">名字：</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本：</translation>
    </message>
    <message>
        <source>Application Label: </source>
        <translation type="vanished">应用标签：</translation>
    </message>
    <message>
        <source>Application Zh Label: </source>
        <translation type="vanished">应用中文标签：</translation>
    </message>
    <message>
        <source>Size: </source>
        <translation type="vanished">大小：</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="140"/>
        <location filename="../installwidget.cpp" line="310"/>
        <source>Note: installing the third party APK may run abnormally</source>
        <translation>མཆན་འགྲེལ། ཕུང་གསུམ་པའི་APKསྒྲིག་སྦྱོར་བྱས་ན་རྒྱུན་ལྡན་མིན་པར་འཁོར་སྐྱོད་བྱེད་སྲིད།</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="223"/>
        <source>Note: installing the third party APK may run abnormally
The CPU is FT1500A, limited by CPU performance, video App and game App are not effective.</source>
        <translation type="unfinished">མཆན་འགྲེལ། ཕུང་གསུམ་པའི་APKསྒྲིག་སྦྱོར་བྱས་ན་རྒྱུན་ལྡན་མིན་པར་འཁོར་སྐྱོད་བྱེད་སྲིད།
མིག་སྔར་ཐག་གཅོད་ཆས་ནི་FT1500A་ཡིན་པ་དང་།ཐག་གཅོད་ཆས་ཀྱི་གཤིས་ནུས་ལ་ཚོད་འཛིན་ཐེབས་པ་དང་།བརྙན་འཕྲིན་རིགས་དང་རོལ་རྩེད་རིགས་ཀྱི་བཀོལ་སྤྱོད་ཕན་འབྲས་ཅུང་ཞན་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="227"/>
        <location filename="../installwidget.cpp" line="230"/>
        <location filename="../installwidget.cpp" line="233"/>
        <location filename="../installwidget.cpp" line="236"/>
        <source>Note: installing the third party APK may run abnormally
The graphics card is %1, limited by graphics card performance, video App and game App are not effective.</source>
        <translation>མཆན་འགྲེལ། ཕུང་གསུམ་པའི་APKསྒྲིག་སྦྱོར་བྱས་ན་རྒྱུན་ལྡན་མིན་པར་འཁོར་སྐྱོད་བྱེད་སྲིད།
རི་མོའི་བྱང་བུ་ནི་%1ཡིན་ཞིང་། རི་མོའི་བྱང་བུའི་ནུས་པ་དང་། བརྙན་ཕབ་ཉེར་སྤྱོད། རོལ་རྩེད་ཉེར་སྤྱོད་བཅས་ཀྱི་ཚོད་འཛིན་ཐེབས་པའི་རྐྱེན་གྱིས་ཕན་འབྲས་ཐོན་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="230"/>
        <source>Zhaoxin</source>
        <translation>འཆར་ཁཱའི་འདུས་གྲུབ་ཀྲུའོ་ཉིང་ལེབ།</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="233"/>
        <source>virtual graphics card</source>
        <translation>རྟོག་བཟོའི་རི་མོའི་བྱང་བུ།</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="236"/>
        <source>JJM</source>
        <translation>ཅིན་ཅ་ཝེ་འཆར་ཁཱ།</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="326"/>
        <source>Installing APK ......</source>
        <translation>APKསྒྲིག་སྦྱོར་བྱེད་བཞིན་པ།</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="331"/>
        <source>Copy APK failed</source>
        <translation>འདྲ་བཤུས་APKལ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="355"/>
        <source>Copying APK ......</source>
        <translation>APK འདྲ་བཤུས་བྱེད་བཞིན་པ།</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="371"/>
        <source>APK does not exists</source>
        <translation>APKནི་ཡིག་ཆ་གནས་མེད་པ་ཞིག་རེད།</translation>
    </message>
    <message>
        <source>APK Installed successfully</source>
        <translation type="vanished">APK安装成功</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="64"/>
        <location filename="../mainwindow.cpp" line="134"/>
        <source>KMRE APK Installer</source>
        <translation>KMRE APKསྒྲིག་སྦྱོར་བྱེད་ཆས།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="192"/>
        <location filename="../mainwindow.cpp" line="260"/>
        <source>KMRE environment not installed,please go to the app store to start the mobile environment</source>
        <translation>ཆི་ལིན་སྤོ་འགུལ་འཁོར་སྐྱོད་ཁོར་ཡུག་སྒོ་ཕྱེ་མེད་པས་མཉེན་ཆས་ཚོང་ཁང་དུ་སོང་ནས་སྤོ་འགུལ་ཁོར་ཡུག་སྒོ་འབྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="197"/>
        <source>APK package does not exist or file type is abnormal!</source>
        <translation>APK ཁུག་མ་མེད་པའམ་ཡང་ན་ཡིག་ཆའི་རིགས་དབྱིབས་རྒྱུན་ལྡན་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="200"/>
        <source>Invalid APK file!</source>
        <translation>གོ་མི་ཆོད་པའི་APKཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="203"/>
        <source>Unknown error!</source>
        <translation>ཤེས་མེད་པའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="208"/>
        <source>%1 install successfully</source>
        <translation>%1སྒྲིག་སྦྱོར་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="211"/>
        <source>%1 install failed</source>
        <translation>%1 སྒྲིག་སྔྱོར་ཕམ་ཁ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="159"/>
        <source>KMRE environment not installed</source>
        <translation>ཆི་ལིན་སྤོ་འགུལ་འཁོར་སྐྱོད་ཁོར་ཡུག་སྒྲིག་སྦྱོར་བྱས་མེད་།མཉེན་ཆས་ཚོང་ཁང་དུ་སོང་ནས་སྤོ་འགུལ་ཁོར་ཡུག་སྒྲིག་སྦྱོར་བྱེད་རོགས།</translation>
    </message>
</context>
<context>
    <name>MetadataName</name>
    <message>
        <location filename="../installwidget.cpp" line="57"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="58"/>
        <source>Version</source>
        <translation>པར་གཞི།</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="59"/>
        <source>Application</source>
        <translation>གདོང་འཛར།</translation>
    </message>
    <message>
        <source>Application Zh</source>
        <translation type="vanished">中文标签</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="60"/>
        <source>File size</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../titlebar.cpp" line="137"/>
        <source>Minimize</source>
        <translation>ཆེས་ཆུང་བ།</translation>
    </message>
    <message>
        <location filename="../titlebar.cpp" line="142"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
</TS>
