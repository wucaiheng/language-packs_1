<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>AddGameWidget</name>
    <message>
        <location filename="../addgamewidget.cpp" line="105"/>
        <source>App supporting game keys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="71"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="74"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="92"/>
        <source>ASTC texture (When turned on, the picture quality is clearer)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="96"/>
        <source>Enable ASTC texture support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="151"/>
        <source>KMRE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="151"/>
        <location filename="../addgamewidget.cpp" line="154"/>
        <source>Add successfully, restart the Android application set to take effect!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="154"/>
        <source>Tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="168"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="222"/>
        <source>Game name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="222"/>
        <source>Game package name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AppMultiplierWidget</name>
    <message>
        <location filename="../appmultiplierwidget.cpp" line="63"/>
        <location filename="../appmultiplierwidget.cpp" line="85"/>
        <source>App Multiplier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appmultiplierwidget.cpp" line="45"/>
        <source>KMRE is not running!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appmultiplierwidget.cpp" line="52"/>
        <source>App Multiplier displays apps in dual windows. Not all apps are supported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appmultiplierwidget.cpp" line="58"/>
        <source>No App is installed which supporting App Multiplier.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CameraWidget</name>
    <message>
        <location filename="../camerawidget.cpp" line="63"/>
        <source>Camera Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../camerawidget.cpp" line="41"/>
        <source>Camera device:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../camerawidget.cpp" line="125"/>
        <source>No camera detected</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CleanerItem</name>
    <message>
        <location filename="../cleaneritem.cpp" line="104"/>
        <source>Delete the idle Android image?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cleaneritem.cpp" line="98"/>
        <location filename="../cleaneritem.cpp" line="104"/>
        <source>After the image is deleted, KMRE will no longer be able to switch to the image. Are you sure you want to perform this operation?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cleaneritem.cpp" line="98"/>
        <source>KMRE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cleaneritem.cpp" line="118"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cleaneritem.cpp" line="119"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cleaneritem.cpp" line="213"/>
        <source>Currently configured</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cleaneritem.cpp" line="213"/>
        <source>Idle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CleanerWidget</name>
    <message>
        <location filename="../cleanerwidget.cpp" line="104"/>
        <location filename="../cleanerwidget.cpp" line="107"/>
        <source>Image cleaning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="64"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="66"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="68"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="46"/>
        <source>Delete all idle images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete all idle Android images?</source>
        <translation type="vanished">删除闲置的所有Android镜像？</translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="331"/>
        <location filename="../cleanerwidget.cpp" line="339"/>
        <source>After all images are deleted, KMRE will no longer be able to switch to the image. Are you sure you want to perform this operation?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="331"/>
        <source>KMRE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="339"/>
        <source>Delete all idle images?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="110"/>
        <source>No idle image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="349"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="350"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeveloperWidget</name>
    <message>
        <location filename="../developerwidget.cpp" line="13"/>
        <source>Developer Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../developerwidget.cpp" line="17"/>
        <source>open developer mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../developerwidget.cpp" line="25"/>
        <source>Please connect to the KMRE environment via adb connect %1 or open the help documentation if in doubt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../developerwidget.cpp" line="25"/>
        <source>Please enter the following command on the terminal to install dependency:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../developerwidget.cpp" line="39"/>
        <source>KMRE is not running!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DisplayModeWidget</name>
    <message>
        <location filename="../displaymodewidget.cpp" line="83"/>
        <source>Performance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="83"/>
        <source>Compatibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>(Supports AMD and Intel graphics cards)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="84"/>
        <source>(Supports all graphics cards)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="122"/>
        <source>The modification takes effect after you restart the system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="90"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="124"/>
        <source>Display mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="99"/>
        <source>Restart system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="84"/>
        <source>Only AMD and Intel graphics card are supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="141"/>
        <source>KMRE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="141"/>
        <location filename="../displaymodewidget.cpp" line="147"/>
        <source>Are you sure you want to restart system?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="147"/>
        <source>Restart system?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="157"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="158"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DockerIpWidget</name>
    <message>
        <location filename="../dockeripwidget.cpp" line="107"/>
        <source>Default IP address of docker morn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="76"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="80"/>
        <source>Cancle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="84"/>
        <source>Restart system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="88"/>
        <source>IP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="89"/>
        <source>subnet mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="133"/>
        <location filename="../dockeripwidget.cpp" line="168"/>
        <source>KMRE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="110"/>
        <source>After the IP address is changed, you need to restart the system for the change to take effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="133"/>
        <location filename="../dockeripwidget.cpp" line="145"/>
        <source>Please check the IP and mask carefully, otherwise the docker environment may not start normally. Are you sure you want to modify it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="145"/>
        <source>Are you sure to modify the docker IP address?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="168"/>
        <location filename="../dockeripwidget.cpp" line="174"/>
        <source>Are you sure you want to restart system?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="174"/>
        <source>Restart system?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="163"/>
        <location filename="../dockeripwidget.cpp" line="196"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="164"/>
        <location filename="../dockeripwidget.cpp" line="197"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GameListItem</name>
    <message>
        <location filename="../gamelistitem.cpp" line="37"/>
        <location filename="../gamelistitem.cpp" line="99"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gamelistitem.cpp" line="71"/>
        <location filename="../gamelistitem.cpp" line="78"/>
        <source>Clearing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gamelistitem.cpp" line="75"/>
        <source>Confirm clear</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GameWidget</name>
    <message>
        <location filename="../gamewidget.cpp" line="51"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="87"/>
        <source>appName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="88"/>
        <source>pkgName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="148"/>
        <source>Game Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="152"/>
        <source>Enable ASTC texture support(When turned on, the picture quality is clearer)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="154"/>
        <source>When added to the list, the app will enable the Game button feature:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="178"/>
        <source>KMRE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="178"/>
        <location filename="../gamewidget.cpp" line="186"/>
        <source>You will remove %1 from the white list of game buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="178"/>
        <location filename="../gamewidget.cpp" line="186"/>
        <source>Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="215"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="216"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GeneralSettingWidget</name>
    <message>
        <location filename="../generalsettingwidget.cpp" line="12"/>
        <source>save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="17"/>
        <source>General Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="21"/>
        <source>Limit APP Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="24"/>
        <source>Application Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="34"/>
        <source>Limit the number of applications running</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="41"/>
        <source>KMRE Auto Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="49"/>
        <source>KMRE starts automatically upon startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="52"/>
        <source>Translation Switch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="55"/>
        <source>default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="50"/>
        <source>Camera device:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="286"/>
        <source>No camera detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="114"/>
        <source>Docker network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="116"/>
        <source>Network segment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="117"/>
        <source>Subnet mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="120"/>
        <source>save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="141"/>
        <source>Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="144"/>
        <source>screenshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="163"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="167"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="173"/>
        <source>Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="93"/>
        <location filename="../generalsettingwidget.cpp" line="97"/>
        <location filename="../generalsettingwidget.cpp" line="100"/>
        <source>Modify successfully, close the settings set to take effect!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="114"/>
        <location filename="../generalsettingwidget.cpp" line="127"/>
        <location filename="../generalsettingwidget.cpp" line="138"/>
        <source>Are you sure to cancel the limited on the number of application?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="114"/>
        <location filename="../generalsettingwidget.cpp" line="127"/>
        <location filename="../generalsettingwidget.cpp" line="138"/>
        <source>Cancel the limited and open too many windows may cause the system to stall. Are you sure you want to cancel it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="170"/>
        <source>auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="171"/>
        <source>manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="729"/>
        <source>Please check the IP, mask carefully, otherwise the docker environment may not start normally. Are you sure you want to modify it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="210"/>
        <source>Are you sure to modify the docker IP address?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="171"/>
        <location filename="../generalsettingwidget.cpp" line="175"/>
        <location filename="../generalsettingwidget.cpp" line="178"/>
        <location filename="../generalsettingwidget.cpp" line="195"/>
        <location filename="../generalsettingwidget.cpp" line="199"/>
        <location filename="../generalsettingwidget.cpp" line="202"/>
        <source>Modify successfully!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="93"/>
        <location filename="../generalsettingwidget.cpp" line="97"/>
        <location filename="../generalsettingwidget.cpp" line="100"/>
        <location filename="../generalsettingwidget.cpp" line="171"/>
        <location filename="../generalsettingwidget.cpp" line="175"/>
        <location filename="../generalsettingwidget.cpp" line="178"/>
        <location filename="../generalsettingwidget.cpp" line="195"/>
        <location filename="../generalsettingwidget.cpp" line="199"/>
        <location filename="../generalsettingwidget.cpp" line="202"/>
        <source>Tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="101"/>
        <location filename="../generalsettingwidget.cpp" line="139"/>
        <location filename="../generalsettingwidget.cpp" line="179"/>
        <location filename="../generalsettingwidget.cpp" line="203"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="140"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="207"/>
        <source>Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="487"/>
        <source>Getting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="686"/>
        <source>failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GlesVersionWidget</name>
    <message>
        <location filename="../glesversionwidget.cpp" line="70"/>
        <source>(Compatibility)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="74"/>
        <source>Autoselect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="75"/>
        <source>OpenGL ES 2.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="76"/>
        <source>OpenGL ES 3.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="77"/>
        <source>OpenGL ES 3.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="vanished">默认</translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="128"/>
        <source>OpenGL ES API level(requires restart)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="100"/>
        <source>Restart system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="72"/>
        <source>(Renderer maximum)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="126"/>
        <source>The modification takes effect after you restart the system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="145"/>
        <source>KMRE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="145"/>
        <location filename="../glesversionwidget.cpp" line="151"/>
        <source>Are you sure you want to restart system?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="151"/>
        <source>Restart system?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="158"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="159"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InputItem</name>
    <message>
        <location filename="../inputitem.cpp" line="34"/>
        <source>For example: the glory of the king</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../inputitem.cpp" line="35"/>
        <source>For example: com.tencent.tmgp.sgame</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KylinUI::MessageBox</name>
    <message>
        <source>Yes</source>
        <translation type="vanished">是(Y)</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">否(N)</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../messagebox.cpp" line="180"/>
        <location filename="../messagebox.cpp" line="183"/>
        <source>Tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../messagebox.cpp" line="180"/>
        <location filename="../messagebox.cpp" line="183"/>
        <source>Modify successfully!The modification takes effect only after the service is restarted. Do you want to restart the service?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../messagebox.cpp" line="184"/>
        <source>restart now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../messagebox.cpp" line="185"/>
        <source>restart later</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KylinUI::MessageBox::MessageBox</name>
    <message>
        <location filename="../messagebox.cpp" line="103"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../messagebox.cpp" line="108"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LogWidget</name>
    <message>
        <location filename="../logwidget.cpp" line="127"/>
        <source>Log collection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../logwidget.cpp" line="80"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../logwidget.cpp" line="88"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../logwidget.cpp" line="165"/>
        <source>Getting logs...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../logwidget.cpp" line="350"/>
        <source>/sbin/iptables command does not exist!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../logwidget.cpp" line="390"/>
        <source>An error occurred while getting log!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../logwidget.cpp" line="402"/>
        <source>Get log complete.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NetMaskItem</name>
    <message>
        <location filename="../netmaskitem.cpp" line="53"/>
        <source>Set the subnet mask of container docker</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="97"/>
        <source>kylin-kmre-settings is already running!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="101"/>
        <source>kylin-kmre-settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PhoneInfoWidget</name>
    <message>
        <location filename="../phoneinfowidget.cpp" line="32"/>
        <source>Phone model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="33"/>
        <source>IMEI setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="35"/>
        <source>preset model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="36"/>
        <source>custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="38"/>
        <source>vendor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="39"/>
        <source>brand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="40"/>
        <source>name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="41"/>
        <source>model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="42"/>
        <source>equip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="57"/>
        <source>random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="60"/>
        <source>default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="56"/>
        <source>save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="64"/>
        <source>PhoneInfo Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="221"/>
        <source>The Settings take effect after the environment is restarted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="216"/>
        <location filename="../phoneinfowidget.cpp" line="220"/>
        <location filename="../phoneinfowidget.cpp" line="223"/>
        <source>Modify successfully!Restart the environment to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="216"/>
        <location filename="../phoneinfowidget.cpp" line="220"/>
        <location filename="../phoneinfowidget.cpp" line="223"/>
        <source>Tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="224"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RadioButtonItem</name>
    <message>
        <location filename="../radiobuttonitem.cpp" line="60"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../radiobuttonitem.cpp" line="106"/>
        <source>KMRE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../radiobuttonitem.cpp" line="110"/>
        <location filename="../radiobuttonitem.cpp" line="113"/>
        <source>Tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../radiobuttonitem.cpp" line="106"/>
        <location filename="../radiobuttonitem.cpp" line="110"/>
        <location filename="../radiobuttonitem.cpp" line="113"/>
        <source>The modification takes effect after you restart the system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../radiobuttonitem.cpp" line="114"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RemoveGameWidget</name>
    <message>
        <location filename="../removegamewidget.cpp" line="34"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../removegamewidget.cpp" line="51"/>
        <source>appName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../removegamewidget.cpp" line="52"/>
        <source>pkgName</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsFrame</name>
    <message>
        <location filename="../settingsframe.cpp" line="110"/>
        <location filename="../settingsframe.cpp" line="140"/>
        <source>KMRE-Preference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="149"/>
        <source>Minimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="158"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="366"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="457"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="461"/>
        <source>Renderer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="318"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="327"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="366"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="374"/>
        <location filename="../settingsframe.cpp" line="377"/>
        <location filename="../settingsframe.cpp" line="383"/>
        <location filename="../settingsframe.cpp" line="386"/>
        <location filename="../settingsframe.cpp" line="465"/>
        <source>Game Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="318"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="327"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="366"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="374"/>
        <location filename="../settingsframe.cpp" line="377"/>
        <location filename="../settingsframe.cpp" line="383"/>
        <location filename="../settingsframe.cpp" line="386"/>
        <location filename="../settingsframe.cpp" line="469"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="318"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="327"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="366"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="374"/>
        <location filename="../settingsframe.cpp" line="377"/>
        <location filename="../settingsframe.cpp" line="383"/>
        <location filename="../settingsframe.cpp" line="386"/>
        <location filename="../settingsframe.cpp" line="473"/>
        <location filename="../settingsframe.cpp" line="512"/>
        <location filename="../settingsframe.cpp" line="527"/>
        <source>Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="318"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="327"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="366"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="374"/>
        <location filename="../settingsframe.cpp" line="377"/>
        <location filename="../settingsframe.cpp" line="383"/>
        <location filename="../settingsframe.cpp" line="386"/>
        <location filename="../settingsframe.cpp" line="479"/>
        <source>Images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="318"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="327"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="366"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="374"/>
        <location filename="../settingsframe.cpp" line="377"/>
        <location filename="../settingsframe.cpp" line="383"/>
        <location filename="../settingsframe.cpp" line="386"/>
        <location filename="../settingsframe.cpp" line="483"/>
        <source>Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="318"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="327"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="377"/>
        <location filename="../settingsframe.cpp" line="386"/>
        <location filename="../settingsframe.cpp" line="487"/>
        <location filename="../settingsframe.cpp" line="532"/>
        <source>AppMultiplier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="318"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="327"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="366"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="374"/>
        <location filename="../settingsframe.cpp" line="377"/>
        <location filename="../settingsframe.cpp" line="383"/>
        <location filename="../settingsframe.cpp" line="386"/>
        <location filename="../settingsframe.cpp" line="493"/>
        <location filename="../settingsframe.cpp" line="537"/>
        <source>Tray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="318"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="327"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="366"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="374"/>
        <location filename="../settingsframe.cpp" line="377"/>
        <location filename="../settingsframe.cpp" line="383"/>
        <location filename="../settingsframe.cpp" line="386"/>
        <location filename="../settingsframe.cpp" line="493"/>
        <location filename="../settingsframe.cpp" line="537"/>
        <source>PhoneInfo Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="318"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="327"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="366"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="374"/>
        <location filename="../settingsframe.cpp" line="377"/>
        <location filename="../settingsframe.cpp" line="383"/>
        <location filename="../settingsframe.cpp" line="386"/>
        <location filename="../settingsframe.cpp" line="493"/>
        <location filename="../settingsframe.cpp" line="537"/>
        <source>General Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="475"/>
        <source>Developer Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="535"/>
        <source>Tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="535"/>
        <source>Current manager vesion does not support developer mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="748"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="748"/>
        <source>Restarting the environment failed!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrayAppWidget</name>
    <message>
        <location filename="../trayappwidget.cpp" line="31"/>
        <source>Tray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../trayappwidget.cpp" line="33"/>
        <source>Set the application displayed in the tray area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../trayappwidget.cpp" line="41"/>
        <source>No items!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../trayappwidget.cpp" line="74"/>
        <source>KMRE is not running!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
