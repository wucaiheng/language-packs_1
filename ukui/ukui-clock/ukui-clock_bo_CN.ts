<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>About</name>
    <message>
        <location filename="../about.ui" line="14"/>
        <source>Dialog</source>
        <translation>རྣམ་བཞག་ཅན།</translation>
    </message>
    <message>
        <location filename="../about.ui" line="87"/>
        <location filename="../about.ui" line="162"/>
        <source>Alarm</source>
        <translation>ཉེན་བརྡ་གཏོང་བ།</translation>
    </message>
    <message>
        <source>Kylin Alarm</source>
        <translation type="vanished">麒麟 闹钟</translation>
    </message>
    <message>
        <source>Clock</source>
        <translatorcomment>闹钟</translatorcomment>
        <translation type="vanished">闹钟</translation>
    </message>
    <message>
        <source>Kylin Clock</source>
        <translatorcomment>麒麟 闹钟</translatorcomment>
        <translation type="vanished">麒麟 闹钟</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="35"/>
        <source>About</source>
        <translatorcomment>关于</translatorcomment>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="57"/>
        <source>Version: </source>
        <translatorcomment>版本：</translatorcomment>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ། </translation>
    </message>
    <message>
        <source>Version: 2020.1.0</source>
        <translatorcomment>版本： 2020.1.22</translatorcomment>
        <translation type="vanished">版本： 2020.1.22</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="122"/>
        <location filename="../about.cpp" line="130"/>
        <source>Service &amp; Support: </source>
        <translatorcomment>服务与支持团队：</translatorcomment>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར་ཚོགས་པ། </translation>
    </message>
    <message>
        <source>Support and service team: support@kylinos.cn</source>
        <translatorcomment>服务与支持团队： support@kylinos.cn</translatorcomment>
        <translation type="vanished">服务与支持团队： support@kylinos.cn</translation>
    </message>
</context>
<context>
    <name>BaseVerticalScroll</name>
    <message>
        <location filename="../baseverticalscroll.cpp" line="104"/>
        <source>AM</source>
        <translation>སྔ་དྲོ།</translation>
    </message>
    <message>
        <location filename="../baseverticalscroll.cpp" line="106"/>
        <source>PM</source>
        <translation>ཕྱི་དྲོ།</translation>
    </message>
</context>
<context>
    <name>Clock</name>
    <message>
        <source>Clock</source>
        <translatorcomment>闹钟</translatorcomment>
        <translation type="vanished">闹钟</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="239"/>
        <location filename="../clock.ui" line="363"/>
        <location filename="../clock.cpp" line="1016"/>
        <location filename="../clock.cpp" line="1121"/>
        <location filename="../clock.cpp" line="2901"/>
        <location filename="../clock.cpp" line="3238"/>
        <source>start</source>
        <translatorcomment>开始</translatorcomment>
        <translation>འགོ་ཚུགས་པ།</translation>
    </message>
    <message>
        <source>5min</source>
        <translatorcomment>5分钟</translatorcomment>
        <translation type="vanished">5分钟</translation>
    </message>
    <message>
        <source>15min</source>
        <translatorcomment>15分钟</translatorcomment>
        <translation type="vanished">15分钟</translation>
    </message>
    <message>
        <source>25min</source>
        <translatorcomment>25分钟</translatorcomment>
        <translation type="vanished">25分钟</translation>
    </message>
    <message>
        <source>30min</source>
        <translation type="vanished">30分钟</translation>
    </message>
    <message>
        <source>60min</source>
        <translation type="vanished">60分钟</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="261"/>
        <location filename="../clock.cpp" line="897"/>
        <location filename="../clock.cpp" line="2906"/>
        <location filename="../clock.cpp" line="3144"/>
        <source>suspend</source>
        <translation>ལས་མཚམས་བཞག་པ།</translation>
    </message>
    <message>
        <source>tiny window</source>
        <translation type="vanished">迷你窗口</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="290"/>
        <source>add</source>
        <translatorcomment>添加</translatorcomment>
        <translation>ཁ་སྣོན་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="336"/>
        <source>no alarm</source>
        <translatorcomment>无闹钟</translatorcomment>
        <translation>ཉེན་བརྡ་མི་གཏོང་བ།</translation>
    </message>
    <message>
        <source>delete</source>
        <translatorcomment>删除</translatorcomment>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="496"/>
        <source>save</source>
        <translatorcomment>保存</translatorcomment>
        <translation>ཉར་ཚགས།</translation>
    </message>
    <message>
        <source>Remind</source>
        <translation type="vanished">提醒铃声</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="192"/>
        <location filename="../clock.cpp" line="1827"/>
        <location filename="../clock.cpp" line="3028"/>
        <location filename="../clock.cpp" line="3118"/>
        <source>PM</source>
        <translation>ཕྱི་དྲོ།</translation>
    </message>
    <message>
        <source>add alarm</source>
        <translatorcomment>添加</translatorcomment>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Remaining time</source>
        <translation type="vanished">点击闹钟显示剩余时间</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="939"/>
        <source>reset</source>
        <translation>བསྐྱར་དུ་བཀོད་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="419"/>
        <location filename="../clock.cpp" line="918"/>
        <location filename="../clock.cpp" line="1048"/>
        <source>count</source>
        <translation>གྲངས་ཀ་རྩིས་རྒྱག</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="130"/>
        <location filename="../clock.cpp" line="2723"/>
        <location filename="../clock.cpp" line="2800"/>
        <source>Count down</source>
        <translation>དུས་ཚོད་ལྡོག་རྩིས་བྱེད་པ།</translation>
    </message>
    <message>
        <source>clock</source>
        <translation type="vanished">闹钟</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation type="vanished">秒表</translation>
    </message>
    <message>
        <source>deletealarm</source>
        <translation type="vanished">删除闹铃</translation>
    </message>
    <message>
        <source>Preservation</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <source>12hour43minThe bell rings</source>
        <translation type="vanished">12小时43分后铃声响</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="474"/>
        <source>cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>New alarm</source>
        <translation type="vanished">新建闹钟</translation>
    </message>
    <message>
        <source>  Name</source>
        <translation type="vanished">  闹钟名</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="458"/>
        <source>  repeat</source>
        <translation>  ཡང་བསྐྱར་ཐེངས་གཅིག</translation>
    </message>
    <message>
        <source>  Remind</source>
        <translation type="vanished">  铃声</translation>
    </message>
    <message>
        <source>  ring time</source>
        <translation type="vanished">  铃声时长</translation>
    </message>
    <message>
        <source> ring time</source>
        <translation type="vanished"> 铃声时长</translation>
    </message>
    <message>
        <source>开始</source>
        <translatorcomment>start</translatorcomment>
        <translation type="vanished">start</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="129"/>
        <location filename="../clock.ui" line="381"/>
        <location filename="../clock.ui" line="397"/>
        <source>00:00:00</source>
        <translation></translation>
    </message>
    <message>
        <source>5分钟</source>
        <translatorcomment>5min</translatorcomment>
        <translation type="vanished">5min</translation>
    </message>
    <message>
        <source>10分钟</source>
        <translatorcomment>10min</translatorcomment>
        <translation type="vanished">10min</translation>
    </message>
    <message>
        <source>20分钟</source>
        <translatorcomment>20min</translatorcomment>
        <translation type="vanished">20min</translation>
    </message>
    <message>
        <source>30分钟</source>
        <translatorcomment>30min</translatorcomment>
        <translation type="vanished">30min</translation>
    </message>
    <message>
        <source>60分钟</source>
        <translatorcomment>60min</translatorcomment>
        <translation type="vanished">60min</translation>
    </message>
    <message>
        <source>下午05:31</source>
        <translatorcomment>PM</translatorcomment>
        <translation type="vanished">PM</translation>
    </message>
    <message>
        <source>暂停</source>
        <translatorcomment>suspend</translatorcomment>
        <translation type="vanished">suspend</translation>
    </message>
    <message>
        <source>  提醒铃声</source>
        <translatorcomment>Remind</translatorcomment>
        <translation type="vanished"> Remind</translation>
    </message>
    <message>
        <source>添加闹钟</source>
        <translatorcomment>add alarm</translatorcomment>
        <translation type="vanished">add alarm</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">继续</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1049"/>
        <source>interval </source>
        <translation>བར་མཚམས་ཆད་པ། </translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1721"/>
        <source>recent alarm</source>
        <translation>ཉེ་ཆར་ཉེན་བརྡ་བཏང་།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1834"/>
        <location filename="../clock.cpp" line="3030"/>
        <location filename="../clock.cpp" line="3125"/>
        <source>AM</source>
        <translation>སྔ་དྲོ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1869"/>
        <location filename="../clock.cpp" line="2353"/>
        <location filename="../clock.cpp" line="3517"/>
        <location filename="../clock.cpp" line="3542"/>
        <source>Mon</source>
        <translation>གཟའ་ཟླ་བ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1871"/>
        <location filename="../clock.cpp" line="2355"/>
        <location filename="../clock.cpp" line="3518"/>
        <location filename="../clock.cpp" line="3543"/>
        <source>Tue</source>
        <translation>གཟའ་མིག་དམར།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1873"/>
        <location filename="../clock.cpp" line="2357"/>
        <location filename="../clock.cpp" line="3519"/>
        <location filename="../clock.cpp" line="3544"/>
        <source>Wed</source>
        <translation>གཟའ་ལྷག་པ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1875"/>
        <location filename="../clock.cpp" line="2359"/>
        <location filename="../clock.cpp" line="3520"/>
        <location filename="../clock.cpp" line="3545"/>
        <source>Thu</source>
        <translation>གཟའ་ཕུར་བུ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1877"/>
        <location filename="../clock.cpp" line="2361"/>
        <location filename="../clock.cpp" line="3521"/>
        <location filename="../clock.cpp" line="3546"/>
        <source>Fri</source>
        <translation>གཟའ་པ་སངས།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1879"/>
        <location filename="../clock.cpp" line="2363"/>
        <location filename="../clock.cpp" line="3522"/>
        <location filename="../clock.cpp" line="3547"/>
        <source>Sat</source>
        <translation>གཟའ་སྤེན་པ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1881"/>
        <location filename="../clock.cpp" line="2365"/>
        <location filename="../clock.cpp" line="3523"/>
        <location filename="../clock.cpp" line="3548"/>
        <source>Sun</source>
        <translation>ཉི་མ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2841"/>
        <source>60 Seconds to close</source>
        <translation>སྒོ་རྒྱག་པའི་དུས་ཚོད་སྐར་ཆ་60</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3735"/>
        <location filename="../clock.cpp" line="3788"/>
        <source>five mins late</source>
        <translation>སྐར་མ་ལྔ་འགོར་འགྱངས་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3736"/>
        <location filename="../clock.cpp" line="3791"/>
        <source>ten mins late</source>
        <translation>སྐར་མ་བཅུའི་རྗེས།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3737"/>
        <location filename="../clock.cpp" line="3794"/>
        <source>twenty mins late</source>
        <translation>སྐར་མ་ཉི་ཤུའི་རྗེས་སུ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3738"/>
        <location filename="../clock.cpp" line="3797"/>
        <source>thirsty mins late</source>
        <translation>30སྐར་མ་རྗེས་སུ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3739"/>
        <location filename="../clock.cpp" line="3800"/>
        <source>one hour late</source>
        <translation>ཆུ་ཚོད་གཅིག་འགོར་རྗེས།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3943"/>
        <source>mini window</source>
        <translation>སྒེའུ་ཁུང་ཆུང་ཆུང་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1688"/>
        <source>2min</source>
        <translation>སྐར་མ2</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="14"/>
        <source>Alarm</source>
        <translation>ཉེན་བརྡ་གཏོང་བ།</translation>
    </message>
    <message>
        <source>Count</source>
        <translation type="vanished">计次</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="134"/>
        <source>Watch</source>
        <translation>སྐར་ཆའི་རེའུ་མིག</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="173"/>
        <source>icon</source>
        <translation>མཚོན་རྟགས།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="445"/>
        <location filename="../clock.cpp" line="447"/>
        <location filename="../clock.cpp" line="462"/>
        <source>  bell</source>
        <translation>  ཅོང་བརྡ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1482"/>
        <source>Minimize</source>
        <translatorcomment>最小化</translatorcomment>
        <translation>ཆེས་ཆུང་དུ་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="258"/>
        <location filename="../clock.cpp" line="1492"/>
        <source>Quit</source>
        <translatorcomment>退出</translatorcomment>
        <translation>ཕྱིར་འཐེན་བྱ་རྒྱུ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1504"/>
        <source>Menu</source>
        <translatorcomment>菜单</translatorcomment>
        <translation>འདེམས་པང་།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="597"/>
        <source>Delete</source>
        <translatorcomment>删除</translatorcomment>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="598"/>
        <source>ClearAll</source>
        <translatorcomment>全部清空</translatorcomment>
        <translation>ཚང་མ་གཙང་སེལ།</translation>
    </message>
    <message>
        <source>Set Up</source>
        <translatorcomment>设置</translatorcomment>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>help</source>
        <translatorcomment>帮助</translatorcomment>
        <translation type="vanished">帮助</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1516"/>
        <source>Help</source>
        <translatorcomment>帮助</translatorcomment>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1517"/>
        <source>About</source>
        <translatorcomment>关于</translatorcomment>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1518"/>
        <source>Close</source>
        <translatorcomment>退出</translatorcomment>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1074"/>
        <source>up to 100 times</source>
        <translation>ཆེས་མང་ན་ཐེངས་100ལ་སླེབས་པ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="466"/>
        <source>  remind</source>
        <translation>  ཅུང་ཙམ་གྱི་རྗེས་ནས་གསལ་འདེབས།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="453"/>
        <source>  name</source>
        <translation>  ཅོང་བརྡའི་མིང་།</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="599"/>
        <source>edit</source>
        <translation>རྩོམ་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1456"/>
        <source>mute</source>
        <translation>སྐད་ཆ་མི་བཤད་པ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1460"/>
        <source>All bells are off</source>
        <translation>ཅོང་བརྡ་ཚང་མ་ལྷུང་སོང་།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1690"/>
        <source>3min</source>
        <translation>སྐར་མ3</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1692"/>
        <source>4min</source>
        <translation>སྐར་མ4</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1694"/>
        <source>6min</source>
        <translation>སྐར་མ6</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1701"/>
        <location filename="../clock.cpp" line="1896"/>
        <location filename="../clock.cpp" line="2272"/>
        <location filename="../clock.cpp" line="2379"/>
        <location filename="../clock.cpp" line="2380"/>
        <location filename="../clock.cpp" line="3515"/>
        <location filename="../clock.cpp" line="3553"/>
        <location filename="../clock.cpp" line="3554"/>
        <source>No repetition</source>
        <translation>བསྐྱར་ཟློས་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <source> Seconds to close</source>
        <translation type="vanished"> 秒后关闭</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1894"/>
        <location filename="../clock.cpp" line="1965"/>
        <location filename="../clock.cpp" line="3516"/>
        <location filename="../clock.cpp" line="3568"/>
        <location filename="../clock.cpp" line="3569"/>
        <source>Workingday</source>
        <translation>ལས་ཞག་ཉིན།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1967"/>
        <source>(default)</source>
        <translation>(སོར་བཞག)</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1972"/>
        <location filename="../clock.cpp" line="3734"/>
        <location filename="../clock.cpp" line="3781"/>
        <location filename="../clock.cpp" line="3785"/>
        <location filename="../clock.cpp" line="3803"/>
        <source>none</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2037"/>
        <location filename="../clock.cpp" line="2091"/>
        <source>Please set alarm name!</source>
        <translatorcomment>请设置闹钟名!</translatorcomment>
        <translation>ཉེན་བརྡའི་མིང་ཞིག་འགོད་རོགས།</translation>
    </message>
    <message>
        <source>hour </source>
        <translation type="vanished">小时 </translation>
    </message>
    <message>
        <source> min bell rings</source>
        <translation type="vanished"> 分钟后铃响</translation>
    </message>
    <message>
        <source>Edit alarm clock</source>
        <translation type="vanished">编辑闹钟</translation>
    </message>
    <message>
        <source>点击闹钟显示剩余时间</source>
        <translatorcomment>Remaining time</translatorcomment>
        <translation type="vanished">Remaining time</translation>
    </message>
    <message>
        <source> days </source>
        <translation type="vanished"> 天 </translation>
    </message>
    <message>
        <source> hour </source>
        <translation type="vanished"> 小时 </translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2269"/>
        <source>glass</source>
        <translation>ཤེལ་སྒོ།</translation>
    </message>
    <message>
        <source>minimize</source>
        <translatorcomment>最小化</translatorcomment>
        <translation type="vanished">最小化</translation>
    </message>
    <message>
        <source>close</source>
        <translatorcomment>关闭</translatorcomment>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>menu</source>
        <translatorcomment>菜单</translatorcomment>
        <translation type="vanished">菜单</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">犬吠</translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">声呐</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">雨滴</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="928"/>
        <location filename="../clock.cpp" line="3164"/>
        <source>continue</source>
        <translation>མུ་མཐུད་དུ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2104"/>
        <source>warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2104"/>
        <source>the number of alarms reaches limit!！</source>
        <translation>ཉེན་བརྡའི་གྲངས་འབོར་ཚད་བཀག་ལ་སླེབས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2104"/>
        <source>yes</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2376"/>
        <location filename="../clock.cpp" line="2377"/>
        <source>  work</source>
        <translation>  ལས་ཀ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2376"/>
        <source>  工作日</source>
        <translation>  ལས་ཀའི་ཉིན།</translation>
    </message>
    <message>
        <source>360 Seconds to close</source>
        <translation type="vanished">360秒后关闭</translation>
    </message>
    <message>
        <source>Time out</source>
        <translation type="vanished">时间到</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2880"/>
        <source>End</source>
        <translation>མཇུག་སྒྲིལ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3013"/>
        <source>after tomorrow</source>
        <translation>གནང་ཉིན།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3016"/>
        <source>Tomorrow</source>
        <translation>སང་ཉིན།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3208"/>
        <location filename="../clock.cpp" line="3461"/>
        <source>hour</source>
        <translation>དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3211"/>
        <location filename="../clock.cpp" line="3464"/>
        <source>min</source>
        <translation>སྐར་མ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3214"/>
        <source>sec</source>
        <translation>སྐར་ཆ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1891"/>
        <location filename="../clock.cpp" line="1892"/>
        <location filename="../clock.cpp" line="2374"/>
        <location filename="../clock.cpp" line="3685"/>
        <source>Every day</source>
        <translation>ཉིན་ལྟར།</translation>
    </message>
    <message>
        <source>glass(default)</source>
        <translation type="vanished">玻璃(默认)</translation>
    </message>
    <message>
        <source>bark(default)</source>
        <translation type="vanished">犬吠(默认)</translation>
    </message>
    <message>
        <source>sonar(default)</source>
        <translation type="vanished">声呐(默认)</translation>
    </message>
    <message>
        <source>drip(default)</source>
        <translation type="vanished">雨滴(默认)</translation>
    </message>
    <message>
        <source>1min</source>
        <translation type="vanished">1分钟</translation>
    </message>
    <message>
        <source>Monday to Friday</source>
        <translation type="vanished">周一周二周三周四周五</translation>
    </message>
    <message>
        <source>24 hour system</source>
        <translation type="vanished">24小时制</translation>
    </message>
    <message>
        <source>Notification</source>
        <translation type="vanished">通知栏弹窗</translation>
    </message>
    <message>
        <source>Alert in 2 minutes</source>
        <translation type="vanished">2分钟后提醒</translation>
    </message>
    <message>
        <source>复位</source>
        <translatorcomment>reset</translatorcomment>
        <translation type="vanished">reset</translation>
    </message>
    <message>
        <source>计次</source>
        <translatorcomment>count</translatorcomment>
        <translation type="vanished">count</translation>
    </message>
    <message>
        <source>删除闹铃</source>
        <translatorcomment>deletealarm</translatorcomment>
        <translation type="vanished">deletealarm</translation>
    </message>
    <message>
        <source>保存</source>
        <translatorcomment>Preservation</translatorcomment>
        <translation type="vanished">Preservation</translation>
    </message>
    <message>
        <source>倒计时</source>
        <translatorcomment>Count down</translatorcomment>
        <translation type="vanished">Count down</translation>
    </message>
    <message>
        <source>闹钟</source>
        <translatorcomment>Alarm</translatorcomment>
        <translation type="obsolete">Alarm</translation>
    </message>
    <message>
        <source>秒表</source>
        <translatorcomment>Stopwatch</translatorcomment>
        <translation type="vanished">Stopwatch</translation>
    </message>
    <message>
        <source>12小时43分后铃声响</source>
        <translatorcomment>12hour43minThe bell rings</translatorcomment>
        <translation type="vanished">12hour43minThe bell rings</translation>
    </message>
    <message>
        <source>取消</source>
        <translatorcomment>cancel</translatorcomment>
        <translation type="vanished">cancel</translation>
    </message>
    <message>
        <source>新建闹钟</source>
        <translatorcomment>New alarm</translatorcomment>
        <translation type="vanished">New alarm</translation>
    </message>
    <message>
        <source>  闹钟名</source>
        <translatorcomment> Name</translatorcomment>
        <translation type="vanished"> Name</translation>
    </message>
    <message>
        <source>  重复</source>
        <translatorcomment> repeat</translatorcomment>
        <translation type="vanished"> repeat</translation>
    </message>
    <message>
        <source>  铃声时长</source>
        <translatorcomment> ring time</translatorcomment>
        <translation type="vanished"> ring time</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="669"/>
        <source>On</source>
        <translation>མུ་མཐུད་དུ་བཤད་</translation>
    </message>
    <message>
        <source>继续</source>
        <translatorcomment>Continue</translatorcomment>
        <translation type="vanished">Continue</translation>
    </message>
    <message>
        <source>间隔 </source>
        <translatorcomment>interval </translatorcomment>
        <translation type="obsolete">interval </translation>
    </message>
    <message>
        <source>下午</source>
        <translatorcomment>PM</translatorcomment>
        <translation type="vanished">PM</translation>
    </message>
    <message>
        <source>上午</source>
        <translatorcomment>AM</translatorcomment>
        <translation type="vanished">AM</translation>
    </message>
    <message>
        <source>周一</source>
        <translatorcomment>Mon</translatorcomment>
        <translation type="vanished">Mon</translation>
    </message>
    <message>
        <source>周二</source>
        <translatorcomment>Tue</translatorcomment>
        <translation type="vanished">Tue</translation>
    </message>
    <message>
        <source>周三</source>
        <translatorcomment>Wed</translatorcomment>
        <translation type="vanished">Wed</translation>
    </message>
    <message>
        <source>周四</source>
        <translatorcomment>Thu</translatorcomment>
        <translation type="vanished">Thu</translation>
    </message>
    <message>
        <source>周五</source>
        <translatorcomment>Fri</translatorcomment>
        <translation type="vanished">Fri</translation>
    </message>
    <message>
        <source>周六</source>
        <translatorcomment>Sat</translatorcomment>
        <translation type="vanished">Sat</translation>
    </message>
    <message>
        <source>周日</source>
        <translatorcomment>Sun</translatorcomment>
        <translation type="vanished">Sun</translation>
    </message>
    <message>
        <source>秒后自动关闭</source>
        <translatorcomment> Seconds to close</translatorcomment>
        <translation type="vanished"> Seconds to close</translation>
    </message>
    <message>
        <source>2分钟后提醒</source>
        <translation type="vanished">Alert in 2 minutes</translation>
    </message>
    <message>
        <source>5分钟后提醒</source>
        <translation type="vanished">Alert in 5 minutes</translation>
    </message>
    <message>
        <source>10分钟后提醒</source>
        <translation type="vanished">Alert in 10 minutes</translation>
    </message>
    <message>
        <source>30分钟后提醒</source>
        <translation type="vanished">Alert in 30 minutes</translation>
    </message>
    <message>
        <source>60分钟后提醒</source>
        <translation type="vanished">Alert in 60 minutes</translation>
    </message>
    <message>
        <source>天</source>
        <translation type="obsolete"> days </translation>
    </message>
    <message>
        <source>玻璃</source>
        <translatorcomment>glass</translatorcomment>
        <translation type="vanished">glass</translation>
    </message>
    <message>
        <source>犬吠</source>
        <translatorcomment>bark</translatorcomment>
        <translation type="vanished">bark</translation>
    </message>
    <message>
        <source>声呐</source>
        <translatorcomment>sonar</translatorcomment>
        <translation type="vanished">sonar</translation>
    </message>
    <message>
        <source>雨滴</source>
        <translatorcomment>drip</translatorcomment>
        <translation type="vanished">drip</translation>
    </message>
    <message>
        <source>2分钟</source>
        <translatorcomment>2min</translatorcomment>
        <translation type="vanished">2min</translation>
    </message>
    <message>
        <source>3分钟</source>
        <translatorcomment>3min</translatorcomment>
        <translation type="vanished">3min</translation>
    </message>
    <message>
        <source>4分钟</source>
        <translatorcomment>4min</translatorcomment>
        <translation type="vanished">4min</translation>
    </message>
    <message>
        <source>6分钟</source>
        <translatorcomment>6min</translatorcomment>
        <translation type="vanished">6min</translation>
    </message>
    <message>
        <source>工作日</source>
        <translatorcomment>Workingday</translatorcomment>
        <translation type="vanished">Workingday</translation>
    </message>
    <message>
        <source>(默认)</source>
        <translatorcomment>(default)</translatorcomment>
        <translation type="vanished">(default)</translation>
    </message>
    <message>
        <source>每天</source>
        <translatorcomment>Every day</translatorcomment>
        <translation type="vanished">Every day</translation>
    </message>
    <message>
        <source>1分钟</source>
        <translatorcomment>1min</translatorcomment>
        <translation type="vanished">1min</translation>
    </message>
    <message>
        <source>小时</source>
        <translatorcomment> hour </translatorcomment>
        <translation type="obsolete"> hour </translation>
    </message>
    <message>
        <source>分钟后铃响</source>
        <translatorcomment> min bell rings</translatorcomment>
        <translation type="obsolete"> min bell rings</translation>
    </message>
    <message>
        <source>编辑闹钟</source>
        <translatorcomment>Edit alarm clock</translatorcomment>
        <translation type="vanished">Edit alarm clock</translation>
    </message>
    <message>
        <source>删除当前闹钟！</source>
        <translatorcomment>delete alame clock !</translatorcomment>
        <translation type="vanished">delete alame clock !</translation>
    </message>
    <message>
        <source>您确定删除当前闹钟吗？</source>
        <translatorcomment>are you sure ?</translatorcomment>
        <translation type="vanished">are you sure ?</translation>
    </message>
    <message>
        <source>倒计时时间结束</source>
        <translatorcomment>End of countdown time</translatorcomment>
        <translation type="vanished">End of countdown time</translation>
    </message>
    <message>
        <source>结束</source>
        <translatorcomment>End</translatorcomment>
        <translation type="vanished">End</translation>
    </message>
    <message>
        <source>明日</source>
        <translatorcomment>Tom</translatorcomment>
        <translation type="vanished">Tom</translation>
    </message>
    <message>
        <source>360秒后自动关闭</source>
        <translatorcomment>360 Seconds to close</translatorcomment>
        <translation type="obsolete">360 Seconds to close</translation>
    </message>
    <message>
        <source>时间到</source>
        <translation type="vanished">Time out</translation>
    </message>
    <message>
        <source>后天</source>
        <translatorcomment>after tomorrow</translatorcomment>
        <translation type="vanished">after tomorrow</translation>
    </message>
    <message>
        <source>明天</source>
        <translatorcomment>Tomorrow</translatorcomment>
        <translation type="vanished">Tomorrow</translation>
    </message>
    <message>
        <source>时</source>
        <translatorcomment>hour</translatorcomment>
        <translation type="vanished">hour</translation>
    </message>
    <message>
        <source>分</source>
        <translatorcomment>min</translatorcomment>
        <translation type="vanished">min</translation>
    </message>
    <message>
        <source>秒</source>
        <translatorcomment>sec</translatorcomment>
        <translation type="vanished">sec</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2379"/>
        <source>不重复</source>
        <translatorcomment>No repetition </translatorcomment>
        <translation>不重复</translation>
    </message>
    <message>
        <source>玻璃(默认)</source>
        <translatorcomment>glass(default)</translatorcomment>
        <translation type="vanished">glass(default)</translation>
    </message>
    <message>
        <source>犬吠(默认)</source>
        <translatorcomment>bark(default)</translatorcomment>
        <translation type="obsolete">bark(default)</translation>
    </message>
    <message>
        <source>声呐(默认)</source>
        <translatorcomment>sonar(default)</translatorcomment>
        <translation type="obsolete">sonar(default)</translation>
    </message>
    <message>
        <source>雨滴(默认)</source>
        <translatorcomment>drip(default)</translatorcomment>
        <translation type="obsolete">drip(default)</translation>
    </message>
    <message>
        <source>1分钟(默认)</source>
        <translatorcomment>1min(default)</translatorcomment>
        <translation type="obsolete">1min(default)</translation>
    </message>
    <message>
        <source>2分钟(默认)</source>
        <translatorcomment>2min(default)</translatorcomment>
        <translation type="obsolete">2min(default)</translation>
    </message>
    <message>
        <source>3分钟(默认)</source>
        <translatorcomment>3min(default)</translatorcomment>
        <translation type="obsolete">3min(default)</translation>
    </message>
    <message>
        <source>4分钟(默认)</source>
        <translatorcomment>4min(default)</translatorcomment>
        <translation type="obsolete">4min(default)</translation>
    </message>
    <message>
        <source>6分钟(默认)</source>
        <translatorcomment>6min(default)</translatorcomment>
        <translation type="obsolete">6min(default)</translation>
    </message>
    <message>
        <source>周一周二周三周四周五</source>
        <translation type="obsolete">Monday to Friday</translation>
    </message>
    <message>
        <source>24小时制(23:59:59)</source>
        <translatorcomment>24 hour system </translatorcomment>
        <translation type="obsolete">24 hour system</translation>
    </message>
    <message>
        <source>通知栏弹窗</source>
        <translatorcomment>Notification</translatorcomment>
        <translation type="obsolete">Notification</translation>
    </message>
    <message>
        <source>一分钟后自动关闭</source>
        <translatorcomment>Turn off after 1 min</translatorcomment>
        <translation type="obsolete">Turn off after 1 min</translation>
    </message>
</context>
<context>
    <name>Natice_alarm</name>
    <message>
        <location filename="../noticeAlarm.ui" line="14"/>
        <source>Form</source>
        <translation>རེའུ་མིག</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.ui" line="120"/>
        <source>Alarm clock</source>
        <translation>ཅོང་བརྡ།</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.ui" line="227"/>
        <source>11:20 设计例会...</source>
        <translation>11:20 འཆར་ཅན་ཚོགས་འདུ།</translation>
    </message>
    <message>
        <source>60秒后自动关闭</source>
        <translation type="vanished">360 Seconds to close {60秒?}</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.ui" line="334"/>
        <location filename="../noticeAlarm.cpp" line="162"/>
        <source>Remind later</source>
        <translation>ཅུང་ཙམ་གྱི་རྗེས་ནས་གསལ་འདེབས།</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="57"/>
        <source>Ring prompt</source>
        <translation>ཅོང་བརྡ་གྲགས་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="216"/>
        <source>none</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="223"/>
        <source>Time out</source>
        <translation>དུས་ཚོད་ཕྱིར་འཐེན་བྱེད་པ།</translation>
    </message>
    <message>
        <source>glass</source>
        <translation type="vanished">玻璃</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">犬吠</translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">声呐</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">雨滴</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="437"/>
        <source> Seconds to close</source>
        <translation> དུས་ཚོད་སྐར་ཆ་ཙམ་ཡང་སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>Notice_Dialog</name>
    <message>
        <source>Ring prompt</source>
        <translation type="vanished">响铃提示</translation>
    </message>
    <message>
        <source>glass</source>
        <translation type="vanished">玻璃</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">犬吠</translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">声呐</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">雨滴</translation>
    </message>
    <message>
        <source>End of countdown time</source>
        <translation type="vanished">End of countdown time</translation>
    </message>
    <message>
        <source>秒后关闭铃声</source>
        <translation type="vanished">秒后关闭铃声</translation>
    </message>
    <message>
        <source>闹钟:</source>
        <translation type="vanished">闹钟:</translation>
    </message>
    <message>
        <source>起床铃</source>
        <translation type="vanished">起床铃</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../clock.cpp" line="2454"/>
        <source>Hint</source>
        <translation>གསལ་འདེབས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2455"/>
        <source>Are you sure to delete？</source>
        <translation>ཁྱོད་ཀྱིས་ངེས་པར་དུ་ཅོང་བརྡ་བསུབ་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2456"/>
        <source>sure</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2457"/>
        <source>cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
</context>
<context>
    <name>SelectBtnUtil</name>
    <message>
        <source>relax</source>
        <translation type="vanished">放松</translation>
    </message>
    <message>
        <source>emotion</source>
        <translation type="vanished">情感</translation>
    </message>
    <message>
        <source>silence</source>
        <translation type="vanished">静谧</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="196"/>
        <source>glass</source>
        <translation>ཤེལ་སྒོ།</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="197"/>
        <source>bark</source>
        <translation>ཁྱི་ཟུག</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="198"/>
        <source>sonar</source>
        <translation>སྒྲ་དུང་།</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="199"/>
        <source>drip</source>
        <translation>ཆུའི་ཐིགས་པ།</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="167"/>
        <source>diy bell</source>
        <translation>རང་ཉིད་ཀྱིས་ཅོང་བརྡ་ལ་མཚན་ཉིད་བཞག</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="195"/>
        <source>none</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="290"/>
        <source>select bell</source>
        <translation>འདེམས་པའི་ཅོང་བརྡ།</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="288"/>
        <source>audio files(*mp3 *wav *ogg)</source>
        <translation>རིགས།(*mp3 *wav *ogg)</translation>
    </message>
</context>
<context>
    <name>TestWidget</name>
    <message>
        <location filename="../countdownAnimation.cpp" line="110"/>
        <source>TestWidget</source>
        <translation>ཚད་ལེན་ཚོད་ལྟ་ཁང་།</translation>
    </message>
</context>
<context>
    <name>VerticalScroll_24</name>
    <message>
        <source>PM</source>
        <translation type="vanished">ཕྱི་དྲོ།</translation>
    </message>
    <message>
        <source>AM</source>
        <translation type="vanished">སྔ་དྲོ།</translation>
    </message>
    <message>
        <location filename="../verticalScroll24.cpp" line="245"/>
        <source>VerticalScroll_24</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>VerticalScroll_60</name>
    <message>
        <location filename="../verticalScroll60.cpp" line="177"/>
        <source>VerticalScroll_60</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>VerticalScroll_99</name>
    <message>
        <location filename="../verticalScroll99.cpp" line="188"/>
        <source>VerticalScroll_99</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>VerticalScroll_APM</name>
    <message>
        <location filename="../verticalscrollapm.cpp" line="173"/>
        <source>VerticalScroll_APM</source>
        <translation>VerticalScroll_APM</translation>
    </message>
</context>
<context>
    <name>close_or_hide</name>
    <message>
        <location filename="../closeOrHide.ui" line="14"/>
        <source>Dialog</source>
        <translation>རྣམ་བཞག་ཅན།</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="310"/>
        <location filename="../closeOrHide.cpp" line="78"/>
        <source>sure</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="406"/>
        <source>请选择关闭后的状态</source>
        <translation>སྒོ་བརྒྱབ་རྗེས་ཀྱི་རྣམ་པ་འདེམས་རོགས།</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="270"/>
        <location filename="../closeOrHide.cpp" line="87"/>
        <source>cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="126"/>
        <source> backstage</source>
        <translation> རྒྱབ་སྟེགས་ལག་བསྟར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>backstage</source>
        <translation type="vanished">后台运行</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="178"/>
        <source> Exit program </source>
        <translation> ཐད་ར་ཕྱིར་འཐེན། </translation>
    </message>
    <message>
        <location filename="../closeOrHide.cpp" line="46"/>
        <source>Please select the state after closing:</source>
        <translation>སྒོ་བརྒྱབ་རྗེས་གནས་ཚུལ་གདམ་གསེས་གནང་རོགས།</translation>
    </message>
</context>
<context>
    <name>delete_msg</name>
    <message>
        <location filename="../deleteMsg.ui" line="14"/>
        <source>Dialog</source>
        <translation>རྣམ་བཞག་ཅན།</translation>
    </message>
    <message>
        <location filename="../deleteMsg.ui" line="241"/>
        <source>sure</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../deleteMsg.ui" line="206"/>
        <source>cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../deleteMsg.ui" line="85"/>
        <source>are you sure ?</source>
        <translation>ཁྱེད་ཀྱིས་མིག་སྔའི་ཅོང་བརྡ་བསུབ་རྒྱུ་གཏན་འཁེལ་ཡིན་ནམ།</translation>
    </message>
</context>
<context>
    <name>item_new</name>
    <message>
        <location filename="../itemNew.cpp" line="89"/>
        <source>Form</source>
        <translation>རེའུ་མིག</translation>
    </message>
</context>
<context>
    <name>set_alarm_repeat_Dialog</name>
    <message>
        <location filename="../setAlarmRepeatDialog.cpp" line="40"/>
        <source>Alarm</source>
        <translation>ཅོང་བརྡ།</translation>
    </message>
    <message>
        <location filename="../setAlarmRepeatDialog.cpp" line="185"/>
        <source>Dialog</source>
        <translation>རྣམ་བཞག་ཅན།</translation>
    </message>
</context>
<context>
    <name>setuppage</name>
    <message>
        <source>开机启动</source>
        <translatorcomment> Boot up</translatorcomment>
        <translation type="vanished"> Boot up</translation>
    </message>
    <message>
        <source>Boot up</source>
        <translation type="vanished">开机启动</translation>
    </message>
    <message>
        <source>  work</source>
        <translation type="vanished">  工作日</translation>
    </message>
    <message>
        <source>  Time</source>
        <translation type="vanished">  时间格式</translation>
    </message>
    <message>
        <source>  Pop-up</source>
        <translation type="vanished">  弹窗方式</translation>
    </message>
    <message>
        <source>  duration</source>
        <translation type="vanished">  稍后提醒</translation>
    </message>
    <message>
        <source>  ringtone</source>
        <translation type="vanished">  默认铃声</translation>
    </message>
    <message>
        <source>  Mute</source>
        <translation type="vanished">  静音</translation>
    </message>
    <message>
        <source>work</source>
        <translation type="vanished">工作日</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">时间格式</translation>
    </message>
    <message>
        <source>Pop-up</source>
        <translation type="vanished">弹窗方式</translation>
    </message>
    <message>
        <source>duration</source>
        <translation type="vanished">稍后提醒</translation>
    </message>
    <message>
        <source>ringtone</source>
        <translation type="vanished">默认铃声</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation type="vanished">静音</translation>
    </message>
    <message>
        <source>volume</source>
        <translation type="vanished">铃声音量</translation>
    </message>
    <message>
        <source>setting</source>
        <translatorcomment>设置</translatorcomment>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Mon</source>
        <translation type="vanished">周一</translation>
    </message>
    <message>
        <source>Tue</source>
        <translation type="vanished">周二</translation>
    </message>
    <message>
        <source>Wed</source>
        <translation type="vanished">周三</translation>
    </message>
    <message>
        <source>Thu</source>
        <translation type="vanished">周四</translation>
    </message>
    <message>
        <source>Fri</source>
        <translation type="vanished">周五</translation>
    </message>
    <message>
        <source>Sat</source>
        <translation type="vanished">周六</translation>
    </message>
    <message>
        <source>Sun</source>
        <translation type="vanished">周日</translation>
    </message>
    <message>
        <source>Every day</source>
        <translation type="vanished">每天</translation>
    </message>
    <message>
        <source>Following system</source>
        <translation type="vanished">跟随系统</translation>
    </message>
    <message>
        <source>  time</source>
        <translation type="vanished">  时间格式</translation>
    </message>
    <message>
        <source>24 hour system</source>
        <translation type="vanished">24小时制</translation>
    </message>
    <message>
        <source>12 hour system</source>
        <translation type="vanished">12小时制</translation>
    </message>
    <message>
        <source>Notification</source>
        <translation type="vanished">通知栏弹窗</translation>
    </message>
    <message>
        <source>Full screen</source>
        <translation type="vanished">全屏弹窗</translation>
    </message>
    <message>
        <source>Alert in 2 minutes</source>
        <translation type="vanished">2分钟后提醒</translation>
    </message>
    <message>
        <source>Alert in 5 minutes</source>
        <translation type="vanished">5分钟后提醒</translation>
    </message>
    <message>
        <source>Alert in 10 minutes</source>
        <translation type="vanished">10分钟后提醒</translation>
    </message>
    <message>
        <source>Alert in 30 minutes</source>
        <translation type="vanished">30分钟后提醒</translation>
    </message>
    <message>
        <source>Alert in 60 minutes</source>
        <translation type="vanished">60分钟后提醒</translation>
    </message>
    <message>
        <source>glass</source>
        <translation type="vanished">玻璃</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">犬吠</translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">声呐</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">雨滴</translation>
    </message>
</context>
<context>
    <name>stopwatch_item</name>
    <message>
        <source>longest</source>
        <translation type="vanished">最长</translation>
    </message>
    <message>
        <location filename="../stopwatchItem.cpp" line="74"/>
        <source>Form</source>
        <translation>རེའུ་མིག</translation>
    </message>
    <message>
        <location filename="../stopwatchItem.cpp" line="51"/>
        <location filename="../stopwatchItem.cpp" line="114"/>
        <source>max</source>
        <translation>ཆེས་རིང་བ།</translation>
    </message>
    <message>
        <location filename="../stopwatchItem.cpp" line="122"/>
        <source>min</source>
        <translation>ཆེས་ཐུང་བ།</translation>
    </message>
    <message>
        <source>shortest</source>
        <translation type="vanished">最短</translation>
    </message>
</context>
<context>
    <name>tinyCountdown</name>
    <message>
        <location filename="../tinycountdown.ui" line="26"/>
        <source>Form</source>
        <translation>རེའུ་མིག</translation>
    </message>
    <message>
        <source>Countdown</source>
        <translation type="vanished">倒计时</translation>
    </message>
    <message>
        <location filename="../tinycountdown.ui" line="78"/>
        <source>01:29:58</source>
        <translation></translation>
    </message>
    <message>
        <source>switch</source>
        <translation type="vanished">切换</translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="281"/>
        <source>close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="289"/>
        <source>main window</source>
        <translation>སྒེའུ་ཁུང་གཙོ་བོ།</translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="301"/>
        <source>suspend</source>
        <translation>ལས་མཚམས་བཞག་པ།</translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="320"/>
        <source>finish</source>
        <translation>མཇུག་སྒྲིལ།</translation>
    </message>
</context>
</TS>
