<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>About</name>
    <message>
        <location filename="../about.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../about.ui" line="87"/>
        <location filename="../about.ui" line="162"/>
        <source>Alarm</source>
        <translation>ᠰᠡᠷᠢᠬᠦᠯᠬᠡᠳᠦ ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>Kylin Alarm</source>
        <translation type="vanished">麒麟 闹钟</translation>
    </message>
    <message>
        <source>Clock</source>
        <translatorcomment>闹钟</translatorcomment>
        <translation type="vanished">闹钟</translation>
    </message>
    <message>
        <source>Kylin Clock</source>
        <translatorcomment>麒麟 闹钟</translatorcomment>
        <translation type="vanished">麒麟 闹钟</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="35"/>
        <source>About</source>
        <translatorcomment>关于</translatorcomment>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="57"/>
        <source>Version: </source>
        <translatorcomment>版本：</translatorcomment>
        <translation>ᠬᠡᠪᠯᠡᠯ: </translation>
    </message>
    <message>
        <source>Version: 2020.1.0</source>
        <translatorcomment>版本： 2020.1.22</translatorcomment>
        <translation type="vanished">版本： 2020.1.22</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="116"/>
        <location filename="../about.cpp" line="124"/>
        <source>Service &amp; Support: </source>
        <translatorcomment>服务与支持团队：</translatorcomment>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠪᠠ ᠳᠡᠮᠵᠢᠯᠬᠡ᠎ᠶ᠋ᠢᠨ ᠪᠦᠯᠬᠦᠮ᠄ </translation>
    </message>
    <message>
        <source>Support and service team: support@kylinos.cn</source>
        <translatorcomment>服务与支持团队： support@kylinos.cn</translatorcomment>
        <translation type="vanished">服务与支持团队： support@kylinos.cn</translation>
    </message>
</context>
<context>
    <name>Clock</name>
    <message>
        <source>Clock</source>
        <translatorcomment>闹钟</translatorcomment>
        <translation type="vanished">闹钟</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="460"/>
        <location filename="../clock.ui" line="582"/>
        <location filename="../clock.cpp" line="867"/>
        <location filename="../clock.cpp" line="965"/>
        <location filename="../clock.cpp" line="2596"/>
        <location filename="../clock.cpp" line="2913"/>
        <source>start</source>
        <translatorcomment>开始</translatorcomment>
        <translation>ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>5min</source>
        <translatorcomment>5分钟</translatorcomment>
        <translation type="vanished">5分钟</translation>
    </message>
    <message>
        <source>15min</source>
        <translatorcomment>15分钟</translatorcomment>
        <translation type="vanished">15分钟</translation>
    </message>
    <message>
        <source>25min</source>
        <translatorcomment>25分钟</translatorcomment>
        <translation type="vanished">25分钟</translation>
    </message>
    <message>
        <source>30min</source>
        <translation type="vanished">30分钟</translation>
    </message>
    <message>
        <source>60min</source>
        <translation type="vanished">60分钟</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="482"/>
        <location filename="../clock.cpp" line="749"/>
        <location filename="../clock.cpp" line="2601"/>
        <location filename="../clock.cpp" line="2823"/>
        <source>suspend</source>
        <translation>ᠵᠤᠭ᠍ᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>tiny window</source>
        <translation type="vanished">迷你窗口</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="511"/>
        <source>add</source>
        <translatorcomment>添加</translatorcomment>
        <translation>ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="554"/>
        <source>no alarm</source>
        <translatorcomment>无闹钟</translatorcomment>
        <translation>ᠰᠡᠷᠢᠬᠦᠯᠬᠡᠳᠦ ᠴᠠᠭ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>delete</source>
        <translatorcomment>删除</translatorcomment>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="715"/>
        <source>save</source>
        <translatorcomment>保存</translatorcomment>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Remind</source>
        <translation type="vanished">提醒铃声</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="413"/>
        <location filename="../clock.cpp" line="1577"/>
        <location filename="../clock.cpp" line="2720"/>
        <location filename="../clock.cpp" line="2797"/>
        <source>PM</source>
        <translation>ᠦᠳᠡ ᠵᠢᠨ ᠬᠤᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>add alarm</source>
        <translatorcomment>添加</translatorcomment>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Remaining time</source>
        <translation type="vanished">点击闹钟显示剩余时间</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="791"/>
        <source>reset</source>
        <translation>ᠬᠡᠪ ᠲᠤ᠌ ᠪᠡᠨ ᠤᠷᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="638"/>
        <location filename="../clock.cpp" line="770"/>
        <location filename="../clock.cpp" line="895"/>
        <source>count</source>
        <translation>ᠳᠤᠭᠠᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="213"/>
        <location filename="../clock.cpp" line="123"/>
        <location filename="../clock.cpp" line="2433"/>
        <location filename="../clock.cpp" line="2501"/>
        <source>Count down</source>
        <translation>ᠡᠳᠦᠷ ᠬᠠᠰᠤᠨ ᠳᠤᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>clock</source>
        <translation type="vanished">闹钟</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation type="vanished">秒表</translation>
    </message>
    <message>
        <source>deletealarm</source>
        <translation type="vanished">删除闹铃</translation>
    </message>
    <message>
        <source>Preservation</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <source>12hour43minThe bell rings</source>
        <translation type="vanished">12小时43分后铃声响</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="693"/>
        <source>cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>New alarm</source>
        <translation type="vanished">新建闹钟</translation>
    </message>
    <message>
        <source>  Name</source>
        <translation type="vanished">  闹钟名</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="357"/>
        <source>  repeat</source>
        <translation>  ᠳᠠᠬᠢᠨ ᠳᠠᠪᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <source>  Remind</source>
        <translation type="vanished">  铃声</translation>
    </message>
    <message>
        <source>  ring time</source>
        <translation type="vanished">  铃声时长</translation>
    </message>
    <message>
        <source> ring time</source>
        <translation type="vanished"> 铃声时长</translation>
    </message>
    <message>
        <source>开始</source>
        <translatorcomment>start</translatorcomment>
        <translation type="vanished">start</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="350"/>
        <location filename="../clock.ui" line="600"/>
        <location filename="../clock.ui" line="616"/>
        <source>00:00:00</source>
        <translation></translation>
    </message>
    <message>
        <source>5分钟</source>
        <translatorcomment>5min</translatorcomment>
        <translation type="vanished">5min</translation>
    </message>
    <message>
        <source>10分钟</source>
        <translatorcomment>10min</translatorcomment>
        <translation type="vanished">10min</translation>
    </message>
    <message>
        <source>20分钟</source>
        <translatorcomment>20min</translatorcomment>
        <translation type="vanished">20min</translation>
    </message>
    <message>
        <source>30分钟</source>
        <translatorcomment>30min</translatorcomment>
        <translation type="vanished">30min</translation>
    </message>
    <message>
        <source>60分钟</source>
        <translatorcomment>60min</translatorcomment>
        <translation type="vanished">60min</translation>
    </message>
    <message>
        <source>下午05:31</source>
        <translatorcomment>PM</translatorcomment>
        <translation type="vanished">PM</translation>
    </message>
    <message>
        <source>暂停</source>
        <translatorcomment>suspend</translatorcomment>
        <translation type="vanished">suspend</translation>
    </message>
    <message>
        <source>  提醒铃声</source>
        <translatorcomment>Remind</translatorcomment>
        <translation type="vanished"> Remind</translation>
    </message>
    <message>
        <source>添加闹钟</source>
        <translatorcomment>add alarm</translatorcomment>
        <translation type="vanished">add alarm</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">继续</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="896"/>
        <source>interval </source>
        <translation>ᠠᠯᠤᠰᠯᠠᠬᠤ </translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1483"/>
        <source>recent alarm</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠤᠢᠷᠠᠬᠢ ᠰᠡᠷᠢᠬᠦᠯᠬᠡᠳᠦ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1584"/>
        <location filename="../clock.cpp" line="2722"/>
        <location filename="../clock.cpp" line="2804"/>
        <source>AM</source>
        <translation>ᠦᠳᠡ ᠵᠢᠨ ᠡᠮᠦᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1619"/>
        <location filename="../clock.cpp" line="2074"/>
        <location filename="../clock.cpp" line="3124"/>
        <location filename="../clock.cpp" line="3149"/>
        <source>Mon</source>
        <translation>ᠭᠠᠷᠠᠭ ᠤ᠋ᠨ ᠨᠢᠭᠡᠨ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1621"/>
        <location filename="../clock.cpp" line="2076"/>
        <location filename="../clock.cpp" line="3125"/>
        <location filename="../clock.cpp" line="3150"/>
        <source>Tue</source>
        <translation>ᠭᠠᠷᠠᠭ ᠤ᠋ᠨ ᠬᠤᠶᠠᠷ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1623"/>
        <location filename="../clock.cpp" line="2078"/>
        <location filename="../clock.cpp" line="3126"/>
        <location filename="../clock.cpp" line="3151"/>
        <source>Wed</source>
        <translation>ᠭᠠᠷᠠᠭ ᠤ᠋ᠨ ᠭᠤᠷᠪᠠᠨ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1625"/>
        <location filename="../clock.cpp" line="2080"/>
        <location filename="../clock.cpp" line="3127"/>
        <location filename="../clock.cpp" line="3152"/>
        <source>Thu</source>
        <translation>ᠭᠠᠷᠠᠭ ᠤ᠋ᠨ ᠳᠦᠷᠪᠡᠨ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1627"/>
        <location filename="../clock.cpp" line="2082"/>
        <location filename="../clock.cpp" line="3128"/>
        <location filename="../clock.cpp" line="3153"/>
        <source>Fri</source>
        <translation>ᠭᠠᠷᠠᠭ ᠤ᠋ᠨ ᠳᠠᠪᠤᠨ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1629"/>
        <location filename="../clock.cpp" line="2084"/>
        <location filename="../clock.cpp" line="3129"/>
        <location filename="../clock.cpp" line="3154"/>
        <source>Sat</source>
        <translation>ᠭᠠᠷᠠᠭ ᠤ᠋ᠨ ᠵᠢᠷᠭᠤᠭᠠᠨ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1631"/>
        <location filename="../clock.cpp" line="2086"/>
        <location filename="../clock.cpp" line="3130"/>
        <location filename="../clock.cpp" line="3155"/>
        <source>Sun</source>
        <translation>ᠭᠠᠷᠠᠭ ᠤ᠋ᠨ ᠡᠳᠦᠷ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2541"/>
        <source>60 Seconds to close</source>
        <translation>60 ᠰᠧᠺᠦ᠋ᠨ᠋ᠳ᠋ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠦᠪᠡᠷ ᠵᠢᠨᠨ ᠬᠠᠭᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3341"/>
        <location filename="../clock.cpp" line="3392"/>
        <source>five mins late</source>
        <translation>5 ᠰᠧᠺᠦ᠋ᠨ᠋ᠳ᠋ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3342"/>
        <location filename="../clock.cpp" line="3395"/>
        <source>ten mins late</source>
        <translation>10 ᠰᠧᠺᠦ᠋ᠨ᠋ᠳ᠋ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3343"/>
        <location filename="../clock.cpp" line="3398"/>
        <source>twenty mins late</source>
        <translation>20 ᠰᠧᠺᠦ᠋ᠨ᠋ᠳ᠋ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3344"/>
        <location filename="../clock.cpp" line="3401"/>
        <source>thirsty mins late</source>
        <translation>30 ᠰᠧᠺᠦ᠋ᠨ᠋ᠳ᠋ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3345"/>
        <location filename="../clock.cpp" line="3404"/>
        <source>one hour late</source>
        <translation>1 ᠴᠠᠭ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3589"/>
        <source>mini window</source>
        <translation>ᠵᠢᠵᠢᠭ ᠴᠤᠨᠭᠬᠤ ᠪᠡᠷ ᠢᠯᠡᠷᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1450"/>
        <source>2min</source>
        <translation>2 ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="14"/>
        <location filename="../clock.ui" line="48"/>
        <location filename="../clock.ui" line="178"/>
        <source>Alarm</source>
        <translation>ᠰᠡᠷᠢᠬᠦᠯᠬᠡᠳᠦ ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>Count</source>
        <translation type="vanished">计次</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="248"/>
        <location filename="../clock.cpp" line="127"/>
        <source>Watch</source>
        <translation>ᠰᠸᠺᠦᠨ᠋ᠲ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="394"/>
        <source>icon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="344"/>
        <location filename="../clock.cpp" line="346"/>
        <location filename="../clock.cpp" line="361"/>
        <source>  bell</source>
        <translation>  ᠬᠤᠨᠭᠬᠤ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1256"/>
        <source>Minimize</source>
        <translatorcomment>最小化</translatorcomment>
        <translation>ᠬᠠᠮᠤᠭ ᠎ᠤᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="240"/>
        <location filename="../clock.cpp" line="1273"/>
        <source>Quit</source>
        <translatorcomment>退出</translatorcomment>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1285"/>
        <source>Menu</source>
        <translatorcomment>菜单</translatorcomment>
        <translation>ᠲᠤᠪᠶᠤᠭ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="493"/>
        <source>Delete</source>
        <translatorcomment>删除</translatorcomment>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="494"/>
        <source>ClearAll</source>
        <translatorcomment>全部清空</translatorcomment>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Set Up</source>
        <translatorcomment>设置</translatorcomment>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>help</source>
        <translatorcomment>帮助</translatorcomment>
        <translation type="vanished">帮助</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1300"/>
        <source>Help</source>
        <translatorcomment>帮助</translatorcomment>
        <translation>ᠬᠠᠪᠰᠤᠷᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1301"/>
        <source>About</source>
        <translatorcomment>关于</translatorcomment>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1302"/>
        <source>Close</source>
        <translatorcomment>退出</translatorcomment>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="918"/>
        <source>up to 100 times</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠤᠯᠠᠨ ᠳ᠋ᠤ᠌ ᠪᠡᠨ 100 ᠤᠳᠠᠭ᠎ᠠ ᠳᠡᠮᠳᠡᠭᠯᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="365"/>
        <source>  remind</source>
        <translation>  ᠪᠠᠢᠰᠬᠢᠭᠠᠳ ᠠᠨᠭᠬᠠᠷᠤᠭᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="352"/>
        <source>  name</source>
        <translation>  ᠰᠡᠷᠢᠬᠦᠯᠬᠡᠳᠦ ᠴᠠᠭ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="495"/>
        <source>edit</source>
        <translation>ᠨᠠᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1227"/>
        <source>mute</source>
        <translation>ᠳᠠᠭᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1231"/>
        <source>All bells are off</source>
        <translation>ᠪᠦᠬᠦ ᠬᠤᠨᠭᠬᠤ ᠵᠢ ᠬᠠᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1452"/>
        <source>3min</source>
        <translation>3 ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1454"/>
        <source>4min</source>
        <translation>4 ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1456"/>
        <source>6min</source>
        <translation>6 ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1463"/>
        <location filename="../clock.cpp" line="1646"/>
        <location filename="../clock.cpp" line="1996"/>
        <location filename="../clock.cpp" line="2100"/>
        <location filename="../clock.cpp" line="2101"/>
        <location filename="../clock.cpp" line="3122"/>
        <location filename="../clock.cpp" line="3160"/>
        <location filename="../clock.cpp" line="3161"/>
        <source>No repetition</source>
        <translation>ᠳᠠᠬᠢᠨ ᠳᠠᠪᠳᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source> Seconds to close</source>
        <translation type="vanished"> 秒后关闭</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1644"/>
        <location filename="../clock.cpp" line="1705"/>
        <location filename="../clock.cpp" line="3123"/>
        <location filename="../clock.cpp" line="3175"/>
        <location filename="../clock.cpp" line="3176"/>
        <source>Workingday</source>
        <translation>ᠠᠵᠢᠯ ᠤ᠋ᠨ ᠡᠳᠦᠷ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1707"/>
        <source>(default)</source>
        <translation>( ᠠᠶᠠᠳᠠᠯ)</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1712"/>
        <location filename="../clock.cpp" line="3340"/>
        <location filename="../clock.cpp" line="3385"/>
        <location filename="../clock.cpp" line="3389"/>
        <location filename="../clock.cpp" line="3407"/>
        <source>none</source>
        <translation>ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1771"/>
        <location filename="../clock.cpp" line="1828"/>
        <source>Please set alarm name!</source>
        <translatorcomment>请设置闹钟名!</translatorcomment>
        <translation>ᠰᠡᠷᠢᠬᠦᠯᠬᠡᠳᠦ ᠴᠠᠭ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <source>hour </source>
        <translation type="vanished">小时 </translation>
    </message>
    <message>
        <source> min bell rings</source>
        <translation type="vanished"> 分钟后铃响</translation>
    </message>
    <message>
        <source>Edit alarm clock</source>
        <translation type="vanished">编辑闹钟</translation>
    </message>
    <message>
        <source>点击闹钟显示剩余时间</source>
        <translatorcomment>Remaining time</translatorcomment>
        <translation type="vanished">Remaining time</translation>
    </message>
    <message>
        <source> days </source>
        <translation type="vanished"> 天 </translation>
    </message>
    <message>
        <source> hour </source>
        <translation type="vanished"> 小时 </translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1993"/>
        <source>glass</source>
        <translation>ᠰᠢᠯ</translation>
    </message>
    <message>
        <source>minimize</source>
        <translatorcomment>最小化</translatorcomment>
        <translation type="vanished">最小化</translation>
    </message>
    <message>
        <source>close</source>
        <translatorcomment>关闭</translatorcomment>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>menu</source>
        <translatorcomment>菜单</translatorcomment>
        <translation type="vanished">菜单</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">犬吠</translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">声呐</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">雨滴</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="780"/>
        <location filename="../clock.cpp" line="2843"/>
        <source>continue</source>
        <translation>ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1841"/>
        <source>warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1841"/>
        <source>the number of alarms reaches limit!！</source>
        <translation>ᠰᠡᠷᠢᠬᠦᠯᠬᠡᠳᠦ ᠴᠠᠭ ᠤ᠋ᠨ ᠲᠤᠭ᠎ᠠ ᠳᠡᠭᠡᠳᠦ ᠬᠢᠵᠠᠭᠠᠷ ᠲᠤ᠌ ᠬᠦᠷᠪᠡ!</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1841"/>
        <source>yes</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2097"/>
        <location filename="../clock.cpp" line="2098"/>
        <source>  work</source>
        <translation>  ᠠᠵᠢᠯ ᠤ᠋ᠨ ᠡᠳᠦᠷ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2097"/>
        <source>  工作日</source>
        <translation>  ᠠᠵᠢᠯ ᠤ᠋ᠨ ᠡᠳᠦᠷ</translation>
    </message>
    <message>
        <source>360 Seconds to close</source>
        <translation type="vanished">360秒后关闭</translation>
    </message>
    <message>
        <source>Time out</source>
        <translation type="vanished">时间到</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2575"/>
        <source>End</source>
        <translation>ᠳᠠᠭᠤᠰᠬᠤ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2705"/>
        <source>after tomorrow</source>
        <translation>ᠨᠦᠬᠦᠬᠡᠳᠦᠷ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2708"/>
        <source>Tomorrow</source>
        <translation>ᠮᠠᠷᠭᠠᠰᠢ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2883"/>
        <location filename="../clock.cpp" line="3092"/>
        <source>hour</source>
        <translation>ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2886"/>
        <location filename="../clock.cpp" line="3095"/>
        <source>min</source>
        <translation>ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2889"/>
        <source>sec</source>
        <translation>ᠰᠸᠺᠦᠨ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1641"/>
        <location filename="../clock.cpp" line="1642"/>
        <location filename="../clock.cpp" line="2095"/>
        <location filename="../clock.cpp" line="3292"/>
        <source>Every day</source>
        <translation>ᠡᠳᠦᠷ ᠪᠦᠷᠢ</translation>
    </message>
    <message>
        <source>glass(default)</source>
        <translation type="vanished">玻璃(默认)</translation>
    </message>
    <message>
        <source>bark(default)</source>
        <translation type="vanished">犬吠(默认)</translation>
    </message>
    <message>
        <source>sonar(default)</source>
        <translation type="vanished">声呐(默认)</translation>
    </message>
    <message>
        <source>drip(default)</source>
        <translation type="vanished">雨滴(默认)</translation>
    </message>
    <message>
        <source>1min</source>
        <translation type="vanished">1分钟</translation>
    </message>
    <message>
        <source>Monday to Friday</source>
        <translation type="vanished">周一周二周三周四周五</translation>
    </message>
    <message>
        <source>24 hour system</source>
        <translation type="vanished">24小时制</translation>
    </message>
    <message>
        <source>Notification</source>
        <translation type="vanished">通知栏弹窗</translation>
    </message>
    <message>
        <source>Alert in 2 minutes</source>
        <translation type="vanished">2分钟后提醒</translation>
    </message>
    <message>
        <source>复位</source>
        <translatorcomment>reset</translatorcomment>
        <translation type="vanished">reset</translation>
    </message>
    <message>
        <source>计次</source>
        <translatorcomment>count</translatorcomment>
        <translation type="vanished">count</translation>
    </message>
    <message>
        <source>删除闹铃</source>
        <translatorcomment>deletealarm</translatorcomment>
        <translation type="vanished">deletealarm</translation>
    </message>
    <message>
        <source>保存</source>
        <translatorcomment>Preservation</translatorcomment>
        <translation type="vanished">Preservation</translation>
    </message>
    <message>
        <source>倒计时</source>
        <translatorcomment>Count down</translatorcomment>
        <translation type="vanished">Count down</translation>
    </message>
    <message>
        <source>闹钟</source>
        <translatorcomment>Alarm</translatorcomment>
        <translation type="obsolete">Alarm</translation>
    </message>
    <message>
        <source>秒表</source>
        <translatorcomment>Stopwatch</translatorcomment>
        <translation type="vanished">Stopwatch</translation>
    </message>
    <message>
        <source>12小时43分后铃声响</source>
        <translatorcomment>12hour43minThe bell rings</translatorcomment>
        <translation type="vanished">12hour43minThe bell rings</translation>
    </message>
    <message>
        <source>取消</source>
        <translatorcomment>cancel</translatorcomment>
        <translation type="vanished">cancel</translation>
    </message>
    <message>
        <source>新建闹钟</source>
        <translatorcomment>New alarm</translatorcomment>
        <translation type="vanished">New alarm</translation>
    </message>
    <message>
        <source>  闹钟名</source>
        <translatorcomment> Name</translatorcomment>
        <translation type="vanished"> Name</translation>
    </message>
    <message>
        <source>  重复</source>
        <translatorcomment> repeat</translatorcomment>
        <translation type="vanished"> repeat</translation>
    </message>
    <message>
        <source>  铃声时长</source>
        <translatorcomment> ring time</translatorcomment>
        <translation type="vanished"> ring time</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="751"/>
        <source>On</source>
        <translation>On</translation>
    </message>
    <message>
        <source>继续</source>
        <translatorcomment>Continue</translatorcomment>
        <translation type="vanished">Continue</translation>
    </message>
    <message>
        <source>间隔 </source>
        <translatorcomment>interval </translatorcomment>
        <translation type="obsolete">interval </translation>
    </message>
    <message>
        <source>下午</source>
        <translatorcomment>PM</translatorcomment>
        <translation type="vanished">PM</translation>
    </message>
    <message>
        <source>上午</source>
        <translatorcomment>AM</translatorcomment>
        <translation type="vanished">AM</translation>
    </message>
    <message>
        <source>周一</source>
        <translatorcomment>Mon</translatorcomment>
        <translation type="vanished">Mon</translation>
    </message>
    <message>
        <source>周二</source>
        <translatorcomment>Tue</translatorcomment>
        <translation type="vanished">Tue</translation>
    </message>
    <message>
        <source>周三</source>
        <translatorcomment>Wed</translatorcomment>
        <translation type="vanished">Wed</translation>
    </message>
    <message>
        <source>周四</source>
        <translatorcomment>Thu</translatorcomment>
        <translation type="vanished">Thu</translation>
    </message>
    <message>
        <source>周五</source>
        <translatorcomment>Fri</translatorcomment>
        <translation type="vanished">Fri</translation>
    </message>
    <message>
        <source>周六</source>
        <translatorcomment>Sat</translatorcomment>
        <translation type="vanished">Sat</translation>
    </message>
    <message>
        <source>周日</source>
        <translatorcomment>Sun</translatorcomment>
        <translation type="vanished">Sun</translation>
    </message>
    <message>
        <source>秒后自动关闭</source>
        <translatorcomment> Seconds to close</translatorcomment>
        <translation type="vanished"> Seconds to close</translation>
    </message>
    <message>
        <source>2分钟后提醒</source>
        <translation type="vanished">Alert in 2 minutes</translation>
    </message>
    <message>
        <source>5分钟后提醒</source>
        <translation type="vanished">Alert in 5 minutes</translation>
    </message>
    <message>
        <source>10分钟后提醒</source>
        <translation type="vanished">Alert in 10 minutes</translation>
    </message>
    <message>
        <source>30分钟后提醒</source>
        <translation type="vanished">Alert in 30 minutes</translation>
    </message>
    <message>
        <source>60分钟后提醒</source>
        <translation type="vanished">Alert in 60 minutes</translation>
    </message>
    <message>
        <source>天</source>
        <translation type="obsolete"> days </translation>
    </message>
    <message>
        <source>玻璃</source>
        <translatorcomment>glass</translatorcomment>
        <translation type="vanished">glass</translation>
    </message>
    <message>
        <source>犬吠</source>
        <translatorcomment>bark</translatorcomment>
        <translation type="vanished">bark</translation>
    </message>
    <message>
        <source>声呐</source>
        <translatorcomment>sonar</translatorcomment>
        <translation type="vanished">sonar</translation>
    </message>
    <message>
        <source>雨滴</source>
        <translatorcomment>drip</translatorcomment>
        <translation type="vanished">drip</translation>
    </message>
    <message>
        <source>2分钟</source>
        <translatorcomment>2min</translatorcomment>
        <translation type="vanished">2min</translation>
    </message>
    <message>
        <source>3分钟</source>
        <translatorcomment>3min</translatorcomment>
        <translation type="vanished">3min</translation>
    </message>
    <message>
        <source>4分钟</source>
        <translatorcomment>4min</translatorcomment>
        <translation type="vanished">4min</translation>
    </message>
    <message>
        <source>6分钟</source>
        <translatorcomment>6min</translatorcomment>
        <translation type="vanished">6min</translation>
    </message>
    <message>
        <source>工作日</source>
        <translatorcomment>Workingday</translatorcomment>
        <translation type="vanished">Workingday</translation>
    </message>
    <message>
        <source>(默认)</source>
        <translatorcomment>(default)</translatorcomment>
        <translation type="vanished">(default)</translation>
    </message>
    <message>
        <source>每天</source>
        <translatorcomment>Every day</translatorcomment>
        <translation type="vanished">Every day</translation>
    </message>
    <message>
        <source>1分钟</source>
        <translatorcomment>1min</translatorcomment>
        <translation type="vanished">1min</translation>
    </message>
    <message>
        <source>小时</source>
        <translatorcomment> hour </translatorcomment>
        <translation type="obsolete"> hour </translation>
    </message>
    <message>
        <source>分钟后铃响</source>
        <translatorcomment> min bell rings</translatorcomment>
        <translation type="obsolete"> min bell rings</translation>
    </message>
    <message>
        <source>编辑闹钟</source>
        <translatorcomment>Edit alarm clock</translatorcomment>
        <translation type="vanished">Edit alarm clock</translation>
    </message>
    <message>
        <source>删除当前闹钟！</source>
        <translatorcomment>delete alame clock !</translatorcomment>
        <translation type="vanished">delete alame clock !</translation>
    </message>
    <message>
        <source>您确定删除当前闹钟吗？</source>
        <translatorcomment>are you sure ?</translatorcomment>
        <translation type="vanished">are you sure ?</translation>
    </message>
    <message>
        <source>倒计时时间结束</source>
        <translatorcomment>End of countdown time</translatorcomment>
        <translation type="vanished">End of countdown time</translation>
    </message>
    <message>
        <source>结束</source>
        <translatorcomment>End</translatorcomment>
        <translation type="vanished">End</translation>
    </message>
    <message>
        <source>明日</source>
        <translatorcomment>Tom</translatorcomment>
        <translation type="vanished">Tom</translation>
    </message>
    <message>
        <source>360秒后自动关闭</source>
        <translatorcomment>360 Seconds to close</translatorcomment>
        <translation type="obsolete">360 Seconds to close</translation>
    </message>
    <message>
        <source>时间到</source>
        <translation type="vanished">Time out</translation>
    </message>
    <message>
        <source>后天</source>
        <translatorcomment>after tomorrow</translatorcomment>
        <translation type="vanished">after tomorrow</translation>
    </message>
    <message>
        <source>明天</source>
        <translatorcomment>Tomorrow</translatorcomment>
        <translation type="vanished">Tomorrow</translation>
    </message>
    <message>
        <source>时</source>
        <translatorcomment>hour</translatorcomment>
        <translation type="vanished">hour</translation>
    </message>
    <message>
        <source>分</source>
        <translatorcomment>min</translatorcomment>
        <translation type="vanished">min</translation>
    </message>
    <message>
        <source>秒</source>
        <translatorcomment>sec</translatorcomment>
        <translation type="vanished">sec</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2100"/>
        <source>不重复</source>
        <translatorcomment>No repetition </translatorcomment>
        <translation>ᠳᠠᠬᠢᠨ ᠳᠠᠪᠳᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>玻璃(默认)</source>
        <translatorcomment>glass(default)</translatorcomment>
        <translation type="vanished">glass(default)</translation>
    </message>
    <message>
        <source>犬吠(默认)</source>
        <translatorcomment>bark(default)</translatorcomment>
        <translation type="obsolete">bark(default)</translation>
    </message>
    <message>
        <source>声呐(默认)</source>
        <translatorcomment>sonar(default)</translatorcomment>
        <translation type="obsolete">sonar(default)</translation>
    </message>
    <message>
        <source>雨滴(默认)</source>
        <translatorcomment>drip(default)</translatorcomment>
        <translation type="obsolete">drip(default)</translation>
    </message>
    <message>
        <source>1分钟(默认)</source>
        <translatorcomment>1min(default)</translatorcomment>
        <translation type="obsolete">1min(default)</translation>
    </message>
    <message>
        <source>2分钟(默认)</source>
        <translatorcomment>2min(default)</translatorcomment>
        <translation type="obsolete">2min(default)</translation>
    </message>
    <message>
        <source>3分钟(默认)</source>
        <translatorcomment>3min(default)</translatorcomment>
        <translation type="obsolete">3min(default)</translation>
    </message>
    <message>
        <source>4分钟(默认)</source>
        <translatorcomment>4min(default)</translatorcomment>
        <translation type="obsolete">4min(default)</translation>
    </message>
    <message>
        <source>6分钟(默认)</source>
        <translatorcomment>6min(default)</translatorcomment>
        <translation type="obsolete">6min(default)</translation>
    </message>
    <message>
        <source>周一周二周三周四周五</source>
        <translation type="obsolete">Monday to Friday</translation>
    </message>
    <message>
        <source>24小时制(23:59:59)</source>
        <translatorcomment>24 hour system </translatorcomment>
        <translation type="obsolete">24 hour system</translation>
    </message>
    <message>
        <source>通知栏弹窗</source>
        <translatorcomment>Notification</translatorcomment>
        <translation type="obsolete">Notification</translation>
    </message>
    <message>
        <source>一分钟后自动关闭</source>
        <translatorcomment>Turn off after 1 min</translatorcomment>
        <translation type="obsolete">Turn off after 1 min</translation>
    </message>
</context>
<context>
    <name>Natice_alarm</name>
    <message>
        <location filename="../noticeAlarm.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../noticeAlarm.ui" line="120"/>
        <source>Alarm clock</source>
        <translation>ᠰᠡᠷᠢᠬᠦᠯᠬᠡᠳᠦ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.ui" line="227"/>
        <source>11:20 设计例会...</source>
        <translation>11:20 ᠵᠢᠷᠤᠭ ᠳᠦᠰᠦᠪ ᠤ᠋ᠨ ᠡᠬᠡᠯᠵᠢᠳᠦ ᠬᠤᠷᠠᠯ...</translation>
    </message>
    <message>
        <source>60秒后自动关闭</source>
        <translation type="vanished">360 Seconds to close {60秒?}</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.ui" line="334"/>
        <source>Remind later</source>
        <translation>ᠪᠠᠢᠰᠬᠢᠭᠠᠳ ᠠᠨᠭᠬᠠᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="60"/>
        <source>Ring prompt</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ᠋ ᠰᠠᠨᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="182"/>
        <source>none</source>
        <translation>ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="189"/>
        <source>Time out</source>
        <translation>ᠴᠠᠭ ᠳᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <source>glass</source>
        <translation type="vanished">玻璃</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">犬吠</translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">声呐</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">雨滴</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="283"/>
        <source> Seconds to close</source>
        <translation> ᠰᠸᠺᠦᠨ᠋ᠲ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠦᠪᠡᠷ ᠵᠢᠨᠨ ᠬᠠᠭᠠᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>Notice_Dialog</name>
    <message>
        <source>Ring prompt</source>
        <translation type="vanished">响铃提示</translation>
    </message>
    <message>
        <source>glass</source>
        <translation type="vanished">玻璃</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">犬吠</translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">声呐</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">雨滴</translation>
    </message>
    <message>
        <source>End of countdown time</source>
        <translation type="vanished">End of countdown time</translation>
    </message>
    <message>
        <source>秒后关闭铃声</source>
        <translation type="vanished">秒后关闭铃声</translation>
    </message>
    <message>
        <source>闹钟:</source>
        <translation type="vanished">闹钟:</translation>
    </message>
    <message>
        <source>起床铃</source>
        <translation type="vanished">起床铃</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../clock.cpp" line="2175"/>
        <source>Hint</source>
        <translation>ᠠᠨᠭᠬᠠᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2176"/>
        <source>Are you sure to delete？</source>
        <translation>ᠤᠳᠤ ᠵᠢᠨ ᠰᠡᠷᠢᠬᠦᠯᠬᠡᠳᠦ ᠴᠠᠭ ᠢ᠋ ᠲᠠ ᠪᠠᠳᠤᠳᠠᠢ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2177"/>
        <source>sure</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2178"/>
        <source>cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
</context>
<context>
    <name>SelectBtnUtil</name>
    <message>
        <source>relax</source>
        <translation type="vanished">放松</translation>
    </message>
    <message>
        <source>emotion</source>
        <translation type="vanished">情感</translation>
    </message>
    <message>
        <source>silence</source>
        <translation type="vanished">静谧</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="180"/>
        <source>glass</source>
        <translation>ᠰᠢᠯ</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="181"/>
        <source>bark</source>
        <translation>ᠨᠤᠬᠠᠢ ᠬᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="182"/>
        <source>sonar</source>
        <translation>ᠰᠤᠨᠤᠷ</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="183"/>
        <source>drip</source>
        <translation>ᠪᠤᠷᠤᠭᠠᠨ ᠤ᠋ ᠳᠤᠰᠤᠯ</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="151"/>
        <source>diy bell</source>
        <translation>ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠬᠤᠨᠭᠬᠤᠨ ᠳᠠᠭᠤ</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="179"/>
        <source>none</source>
        <translation>ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="269"/>
        <source>select bell</source>
        <translation>ᠬᠤᠨᠭᠬᠤᠨ ᠳᠠᠭᠤ ᠵᠢ ᠰᠤᠨᠭᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="267"/>
        <source>audio files(*mp3 *wav *ogg)</source>
        <translation>ᠳᠦᠷᠦᠯ (*mp3 *wav *ogg)</translation>
    </message>
</context>
<context>
    <name>TestWidget</name>
    <message>
        <location filename="../countdownAnimation.cpp" line="110"/>
        <source>TestWidget</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>VerticalScroll_24</name>
    <message>
        <location filename="../verticalScroll24.cpp" line="190"/>
        <source>PM</source>
        <translation>PM</translation>
    </message>
    <message>
        <location filename="../verticalScroll24.cpp" line="192"/>
        <source>AM</source>
        <translation>AM</translation>
    </message>
    <message>
        <location filename="../verticalScroll24.cpp" line="227"/>
        <source>VerticalScroll_24</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>VerticalScroll_60</name>
    <message>
        <location filename="../verticalScroll60.cpp" line="161"/>
        <source>VerticalScroll_60</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>VerticalScroll_99</name>
    <message>
        <location filename="../verticalScroll99.cpp" line="172"/>
        <source>VerticalScroll_99</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>close_or_hide</name>
    <message>
        <location filename="../closeOrHide.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="310"/>
        <source>sure</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="406"/>
        <source>请选择关闭后的状态</source>
        <translation>ᠬᠠᠭᠠᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠳᠦᠯᠦᠪ ᠢ᠋ ᠰᠤᠨᠭᠭᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="270"/>
        <source>cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="126"/>
        <source> backstage</source>
        <translation> ᠠᠷᠤ ᠳᠠᠪᠴᠠᠨᠭ ᠳ᠋ᠤ᠌ ᠠᠵᠢᠯᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>backstage</source>
        <translation type="vanished">后台运行</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="178"/>
        <source> Exit program </source>
        <translation> ᠰᠢᠭ᠋ᠤᠳ ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ </translation>
    </message>
    <message>
        <location filename="../closeOrHide.cpp" line="41"/>
        <source>Please select the state after closing:</source>
        <translation>ᠬᠠᠭᠠᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠳᠦᠯᠦᠪ ᠢ᠋ ᠰᠤᠨᠭᠭᠤᠭᠠᠷᠠᠢ:</translation>
    </message>
</context>
<context>
    <name>delete_msg</name>
    <message>
        <location filename="../deleteMsg.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../deleteMsg.ui" line="241"/>
        <source>sure</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../deleteMsg.ui" line="206"/>
        <source>cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../deleteMsg.ui" line="85"/>
        <source>are you sure ?</source>
        <translation>ᠤᠳᠤ ᠵᠢᠨ ᠰᠡᠷᠢᠬᠦᠯᠬᠡᠳᠦ ᠴᠠᠭ ᠢ᠋ ᠲᠠ ᠪᠠᠳᠤᠳᠠᠢ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
</context>
<context>
    <name>item_new</name>
    <message>
        <location filename="../itemNew.cpp" line="81"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>set_alarm_repeat_Dialog</name>
    <message>
        <location filename="../setAlarmRepeatDialog.cpp" line="40"/>
        <source>Alarm</source>
        <translation>ᠰᠡᠷᠢᠬᠦᠯᠬᠡᠳᠦ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../setAlarmRepeatDialog.cpp" line="148"/>
        <source>Dialog</source>
        <translation>ᠬᠠᠷᠢᠯᠴᠠᠬᠤ ᠴᠤᠩᠬᠤ</translation>
    </message>
</context>
<context>
    <name>setuppage</name>
    <message>
        <source>开机启动</source>
        <translatorcomment> Boot up</translatorcomment>
        <translation type="vanished"> Boot up</translation>
    </message>
    <message>
        <source>Boot up</source>
        <translation type="vanished">开机启动</translation>
    </message>
    <message>
        <source>  work</source>
        <translation type="vanished">  工作日</translation>
    </message>
    <message>
        <source>  Time</source>
        <translation type="vanished">  时间格式</translation>
    </message>
    <message>
        <source>  Pop-up</source>
        <translation type="vanished">  弹窗方式</translation>
    </message>
    <message>
        <source>  duration</source>
        <translation type="vanished">  稍后提醒</translation>
    </message>
    <message>
        <source>  ringtone</source>
        <translation type="vanished">  默认铃声</translation>
    </message>
    <message>
        <source>  Mute</source>
        <translation type="vanished">  静音</translation>
    </message>
    <message>
        <source>work</source>
        <translation type="vanished">工作日</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">时间格式</translation>
    </message>
    <message>
        <source>Pop-up</source>
        <translation type="vanished">弹窗方式</translation>
    </message>
    <message>
        <source>duration</source>
        <translation type="vanished">稍后提醒</translation>
    </message>
    <message>
        <source>ringtone</source>
        <translation type="vanished">默认铃声</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation type="vanished">静音</translation>
    </message>
    <message>
        <source>volume</source>
        <translation type="vanished">铃声音量</translation>
    </message>
    <message>
        <source>setting</source>
        <translatorcomment>设置</translatorcomment>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Mon</source>
        <translation type="vanished">周一</translation>
    </message>
    <message>
        <source>Tue</source>
        <translation type="vanished">周二</translation>
    </message>
    <message>
        <source>Wed</source>
        <translation type="vanished">周三</translation>
    </message>
    <message>
        <source>Thu</source>
        <translation type="vanished">周四</translation>
    </message>
    <message>
        <source>Fri</source>
        <translation type="vanished">周五</translation>
    </message>
    <message>
        <source>Sat</source>
        <translation type="vanished">周六</translation>
    </message>
    <message>
        <source>Sun</source>
        <translation type="vanished">周日</translation>
    </message>
    <message>
        <source>Every day</source>
        <translation type="vanished">每天</translation>
    </message>
    <message>
        <source>Following system</source>
        <translation type="vanished">跟随系统</translation>
    </message>
    <message>
        <source>  time</source>
        <translation type="vanished">  时间格式</translation>
    </message>
    <message>
        <source>24 hour system</source>
        <translation type="vanished">24小时制</translation>
    </message>
    <message>
        <source>12 hour system</source>
        <translation type="vanished">12小时制</translation>
    </message>
    <message>
        <source>Notification</source>
        <translation type="vanished">通知栏弹窗</translation>
    </message>
    <message>
        <source>Full screen</source>
        <translation type="vanished">全屏弹窗</translation>
    </message>
    <message>
        <source>Alert in 2 minutes</source>
        <translation type="vanished">2分钟后提醒</translation>
    </message>
    <message>
        <source>Alert in 5 minutes</source>
        <translation type="vanished">5分钟后提醒</translation>
    </message>
    <message>
        <source>Alert in 10 minutes</source>
        <translation type="vanished">10分钟后提醒</translation>
    </message>
    <message>
        <source>Alert in 30 minutes</source>
        <translation type="vanished">30分钟后提醒</translation>
    </message>
    <message>
        <source>Alert in 60 minutes</source>
        <translation type="vanished">60分钟后提醒</translation>
    </message>
    <message>
        <source>glass</source>
        <translation type="vanished">玻璃</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">犬吠</translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">声呐</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">雨滴</translation>
    </message>
</context>
<context>
    <name>stopwatch_item</name>
    <message>
        <source>longest</source>
        <translation type="vanished">最长</translation>
    </message>
    <message>
        <location filename="../stopwatchItem.cpp" line="74"/>
        <source>Form</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ ᠮᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../stopwatchItem.cpp" line="51"/>
        <location filename="../stopwatchItem.cpp" line="114"/>
        <source>max</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠤᠷᠳᠤ</translation>
    </message>
    <message>
        <location filename="../stopwatchItem.cpp" line="122"/>
        <source>min</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠤᠬᠤᠷ</translation>
    </message>
    <message>
        <source>shortest</source>
        <translation type="vanished">最短</translation>
    </message>
</context>
<context>
    <name>tinyCountdown</name>
    <message>
        <location filename="../tinycountdown.ui" line="26"/>
        <source>Form</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ ᠮᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <source>Countdown</source>
        <translation type="vanished">倒计时</translation>
    </message>
    <message>
        <location filename="../tinycountdown.ui" line="72"/>
        <source>01:29:58</source>
        <translation></translation>
    </message>
    <message>
        <source>switch</source>
        <translation type="vanished">切换</translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="259"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="267"/>
        <source>main window</source>
        <translation>ᠭᠤᠤᠯ ᠴᠤᠨᠭᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="279"/>
        <source>suspend</source>
        <translation>ᠵᠤᠭ᠍ᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="298"/>
        <source>finish</source>
        <translation>ᠳᠠᠭᠤᠰᠬᠤ</translation>
    </message>
</context>
</TS>
