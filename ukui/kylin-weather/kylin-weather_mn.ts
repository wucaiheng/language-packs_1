<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AddCityAction</name>
    <message>
        <source>kylin-weather</source>
        <translation type="obsolete">天气</translation>
    </message>
</context>
<context>
    <name>Air</name>
    <message>
        <source>Air</source>
        <translation type="vanished">空气指数</translation>
    </message>
</context>
<context>
    <name>AllowLocation</name>
    <message>
        <location filename="../view/horscreen/allowlocation.cpp" line="28"/>
        <source>Whether to enable application locating?</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠦ ᠤᠷᠤᠨ ᠪᠠᠢᠷᠢ ᠶᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../view/horscreen/allowlocation.cpp" line="52"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../view/horscreen/allowlocation.cpp" line="56"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>CityCollectionWidget</name>
    <message>
        <source>Kylin Weather</source>
        <translation type="vanished">麒麟天气</translation>
    </message>
    <message>
        <source>Weather</source>
        <translation type="vanished">天气</translation>
    </message>
    <message>
        <source>Collections</source>
        <translation type="vanished">收藏城市</translation>
    </message>
    <message>
        <source>Local</source>
        <translation type="vanished">当前城市</translation>
    </message>
    <message>
        <source>Current Network Exception, Please Check Network Settings</source>
        <translation type="vanished">当前网络异常，请检查网络设置</translation>
    </message>
</context>
<context>
    <name>Clothe</name>
    <message>
        <source>Clothe</source>
        <translation type="vanished">穿衣指数</translation>
    </message>
</context>
<context>
    <name>CollectCity</name>
    <message>
        <location filename="../view/search/collectcity.cpp" line="10"/>
        <source>Collect City</source>
        <translation>ᠬᠤᠳᠠ ᠶᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../view/search/collectcity.cpp" line="15"/>
        <source>None</source>
        <translation>ᠳᠦᠷ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <location filename="../controller/core/core.cpp" line="220"/>
        <location filename="../controller/core/core.cpp" line="225"/>
        <source>Seek failed!</source>
        <translation>ᠯᠠᠪᠯᠠᠭ᠎ᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ!</translation>
    </message>
</context>
<context>
    <name>DataParser</name>
    <message>
        <location filename="../model/dataparser.cpp" line="28"/>
        <location filename="../model/dataparser.cpp" line="47"/>
        <location filename="../model/dataparser.cpp" line="57"/>
        <location filename="../model/dataparser.cpp" line="125"/>
        <location filename="../model/dataparser.cpp" line="173"/>
        <location filename="../model/dataparser.cpp" line="206"/>
        <location filename="../model/dataparser.cpp" line="261"/>
        <location filename="../model/dataparser.cpp" line="301"/>
        <source>Data is empty</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
</context>
<context>
    <name>ErrorReact</name>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="19"/>
        <source>Network not connected!</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠦ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="22"/>
        <source>Network error!</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠪᠤᠷᠤᠭᠤ!</translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="27"/>
        <source>Abnormal access address!</source>
        <translation>ᠠᠢᠯᠴᠢᠯᠠᠭᠰᠠᠨ ᠬᠠᠶ᠋ᠢᠭ ᠬᠡᠪ ᠦᠨ ᠪᠤᠰᠤ!</translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="33"/>
        <source>Data parsing error!</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠶᠢᠨ ᠵᠠᠳᠠᠯᠤᠯᠳᠠ ᠪᠤᠷᠤᠭᠤ!</translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="51"/>
        <source>Network Timeout!</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠴᠠᠭ ᠠᠴᠠ ᠬᠡᠳᠦᠡᠷᠪᠡ!</translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="57"/>
        <source>Seek failed!</source>
        <translation>ᠯᠠᠪᠯᠠᠭ᠎ᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ!</translation>
    </message>
    <message>
        <source>seek failed!</source>
        <translation type="vanished">定位失败！</translation>
    </message>
</context>
<context>
    <name>Flu</name>
    <message>
        <source>Flu</source>
        <translation type="vanished">感冒指数</translation>
    </message>
</context>
<context>
    <name>HotCity</name>
    <message>
        <location filename="../view/search/hotcity.cpp" line="9"/>
        <source>Hot City</source>
        <translation>ᠬᠠᠯᠠᠮᠰᠢᠯ ᠬᠤᠳᠠ</translation>
    </message>
</context>
<context>
    <name>Information</name>
    <message>
        <source>Today</source>
        <translation type="vanished">今天</translation>
    </message>
</context>
<context>
    <name>LeftUpSearchBox</name>
    <message>
        <source>search</source>
        <translation type="vanished">搜索</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Weather</source>
        <translation type="vanished">天气</translation>
    </message>
    <message>
        <source>Open Kylin Weather</source>
        <translation type="vanished">打开麒麟天气</translation>
    </message>
    <message>
        <source>Add City</source>
        <translation type="vanished">增加城市</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Open Weather</source>
        <translation type="vanished">打开天气</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <source>Network not connected</source>
        <translation type="vanished">网络未连接</translation>
    </message>
    <message>
        <source>minimize</source>
        <translation type="vanished">最小化</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>Incorrect access address</source>
        <translation type="vanished">访问地址异常</translation>
    </message>
    <message>
        <source>Network error code:%1</source>
        <translation type="vanished">网络错误代码：%1</translation>
    </message>
    <message>
        <source>Kylin Weather</source>
        <translation type="obsolete">麒麟天气</translation>
    </message>
</context>
<context>
    <name>PromptWidget</name>
    <message>
        <location filename="../view/horscreen/promptwidget.cpp" line="21"/>
        <source>Retry</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠬᠤ</translation>
    </message>
    <message>
        <source>retry</source>
        <translation type="vanished">重试</translation>
    </message>
</context>
<context>
    <name>SearchBox</name>
    <message>
        <location filename="../view/search/searchbox.cpp" line="13"/>
        <source>Search</source>
        <translation>ᠬᠠᠢᠯᠲᠠ</translation>
    </message>
</context>
<context>
    <name>SearchCityLIst</name>
    <message>
        <location filename="../view/search/searchcitylist.cpp" line="44"/>
        <location filename="../view/search/searchcitylist.cpp" line="83"/>
        <source>there is no search city!</source>
        <translation>ᠬᠠᠢᠭᠠᠳ ᠬᠤᠳᠠ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
</context>
<context>
    <name>Sport</name>
    <message>
        <source>Sport</source>
        <translation type="vanished">运动指数</translation>
    </message>
</context>
<context>
    <name>ViewVar</name>
    <message>
        <location filename="../global/viewvar.cpp" line="121"/>
        <source>Beijing</source>
        <translation>ᠪᠡᠬᠡᠵᠢᠩ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="122"/>
        <source>Shanghai</source>
        <translation>ᠱᠠᠩᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="123"/>
        <source>Guangzhou</source>
        <translation>ᠭᠤᠸᠠᠩᠵᠸᠤ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="124"/>
        <source>Shenzhen</source>
        <translation>ᠱᠸᠨᠵᠸᠨ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="125"/>
        <source>Chengdu</source>
        <translation>ᠴᠸᠨᠭᠳᠦ᠋</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="126"/>
        <source>Wuhan</source>
        <translation>ᠦᠬᠠᠨ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="127"/>
        <source>Chongqing</source>
        <translation>ᠴᠦᠩᠴᠢᠩ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="128"/>
        <source>Hangzhou</source>
        <translation>ᠬᠠᠩᠵᠸᠤ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="129"/>
        <source>Nanjing</source>
        <translation>ᠨᠠᠨᠵᠢᠩ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="130"/>
        <source>Taibei</source>
        <translation>ᠲᠠᠢᠪᠸᠢ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="131"/>
        <source>Suzhou</source>
        <translation>ᠰᠦᠵᠸᠤ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="132"/>
        <source>Tianjin</source>
        <translation>ᠲᠢᠶᠠᠨᠵᠢᠨ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="133"/>
        <source>Qingdao</source>
        <translation>ᠴᠢᠩᠳᠤᠤ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="134"/>
        <source>Changsha</source>
        <translation>ᠴᠠᠩᠱᠠ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="135"/>
        <source>Zhengzhou</source>
        <translation>ᠵᠸᠩᠵᠸᠤ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="136"/>
        <source>Xian</source>
        <translation>ᠰᠢ ᠠᠨ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="137"/>
        <source>Wuxi</source>
        <translation>ᠦᠰᠢ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="138"/>
        <source>Jinan</source>
        <translation>ᠵᠢᠨᠠᠨ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="139"/>
        <source>Hongkong</source>
        <translation>ᠰᠢᠶᠠᠩᠭᠠᠩ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="140"/>
        <source>Macao</source>
        <translation>ᠤᠤᠮᠸᠨ</translation>
    </message>
    <message>
        <source>Aomen</source>
        <translation type="vanished">澳门</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="141"/>
        <source>Lasa</source>
        <translation>ᠯᠠᠾᠰᠠ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="142"/>
        <source>Lijiang</source>
        <translation>ᠯᠢᠵᠢᠶᠠᠩ</translation>
    </message>
</context>
<context>
    <name>WashCar</name>
    <message>
        <source>Wash Car</source>
        <translation type="vanished">洗车指数</translation>
    </message>
</context>
<context>
    <name>air</name>
    <message>
        <location filename="../view/horscreen/air.cpp" line="17"/>
        <source>Air</source>
        <translation>ᠠᠭᠠᠷ ᠤᠨ ᠵᠡᠷᠭᠡ ᠢᠯᠡᠳᠬᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../view/horscreen/air.cpp" line="19"/>
        <source>N/A</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>city</name>
    <message>
        <source>Switch</source>
        <translation type="vanished">切换</translation>
    </message>
    <message>
        <source>【City】</source>
        <translation type="vanished">【城市】</translation>
    </message>
    <message>
        <location filename="../view/horscreen/city.cpp" line="71"/>
        <source>【Change】</source>
        <translation>【ᠰᠤᠯᠢᠬᠤ】</translation>
    </message>
</context>
<context>
    <name>clothe</name>
    <message>
        <location filename="../view/horscreen/clothe.cpp" line="17"/>
        <source>Clothe</source>
        <translation>ᠬᠤᠪᠴᠠᠰᠤ ᠡᠮᠦᠰᠬᠦ ᠵᠡᠷᠭᠡ ᠢᠯᠡᠳᠬᠡᠭᠴᠢ</translation>
    </message>
</context>
<context>
    <name>flu</name>
    <message>
        <location filename="../view/horscreen/flu.cpp" line="17"/>
        <source>Flu</source>
        <translation>ᠳᠤᠮᠤᠭᠤ ᠬᠦᠷᠬᠦ ᠵᠡᠷᠭᠡ ᠢᠯᠡᠳᠬᠡᠭᠴᠢ</translation>
    </message>
</context>
<context>
    <name>horscreen</name>
    <message>
        <source>Retract</source>
        <translation type="vanished">还原</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="172"/>
        <source>【Retract】</source>
        <translation>【ᠪᠤᠴᠠᠭᠠᠬᠤ】</translation>
    </message>
    <message>
        <source>【City】</source>
        <translation type="vanished">【城市】</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="175"/>
        <location filename="../view/horscreen/horscreen.cpp" line="542"/>
        <source>【Change】</source>
        <translation>【ᠰᠤᠯᠢᠬᠤ】</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="252"/>
        <source>full srceen</source>
        <translation>ᠪᠦᠳᠦᠨ ᠳᠡᠯᠭᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="257"/>
        <source>recovery</source>
        <translation>ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="32"/>
        <location filename="../view/horscreen/horscreen.cpp" line="159"/>
        <location filename="../view/horscreen/horscreen.cpp" line="359"/>
        <source>Weather</source>
        <translation>ᠴᠠᠭ ᠠᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="160"/>
        <source>Version:</source>
        <translation>ᠬᠡᠪᠯᠡᠯ:</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="162"/>
        <source>Weather is a weather information tool, it can browse the weather conditions and living index.</source>
        <translation>ᠴᠠᠭ ᠠᠭᠤᠷ ᠪᠤᠯ ᠬᠤᠳᠠ ᠶᠢᠨ ᠴᠠᠭ ᠠᠭᠤᠷ ᠤᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ ᠶᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ ᠪᠠᠭᠠᠵᠢ ᠂ ᠢᠷᠡᠭᠡᠳᠦᠢ ᠶᠢᠨ ᠴᠠᠭ ᠠᠭᠤᠷ ᠤᠨ ᠪᠠᠢᠳᠠᠯ ᠵᠢᠴᠢ ᠠᠮᠢᠳᠤᠷᠠᠯ ᠤᠨ ᠵᠡᠷᠭᠡ ᠢᠯᠡᠳᠬᠡᠭᠴᠢ ᠶᠢ ᠬᠠᠢᠨ ᠦᠵᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ ᠂ ᠳᠦᠬᠦᠮ ᠳᠤᠳᠤᠷᠬᠠᠢ ᠪᠠᠢᠳᠠᠭ.</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="295"/>
        <location filename="../view/horscreen/horscreen.cpp" line="297"/>
        <source>kylin-weather</source>
        <translation>ᠴᠢ ᠯᠢᠨ ᠴᠠᠭ ᠠᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="365"/>
        <source>Open Weather</source>
        <translation>ᠴᠠᠭ ᠠᠭᠤᠷ ᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="366"/>
        <source>Exit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <source>Switch</source>
        <translation type="vanished">切换</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>KylinWeather</source>
        <translation type="vanished">Weather</translation>
    </message>
    <message>
        <source>show kylin-weather test</source>
        <translation type="vanished">show kylin-weather test</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">服务与支持团队: </translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="26"/>
        <location filename="../view/horscreen/menumodule.cpp" line="142"/>
        <location filename="../view/horscreen/menumodule.cpp" line="158"/>
        <source>Service &amp; Support Team: </source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠤᠯᠤᠨ ᠳᠡᠮᠵᠢᠬᠦ ᠪᠦᠯᠬᠦᠮ: </translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="34"/>
        <source>menu</source>
        <translation>ᠲᠤᠪᠶᠤᠭ</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="49"/>
        <location filename="../view/horscreen/menumodule.cpp" line="99"/>
        <source>Help</source>
        <translation>ᠬᠠᠪᠰᠤᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="54"/>
        <source>Location</source>
        <translation>ᠪᠠᠢᠷᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="56"/>
        <location filename="../view/horscreen/menumodule.cpp" line="101"/>
        <source>yes</source>
        <translation>ᠮᠦᠨ</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="59"/>
        <location filename="../view/horscreen/menumodule.cpp" line="106"/>
        <source>no</source>
        <translation>ᠪᠢᠰᠢ</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="74"/>
        <location filename="../view/horscreen/menumodule.cpp" line="96"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="78"/>
        <location filename="../view/horscreen/menumodule.cpp" line="94"/>
        <source>Quit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="154"/>
        <location filename="../view/horscreen/menumodule.cpp" line="169"/>
        <source>Weather is a weather information tool, it can browse the weather conditions and living index.</source>
        <translation>ᠴᠠᠭ ᠠᠭᠤᠷ ᠪᠤᠯ ᠬᠤᠳᠠ ᠶᠢᠨ ᠴᠠᠭ ᠠᠭᠤᠷ ᠤᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ ᠶᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ ᠪᠠᠭᠠᠵᠢ ᠂ ᠢᠷᠡᠭᠡᠳᠦᠢ ᠶᠢᠨ ᠴᠠᠭ ᠠᠭᠤᠷ ᠤᠨ ᠪᠠᠢᠳᠠᠯ ᠵᠢᠴᠢ ᠠᠮᠢᠳᠤᠷᠠᠯ ᠤᠨ ᠵᠡᠷᠭᠡ ᠢᠯᠡᠳᠬᠡᠭᠴᠢ ᠶᠢ ᠬᠠᠢᠨ ᠦᠵᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ ᠂ ᠳᠦᠬᠦᠮ ᠳᠤᠳᠤᠷᠬᠠᠢ ᠪᠠᠢᠳᠠᠭ.</translation>
    </message>
    <message>
        <source>Weather is a city weather information viewing tool, you can browse the future weather conditions and living index, simple and convenient operation, accurately understand the weather changes.</source>
        <translation type="vanished">天气是一款城市气象信息查看工具，可以快速浏览城市未来的天气情况及生活指数，操作简单便捷，准确了解天气变化。</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="246"/>
        <location filename="../view/horscreen/menumodule.cpp" line="264"/>
        <source>Weather</source>
        <translation>ᠴᠠᠭ ᠠᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="253"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="30"/>
        <location filename="../view/horscreen/menumodule.cpp" line="292"/>
        <source>Version: </source>
        <translation>ᠬᠡᠪᠯᠡᠯ᠄ </translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="vanished">主题</translation>
    </message>
    <message>
        <source>weather</source>
        <translation type="vanished">天气</translation>
    </message>
    <message>
        <source>indicator china weather</source>
        <translation type="vanished">麒麟天气</translation>
    </message>
    <message>
        <source>Indicator China Weather</source>
        <translation type="vanished">麒麟天气</translation>
    </message>
    <message>
        <source>Support: support@kylinos.cn</source>
        <translation type="vanished">支持：support@kylinos.cn</translation>
    </message>
</context>
<context>
    <name>sport</name>
    <message>
        <location filename="../view/horscreen/sport.cpp" line="17"/>
        <source>Sport</source>
        <translation>ᠲᠠᠮᠢᠷ ᠤᠨ ᠬᠦᠳᠡᠯᠬᠡᠬᠡᠨ ᠦ ᠵᠡᠷᠭᠡ ᠢᠯᠡᠳᠬᠡᠭᠴᠢ</translation>
    </message>
</context>
<context>
    <name>title</name>
    <message>
        <source>kylin-weather</source>
        <translation type="vanished">麒麟天气</translation>
    </message>
    <message>
        <location filename="../view/horscreen/title.cpp" line="16"/>
        <source>Weather</source>
        <translation>ᠴᠠᠭ ᠠᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../view/horscreen/title.cpp" line="30"/>
        <source>minimize</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <source>full screen</source>
        <translation type="vanished">全屏</translation>
    </message>
    <message>
        <location filename="../view/horscreen/title.cpp" line="50"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>ultravioletrays</name>
    <message>
        <location filename="../view/horscreen/ultravioletrays.cpp" line="17"/>
        <source>UV</source>
        <translation>ᠬᠦᠷᠡᠩ ᠦᠨ ᠭᠠᠳᠠᠨᠠᠬᠢ ᠲᠤᠶᠠᠭᠠᠨ ᠤ ᠵᠡᠷᠭᠡ ᠢᠯᠡᠳᠬᠡᠭᠴᠢ</translation>
    </message>
</context>
<context>
    <name>washcar</name>
    <message>
        <location filename="../view/horscreen/washcar.cpp" line="17"/>
        <source>Wash Car</source>
        <translation>ᠮᠠᠰᠢᠨ ᠲᠡᠷᠭᠡ ᠤᠬᠢᠶᠠᠬᠤ ᠵᠡᠷᠭᠡ ᠢᠯᠡᠳᠬᠡᠭᠴᠢ</translation>
    </message>
</context>
<context>
    <name>weekweather</name>
    <message>
        <source>今天</source>
        <translation type="vanished">今天</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="32"/>
        <source>Today</source>
        <translation>ᠦᠨᠦᠳᠦᠷ</translation>
    </message>
</context>
</TS>
