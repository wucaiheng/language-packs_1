<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>KyFileDialog</name>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="146"/>
        <source>Go Back</source>
        <translation>ཕྱིར་ལོག</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="153"/>
        <source>Go Forward</source>
        <translation>མདུན་སྐྱོད།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="159"/>
        <source>Cd Up</source>
        <translation>གོང་ཕྱོགས།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="166"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="174"/>
        <source>View Type</source>
        <translation>མཐོང་རིས་ཀྱི་རིགས།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="184"/>
        <source>Sort Type</source>
        <translation>གོ་རིམ་གྱི་རིགས།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="777"/>
        <source>Name</source>
        <translation>ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="780"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="781"/>
        <location filename="../widget/kyfiledialog.cpp" line="790"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="785"/>
        <source>Save as</source>
        <translation>ཉར་ཚགས་གཞན།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="787"/>
        <source>New Folder</source>
        <translation>ཡིག་ཁུག་གསར་འཛུགས།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="789"/>
        <source>Save</source>
        <translation>གྲོན་ཆུང་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="824"/>
        <location filename="../widget/kyfiledialog.cpp" line="826"/>
        <source>Directories</source>
        <translation>དཀར་ཆག</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1010"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1010"/>
        <source>exist, are you sure replace?</source>
        <translation>གནས་ཡོད་པས། ཁྱོད་ཀྱིས་དངོས་གནས་ཚབ་བྱེད་ཐུབ་བམ།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1013"/>
        <source>ok</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1022"/>
        <source>no</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1452"/>
        <source>NewFolder</source>
        <translation>དཀར་ཆག་གསར་བ།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1601"/>
        <source>Undo</source>
        <translation>ཁ་ཕྱིར་འཐེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1608"/>
        <source>Redo</source>
        <translation>ཡང་བསྐྱར་ལས།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1805"/>
        <source>warn</source>
        <translation>ཉེན་བརྡ་བཏང་བ།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1805"/>
        <source>This operation is not supported.</source>
        <translation>བཀོལ་སྤྱོད་འདི་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
</context>
<context>
    <name>KyFileDialogHelper</name>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1924"/>
        <source>Open File</source>
        <translation>ཁ་ཕྱེ་བའི་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1925"/>
        <source>Save File</source>
        <translation>ཡིག་ཆ་ཉར་ཚགས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1938"/>
        <source>All Files (*)</source>
        <translation>ཡིག་ཆ་ཡོད་ཚད་(*)</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <location filename="../../test/highlighted-icon-button/mainwindow.ui"/>
        <location filename="../../test/mps-style-application/mainwindow.ui"/>
        <location filename="../../test/system-settings/mainwindow.ui"/>
        <source>MainWindow</source>
        <translation>MainWindow</translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>test open</source>
        <translation>ཚད་ལེན་ཚོད་ལྟ་བྱས་ནས</translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>directory</source>
        <translation>དཀར་ཆག</translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>selected uri</source>
        <translation>བདམས་ཟིན་པའི་uri</translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>test show</source>
        <translation>ཚད་ལེན་ཚོད་ལྟའི་ལེ་ཚན</translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>test exec</source>
        <translation>ཚད་ལེན་ཚོད་ལྟ་ཁང་།</translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>test save</source>
        <translation>ཚོད་ལྟའི་གྲོན་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>test static open</source>
        <translation>ཚོད་ལྟའི་ལྷིང་འཇགས་ཀྱི་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../test/highlighted-icon-button/mainwindow.ui"/>
        <location filename="../../test/mps-style-application/mainwindow.ui"/>
        <source>PushButton</source>
        <translation>མཐེབ་གནོན་</translation>
    </message>
    <message>
        <location filename="../../test/highlighted-icon-button/mainwindow.ui"/>
        <source>use auto highlight icon</source>
        <translation>རང་འགུལ་གྱིས་བཀྲག་མདངས་འཚེར་བའི་མཚོན་རྟགས</translation>
    </message>
    <message>
        <location filename="../../test/highlighted-icon-button/mainwindow.ui"/>
        <location filename="../../test/mps-style-application/mainwindow.ui"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../test/highlighted-icon-button/mainwindow.ui"/>
        <source>highlightOnly</source>
        <translation>བཀྲག་མདངས་འཚེརས་ཞིག</translation>
    </message>
    <message>
        <location filename="../../test/highlighted-icon-button/mainwindow.ui"/>
        <source>bothDefaultAndHighlight</source>
        <translation>ཕྱོགས་གཉིས་ཀས་ཉེན་འགོག་འགན་ལེན་གྱི་འོད་ཟེར་འཕྲོ་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../test/mps-style-application/mainwindow.ui"/>
        <source>RadioButton</source>
        <translation>ཀུན་ཁྱབ་རླུང་འཕྲིན་ལས་ཁུངས།</translation>
    </message>
    <message>
        <location filename="../../test/system-settings/mainwindow.ui"/>
        <source>style</source>
        <translation>ཉམས་འགྱུར།</translation>
    </message>
    <message>
        <location filename="../../test/system-settings/mainwindow.ui"/>
        <source>icon</source>
        <translation>icon</translation>
    </message>
    <message>
        <location filename="../../test/system-settings/mainwindow.ui"/>
        <source>menu opacity</source>
        <translation>ཟས་ཐོའི་སྟེང་གི་གོ་མི་ཆོད་པའི་</translation>
    </message>
    <message>
        <location filename="../../test/system-settings/mainwindow.ui"/>
        <source>font</source>
        <translation>ཡིག་གཟུགས་</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../widget/message-box.h" line="190"/>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation>ལག་བསྟར་བྱེད་ཆོག་པའི་&apos;%1&apos;ལ་Qt %2,Qt%3རྙེད་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../widget/message-box.h" line="192"/>
        <source>Incompatible Qt Library Error</source>
        <translation>ཕན་ཚུན་མཐུན་ཐབས་མེད་པའི་Qt དཔེ་མཛོད་ཁང་གི་ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../widget/message-box.cpp" line="405"/>
        <location filename="../widget/message-box.cpp" line="1075"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../widget/message-box.cpp" line="461"/>
        <location filename="../widget/message-box.cpp" line="866"/>
        <location filename="../widget/message-box.cpp" line="1376"/>
        <source>Show Details...</source>
        <translation>ཞིབ་ཕྲའི་གནས་ཚུལ་གསལ་བཤད་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../widget/message-box.cpp" line="866"/>
        <location filename="../widget/message-box.cpp" line="1376"/>
        <source>Hide Details...</source>
        <translation>གནས་ཚུལ་ཞིབ་ཕྲ་སྦས་སྐུང་བྱེད་</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="19"/>
        <source>File Name</source>
        <translation>ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="23"/>
        <source>Modified Date</source>
        <translation>བཟོ་བཅོས་བརྒྱབ་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="27"/>
        <source>File Type</source>
        <translation>ཡིག་ཆའི་རིགས་གྲས།</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="31"/>
        <source>File Size</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="35"/>
        <source>Original Path</source>
        <translation>ཐོག་མའི་འགྲོ་ལམ།</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="44"/>
        <source>Descending</source>
        <translation>མར་འབབ་པ།</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="49"/>
        <source>Ascending</source>
        <translation>རིམ་པ་ཇེ་མང་དུ་འགྲོ་བཞིན།</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="55"/>
        <source>Use global sorting</source>
        <translation>ཁྱོན་ཡོངས་ཀྱི་གོ་རིམ་བེད་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="75"/>
        <source>List View</source>
        <translation>མཐོང་རིས་རེའུ་མིག</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="76"/>
        <source>Icon View</source>
        <translation>མཚོན་རྟགས་ལྟ་ཚུལ།</translation>
    </message>
</context>
<context>
    <name>UKUI::TabWidget::DefaultSlideAnimatorFactory</name>
    <message>
        <location filename="../../libqt5-ukui-style/animations/tabwidget/ukui-tabwidget-default-slide-animator-factory.h" line="49"/>
        <source>Default Slide</source>
        <translation>ཁ་ཆད་བཞག་པའི་སྒྲོན་བརྙན</translation>
    </message>
    <message>
        <location filename="../../libqt5-ukui-style/animations/tabwidget/ukui-tabwidget-default-slide-animator-factory.h" line="50"/>
        <source>Let tab widget switch with a slide animation.</source>
        <translation>སྒྲོན་བརྙན་གྱི་འགུལ་རིས་ལ་བརྟེན་ནས་ཤོག་བྱང་ཆུང་ཆུང་བརྗེ་རེས་བྱེད་དུ་འཇུག་དགོས།</translation>
    </message>
</context>
</TS>
