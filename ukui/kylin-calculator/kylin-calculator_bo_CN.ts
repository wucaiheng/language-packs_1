<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Calc</name>
    <message>
        <source>The expression is empty!</source>
        <translation>མཚོན་པའི་རྣམ་པ་སྟོང་བ་ཡིན།</translation>
    </message>
    <message>
        <source>Expression error!</source>
        <translation>མཚོན་ཚུལ་ནོར་སྐྱོན།!</translation>
    </message>
    <message>
        <source>Missing left parenthesis!</source>
        <translation>གུག་རྟགས་གཡོན་པ་ཆད་འདུག!</translation>
    </message>
    <message>
        <source>The value is too large!</source>
        <translation>གྲངས་ཆེ་དྲགས་པ།!</translation>
    </message>
    <message>
        <source>Miss operand!</source>
        <translation>བཀོལ་སྤྱོད་གྲངས་མེད།!</translation>
    </message>
    <message>
        <source>Operator undefined!</source>
        <translation>རྩིས་རྟགས་ལ་མཚན་ཉིད་བཞག་མེད་པ།!</translation>
    </message>
    <message>
        <source>Divisor cannot be 0!</source>
        <translation>བགོ་གྲངས་ནི་༠ཡིན་མི་རུང་།!</translation>
    </message>
    <message>
        <source>The shifted right operand is negative!</source>
        <translation type="vanished">གཡས་སྤོས་བཀོལ་སྤྱོད་ཀྱི་གྲངས་ཀ་ལ་མཚོན་ན།!</translation>
    </message>
    <message>
        <source>Right operand error!</source>
        <translation>གཡས་ཕྱོགས་ཀྱི་བཀོལ་སྤྱོད་གྲངས་ཀ་ནོར་འདུག!</translation>
    </message>
</context>
<context>
    <name>FuncList</name>
    <message>
        <source>Unit converter</source>
        <translation>ཕབ་རྩིས་ཆས།</translation>
    </message>
    <message>
        <source>exchange rate</source>
        <translation>དངུལ་བརྗེས་འཛའ་ཐང་།</translation>
    </message>
    <message>
        <source>standard</source>
        <translation>ཚད་གཞི།</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation>རྩིས་ཆས།</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>ཚན་རིག</translation>
    </message>
</context>
<context>
    <name>IntelModeList</name>
    <message>
        <source>standard</source>
        <translation>ཚད་གཞི།</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>ཚན་རིག</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Copy</source>
        <translation>འདྲ་ཕབ།</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation>སྦྱར་བ།</translation>
    </message>
    <message>
        <source>input too long</source>
        <translation>ནང་འཇུག་དུས་ཚོད་རིང་བ།</translation>
    </message>
    <message>
        <source>exchange rate</source>
        <translation>འཛའ་ཐང་།</translation>
    </message>
    <message>
        <source>Input error!</source>
        <translation>ནང་འཇུག་ནོར་འཁྲུལ་བྱུང་བ།!</translation>
    </message>
    <message>
        <source>Error!</source>
        <translation>ནོར་འཁྲུལ།!</translation>
    </message>
    <message>
        <source>standard</source>
        <translation>ཚད་གཞི།</translation>
    </message>
    <message>
        <source>calculator</source>
        <translation>རྩིས་ཆས།</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation>རྩིས་ཆས།</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>ཚན་རིག</translation>
    </message>
</context>
<context>
    <name>ProgramDisplay</name>
    <message>
        <source>input too long!</source>
        <translation>ནང་འཇུག་རིང་བ།!</translation>
    </message>
</context>
<context>
    <name>ProgramModel</name>
    <message>
        <source>Input error!</source>
        <translation>ནང་འཇུག་ནོར་འཁྲུལ་བྱུང་བ།!</translation>
    </message>
    <message>
        <source>DisplayBinary</source>
        <translation type="vanished">ཁྱད་པ་གཉིས་ལྡན་པ།</translation>
    </message>
    <message>
        <source>HideBinary</source>
        <translation>གཉིས་གོང་འགྲིལ་ལུགས་སྦས་སྐུང་།</translation>
    </message>
    <message>
        <source>ShowBinary</source>
        <translation>གཉིས་གོང་འགྲིལ་ལུགས་མངོན་པ།</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Close</source>
        <translation>ཁ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>ཆེས་ཆུང་འགྱུར།</translation>
    </message>
    <message>
        <source>Exchange Rate</source>
        <translation>རྩིས་ཆས། འཛའ་ཐང་།</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation>ཚད་གཞི།</translation>
    </message>
    <message>
        <source>StayTop</source>
        <translation>མགོར་འཇོག་པ།</translation>
    </message>
    <message>
        <source>Scientific</source>
        <translation>ཚན་རིག</translation>
    </message>
    <message>
        <source>standard</source>
        <translation>ཚད་གཞི།</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>ཚན་རིག</translation>
    </message>
    <message>
        <source>Programmer</source>
        <translation>རྩིས་ཆས། བྱ་རིམ་པ།</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation>ཆེས་ཆེ་བ།</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <source>DisplayBinary</source>
        <translation type="vanished">ཁྱད་པ་གཉིས་ལྡན་པ།</translation>
    </message>
    <message>
        <source>HideBinary</source>
        <translation>གཉིས་གོང་འགྲིལ་ལུགས་སྦས་སྐུང་།</translation>
    </message>
    <message>
        <source>ShowBinary</source>
        <translation>གཉིས་གོང་འགྲིལ་ལུགས་མངོན་པ།</translation>
    </message>
</context>
<context>
    <name>ToolModelOutput</name>
    <message>
        <source>US Dollar</source>
        <translation>ཨ་སྒོར།</translation>
    </message>
    <message>
        <source>Rate update</source>
        <translation>འཛའ་ཐང་གསར་བསྒྱུར།</translation>
    </message>
    <message>
        <source>Chinese Yuan</source>
        <translation>མི་དམངས་ཤོག་སྒོར།</translation>
    </message>
    <message>
        <source>Error!</source>
        <translation>ནོར་འཁྲུལ་བྱུང་བ་རེད།!</translation>
    </message>
</context>
<context>
    <name>UnitListWidget</name>
    <message>
        <source>cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>search</source>
        <translation type="vanished">བཤེར་འཚོལ།</translation>
    </message>
    <message>
        <source>currency</source>
        <translation>དངུལ་ལོར།</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Auto</source>
        <translation>རང་འགུལ།</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>ཚོས་གཞི་ནག་པོ།</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>རོགས་རམ།</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation>འདེམས་བྱང་།</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>ཕྱིར་འབུད།</translation>
    </message>
    <message>
        <source>About</source>
        <translation>འབྲེལ་ཡོད།</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>མདོག་སྐྱ་བོ།</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>བརྗོད་གཞི།</translation>
    </message>
    <message>
        <source>Exchange Rate</source>
        <translation>འཛའ་ཐང་།</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར་ཚོགས་པ།： </translation>
    </message>
    <message>
        <source>Version: </source>
        <translation>པར་གཞིའི་ཨང་། </translation>
    </message>
    <message>
        <source>Standard</source>
        <translation>ཚད་གཞི།</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation>རྩིས་ཆས།</translation>
    </message>
    <message>
        <source>Scientific</source>
        <translation>ཚན་རིག</translation>
    </message>
    <message>
        <source>Programmer</source>
        <translation>བྱ་རིམ་པ།</translation>
    </message>
    <message>
        <source>Calculator is a lightweight calculator based on Qt5, which provides standard calculation, scientific calculation and exchange rate conversion.</source>
        <translation>རྩིས་ཆས་ནི་qt5གསར་སྤེལ་བྱས་པའི་ཡང་རིམ་རྩིས་ཆས་ཤིག་ཡིན་པ་དང་།ཚད་ལྡན་རྩིས་རྒྱག་མཁོ་འདོན་དང་།ཚན་རིག་དང་མཐུན་པའི་རྩིས་རྒྱག་དང་དངུལ་བརྗེས་འཛའ་ཐང་བརྗེ་རྩིས་རྒྱག་དགོས།</translation>
    </message>
</context>
</TS>
