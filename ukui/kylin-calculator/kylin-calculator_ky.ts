<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>Calc</name>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="85"/>
        <source>The expression is empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="106"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="121"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="126"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="153"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="180"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="247"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="272"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="298"/>
        <source>Expression error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="116"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="140"/>
        <source>Missing left parenthesis!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="207"/>
        <source>The value is too large!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="219"/>
        <source>Miss operand!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="328"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="386"/>
        <source>Operator undefined!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="350"/>
        <source>Divisor cannot be 0!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="370"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="378"/>
        <source>Right operand error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The shifted right operand is negative!</source>
        <translation type="vanished">移位操作右值不能为负数!</translation>
    </message>
</context>
<context>
    <name>FuncList</name>
    <message>
        <location filename="../src/funclist.cpp" line="37"/>
        <source>Calculator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>standard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>scientific</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="58"/>
        <source>Unit converter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="64"/>
        <source>exchange rate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IntelModeList</name>
    <message>
        <location filename="../src/basicbutton.cpp" line="129"/>
        <source>standard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/basicbutton.cpp" line="132"/>
        <source>scientific</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="275"/>
        <source>Calculator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1195"/>
        <location filename="../src/mainwindow.cpp" line="1377"/>
        <location filename="../src/mainwindow.cpp" line="1393"/>
        <source>standard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1375"/>
        <source>calculator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="302"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="303"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1052"/>
        <source>input too long</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="55"/>
        <location filename="../src/mainwindow.cpp" line="1377"/>
        <location filename="../src/mainwindow.cpp" line="1397"/>
        <source>scientific</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1410"/>
        <source>exchange rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="947"/>
        <location filename="../src/mainwindow.cpp" line="951"/>
        <source>Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="955"/>
        <source>Input error!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProgramDisplay</name>
    <message>
        <location filename="../src/programmer/programdisplay.cpp" line="56"/>
        <location filename="../src/programmer/programdisplay.cpp" line="79"/>
        <source>input too long!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProgramModel</name>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="234"/>
        <location filename="../src/programmer/programmodel.cpp" line="318"/>
        <source>Input error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="400"/>
        <source>ShowBinary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>DisplayBinary</source>
        <translation type="vanished">显示二进制</translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="406"/>
        <source>HideBinary</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>FuncList</source>
        <translation type="vanished">功能列表</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="264"/>
        <source>Standard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="265"/>
        <source>Scientific</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>standard </source>
        <translation type="vanished">标准 </translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="56"/>
        <location filename="../src/titlebar.cpp" line="67"/>
        <location filename="../src/titlebar.cpp" line="232"/>
        <source>standard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="57"/>
        <location filename="../src/titlebar.cpp" line="244"/>
        <source>scientific</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>scientific </source>
        <translation type="vanished">科学 </translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="266"/>
        <source>Exchange Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="267"/>
        <source>Programmer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="292"/>
        <source>StayTop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="110"/>
        <location filename="../src/titlebar.cpp" line="293"/>
        <source>Minimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="111"/>
        <location filename="../src/titlebar.cpp" line="294"/>
        <source>Maximize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="112"/>
        <location filename="../src/titlebar.cpp" line="295"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <source>DisplayBinary</source>
        <translation type="vanished">显示二进制</translation>
    </message>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="63"/>
        <location filename="../src/programmer/toolbar.cpp" line="219"/>
        <location filename="../src/programmer/toolbar.cpp" line="222"/>
        <source>ShowBinary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="220"/>
        <location filename="../src/programmer/toolbar.cpp" line="221"/>
        <source>HideBinary</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolModelOutput</name>
    <message>
        <location filename="../src/toolmodel.cpp" line="80"/>
        <source>Rate update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="95"/>
        <source>Chinese Yuan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="103"/>
        <source>US Dollar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="515"/>
        <source>Error!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UnitListWidget</name>
    <message>
        <location filename="../src/toolmodel.cpp" line="1163"/>
        <source>currency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="1169"/>
        <source>cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>search</source>
        <translation type="vanished">搜索</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="44"/>
        <source>Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="56"/>
        <location filename="../src/menumodule/menumodule.cpp" line="155"/>
        <source>Standard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="58"/>
        <location filename="../src/menumodule/menumodule.cpp" line="157"/>
        <source>Scientific</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="60"/>
        <location filename="../src/menumodule/menumodule.cpp" line="159"/>
        <source>Exchange Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="62"/>
        <location filename="../src/menumodule/menumodule.cpp" line="161"/>
        <source>Programmer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="68"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="70"/>
        <location filename="../src/menumodule/menumodule.cpp" line="153"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="72"/>
        <location filename="../src/menumodule/menumodule.cpp" line="151"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="74"/>
        <location filename="../src/menumodule/menumodule.cpp" line="149"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="85"/>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="89"/>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="93"/>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="302"/>
        <source>Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="306"/>
        <source>Calculator is a lightweight calculator based on Qt5, which provides standard calculation, scientific calculation and exchange rate conversion.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="371"/>
        <location filename="../src/menumodule/menumodule.cpp" line="379"/>
        <source>Service &amp; Support: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Support: support@kylinos.cn</source>
        <translation type="vanished">支持：support@kylinos.cn</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.h" line="66"/>
        <source>Calculator</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
