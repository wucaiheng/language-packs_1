<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>DictionaryForChinesePrinter</name>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="7"/>
        <source>letter</source>
        <translation>ھەرىپى</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="8"/>
        <source>Page Size</source>
        <translation>بەت چوڭ - كىچىكلىكى</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="9"/>
        <source>Resolution</source>
        <translation>قارار</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="10"/>
        <source>Paper Source</source>
        <translation>قەغەز مەنبەسى</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="11"/>
        <source>Source</source>
        <translation>مەنبە</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="12"/>
        <source>PageType</source>
        <translation>PageType</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="13"/>
        <source>Media Type</source>
        <translation>مېدىيا تۈرى</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="14"/>
        <source>Color</source>
        <translation>رەڭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="15"/>
        <source>Direction</source>
        <translation>يۆنىلىش</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="16"/>
        <source>Duplex</source>
        <translation>دۇپلېكس</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="17"/>
        <source>Pages per Side</source>
        <translation>&quot;ھەر يان&quot; تۈردىكى بەتلەر</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="18"/>
        <source>Ink Rank</source>
        <translation>سىياھ رەت تەرتىپى</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="19"/>
        <source>Output Order</source>
        <translation>چىقىرىش تەرتىپى</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="20"/>
        <source>legal</source>
        <translation>قانۇن-قانۇن</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="21"/>
        <source>statement</source>
        <translation>بايانات</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="22"/>
        <source>executive</source>
        <translation>ئىجرائىيە ئەمەلدارى</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="23"/>
        <source>monarch</source>
        <translation>پادىشاھ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="24"/>
        <source>com10</source>
        <translation>com10</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="25"/>
        <source>auto</source>
        <translation>ئاپتو</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="26"/>
        <source>multi-purpose tray</source>
        <translation>كۆپ مەقسەدلىك تاختا</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="27"/>
        <source>drawer 1</source>
        <translation>تارتما رەسىم 1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="28"/>
        <source>drawer 2</source>
        <translation>تارتما تارتما 2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="29"/>
        <source>plain paper</source>
        <translation>تۈز قەغەز</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="30"/>
        <source>left</source>
        <translation>سول</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="31"/>
        <source>right</source>
        <translation>ئوڭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="32"/>
        <source>top</source>
        <translation>چوققا</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="33"/>
        <source>bindingedge</source>
        <translation>باغلاش</translation>
    </message>
</context>
</TS>
