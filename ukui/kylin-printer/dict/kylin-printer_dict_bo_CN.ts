<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>DictionaryForChinesePrinter</name>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="7"/>
        <source>letter</source>
        <translation>ཡིག་ཤོག</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="8"/>
        <source>Page Size</source>
        <translation>ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="9"/>
        <source>Resolution</source>
        <translation>འབྱེད་ཕྱོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="10"/>
        <source>Paper Source</source>
        <translation>འབྱུང་ཁུངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="11"/>
        <source>Source</source>
        <translation>འབྱུང་ཁུངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="12"/>
        <source>PageType</source>
        <translation>ཤོག་ངོས་ཀྱི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="13"/>
        <source>Media Type</source>
        <translation>ཤོག་བུའི་རིགས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="14"/>
        <source>Color</source>
        <translation>ཁ་དོག</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="15"/>
        <source>Direction</source>
        <translation>བསྐྱར་དཔར་ཁ་ཕྱོགས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="16"/>
        <source>Duplex</source>
        <translation>ངོས་གཉིས་དཔར་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="17"/>
        <source>Pages per Side</source>
        <translation>ཕྱོགས་རེ་རེའི་ཤོག་ལྷེ་གྲངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="18"/>
        <source>Ink Rank</source>
        <translation>སྣག་ཚའི་ཚད་རིམ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="19"/>
        <source>Output Order</source>
        <translation>ཕྱིར་གཏོང་གི་གོ་རིམ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="20"/>
        <source>legal</source>
        <translation>ཁྲིམས་མཐུན་རང་བཞིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="21"/>
        <source>statement</source>
        <translation>གསལ་བསྒྲགས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="22"/>
        <source>executive</source>
        <translation>སྲིད་འཛིན་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="23"/>
        <source>monarch</source>
        <translation>མདོག་རྐྱང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="24"/>
        <source>com10</source>
        <translation>ཕྲེང་ཁ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="25"/>
        <source>auto</source>
        <translation>རང་འགུལ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="26"/>
        <source>multi-purpose tray</source>
        <translation>སྤྱོད་སྒོ་མང་བའི་སྡེར་མ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="27"/>
        <source>drawer 1</source>
        <translation>ཤོག་སྒམ1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="28"/>
        <source>drawer 2</source>
        <translation>ཤོག་སྒམ་2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="29"/>
        <source>plain paper</source>
        <translation>སྤྱིར་བཏང་གི་ཤོག་བུ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="30"/>
        <source>left</source>
        <translation>གཡོན་ཕྱོགས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="31"/>
        <source>right</source>
        <translation>གཡས་ཕྱོགས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="32"/>
        <source>top</source>
        <translation>ཀླད་འཆར།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="33"/>
        <source>bindingedge</source>
        <translation>འཚེམ་སྒྲིག</translation>
    </message>
</context>
</TS>
