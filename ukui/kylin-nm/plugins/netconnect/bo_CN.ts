<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AddNetBtn</name>
    <message>
        <location filename="../addnetbtn.cpp" line="22"/>
        <source>Add WiredNetork</source>
        <translation>སྐུད་ཡོད་དྲ་རྒྱ་སྣོན་པ།</translation>
    </message>
</context>
<context>
    <name>NetConnect</name>
    <message>
        <location filename="../netconnect.ui" line="50"/>
        <location filename="../netconnect.cpp" line="152"/>
        <source>Wired Network</source>
        <translation>སྐུད་ཡོད་དྲ་བ།</translation>
    </message>
    <message>
        <location filename="../netconnect.ui" line="112"/>
        <location filename="../netconnect.cpp" line="154"/>
        <source>open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
        <extra-contents_path>/netconnect/open</extra-contents_path>
    </message>
    <message>
        <location filename="../netconnect.ui" line="198"/>
        <location filename="../netconnect.cpp" line="151"/>
        <source>Advanced settings</source>
        <translation>སྔོན་ཐོན་གྱི་སྒྲིག་བཀོད།</translation>
        <extra-contents_path>/netconnect/Advanced settings&quot;</extra-contents_path>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="63"/>
        <source>ukui control center</source>
        <translation>ཚོད་འཛིན་ངོས་པང་།</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="66"/>
        <source>ukui control center desktop message</source>
        <translation>ཚོད་འཛིན་ངོས་པང་ཀྱི་གཙོ་ངོས་བརྡ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="80"/>
        <source>WiredConnect</source>
        <translation>སྐུད་ཡོད་སྦྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="177"/>
        <source>No ethernet device avaliable</source>
        <translation>སྐུད་ཡོད་སྒྲིག་ཆས་ལ་ཞིབ་དཔྱད་ཚད་ལེན་མ་བྱས།</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="426"/>
        <location filename="../netconnect.cpp" line="833"/>
        <source>connected</source>
        <translation>འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="490"/>
        <source>card</source>
        <translation>བྱང་བུ།</translation>
    </message>
</context>
</TS>
