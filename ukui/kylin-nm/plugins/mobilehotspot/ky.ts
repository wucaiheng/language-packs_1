<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>BlacklistItem</name>
    <message>
        <location filename="../blacklistitem.cpp" line="30"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BlacklistPage</name>
    <message>
        <location filename="../blacklistpage.cpp" line="26"/>
        <source>Blacklist</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConnectDevListItem</name>
    <message>
        <location filename="../connectdevlistitem.cpp" line="30"/>
        <source>drag into blacklist</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConnectdevPage</name>
    <message>
        <location filename="../connectdevpage.cpp" line="27"/>
        <source>Connect device</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MobileHotspot</name>
    <message>
        <location filename="../mobilehotspot.cpp" line="35"/>
        <source>MobileHotspot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mobilehotspot.cpp" line="101"/>
        <source>mobilehotspot</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/mobilehotspot/mobilehotspot</extra-contents_path>
    </message>
    <message>
        <location filename="../mobilehotspot.cpp" line="103"/>
        <source>mobilehotspot open</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/mobilehotspot/mobilehotspot open</extra-contents_path>
    </message>
</context>
<context>
    <name>MobileHotspotWidget</name>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="38"/>
        <source>ukui control center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="41"/>
        <source>ukui control center desktop message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="117"/>
        <source>wirless switch is close or no wireless device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="121"/>
        <source>start to close hotspot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="130"/>
        <source>hotpots name or device is invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>can not  create hotspot with password length less than eight!</source>
        <translation type="vanished">不能创建密码长度小于八位的热点！</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="137"/>
        <source>start to open hotspot </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="257"/>
        <source>Contains at least 8 characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="172"/>
        <source>Hotspot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="268"/>
        <location filename="../mobilehotspotwidget.cpp" line="582"/>
        <source>hotspot already close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="408"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="429"/>
        <source>Wi-Fi Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="452"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="490"/>
        <source>Frequency band</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="516"/>
        <source>Net card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="611"/>
        <location filename="../mobilehotspotwidget.cpp" line="619"/>
        <source>hotspot already open</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
