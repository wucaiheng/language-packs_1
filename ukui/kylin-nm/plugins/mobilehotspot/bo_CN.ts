<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>BlacklistItem</name>
    <message>
        <location filename="../blacklistitem.cpp" line="30"/>
        <source>Remove</source>
        <translation>ཕྱིར་སྤོ་བ།</translation>
    </message>
</context>
<context>
    <name>BlacklistPage</name>
    <message>
        <location filename="../blacklistpage.cpp" line="26"/>
        <source>Blacklist</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་ཐོ་ནག་པོ།</translation>
    </message>
</context>
<context>
    <name>ConnectDevListItem</name>
    <message>
        <location filename="../connectdevlistitem.cpp" line="30"/>
        <source>drag into blacklist</source>
        <translation>མིང་ཐོ་ནག་པོའི་ཁ་སྣོན་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>ConnectdevPage</name>
    <message>
        <location filename="../connectdevpage.cpp" line="27"/>
        <source>Connect device</source>
        <translation>འབྲེལ་མཐུད་སྒྲིག་ཆས།</translation>
    </message>
</context>
<context>
    <name>MobileHotspot</name>
    <message>
        <location filename="../mobilehotspot.cpp" line="35"/>
        <source>MobileHotspot</source>
        <translation>སྤོ་འགུལ་ཚ་དྲགས།</translation>
    </message>
    <message>
        <location filename="../mobilehotspot.cpp" line="101"/>
        <source>mobilehotspot</source>
        <translation>སྤོ་འགུལ་ཚ་དྲགས།</translation>
        <extra-contents_path>/mobilehotspot/mobilehotspot</extra-contents_path>
    </message>
    <message>
        <location filename="../mobilehotspot.cpp" line="103"/>
        <source>mobilehotspot open</source>
        <translation>སྤོ་འགུལ་ཚ་དྲགས་སྒོ་ཕྱེ་བ།</translation>
        <extra-contents_path>/mobilehotspot/mobilehotspot open</extra-contents_path>
    </message>
</context>
<context>
    <name>MobileHotspotWidget</name>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="38"/>
        <source>ukui control center</source>
        <translation>ཚོད་འཛིན་ངོས་པང་།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="41"/>
        <source>ukui control center desktop message</source>
        <translation>ཚོད་འཛིན་ངོས་པང་ཀྱི་ཅོག་ངོས་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="117"/>
        <source>wirless switch is close or no wireless device</source>
        <translation>སྐུད་མེད་གློག་སྒོ་བརྒྱབ་ཟིན་པའམ་ཡང་ན་ཀུན་གྱིས་དོ་སྣང་བྱེད་པའི་ནུས་པ་མེད་པའི་སྐུད་མེད་དྲ་བའི་བྱང་བུ།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="121"/>
        <source>start to close hotspot</source>
        <translation>ཚ་གནད་སྒོ་རྒྱག་མགོ་བརྩམས།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="130"/>
        <source>hotpots name or device is invalid</source>
        <translation>ཀུན་གྱིས་དོ་སྣང་བྱེད་པའི་མིང་དང་སྒྲིག་ཆས་ནོར་འདུག</translation>
    </message>
    <message>
        <source>can not  create hotspot with password length less than eight!</source>
        <translation type="vanished">གསང་གྲངས་ཀྱི་རིང་ཚད་ནི་གླེང་མང་བའི་གནད་དོན་བརྒྱད་ལས་ཆུང་བ་བྱེད་མི་རུང་།!</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="137"/>
        <source>start to open hotspot </source>
        <translation>ཀུན་གྱིས་དོ་སྣང་བྱ་ཡུལ་གསར་འཛུགས་བྱེད་འགོ </translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="257"/>
        <source>Contains at least 8 characters</source>
        <translation>མ་མཐའ་ཡང་ཡིག་རྟགས་བརྒྱད་འདུས།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="172"/>
        <source>Hotspot</source>
        <translation>སྤོ་འགུལ་ཚ་གནད།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="268"/>
        <location filename="../mobilehotspotwidget.cpp" line="582"/>
        <source>hotspot already close</source>
        <translation>ཀུན་གྱིས་དོ་སྣང་བྱ་ཡུལ་སྒོ་བརྒྱབ།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="408"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="429"/>
        <source>Wi-Fi Name</source>
        <translation>Wi-Fiཡི་མིང་།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="452"/>
        <source>Password</source>
        <translation>དྲ་རྒྱའི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="490"/>
        <source>Frequency band</source>
        <translation>དྲ་རྒྱའི་བརྒྱུད་ལམ།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="516"/>
        <source>Net card</source>
        <translation>མཉམ་སྤྱོད་དྲ་བྱང་མཐུད་སྣེ།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="611"/>
        <location filename="../mobilehotspotwidget.cpp" line="619"/>
        <source>hotspot already open</source>
        <translation>ཀུན་གྱིས་དོ་སྣང་བྱ་ཡུལ་གྱི་སྒོ་ཕྱེ་ཟིན།</translation>
    </message>
</context>
</TS>
