<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>WlanConnect</name>
    <message>
        <location filename="../wlanconnect.ui" line="14"/>
        <location filename="../wlanconnect.cpp" line="97"/>
        <source>WlanConnect</source>
        <translation>སྐུད་མེད་ཁྱབ་ཆུང་དྲ་བ།</translation>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="35"/>
        <location filename="../wlanconnect.cpp" line="168"/>
        <source>WLAN</source>
        <translation>སྐུད་མེད་ཁྱབ་ཆུང་དྲ་བ།</translation>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="94"/>
        <location filename="../wlanconnect.cpp" line="170"/>
        <source>open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
        <extra-contents_path>/wlanconnect/open</extra-contents_path>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="147"/>
        <location filename="../wlanconnect.cpp" line="167"/>
        <source>Advanced settings</source>
        <translation>སྔོན་ཐོན་གྱི་སྒྲིག་བཀོད།</translation>
        <extra-contents_path>/wlanconnect/Advanced settings&quot;</extra-contents_path>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="80"/>
        <source>ukui control center</source>
        <translation>ཚོད་འཛིན་ངོས་པང་།</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="83"/>
        <source>ukui control center desktop message</source>
        <translation>ཚོད་འཛིན་ངོས་ལེབ་ངོས་ཀྱི་བརྡ་ཐོ།</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="192"/>
        <source>No wireless network card detected</source>
        <translation>སྐུད་མེད་དྲ་བྱང་ལ་ཞིབ་དཔྱད་ཚད་ལེན་མ་བྱས།</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="330"/>
        <location filename="../wlanconnect.cpp" line="933"/>
        <location filename="../wlanconnect.cpp" line="995"/>
        <source>connected</source>
        <translation>འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="879"/>
        <source>card</source>
        <translation>བྱང་བུ།</translation>
    </message>
</context>
</TS>
