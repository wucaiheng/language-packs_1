<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>WlanConnect</name>
    <message>
        <location filename="../wlanconnect.ui" line="14"/>
        <location filename="../wlanconnect.cpp" line="97"/>
        <source>WlanConnect</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠬᠡᠰᠡᠭ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="35"/>
        <location filename="../wlanconnect.cpp" line="168"/>
        <source>WLAN</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠬᠡᠰᠡᠭ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="94"/>
        <location filename="../wlanconnect.cpp" line="170"/>
        <source>open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
        <extra-contents_path>/wlanconnect/open</extra-contents_path>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="147"/>
        <location filename="../wlanconnect.cpp" line="167"/>
        <source>Advanced settings</source>
        <translation>ᠦᠨᠳᠦᠷ ᠵᠡᠷᠬᠡ ᠵᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ</translation>
        <extra-contents_path>/wlanconnect/Advanced settings&quot;</extra-contents_path>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="80"/>
        <source>ukui control center</source>
        <translation>ᠡᠵᠡᠮᠰᠢᠯ ᠦᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="83"/>
        <source>ukui control center desktop message</source>
        <translation>ᠡᠵᠡᠮᠰᠢᠯ ᠦᠨ ᠬᠠᠪᠳᠠᠰᠤᠨ ᠤ ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤᠨ ᠮᠡᠳᠡᠭᠳᠡᠯ</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="192"/>
        <source>No wireless network card detected</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠨᠧᠲ ᠺᠠᠷᠲ᠎ᠢ ᠬᠢᠨᠠᠨ ᠬᠡᠮᠵᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="330"/>
        <location filename="../wlanconnect.cpp" line="933"/>
        <location filename="../wlanconnect.cpp" line="995"/>
        <source>connected</source>
        <translation>ᠨᠢᠬᠡᠨᠳᠡ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="879"/>
        <source>card</source>
        <translation>ᠨᠧᠲ ᠺᠠᠷᠲ</translation>
    </message>
</context>
</TS>
