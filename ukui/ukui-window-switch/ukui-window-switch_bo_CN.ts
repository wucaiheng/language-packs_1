<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN" sourcelanguage="en_US">
<context>
    <name>DesktopArea</name>
    <message>
        <location filename="qml/DesktopArea.qml" line="108"/>
        <source>Desktop</source>
        <translatorcomment>工作区</translatorcomment>
        <translation>ལས་སྒྲུབ་ཁུལ།</translation>
    </message>
    <message>
        <location filename="qml/DesktopArea.qml" line="295"/>
        <source>New Desktop</source>
        <translatorcomment>新建工作区</translatorcomment>
        <translation>གསར་དུ་བཙུགས་པའི་ལས་དོན་ཁུལ།</translation>
    </message>
</context>
<context>
    <name>TabletViewClearAllWindowButton</name>
    <message>
        <location filename="qml/TabletViewClearAllWindowButton.qml" line="43"/>
        <source>Clear</source>
        <translatorcomment>清除</translatorcomment>
        <translation>གསལ་པོར་བཤད་ན།</translation>
    </message>
</context>
<context>
    <name>TabletViewRootWindow</name>
    <message>
        <location filename="qml/TabletViewRootWindow.qml" line="139"/>
        <source>No recent tasks</source>
        <translatorcomment>无应用开启</translatorcomment>
        <translation>ཉེ་ལམ་གྱི་ལས་འགན་གང་ཡང་མེད།</translation>
    </message>
</context>
</TS>
