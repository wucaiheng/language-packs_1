# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-12-21 15:37+0800\n"
"PO-Revision-Date: 2022-09-17 02:19+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Chinese (Simplified) <https://weblate.ubuntukylin.com/"
"projects/logviewer/logviewer/zh_Hans/>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.10.1\n"

#clogoptions.cpp
#caboutdialog.cpp
msgid "Log Tool"
msgstr "日志查看器"

#clogconfig.cpp
msgid "System"
msgstr "系统日志"

msgid "Start-up"
msgstr "启动日志"

msgid "Login"
msgstr "登录日志"

msgid "Application"
msgstr "应用日志"

msgid "Security"
msgstr "安全"

msgid "Kysec Log"
msgstr "麒麟安全"

msgid "Crash Log"
msgstr "崩溃日志"

msgid "Audit Log"
msgstr "审计日志"

#clogcontrol.cpp
msgid "About"
msgstr "关于本机"

msgid "Help"
msgstr "帮助"

msgid "Exit"
msgstr "退出"

msgid "Menu"
msgstr "菜单"

msgid "Minimize"
msgstr "最小化"

msgid "Maximize"
msgstr "最大化"

msgid "Restore"
msgstr "还原"

msgid "Quit"
msgstr "退出"

msgid "ALL"
msgstr "全部"

msgid "Time"
msgstr "时间"

msgid "Start time"
msgstr "开始时间"

msgid "End time"
msgstr "结束时间"

#clogcontrol.cpp
#ItemObject.h
msgid "INFO"
msgstr "信息"

msgid "WARN"
msgstr "注意"

msgid "ERROR"
msgstr "错误"

#cinfotable.cpp
msgid "Host name : %1"
msgstr "主机名 : %1"

#clogcontrol.cpp
#cinfotable.cpp
#cmytablewidget.cpp
msgid "Export"
msgstr "导出"

#csearchwidget.cpp
msgid "Search"
msgstr "搜索"

msgid "Please enter search content"
msgstr "请输入搜索内容"

#ctimefilterwidget.cpp
msgid "Today"
msgstr "今天"

msgid "Last three days"
msgstr "近三天"

msgid "Last week"
msgstr "近一周"

msgid "Last month"
msgstr "近一月"

msgid "Last three months"
msgstr "近三月"

msgid "Start Date"
msgstr "开始日期"

msgid "End Date"
msgstr "结束日期"

msgid "Reset"
msgstr "清空"

msgid "End time must be later than start time"
msgstr "结束时间需晚于开始时间"

#clogcontrol.cpp
#ctimefilterwidget.cpp
msgid "Start Time"
msgstr "开始时间"

msgid "End Time"
msgstr "结束时间"

#ctimerollingdialog.cpp
msgid "Cancle"
msgstr "取消"

#ctimefilterwidget.cpp
#ctimerollingdialog.cpp
msgid "Ok"
msgstr "还行"

#ccalendarwidgetcpp
msgid "%1-%2"
msgstr "%1年%2月"

msgid "Sun"
msgstr "星期日"

msgid "Mon"
msgstr "星期一"

msgid "Tue"
msgstr "星期二"

msgid "Wed"
msgstr "星期三"

msgid "Thu"
msgstr "四"

msgid "Fri"
msgstr "星期五"

msgid "Sat"
msgstr "星期六"

#caboutdialog.cpp
msgid "Log tool is a centralized display tool for system logs, which provides log parsing and classified display functions."
msgstr "日志查看器是一款系统日志集中展示工具，提供日志解析和分类显示功能。"

msgid "Version Number: %1"
msgstr "版本号: %1"

msgid "Service & Support:"
msgstr "服务与支持团队:"

#clogtable.cpp
msgid "Export success"
msgstr "导出成功"

msgid "Export error"
msgstr "导出失败"

msgid "Permission denied:please try to change the directory"
msgstr "当前用户无权限导出日志到所选目录"

msgid "File Exits:Overwrite the existing file?"
msgstr "文件已存在，是否覆盖该文件?"

msgid "The selection is empty"
msgstr "所选内容为空"

#clogtable.cpp
#cinfotable.cpp
msgid "Refresh"
msgstr "刷新"

msgid "Copy"
msgstr "复制"

msgid "Loading,please wait"
msgstr "数据加载中，请稍候"

#SysPanelItem.h
msgid "Process"
msgstr "进程"

#AppTable.cpp
#AuditTable.cpp
#BootTable.cpp
#CupsAccessLog.cpp
#ExceptionTable.cpp
#KysecTable
#LoginPanelItme.h
#LoginTable.cpp
#SysTable.cpp
msgid "Information"
msgstr "信息"

#BootPanelItem.h
msgid "Position"
msgstr "路径"

#CupsAccessLog.cpp
#LoginTable.cpp
msgid "Ip"
msgstr "IP"

#LoginTable.cpp
msgid "Port"
msgstr "端口"

msgid "Logout time"
msgstr "登出时间"

msgid "Reason"
msgstr "失败原因"

#AppPanelItem.h
#AppTable.cpp
msgid "App"
msgstr "应用"

#CupsAccessLog.cpp
msgid "Email"
msgstr "邮箱"

#CupsAccessLog.cpp
#LoginTable.cpp
msgid "User"
msgstr "用户"

#CupsAccessLog.cpp
msgid "Policy"
msgstr "协议"

msgid "State Number"
msgstr "状态码"

msgid "Byte Size"
msgstr "发送字节数"

#AppTable.cpp
msgid "Error Information"
msgstr "错误信息"

#AlternativesLog.cpp
msgid "Option"
msgstr "操作"

#AppTable.cpp
msgid "Package Name"
msgstr "包"

msgid "Version"
msgstr "版本"

#AppTable.cpp
#AlternativesLog.cpp
msgid "Name"
msgstr "名称"

#AlternativesLog.cpp
msgid "Priority"
msgstr "优先级"

#KysecTable.cpp
msgid "Pid"
msgstr "进程ID"

msgid "Uid"
msgstr "用户ID"

msgid "Comm"
msgstr "命令"

msgid "Op"
msgstr "操作"

msgid "Obj"
msgstr "对象"

msgid "Message"
msgstr "内容"

#AuditPanelItem.h
#AuditTable.cpp
#BootTable.cpp
#ExceptionPanelItem.h
#ExceptionTable.cpp
msgid "Type"
msgstr "类型"

#ExceptionTable.cpp
msgid "Core Path"
msgstr "Core 文件路径"

msgid "Elf Path"
msgstr "Elf 文件路径"

#SysTable.cpp
#LoginTable.cpp
#KysecTable.cpp
#ExceptionTable.cpp
#BootTable.cpp
#AuditTable.cpp
#AppTable.cpp
#CLogControl.cpp
msgid "Level"
msgstr "级别"

#AppTable.cpp
#AuditTable.cpp
#BootTable.cpp
#SysTable.cpp
#LoginTable.cpp
msgid "Remote login success"
msgstr "远程登录成功"

msgid "Remote login failure"
msgstr "远程登录失败"

msgid "Local login success"
msgstr "本地登录成功"

msgid "Local login failure"
msgstr "本地登录失败"

msgid "Login user"
msgstr "登录用户"

msgid "Status"
msgstr "状态"

msgid "Logging in"
msgstr "登录中"

msgid "Logged out"
msgstr "已登出"

msgid "Account locked"
msgstr "当前账户已锁定"

msgid "Account validation failed"
msgstr "账户信息验证失败"

#KysecTable.cpp
#ExceptionTable.cpp
#ItemObject.h
msgid "UNKNOWN"
msgstr "未知"

#BootLog.cpp
msgid "Boot"
msgstr "开机启动"

#DmesgLog.cpp
msgid "Dmesg"
msgstr "硬件启动"

#LoginTable.cpp
msgid "UNKNOWN USER"
msgstr "未知用户"

msgid "File names cannot contain special characters"
msgstr "文件名不能包含特殊字符"

#XrdpLog.cpp
msgid "Remote desktop login"
msgstr "远程桌面登录"

#CmdMgr.cpp
msgid " - view log content"
msgstr " - 查看日志内容"

msgid "Summary"
msgstr "概要"

msgid "[options] [keyword...]"
msgstr "[选项] [关键字...]"

msgid "POSIX standard options： [-hkse012ytlacrup]"
msgstr "POSIX 标准选项： [-hkse012ytlacrup]"

msgid "GNU options (short format):"
msgstr "GNU 选项 (短格式):"

msgid "Describe"
msgstr "描述"

msgid "Logview can view different logs according to different options. If no log is entered"
msgstr "程序logview可以根据不同选项查看不同日志，如果不填写日志"

msgid "Type parameters enter the help documentation by default"
msgstr "类型参数默认进入帮助文档"

msgid "Note: If the option parameter contains a space, you should enter'\\', for example: abc\\ def"
msgstr "注意：选项参数中若包含空格，应输入'\\ ',例如：abc\\ def"

msgid "Help document, you can view the options and how the options are implemented"
msgstr "帮助文档，可以查看选项及选项参数如何实现"

msgid "View all log information of the system log"
msgstr "查看系统日志的所有日志信息"

msgid "View all log information of the startup log"
msgstr "查看启动日志的所有日志信息"

msgid "View all log information of the login log"
msgstr "查看登录日志的所有日志信息"

msgid "View all log information of the application log"
msgstr "查看应用日志的所有日志信息"

msgid "View all log information of the kysce log"
msgstr "查看麒麟安全日志的所有日志信息"

msgid "View all log information of the crash log"
msgstr "查看崩溃日志的所有日志信息"

msgid "View all log information of the audit log"
msgstr "查看审计日志的所有日志信息"

msgid "View all log information of the httc log"
msgstr "查看指令流日志的所有日志信息"

msgid "Filter the second and fourth columns of the log by keyword, which is used in combination with the log option"
msgstr "按关键字筛选日志第二列和第四列，和日志选项搭配使用"

msgid "It is recommended to use \"\" to search for keywords, for example: "
msgstr "建议使用\"\"搜索关键字,例： "

msgid "Filter out all log information of a log from the specified time"
msgstr "筛选出一种日志中从指定时间开始的所有日志信息"

msgid " -l -s 2021-06-18\\ 12:00:00, must be added here\\"
msgstr " -l -s 2021-06-18\\ 12:00:00,此处必须加入\\"

msgid "Example:"
msgstr "例："

msgid "Filter out all information of a log up to the specified time"
msgstr "筛选出一种日志截止到指定时间的所有信息"

msgid " -l -e 2021-06-18\\ 12:00:00, must be added here\\"
msgstr " -l -e 2021-06-18\\ 12:00:00,此处必须加入\\"

msgid "Filter out the log information of the error level"
msgstr "筛选出错误等级的日志信息"

msgid "Filter out the log information of warning level"
msgstr "筛选出警告等级的日志信息"

msgid "Filter out the log information of the information level"
msgstr "筛选出信息等级的日志信息"

msgid "Export the specified log to a path"
msgstr "导出指定日志到某个路径下"

msgid "use -h or --help to get help"
msgstr "使用 -h 或者 --help 获取帮助"

msgid "Loading logs ... "
msgstr "正在加载日志... "

msgid " does not exist !"
msgstr " 不存在！"

msgid " is not a path !"
msgstr " 不是路径！"

msgid " does not have permission !"
msgstr " 没有权限！"

msgid "Time parameter format error !"
msgstr "时间参数格式错误！"

msgid "Export log succeeded"
msgstr "导出日志成功"

msgid "Parameter does not meet specification : "
msgstr "参数不符合规范: "

#TiangouLog.cpp
msgid "Attack Type"
msgstr "攻击类型"

msgid "Load dynamic library events:Load dynamic library and execute malicious code"
msgstr "加载动态库事件：加载动态库，执行恶意代码"

msgid "Create process:Create illegal programs to release viruses or trojans"
msgstr "创建进程：创建非法程序，释放病毒或木马"

msgid "Drive loading:Illegally loading malicious drivers to break through system defense"
msgstr "驱动加载：非法加载恶意驱动，突破系统防御"

msgid "Execute application:Illegal execution of reference program, suspected virus intrusion"
msgstr "执行应用程序：非法执行引用程序，疑似病毒入侵"

msgid "File read open:An illegal program reads a file and attempts to steal user data"
msgstr "文件读打开：非法程序读取文件，试图窃取用户数据"

msgid "File write open:Illegal program reads and writes files and tries to steal user data"
msgstr "文件写打开：非法程序读写文件，试图窃取用户数据"

msgid "File creation:Suspicious file creation found, trying to steal user data"
msgstr "文件创建：发现可疑文件创建，试图窃取用户数据"

msgid "Delete file:Illegal deletion of files in an attempt to destroy user data"
msgstr "删除文件：非法删除文件，试图破坏用户数据"

msgid "Delete directory:Illegal directory deletion, trying to destroy user data"
msgstr "删除目录：非法删除目录，试图破坏用户数据"

msgid "directories creating:Suspicious directory creation found, trying to steal user data"
msgstr "目录创建：发现可疑目录创建，试图窃取用户数据"

msgid "File rename:Illegal renaming of files to steal user data"
msgstr "文件重命名：非法重命名文件，试图窃取用户数据"

msgid "Create hard link:Illegal creation of hard links, trying to steal user data"
msgstr "创建硬链接：非法创建硬链接，试图窃取用户数据"

msgid "Create soft connection:Illegal creation of soft connection and attempt to steal user data"
msgstr "创建软连接：非法创建软连接，试图窃取用户数据"

msgid "Change UNIX permissions for files:Illegally changing UNIX permissions of files, trying to steal user data"
msgstr "更改文件unix权限：非法更改文件unix权限，试图窃取用户数据"

msgid "Change file ownership:Illegal change of file ownership, trying to steal user data"
msgstr "更改文件属主：非法更改文件属主，试图窃取用户数据"

msgid "Resize file:Illegal file resizing, trying to steal user data"
msgstr "调整文件大小：非法非法调整文件大小，试图窃取用户数据"

msgid "change file POSIX ACL:Illegally changing the POSIX ACL of the file in an attempt to steal user data"
msgstr "更改文件 POSIX ACL：非法更改文件POSIX ACL，试图窃取用户数据"

msgid "file IO operation:An abnormal file IO operation was found and attempted to steal user data"
msgstr "文件 IO 操作：发现异常的文件IO操作，试图窃取用户数据"

msgid "MMAP executable memory allocation:Malicious file execution in memory is found, which is suspected of virus intrusion"
msgstr "mmap-可执行内存分配：发现内存恶意文件执行，疑似病毒入侵"

msgid "Mprotect - modifies the protected mode of the memory image:Illegally modify the protection mode of memory image to attempt to attack the system"
msgstr "mprotect-修改内存映像的保护模式：非法修改内存映像的保护模式，试图攻击系统"

msgid "linux-ptrace:Illegal tracking process execution, trying to attack the system"
msgstr "linux-ptrace：非法追踪进程执行，试图攻击系统"

msgid "socket_create:Illegal socket creation, attempting to access network resources beyond authority"
msgstr "socket_create：非法创建socket，试图越权访问网络资源"

msgid "socket_bind:Illegally binding socket, trying to access network resources beyond authority"
msgstr "socket_bind：非法绑定socket，试图越权访问网络资源"

msgid "socket_connect:Illegal connection to socket, trying to access network resources beyond authority"
msgstr "socket_connect：非法连接socket，试图越权访问网络资源"

msgid "Setuid monitoring:Illegally set uid to attack the system"
msgstr "setuid监控：非法设置uid，试图攻击系统"

msgid "Uid monitoring:Illegally set uid to attack the system"
msgstr "uid监控：非法设置uid，试图攻击系统"

msgid "Sudo operation monitoring:Illegal sudo operation, attempting to attack the system"
msgstr "sudo操作监控：非法sudo操作，试图攻击系统"

msgid "Rebound shell monitoring:Illegal rebound shell, trying to attack the system"
msgstr "反弹shell监控：非法反弹shell，试图攻击系统"

msgid "Load drive monitoring:Illegally loading the driver and trying to attack the system"
msgstr "加载驱动监控：非法加载驱动，试图攻击系统"

msgid "Remote vulnerability attack"
msgstr "远程漏洞攻击"

msgid "Blocked"
msgstr "已拦截"

msgid "Not intercepted"
msgstr "未拦截"

msgid "AlertType"
msgstr "攻击类型"

msgid "AttackObject"
msgstr "主体进程"

msgid "AttackPath"
msgstr "主体路径"

msgid "AttackProxy"
msgstr "客体对象"

msgid "AttackCommand"
msgstr "命令行参数"

msgid "EventType"
msgstr "事件类型"

msgid "Action"
msgstr "处理结果"

msgid "AttackType"
msgstr "攻击类型"

msgid "Httc Log"
msgstr "指令流日志"

#clogtable.cpp
msgid "No log information"
msgstr "暂无日志信息"

msgid "TIME"
msgstr "时间段"

msgid "The process is normal and needs no attention"
msgstr "进程正常,无需关注"

msgid "The process is faulty and needs to be repaired"
msgstr "进程有故障,需要修复"

msgid "The process is abnormal and needs attention"
msgstr "进程有异常,需要关注"

msgid "This time, all startup items are loaded normally"
msgstr "当次启动，所有启动项全部正常加载"

msgid "Failed to start this time"
msgstr "当次启动失败"

msgid "The startup is successful, but the status of startup items is failed"
msgstr "当次启动成功，但有启动项状态为失败"

msgid "The account and password are verified and logged in successfully"
msgstr "账号密码验证成功,登录成功"

msgid "Abnormal login, account locking"
msgstr "异常登录,账户锁定"

msgid "Account password verification failed, login failed"
msgstr "账号密码验证失败,登录失败"

msgid "%1 Log Item"
msgstr "%1 条日志"

msgid "%1 Log Items"
msgstr "%1 条日志"

msgid "NORMAL"
msgstr "正常"

msgid "ABNORMAL"
msgstr "异常"

msgid "FAIL"
msgstr "失败"

msgid "SUCCESS"
msgstr "成功"

#privilege
msgid "System "
msgstr "系统 "

msgid "App Service"
msgstr "应用服务"

msgid "Event Type"
msgstr "事件类型"

msgid "Information Path"
msgstr "信息路径"
