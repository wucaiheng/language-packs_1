<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>ApplicationVolumeWidget</name>
    <message>
        <location filename="../ukmedia_application_volume_widget.cpp" line="28"/>
        <source>Application Volume</source>
        <translation>བཀོལ་སྤྱོད་བྱེད་ཚད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_application_volume_widget.cpp" line="30"/>
        <source>System Volume</source>
        <translation>མ་ལག་གི་སྒྲ་ཚད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_application_volume_widget.cpp" line="82"/>
        <source>Sound Settings</source>
        <translation>སྒྲའི་སྒྲིག་བཀོད།</translation>
    </message>
</context>
<context>
    <name>DeviceSwitchWidget</name>
    <message>
        <source>Go Into Mini Mode</source>
        <translation type="vanished">进入Mini模式</translation>
    </message>
    <message>
        <source>Output volume control</source>
        <translation type="vanished">输出音量控制</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation type="vanished">静音</translation>
    </message>
    <message>
        <source>Sound preference(S)</source>
        <translation type="vanished">声音首选项</translation>
    </message>
    <message>
        <source>Device Volume</source>
        <translation type="vanished">设备音量</translation>
    </message>
    <message>
        <source>Application Volume</source>
        <translation type="vanished">应用音量</translation>
    </message>
    <message>
        <source>is using</source>
        <translation type="vanished">正在使用</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation type="vanished">蓝牙</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>Unable to connect to the sound system, please check whether the pulseaudio service is running!</source>
        <translation type="vanished">无法连接到系统声音，请检查pulseaudio服务是否正在运行！</translation>
    </message>
    <message>
        <source>Dummy output</source>
        <translation type="vanished">伪输出</translation>
    </message>
    <message>
        <source>Speaker (Realtek Audio)</source>
        <translation type="vanished">扬声器(Realtek Audio)</translation>
    </message>
    <message>
        <source>Headphone</source>
        <translation type="vanished">模拟耳机</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1709"/>
        <source>pa_context_subscribe() failed</source>
        <translation>pa_context_subscribe()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1732"/>
        <source>pa_context_get_card_info_list() failed</source>
        <translation>pa_context_get_card_info_list()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1739"/>
        <source>pa_context_get_sink_info_list() failed</source>
        <translation>pa_context_get_sink_info_list()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1746"/>
        <source>pa_context_get_source_info_list() failed</source>
        <translation>pa_context_get_source_info_list()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1472"/>
        <source>Failed to initialize stream_restore extension: %s</source>
        <translation>ཐོག་མའི་རང་བཞིན་གྱི་stream_restore་ཕམ་ཁ་རྒྱ་བསྐྱེད་པ།%s</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1487"/>
        <source>pa_ext_stream_restore_read() failed</source>
        <translation>pa_ext_stream_restore_read()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1580"/>
        <source>pa_context_get_sink_info_by_index() failed</source>
        <translation>pa_context_get_sink_info_by_index()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1593"/>
        <source>pa_context_get_source_info_by_index() failed</source>
        <translation>pa_context_get_source_info_by_index()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1669"/>
        <source>pa_context_get_card_info_by_index() failed</source>
        <translation>pa_context_get_card_info_by_index()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1177"/>
        <source>Card callback failure</source>
        <translation>བྱང་བུ་ཕྱིར་བརྗེ་བ་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1205"/>
        <location filename="../ukmedia_volume_control.cpp" line="1325"/>
        <source>Sink callback failure</source>
        <translation>ཕྱིར་ལྡོག་བྱེད་པར་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1284"/>
        <location filename="../ukmedia_volume_control.cpp" line="1345"/>
        <source>Source callback failure</source>
        <translation>འབྱུང་ཁུངས་ཕྱིར་ལྡོག་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="86"/>
        <source>Fatal Error: Unable to connect to PulseAudio</source>
        <translation>ནོར་འཁྲུལ།pulseaudio་ལ་སྦྲེལ་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="200"/>
        <location filename="../ukmedia_volume_control.cpp" line="1642"/>
        <location filename="../ukmedia_volume_control.cpp" line="1718"/>
        <source>pa_context_get_server_info() failed</source>
        <translation>pa_context_get_server_info()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1366"/>
        <location filename="../ukmedia_volume_control.cpp" line="1862"/>
        <source>Sink input callback failure</source>
        <translation>ཆུ་གཞོང་ནང་དུ་བླུགས་ནས་ཕྱིར་སྙོམས་སྒྲིག་བྱས་པ་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1385"/>
        <source>Source output callback failure</source>
        <translation>source-ouputཕྱིར་ལྡོག་བྱེད་པར་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1404"/>
        <source>Client callback failure</source>
        <translation>མཁོ་མཁན་ཕྱོགས་ཀྱི་ལེགས་སྒྲིག་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1420"/>
        <location filename="../ukmedia_volume_control.cpp" line="1447"/>
        <source>Server info callback failure</source>
        <translation>ཞབས་ཞུའི་ཡོ་བྱད་ཀྱི་ཆ་འཕྲིན་ཕྱིར་ལྡོག་བྱེད་མ་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1505"/>
        <source>Failed to initialize device restore extension: %s</source>
        <translation>ཐོག་མའི་སྒྲིག་ཆས་ཆ་འཕྲིན་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1526"/>
        <source>pa_ext_device_restore_read_sink_formats() failed</source>
        <translation>pa_ext_device_restore_read_sink_formats()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1544"/>
        <source>Failed to initialize device manager extension: %s</source>
        <translation>སྒྲིག་ཆས་དོ་དམ་གྱི་དུས་འགྱངས་ལ་ཐོག་མའི་དུས་འགྱངས་བྱེད་མ་ཐུབ་པ། %s</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1563"/>
        <source>pa_ext_device_manager_read() failed</source>
        <translation>pa_ext_device_manager_read()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1606"/>
        <location filename="../ukmedia_volume_control.cpp" line="1619"/>
        <source>pa_context_get_sink_input_info() failed</source>
        <translation>pa_context_get_sink_input_info()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1632"/>
        <source>pa_context_get_client_info() failed</source>
        <translation>pa_context_get_client_info()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1725"/>
        <source>pa_context_client_info_list() failed</source>
        <translation>pa_context_client_info_list()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1752"/>
        <source>pa_context_get_sink_input_info_list() failed</source>
        <translation>pa_context_get_sink_input_info_list()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1759"/>
        <source>pa_context_get_source_output_info_list() failed</source>
        <translation>pa_context_get_source_output_info_list()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1774"/>
        <source>Connection failed, attempting reconnect</source>
        <translation>འབྲེལ་མཐུད་བྱེད་མ་ཐུབ་པར་ཡང་བསྐྱར་འབྲེལ་མཐུད་བྱེད་རྩིས་བྱས།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1793"/>
        <source>moduleInfoCb callback failure</source>
        <translation>moduleInfoCb ཕྱིར་ལྡོག་བྱེད་པར་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1810"/>
        <source>Ukui Media Volume Control</source>
        <translation>UKUI སྨྱན་སྦྱོར་གྱི་སྒྲ་གདངས་ཚོད་འཛིན།</translation>
    </message>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="3280"/>
        <source>pa_context_load_module() failed</source>
        <translation>pa_context_load_module()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>UkmediaDeviceWidget</name>
    <message>
        <source>Input Device</source>
        <translation type="vanished">输入设备</translation>
    </message>
    <message>
        <source>Microphone</source>
        <translation type="vanished">麦克风</translation>
    </message>
    <message>
        <source>Output Device</source>
        <translation type="vanished">输出设备</translation>
    </message>
    <message>
        <source>Speaker Realtek Audio</source>
        <translation type="vanished">扬声器(Realtek Audio)</translation>
    </message>
    <message>
        <source>Input device can not be detected</source>
        <translation type="vanished">无法检测到输入设备</translation>
    </message>
</context>
<context>
    <name>UkmediaMainWidget</name>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="107"/>
        <source>Output volume control</source>
        <translation>ཕྱིར་གཏོང་སྒྲ་གདངས་ཚོད་འཛིན།</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation type="vanished">静音</translation>
    </message>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="109"/>
        <location filename="../ukmedia_main_widget.cpp" line="111"/>
        <source>Sound preference(S)</source>
        <translation>སྒྲ་ཚན་བཀོད་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="172"/>
        <source>System Volume</source>
        <translation>མ་ལག་གི་སྒྲ་ཚད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="174"/>
        <source>App Volume</source>
        <translation>ཉེར་སྤྱོད་སྒྲ་ཚད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="1078"/>
        <source>Current volume:</source>
        <translation>མིག་སྔའི་སྒྲ་ཚད་ནི།</translation>
    </message>
</context>
<context>
    <name>UkmediaMiniMasterVolumeWidget</name>
    <message>
        <source>Speaker (Realtek Audio)</source>
        <translation type="vanished">扬声器(Realtek Audio)</translation>
    </message>
    <message>
        <source>Go Into Full Mode</source>
        <translation type="vanished">进入完整模式</translation>
    </message>
</context>
<context>
    <name>UkmediaSystemVolumeWidget</name>
    <message>
        <location filename="../ukmedia_system_volume_widget.cpp" line="58"/>
        <source>Volume</source>
        <translation>སྒྲ་ཚད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_system_volume_widget.cpp" line="69"/>
        <source>Output</source>
        <translation>ཕྱིར་འདོན།</translation>
    </message>
    <message>
        <location filename="../ukmedia_system_volume_widget.cpp" line="85"/>
        <source>Sound Settings</source>
        <translation>སྒྲའི་སྒྲིག་བཀོད།</translation>
    </message>
</context>
<context>
    <name>UkmediaVolumeControl</name>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="97"/>
        <location filename="../ukmedia_volume_control.cpp" line="121"/>
        <location filename="../ukmedia_volume_control.cpp" line="125"/>
        <location filename="../ukmedia_volume_control.cpp" line="138"/>
        <location filename="../ukmedia_volume_control.cpp" line="160"/>
        <location filename="../ukmedia_volume_control.cpp" line="252"/>
        <location filename="../ukmedia_volume_control.cpp" line="256"/>
        <location filename="../ukmedia_volume_control.cpp" line="269"/>
        <source>pa_context_set_sink_volume_by_index() failed</source>
        <translation>pa_context_set_sink_volume_by_index()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="287"/>
        <location filename="../ukmedia_volume_control.cpp" line="291"/>
        <source>pa_context_set_source_output_volume() failed</source>
        <translation>pa_context_set_source_output_volume()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="304"/>
        <source>pa_context_set_source_output_mute() failed</source>
        <translation>pa_context_set_source_output_mute()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="317"/>
        <source>pa_context_set_card_profile_by_index() failed</source>
        <translation>pa_context_set_card_profile_by_index()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="333"/>
        <source>pa_context_set_default_sink() failed</source>
        <translation>pa_context_set_default_sink()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="349"/>
        <source>pa_context_set_default_source() failed</source>
        <translation>pa_context_set_default_source()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="365"/>
        <source>pa_context_set_sink_port_by_name() failed</source>
        <translation>pa_context_set_sink_port_by_name()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="380"/>
        <source>pa_context_set_source_port_by_name() failed</source>
        <translation>pa_context_set_source_port_by_name()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="393"/>
        <source>pa_context_kill_sink_input() failed</source>
        <translation>pa_context_kill_sink_input()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="422"/>
        <source> (plugged in)</source>
        <translation> (ནང་དུ་བཅུག་པ)།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="426"/>
        <location filename="../ukmedia_volume_control.cpp" line="561"/>
        <source> (unavailable)</source>
        <translation> (སྤྱོད་གོ་མི་ཆོད་པ)།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="428"/>
        <location filename="../ukmedia_volume_control.cpp" line="558"/>
        <source> (unplugged)</source>
        <translation> (སྦྲེལ་མེད་པ)།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="734"/>
        <source>Failed to read data from stream</source>
        <translation>བཞུར་རྒྱུན་ཁྲོད་ནས་གཞི་གྲངས་ཀློག་འདོན་བྱས་པ་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="776"/>
        <source>Peak detect</source>
        <translation>ཡང་རྩེར་ཞིབ་དཔྱད་ཚད་ལེན།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="777"/>
        <source>Failed to create monitoring stream</source>
        <translation>ལྟ་ཞིབ་ཚད་ལེན་གྱི་ཆུ་ཕྲན་གསར་སྐྲུན་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="792"/>
        <source>Failed to connect monitoring stream</source>
        <translation>ལྟ་ཞིབ་ཚད་ལེན་གྱི་ཆུ་ཕྲན་སྦྲེལ་མཐུད་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="909"/>
        <source>Ignoring sink-input due to it being designated as an event and thus handled by the Event widget</source>
        <translation>དོན་རྐྱེན་ཞིག་ཏུ་བརྩིས་ནས་དོན་རྐྱེན་ཆུང་ཆུང་ཞིག་ཏུ་བརྩིས་ནས་ཐག་གཅོད་བྱས་པའི་རྐྱེན་གྱིས་ཆུ་ནང་དུ་འཛུལ་བར་སྣང་མེད་བྱས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1148"/>
        <source>Establishing connection to PulseAudio. Please wait...</source>
        <translation>PulseAudioདང་འབྲེལ་མཐུད་བྱེད་བཞིན་ཡོད།སྒུག་རོགས།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1429"/>
        <source>pa_context_get_sink_info_by_name() failed</source>
        <translation>pa_context_get_sink_info_by_index()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1433"/>
        <source>pa_context_get_source_info_by_name() failed</source>
        <translation>pa_context_get_source_info_by_index()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
</context>
<context>
    <name>UkuiMediaSetHeadsetWidget</name>
    <message>
        <location filename="../ukui_media_set_headset_widget.cpp" line="36"/>
        <source>Sound Settings</source>
        <translation>སྒྲའི་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../ukui_media_set_headset_widget.cpp" line="37"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ukui_media_set_headset_widget.cpp" line="38"/>
        <source>Select Sound Device</source>
        <translation>སྒྲའི་སྒྲིག་ཆས་བདམས་པ།</translation>
    </message>
    <message>
        <location filename="../ukui_media_set_headset_widget.cpp" line="66"/>
        <source>Headphone</source>
        <translation>རྣ་ཉན།</translation>
    </message>
    <message>
        <location filename="../ukui_media_set_headset_widget.cpp" line="67"/>
        <source>Headset</source>
        <translation>རྣ་ཉན།</translation>
    </message>
    <message>
        <location filename="../ukui_media_set_headset_widget.cpp" line="68"/>
        <source>Microphone</source>
        <translation>སྐད་སྦུག</translation>
    </message>
</context>
</TS>
