<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Audio</name>
    <message>
        <location filename="../audio.ui" line="26"/>
        <location filename="../audio.cpp" line="38"/>
        <source>Audio</source>
        <translation>སྒྲ་ཕབ།</translation>
    </message>
    <message>
        <location filename="../audio.cpp" line="93"/>
        <source>UkccPlugin</source>
        <translation>བསྒར་ལྷུ།</translation>
        <extra-contents_path>/UkccPlugin/UkccPlugin</extra-contents_path>
    </message>
    <message>
        <location filename="../audio.cpp" line="95"/>
        <source>ukccplugin test</source>
        <translation>ukccབསྒར་ལྷུ་ཚོད་ལྟ།</translation>
        <extra-contents_path>/UkccPlugin/ukccplugin test</extra-contents_path>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="217"/>
        <location filename="../ukmedia_volume_control.cpp" line="1640"/>
        <location filename="../ukmedia_volume_control.cpp" line="1719"/>
        <source>pa_context_get_server_info() failed</source>
        <translation>pa_context_get_server_info()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1228"/>
        <source>Card callback failure</source>
        <translation>བྱང་བུ་ཕྱིར་བརྗེ་བ་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1246"/>
        <location filename="../ukmedia_volume_control.cpp" line="1333"/>
        <source>Sink callback failure</source>
        <translation>ཕྱིར་ལྡོག་བྱེད་པར་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1282"/>
        <location filename="../ukmedia_volume_control.cpp" line="1354"/>
        <source>Source callback failure</source>
        <translation>འབྱུང་ཁུངས་ཕྱིར་ལྡོག་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1376"/>
        <location filename="../ukmedia_volume_control.cpp" line="1873"/>
        <source>Sink input callback failure</source>
        <translation>ཆུ་གཞོང་ནང་དུ་བླུགས་ནས་ཕྱིར་སྙོམས་སྒྲིག་བྱས་པ་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1395"/>
        <source>Source output callback failure</source>
        <translation>source-ouputཕྱིར་ལྡོག་བྱེད་པར་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1443"/>
        <source>Client callback failure</source>
        <translation>མཁོ་མཁན་ཕྱོགས་ཀྱི་ལེགས་སྒྲིག་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1459"/>
        <location filename="../ukmedia_volume_control.cpp" line="1473"/>
        <source>Server info callback failure</source>
        <translation>ཞབས་ཞུའི་ཡོ་བྱད་ཀྱི་ཆ་འཕྲིན་ཕྱིར་ལྡོག་བྱེད་མ་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1508"/>
        <source>Failed to initialize stream_restore extension: %s</source>
        <translation>ཐོག་མའི་རང་བཞིན་གྱི་stream_restore་ཕམ་ཁ་རྒྱ་བསྐྱེད་པ།%s</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1525"/>
        <source>pa_ext_stream_restore_read() failed</source>
        <translation>pa_ext_stream_restore_read()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1542"/>
        <source>Failed to initialize device manager extension: %s</source>
        <translation>སྒྲིག་ཆས་དོ་དམ་གྱི་དུས་འགྱངས་ལ་ཐོག་མའི་དུས་འགྱངས་བྱེད་མ་ཐུབ་པ། %s</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1561"/>
        <source>pa_ext_device_manager_read() failed</source>
        <translation>pa_ext_device_manager_read()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1578"/>
        <source>pa_context_get_sink_info_by_index() failed</source>
        <translation>pa_context_get_sink_info_by_index()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1591"/>
        <source>pa_context_get_source_info_by_index() failed</source>
        <translation>pa_context_get_source_info_by_index()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1604"/>
        <location filename="../ukmedia_volume_control.cpp" line="1617"/>
        <source>pa_context_get_sink_input_info() failed</source>
        <translation>pa_context_get_sink_input_info()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1630"/>
        <source>pa_context_get_client_info() failed</source>
        <translation>pa_context_get_client_info()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1670"/>
        <source>pa_context_get_card_info_by_index() failed</source>
        <translation>pa_context_get_card_info_by_index()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1710"/>
        <source>pa_context_subscribe() failed</source>
        <translation>pa_context_subscribe()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1726"/>
        <source>pa_context_client_info_list() failed</source>
        <translation>pa_context_client_info_list()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1733"/>
        <source>pa_context_get_card_info_list() failed</source>
        <translation>pa_context_get_card_info_list()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1740"/>
        <source>pa_context_get_sink_info_list() failed</source>
        <translation>pa_context_get_sink_info_list()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1747"/>
        <source>pa_context_get_source_info_list() failed</source>
        <translation>pa_context_get_source_info_list()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1754"/>
        <source>pa_context_get_sink_input_info_list() failed</source>
        <translation>pa_context_get_sink_input_info_list()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1761"/>
        <source>pa_context_get_source_output_info_list() failed</source>
        <translation>pa_context_get_source_output_info_list()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1777"/>
        <source>Connection failed, attempting reconnect</source>
        <translation>འབྲེལ་མཐུད་བྱེད་མ་ཐུབ་པར་ཡང་བསྐྱར་འབྲེལ་མཐུད་བྱེད་རྩིས་བྱས།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1797"/>
        <source>Ukui Media Volume Control</source>
        <translation>UKUI སྨྱན་སྦྱོར་གྱི་སྒྲ་གདངས་ཚོད་འཛིན།</translation>
    </message>
</context>
<context>
    <name>UkmediaInputWidget</name>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="53"/>
        <source>Input</source>
        <translation>ནང་འཇུག</translation>
        <extra-contents_path>/Audio/Input</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="58"/>
        <source>Input Device</source>
        <translation>ནང་འཇུག་སྒྲིག་ཆས་བདམས་པ།</translation>
        <extra-contents_path>/Audio/Input Device</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="61"/>
        <source>Volume</source>
        <translation>སྒྲ་ཚད།</translation>
        <extra-contents_path>/Audio/Volume</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="66"/>
        <source>Input Level</source>
        <translation>ནང་འཇུག་ལྡོག་ལན།</translation>
        <extra-contents_path>/Audio/Input Level</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="77"/>
        <source>Noise Reduction</source>
        <translation>རིག་ནུས་འཛེར་སྒྲ་ཇེ་ཆུང་དུ་བཏང་།</translation>
        <extra-contents_path>/audio/Noise</extra-contents_path>
    </message>
</context>
<context>
    <name>UkmediaMainWidget</name>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="2454"/>
        <location filename="../ukmedia_main_widget.cpp" line="2461"/>
        <location filename="../ukmedia_main_widget.cpp" line="2548"/>
        <location filename="../ukmedia_main_widget.cpp" line="2646"/>
        <location filename="../ukmedia_main_widget.cpp" line="2737"/>
        <location filename="../ukmedia_main_widget.cpp" line="2743"/>
        <location filename="../ukmedia_main_widget.cpp" line="2748"/>
        <location filename="../ukmedia_main_widget.cpp" line="2795"/>
        <location filename="../ukmedia_main_widget.cpp" line="2829"/>
        <location filename="../ukmedia_main_widget.cpp" line="3045"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
</context>
<context>
    <name>UkmediaOutputWidget</name>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="72"/>
        <source>Output</source>
        <translation>ཕྱིར་གཏོང་།</translation>
        <extra-contents_path>/Audio/Output</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="76"/>
        <source>Output Device</source>
        <translation>ཕྱིར་གཏོང་སྒྲིག་ཆས་བདམས་པ།</translation>
        <extra-contents_path>/Audio/Output Device</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="79"/>
        <source>Master Volume</source>
        <translation>སྒྲ་ཚད།</translation>
        <extra-contents_path>/Audio/Master Volume</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="84"/>
        <source>Balance</source>
        <translation>སྒྲ་ལམ་དོ་མཉམ།</translation>
        <extra-contents_path>/Audio/Balance</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="85"/>
        <source>Left</source>
        <translation>གཡོན་ཕྱོགས་པ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="87"/>
        <source>Right</source>
        <translation>གཡས་ཕྱོགས་པ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="90"/>
        <source>Volume Increase</source>
        <translation>གྲངས་འབོར་འཕར་སྣོན་བྱུང་བ།</translation>
        <extra-contents_path>/audio/Noise</extra-contents_path>
    </message>
</context>
<context>
    <name>UkmediaSoundEffectsWidget</name>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="58"/>
        <source>System Sound</source>
        <translation>མ་ལག་འཐུས་སྒོ་ཚང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="61"/>
        <source>Sound Theme</source>
        <translation>སྒྲའི་བརྗོད་བྱ་གཙོ་བོ།</translation>
        <extra-contents_path>/Audio/Sound Theme</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="64"/>
        <source>Alert Sound</source>
        <translation>ཉེན་བརྡ།</translation>
        <extra-contents_path>/Audio/Alert Sound</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="67"/>
        <source>Beep Switch</source>
        <translation>གསལ་སྟོན་སྒྲ།</translation>
        <extra-contents_path>/Audio/Beep Switch</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="69"/>
        <source>Poweroff Music</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
        <extra-contents_path>/Audio/Poweroff Music</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="71"/>
        <source>Startup Music</source>
        <translation>འཁོར་ཁ་འབྱེད་པ།</translation>
        <extra-contents_path>/Audio/Startup Music</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="73"/>
        <source>Wakeup Music</source>
        <translation>གཉིད་ལས་སད་པའི་རོལ་མོ།</translation>
        <extra-contents_path>/Audio/Wakeup Music</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="75"/>
        <source>Volume Change</source>
        <translation>སྒྲ་གདངས་འགྱུར་ལྡོག</translation>
        <extra-contents_path>/Audio/Volume Change</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="77"/>
        <source>Logout Music</source>
        <translation>དོར་བ།</translation>
        <extra-contents_path>/Audio/Logout Music</extra-contents_path>
    </message>
</context>
<context>
    <name>UkmediaVolumeControl</name>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="67"/>
        <location filename="../ukmedia_volume_control.cpp" line="92"/>
        <location filename="../ukmedia_volume_control.cpp" line="96"/>
        <location filename="../ukmedia_volume_control.cpp" line="111"/>
        <location filename="../ukmedia_volume_control.cpp" line="164"/>
        <location filename="../ukmedia_volume_control.cpp" line="239"/>
        <location filename="../ukmedia_volume_control.cpp" line="243"/>
        <location filename="../ukmedia_volume_control.cpp" line="255"/>
        <source>pa_context_set_sink_volume_by_index() failed</source>
        <translation>pa_context_set_sink_volume_by_index()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="133"/>
        <source>pa_context_set_source_mute_by_index() failed</source>
        <translation>pa_context_set_source_mute_by_index()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="138"/>
        <source>pa_context_set_source_volume_by_index() failed</source>
        <translation>pa_context_set_source_volume_by_index()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="272"/>
        <location filename="../ukmedia_volume_control.cpp" line="276"/>
        <source>pa_context_set_source_output_volume() failed</source>
        <translation>pa_context_set_source_output_volume()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="288"/>
        <source>pa_context_set_source_output_mute() failed</source>
        <translation>pa_context_set_source_output_mute()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="300"/>
        <source>pa_context_set_card_profile_by_index() failed</source>
        <translation>pa_context_set_card_profile_by_index()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="314"/>
        <source>pa_context_set_default_sink() failed</source>
        <translation>pa_context_set_default_sink()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="327"/>
        <location filename="../ukmedia_volume_control.cpp" line="337"/>
        <location filename="../ukmedia_volume_control.cpp" line="805"/>
        <location filename="../ukmedia_volume_control.cpp" line="923"/>
        <location filename="../ukmedia_volume_control.cpp" line="1313"/>
        <source>pa_context_set_default_source() failed</source>
        <translation>pa_context_set_default_source()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="356"/>
        <source>pa_context_set_sink_port_by_name() failed</source>
        <translation>pa_context_set_sink_port_by_name()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="370"/>
        <source>pa_context_set_source_port_by_name() failed</source>
        <translation>pa_context_set_source_port_by_name()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="399"/>
        <source> (plugged in)</source>
        <translation> (ནང་དུ་བཅུག་པ)།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="403"/>
        <location filename="../ukmedia_volume_control.cpp" line="550"/>
        <source> (unavailable)</source>
        <translation> (སྤྱོད་གོ་མི་ཆོད་པ)།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="405"/>
        <location filename="../ukmedia_volume_control.cpp" line="547"/>
        <source> (unplugged)</source>
        <translation> (སྦྲེལ་མེད་པ)།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="696"/>
        <source>Failed to read data from stream</source>
        <translation>བཞུར་རྒྱུན་ཁྲོད་ནས་གཞི་གྲངས་ཀློག་འདོན་བྱས་པ་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="746"/>
        <source>Peak detect</source>
        <translation>ཡང་རྩེར་ཞིབ་དཔྱད་ཚད་ལེན།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="747"/>
        <source>Failed to create monitoring stream</source>
        <translation>ལྟ་ཞིབ་ཚད་ལེན་གྱི་ཆུ་ཕྲན་གསར་སྐྲུན་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="762"/>
        <source>Failed to connect monitoring stream</source>
        <translation>ལྟ་ཞིབ་ཚད་ལེན་གྱི་ཆུ་ཕྲན་སྦྲེལ་མཐུད་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="881"/>
        <source>Ignoring sink-input due to it being designated as an event and thus handled by the Event widget</source>
        <translation>དོན་རྐྱེན་ཞིག་ཏུ་བརྩིས་ནས་དོན་རྐྱེན་ཆུང་ཆུང་ཞིག་ཏུ་བརྩིས་ནས་ཐག་གཅོད་བྱས་པའི་རྐྱེན་གྱིས་ཆུ་ནང་དུ་འཛུལ་བར་སྣང་མེད་བྱས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1194"/>
        <source>Establishing connection to PulseAudio. Please wait...</source>
        <translation>PulseAudioདང་འབྲེལ་མཐུད་བྱེད་བཞིན་ཡོད།སྒུག་རོགས།</translation>
    </message>
</context>
</TS>
