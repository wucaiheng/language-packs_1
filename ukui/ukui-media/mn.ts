<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Audio</name>
    <message>
        <location filename="../audio.ui" line="26"/>
        <location filename="../audio.cpp" line="38"/>
        <source>Audio</source>
        <translation>ᠠᠦ᠋ᠳᠢᠤ᠋</translation>
    </message>
    <message>
        <location filename="../audio.cpp" line="93"/>
        <source>UkccPlugin</source>
        <translation>Ukcc ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ</translation>
        <extra-contents_path>/UkccPlugin/UkccPlugin</extra-contents_path>
    </message>
    <message>
        <location filename="../audio.cpp" line="95"/>
        <source>ukccplugin test</source>
        <translation>Ukcc ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ ᠳᠤᠷᠰᠢᠬᠤ</translation>
        <extra-contents_path>/UkccPlugin/ukccplugin test</extra-contents_path>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="217"/>
        <location filename="../ukmedia_volume_control.cpp" line="1640"/>
        <location filename="../ukmedia_volume_control.cpp" line="1719"/>
        <source>pa_context_get_server_info() failed</source>
        <translation>pa_context_get_server_info() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1228"/>
        <source>Card callback failure</source>
        <translation>ᠺᠠᠷᠲ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1246"/>
        <location filename="../ukmedia_volume_control.cpp" line="1333"/>
        <source>Sink callback failure</source>
        <translation>ᠡᠬᠡᠬᠦᠯᠬᠦ ᠵᠢᠨ ᠭᠠᠷᠭᠠᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1282"/>
        <location filename="../ukmedia_volume_control.cpp" line="1354"/>
        <source>Source callback failure</source>
        <translation>ᠡᠬᠡᠬᠦᠯᠬᠦ ᠵᠢᠨ ᠤᠷᠤᠭᠤᠯᠤᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1376"/>
        <location filename="../ukmedia_volume_control.cpp" line="1873"/>
        <source>Sink input callback failure</source>
        <translation>ᠤᠰᠤᠨ ᠬᠤᠪᠢᠯ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠬᠦ ᠤᠷᠤᠭᠤᠯᠤᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1395"/>
        <source>Source output callback failure</source>
        <translation>source-ouput ᠡᠬᠡᠬᠦᠯᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1443"/>
        <source>Client callback failure</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠵᠦᠬᠦᠷᠯᠢᠭ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1459"/>
        <location filename="../ukmedia_volume_control.cpp" line="1473"/>
        <source>Server info callback failure</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠬᠦ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1508"/>
        <source>Failed to initialize stream_restore extension: %s</source>
        <translation>stream_restore ᠵᠢ/ ᠢ᠋ ᠠᠨᠭᠬᠠᠵᠢᠭᠤᠯᠵᠤ ᠦᠷᠬᠡᠳᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ᠄%s</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1525"/>
        <source>pa_ext_stream_restore_read() failed</source>
        <translation>pa_ext_stream_restore_read() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1542"/>
        <source>Failed to initialize device manager extension: %s</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠳᠠ ᠵᠢᠨ ᠪᠠᠭᠠᠵᠢ ᠵᠢ ᠠᠨᠭᠬᠠᠵᠢᠭᠤᠯᠵᠤ ᠦᠷᠬᠡᠳᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ᠄%s</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1561"/>
        <source>pa_ext_device_manager_read() failed</source>
        <translation>pa_ext_device_manager_read() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1578"/>
        <source>pa_context_get_sink_info_by_index() failed</source>
        <translation>pa_context_get_sink_info_by_index() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1591"/>
        <source>pa_context_get_source_info_by_index() failed</source>
        <translation>pa_context_get_source_info_by_index() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1604"/>
        <location filename="../ukmedia_volume_control.cpp" line="1617"/>
        <source>pa_context_get_sink_input_info() failed</source>
        <translation>pa_context_get_sink_input_info() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1630"/>
        <source>pa_context_get_client_info() failed</source>
        <translation>pa_context_get_client_info() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1670"/>
        <source>pa_context_get_card_info_by_index() failed</source>
        <translation>pa_context_get_card_info_by_index() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1710"/>
        <source>pa_context_subscribe() failed</source>
        <translation>pa_context_subscribe() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1726"/>
        <source>pa_context_client_info_list() failed</source>
        <translation>pa_context_client_info_list() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1733"/>
        <source>pa_context_get_card_info_list() failed</source>
        <translation>pa_context_get_card_info_list() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1740"/>
        <source>pa_context_get_sink_info_list() failed</source>
        <translation>pa_context_get_sink_info_list() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1747"/>
        <source>pa_context_get_source_info_list() failed</source>
        <translation>pa_context_get_source_info_list() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1754"/>
        <source>pa_context_get_sink_input_info_list() failed</source>
        <translation>pa_context_get_sink_input_info_list() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1761"/>
        <source>pa_context_get_source_output_info_list() failed</source>
        <translation>pa_context_get_source_output_info_list() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1777"/>
        <source>Connection failed, attempting reconnect</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ ᠵᠢ ᠳᠤᠷᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1797"/>
        <source>Ukui Media Volume Control</source>
        <translation>Ukui ᠵᠠᠭᠤᠴᠢᠯᠠᠭᠤᠷ ᠤ᠋ᠨ ᠳᠠᠭᠤᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠵᠢ ᠡᠵᠡᠮᠳᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>UkmediaInputWidget</name>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="53"/>
        <source>Input</source>
        <translation>ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
        <extra-contents_path>/Audio/Input</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="58"/>
        <source>Input Device</source>
        <translation>ᠤᠷᠤᠭᠤᠯᠬᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠰᠤᠨᠭᠭᠤᠬᠤ</translation>
        <extra-contents_path>/Audio/Input Device</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="61"/>
        <source>Volume</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
        <extra-contents_path>/Audio/Volume</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="66"/>
        <source>Input Level</source>
        <translation>ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯᠳᠠ</translation>
        <extra-contents_path>/Audio/Input Level</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="77"/>
        <source>Noise Reduction</source>
        <translation>ᠤᠶᠤᠯᠢᠭ ᠵᠢᠨᠷ ᠱᠤᠤᠬᠢᠶᠠᠨ ᠢ᠋ ᠪᠠᠭᠠᠰᠬᠠᠬᠤ</translation>
        <extra-contents_path>/audio/Noise</extra-contents_path>
    </message>
</context>
<context>
    <name>UkmediaMainWidget</name>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="2454"/>
        <location filename="../ukmedia_main_widget.cpp" line="2461"/>
        <location filename="../ukmedia_main_widget.cpp" line="2548"/>
        <location filename="../ukmedia_main_widget.cpp" line="2646"/>
        <location filename="../ukmedia_main_widget.cpp" line="2737"/>
        <location filename="../ukmedia_main_widget.cpp" line="2743"/>
        <location filename="../ukmedia_main_widget.cpp" line="2748"/>
        <location filename="../ukmedia_main_widget.cpp" line="2795"/>
        <location filename="../ukmedia_main_widget.cpp" line="2829"/>
        <location filename="../ukmedia_main_widget.cpp" line="3045"/>
        <source>None</source>
        <translation>ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
</context>
<context>
    <name>UkmediaOutputWidget</name>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="72"/>
        <source>Output</source>
        <translation>ᠭᠠᠷᠭᠠᠬᠤ</translation>
        <extra-contents_path>/Audio/Output</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="76"/>
        <source>Output Device</source>
        <translation>ᠭᠠᠷᠭᠠᠬᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠰᠤᠨᠭᠭᠤᠬᠤ</translation>
        <extra-contents_path>/Audio/Output Device</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="79"/>
        <source>Master Volume</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
        <extra-contents_path>/Audio/Master Volume</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="84"/>
        <source>Balance</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ᠋ ᠵᠠᠮ ᠢ᠋ ᠳᠡᠨᠭᠴᠡᠬᠦᠯᠬᠦ</translation>
        <extra-contents_path>/Audio/Balance</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="85"/>
        <source>Left</source>
        <translation>ᠵᠡᠬᠦᠨ</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="87"/>
        <source>Right</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="90"/>
        <source>Volume Increase</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠵᠢ ᠨᠡᠮᠡᠭᠳᠡᠬᠦᠯᠬᠦ</translation>
        <extra-contents_path>/audio/Noise</extra-contents_path>
    </message>
</context>
<context>
    <name>UkmediaSoundEffectsWidget</name>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="58"/>
        <source>System Sound</source>
        <translation>ᠰᠢᠰᠲᠸᠮ ᠤ᠋ᠨ ᠳᠠᠭᠤᠨ ᠤ᠋ ᠴᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="61"/>
        <source>Sound Theme</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ᠋ ᠴᠢᠨᠠᠷ ᠤ᠋ᠨ ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ</translation>
        <extra-contents_path>/Audio/Sound Theme</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="64"/>
        <source>Alert Sound</source>
        <translation>ᠮᠡᠳᠡᠭᠳᠡᠯ</translation>
        <extra-contents_path>/Audio/Alert Sound</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="67"/>
        <source>Beep Switch</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ</translation>
        <extra-contents_path>/Audio/Beep Switch</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="69"/>
        <source>Poweroff Music</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
        <extra-contents_path>/Audio/Poweroff Music</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="71"/>
        <source>Startup Music</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
        <extra-contents_path>/Audio/Startup Music</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="73"/>
        <source>Wakeup Music</source>
        <translation>ᠳᠠᠭᠤᠳᠠᠨ ᠰᠡᠷᠡᠬᠡᠬᠦ</translation>
        <extra-contents_path>/Audio/Wakeup Music</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="75"/>
        <source>Volume Change</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠵᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
        <extra-contents_path>/Audio/Volume Change</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="77"/>
        <source>Logout Music</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ</translation>
        <extra-contents_path>/Audio/Logout Music</extra-contents_path>
    </message>
</context>
<context>
    <name>UkmediaVolumeControl</name>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="67"/>
        <location filename="../ukmedia_volume_control.cpp" line="92"/>
        <location filename="../ukmedia_volume_control.cpp" line="96"/>
        <location filename="../ukmedia_volume_control.cpp" line="111"/>
        <location filename="../ukmedia_volume_control.cpp" line="164"/>
        <location filename="../ukmedia_volume_control.cpp" line="239"/>
        <location filename="../ukmedia_volume_control.cpp" line="243"/>
        <location filename="../ukmedia_volume_control.cpp" line="255"/>
        <source>pa_context_set_sink_volume_by_index() failed</source>
        <translation>pa_context_set_sink_volume_by_index() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="133"/>
        <source>pa_context_set_source_mute_by_index() failed</source>
        <translation>pa_context_set_source_mute_by_index() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="138"/>
        <source>pa_context_set_source_volume_by_index() failed</source>
        <translation>pa_context_set_source_volume_by_index() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="272"/>
        <location filename="../ukmedia_volume_control.cpp" line="276"/>
        <source>pa_context_set_source_output_volume() failed</source>
        <translation>pa_context_set_source_output_volume() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="288"/>
        <source>pa_context_set_source_output_mute() failed</source>
        <translation>pa_context_set_source_output_mute() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="300"/>
        <source>pa_context_set_card_profile_by_index() failed</source>
        <translation>pa_context_set_card_profile_by_index() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="314"/>
        <source>pa_context_set_default_sink() failed</source>
        <translation>pa_context_set_default_sink() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="327"/>
        <location filename="../ukmedia_volume_control.cpp" line="337"/>
        <location filename="../ukmedia_volume_control.cpp" line="805"/>
        <location filename="../ukmedia_volume_control.cpp" line="923"/>
        <location filename="../ukmedia_volume_control.cpp" line="1313"/>
        <source>pa_context_set_default_source() failed</source>
        <translation>pa_context_set_default_source() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="356"/>
        <source>pa_context_set_sink_port_by_name() failed</source>
        <translation>pa_context_set_sink_port_by_name() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="370"/>
        <source>pa_context_set_source_port_by_name() failed</source>
        <translation>pa_context_set_source_port_by_name() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="399"/>
        <source> (plugged in)</source>
        <translation> ( ᠬᠠᠪᠴᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠪᠠᠢᠨ᠎ᠠ)</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="403"/>
        <location filename="../ukmedia_volume_control.cpp" line="550"/>
        <source> (unavailable)</source>
        <translation> ( ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ)</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="405"/>
        <location filename="../ukmedia_volume_control.cpp" line="547"/>
        <source> (unplugged)</source>
        <translation> ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="696"/>
        <source>Failed to read data from stream</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠤᠨᠭᠰᠢᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="746"/>
        <source>Peak detect</source>
        <translation>ᠣᠷᠭᠢᠯ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠦ᠋ᠨ ᠪᠠᠢᠴᠠᠭᠠᠯᠲᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="747"/>
        <source>Failed to create monitoring stream</source>
        <translation>ᠬᠢᠨᠠᠨ ᠬᠡᠮᠵᠢᠬᠦ ᠤᠷᠤᠰᠬᠠᠯ ᠢ᠋ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="762"/>
        <source>Failed to connect monitoring stream</source>
        <translation>ᠬᠢᠨᠠᠨ ᠬᠡᠮᠵᠢᠬᠦ ᠤᠷᠤᠰᠬᠠᠯ ᠢ᠋ ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="881"/>
        <source>Ignoring sink-input due to it being designated as an event and thus handled by the Event widget</source>
        <translation>ᠰᠠᠭᠤᠭᠳᠠᠭᠤᠯᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠵᠢ ᠤᠮᠳᠤᠭᠠᠢᠯᠠᠨ᠎ᠠ᠂ ᠤᠴᠢᠷ ᠨᠢ ᠳᠡᠬᠦᠨ ᠢ᠋ ᠶᠠᠪᠤᠳᠠᠯ ᠵᠢᠨᠷ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ᠂ ᠡᠢᠮᠤ ᠡᠴᠡ ᠶᠠᠪᠤᠳᠠᠯ ᠤ᠋ᠨ ᠵᠢᠵᠢᠭ ᠰᠡᠯᠪᠢᠭ ᠵᠢᠡᠷ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠬᠡᠷᠡᠭᠳᠡᠢ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1194"/>
        <source>Establishing connection to PulseAudio. Please wait...</source>
        <translation>PulseAudio ᠳ᠋ᠤ᠌/ ᠲᠤ᠌ ᠴᠦᠷᠬᠡᠯᠡᠵᠦ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠳᠦᠷ ᠬᠦᠯᠢᠶᠡᠬᠡᠷᠡᠢ...</translation>
    </message>
</context>
</TS>
