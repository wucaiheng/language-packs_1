<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AppUpdateWid</name>
    <message>
        <location filename="../src/appupdate.cpp" line="335"/>
        <source>Cancel failed,Being installed</source>
        <translatorcomment>取消失败，安装中</translatorcomment>
        <translation>ཕམ་ཉེས་བྱུང་ནས་སྒྲིག་སྦྱོར་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="338"/>
        <source>Being installed</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་བཞིན་པ།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="392"/>
        <source>Download succeeded!</source>
        <translation>ཕབ་ལེན་ལེགས་འགྲུབ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="368"/>
        <location filename="../src/appupdate.cpp" line="370"/>
        <location filename="../src/appupdate.cpp" line="371"/>
        <location filename="../src/appupdate.cpp" line="474"/>
        <location filename="../src/appupdate.cpp" line="476"/>
        <location filename="../src/appupdate.cpp" line="477"/>
        <source>Update succeeded , It is recommended that you restart later!</source>
        <translation>གསར་སྒྱུར་ལེགས་འགྲུབ་བྱུང་སོང་། ཁྱེད་ཀྱིས་རྗེས་སུ་ཡང་བསྐྱར་འགོ་འཛུགས་རྒྱུའི་གྲོས་འགོ་འདོན་རྒྱུ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="193"/>
        <location filename="../src/appupdate.cpp" line="194"/>
        <location filename="../src/appupdate.cpp" line="199"/>
        <location filename="../src/appupdate.cpp" line="215"/>
        <source>Version:</source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="375"/>
        <location filename="../src/appupdate.cpp" line="377"/>
        <location filename="../src/appupdate.cpp" line="378"/>
        <location filename="../src/appupdate.cpp" line="481"/>
        <location filename="../src/appupdate.cpp" line="483"/>
        <location filename="../src/appupdate.cpp" line="484"/>
        <source>Update succeeded , It is recommended that you log out later and log in again!</source>
        <translation>གསར་སྒྱུར་ལེགས་འགྲུབ་བྱུང་བ་རེད། ཁྱེད་ཀྱིས་རྗེས་སུ་ཐོ་འགོད་བྱས་ནས་ཡང་བསྐྱར་ཐོ་འགོད་བྱ་རྒྱུའི་གྲོས་འགོ་འདོན་རྒྱུ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="396"/>
        <location filename="../src/appupdate.cpp" line="406"/>
        <location filename="../src/appupdate.cpp" line="487"/>
        <location filename="../src/appupdate.cpp" line="496"/>
        <source>Update succeeded!</source>
        <translation>གསར་སྒྱུར་ལེགས་འགྲུབ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="417"/>
        <location filename="../src/appupdate.cpp" line="507"/>
        <location filename="../src/appupdate.cpp" line="611"/>
        <location filename="../src/appupdate.cpp" line="632"/>
        <source>Update has been canceled!</source>
        <translation>གསར་སྒྱུར་མེད་པར་བཟོས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="428"/>
        <location filename="../src/appupdate.cpp" line="437"/>
        <location filename="../src/appupdate.cpp" line="518"/>
        <location filename="../src/appupdate.cpp" line="527"/>
        <source>Update failed!</source>
        <translation>གསར་སྒྱུར་ལ་ཕམ་ཉེས་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="430"/>
        <location filename="../src/appupdate.cpp" line="520"/>
        <source>Failure reason:</source>
        <translation>ཕམ་ཉེས་བྱུང་བའི་རྒྱུ་རྐྱེན་ནི།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="544"/>
        <source>There are unresolved dependency conflicts in this update，Please select update all</source>
        <translation>གསར་སྒྱུར་འདིའི་ནང་དུ་ཐག་གཅོད་བྱས་མེད་པའི་གཞན་རྟེན་རང་བཞིན་གྱི་འགལ་བ་ཡོད་པས། ཚང་མ་གསར་སྒྱུར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="545"/>
        <location filename="../src/appupdate.cpp" line="736"/>
        <source>Prompt information</source>
        <translation>གསལ་འདེབས་ཀྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="547"/>
        <source>Update ALL</source>
        <translation>ཚང་མ་གསར་སྒྱུར་བྱ་བ།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="548"/>
        <location filename="../src/appupdate.cpp" line="622"/>
        <location filename="../src/appupdate.cpp" line="745"/>
        <location filename="../src/appupdate.cpp" line="817"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="595"/>
        <source>packages are going to be removed,Please confirm whether to accept!</source>
        <translation>ཐུམ་སྒྲིལ་མེད་པར་བཟོ་རྒྱུ་རེད། ཁྱེད་ཀྱིས་དང་ལེན་བྱེད་མིན་གཏན་འཁེལ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="698"/>
        <source>version:</source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="719"/>
        <source>The update stopped because of low battery.</source>
        <translation>གློག་སྨན་དམའ་བའི་རྐྱེན་གྱིས་གསར་སྒྱུར་བྱེད་མཚམས་བཞག་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="720"/>
        <source>The system update requires that the battery power is not less than 50%</source>
        <translation>མ་ལག་གསར་སྒྱུར་བྱེད་པར་གློག་སྨན་གྱི་སྒུལ་ཤུགས་50%ལས་མི་ཉུང་བའི་བླང་བྱ་བཏོན་ཡོད།</translation>
    </message>
    <message>
        <source>pkg will be uninstall!</source>
        <translation type="vanished">个软件包将被卸载！</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="92"/>
        <location filename="../src/appupdate.cpp" line="426"/>
        <location filename="../src/appupdate.cpp" line="516"/>
        <location filename="../src/appupdate.cpp" line="612"/>
        <location filename="../src/appupdate.cpp" line="633"/>
        <location filename="../src/appupdate.cpp" line="712"/>
        <location filename="../src/appupdate.cpp" line="772"/>
        <location filename="../src/appupdate.cpp" line="923"/>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="81"/>
        <source>details</source>
        <translation>ཞིབ་ཕྲའི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="146"/>
        <location filename="../src/appupdate.cpp" line="214"/>
        <source>Update log</source>
        <translation>གསར་སྒྱུར་ཉིན་ཐོ།</translation>
    </message>
    <message>
        <source>Newest:</source>
        <translation type="vanished">最新：</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="224"/>
        <source>Download size:</source>
        <translation>ཕབ་ལེན་གྱི་གཞི་ཁྱོན་ནི།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="225"/>
        <source>Install size:</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པའི་གཞི་ཁྱོན་ནི།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="251"/>
        <source>Current version:</source>
        <translation>ད་ལྟའི་པར་གཞི་ནི།</translation>
    </message>
    <message>
        <source>back</source>
        <translation type="vanished">收起</translation>
    </message>
    <message>
        <source>The battery is below 50% and the update cannot be downloaded</source>
        <translation type="vanished">电池电量低于 50%，无法下载更新</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="723"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="735"/>
        <source>A single update will not automatically backup the system, if you want to backup, please click Update All.</source>
        <translation>གསར་སྒྱུར་རྐྱང་པ་ཞིག་གིས་མ་ལག་རང་འགུལ་གྱིས་རྗེས་གྲབས་བྱེད་མི་སྲིད། གལ་ཏེ་ཁྱོད་ཀྱིས་རྗེས་གྲབས་བྱེད་འདོད་ན་གསར་སྒྱུར་ཚང་མ་མནན་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="744"/>
        <source>Do not backup, continue to update</source>
        <translation>རྗེས་གྲབས་མི་བྱེད་པར་མུ་མཐུད་དུ་གསར་སྒྱུར་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="748"/>
        <source>This time will no longer prompt</source>
        <translation>ཐེངས་འདིར་ཡང་བསྐྱར་སྐུལ་འདེད་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="825"/>
        <source>Ready to update</source>
        <translatorcomment>准备更新</translatorcomment>
        <translation>གསར་སྒྱུར་བྱེད་པར་གྲ་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="899"/>
        <source>downloaded</source>
        <translatorcomment>已下载</translatorcomment>
        <translation>ཕབ་ལེན་བྱས་ཟིན་པ།</translation>
    </message>
    <message>
        <source>Ready to install</source>
        <translation type="vanished">准备安装</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="905"/>
        <location filename="../src/appupdate.cpp" line="908"/>
        <source>downloading</source>
        <translation>ཕབ་ལེན་བྱེད་བཞིན་པ།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="905"/>
        <source>calculating</source>
        <translatorcomment>计算中</translatorcomment>
        <translation>རྩིས་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="983"/>
        <source>No content.</source>
        <translation>ནང་དོན་གང་ཡང་མེད།</translation>
    </message>
</context>
<context>
    <name>HistoryUpdateListWig</name>
    <message>
        <location filename="../src/historyupdatelistwig.cpp" line="99"/>
        <source>Success</source>
        <translation>ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../src/historyupdatelistwig.cpp" line="104"/>
        <source>Failed</source>
        <translation>གསར་བསྒྱུར་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/backup.cpp" line="135"/>
        <source>system upgrade new backup</source>
        <translation>མ་ལག་རིམ་སྤར་གསར་འཛུགས་གྲབས་ཉར།</translation>
    </message>
    <message>
        <location filename="../src/backup.cpp" line="136"/>
        <source>system upgrade increment backup</source>
        <translation>མ་ལག་རིམ་འཕར་འཕར་སྣོན་གྲབས་ཉར།</translation>
    </message>
</context>
<context>
    <name>TabWid</name>
    <message>
        <location filename="../src/tabwidget.cpp" line="58"/>
        <location filename="../src/tabwidget.cpp" line="126"/>
        <location filename="../src/tabwidget.cpp" line="141"/>
        <location filename="../src/tabwidget.cpp" line="146"/>
        <location filename="../src/tabwidget.cpp" line="186"/>
        <location filename="../src/tabwidget.cpp" line="1353"/>
        <location filename="../src/tabwidget.cpp" line="1426"/>
        <location filename="../src/tabwidget.cpp" line="1705"/>
        <location filename="../src/tabwidget.cpp" line="1726"/>
        <location filename="../src/tabwidget.cpp" line="1799"/>
        <location filename="../src/tabwidget.cpp" line="1992"/>
        <location filename="../src/tabwidget.cpp" line="2076"/>
        <location filename="../src/tabwidget.cpp" line="2231"/>
        <location filename="../src/tabwidget.cpp" line="2304"/>
        <location filename="../src/tabwidget.cpp" line="2655"/>
        <source>Check Update</source>
        <translation>ཞིབ་བཤེར་གསར་སྒྱུར།</translation>
    </message>
    <message>
        <source>initializing</source>
        <translation type="vanished">初始化中</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="133"/>
        <location filename="../src/tabwidget.cpp" line="293"/>
        <location filename="../src/tabwidget.cpp" line="708"/>
        <location filename="../src/tabwidget.cpp" line="1387"/>
        <location filename="../src/tabwidget.cpp" line="1815"/>
        <location filename="../src/tabwidget.cpp" line="1967"/>
        <location filename="../src/tabwidget.cpp" line="2190"/>
        <location filename="../src/tabwidget.cpp" line="2330"/>
        <location filename="../src/tabwidget.cpp" line="2367"/>
        <location filename="../src/tabwidget.cpp" line="2711"/>
        <source>UpdateAll</source>
        <translation>ཚང་མ་གསར་སྒྱུར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Your system is the latest!</source>
        <translation type="vanished">您的系统已是最新！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="929"/>
        <location filename="../src/tabwidget.cpp" line="1480"/>
        <source>No Information!</source>
        <translation>དཔྱད་གཞིའི་ཡིག་ཆ་གསར་བསྒྱུར་བྱས་མེད།</translation>
    </message>
    <message>
        <source>Last refresh:</source>
        <translation type="vanished">检查时间：</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="196"/>
        <source>Downloading and installing updates...</source>
        <translation>ཕབ་ལེན་དང་སྒྲིག་སྦྱོར་བྱས་པའི་གནས་ཚུལ་གསར་བ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Update now</source>
        <translation type="vanished">立即更新</translation>
    </message>
    <message>
        <source>Cancel update</source>
        <translation type="vanished">取消更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="261"/>
        <location filename="../src/tabwidget.cpp" line="668"/>
        <source>Being updated...</source>
        <translation>གསར་སྒྱུར་བྱེད་བཞིན་པའི་སྒང་ཡིན།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="273"/>
        <location filename="../src/tabwidget.cpp" line="1411"/>
        <source>Updatable app detected on your system!</source>
        <translation>ཁྱེད་ཚོའི་མ་ལག་ནང་དུ་ཞིབ་དཔྱད་ཚད་ལེན་བྱས་པའི་ཉེར་སྤྱོད་གོ་རིམ་གསར་པ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="312"/>
        <source>The backup restore partition could not be found. The system will not be backed up in this update!</source>
        <translation>རྗེས་གྲབས་སླར་གསོ་བྱེད་པའི་ཆ་ཤས་རྙེད་མི་ཐུབ། ཐེངས་འདིའི་གསར་སྒྱུར་ཁྲོད་མ་ལག་ལ་རྒྱབ་སྐྱོར་བྱེད་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="316"/>
        <source>Kylin backup restore tool is doing other operations, please update later.</source>
        <translation>ཆི་ལིན་སླར་གསོ་ལག་ཆ་ད་ལྟ་བཀོལ་སྤྱོད་གཞན་པ་བྱེད་བཞིན་ཡོད་པས་ཅུང་ཙམ་འགོར་རྗེས་གསར་བསྒྱུར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="321"/>
        <source>The source manager configuration file is abnormal, the system temporarily unable to update!</source>
        <translation>འབྱུང་ཁུངས་དོ་དམ་ཆས་ཀྱིས་ཡིག་ཆ་བཀོད་སྒྲིག་བྱས་པ་རྒྱུན་ལྡན་མིན་པས་གནས་སྐབས་སུ་གསར་བསྒྱུར་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="326"/>
        <source>Backup already, no need to backup again.</source>
        <translation>རྗེས་གྲབས་བྱས་ཟིན་པས་སླར་ཡང་རྗེས་གྲབས་བྱེད་དགོས་དོན་མེད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="335"/>
        <location filename="../src/tabwidget.cpp" line="2399"/>
        <source>Start backup,getting progress</source>
        <translation>གྲབས་ཉར་བྱེད་མགོ་བརྩམས་ནས་མྱུར་ཚད་ལེན་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="351"/>
        <source>Kylin backup restore tool does not exist, this update will not backup the system!</source>
        <translation>ཆི་ལིན་གྲབས་ཉར་སླར་གསོ་ཡོ་བྱདUUID་རྙེད་མི་ཐུབ་པས་ཐེངས་འདིའི་གསར་བསྒྱུར་གྲབས་ཉར་མ་ལག་མི་ཤེས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="247"/>
        <location filename="../src/tabwidget.cpp" line="367"/>
        <location filename="../src/tabwidget.cpp" line="377"/>
        <location filename="../src/tabwidget.cpp" line="402"/>
        <location filename="../src/tabwidget.cpp" line="673"/>
        <location filename="../src/tabwidget.cpp" line="1655"/>
        <location filename="../src/tabwidget.cpp" line="1837"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="391"/>
        <source>Calculated</source>
        <translation>རྩིས་བརྒྱབ་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="464"/>
        <source>There are unresolved dependency conflicts in this update，Please contact the administrator!</source>
        <translation>གསར་སྒྱུར་འདིའི་ནང་དུ་ཐག་གཅོད་བྱས་མེད་པའི་གཞན་རྟེན་རང་བཞིན་གྱི་འགལ་བ་ཡོད་པས། དོ་དམ་པར་འབྲེལ་གཏུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="399"/>
        <location filename="../src/tabwidget.cpp" line="465"/>
        <location filename="../src/tabwidget.cpp" line="1834"/>
        <source>Prompt information</source>
        <translation>མགྱོགས་མྱུར་གྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="467"/>
        <source>Sure</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="643"/>
        <location filename="../src/tabwidget.cpp" line="646"/>
        <source>In the download</source>
        <translation>ཕབ་ལེན་བྱེད་པའི་ཁྲོད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="643"/>
        <source>calculating</source>
        <translatorcomment>计算中</translatorcomment>
        <translation>རྩིས་རྒྱག་བཞིན་པ།</translation>
    </message>
    <message>
        <source>In the install...</source>
        <translation type="vanished">安装中...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="658"/>
        <source>Backup complete.</source>
        <translation>གྲབས་ཉར་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="678"/>
        <source>System is backing up...</source>
        <translation>གྲབས་ཉར་མ་ལག་གིས་རྒྱབ་སྐྱོར་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="696"/>
        <source>Backup interrupted, stop updating!</source>
        <translation>རྗེས་གྲབས་བྱེད་མཚམས་བཞག་ནས་གསར་སྒྱུར་བྱེད་མཚམས་འཇོག་དགོས།</translation>
    </message>
    <message>
        <source>Backup finished!</source>
        <translation type="vanished">备份完成！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="721"/>
        <source>Kylin backup restore tool exception:</source>
        <translation>ཆི་ལིན་གྲབས་ཉར་སླར་གསོ་ཡོ་བྱད་རྒྱུན་ལྡན་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="721"/>
        <source>There will be no backup in this update!</source>
        <translation>གསར་སྒྱུར་འདིའི་ནང་དུ་རྗེས་གྲབས་དཔུང་ཁག་ཡོད་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="719"/>
        <source>The status of backup completion is abnormal</source>
        <translatorcomment>备份完成状态异常</translatorcomment>
        <translation>རྗེས་གྲབས་ལེགས་འགྲུབ་བྱུང་བའི་གནས་ཚུལ་ནི་རྒྱུན་ལྡན་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="842"/>
        <source>Getting update list</source>
        <translation>གསར་སྒྱུར་གྱི་མིང་ཐོ་ཐོབ་པ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="865"/>
        <source>Software source update successed: </source>
        <translatorcomment>软件源更新成功： </translatorcomment>
        <translation>མཉེན་ཆས་ཀྱི་འབྱུང་ཁུངས་གསར་སྒྱུར་ལེགས་འགྲུབ་བྱུང་བ། </translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="876"/>
        <source>Software source update failed: </source>
        <translation>མཉེན་ཆས་ཀྱི་འབྱུང་ཁུངས་གསར་སྒྱུར་ལ་ཕམ་ཉེས་བྱུང་། </translation>
    </message>
    <message>
        <source>Update software source :</source>
        <translation type="vanished">更新软件源进度：</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="255"/>
        <location filename="../src/tabwidget.cpp" line="990"/>
        <source>Update</source>
        <translation>མ་ལག་གསར་སྒྱུར།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="398"/>
        <source>There are unresolved dependency conflicts in this update，Please select Dist-upgrade</source>
        <translatorcomment>本次更新存在无法解决的依赖冲突，请选择全盘更新</translatorcomment>
        <translation>ཐེངས་འདིའི་གསར་བསྒྱུར་ལ་ཐག་གཅོད་བྱེད་མི་ཐུབ་པའི་འགལ་བ་ཡོད་པས་ཕྱོགས་ཡོངས་ནས་གསར་བསྒྱུར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="401"/>
        <source>Dist-upgrade</source>
        <translatorcomment>全盘更新</translatorcomment>
        <translation>ཡོངས་རྫོགས་གསར་སྒྱུར།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="546"/>
        <source>The system is downloading the update!</source>
        <translation>མ་ལག་གིས་གསར་སྒྱུར་ཕབ་ལེན་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="551"/>
        <source>Downloading the updates...</source>
        <translation>གནས་ཚུལ་གསར་པ་ཕབ་ལེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="553"/>
        <source>Installing the updates...</source>
        <translation>གསར་སྒྱུར་སྒྲིག་སྦྱོར་བྱེད་པ...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="635"/>
        <source>In the install</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པའི་ཁྲོད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="874"/>
        <location filename="../src/tabwidget.cpp" line="1783"/>
        <source>Retry</source>
        <translation>བསྐྱར་དུ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="972"/>
        <source>The system is checking update :</source>
        <translation>མ་ལག་གིས་གནས་ཚུལ་གསར་བར་ཞིབ་བཤེར་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>Download Limit(Kb/s)</source>
        <translation type="vanished">下载限速(Kb/s)</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1241"/>
        <source>View history</source>
        <translation>ལོ་རྒྱུས་གསར་སྒྱུར་ལ་ལྟ་ཞིབ།</translation>
    </message>
    <message>
        <source>details</source>
        <translation type="vanished">详情</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1094"/>
        <source>Update Settings</source>
        <translation>གསར་སྒྱུར་སྒྲིག་བཀོད།</translation>
        <extra-contents_path>/upgrade/Update Settings</extra-contents_path>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1109"/>
        <source>Allowed to renewable notice</source>
        <translation>བསྐྱར་སྤྱོད་རུང་བའི་བརྡ་ཐོ་བཏང་ཆོག།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1121"/>
        <source>Backup current system before updates all</source>
        <translation>ཚང་མ་གསར་སྒྱུར་མ་བྱས་གོང་ལ་ད་ལྟ་སྤྱོད་བཞིན་ཡོད་པ།</translation>
    </message>
    <message>
        <source>Download Limit(KB/s)</source>
        <translation type="vanished">下载限速(KB/s)</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1141"/>
        <source>It will be avaliable in the next download.</source>
        <translation>ཐེངས་རྗེས་མར་ཕབ་ལེན་བྱས་ན་འགན་འཁྲི་འཁུར་དགོས།</translation>
    </message>
    <message>
        <source>Automatically download and install updates</source>
        <translation type="vanished">自动下载和安装更新</translation>
    </message>
    <message>
        <source>After it is turned on, the system will automatically download and install updates when there is an available network and available backup and restore partitions.</source>
        <translation type="vanished">开启后，当有可用网络和可用备份和恢复分区时，系统会自动下载和安装更新。</translation>
    </message>
    <message>
        <source>Upgrade during poweroff</source>
        <translation type="vanished">关机检测更新</translation>
    </message>
    <message>
        <source>Download Limit(kB/s)</source>
        <translation type="vanished">下载限速(kB/s)</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1167"/>
        <source>The system will automatically updates when there is an available network and backup.</source>
        <translation>མ་ལག་འདི་ལ་ད་ཡོད་ཀྱི་དྲ་རྒྱ་དང་རྗེས་གྲབས་ཡོད་པའི་སྐབས་སུ་རང་འགུལ་གྱིས་གསར་སྒྱུར་བྱེད་སྲིད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1313"/>
        <source>Ready to install</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པར་གྲ་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="184"/>
        <location filename="../src/tabwidget.cpp" line="292"/>
        <location filename="../src/tabwidget.cpp" line="896"/>
        <location filename="../src/tabwidget.cpp" line="967"/>
        <location filename="../src/tabwidget.cpp" line="1378"/>
        <location filename="../src/tabwidget.cpp" line="1408"/>
        <location filename="../src/tabwidget.cpp" line="1451"/>
        <location filename="../src/tabwidget.cpp" line="1490"/>
        <location filename="../src/tabwidget.cpp" line="2052"/>
        <location filename="../src/tabwidget.cpp" line="2172"/>
        <location filename="../src/tabwidget.cpp" line="2277"/>
        <source>Last Checked:</source>
        <translation>མཇུག་མཐར་ཞིབ་བཤེར་བྱས་པ།</translation>
    </message>
    <message>
        <source>Your system is the latest:V10sp1-</source>
        <translation type="vanished">你的系统已是最新版本：V10sp1-</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="177"/>
        <location filename="../src/tabwidget.cpp" line="285"/>
        <location filename="../src/tabwidget.cpp" line="889"/>
        <location filename="../src/tabwidget.cpp" line="960"/>
        <location filename="../src/tabwidget.cpp" line="1371"/>
        <location filename="../src/tabwidget.cpp" line="1401"/>
        <location filename="../src/tabwidget.cpp" line="1444"/>
        <location filename="../src/tabwidget.cpp" line="2045"/>
        <location filename="../src/tabwidget.cpp" line="2165"/>
        <location filename="../src/tabwidget.cpp" line="2270"/>
        <source>No information!</source>
        <translation>དཔྱད་གཞིའི་ཡིག་ཆ་གསར་བསྒྱུར་བྱས་མེད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1135"/>
        <source>Download Limit</source>
        <translation>ཕབ་ལེན་གྱི་བཅད་གྲངས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1589"/>
        <location filename="../src/tabwidget.cpp" line="1617"/>
        <source>Dependency conflict exists in this update,need to be completely repaired!</source>
        <translation>གསར་སྒྱུར་འདིའི་ཁྲོད་དུ་གཞན་རྟེན་རང་བཞིན་གྱི་འགལ་བ་ཡོད་པས་རྦད་དེ་ཉམས་གསོ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1591"/>
        <source>There are </source>
        <translation>ཐེངས་འདིའི་གསར་བསྒྱུར་ལ་འགལ་བ་ཡོད་པས་བསུབ་རྒྱུ་རེད། </translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1591"/>
        <source> packages going to be removed,Please confirm whether to accept!</source>
        <translation> ཐུམ་སྒྲིལ་མེད་པར་བཟོ་རྒྱུ་རེད། ཁྱེད་ཀྱིས་དང་ལེན་བྱེད་མིན་གཏན་འཁེལ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1619"/>
        <source>packages are going to be removed,Please confirm whether to accept!</source>
        <translation>ཐུམ་སྒྲིལ་མེད་པར་བཟོ་རྒྱུ་རེད། ཁྱེད་ཀྱིས་དང་ལེན་བྱེད་མིན་གཏན་འཁེལ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1630"/>
        <source>trying to reconnect </source>
        <translation>ཐབས་བརྒྱ་ཇུས་སྟོང་གིས་འབྲེལ་མཐུད་བྱེད་པ། </translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1630"/>
        <source> times</source>
        <translation> ཐེངས་གྲངས།</translation>
    </message>
    <message>
        <source>back</source>
        <translation type="vanished">收起</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1674"/>
        <source>Auto-Update is backing up......</source>
        <translatorcomment>自动更新进程正在备份中......</translatorcomment>
        <translation>རང་འགུལ་གྱིས་གསར་སྒྱུར་བྱེད་པར་རྒྱབ་སྐྱོར་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1746"/>
        <source>The updater is NOT start</source>
        <translation>གསར་སྒྱུར་བྱེད་མཁན་གྱིས་གསར་སྒྱུར་བྱེད་འགོ་ཚུགས་མེད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1761"/>
        <source>The progress is updating...</source>
        <translation>འཕེལ་རིམ་གསར་སྒྱུར་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1770"/>
        <source>The progress is installing...</source>
        <translation>འཕེལ་རིམ་སྒྲིག་སྦྱོར་བྱེད་བཞིན་པའི་སྒང་རེད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1779"/>
        <source>The updater is busy！</source>
        <translation>རྒྱབ་སྟེགས་ཀྱི་གོ་རིམ་གསར་སྒྱུར་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1795"/>
        <location filename="../src/tabwidget.cpp" line="1811"/>
        <source>Updating the software source</source>
        <translation>མཉེན་ཆས་ཀྱི་འབྱུང་ཁུངས་གསར་སྒྱུར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>The battery is below 50% and the update cannot be downloaded</source>
        <translation type="vanished">电池电量低于 50%，无法下载更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="450"/>
        <location filename="../src/tabwidget.cpp" line="1826"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1833"/>
        <source>Please back up the system before all updates to avoid unnecessary losses</source>
        <translation>གསར་སྒྱུར་མ་བྱས་གོང་ལ་མ་ལག་ཕྱིར་འཐེན་བྱས་ནས་དགོས་མེད་ཀྱི་གྱོང་གུན་མི་ཡོང་བ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1838"/>
        <source>Only Update</source>
        <translation>ཐད་ཀར་གསར་སྒྱུར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1839"/>
        <source>Back And Update</source>
        <translation>གྲབས་ཉར་དང་གསར་སྒྱུར།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2016"/>
        <location filename="../src/tabwidget.cpp" line="2138"/>
        <location filename="../src/tabwidget.cpp" line="2211"/>
        <location filename="../src/tabwidget.cpp" line="2332"/>
        <source>update has been canceled!</source>
        <translation>གསར་སྒྱུར་མེད་པར་བཟོས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1996"/>
        <location filename="../src/tabwidget.cpp" line="2081"/>
        <source>This update has been completed！</source>
        <translatorcomment>本次更新已完成！</translatorcomment>
        <translation>གསར་སྒྱུར་འདི་ལེགས་འགྲུབ་བྱུང་སོང་།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="164"/>
        <location filename="../src/tabwidget.cpp" line="1356"/>
        <location filename="../src/tabwidget.cpp" line="1429"/>
        <location filename="../src/tabwidget.cpp" line="2000"/>
        <location filename="../src/tabwidget.cpp" line="2121"/>
        <location filename="../src/tabwidget.cpp" line="2246"/>
        <location filename="../src/tabwidget.cpp" line="2306"/>
        <source>Your system is the latest:V10</source>
        <translation>ཁྱེད་ཚོའི་མ་ལག་ནི་ཆེས་གསར་བ་ཡིན། V10</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="246"/>
        <source>Keeping update</source>
        <translation>གསར་སྒྱུར་རྒྱུན་འཁྱོངས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="248"/>
        <source>It is recommended to back up the system before all updates to avoid unnecessary losses!</source>
        <translation>གསར་སྒྱུར་མ་བྱས་གོང་ལ་མ་ལག་ལ་རྒྱབ་སྐྱོར་བྱས་ནས་དགོས་མེད་ཀྱི་གྱོང་གུན་མི་ཡོང་བ་བྱ་རྒྱུའི་གྲོས་འགོ་བཏོན་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="448"/>
        <source>Insufficient disk space to download updates!</source>
        <translation>སྡུད་སྡེར་བར་སྟོང་མི་འདང་བས་གསར་བསྒྱུར་ཕབ་ལེན་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="519"/>
        <source>supposed</source>
        <translation>སྔོན་དཔག་བྱས་ན་བཟུང་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="575"/>
        <source>s</source>
        <translation>སྐར་ཆ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="577"/>
        <source>min</source>
        <translation>སྐར་མ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="579"/>
        <source>h</source>
        <translation>དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1166"/>
        <source>Automatically updates</source>
        <translation>རང་འགུལ་གྱིས་གསར་སྒྱུར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1822"/>
        <source>The update stopped because of low battery.</source>
        <translation>གློག་སྨན་དམའ་བའི་རྐྱེན་གྱིས་གསར་སྒྱུར་བྱེད་མཚམས་བཞག་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1823"/>
        <source>The system update requires that the battery power is not less than 50%</source>
        <translation>མ་ལག་གསར་སྒྱུར་བྱེད་པར་གློག་སྨན་གྱི་སྒུལ་ཤུགས་50%ལས་མི་ཉུང་བའི་བླང་བྱ་བཏོན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2021"/>
        <location filename="../src/tabwidget.cpp" line="2143"/>
        <location filename="../src/tabwidget.cpp" line="2216"/>
        <source>Part of the update failed!</source>
        <translation>གསར་སྒྱུར་གྱི་ཆ་ཤས་ཤིག་ལ་ཕམ་ཉེས་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2022"/>
        <location filename="../src/tabwidget.cpp" line="2144"/>
        <location filename="../src/tabwidget.cpp" line="2217"/>
        <source>Failure reason:</source>
        <translatorcomment>失败原因：</translatorcomment>
        <translation>ཕམ་ཉེས་བྱུང་བའི་རྒྱུ་རྐྱེན་ནི།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2077"/>
        <source>Finish the download!</source>
        <translation>ཕབ་ལེན་བྱས་ཚར་སོང་།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2096"/>
        <source>The system has download the update!</source>
        <translation>མ་ལག་གིས་གསར་སྒྱུར་ཕབ་ལེན་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2098"/>
        <source>It&apos;s need to reboot to make the install avaliable</source>
        <translation>གསར་བསྒྱུར་ཕབ་ལེན་ལེགས་འགྲུབ་བྱུང་སོང་།་གསར་བསྒྱུར་ནང་འཇུག་བྱེད་དམ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2099"/>
        <source>Reboot notification</source>
        <translation>བསྐྱར་དུ་བརྡ་ཐོ་གཏོང་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2101"/>
        <source>Reboot rightnow</source>
        <translation>ཡང་བསྐྱར་སྒོ་ཕྱེ་བ་མ་ཟད་ནང་འཇུག་བྱས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2102"/>
        <source>Later</source>
        <translation>རྗེས་སུ་སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2196"/>
        <location filename="../src/tabwidget.cpp" line="2285"/>
        <source>Part of the update success!</source>
        <translation>གསར་སྒྱུར་ལེགས་འགྲུབ་བྱུང་བའི་ཆ་ཤས་ཤིག་རེད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2241"/>
        <source>All the update has been downloaded.</source>
        <translation>གསར་སྒྱུར་ཚང་མ་ཕབ་ལེན་བྱས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2379"/>
        <source>An important update is in progress, please wait.</source>
        <translation>གསར་བསྒྱུར་གལ་ཆེན་ཞིག་བྱེད་བཞིན་ཡོད་པས་སྒུག་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2412"/>
        <source>Failed to write configuration file, this update will not back up the system!</source>
        <translation>བཀོད་སྒྲིག་ཡིག་ཆ་འབྲི་མ་ཐུབ་ན། གསར་སྒྱུར་འདིས་མ་ལག་ལ་རྒྱབ་སྐྱོར་བྱེད་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2416"/>
        <source>Insufficient backup space, this update will not backup your system!</source>
        <translation>རྗེས་གྲབས་ཀྱི་བར་སྟོང་མི་འདང་བས། གསར་སྒྱུར་འདིས་ཁྱེད་ཚོའི་མ་ལག་ལ་རྗེས་གྲབས་བྱེད་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2420"/>
        <source>Kylin backup restore tool could not find the UUID, this update will not backup the system!</source>
        <translation>ཅིན་ལིན་གྱི་རྗེས་གྲབས་ཡོ་བྱད་ཀྱིས་UUIDམ་རྙེད་པས། གསར་སྒྱུར་འདིས་མ་ལག་ལ་རྗེས་གྲབས་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>The backup restore partition is abnormal. You may not have a backup restore partition.For more details,see /var/log/backup.log</source>
        <translation type="vanished">备份还原分区异常，您可能没有备份还原分区。更多详细信息，可以参看/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1870"/>
        <location filename="../src/tabwidget.cpp" line="2431"/>
        <source>Calculating Capacity...</source>
        <translation>རྩིས་འཁོར་མ་ལག་གི་བར་སྟོང་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>The system backup partition is not detected. Do you want to continue updating?</source>
        <translation type="vanished">未检测到系统备份还原分区，是否继续更新？</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2533"/>
        <source>Calculating</source>
        <translation>རྩིས་རྒྱག་བཞིན་པ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2546"/>
        <source>The system is updating...</source>
        <translation>གསར་སྒྱུར་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2602"/>
        <source>Auto-Update progress is installing new file：</source>
        <translatorcomment>系统自动更新功能正在安装新文件：</translatorcomment>
        <translation>རང་འགུལ་གྱིས་གསར་སྒྱུར་བྱེད་པའི་འཕེལ་རིམ་ནི་ཡིག་ཆ་གསར་པ་སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2623"/>
        <source>Auto-Update progress finished!</source>
        <translatorcomment>系统自动更新完成！</translatorcomment>
        <translation>རང་འགུལ་གྱིས་གསར་སྒྱུར་གྱི་འཕེལ་རིམ་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2631"/>
        <source>Auto-Update progress fail in backup!</source>
        <translatorcomment>自动更新安装时备份失败！</translatorcomment>
        <translation>རང་འགུལ་གྱིས་གསར་སྒྱུར་བྱེད་པའི་འཕེལ་རིམ་དེ་རྗེས་གྲབས་ལས་དོན་ལ་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2654"/>
        <source>Failed in updating because of broken environment.</source>
        <translation>ཁོར་ཡུག་གཏོར་བཤིག་བཏང་བའི་རྐྱེན་གྱིས་གསར་སྒྱུར་བྱེད་མ་ཐུབ་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2668"/>
        <source>It&apos;s fixing up the environment...</source>
        <translation>ཁོར་ཡུག་ཞིག་གསོ་དང་གསར་བསྒྱུར་བྱེད་བཞིན་ཡོད།</translation>
    </message>
</context>
<context>
    <name>UpdateDbus</name>
    <message>
        <location filename="../src/updatedbus.cpp" line="118"/>
        <source>System-Upgrade</source>
        <translation>མ་ལག་རིམ་སྤར།</translation>
    </message>
    <message>
        <location filename="../src/updatedbus.cpp" line="121"/>
        <source>ukui-control-center-update</source>
        <translation>སྒྲིག་བཀོད།གསར་བསྒྱུར་གྱི་གསལ་བརྡ།</translation>
    </message>
</context>
<context>
    <name>UpdateLog</name>
    <message>
        <location filename="../src/updatelog.cpp" line="17"/>
        <source>Update log</source>
        <translation>གསར་སྒྱུར་ཉིན་ཐོ།</translation>
    </message>
</context>
<context>
    <name>UpdateSource</name>
    <message>
        <location filename="../src/updatesource.cpp" line="71"/>
        <source>Connection failed, please reconnect!</source>
        <translation>འབྲེལ་མཐུད་བྱེད་མ་ཐུབ་པས་ཡང་བསྐྱར་འབྲེལ་མཐུད་བྱེད་རོགས།</translation>
    </message>
</context>
<context>
    <name>Upgrade</name>
    <message>
        <location filename="../upgrade.cpp" line="12"/>
        <source>Upgrade</source>
        <translation>རིམ་པ་སྤར་བ།</translation>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="81"/>
        <source>View history</source>
        <translation>ལོ་རྒྱུས་ལ་གསར་བསྒྱུར་ལ་ལྟ་ཞིབ་བྱེད་པ།</translation>
        <extra-contents_path>/Upgrade/View history</extra-contents_path>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="83"/>
        <source>Update Settings</source>
        <translation>གསར་སྒྱུར་སྒྲིག་བཀོད།</translation>
        <extra-contents_path>/Upgrade/Update Settings</extra-contents_path>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="85"/>
        <source>Allowed to renewable notice</source>
        <translation>བསྐྱར་སྤྱོད་རུང་བའི་བརྡ་ཐོ་བཏང་ཆོག།</translation>
        <extra-contents_path>/Upgrade/Allowed to renewable notice</extra-contents_path>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="87"/>
        <source>Automatically download and install updates</source>
        <translation>རང་འགུལ་གྱིས་ཕབ་ལེན་དང་སྒྲིག་སྦྱོར་བྱས་པ།</translation>
        <extra-contents_path>/Upgrade/Automatically download and install updates</extra-contents_path>
    </message>
</context>
<context>
    <name>dependencyfixdialog</name>
    <message>
        <source>details</source>
        <translation type="vanished">详情</translation>
    </message>
    <message>
        <location filename="../src/dependencyfixdialog.cpp" line="20"/>
        <source>show details</source>
        <translation>ཞིབ་ཕྲའི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../src/dependencyfixdialog.cpp" line="29"/>
        <source>uninstall and update</source>
        <translation>སྒྲིག་སྦྱོར་དང་གསར་སྒྱུར་བྱས་པ།</translation>
    </message>
    <message>
        <source>uninstall</source>
        <translation type="vanished">移除</translation>
    </message>
    <message>
        <location filename="../src/dependencyfixdialog.cpp" line="31"/>
        <source>cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
</context>
<context>
    <name>fixbrokeninstalldialog</name>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="80"/>
        <source>We need to fix up the environment!</source>
        <translation>ཁོར་ཡུག་ཞིག་གསོ་དང་གསར་བསྒྱུར་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="86"/>
        <source>There will be uninstall some packages to complete the update!</source>
        <translation>དེ་རུ་ཐུམ་སྒྲིལ་ཁ་ཤས་སྒྲིག་སྦྱོར་བྱས་ནས་གསར་སྒྱུར་ལེགས་འགྲུབ་བྱེད་རྒྱུ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="95"/>
        <source>The following packages will be uninstalled:</source>
        <translation>གཤམ་གསལ་གྱི་ཐུམ་སྒྲིལ་དེ་དག་སྒྲིག་སྦྱོར་བྱས་མེད་པ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="120"/>
        <source>PKG Details</source>
        <translation>མཉེན་ཆས་ཁུག་མའི་ཞིབ་ཕྲའི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="130"/>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="395"/>
        <source>details</source>
        <translation>ཞིབ་ཕྲའི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="134"/>
        <source>Keep</source>
        <translation>ཟློག་པ།</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="138"/>
        <source>Remove</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="294"/>
        <source>Attention on update</source>
        <translation>གསར་སྒྱུར་ལ་དོ་སྣང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="383"/>
        <source>back</source>
        <translation>བསྡུ་ཉར།</translation>
    </message>
</context>
<context>
    <name>fixupdetaillist</name>
    <message>
        <location filename="../src/fixupdetaillist.cpp" line="59"/>
        <source>No content.</source>
        <translation>ནང་དོན་གང་ཡང་མེད།</translation>
    </message>
    <message>
        <location filename="../src/fixupdetaillist.cpp" line="115"/>
        <source>Update Details</source>
        <translation>གནས་ཚུལ་ཞིབ་ཕྲ་གསར་སྒྱུར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/fixupdetaillist.cpp" line="549"/>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
</context>
<context>
    <name>m_updatelog</name>
    <message>
        <location filename="../src/m_updatelog.cpp" line="57"/>
        <source>No content.</source>
        <translation>ནང་དོན་གང་ཡང་མེད།</translation>
    </message>
    <message>
        <location filename="../src/m_updatelog.cpp" line="113"/>
        <source>Update Details</source>
        <translation>གནས་ཚུལ་ཞིབ་ཕྲ་གསར་སྒྱུར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/m_updatelog.cpp" line="547"/>
        <location filename="../src/m_updatelog.cpp" line="585"/>
        <source>Search content</source>
        <translation>འཚོལ་བཤེར་གྱི་ནང་དོན།</translation>
    </message>
    <message>
        <location filename="../src/m_updatelog.cpp" line="634"/>
        <source>History Log</source>
        <translation>ལོ་རྒྱུས་ཀྱི་གསར་སྒྱུར།</translation>
    </message>
</context>
<context>
    <name>updatedeleteprompt</name>
    <message>
        <source>Dependency conflict exists in this update!</source>
        <translation type="vanished">本次更新存在依赖冲突！</translation>
    </message>
    <message>
        <source>There will be uninstall some packages to complete the update!</source>
        <translation type="vanished">将卸载部分软件包以完成更新！</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="78"/>
        <source>The following packages will be uninstalled:</source>
        <translation>གཤམ་གསལ་གྱི་ཐུམ་སྒྲིལ་དེ་དག་སྒྲིག་སྦྱོར་བྱས་མེད་པ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="97"/>
        <source>PKG Details</source>
        <translation>མཉེན་ཆས་ཁུག་མའི་ཞིབ་ཕྲའི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="241"/>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <source>details</source>
        <translation type="vanished">详情</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="105"/>
        <source>Keep</source>
        <translation>ཟློག་པ།</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="109"/>
        <source>Remove</source>
        <translation>དང་དུ་ལེན་པ།</translation>
    </message>
    <message>
        <source>Update Prompt</source>
        <translation type="vanished">更新提示</translation>
    </message>
    <message>
        <source>back</source>
        <translation type="vanished">收起</translation>
    </message>
</context>
</TS>
