<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>AlertDialog</name>
    <message>
        <location filename="../alertdialog.ui" line="14"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
</context>
<context>
    <name>KAlertDialog</name>
    <message>
        <location filename="../kalertdialog.cpp" line="28"/>
        <source>The file you selected is a directory or does not have permissions, cannot be shredded!</source>
        <translation>Сиз тандап алган файл каталог болуп саналат же уруксаты жок, майдаланбайт!</translation>
    </message>
    <message>
        <location filename="../kalertdialog.cpp" line="31"/>
        <source>sure</source>
        <translation>албетте</translation>
    </message>
</context>
<context>
    <name>KSureDialog</name>
    <message>
        <location filename="../ksuredialog.cpp" line="26"/>
        <source>Are you sure to start crushing?</source>
        <translation>Сиз майдалап баштай турганы шексизби?</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="32"/>
        <source>Crushed files cannot be recovered!</source>
        <translation>Майдаланган файлдарды калыбына келтирүү мүмкүн эмес!</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="39"/>
        <source>sure</source>
        <translation>албетте</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="47"/>
        <source>cancle</source>
        <translation>канкл</translation>
    </message>
</context>
<context>
    <name>ShredDialog</name>
    <message>
        <source>Kylin Shred Manager</source>
        <translation type="vanished">Кайлин Шред менеджери</translation>
    </message>
    <message>
        <source>Shred Manager</source>
        <translation type="vanished">文件粉碎机</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="303"/>
        <source>No file selected to be shredded</source>
        <translation>Майдалоо үчүн тандалган файл жок</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="155"/>
        <source>Shred File</source>
        <translation>Майдаланган файл</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="37"/>
        <source>FileShredder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="154"/>
        <location filename="../shreddialog.cpp" line="199"/>
        <source>The file is completely shredded and cannot be recovered</source>
        <translation>Файл толугу менен майдаланган жана калыбына келтирилбейт</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="156"/>
        <source>Deselect</source>
        <translation>Деселект</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="157"/>
        <source>Add File</source>
        <translation>Файлды кошуу</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="158"/>
        <source>Note:File shredding cannot be cancelled, please exercise caution!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Note: The file shredding process cannot be cancelled, please operate with caution!</source>
        <translation type="vanished">Эскертүү: Файлды майдалоо процессин жокко чыгаруу мүмкүн эмес, сураныч, этияттык менен иштейсиз!</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="231"/>
        <source>File Shredding ...</source>
        <translation>Файл майдалоо ...</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="288"/>
        <source>Please select the file to shred!</source>
        <translation>Файлды майдалоо үчүн тандаңыз!</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="293"/>
        <source>You did not select a file with permissions!</source>
        <translation>Сиз уруксаты бар файлды тандаган жоксуңар!</translation>
    </message>
    <message>
        <source>Shattering...</source>
        <translation type="vanished">粉碎中...</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="190"/>
        <source>Select file</source>
        <translation>Файлды тандоо</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="190"/>
        <source>All Files(*)</source>
        <translation>Бардык файлдар (*)</translation>
    </message>
    <message>
        <source>Select file!</source>
        <translation type="vanished">请选择文件！</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="253"/>
        <source>Shred successfully!</source>
        <translation>Майдаланган ийгиликтүү!</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="266"/>
        <source>Shred failed!</source>
        <translation>Майдаланган ишке ашпады!</translation>
    </message>
</context>
<context>
    <name>ShredManager</name>
    <message>
        <location filename="../shredmanager.cpp" line="50"/>
        <source>Shred Manager</source>
        <translation>Шред менеджери</translation>
    </message>
    <message>
        <location filename="../shredmanager.cpp" line="55"/>
        <source>Delete files makes it unable to recover</source>
        <translation>Файлдарды жоготуу аны калыбына келтире албайт</translation>
    </message>
</context>
</TS>
