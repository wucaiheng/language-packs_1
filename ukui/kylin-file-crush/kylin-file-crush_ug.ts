<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>AlertDialog</name>
    <message>
        <location filename="../alertdialog.ui" line="14"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
</context>
<context>
    <name>KAlertDialog</name>
    <message>
        <location filename="../kalertdialog.cpp" line="28"/>
        <source>The file you selected is a directory or does not have permissions, cannot be shredded!</source>
        <translation>سىز تاللىغان ھۆججەت مۇندەرىجە ياكى ئىجازەتنامىسى يوق، پارچىلاشقا بولمايدۇ!</translation>
    </message>
    <message>
        <location filename="../kalertdialog.cpp" line="31"/>
        <source>sure</source>
        <translation>ئەلۋەتتە</translation>
    </message>
</context>
<context>
    <name>KSureDialog</name>
    <message>
        <location filename="../ksuredialog.cpp" line="26"/>
        <source>Are you sure to start crushing?</source>
        <translation>سىز چوقۇم چېقىشنى باشلامسىز؟</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="32"/>
        <source>Crushed files cannot be recovered!</source>
        <translation>پارچىلانغان ھۆججەتلەرنى ئەسلىگە كەلتۈرگىلى بولمايدۇ!</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="39"/>
        <source>sure</source>
        <translation>ئەلۋەتتە</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="47"/>
        <source>cancle</source>
        <translation>cancle</translation>
    </message>
</context>
<context>
    <name>ShredDialog</name>
    <message>
        <source>Kylin Shred Manager</source>
        <translation type="vanished">Kylin Shred باشقۇرغۇ</translation>
    </message>
    <message>
        <source>Shred Manager</source>
        <translation type="vanished">文件粉碎机</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="303"/>
        <source>No file selected to be shredded</source>
        <translation>پارچىلاشقا تاللانغان ھۆججەت يوق</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="155"/>
        <source>Shred File</source>
        <translation>Shred ھۆججەت</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="37"/>
        <source>FileShredder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="154"/>
        <location filename="../shreddialog.cpp" line="199"/>
        <source>The file is completely shredded and cannot be recovered</source>
        <translation>ھۆججەت پۈتۈنلەي پارچىلىنىپ، ئەسلىگە كەلتۈرگىلى بولمايدۇ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="156"/>
        <source>Deselect</source>
        <translation>دېسكىغا ئېلىش</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="157"/>
        <source>Add File</source>
        <translation>ھۆججەت قوشۇش</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="158"/>
        <source>Note:File shredding cannot be cancelled, please exercise caution!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Note: The file shredding process cannot be cancelled, please operate with caution!</source>
        <translation type="vanished">ئەسكەرتىش: ھۆججەت پارچىلاش جەريانىنى بىكار قىلىشقا بولمايدۇ، ئېھتىيات بىلەن مەشغۇلات قىلىڭ!</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="231"/>
        <source>File Shredding ...</source>
        <translation>ھۆججەت Shredding ...</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="288"/>
        <source>Please select the file to shred!</source>
        <translation>پارچىلاش ئۈچۈن ھۆججەتنى تاللاپ كۆرۈڭ!</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="293"/>
        <source>You did not select a file with permissions!</source>
        <translation>سىز ئىجازەتنامىسى بار ھۆججەتنى تاللىمىدىڭىز!</translation>
    </message>
    <message>
        <source>Shattering...</source>
        <translation type="vanished">粉碎中...</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="190"/>
        <source>Select file</source>
        <translation>ھۆججەت تاللاش</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="190"/>
        <source>All Files(*)</source>
        <translation>بارلىق ھۆججەتلەر(*)</translation>
    </message>
    <message>
        <source>Select file!</source>
        <translation type="vanished">请选择文件！</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="253"/>
        <source>Shred successfully!</source>
        <translation>مۇۋەپپىقىيەتلىك قىرغىن قىل!</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="266"/>
        <source>Shred failed!</source>
        <translation>Shred مەغلۇپ بولدى!</translation>
    </message>
</context>
<context>
    <name>ShredManager</name>
    <message>
        <location filename="../shredmanager.cpp" line="50"/>
        <source>Shred Manager</source>
        <translation>Shred باشقۇرغۇ</translation>
    </message>
    <message>
        <location filename="../shredmanager.cpp" line="55"/>
        <source>Delete files makes it unable to recover</source>
        <translation>ھۆججەتلەرنى ئۆچۈرۈش ئارقىلىق ئەسلىگە كەلتۈرگىلى بولمايدۇ</translation>
    </message>
</context>
</TS>
