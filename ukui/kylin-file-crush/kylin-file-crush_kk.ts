<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>AlertDialog</name>
    <message>
        <location filename="../alertdialog.ui" line="14"/>
        <source>Form</source>
        <translation>Пішін</translation>
    </message>
</context>
<context>
    <name>KAlertDialog</name>
    <message>
        <location filename="../kalertdialog.cpp" line="28"/>
        <source>The file you selected is a directory or does not have permissions, cannot be shredded!</source>
        <translation>Таңдалған файл каталог болып табылады немесе рұқсаттары жоқ, ұсақталуы мүмкін емес!</translation>
    </message>
    <message>
        <location filename="../kalertdialog.cpp" line="31"/>
        <source>sure</source>
        <translation>сенімді</translation>
    </message>
</context>
<context>
    <name>KSureDialog</name>
    <message>
        <location filename="../ksuredialog.cpp" line="26"/>
        <source>Are you sure to start crushing?</source>
        <translation>Сіз міндетті түрде ұсақтауды бастайсыз ба?</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="32"/>
        <source>Crushed files cannot be recovered!</source>
        <translation>Бөлшектелген файлдарды қалпына келтіру мүмкін емес!</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="39"/>
        <source>sure</source>
        <translation>сенімді</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="47"/>
        <source>cancle</source>
        <translation>канкл</translation>
    </message>
</context>
<context>
    <name>ShredDialog</name>
    <message>
        <source>Kylin Shred Manager</source>
        <translation type="vanished">Килин Шред менеджері</translation>
    </message>
    <message>
        <source>Shred Manager</source>
        <translation type="vanished">文件粉碎机</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="303"/>
        <source>No file selected to be shredded</source>
        <translation>Ұсақталу үшін файл таңдалмаған</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="155"/>
        <source>Shred File</source>
        <translation>Ұсақталған файл</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="37"/>
        <source>FileShredder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="154"/>
        <location filename="../shreddialog.cpp" line="199"/>
        <source>The file is completely shredded and cannot be recovered</source>
        <translation>Файл толығымен ұсақталады және қалпына келтірілмеді</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="156"/>
        <source>Deselect</source>
        <translation>Жоққа шығару</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="157"/>
        <source>Add File</source>
        <translation>Файлды қосу</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="158"/>
        <source>Note:File shredding cannot be cancelled, please exercise caution!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Note: The file shredding process cannot be cancelled, please operate with caution!</source>
        <translation type="vanished">Ескерту: Файлды ұсақтау процесін алып тастауға болмайды, сақтықпен жұмыс істеуіңізді сұраймыз!</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="231"/>
        <source>File Shredding ...</source>
        <translation>Файлды ұсақтау ...</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="288"/>
        <source>Please select the file to shred!</source>
        <translation>Файлды ұсақтап таңдауыңызды сұраймын!</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="293"/>
        <source>You did not select a file with permissions!</source>
        <translation>Рұқсаттары бар файлды таңдаған жоқсыз!</translation>
    </message>
    <message>
        <source>Shattering...</source>
        <translation type="vanished">粉碎中...</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="190"/>
        <source>Select file</source>
        <translation>Файлды таңдау</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="190"/>
        <source>All Files(*)</source>
        <translation>Барлық файлдар(*)</translation>
    </message>
    <message>
        <source>Select file!</source>
        <translation type="vanished">请选择文件！</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="253"/>
        <source>Shred successfully!</source>
        <translation>Ойдағыдай ұсақтап қағып ал!</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="266"/>
        <source>Shred failed!</source>
        <translation>Ұсақталған сәтсіздікке ұшырады!</translation>
    </message>
</context>
<context>
    <name>ShredManager</name>
    <message>
        <location filename="../shredmanager.cpp" line="50"/>
        <source>Shred Manager</source>
        <translation>Ұсақталған менеджер</translation>
    </message>
    <message>
        <location filename="../shredmanager.cpp" line="55"/>
        <source>Delete files makes it unable to recover</source>
        <translation>Файлдарды жою қалпына келтіруге мүмкіндік бермейді</translation>
    </message>
</context>
</TS>
